package sysdeliz2.views.comercial.exportacao;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.apache.poi.ss.usermodel.CellCopyPolicy;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import sysdeliz2.controllers.views.comercial.exportacao.CadastroEmbarqueController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.sysdeliz.SdEmbarque;
import sysdeliz2.models.sysdeliz.SdItemEmbarque;
import sysdeliz2.models.sysdeliz.SdItemGradeEmbarque;
import sysdeliz2.models.view.VSdDadosOfPendente;
import sysdeliz2.models.view.VSdDadosProdutoImportacao;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.hibernate.DefaultFilter;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.mails.SimpleMail;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static sysdeliz2.utils.SortUtils.distinctByKey;

public class CadastroEmbarqueView extends CadastroEmbarqueController {

    private SdEmbarque embarqueSelecionado = null;
    private DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    // <editor-fold defaultstate="collapsed" desc="Booleans">
    public final BooleanProperty inEdition = new SimpleBooleanProperty(false);
    private final BooleanProperty isConcluido = new SimpleBooleanProperty(false);
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Bean">
    private ListProperty<SdEmbarque> embarquesBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private ListProperty<SdItemEmbarque> itensEmbarqueBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Filter">
    private final FormFieldMultipleFind<SdEmbarque> codEmbarqueFilter = FormFieldMultipleFind.create(SdEmbarque.class, field -> {
        field.title("Cod Embarque");
    });
    private final FormFieldDate dataFimEmbarqueFilter = FormFieldDate.create(field -> {
        field.title("Data Fim");
        field.value.set(LocalDate.now());
    });
    private final FormFieldDate dataInicioEmbarqueFilter = FormFieldDate.create(field -> {
        field.title("Data Início");
        field.value.set(LocalDate.now().minusWeeks(1));
        field.value.addListener((observable, oldValue, newValue) -> {
            if (newValue.isAfter(dataFimEmbarqueFilter.value.getValue())) {
                dataFimEmbarqueFilter.value.setValue(newValue);
            }
        });
    });
    private final FormFieldComboBox<String> tipoEmbarqueFilter = FormFieldComboBox.create(String.class, field -> {
        field.title("Tipo");
        field.items(FXCollections.observableArrayList(Arrays.asList("", SdEmbarque.TipoEmbarque.EXPORTACAO.name(), SdEmbarque.TipoEmbarque.IMPORTACAO.name())));
    });
    private final FormFieldComboBox<String> statusEmbarqueFilter = FormFieldComboBox.create(String.class, field -> {
        field.title("Status");
        field.items(FXCollections.observableArrayList(Arrays.asList("", SdEmbarque.StatusEmbarque.CRIADO.name(), SdEmbarque.StatusEmbarque.CONCLUIDO.name(), SdEmbarque.StatusEmbarque.FINALIZADO.name())));
    });
    private final FormFieldMultipleFind<VSdDadosOfPendente> addOfFilter = FormFieldMultipleFind.create(VSdDadosOfPendente.class, field -> {
        field.title("OP/OF");
        field.width(300);
        field.getRoot().setAlignment(Pos.BOTTOM_LEFT);
        field.defaults.add(new DefaultFilter("PY", "pais"));
        field.editable.bind(inEdition.and(isConcluido));
        field.codeReference.set("id.numero");
        field.editable.bind(inEdition);
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Field">
    private final FormFieldText codEmbarqueField = FormFieldText.create(field -> {
        field.title("Código Embarque");
        field.editable.set(false);
        field.width(150);
    });
    private final FormFieldDate dataEmbarqueField = FormFieldDate.create(field -> {
        field.title("Data Embarque");
        field.editable.bind(inEdition);
    });
    private final FormFieldText tipoEmbarqueField = FormFieldText.create(field -> {
        field.title("Tipo");
        field.editable.set(false);
        field.width(150);
    });
    private final FormFieldText valorEmbarqueField = FormFieldText.create(field -> {
        field.title("Valor Total");
        field.editable.set(false);
        field.width(190);
    });
    private final FormFieldText qtdeEmbarqueField = FormFieldText.create(field -> {
        field.title("Quantidade Total");
        field.editable.set(false);
        field.width(193);
    });
    private final FormFieldText pesoEmbarqueField = FormFieldText.create(field -> {
        field.title("Peso Total");
        field.editable.set(false);
        field.width(195);
    });
    private final FormFieldText statusEmbarqueField = FormFieldText.create(field -> {
        field.title("Status");
        field.editable.set(false);
        field.width(150);
        field.value.addListener((observable, oldValue, newValue) -> {

            field.removeStyle("info");
            field.removeStyle("warning");
            field.removeStyle("primary");
            field.removeStyle("success");

            if (newValue != null) {
                if (newValue.equals(SdEmbarque.StatusEmbarque.CRIADO.name())) field.addStyle("info");
                else if (newValue.equals(SdEmbarque.StatusEmbarque.EM_EDICAO.name().replace("_", " ")))
                    field.addStyle("warning");
                else if (newValue.equals(SdEmbarque.StatusEmbarque.CONCLUIDO.name())) field.addStyle("primary");
                else if (newValue.equals(SdEmbarque.StatusEmbarque.FINALIZADO.name())) field.addStyle("success");
                else {
                    field.removeStyle("info");
                    field.removeStyle("warning");
                    field.removeStyle("primary");
                    field.removeStyle("success");
                }
            }
        });
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Table">
    private final FormTableView<SdEmbarque> tblEbarques = FormTableView.create(SdEmbarque.class, table -> {
        table.title("Embarques");
        table.expanded();
        table.disable.bind(inEdition);
        table.items.bind(embarquesBean);
        table.selectionModelItem((observable, oldValue, newValue) -> {
            if (newValue != null) {
                embarqueSelecionado = (SdEmbarque) newValue;
                carregaEmbarque();
            }
        });
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(60);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdEmbarque, SdEmbarque>, ObservableValue<SdEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Data");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdEmbarque, SdEmbarque>, ObservableValue<SdEmbarque>>) param -> new ReadOnlyObjectWrapper(StringUtils.toDateFormat(param.getValue().getData())));
                }).build(), /*Data*/
                FormTableColumn.create(cln -> {
                    cln.title("Tipo");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdEmbarque, SdEmbarque>, ObservableValue<SdEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTipo()));
                }).build(), /*Tipo*/
                FormTableColumn.create(cln -> {
                    cln.title("Status");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdEmbarque, SdEmbarque>, ObservableValue<SdEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getStatus()));
                }).build(), /*Status*/
                FormTableColumn.create(cln -> {
                    cln.title("Qtde");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdEmbarque, SdEmbarque>, ObservableValue<SdEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Valor Total");
                    cln.width(130);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdEmbarque, SdEmbarque>, ObservableValue<SdEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPreco()));
                }).build()

        );
        table.factoryRow(param -> {
            return new TableRow<SdEmbarque>() {
                @Override
                protected void updateItem(SdEmbarque item, boolean empty) {
                    super.updateItem(item, empty);
                    clear();

                    if (item != null && !empty) {
                        if (item.getStatus().equals(SdEmbarque.StatusEmbarque.CRIADO.name())) {
                            getStyleClass().add("table-row-info");
                        } else if (item.getStatus().equals(SdEmbarque.StatusEmbarque.CONCLUIDO.name())) {
                            getStyleClass().add("table-row-warning");
                        } else if (item.getStatus().equals(SdEmbarque.StatusEmbarque.FINALIZADO.name())) {
                            getStyleClass().add("table-row-success");
                        }
                    }
                }

                private void clear() {
                    getStyleClass().removeAll("table-row-info", "table-row-success", "table-row-warning");
                }
            };
        });
    });

    private final FormTableView<SdItemEmbarque> tblItensEmbarque = FormTableView.create(SdItemEmbarque.class, table -> {
        table.title("Itens");
        table.expanded();
        table.items.bind(itensEmbarqueBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(80);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemEmbarque, SdItemEmbarque>, ObservableValue<SdItemEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdItemEmbarque, SdItemEmbarque>() {
                            final Button btnRemove = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                btn.tooltip("Remover OF");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("sm").addStyle("danger");
                                btn.disableProperty().bind(inEdition.not());
                                btn.setAlignment(Pos.CENTER);
                            });
                            final Button btnCalculaPesoAuto = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CALCULADORA, ImageUtils.IconSize._16));
                                btn.tooltip("Cálcular Peso Automático");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("sm").addStyle("primary");
                                btn.disableProperty().bind(inEdition.not());
                                btn.setAlignment(Pos.CENTER);
                            });

                            @Override
                            protected void updateItem(SdItemEmbarque item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    btnRemove.setOnAction(evt -> {
                                        table.items.remove(item);
                                        embarqueSelecionado.getListItens().remove(item);
                                        new FluentDao().delete(item);
                                        table.refresh();
                                    });
                                    btnCalculaPesoAuto.setOnAction(evt -> {
                                        calculaPesoAutomatico(item);
                                    });

                                    setGraphic(FormBox.create(box -> {
                                        box.horizontal();
                                        box.alignment(Pos.CENTER);
                                        box.add(btnRemove, btnCalculaPesoAuto);
                                    }));
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("OP/OF");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemEmbarque, SdItemEmbarque>, ObservableValue<SdItemEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumero()));
                }).build(), /*OP/OF*/
                FormTableColumn.create(cln -> {
                    cln.title("Produto");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemEmbarque, SdItemEmbarque>, ObservableValue<SdItemEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getProduto().getCodigo()));
                }).build(), /*Produto*/
                FormTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(300);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemEmbarque, SdItemEmbarque>, ObservableValue<SdItemEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getProduto().getDescricao()));
                }).build(), /*Descrição*/
                FormTableColumn.create(cln -> {
                    cln.title("Un");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemEmbarque, SdItemEmbarque>, ObservableValue<SdItemEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getUnidade()));
                }).build(), /*Qtde*/
                FormTableColumn.create(cln -> {
                    cln.title("Qtde");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemEmbarque, SdItemEmbarque>, ObservableValue<SdItemEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                }).build(), /*Qtde*/
                FormTableColumn.create(cln -> {
                    cln.title("Peso OF");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemEmbarque, SdItemEmbarque>, ObservableValue<SdItemEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue()));

                    cln.format(param -> new TableCell<SdItemEmbarque, SdItemEmbarque>() {
                        @Override
                        protected void updateItem(SdItemEmbarque item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                FormFieldText formFieldText = FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.editable.bind(inEdition);
                                    field.value.setValue(item.getPesoOf() == null ? "0" : item.getPesoOf().toString());
                                    field.focusedListener((observable, oldValue, newValue) -> {
                                        if (!newValue) {
                                            BigDecimal pesoOf = field.value.getValue().equals("") || field.value.isEmpty().get() ? BigDecimal.ZERO : new BigDecimal(field.value.getValue().replace(",", "."));
                                            calculaPeloPesoOfDigitada(item, pesoOf);
                                        }
                                    });

                                    field.mouseClicked(event -> {
                                        field.textField.selectAll();
                                    });
                                });
                                setGraphic(formFieldText.build());
                            }
                        }
                    });
                }).build(), /*Valor*/
                FormTableColumn.create(cln -> {
                    cln.title("Peso Peça");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemEmbarque, SdItemEmbarque>, ObservableValue<SdItemEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue()));

                    cln.format(param -> {
                        return new TableCell<SdItemEmbarque, SdItemEmbarque>() {
                            @Override
                            protected void updateItem(SdItemEmbarque item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                getStyleClass().remove("table-row-danger");

                                if (item != null && !empty && item.getPesoUnidade() != null) {
                                    setText(item.getPesoUnidade() != null ? item.getPesoUnidade().setScale(4, RoundingMode.CEILING).toString() : "");
                                    BigDecimal pesoUnidade = item.getPesoUnidade();
                                    //Compara os valores pra saber se é menor que o minimo ou maior que o maximo
                                    if (item.getProduto() != null && item.getProduto().getPesoMinimo() != null && item.getProduto().getPesoMaximo() != null) {
                                        if (pesoUnidade.compareTo(item.getProduto().getPesoMinimo()) < 0 || pesoUnidade.compareTo(item.getProduto().getPesoMaximo()) > 0) {
                                            getStyleClass().add("table-row-danger");
                                        } else {
                                            getStyleClass().remove("table-row-danger");
                                        }
                                    }
                                }
                            }
                        };
                    });
                }).build(), /*Peso Peça*/
                FormTableColumn.create(cln -> {
                    cln.title("Peso Final");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemEmbarque, SdItemEmbarque>, ObservableValue<SdItemEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPesoTotal() != null ? param.getValue().getPesoTotal().setScale(4, RoundingMode.CEILING) : ""));
                }).build(), /*Peso Peça*/
                FormTableColumn.create(cln -> {
                    cln.title("Valor da OF");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemEmbarque, SdItemEmbarque>, ObservableValue<SdItemEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPrecoTotal()));
                }).build() /*Valor*/

        );
    });

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Button">
    private final Button btnAddOf = FormButton.create(btn -> {
        btn.title("Adicionar");
        btn.addStyle("success");
        btn.width(200);
        btn.disableProperty().bind(inEdition.not().or(isConcluido));
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            if (embarqueSelecionado != null) {
                try {
                    adicionarOfImportacao(addOfFilter.objectValues.getValue());
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    addOfFilter.clear();
                }
            }
        });
    });

    private final Button btnEditarEmbarque = FormButton.create(btn -> {
        btn.title("Editar");
        btn.addStyle("warning");
        btn.disableProperty().bind(inEdition);
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EDIT, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            if (embarqueSelecionado != null) {
                if (embarqueSelecionado.getStatus().equals(SdEmbarque.StatusEmbarque.FINALIZADO.name())) {
                    MessageBox.create(message -> {
                        message.message("Embarque já foi finalizado!");
                        message.type(MessageBox.TypeMessageBox.WARNING);
                        message.showAndWait();
                    });
                } else editarEmbarque();
            }
        });
    });

    private final Button btnDeletarEmbarque = FormButton.create(btn -> {
        btn.title("Excluir");
        btn.addStyle("danger");
        btn.disableProperty().bind(inEdition);
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            if (embarqueSelecionado != null) {
                /*if (embarqueSelecionado.getStatus().equals(SdEmbarque.StatusEmbarque.FINALIZADO.name())) {
                    MessageBox.create(message -> {
                        message.message("Embarque já foi finalizado!");
                        message.type(MessageBox.TypeMessageBox.WARNING);
                        message.showAndWait();
                    });
                } else if (embarqueSelecionado.getStatus().equals(SdEmbarque.StatusEmbarque.CONCLUIDO.name())) {
                    MessageBox.create(message -> {
                        message.message("Embarque está no processo de geração da nota. \nNão é possível excluí-lo!");
                        message.type(MessageBox.TypeMessageBox.WARNING);
                        message.showAndWait();
                    });
                } else*/
                excluirEmbarque();
            }
        });
    });

    private final Button btnCriarEmbarque = FormButton.create(btn -> {
        btn.title("Criar Novo");
        btn.addStyle("info");
        btn.disableProperty().bind(inEdition);
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR_PEDIDO, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            criarEmbarque();
        });
    });

    private final Button btnSalvarEmbarque = FormButton.create(btn -> {
        btn.title("Salvar");
        btn.addStyle("primary");
        btn.disableProperty().bind(inEdition.not());
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.SALVAR, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            embarqueSelecionado.setStatus(SdEmbarque.StatusEmbarque.CRIADO.name());
            embarqueSelecionado = new FluentDao().merge(embarqueSelecionado);
            salvarEmbarque();
        });
    });

    private final Button btnFinalizarEmbarque = FormButton.create(btn -> {
        btn.title("Concluir");
        btn.addStyle("success");
        btn.disableProperty().bind(inEdition.not());
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            finalizarEmbarque();
        });
    });

    private final Button btnCancelarEmbarque = FormButton.create(btn -> {
        btn.title("Cancelar");
        btn.addStyle("danger");
        btn.disableProperty().bind(inEdition.not());
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            inEdition.set(false);
            tblEbarques.tableview().getSelectionModel().clearSelection();
            embarqueSelecionado.setStatus(SdEmbarque.StatusEmbarque.CRIADO.name());
            embarqueSelecionado = null;
            limparCampos();
        });
    });

    private final Button btnExcel = FormButton.create(btn -> {
        btn.title("Exportar");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EXCEL, ImageUtils.IconSize._24));
        btn.addStyle("success");
        btn.setOnAction(evt -> {
            if (embarqueSelecionado != null) {
                criarExcel();
            }
        });
    });

    private final Button btnEnviarEmail = FormButton.create(btn -> {
        btn.title("Enviar Email");
        btn.addStyle("info");
        btn.disableProperty().bind(isConcluido.not());
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            abrirJanelaEmail();
        });
    });

    // </editor-fold>

    public CadastroEmbarqueView() {
        super("Cadastro de Embarque", ImageUtils.getImage(ImageUtils.Icon.EXPORTACAO));
        init();
    }

    private void init() {
        box.getChildren().add(FormBox.create(principal -> {
            principal.horizontal();
            principal.expanded();
            principal.add(FormBox.create(col1 -> {
                col1.vertical();
                col1.size(600, 1000);
                col1.add(FormBox.create(boxHeader -> {
                    boxHeader.vertical();
                    boxHeader.setMaxWidth(600);
                    boxHeader.add(FormTitledPane.create(filter -> {
                        filter.filter();
                        filter.width(600);
                        filter.add(FormBox.create(boxFilter -> {
                            boxFilter.vertical();
                            boxFilter.add(FormBox.create(linha1 -> {
                                linha1.horizontal();
                                linha1.add(codEmbarqueFilter.build(), tipoEmbarqueFilter.build(), statusEmbarqueFilter.build());
                            }));
                            boxFilter.add(FormBox.create(linha2 -> {
                                linha2.horizontal();
                                linha2.add(dataInicioEmbarqueFilter.build(), dataFimEmbarqueFilter.build());
                            }));
                        }));
                        filter.find.setOnAction(evt -> {
                            new RunAsyncWithOverlay(this).exec(task -> {
                                buscarEmbarques(codEmbarqueFilter.objectValues.getValue() == null ? null : codEmbarqueFilter.objectValues.getValue().stream().map(SdEmbarque::getCodigo).toArray(),
                                        tipoEmbarqueFilter.value.getValue(),
                                        statusEmbarqueFilter.value.getValue(),
                                        dataInicioEmbarqueFilter.value.getValue(),
                                        dataFimEmbarqueFilter.value.getValue()
                                );
                                return ReturnAsync.OK.value;
                            }).addTaskEndNotification(taskReturn -> {
                                if (taskReturn.equals(ReturnAsync.OK.value)) {
                                    embarquesBean.set(FXCollections.observableArrayList(embarques));
                                }
                            });
                        });
                        filter.clean.setOnAction(evt -> {
                            codEmbarqueFilter.clear();
                            tipoEmbarqueFilter.select(0);
                            statusEmbarqueFilter.select(0);
                            dataInicioEmbarqueFilter.value.set(LocalDate.now().minusWeeks(1));
                            dataFimEmbarqueFilter.value.set(LocalDate.now());
                        });
                    }));
                }));
                col1.add(FormBox.create(boxTabela -> {
                    boxTabela.vertical();
                    boxTabela.expanded();
                    boxTabela.setMaxWidth(600);
                    boxTabela.expanded();
                    boxTabela.add(tblEbarques.build());
                }));
                col1.add(FormBox.create(buttonBox -> {
                    buttonBox.horizontal();
                    buttonBox.setMaxWidth(600);
                    buttonBox.alignment(Pos.TOP_RIGHT);
                    buttonBox.add(btnDeletarEmbarque, btnCriarEmbarque, btnEditarEmbarque);
                }));
            }));
            principal.add(FormBox.create(col2 -> {
                col2.vertical();
                col2.expanded();
                col2.size(1700, 1000);
                col2.add(FormBox.create(boxHeader -> {
                    boxHeader.horizontal();
                    boxHeader.alignment(Pos.BOTTOM_LEFT);
                    boxHeader.size(1700, 150);
                    boxHeader.add(FormBox.create(boxInfos -> {
                        boxInfos.vertical();
                        boxInfos.title("INFOS");
                        boxInfos.size(750, 150);
                        boxInfos.add(FormBox.create(linha1 -> {
                            linha1.horizontal();
                            linha1.add(codEmbarqueField.build(), dataEmbarqueField.build(), tipoEmbarqueField.build(), statusEmbarqueField.build());
                        }));
                        boxInfos.add(FormBox.create(linha2 -> {
                            linha2.horizontal();
                            linha2.add(qtdeEmbarqueField.build(), pesoEmbarqueField.build(), valorEmbarqueField.build());
                        }));
                    }));
                    boxHeader.add(FormBox.create(boxAddof -> {
                        boxAddof.horizontal();
                        boxAddof.alignment(Pos.BOTTOM_LEFT);
                        boxAddof.add(addOfFilter.build(), btnAddOf);
                    }));
                }));
                col2.add(FormBox.create(boxTabela -> {
                    boxTabela.vertical();
                    boxTabela.size(1700, 750);
                    boxTabela.add(tblItensEmbarque.build());
                }));
                col2.add(FormBox.create(buttonBox -> {
                    buttonBox.horizontal();
                    buttonBox.alignment(Pos.TOP_RIGHT);
                    buttonBox.add(btnSalvarEmbarque, btnCancelarEmbarque, btnFinalizarEmbarque, btnEnviarEmail, btnExcel);
                }));
            }));
        }));
    }

    //<editor-fold desc="Operações Embarque">

    private void criarEmbarque() {
        embarqueSelecionado = new SdEmbarque();
        if (abrirJanelaCriacao()) {
            editarEmbarque();
            carregaTabela();
            embarqueSelecionado = new FluentDao().merge(embarqueSelecionado);
            atualizaCampos();
            isConcluido.set(embarqueSelecionado.getStatus().equals(SdEmbarque.StatusEmbarque.CONCLUIDO.name()));
            tblEbarques.tableview().getSelectionModel().clearSelection();
        }
    }

    private void carregaEmbarque() {
        new RunAsyncWithOverlay(this).exec(task -> {
            embarqueSelecionado = new FluentDao().selectFrom(SdEmbarque.class).where(it -> it.equal("codigo", embarqueSelecionado.getCodigo())).singleResult();
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                itensEmbarqueBean.set(FXCollections.observableArrayList(embarqueSelecionado.getListItens()));
                isConcluido.set(embarqueSelecionado.getStatus().equals(SdEmbarque.StatusEmbarque.CONCLUIDO.name()));
                tblEbarques.refresh();
                atualizaCampos();
            }
        });
    }

    private void atualizaCampos() {
        itensEmbarqueBean.set(FXCollections.observableArrayList(embarqueSelecionado.getListItens()));
        tblItensEmbarque.refresh();
        codEmbarqueField.value.setValue(embarqueSelecionado.getCodigo() == null ? "" : embarqueSelecionado.getCodigo().toString());
        dataEmbarqueField.value.setValue(embarqueSelecionado.getData());
        tipoEmbarqueField.value.setValue(embarqueSelecionado.getTipo() == null ? "" : embarqueSelecionado.getTipo());
        statusEmbarqueField.value.setValue(embarqueSelecionado.getStatus() == null ? "" : embarqueSelecionado.getStatus().replace("_", " "));
        pesoEmbarqueField.value.setValue(embarqueSelecionado.getListItens().stream().filter(it -> it.getPesoTotal() != null).reduce(BigDecimal.ZERO, (partialResult, it) -> it.getPesoTotal().add(partialResult), BigDecimal::add).toString());
        qtdeEmbarqueField.value.setValue(embarqueSelecionado.getListItens().stream().filter(it -> it.getQtde() != null).reduce(BigDecimal.ZERO, (partialResult, it) -> it.getQtde().add(partialResult), BigDecimal::add).toString());
        valorEmbarqueField.value.setValue(embarqueSelecionado.getListItens().stream().filter(it -> it.getPrecoTotal() != null).reduce(BigDecimal.ZERO, (partialResult, it) -> it.getPrecoTotal().add(partialResult), BigDecimal::add).toString());
    }

    private void carregaTabela() {
        if (embarqueSelecionado != null) {
            itensEmbarqueBean.set(FXCollections.observableArrayList(embarqueSelecionado.getListItens()));
            tblItensEmbarque.refresh();
        }
    }

    private void adicionarOfImportacao(ObservableList<VSdDadosOfPendente> listaOfs) {
        for (VSdDadosOfPendente of : listaOfs) {
            if (embarqueSelecionado.getListItens().stream().noneMatch(it -> it.getNumero().equals(of.getId().getNumero()))) {
                VSdDadosProdutoImportacao produtoImportacao = new FluentDao().selectFrom(VSdDadosProdutoImportacao.class).where(it -> it.equal("codigo", of.getCodigo().getCodigo())).singleResult();
                adicionaItensOfImportacao(of, produtoImportacao);
            }
        }
        atualizaCampos();
    }

    private void adicionaItensOfImportacao(VSdDadosOfPendente of, VSdDadosProdutoImportacao produtoImportacao) {
        try {
            SdItemEmbarque item = new SdItemEmbarque(embarqueSelecionado, of, produtoImportacao, of.getQtde());
            List<SdItemGradeEmbarque> listGrade = buscaItensGrade(item, of);

            if (produtoImportacao.getGenero().equals("FEMENINO") && listGrade.stream().filter(it -> it.getTamanho().equals("34")).findFirst().orElse(null) != null) {
                SdItemGradeEmbarque itemGrade = listGrade.stream().filter(it -> it.getTamanho().equals("34")).findFirst().orElse(null);

                SdItemEmbarque itenJuvenil = new SdItemEmbarque(embarqueSelecionado, of, produtoImportacao, itemGrade.getQtde());
                itenJuvenil.getListGrade().add(itemGrade);

                SdItemEmbarque itemAdultos = new SdItemEmbarque(embarqueSelecionado, of, produtoImportacao, of.getQtde() - itemGrade.getQtde());
                itemAdultos.setListGrade(listGrade.stream().filter(it -> it != itemGrade).collect(Collectors.toList()));

                embarqueSelecionado.getListItens().add(itenJuvenil);
                embarqueSelecionado.getListItens().add(itemAdultos);
            } else {
                item.setListGrade(listGrade);
                embarqueSelecionado.getListItens().add(item);
                SysLogger.addSysDelizLog("Cadastro de Embarque", TipoAcao.CADASTRAR, of.getId().getNumero() + " - " + produtoImportacao.getCodigo(), "Of " + of.getId().getNumero() + " importada no embarque " + embarqueSelecionado.getCodigo());
            }
            embarqueSelecionado = new FluentDao().merge(embarqueSelecionado);
        } catch (Exception e) {
            MessageBox.create(message -> {
                message.message("Houve um problema ao adicionar a OF " + of.getId().getNumero() + ", verifique o estado da OF e dos produtos nela inclusos!");
                message.type(MessageBox.TypeMessageBox.ERROR);
                message.showAndWait();
            });
            e.printStackTrace();
        }
    }

    private void salvarEmbarque() {
        try {
            new RunAsyncWithOverlay(this).exec(task -> {
                JPAUtils.clearEntity(embarqueSelecionado);

                embarqueSelecionado = new FluentDao().selectFrom(SdEmbarque.class).where(it -> it.equal("codigo", embarqueSelecionado.getCodigo())).singleResult();
                embarqueSelecionado.setPeso(new BigDecimal(pesoEmbarqueField.value.getValue()));
                embarqueSelecionado.setPreco(new BigDecimal(valorEmbarqueField.value.getValue()));
                embarqueSelecionado.setQtde(new BigDecimal(qtdeEmbarqueField.value.getValue()));
                embarqueSelecionado.setData(dataEmbarqueField.value.getValue());
                calculaDivisaoPesosItensGrade();
                for (SdItemEmbarque item : embarqueSelecionado.getListItens()) {
                    if (item.getPesoOf() != null && item.getPesoOf() != BigDecimal.ZERO) {
                        item.setPesoTotal(item.getListGrade().stream().reduce(BigDecimal.ZERO, (partial, it) -> it.getPesoLiquido().add(partial), BigDecimal::add));
                        item = new FluentDao().merge(item);
                    }
                }

                embarqueSelecionado = new FluentDao().merge(embarqueSelecionado);

                return ReturnAsync.OK.value;
            }).addTaskEndNotification(taskReturn -> {
                if (taskReturn.equals(ReturnAsync.OK.value)) {
                    inEdition.set(false);
                    SysLogger.addSysDelizLog("Cadastro de Embarque", TipoAcao.CADASTRAR, String.valueOf(embarqueSelecionado.getCodigo()), "Embarque " + embarqueSelecionado.getCodigo() + " salvo!");
                    atualizaCampos();
                    MessageBox.create(message -> {
                        message.message("Embarque salvo com sucesso!");
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.position(Pos.TOP_RIGHT);
                        message.notification();
                    });
                }
            });
        } catch (Exception e) {
            MessageBox.create(message -> {
                message.message("Erro ao salvar o embarque!");
                message.type(MessageBox.TypeMessageBox.ERROR);
                message.showAndWait();
            });
            e.printStackTrace();
        }
    }

    private void excluirEmbarque() {
        if (embarqueSelecionado != null) {
            if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                message.message("Tem certeza que deseja exluir o embarque?");
                message.showAndWait();
            }).value.get())) {
                new FluentDao().delete(embarqueSelecionado);
                embarquesBean.remove(embarqueSelecionado);
                SysLogger.addSysDelizLog("Cadastro de Embarque", TipoAcao.EXCLUIR, String.valueOf(embarqueSelecionado.getCodigo()), "Embarque " + embarqueSelecionado.getCodigo() + " excluído!");
                embarqueSelecionado = null;
                limparCampos();
            }
        }
    }

    private void limparCampos() {
        codEmbarqueField.value.setValue("");
        dataEmbarqueField.value.setValue(LocalDate.now());
        tipoEmbarqueField.value.setValue("");
        statusEmbarqueField.value.setValue("");

        pesoEmbarqueField.value.setValue("");
        qtdeEmbarqueField.value.setValue("");
        valorEmbarqueField.value.setValue("");

        tblItensEmbarque.items.clear();
    }

    private void finalizarEmbarque() {
        SysLogger.addSysDelizLog("Cadastro de Embarque", TipoAcao.CADASTRAR, String.valueOf(embarqueSelecionado.getCodigo()), "Embarque " + embarqueSelecionado.getCodigo() + " concluído!");
        embarqueSelecionado.setStatus(SdEmbarque.StatusEmbarque.CONCLUIDO.name());
        embarqueSelecionado = new FluentDao().merge(embarqueSelecionado);
        salvarEmbarque();
    }

    private void editarEmbarque() {
        embarqueSelecionado.setStatus(SdEmbarque.StatusEmbarque.EM_EDICAO.name());
        inEdition.set(true);
    }
    //</editor-fold>

    //<editor-fold desc="Calculos de Peso">
    private void divisaoDePesosEntreOfsIguais(SdItemEmbarque item, SdItemEmbarque segundoItem, BigDecimal pesoOF) {

        if (pesoOF.equals(BigDecimal.ZERO)) {
            item.setPesoOf(null);
            item.setPesoUnidade(BigDecimal.ZERO);
            item.setPesoTotal(BigDecimal.ZERO);
            return;
        }

        if (pesoOF.equals(item.getPesoOf())) return;

        if (segundoItem.getPesoOf() != null) {
            if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                message.message("Você está alterando um campo já calculado!\nCaso você altere este campo, outro produto da mesma OF será alterado.\nTem certeza que deseja fazer isso?");
                message.showAndWait();
            }).value.get())) {
                calculaRegraDe3Peso(item, pesoOF, segundoItem);
            }
        } else calculaRegraDe3Peso(item, pesoOF, segundoItem);
    }

    private void calculaRegraDe3Peso(SdItemEmbarque item, BigDecimal pesoOf, SdItemEmbarque segundoItem) {
        BigDecimal qtdeTotal = item.getQtde().add(segundoItem.getQtde());
        calculaPesosItem(item, pesoOf, qtdeTotal);
        calculaPesosItem(segundoItem, pesoOf, qtdeTotal);
    }

    private void calculaPesosItem(SdItemEmbarque item, BigDecimal pesoTotal, BigDecimal qtdeTotal) {
        item.setPesoOf(item.getQtde().multiply(pesoTotal).divide(qtdeTotal, 9, RoundingMode.CEILING));
        item.setPesoUnidade(item.getPesoOf().add(item.getQtde().multiply(new BigDecimal("0.007"))).divide(item.getQtde(), 9, RoundingMode.CEILING));
        item.setPesoTotal(item.getPesoUnidade().multiply(item.getQtde()));
        new FluentDao().merge(item);
    }

    private void calculaPesoAutomatico(SdItemEmbarque item) {
        BigDecimal pesoOf = item.getProduto().getPesoMedio().multiply(item.getQtde());
        SdItemEmbarque segundoItem = embarqueSelecionado.getListItens().stream().filter(it -> it != item && it.getNumero().equals(item.getNumero())).findFirst().orElse(null);
        if (segundoItem != null)
            pesoOf = pesoOf.add(segundoItem.getProduto().getPesoMedio().multiply(segundoItem.getQtde()));
        calculaPeloPesoOfDigitada(item, pesoOf);
    }

    private void calculaPeloPesoOfDigitada(SdItemEmbarque item, BigDecimal pesoOF) {
        SdItemEmbarque segundoItem = embarqueSelecionado.getListItens().stream().filter(it -> it != item && it.getNumero().equals(item.getNumero())).findFirst().orElse(null);
        if (isOfUnica(item)) {
            item.setPesoOf(pesoOF);
            item.setPesoUnidade(item.getPesoOf().add(item.getQtde().multiply(new BigDecimal("0.007"))).divide(item.getQtde(), 4, RoundingMode.CEILING));
            item.setPesoTotal(item.getPesoUnidade().multiply(item.getQtde()));
            new FluentDao().merge(item);
        } else {
            divisaoDePesosEntreOfsIguais(item, segundoItem, pesoOF);
        }
        tblItensEmbarque.refresh();
        atualizaCampos();
    }
    //</editor-fold>

    //<editor-fold desc="Email">
    private void enviarEmail(List<String> destinatarios, List<String> copias, List<File> files) {
        SimpleMail mail = SimpleMail.INSTANCE;
        mail.setSender(Globals.getUsuarioLogado().getEmail());
        destinatarios.forEach(mail::addDestinatario);
        files.forEach(it -> mail.addAttachment(it.getAbsolutePath()));
        copias.forEach(mail::addCC);
        mail.comAssunto("Embarque do dia " + embarqueSelecionado.getData());
        mail.comCorpoHtml(this::criaCorpoHtmlEmail);
        mail.send();
        MessageBox.create(message -> {
            message.message("Email enviado com sucesso!");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.show();
        });
    }

    private String criaCorpoHtmlEmail() {
        StringBuilder sb = new StringBuilder();
        sb.append("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
                "    <title>Email SysDeliz</title>\n" +
                "    <style>\n" +
                "        table {\n" +
                "          border-collapse: collapse;\n" +
                "          width: 30%;\n" +
                "          margin: 50px 0 50px 0;\n" +
                "        }\n" +
                "        \n" +
                "        td, th {\n" +
                "          border: 1px solid #dddddd;\n" +
                "          text-align: left;\n" +
                "          padding: 5px;\n" +
                "        }\n" +
                "        \n" +
                "        tr:nth-child(even) {\n" +
                "          background-color: #dddddd;\n" +
                "        }\n" +
                "        </style>\n" +
                "</head>\n" +
                "\n" +
                "\n" +
                "<body>\n" +
                "    <div>\n" +
                "        <h2>EMBARQUE DIA " + embarqueSelecionado.getData().format(dateFormatter) + " - AMERICA</h2>\n" +
                "        \n" +
                "        <span style=\"margin-bottom: 25px;\">Olá, segue em anexo o documento referente ao embaruque do dia " + embarqueSelecionado.getData().format(dateFormatter) + ".</span>\n" +
                "\n" +
                "\n" +
                "        Atenciosamente,<br>\n" +
                "        <h4 style=\"margin-bottom: 0%; padding-bottom: 0%;\">Solange Dalpiaz <br> \n" +
                "        Compras</h4>\n" +
                "\n" +
                "        <h4>Deliz Indústria da Moda</h4>\n" +
                "        <b><i>Telefone: </i> (48) 3641 1900</b> <br>\n" +
                "        <i>Email: fabianasoares@deliz.com.br <br>\n" +
                "            Site: www.deliz.com.br </i>\n" +
                "\n" +
                "\n" +
                "    </div>\n" +
                "</body>\n" +
                "\n" +
                "</html>");
        return sb.toString();
    }
    //</editor-fold>

    //<editor-fold desc="Utils">
    private void calculaDivisaoPesosItensGrade() throws IndexOutOfBoundsException {
        embarqueSelecionado.getListItens().forEach(item -> {
            if (item.getPesoOf() != null) {
                List<SdItemGradeEmbarque> listGrade = item.getListGrade();

                try {
                    sortListaTamanhosGrade(listGrade);

                    BigDecimal pesoUN = (BigDecimal) ((Map<String, Object>) new NativeDAO().runNativeQuery(String.format("" +
                            "select pr.peso from sd_proforma_modelagem_001 pr\n" +
                            "join v_sd_dados_produto_importacao prd\n" +
                            "on pr.descricao = prd.descricion\n" +
                            "where prd.codigo = '%s'", item.getProduto().getCodigo())).get(0)).get("PESO");

                    for (SdItemGradeEmbarque itemGrade : listGrade) {
                        itemGrade.setPesoUnidade(pesoUN);
                        itemGrade.setPesoBruto(pesoUN.multiply(BigDecimal.valueOf(itemGrade.getQtde())));
                        pesoUN = pesoUN.add(new BigDecimal("0.025"));
                    }

                    BigDecimal pesoBrutoTotal = listGrade.stream().reduce(BigDecimal.ZERO, (partialResult, it) -> partialResult.add(it.getPesoBruto()), BigDecimal::add);
                    BigDecimal diferenca = item.getPesoTotal().subtract(pesoBrutoTotal);

                    for (SdItemGradeEmbarque itemGrade : listGrade) {
                        BigDecimal porcentagem = itemGrade.getPesoBruto().divide(pesoBrutoTotal, 9, RoundingMode.CEILING);
                        itemGrade.setPesoLiquido((porcentagem.multiply(diferenca)).add(itemGrade.getPesoBruto()));
                        itemGrade.setPesoUnidade(itemGrade.getPesoLiquido().divide(BigDecimal.valueOf(itemGrade.getQtde()), 9, RoundingMode.CEILING));
                        itemGrade.setPesoBruto(itemGrade.getPesoLiquido().add(new BigDecimal("0.02")));
                    }
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                    throw new IndexOutOfBoundsException(item.getNumero());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private boolean abrirJanelaCriacao() {
        AtomicReference<Boolean> retorno = new AtomicReference<>(false);
        new Fragment().show(fragment -> {
            FormFieldDate dataFilter = FormFieldDate.create(field -> {
                field.title("Data");
                field.width(150);
                field.value.set(LocalDate.now());
            });
            fragment.title.setText("Data do Embarque");
            fragment.size(250.0, 150.0);

            fragment.box.getChildren().add(FormBox.create(principal -> {
                principal.horizontal();
                principal.add(dataFilter.build());
            }));
            fragment.buttonsBox.getChildren().add(FormButton.create(botao -> {
                botao.title("Selecionar");
                botao.addStyle("success");
                botao.icon(ImageUtils.getIconButton(ImageUtils.ButtonIcon.SELECT, ImageUtils.IconSize._24));
                botao.setOnAction(event -> {
                    embarqueSelecionado.setData(dataFilter.value.getValue());
                    embarqueSelecionado.setStatus(SdEmbarque.StatusEmbarque.EM_EDICAO.name());
                    embarqueSelecionado.setTipo(SdEmbarque.TipoEmbarque.IMPORTACAO.name());
                    retorno.set(true);
                    fragment.close();
                });
            }));
        });
        return retorno.get();
    }

    private boolean isOfUnica(SdItemEmbarque item) {
        return embarqueSelecionado.getListItens().stream().noneMatch(it -> it != item && it.getNumero().equals(item.getNumero()));
    }

    private void sortListaTamanhosGrade(List<SdItemGradeEmbarque> listGrade) {
        if (listGrade.get(0).getTamanho().matches("[0-9]*")) {
            listGrade.sort(Comparator.comparingInt(s -> Integer.parseInt(s.getTamanho())));
        } else {
            listGrade.sort((o1, o2) -> {
                int result = 0;

                if (o1.getTamanho().equals("PP")) {
                    if (!o2.getTamanho().equals("PP")) result = -1;
                }

                if (o1.getTamanho().equals("P")) {
                    if ((o2.getTamanho().equals("PP"))) result = 1;
                    else if (o2.getTamanho().equals("P")) result = 0;
                    else result = -1;
                }

                if (o1.getTamanho().equals("M")) {
                    if (o2.getTamanho().equals("M")) result = 0;
                    else if ((o2.getTamanho().equals("PP") || (o2.getTamanho().equals("P")))) result = 1;
                    else result = -1;
                }

                if (o1.getTamanho().equals("G")) {
                    if (o2.getTamanho().equals("G")) result = 0;
                    else if ((o2.getTamanho().equals("PP") || (o2.getTamanho().equals("P") || (o2.getTamanho().equals("M")))))
                        result = 1;
                    else result = -1;
                }

                if (o1.getTamanho().equals("GG")) {
                    if (o2.getTamanho().equals("GG")) result = 0;
                    else if (o2.getTamanho().equals("XGG")) result = -1;
                    else result = 1;
                }

                if (o1.getTamanho().equals("XGG")) {
                    if (!o2.getTamanho().equals("XGG")) result = -1;
                }
                return result;
            });
        }
    }

    //</editor-fold>

    //<editor-fold desc="Excel">
    private void criarExcel() {
        String path = GUIUtils.showSaveFileDialog(new ArrayList<>(Collections.singleton(new FileChooser.ExtensionFilter("Arquivo Excel .xlsx", "*.xlsx"))));
        if (path != null && !path.equals("")) {
            new RunAsyncWithOverlay(this).exec(task -> {
                try {
                    XSSFWorkbook workbook;
                    FileInputStream file = new FileInputStream("C:\\SysDelizLocal\\local_files\\ModeloBaseEmbarque.xlsx");
                    workbook = new XSSFWorkbook(file);
                    XSSFSheet sheet = workbook.getSheetAt(0);
                    sheet.getRow(0).getCell(0).setCellValue("EMBARQUE DIA " + embarqueSelecionado.getData().format(dateFormatter) + " - AMERICA");
                    int contador = 2;
                    for (SdItemEmbarque item : embarqueSelecionado.getListItens().stream().filter(distinctByKey(SdItemEmbarque::getNumero)).collect(Collectors.toList())) {
                        List<Object> listCoresItem = getItensExcelComCor(item);
                        for (Object objetos : listCoresItem) {
                            if (contador != 2) {
                                sheet.copyRows(contador - 1, contador - 1, contador, new CellCopyPolicy());
                            }
                            XSSFRow row = sheet.getRow(contador);

                            Map<String, Object> mapInformacoesItem = (HashMap<String, Object>) objetos;

                            row.getCell(0).setCellValue(embarqueSelecionado.getData().format(dateFormatter));
                            row.getCell(1).setCellValue(mapInformacoesItem.get("CODCOL").toString());
                            row.getCell(2).setCellValue(mapInformacoesItem.get("MARCA").toString());
                            row.getCell(3).setCellValue(embarqueSelecionado.getData().format(dateFormatter));
                            row.getCell(4).setCellValue(mapInformacoesItem.get("TIPO").toString());
                            row.getCell(5).setCellValue(mapInformacoesItem.get("NCMPROD").toString());
                            row.getCell(6).setCellValue(mapInformacoesItem.get("CODIGO").toString() + "-COSTP");
                            row.getCell(7).setCellValue(mapInformacoesItem.get("DESCRICAO").toString());
                            row.getCell(8).setCellValue(mapInformacoesItem.get("NCMMAT").toString());
                            row.getCell(9).setCellValue(mapInformacoesItem.get("TECIDOPRINCIAL").toString());
                            row.getCell(10).setCellValue(mapInformacoesItem.get("DESCTECIDO").toString());
                            row.getCell(11).setCellValue(mapInformacoesItem.get("COR").toString());
                            row.getCell(12).setCellValue(item.getNumero());
                            row.getCell(13).setCellValue(mapInformacoesItem.get("CODIGO").toString());
                            row.getCell(14).setCellValue(mapInformacoesItem.get("QTDE").toString());
                            row.getCell(15).setCellValue(mapInformacoesItem.get("OBS") == null ? "" : mapInformacoesItem.get("OBS").toString());
                            row.getCell(16).setCellValue(mapInformacoesItem.get("DT_ENTREGA") == null ? "" : mapInformacoesItem.get("DT_ENTREGA").toString());
                            row.getCell(17).setCellValue(mapInformacoesItem.get("PRECO_DOLAR").toString());
                            row.getCell(18).setCellValue(mapInformacoesItem.get("TOTAL").toString());
                            row.getCell(19).setCellValue(mapInformacoesItem.get("COMPOSICAO").toString());
                            row.getCell(20).setCellValue(item.getPesoOf() == null ? 0 : item.getPesoOf().doubleValue());
                            row.getCell(21).setCellValue(item.getPesoUnidade() == null ? 0 : item.getPrecoUnidade().doubleValue());
                            row.getCell(22).setCellValue(item.getPesoUnidade() == null ? 0 : item.getPesoUnidade().multiply(new BigDecimal(mapInformacoesItem.get("QTDE").toString())).doubleValue());
                            contador++;
                        }
                    }
                    file.close();
                    FileOutputStream outFile = new FileOutputStream(path);
                    workbook.write(outFile);
                    outFile.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    ExceptionBox.build(message -> {
                        message.exception(e);
                        message.showAndWait();
                    });
                }
                return ReturnAsync.OK.value;
            }).addTaskEndNotification(taskReturn -> {
                if (taskReturn.equals(ReturnAsync.OK.value)) {
                    MessageBox.create(message -> {
                        message.message("Excel exportado com sucesso!");
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.show();
                    });
                }
            });
        }
    }

    private List<Object> getItensExcelComCor(SdItemEmbarque item) {
        List<Object> listCoresItem = new ArrayList<>();
        try {
            listCoresItem = new NativeDAO().runNativeQuery("" +
                    "SELECT PRD.CODCOL,\n" +
                    "       PRD.MARCA,\n" +
                    "       OF1.PERIODO TIPO,\n" +
                    "       PRD.NCM NCMPROD,\n" +
                    "       PRD.CODIGO,\n" +
                    "       PRD.DESCRICAO,\n" +
                    "       TFIS.DESCRICAO NCMMAT,\n" +
                    "       PRD.TECIDOPRINCIAL,\n" +
                    "       PRD.DESCTECIDO,\n" +
                    "       SKU.COR || ' | ' || COR.DESCRICAO COR,\n" +
                    "       SKU.NUMERO,\n" +
                    "       PRD.CODIGO,\n" +
                    "       SUM(QTDE) QTDE,\n" +
                    "       TO_CHAR(OF1.OBSERVACAO) OBS,\n" +
                    "       TO_CHAR(PRD.DTENTREGA, 'MONTH') DT_ENTREGA,\n" +
                    "       EXPO.PRECO_DOLAR,\n" +
                    "       (EXPO.PRECO_DOLAR * SUM(QTDE)) TOTAL,\n" +
                    "       EXPO.COMPOSICAO\n" +
                    "\n" +
                    "  FROM V_SD_DADOS_OF_PENDENTE_SKU SKU\n" +
                    "  JOIN V_SD_DADOS_PRODUTO PRD\n" +
                    "    ON SKU.CODIGO = PRD.CODIGO\n" +
                    "  JOIN OF1_001 OF1\n" +
                    "    ON OF1.NUMERO = SKU.NUMERO\n" +
                    "  JOIN V_SD_DADOS_PRODUTO_IMPORTACAO EXPO\n" +
                    "    ON EXPO.CODIGO = PRD.CODIGO\n" +
                    "  JOIN CADCOR_001 COR\n" +
                    "    ON COR.COR = SKU.COR\n" +
                    "  JOIN MATERIAL_001 MAT\n" +
                    "    ON MAT.CODIGO = PRD.TECIDOPRINCIAL\n" +
                    "  JOIN TABFIS_001 TFIS\n" +
                    "    ON TFIS.CODIGO = MAT.CODFIS\n" +
                    "\n" +
                    " WHERE SKU.NUMERO = '%s'" +
                    " GROUP BY PRD.CODCOL,\n" +
                    "          PRD.MARCA,\n" +
                    "          OF1.PERIODO,\n" +
                    "          PRD.CODIGO,\n" +
                    "          PRD.DESCRICAO,\n" +
                    "          PRD.TECIDOPRINCIAL,\n" +
                    "          PRD.DESCTECIDO,\n" +
                    "          TO_CHAR(OF1.OBSERVACAO),\n" +
                    "          SKU.COR,\n" +
                    "          COR.DESCRICAO,\n" +
                    "          SKU.NUMERO,\n" +
                    "          PRD.CODIGO,\n" +
                    "          EXPO.PRECO_DOLAR,\n" +
                    "          EXPO.COMPOSICAO,\n" +
                    "          PRD.DTENTREGA,\n" +
                    "          PRD.NCM,\n" +
                    "          TFIS.DESCRICAO\n", item.getNumero());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listCoresItem;
    }
    //</editor-fold>

    private void abrirJanelaEmail() {
        new Fragment().show(fragment -> {
            AtomicReference<List<File>> files = new AtomicReference<>(new ArrayList<>());
            FormFieldText destinatariosField = FormFieldText.create(field -> {
                field.width(300);
                field.title("Destinatários");
            });

            FormFieldText copiaField = FormFieldText.create(field -> {
                field.title("CC");
            });

            FormFieldText arquivosField = FormFieldText.create(field -> {
                field.title("Arquivos");
                field.editable.set(false);
            });

            Button btnAnexarArq = FormButton.create(button -> {
                button.icon(ImageUtils.getIcon(ImageUtils.Icon.ANEXO, ImageUtils.IconSize._16));
                button.addStyle("warning");
                button.setOnAction(event -> {
                    if (files.get() != null) files.getAndSet(new ArrayList<>());
                    arquivosField.value.setValue("");
                    FileChooser fs = new FileChooser();
                    files.set(fs.showOpenMultipleDialog(new Stage()));
                    if (files.get() != null) {
                        files.get().forEach(it -> {
                            arquivosField.value.setValue(arquivosField.value.getValueSafe() + it.getName() + " ,");
                        });
                    }
                });
            });

            fragment.title("Configurações Email");
            fragment.size(400.0, 200.0);
            fragment.box.getChildren().add(FormBox.create(principal -> {
                principal.vertical();
                principal.add(FormBox.create(linha1 -> {
                    linha1.horizontal();
                    linha1.alignment(Pos.BOTTOM_LEFT);
                    linha1.add(destinatariosField.build(), btnAnexarArq);
                }));
                principal.add(copiaField.build());
                principal.add(arquivosField.build());
            }));

            fragment.buttonsBox.getChildren().add(FormButton.create(botao -> {
                botao.title("Concluir");
                botao.addStyle("success");
                botao.icon(ImageUtils.getIconButton(ImageUtils.ButtonIcon.SELECT, ImageUtils.IconSize._24));
                botao.setOnAction(event -> {
                    if (destinatariosField.value.isEmpty().get()) {
                        MessageBox.create(message -> {
                            message.message("Preencha o campo com os destinatários para prosseguir!");
                            message.type(MessageBox.TypeMessageBox.WARNING);
                            message.showAndWait();
                        });
                    } else {
                        List<String> emailsDestinatarios = Arrays.asList(destinatariosField.value.getValue().split(","));
                        List<String> emailsCopia = copiaField.value.isEmpty().get() ? new ArrayList<>() : Arrays.asList(copiaField.value.getValue().split(","));
                        enviarEmail(emailsCopia, emailsDestinatarios, files.get());
                        fragment.close();
                    }
                });
            }));
        });
    }
}
