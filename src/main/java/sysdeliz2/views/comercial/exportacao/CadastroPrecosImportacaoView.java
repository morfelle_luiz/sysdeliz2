package sysdeliz2.views.comercial.exportacao;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import sysdeliz2.controllers.views.comercial.exportacao.CadastroPrecosImportacaoController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.sysdeliz.SdGrupoModelagem;
import sysdeliz2.models.sysdeliz.SdProformaModelagem;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;

import java.math.BigDecimal;
import java.util.Arrays;

public class CadastroPrecosImportacaoView extends CadastroPrecosImportacaoController {

    // <editor-fold defaultstate="collapsed" desc="Bean">
    private final ListProperty<SdProformaModelagem> proformaBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Table">
    private final FormTableView<SdProformaModelagem> proformaModelagemTbl = FormTableView.create(SdProformaModelagem.class, table -> {
        table.items.bind(proformaBean);
        table.title("Preços");
        table.expanded();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Ações");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(115.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdProformaModelagem, SdProformaModelagem>, ObservableValue<SdProformaModelagem>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdProformaModelagem, SdProformaModelagem>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnVisualizar = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIconButton(ImageUtils.ButtonIcon.FIND, ImageUtils.IconSize._16));
                                btn.tooltip("Visualizar Cadastro");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("info");
                            });
                            final Button btnEditar = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EDIT, ImageUtils.IconSize._16));
                                btn.tooltip("Editar Cadastro");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("warning");
                            });
                            final Button btnExcluir = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.TRASH, ImageUtils.IconSize._16));
                                btn.tooltip("Excluir Cadastro");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("danger");
                            });

                            @Override
                            protected void updateItem(SdProformaModelagem item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {

                                    btnVisualizar.setOnAction(evt -> {
                                        abrirCadastro(item);
                                    });
                                    btnEditar.setOnAction(evt -> {
                                        editarCadastro(item);
                                    });
                                    btnExcluir.setOnAction(evt -> {
                                        excluirCadastro(item);
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnVisualizar, btnEditar, btnExcluir);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdProformaModelagem, SdProformaModelagem>, ObservableValue<SdProformaModelagem>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(200);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdProformaModelagem, SdProformaModelagem>, ObservableValue<SdProformaModelagem>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodGrupo().getGrupo()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Preço");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdProformaModelagem, SdProformaModelagem>, ObservableValue<SdProformaModelagem>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPreco().toString()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Gênero");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdProformaModelagem, SdProformaModelagem>, ObservableValue<SdProformaModelagem>>) param -> new ReadOnlyObjectWrapper(param.getValue().getGenero()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Marca");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdProformaModelagem, SdProformaModelagem>, ObservableValue<SdProformaModelagem>>) param -> new ReadOnlyObjectWrapper(param.getValue().getMarca()));
                }).build() /*Código*/
        );
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Filter">
    private final FormFieldComboBox<String> generoFitler = FormFieldComboBox.create(String.class, field -> {
        field.width(80);
        field.title("Gênero");
        field.items(FXCollections.observableArrayList(Arrays.asList("Todos", "M", "F", "U")));
        field.select(0);
    });

    private final FormFieldMultipleFind<Marca> codMarcaFilter = FormFieldMultipleFind.create(Marca.class, field -> {
        field.title("Marca");
        field.toUpper();
    });

    private final FormFieldMultipleFind<SdGrupoModelagem> grupoModelagemFilter = FormFieldMultipleFind.create(SdGrupoModelagem.class, field -> {
        field.title("Grupo Modelagem");
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Vbox">
    private final VBox boxListagem = (VBox) super.tabs.getTabs().get(0).getContent();
    private final VBox boxManutencao = (VBox) super.tabs.getTabs().get(1).getContent();
    private final FormNavegation<SdProformaModelagem> navegation = FormNavegation.create(nav -> {
        nav.withNavegation(proformaModelagemTbl);
        nav.withActions(true, true, true);
        nav.withReturn(evt -> super.tabs.getSelectionModel().select(0));
        nav.btnAddRegister(evt -> novoCadastro());
        nav.btnDeleteRegister(evt -> excluirCadastro((SdProformaModelagem) nav.selectedItem.get()));
        nav.btnEditRegister(evt -> {
            editarCadastro((SdProformaModelagem) nav.selectedItem.get());
        });
        nav.btnSave(evt -> {
            salvarCadastro();
        });
        nav.btnCancel(evt -> cancelarCadastro());
        nav.selectedItem.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                carregaDados((SdProformaModelagem) newValue);
            }
        });
    });

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Fields">

    private final FormFieldText codigoField = FormFieldText.create(field -> {
        field.title("Código");
        field.editable.set(false);
    });

    private final FormFieldText descricaoField = FormFieldText.create(field -> {
        field.title("Descrição");
        field.width(250);
        field.editable.bind(navegation.inEdition);
    });

    private final FormFieldSingleFind<SdGrupoModelagem> grupoModelagemField = FormFieldSingleFind.create(SdGrupoModelagem.class, field -> {
        field.title("Grupo Modelagem");
        field.editable.bind(navegation.inEdition);
    });

    private final FormFieldText precoField = FormFieldText.create(field -> {
        field.title("Preço");
        field.editable.bind(navegation.inEdition);
    });

    private final FormFieldSingleFind<Marca> marcaField = FormFieldSingleFind.create(Marca.class, field -> {
        field.title("Marca");
        field.editable.bind(navegation.inEdition);
    });

    private final FormFieldComboBox<String> generoField = FormFieldComboBox.create(String.class, field -> {
        field.items(FXCollections.observableArrayList(Arrays.asList("M", "F", "U")));
        field.width(80);
        field.title("Gênero");
        field.editable.bind(navegation.inEdition);
    });

    // </editor-fold>

    public CadastroPrecosImportacaoView() {
        super("Cadastro Preços Importação", ImageUtils.getImage(ImageUtils.Icon.NATUREZA), new String[]{"Listagem", "Manutenção"});
        initListagem();
        intiManutencao();
    }

    private void initListagem() {
        boxListagem.getChildren().add(FormBox.create(principal -> {
            principal.vertical();
            principal.expanded();
            principal.add(FormBox.create(boxHeader -> {
                boxHeader.horizontal();
                boxHeader.add(FormBox.create(boxTitledFilter -> {
                    boxTitledFilter.vertical();
                    boxTitledFilter.add(FormTitledPane.create(filter -> {
                        filter.filter();
                        filter.add(FormBox.create(boxFilter -> {
                            boxFilter.vertical();
                            boxFilter.add(FormBox.create(linha1 -> {
                                linha1.horizontal();
                                linha1.add(codMarcaFilter.build(), generoFitler.build(), grupoModelagemFilter.build());
                            }));
                        }));
                        filter.find.setOnAction(evt -> {
                            new RunAsyncWithOverlay(this).exec(task -> {
                                buscarPrecos(
                                        codMarcaFilter.objectValues.isNull().get() ? new String[]{} : codMarcaFilter.objectValues.stream().map(Marca::getCodigo).toArray(),
                                        grupoModelagemFilter.objectValues.isNull().get() ? new String[]{} : grupoModelagemFilter.objectValues.stream().map(it -> String.valueOf(it.getCodigo())).toArray(),
                                        generoFitler.value.getValue()
                                );
                                return ReturnAsync.OK.value;
                            }).addTaskEndNotification(taskReturn -> {
                                if (taskReturn.equals(ReturnAsync.OK.value)) {
                                    proformaBean.set(FXCollections.observableArrayList(precosModelagem));
                                }
                            });
                        });
                        filter.clean.setOnAction(evt -> {
                            codMarcaFilter.clear();
                            generoFitler.select(0);
                            grupoModelagemFilter.clear();
                        });
                    }));
                }));
            }));
            principal.add(FormBox.create(boxCenter -> {
                boxCenter.horizontal();
                boxCenter.expanded();
                boxCenter.add(proformaModelagemTbl.build());
            }));
            principal.add(FormBox.create(boxFooter -> {
                boxFooter.horizontal();
                boxFooter.add();
            }));
        }));
    }

    private void intiManutencao() {
        boxManutencao.getChildren().add(navegation);
        boxManutencao.getChildren().add(FormBox.create(principal -> {
            principal.vertical();
            principal.add(FormBox.create(l1 -> {
                l1.horizontal();
                l1.add(codigoField.build(), descricaoField.build());
            }));
            principal.add(FormBox.create(l2 -> {
                l2.horizontal();
                l2.add(grupoModelagemField.build(), precoField.build(), generoField.build(), marcaField.build());
            }));
        }));
    }

    private void carregaDados(SdProformaModelagem newValue) {
        codigoField.value.setValue(String.valueOf(newValue.getCodigo()));
        descricaoField.value.setValue(newValue.getDescricao());
        grupoModelagemField.value.setValue(newValue.getCodGrupo());
        precoField.value.setValue(newValue.getPreco().toString());
        generoField.select(newValue.getGenero());
        marcaField.value.setValue(newValue.getMarca());
    }

    private void cancelarCadastro() {
        proformaModelagemTbl.refresh();
        navegation.inEdition.set(false);
        navegation.selectedItem.set(null);
        limparCampos();
        if (!proformaModelagemTbl.items.isEmpty()) proformaModelagemTbl.selectItem(0);
    }

    private void limparCampos() {
        codigoField.clear();
        descricaoField.clear();
        grupoModelagemField.clear();
        precoField.clear();
        generoField.select(0);
        marcaField.clear();
    }

    private void salvarCadastro() {
        valoresCampos();
        navegation.inEdition.set(false);
        SdProformaModelagem item = navegation.selectedItem.get();
        item = new FluentDao().merge(item);
        MessageBox.create(message -> {
            message.message("Cadastro realizado com sucesso!");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }

    private void valoresCampos() {
        SdProformaModelagem item = navegation.selectedItem.get();
        item.setDescricao(descricaoField.value.getValue());
        item.setCodGrupo(grupoModelagemField.value.getValue());
        item.setPreco(new BigDecimal(precoField.value.getValue()));
        item.setGenero(generoField.value.getValue());
        item.setMarca(marcaField.value.getValue());
    }

    private void editarCadastro(SdProformaModelagem sdProformaModelagem) {
        if (sdProformaModelagem != null) {
            navegation.inEdition.set(true);
            navegation.selectedItem.set(sdProformaModelagem);
            MessageBox.create(message -> {
                message.message("Modo edição ativado!");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            abrirCadastro(sdProformaModelagem);
        }

    }

    private void abrirCadastro(SdProformaModelagem sdProformaModelagem) {
        navegation.selectedItem.set(sdProformaModelagem);
        tabs.getSelectionModel().select(1);
        carregaDados(sdProformaModelagem);
    }

    private void excluirCadastro(SdProformaModelagem sdProformaModelagem) {
        if (sdProformaModelagem != null) {
            if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                message.message("Tem certeza que deseja exluir?");
                message.showAndWait();
            }).value.get())) {
                new FluentDao().delete(sdProformaModelagem);
                proformaBean.remove(sdProformaModelagem);
                MessageBox.create(message -> {
                    message.message("Cadastro excluído com sucesso!");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
                cancelarCadastro();
            }
        }
    }

    private void novoCadastro() {
        navegation.inEdition.set(true);
        navegation.selectedItem.set(new SdProformaModelagem());
    }
}
