package sysdeliz2.views.comercial.exportacao;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.input.KeyCode;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import org.apache.poi.ss.usermodel.CellCopyPolicy;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import sysdeliz2.controllers.views.comercial.exportacao.CriacaoProformaController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.sysdeliz.*;
import sysdeliz2.models.ti.*;
import sysdeliz2.models.view.VSdDadosProdutoImportacao;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.ReportUtils;
import sysdeliz2.utils.SortUtils;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.mails.SimpleMail;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import javax.swing.text.MaskFormatter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CriacaoProformaView extends CriacaoProformaController {

    //<editor-fold desc="Globals">
    private SdEmbarque embarqueSelecionado = null;
    private SdProforma proformaSelecionada = null;
    //</editor-fold>

    //<editor-fold desc="Booleans">
    private final BooleanProperty inEdition = new SimpleBooleanProperty(false);
    private final BooleanProperty isFinalizado = new SimpleBooleanProperty(false);
    //</editor-fold>

    //<editor-fold desc="Date Formats">
    private final DateTimeFormatter formatterDefault = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    private final DateTimeFormatter dateFormatHifen = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Variáveis DI">
    private BigDecimal syscomex = null;
    private BigDecimal cofins = null;
    private BigDecimal frete = null;
    private BigDecimal dolar = null;
    private BigDecimal pis = null;
    private String numeroDi = null;
    private String refSDi = null;
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Bean">
    private ListProperty<SdEmbarque> embarquesBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private ListProperty<SdItemEmbarque> itensEmbarqueBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private ListProperty<SdItemGradeEmbarque> itensGradeEmbarqueBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Filter">
    private final FormFieldMultipleFind<SdEmbarque> codEmbarqueFilter = FormFieldMultipleFind.create(SdEmbarque.class, field -> {
        field.title("Cod Embarque");
    });
    private final FormFieldComboBox<String> statusEmbarqueFilter = FormFieldComboBox.create(String.class, field -> {
        field.title("Status");
        field.items(FXCollections.observableArrayList(Arrays.asList("", SdEmbarque.StatusEmbarque.CRIADO.name(), SdEmbarque.StatusEmbarque.EM_EDICAO.name(), SdEmbarque.StatusEmbarque.CONCLUIDO.name(), SdEmbarque.StatusEmbarque.FINALIZADO.name())));
    });
    private final FormFieldDate dataFimEmbarqueFilter = FormFieldDate.create(field -> {
        field.title("Data Fim");
        field.value.set(LocalDate.now());
    });
    private final FormFieldDate dataInicioEmbarqueFilter = FormFieldDate.create(field -> {
        field.title("Data Início");
        field.value.set(LocalDate.now().minusWeeks(1));
        field.value.addListener((observable, oldValue, newValue) -> {
            if (newValue.isAfter(dataFimEmbarqueFilter.value.getValue())) {
                dataFimEmbarqueFilter.value.setValue(newValue);
            }
        });
    });

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Field">
    private final FormFieldText codEmbarqueField = FormFieldText.create(field -> {
        field.title("Código Embarque");
        field.editable.set(false);
        field.width(150);
    });
    private final FormFieldText dataEmbarqueField = FormFieldText.create(field -> {
        field.title("Data Embarque");
        field.editable.bind(inEdition);
    });
    private final FormFieldText tipoEmbarqueField = FormFieldText.create(field -> {
        field.title("Tipo");
        field.editable.set(false);
        field.width(150);
    });
    private final FormFieldText valorEmbarqueField = FormFieldText.create(field -> {
        field.title("Valor Total");
        field.editable.set(false);
        field.width(190);
    });
    private final FormFieldText statusEmbarqueField = FormFieldText.create(field -> {
        field.title("Status");
        field.editable.set(false);
        field.width(150);
        field.value.addListener((observable, oldValue, newValue) -> {
            field.removeStyle("info");
            field.removeStyle("warning");
            field.removeStyle("success");

            if (newValue != null) {
                if (newValue.equals(SdEmbarque.StatusEmbarque.CRIADO.name())) {
                    field.addStyle("info");
                } else if (newValue.equals(SdEmbarque.StatusEmbarque.CONCLUIDO.name())) {
                    field.addStyle("warning");
                } else if (newValue.equals(SdEmbarque.StatusEmbarque.FINALIZADO.name())) {
                    field.addStyle("success");
                } else {
                    field.removeStyle("info");
                    field.removeStyle("warning");
                    field.removeStyle("success");
                }
            }
        });
    });

    private final FormFieldText qtdeItensField = FormFieldText.create(field -> {
        field.title("Total Itens");
        field.editable.set(false);
        field.width(150);
    });
    private final FormFieldText qtdeVolumesField = FormFieldText.create(field -> {
        field.title("Total Volumes");
        field.editable.bind(inEdition);
    });
    private final FormFieldText pesoLiquidoField = FormFieldText.create(field -> {
        field.title("Peso Líquido Total");
        field.editable.set(false);
        field.width(150);
    });
    private final FormFieldText pesoBrutoField = FormFieldText.create(field -> {
        field.title("Peso Bruto Total");
        field.editable.set(false);
        field.width(190);
    });
    private final FormFieldText cubagemField = FormFieldText.create(field -> {
        field.title("Cubagem Total");
        field.editable.set(false);
        field.width(193);
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Table">
    private final FormTableView<SdEmbarque> tblEbarques = FormTableView.create(SdEmbarque.class, table -> {
        table.title("Embarques");
        table.expanded();
        table.disable.bind(inEdition);
        table.selectionModelItem((observable, oldValue, newValue) -> {
            if (newValue != null && newValue != oldValue) {
                embarqueSelecionado = (SdEmbarque) newValue;
                carregaEmbarque();
            }
        });
        table.items.bind(embarquesBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(60);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdEmbarque, SdEmbarque>, ObservableValue<SdEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Data");
                    cln.width(110);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdEmbarque, SdEmbarque>, ObservableValue<SdEmbarque>>) param -> new ReadOnlyObjectWrapper(StringUtils.toDateFormat(param.getValue().getData())));
                }).build(), /*Data*/
                FormTableColumn.create(cln -> {
                    cln.title("Tipo");
                    cln.width(110);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdEmbarque, SdEmbarque>, ObservableValue<SdEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTipo()));
                }).build(), /*Tipo*/
                FormTableColumn.create(cln -> {
                    cln.title("Status");
                    cln.width(110);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdEmbarque, SdEmbarque>, ObservableValue<SdEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getStatus()));
                }).build(), /*Status*/
                FormTableColumn.create(cln -> {
                    cln.title("Qtde");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdEmbarque, SdEmbarque>, ObservableValue<SdEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                }).build()
        );
        table.factoryRow(param -> {
            return new TableRow<SdEmbarque>() {
                @Override
                protected void updateItem(SdEmbarque item, boolean empty) {
                    super.updateItem(item, empty);
                    clear();

                    if (item != null && !empty) {
                        if (item.getStatus().equals(SdEmbarque.StatusEmbarque.CRIADO.name())) {
                            getStyleClass().add("table-row-info");
                        } else if (item.getStatus().equals(SdEmbarque.StatusEmbarque.CONCLUIDO.name())) {
                            getStyleClass().add("table-row-warning");
                        } else if (item.getStatus().equals(SdEmbarque.StatusEmbarque.FINALIZADO.name())) {
                            getStyleClass().add("table-row-success");
                        }
                    }
                }

                private void clear() {
                    getStyleClass().removeAll("table-row-info", "table-row-success", "table-row-warning");
                }
            };
        });
    });

    private final FormTableView<SdItemEmbarque> tblItensEmbarque = FormTableView.create(SdItemEmbarque.class, table -> {
        table.title("Itens");
        table.expanded();
        table.selectionModelItem((observable, oldValue, newValue) -> {
            if (newValue != null && newValue != oldValue) {
                carregaTabelaGrade((SdItemEmbarque) newValue);
            }
        });
        table.items.bind(itensEmbarqueBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Documento");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemEmbarque, SdItemEmbarque>, ObservableValue<SdItemEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumero()));
                }).build(), /*OP/OF*/
                FormTableColumn.create(cln -> {
                    cln.title("Produto");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemEmbarque, SdItemEmbarque>, ObservableValue<SdItemEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getProduto().getCodigo()));
                }).build(), /*Produto*/
                FormTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(300);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemEmbarque, SdItemEmbarque>, ObservableValue<SdItemEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getProduto().getDescricao()));
                }).build(), /*Descrição*/
                FormTableColumn.create(cln -> {
                    cln.title("NCM");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemEmbarque, SdItemEmbarque>, ObservableValue<SdItemEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNcm()));
                }).build(), /*Descrição*/
                FormTableColumn.create(cln -> {
                    cln.title("UN");
                    cln.width(70);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemEmbarque, SdItemEmbarque>, ObservableValue<SdItemEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getUnidade()));
                }).build(), /*Valor*/
                FormTableColumn.create(cln -> {
                    cln.title("Qtde");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemEmbarque, SdItemEmbarque>, ObservableValue<SdItemEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                }).build(), /*Valor*/
                FormTableColumn.create(cln -> {
                    cln.title("Valor UN");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemEmbarque, SdItemEmbarque>, ObservableValue<SdItemEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPrecoUnidade()));
                }).build(), /*Valor*/
                FormTableColumn.create(cln -> {
                    cln.title("Peso Peça");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemEmbarque, SdItemEmbarque>, ObservableValue<SdItemEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdItemEmbarque, SdItemEmbarque>() {
                            @Override
                            protected void updateItem(SdItemEmbarque item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                getStyleClass().remove("table-row-danger");

                                if (item != null && !empty && item.getPesoUnidade() != null) {
                                    setText(item.getPesoUnidade() != null ? item.getPesoUnidade().toString() : "");
                                    BigDecimal pesoUnidade = item.getPesoUnidade();
                                    //Compara os valores pra saber se é menor que o minimo ou maior que o maximo
                                    if (pesoUnidade.compareTo(item.getProduto().getPesoMinimo()) < 0 || pesoUnidade.compareTo(item.getProduto().getPesoMaximo()) > 0) {
                                        getStyleClass().add("table-row-danger");
                                    } else {
                                        getStyleClass().remove("table-row-danger");
                                    }
                                }
                            }
                        };
                    });
                }).build(), /*Peso Peça*/
                FormTableColumn.create(cln -> {
                    cln.title("Peso Final");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemEmbarque, SdItemEmbarque>, ObservableValue<SdItemEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPesoTotal()));
                }).build(), /*Peso Peça*/
                FormTableColumn.create(cln -> {
                    cln.title("Peso OF");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemEmbarque, SdItemEmbarque>, ObservableValue<SdItemEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPesoOf()));
                }).build(), /*Valor*/
                FormTableColumn.create(cln -> {
                    cln.title("Valor da OF");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemEmbarque, SdItemEmbarque>, ObservableValue<SdItemEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPrecoTotal()));
                }).build() /*Valor*/

        );
    });

    private final FormTableView<SdItemGradeEmbarque> tblGradeItensEmbarque = FormTableView.create(SdItemGradeEmbarque.class, table -> {
        table.withoutHeader();
        table.items.bind(itensGradeEmbarqueBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Tamanho");
                    cln.width(70);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemGradeEmbarque, SdItemGradeEmbarque>, ObservableValue<SdItemGradeEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTamanho()));
                }).build(), /*Tamanho*/
                FormTableColumn.create(cln -> {
                    cln.title("Quantidade");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemGradeEmbarque, SdItemGradeEmbarque>, ObservableValue<SdItemGradeEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                }).build(), /*Tamanho*/
                FormTableColumn.create(cln -> {
                    cln.title("Volumes");
                    cln.width(60);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemGradeEmbarque, SdItemGradeEmbarque>, ObservableValue<SdItemGradeEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getVolumes()));
                }).build(), /*Volumes*/
                FormTableColumn.create(cln -> {
                    cln.title("Peso Líquido");
                    cln.width(110);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemGradeEmbarque, SdItemGradeEmbarque>, ObservableValue<SdItemGradeEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPesoLiquido()));
                }).build(), /*Peso Líquido*/
                FormTableColumn.create(cln -> {
                    cln.title("Peso Bruto");
                    cln.width(110);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemGradeEmbarque, SdItemGradeEmbarque>, ObservableValue<SdItemGradeEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPesoBruto()));
                }).build(), /*Peso Bruto*/
                FormTableColumn.create(cln -> {
                    cln.title("Peso Por Peça");
                    cln.width(110);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemGradeEmbarque, SdItemGradeEmbarque>, ObservableValue<SdItemGradeEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPesoUnidade()));
                }).build(), /*Peso Bruto*/
                FormTableColumn.create(cln -> {
                    cln.title("Cubagem");
                    cln.width(70);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemGradeEmbarque, SdItemGradeEmbarque>, ObservableValue<SdItemGradeEmbarque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCubagem()));
                }).build() /*Cubagem*/
        );
    });

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Button">
    private Button btnExcel = FormButton.create(btn -> {
        btn.title("Exportar");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EXCEL, ImageUtils.IconSize._24));
        btn.addStyle("success");
        btn.setOnAction(evt -> {
            if (embarqueSelecionado != null) {
                criarExcelFaturaImportacao();
            }
        });
    });

    private Button btnEnviarEmail = FormButton.create(btn -> {
        btn.title("Enviar Email");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._24));
        btn.addStyle("primary");
        btn.setOnAction(evt -> {
            new Fragment().show(fragment -> {
                AtomicReference<List<File>> files = new AtomicReference<>(new ArrayList<>());
                FormFieldText destinatariosField = FormFieldText.create(field -> {
                    field.width(300);
                    field.title("Destinatários");
                });

                FormFieldText copiaField = FormFieldText.create(field -> {
                    field.title("CC");
                });

                FormFieldText arquivosField = FormFieldText.create(field -> {
                    field.title("Arquivos");
                    field.editable.set(false);
                });

                Button btnAnexarArq = FormButton.create(button -> {
                    button.icon(ImageUtils.getIcon(ImageUtils.Icon.ANEXO, ImageUtils.IconSize._16));
                    button.addStyle("warning");
                    button.setOnAction(event -> {
                        if (files.get() != null) files.getAndSet(new ArrayList<>());
                        arquivosField.value.setValue("");
                        FileChooser fs = new FileChooser();
                        files.set(fs.showOpenMultipleDialog(new Stage()));
                        if (files.get() != null) {
                            files.get().forEach(it -> {
                                arquivosField.value.setValue(arquivosField.value.getValueSafe() + it.getName() + " ,");
                            });
                        }
                    });
                });

                fragment.title("Configurações Email");
                fragment.size(400.0, 200.0);
                fragment.box.getChildren().add(FormBox.create(principal -> {
                    principal.vertical();
                    principal.add(FormBox.create(linha1 -> {
                        linha1.horizontal();
                        linha1.alignment(Pos.BOTTOM_LEFT);
                        linha1.add(destinatariosField.build(), btnAnexarArq);
                    }));
                    principal.add(copiaField.build());
                    principal.add(arquivosField.build());
                }));

                fragment.buttonsBox.getChildren().add(FormButton.create(botao -> {
                    botao.title("Concluir");
                    botao.addStyle("success");
                    botao.icon(ImageUtils.getIconButton(ImageUtils.ButtonIcon.SELECT, ImageUtils.IconSize._24));
                    botao.setOnAction(event -> {
                        if (destinatariosField.value.isEmpty().get()) {
                            MessageBox.create(message -> {
                                message.message("Preencha o campo com os destinatários para prosseguir!");
                                message.type(MessageBox.TypeMessageBox.WARNING);
                                message.showAndWait();
                            });
                        } else {
                            List<String> emailsDestinatarios = Arrays.asList(destinatariosField.value.getValue().split(","));
                            List<String> emailsCopia = copiaField.value.isEmpty().get() ? new ArrayList<>() : Arrays.asList(copiaField.value.getValue().split(","));
                            enviarEmail(emailsCopia, emailsDestinatarios, files.get());
                            fragment.close();
                        }
                    });
                }));
            });
        });
    });

    private Button btnVizualizarNota = FormButton.create(btn -> {
        btn.title("Visualizar Dados NF");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.NOTA_FISCAL, ImageUtils.IconSize._24));
        btn.addStyle("warning");
        btn.disableProperty().bind(isFinalizado.not());
        btn.setOnAction(evt -> {
            mostrarNF();
        });
    });

    private Button btnFinalizar = FormButton.create(btn -> {
        btn.title("Finalizar");
        btn.addStyle("success");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._24));
        btn.disableProperty().bind(isFinalizado);
        btn.setOnAction(evt -> {
            if (embarqueSelecionado != null) {
                if (embarqueSelecionado.getStatus().equals(SdEmbarque.StatusEmbarque.CRIADO.name())) {
                    MessageBox.create(message -> {
                        message.message("Embarque não foi concluído ainda. \nUtilize a tela de Cadastro de Embarque para concluí-lo!");
                        message.type(MessageBox.TypeMessageBox.WARNING);
                        message.showAndWait();
                    });
                } else {
                    abrirJanelaDadosDI();
                    if (dolar != null && pis != null && syscomex != null && frete != null && cofins != null) {
                        finalizarEmbarque();
                    }
                }
            }
        });
    });

    private Button btnRetornarEmbarque = FormButton.create(btn -> {
        btn.title("Retornar");
        btn.addStyle("danger");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.RECOLHER, ImageUtils.IconSize._24));
        btn.disableProperty().bind(isFinalizado);
        btn.setOnAction(evt -> {
            if (embarqueSelecionado != null) {
                if (embarqueSelecionado.getStatus().equals(SdEmbarque.StatusEmbarque.CRIADO.name())) {
                    MessageBox.create(message -> {
                        message.message("Embarque não foi concluído ainda. \nUtilize a tela de Cadastro de Embarque para concluí-lo!");
                        message.type(MessageBox.TypeMessageBox.WARNING);
                        message.showAndWait();
                    });
                } else if (embarqueSelecionado.getStatus().equals(SdEmbarque.StatusEmbarque.FINALIZADO.name())) {
                    MessageBox.create(message -> {
                        message.message("Embarque já foi finalizado!");
                        message.type(MessageBox.TypeMessageBox.WARNING);
                        message.showAndWait();
                    });
                } else retornarEmbarque();
            }
        });
    });

    private Button btnGerarNF = FormButton.create(btn -> {
        btn.title("Gerar Nota Fiscal");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.NOTA_FISCAL, ImageUtils.IconSize._24));
        btn.addStyle("secondary");
        btn.disableProperty().bind(isFinalizado.not());
        btn.setOnAction(evt -> {

            if (proformaSelecionada != null && proformaSelecionada.getNumeroNota() == null) {
                if (embarqueSelecionado != null) {
                    gerarNota();
                    MessageBox.create(message -> {
                        message.message("Nota Fiscal criada com sucesso!");
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.show();
                    });
                }
            } else {
                MessageBox.create(message -> {
                    message.message("Nota Fiscal já criada!");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showAndWait();
                });
            }

        });
    });

    // </editor-fold>

    public CriacaoProformaView() {
        super("Criação da Proforma", ImageUtils.getImage(ImageUtils.Icon.CADASTROS));
        init();
    }

    private void init() {
        box.getChildren().add(FormBox.create(principal -> {
            principal.horizontal();
            principal.add(FormBox.create(col1 -> {
                col1.vertical();
                col1.maxWidthSize(600);
                col1.add(FormTitledPane.create(filter -> {
                    filter.filter();
                    filter.width(500);
                    filter.add(FormBox.create(boxFilter -> {
                        boxFilter.vertical();
                        boxFilter.maxWidthSize(500);
                        boxFilter.add(FormBox.create(linha1 -> {
                            linha1.horizontal();
                            linha1.add(codEmbarqueFilter.build(), statusEmbarqueFilter.build());
                        }));
                        boxFilter.add(FormBox.create(linha2 -> {
                            linha2.horizontal();
                            linha2.add(dataInicioEmbarqueFilter.build(), dataFimEmbarqueFilter.build());
                        }));
                    }));
                    filter.find.setOnAction(evt -> {
                        new RunAsyncWithOverlay(this).exec(task -> {
                            carregarEmbarquesImportacao(codEmbarqueFilter.objectValues.getValue() == null ? null : codEmbarqueFilter.objectValues.getValue().stream().map(SdEmbarque::getCodigo).toArray(),
                                    statusEmbarqueFilter.value.getValue(),
                                    dataInicioEmbarqueFilter.value.getValue(),
                                    dataFimEmbarqueFilter.value.getValue()
                            );
                            return ReturnAsync.OK.value;
                        }).addTaskEndNotification(taskReturn -> {
                            if (taskReturn.equals(ReturnAsync.OK.value)) {
                                embarquesBean.set(FXCollections.observableArrayList(embarques));
                            }
                        });

                    });
                    filter.clean.setOnAction(evt -> {
                        codEmbarqueFilter.clear();
                        statusEmbarqueFilter.select(0);
                        dataInicioEmbarqueFilter.value.setValue(LocalDate.now().minusWeeks(1));
                        dataFimEmbarqueFilter.value.setValue(LocalDate.now());
                    });
                }));
                col1.add(FormBox.create(boxTable -> {
                    boxTable.maxWidthSize(500);
                    boxTable.vertical();
                    boxTable.expanded();
                    boxTable.add(tblEbarques.build());
                }));
            }));
            principal.add(FormBox.create(col2 -> {
                col2.vertical();
                col2.setPrefWidth(1400);
                col2.add(FormBox.create(boxTable -> {
                    boxTable.vertical();
                    boxTable.expanded();
                    boxTable.size(1400, 700);
                    boxTable.add(tblItensEmbarque.build());
                }));
                col2.add(FormBox.create(boxFooter -> {
                    boxFooter.horizontal();
                    boxFooter.size(1500, 300);
                    boxFooter.add(FormBox.create(boxGrade -> {
                        boxGrade.vertical();
                        boxGrade.size(700, 600);
                        boxGrade.add(tblGradeItensEmbarque.build());
                    }));
                    boxFooter.add(FormBox.create(boxTotaisEBotoes -> {
                        boxTotaisEBotoes.vertical();
                        boxTotaisEBotoes.add(FormBox.create(boxTotais -> {
                            boxTotais.vertical();
                            boxTotais.title("Embarque");
                            boxTotais.size(750, 100);
                            boxTotais.add(FormBox.create(linha1 -> {
                                linha1.horizontal();
                                linha1.add(codEmbarqueField.build(), dataEmbarqueField.build(), tipoEmbarqueField.build(), statusEmbarqueField.build(), valorEmbarqueField.build());
                            }));
                        }));
                        boxTotaisEBotoes.add(FormBox.create(boxTotaisGrade -> {
                            boxTotaisGrade.vertical();
                            boxTotaisGrade.title("OF");
                            boxTotaisGrade.size(750, 150);
                            boxTotaisGrade.add(FormBox.create(linha1 -> {
                                linha1.horizontal();
                                linha1.add(qtdeItensField.build(), qtdeVolumesField.build());
                            }));
                            boxTotaisGrade.add(FormBox.create(linha2 -> {
                                linha2.horizontal();
                                linha2.add(pesoLiquidoField.build(), pesoBrutoField.build(), cubagemField.build());
                            }));
                        }));
                        boxTotaisEBotoes.add(FormBox.create(boxBotoes -> {
                            boxBotoes.horizontal();
                            boxBotoes.add(btnExcel, btnEnviarEmail, btnFinalizar, btnRetornarEmbarque, btnVizualizarNota, btnGerarNF);
                        }));
                    }));
                }));
            }));

        }));
    }

    //<editor-fold desc="Funções do Embarque">
    private void carregaEmbarque() {

        new RunAsyncWithOverlay(this).exec(task -> {
            isFinalizado.set(embarqueSelecionado.getStatus().equals(SdEmbarque.StatusEmbarque.FINALIZADO.name()));
            if (isFinalizado.get())
                proformaSelecionada = new FluentDao().selectFrom(SdProforma.class).where(it -> it.equal("codigo", embarqueSelecionado.getCodProforma())).singleResult();

            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                itensEmbarqueBean.set(FXCollections.observableArrayList(embarqueSelecionado.getListItens()));
                carregaCampos();
                tblGradeItensEmbarque.items.clear();
                tblItensEmbarque.refresh();
            }
        });

    }

    private void retornarEmbarque() {
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Tem certeza que deseja retornar o embarque ao estado de Criado?");
            message.showAndWait();
        }).value.get())) {
            embarqueSelecionado.setStatus(SdEmbarque.StatusEmbarque.CRIADO.name());
            embarqueSelecionado = new FluentDao().merge(embarqueSelecionado);
            tblEbarques.refresh();
        }
    }

    private void finalizarEmbarque() {
        embarqueSelecionado.setStatus(SdEmbarque.StatusEmbarque.FINALIZADO.name());
        criarProforma();
        tblEbarques.refresh();
        carregaEmbarque();
    }
    //</editor-fold>

    //<editor-fold desc="Email">
    private void enviarEmail(List<String> destinatarios, List<String> copias, List<File> files) {
        SimpleMail mail = SimpleMail.INSTANCE;
        mail.setSender(Globals.getUsuarioLogado().getEmail());
        destinatarios.forEach(mail::addDestinatario);
        files.forEach(it -> mail.addAttachment(it.getAbsolutePath()));
        copias.forEach(mail::addCC);
        mail.comAssunto("Fatura de " + proformaSelecionada.getTipo() + " - Nº " + proformaSelecionada.getCodigo());
        mail.comCorpoHtml(this::criaCorpoHtmlEmailImportacao);
        mail.send();
        MessageBox.create(message -> {
            message.message("Email enviado com sucesso!");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.show();
        });
    }

    private String criaCorpoHtmlEmailImportacao() {
        StringBuilder sb = new StringBuilder();
        List<List<SdItemEmbarque>> listItens = divideProdutosEntreArquivos();
        sb.append("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
                "    <title>Email SysDeliz</title>\n" +
                "    <style>\n" +
                "        table {\n" +
                "          border-collapse: collapse;\n" +
                "          width: 30%;\n" +
                "          margin: 50px 0 50px 0;\n" +
                "        }\n" +
                "        \n" +
                "        td, th {\n" +
                "          border: 1px solid #dddddd;\n" +
                "          text-align: left;\n" +
                "          padding: 5px;\n" +
                "        }\n" +
                "        \n" +
                "        tr:nth-child(even) {\n" +
                "          background-color: #dddddd;\n" +
                "        }\n" +
                "        </style>\n" +
                "</head>");
        sb.append("<body>\n" +
                "    <div>\n" +
                "        <h2>Proforma de Nº " + proformaSelecionada.getCodigo() + " </h2>\n" +
                "        \n" +
                "        <span style=\"margin-bottom: 25px;\">Olá, segue em anexo as faturas referentes a proforma de código " + proformaSelecionada.getCodigo() + ".</span>");
        sb.append("<table>\n" +
                "            <tr>\n" +
                "                <th>Código</th>\n" +
                "                <th>Qtde</th>\n" +
                "                <th>Peso Líquido</th>\n" +
                "                <th>Peso Bruto</th>\n" +
                "                <th>Volumes</th>\n" +
                "            </tr>");
        int cont = 1;
        for (List<SdItemEmbarque> listIten : listItens) {
            sb.append("<tr>" +
                    "<td>" + cont++ + "</td>" +
                    "<td>" + getQtdeByListItens(listIten) + "</td>" +
                    "<td>" + getPesoLiquidoByListItens(listIten) + "kg</td>" +
                    "<td>" + getPesoBrutoByListItens(listIten) + "kg</td>" +
                    "<td>" + getVolumesByListItens(listIten) + "</td>" +
                    "</tr>");
        }
        sb.append("<tr>" +
                "<td><b> TOTAL </b></td>" +
                "<td><b>" + proformaSelecionada.getQtde() + "<b></td>" +
                "<td><b>" + proformaSelecionada.getPesoLiquido() + "kg<b></td>" +
                "<td><b>" + proformaSelecionada.getPesoBruto() + "kg<b></td>" +
                "<td><b>" + proformaSelecionada.getVolumes() + "<b></td>" +
                "</tr>");
        sb.append("</table>\n" +
                "\n" +
                "\n" +
                "        Atenciosamente,<br>\n" +
                "        <h4 style=\"margin-bottom: 0%; padding-bottom: 0%;\">Fabiana Soares <br> \n" +
                "        Compras</h4>\n" +
                "\n" +
                "        <h4>Deliz Indústria da Moda</h4>\n" +
                "        <b><i>Telefone: </i> (48) 3641 1900</b> <br>\n" +
                "        <i>Email: fabianasoares@deliz.com.br <br>\n" +
                "            Site: www.deliz.com.br </i>\n" +
                "\n" +
                "\n" +
                "    </div>\n" +
                "</body>\n" +
                "\n" +
                "</html>");
        return sb.toString();
    }

    private String criaCorpoHtmlEmailExportacao() {
        StringBuilder sb = new StringBuilder();
        List<List<SdItemEmbarque>> listItens = divideProdutosEntreArquivos();
        sb.append("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
                "    <title>Email SysDeliz</title>\n" +
                "    <style>\n" +
                "        table {\n" +
                "          border-collapse: collapse;\n" +
                "          width: 30%;\n" +
                "          margin: 50px 0 50px 0;\n" +
                "        }\n" +
                "        \n" +
                "        td, th {\n" +
                "          border: 1px solid #dddddd;\n" +
                "          text-align: left;\n" +
                "          padding: 5px;\n" +
                "        }\n" +
                "        \n" +
                "        tr:nth-child(even) {\n" +
                "          background-color: #dddddd;\n" +
                "        }\n" +
                "        </style>\n" +
                "</head>");
        sb.append("<body>\n" +
                "    <div>\n" +
                "        <h2>Proforma de Nº " + proformaSelecionada.getCodigo() + " </h2>\n" +
                "        \n" +
                "        <span style=\"margin-bottom: 25px;\">Olá, segue em anexo as faturas referentes a proforma de código " + proformaSelecionada.getCodigo() + ".</span>");
        sb.append("<table>\n" +
                "            <tr>\n" +
                "                <th>Código</th>\n" +
                "                <th>Qtde</th>\n" +
                "                <th>Peso Líquido</th>\n" +
                "                <th>Peso Bruto</th>\n" +
                "                <th>Volumes</th>\n" +
                "            </tr>");
        int cont = 1;
        for (List<SdItemEmbarque> listIten : listItens) {
            sb.append("<tr>" +
                    "<td>" + cont++ + "</td>" +
                    "<td>" + getQtdeByListItens(listIten) + "</td>" +
                    "<td>" + getPesoLiquidoByListItens(listIten) + "kg</td>" +
                    "<td>" + getPesoBrutoByListItens(listIten) + "kg</td>" +
                    "<td>" + getVolumesByListItens(listIten) + "</td>" +
                    "</tr>");
        }
        sb.append("<tr>" +
                "<td><b> TOTAL </b></td>" +
                "<td><b>" + proformaSelecionada.getQtde() + "<b></td>" +
                "<td><b>" + proformaSelecionada.getPesoLiquido() + "kg<b></td>" +
                "<td><b>" + proformaSelecionada.getPesoBruto() + "kg<b></td>" +
                "<td><b>" + proformaSelecionada.getVolumes() + "<b></td>" +
                "</tr>");
        sb.append("</table>\n" +
                "\n" +
                "\n" +
                "        Atenciosamente,<br>\n" +
                "        <h4 style=\"margin-bottom: 0%; padding-bottom: 0%;\">Fabiana Soares <br> \n" +
                "        Compras</h4>\n" +
                "\n" +
                "        <h4>Deliz Indústria da Moda</h4>\n" +
                "        <b><i>Telefone: </i> (48) 3641 1900</b> <br>\n" +
                "        <i>Email: fabianasoares@deliz.com.br <br>\n" +
                "            Site: www.deliz.com.br </i>\n" +
                "\n" +
                "\n" +
                "    </div>\n" +
                "</body>\n" +
                "\n" +
                "</html>");
        return sb.toString();
    }
    //</editor-fold>

    //<editor-fold desc="Funções de Carregamento">
    private void carregaCampos() {
        if (embarqueSelecionado != null) {
            codEmbarqueField.value.setValue(embarqueSelecionado.getCodigo() == null ? "" : embarqueSelecionado.getCodigo().toString());
            dataEmbarqueField.value.setValue(embarqueSelecionado.getData().format(formatterDefault));
            tipoEmbarqueField.value.setValue(embarqueSelecionado.getTipo() == null ? "" : embarqueSelecionado.getTipo());
            statusEmbarqueField.value.setValue(embarqueSelecionado.getStatus() == null ? "" : embarqueSelecionado.getStatus());
            valorEmbarqueField.value.setValue(embarqueSelecionado.getPreco() == null ? "" : embarqueSelecionado.getPreco().toString());
        }
    }

    private void carregaTabelaGrade(SdItemEmbarque itemEmbarque) {
        itensGradeEmbarqueBean.set(FXCollections.observableArrayList(itemEmbarque.getListGrade()));
        if (itensGradeEmbarqueBean != null) {
            qtdeItensField.value.setValue(itensGradeEmbarqueBean.stream().reduce(0, (partialResult, it) -> it.getQtde() + partialResult, Integer::sum).toString());
            qtdeVolumesField.value.setValue(itensGradeEmbarqueBean.stream().reduce(0, (partialResult, it) -> it.getVolumes() + partialResult, Integer::sum).toString());
            pesoLiquidoField.value.setValue(itensGradeEmbarqueBean.stream().filter(item -> item.getPesoLiquido() != null).reduce(BigDecimal.ZERO, (partialResult, it) -> it.getPesoLiquido().add(partialResult), BigDecimal::add).toString());
            pesoBrutoField.value.setValue(itensGradeEmbarqueBean.stream().filter(item -> item.getPesoBruto() != null).reduce(BigDecimal.ZERO, (partialResult, it) -> it.getPesoBruto().add(partialResult), BigDecimal::add).toString());
            cubagemField.value.setValue(itensGradeEmbarqueBean.stream().filter(item -> item.getCubagem() != null).reduce(BigDecimal.ZERO, (partialResult, it) -> it.getCubagem().add(partialResult), BigDecimal::add).toString());
        }
        tblGradeItensEmbarque.refresh();
    }
    //</editor-fold>

    //<editor-fold desc="Criação do Excel Importação">

    private void criarExcelFaturaImportacao() {
        File fileDest = new DirectoryChooser().showDialog(Globals.getMainStage());
        StringBuilder faturaString = new StringBuilder();
        if (fileDest != null) {
            try {
                String path = fileDest.getAbsolutePath();
                XSSFWorkbook workbook;
                int nFatura = janelaNumeroFatura();
                if (nFatura != 0) {
                    criarDeclaracaoJurada(path, nFatura);
                    List<List<SdItemEmbarque>> listsParaImpressao = new ArrayList<>(divideProdutosEntreArquivos());

                    for (List<SdItemEmbarque> listaEmbarque : listsParaImpressao) {

                        faturaString.append(nFatura).append("/");

                        FileInputStream file = new FileInputStream("C:\\SysDelizLocal\\local_files\\ModeloBaseFaturaImportacao.xlsx");
                        workbook = new XSSFWorkbook(file);
                        XSSFSheet sheetItens = workbook.getSheetAt(0);
                        workbook.setSheetName(0, "FATURA " + nFatura);
                        preparaSheetItens(sheetItens);

                        List<List<SdItemEmbarque>> itensPorSheet = new ArrayList<>(getListItensPorSheet(listaEmbarque));
                        int qtdeSheetsExtras = listaEmbarque.size() % 6 == 0 ? listaEmbarque.size() / 6 : (listaEmbarque.size() / 6) + 1;

                        int contador = 0;
                        int contadorItem = 0;

                        for (List<SdItemEmbarque> listItensSheet : itensPorSheet) {
                            Integer qtdItensTotal = listItensSheet.stream().reduce(0, (partialResult, it) -> partialResult + it.getListGrade().size(), Integer::sum);
                            if (contador < qtdeSheetsExtras - 1) {
                                workbook.cloneSheet(1 + contador);
                                workbook.setSheetName(2 + contador, "P.L." + (contador + 2));
                            }
                            preparaSheetGrade(workbook, qtdItensTotal, listItensSheet.size(), 1 + contador);

                            XSSFSheet sheetGrade = workbook.getSheetAt(contador + 1);

                            int contadorGrade = 0;
                            for (SdItemEmbarque itemEmbarque : listItensSheet) {

                                if (contadorItem != listaEmbarque.size() - 1) {
                                    sheetItens.copyRows(18, 18, 19 + contadorItem, new CellCopyPolicy());
                                }
                                String nomeCurto = criaStringNomeCurto(itemEmbarque);
                                criaLinhasItens(sheetItens, contadorItem, itemEmbarque, nomeCurto);
                                List<SdItemGradeEmbarque> sortList = new ArrayList<>(itemEmbarque.getListGrade());
                                sortListaTamanhosGrade(sortList);
                                for (SdItemGradeEmbarque itemGrade : sortList) {

                                    criaLinhasGrade(sheetGrade, contadorGrade, itemEmbarque, nomeCurto, itemGrade, itemEmbarque.getListGrade().size() == 1);
                                    contadorGrade++;
                                }
                                criaMergedRegions(sheetGrade, contadorGrade, itemEmbarque);
                                criaLinhaTotaisGrade(sheetGrade, qtdItensTotal, contadorGrade, itemEmbarque, listItensSheet.size());

                                if (listItensSheet.indexOf(itemEmbarque) != listItensSheet.size() - 1)
                                    sheetGrade.copyRows(25, 25, 26 + contadorGrade, new CellCopyPolicy());
                                else {
                                    sheetGrade.removeRow(sheetGrade.getRow(qtdItensTotal + 25 + listItensSheet.size()));
                                }
                                contadorGrade++;
                                contadorItem++;
                            }
                            calculaValoresGradeFixos(sheetGrade, qtdItensTotal, listItensSheet, listItensSheet.size(), nFatura);
                            contador++;
                        }

                        sheetItens.copyRows(46, sheetItens.getLastRowNum(), 19 + listaEmbarque.size() - 1, new CellCopyPolicy()); // Traz o rodapé pra cima de volta

                        calculaValoresFixos(sheetItens, formatterDefault, listaEmbarque.size(), listaEmbarque, nFatura);

                        for (int i = 0; i < 40; i++) {
                            if (sheetItens.getRow(45 + listaEmbarque.size() + i) != null) {
                                sheetItens.removeRow(sheetItens.getRow(45 + listaEmbarque.size() + i));
                            }
                        }

                        file.close();

                        FileOutputStream outFile = new FileOutputStream(path + "\\Fatura " + nFatura + ".xlsx");
                        workbook.write(outFile);
                        outFile.close();
                        nFatura++;
                    }
                    embarqueSelecionado.setFaturas(faturaString.toString());
                    embarqueSelecionado = new FluentDao().merge(embarqueSelecionado);
                }

            } catch (IOException e) {
                e.printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(e);
                    message.showAndWait();
                });
            }
            SysLogger.addSysDelizLog("Criação de Proforma", TipoAcao.CADASTRAR, String.valueOf(embarqueSelecionado.getCodigo()), "Faturas " + faturaString + " criadas");
            MessageBox.create(message -> {
                message.message("Excel exportado com sucesso!");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.showAndWait();
            });
        }
    }

    private void sortListaTamanhosGrade(List<SdItemGradeEmbarque> listGrade) {
        if (listGrade.get(0).getTamanho().matches("[0-9]*")) {
            listGrade.sort(Comparator.comparingInt(s -> Integer.parseInt(s.getTamanho())));
        } else {
            listGrade.sort((o1, o2) -> {
                int result = 0;

                if (o1.getTamanho().equals("PP")) {
                    if (!o2.getTamanho().equals("PP")) result = -1;
                }

                if (o1.getTamanho().equals("P")) {
                    if ((o2.getTamanho().equals("PP"))) result = 1;
                    else if (o2.getTamanho().equals("P")) result = 0;
                    else result = -1;
                }

                if (o1.getTamanho().equals("M")) {
                    if (o2.getTamanho().equals("M")) result = 0;
                    else if ((o2.getTamanho().equals("PP") || (o2.getTamanho().equals("P")))) result = 1;
                    else result = -1;
                }

                if (o1.getTamanho().equals("G")) {
                    if (o2.getTamanho().equals("G")) result = 0;
                    else if ((o2.getTamanho().equals("PP") || (o2.getTamanho().equals("P") || (o2.getTamanho().equals("M")))))
                        result = 1;
                    else result = -1;
                }

                if (o1.getTamanho().equals("GG")) {
                    if (o2.getTamanho().equals("GG")) result = 0;
                    else if (o2.getTamanho().equals("XGG")) result = -1;
                    else result = 1;
                }

                if (o1.getTamanho().equals("XGG")) {
                    if (!o2.getTamanho().equals("XGG")) result = -1;
                }
                return result;
            });
        }
    }

    private int janelaNumeroFatura() {
        AtomicInteger fatura = new AtomicInteger(0);
        new Fragment().show(fragment -> {
            FormFieldText faturaField = FormFieldText.create(field -> {
                field.withoutTitle();
                field.mask(FormFieldText.Mask.INTEGER);
                field.label("Fatura: ");
            });

            fragment.title.setText("Número da Fatura");
            fragment.size(400.0, 110.0);

            fragment.box.getChildren().add(FormBox.create(principal -> {
                principal.vertical();
                principal.add(faturaField.build());
            }));
            fragment.buttonsBox.getChildren().add(FormButton.create(botao -> {
                botao.title("Concluir");
                botao.addStyle("success");
                fragment.setOnKeyReleased(evt -> {
                    if (evt.getCode() == KeyCode.ENTER) {
                        botao.fire();
                    }
                });
                botao.icon(ImageUtils.getIconButton(ImageUtils.ButtonIcon.SELECT, ImageUtils.IconSize._24));
                botao.setOnAction(event -> {
                    if (faturaField.value.isNotEmpty().get()) {
                        fatura.set(Integer.parseInt(faturaField.value.get()));
                        fragment.close();
                    }
                });
            }));
        });
        return fatura.get();
    }

    private void criarDeclaracaoJurada(String path, int nFatura) {

        Map<String, Map<String, List<SdItemEmbarque>>> collect = embarqueSelecionado.getListItens().stream().collect(Collectors.groupingBy(w -> w.getProduto().getGenero(), Collectors.groupingBy(b -> b.getProduto().getDescricion())));
        collect.forEach((genero, map) -> {
            map.forEach((descricion, listItens) -> {
                try {
                    XSSFWorkbook workbook;
                    FileInputStream file = new FileInputStream("C:\\SysDelizLocal\\local_files\\ModeloBaseDeclaracaoJurada.xlsx");
                    workbook = new XSSFWorkbook(file);
                    XSSFSheet sheet = workbook.getSheetAt(0);
                    workbook.setSheetName(0, descricion + " " + genero.toLowerCase());
                    sheet.getRow(8).getCell(0).setCellValue("según factura de exportación nro. 001-001-" + StringUtils.lpad(nFatura, 7, "0") + ".");
                    List<BigDecimal> valores = listItens.stream().map(SdItemEmbarque::getPrecoUnidade).distinct().collect(Collectors.toList());
                    AtomicInteger contador = new AtomicInteger(0);
                    double qtdeTotal = 0;
                    for (BigDecimal valor : valores) {
                        List<SdItemEmbarque> itens = listItens.stream().filter(eb -> eb.getPrecoUnidade().equals(valor)).collect(Collectors.toList());
                        double qtde = itens.stream().mapToDouble(eb -> eb.getQtde().doubleValue()).sum();
                        SdItemGradeEmbarque feminino = itens.get(0).getListGrade().stream().filter(it -> it.getTamanho().equals("34") && itens.get(0).getProduto().getGenero().equals("FEMENINO")).findFirst().orElse(null);
                        sheet.getRow(contador.get() + 15).getCell(0).setCellValue(itens.get(0).getNcm());
                        sheet.getRow(contador.get() + 15).getCell(1).setCellValue(itens.get(0).getProduto().getDescricion() + " " + itens.get(0).getProduto().getGenero().toLowerCase() + (feminino != null ? " infanto-juvenil" : " adulto"));
                        sheet.getRow(contador.get() + 15).getCell(2).setCellValue(qtde);
                        sheet.getRow(contador.get() + 15).getCell(3).setCellValue(itens.get(0).getUnidade());
                        sheet.getRow(contador.get() + 15).getCell(4).setCellValue(itens.get(0).getPrecoUnidade().doubleValue());
                        sheet.getRow(contador.get() + 15).getCell(5).setCellValue(itens.get(0).getPrecoUnidade().doubleValue() * qtde);
                        contador.getAndIncrement();
                        qtdeTotal += qtde;
                    }

                    sheet.getRow(7).getCell(0).setCellValue("Producto: " + qtdeTotal + " unidades de prendas de vestir a ser exportado a Brazil son el fiel reflejo de la realidad,");

                    file.close();
                    XSSFFormulaEvaluator.evaluateAllFormulaCells(workbook);
                    FileOutputStream outFile = new FileOutputStream(path + "\\Declaracao Jurada " + descricion + " " + genero.toLowerCase() + ".xlsx");
                    workbook.write(outFile);
                    outFile.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        });

    }

    private List<List<SdItemEmbarque>> divideProdutosEntreArquivos() {
        List<List<SdItemEmbarque>> listsParaImpressao = new ArrayList<>();
        listsParaImpressao.addAll(adicionaItensParaImpressao("MASCULINO"));
        listsParaImpressao.addAll(adicionaItensParaImpressao("FEMENINO"));
        return listsParaImpressao;
    }

    private List<List<SdItemEmbarque>> adicionaItensParaImpressao(String genero) {
        int contador = 0;
        List<List<SdItemEmbarque>> listsParaImpressao = new ArrayList<>();

        List<SdItemEmbarque> listaItens = embarqueSelecionado.getListItens()
                .stream()
                .filter(it -> it.getProduto().getGenero().toUpperCase().equals(genero))
                .collect(Collectors.toList());

        listaItens = listaItens
                .stream()
                .sorted(Comparator.comparing((SdItemEmbarque obj) -> obj.getProduto().getGrupoModelagem()).thenComparing(obj -> obj.getProduto().getCodigo()))
                .collect(Collectors.toList());

        SdItemEmbarque itemToRemove = null;

        while (contador < listaItens.size()) {
            List<SdItemEmbarque> splitList = listaItens.subList(contador, Math.min(contador + 19, listaItens.size()));

            if (itemToRemove != null && splitList.contains(itemToRemove)) {
                SdItemEmbarque finalItemToRemove = itemToRemove;
                splitList = listaItens.subList(contador, Math.min(contador + 19, listaItens.size())).stream().filter(it -> !it.equals(finalItemToRemove)).collect(Collectors.toList());
            }

            if (splitList.size() > 0) {
                SdItemEmbarque restoTamanhos = listaItens.get( Math.min(contador + 19, listaItens.size() - 1));
                SdItemEmbarque tamanhoUnico = splitList.get(splitList.size() - 1);

                if (tamanhoUnico.getListGrade().size() == 1 && tamanhoUnico.getListGrade().stream().map(it -> it.getTamanho()).collect(Collectors.toList()).contains("34") && (restoTamanhos.getProduto().equals(tamanhoUnico.getProduto()))) {
                    splitList.add(restoTamanhos);
                    itemToRemove = restoTamanhos;
                }
                listsParaImpressao.add(splitList);
            }
            contador += 19;
        }
        return listsParaImpressao;
    }

    private List<List<SdItemEmbarque>> getListItensPorSheet(List<SdItemEmbarque> listaEmbarque) {
        List<List<SdItemEmbarque>> itensPorSheet = new ArrayList<>();
        int limiteItens = 6;
        int contador = 0;

        while (contador < listaEmbarque.size()) {
            List<SdItemEmbarque> sList = listaEmbarque.subList(contador, Math.min(contador + limiteItens, listaEmbarque.size()));
            itensPorSheet.add(sList);
            contador += limiteItens;
        }
        return itensPorSheet;
    }

    private void calculaValoresGradeFixos(XSSFSheet sheetGrade, Integer qtdItensTotal, List<SdItemEmbarque> listItens, int size, int nFatura) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        sheetGrade.getRow(12).getCell(3).setCellValue(LocalDate.now().format(formatter));
        sheetGrade.getRow(13).getCell(4).setCellValue("Nº 003-001-" + StringUtils.lpad(nFatura, 7, "0"));
        sheetGrade.getRow(13).getCell(8).setCellValue(getQtdeByListItens(listItens).intValue());

        sheetGrade.getRow(25 + qtdItensTotal + size + 4).getCell(4).setCellValue(getQtdeByListItens(listItens).intValue());
        sheetGrade.getRow(25 + qtdItensTotal + size + 5).getCell(4).setCellValue(getVolumesByListItens(listItens));
        sheetGrade.getRow(25 + qtdItensTotal + size + 6).getCell(4).setCellValue(getPesoLiquidoByListItens(listItens).doubleValue());
        sheetGrade.getRow(25 + qtdItensTotal + size + 7).getCell(4).setCellValue(getPesoBrutoByListItens(listItens).doubleValue());
        sheetGrade.getRow(25 + qtdItensTotal + size + 8).getCell(4).setCellValue(listItens.stream().reduce(BigDecimal.ZERO, (partialResult, it) -> partialResult.add(it.getListGrade().stream().reduce(BigDecimal.ZERO, (p, eb) -> p.add(eb.getCubagem()), BigDecimal::add)), BigDecimal::add).doubleValue());
    }

    private void preparaSheetGrade(XSSFWorkbook workbook, Integer qtdItensTotal, int size, int nSheet) {
        XSSFSheet sheetGrade = workbook.getSheetAt(nSheet);
        for (int i = 9; i >= 0; i--) {
            sheetGrade.copyRows(26 + i, 26 + i, qtdItensTotal + 25 + size + i, new CellCopyPolicy());
        }
        deleteMergedRegions(sheetGrade, 25, 38);

    }

    private void preparaSheetItens(XSSFSheet sheetItens) {
        sheetItens.shiftRows(19, 47, 27);
//        deleteMergedRegions(sheetItens, 20, 45);
    }

    private void criaLinhaTotaisGrade(XSSFSheet sheetGrade, Integer qtdItensTotal, int contadorGrade, SdItemEmbarque itemEmbarque, int size) {
        sheetGrade.copyRows(qtdItensTotal + 25 + size, qtdItensTotal + 25 + size, contadorGrade + 25, new CellCopyPolicy());
        sheetGrade.getRow(contadorGrade + 25).getCell(5).setCellValue(itemEmbarque.getQtde().intValue());
        sheetGrade.getRow(contadorGrade + 25).getCell(6).setCellValue(itemEmbarque.getListGrade().stream().reduce(0, (partialResult, it) -> partialResult + it.getVolumes(), Integer::sum));
        sheetGrade.getRow(contadorGrade + 25).getCell(7).setCellValue(itemEmbarque.getListGrade().stream().reduce(BigDecimal.ZERO, (partialResult, it) -> it.getPesoLiquido().add(partialResult), BigDecimal::add).doubleValue());
        sheetGrade.getRow(contadorGrade + 25).getCell(8).setCellValue(itemEmbarque.getListGrade().stream().reduce(BigDecimal.ZERO, (partialResult, it) -> it.getPesoBruto().add(partialResult), BigDecimal::add).doubleValue());
        sheetGrade.getRow(contadorGrade + 25).getCell(9).setCellValue(itemEmbarque.getListGrade().stream().reduce(BigDecimal.ZERO, (partialResult, it) -> it.getCubagem().add(partialResult), BigDecimal::add).doubleValue());
    }

    private void criaMergedRegions(XSSFSheet sheetGrade, int contadorGrade, SdItemEmbarque itemEmbarque) {
        if (itemEmbarque.getListGrade().size() > 1)
            sheetGrade.addMergedRegion(new CellRangeAddress(contadorGrade + 25 - itemEmbarque.getListGrade().size(), contadorGrade + 24, 0, 0));
        if (itemEmbarque.getListGrade().size() > 1)
            sheetGrade.addMergedRegion(new CellRangeAddress(contadorGrade + 25 - itemEmbarque.getListGrade().size(), contadorGrade + 24, 1, 1));
        if (itemEmbarque.getListGrade().size() > 1)
            sheetGrade.addMergedRegion(new CellRangeAddress(contadorGrade + 25 - itemEmbarque.getListGrade().size(), contadorGrade + 24, 2, 2));
        if (itemEmbarque.getListGrade().size() > 1)
            sheetGrade.addMergedRegion(new CellRangeAddress(contadorGrade + 25 - itemEmbarque.getListGrade().size(), contadorGrade + 24, 4, 4));
    }

    private void criaLinhasGrade(XSSFSheet sheetGrade, int contadorGrade, SdItemEmbarque itemEmbarque, String nomeCurto, SdItemGradeEmbarque itemGrade, boolean unico) {
        if (itemEmbarque.getListGrade().indexOf(itemGrade) != itemEmbarque.getListGrade().size() - 1) {
            sheetGrade.copyRows(25, 25, 26 + contadorGrade, new CellCopyPolicy());
        }
        XSSFRow row = sheetGrade.getRow(25 + contadorGrade);
        if (unico) row.setHeight(new Short("700"));
        else row.setHeight(new Short("350"));
        row.getCell(0).setCellValue(itemEmbarque.getListGrade().stream().reduce(0, (p, eb) -> p + eb.getVolumes(), Integer::sum));
        row.getCell(1).setCellValue(itemEmbarque.getProduto().getCodigo() + "-COSTP");
        row.getCell(2).setCellValue(itemEmbarque.getNumero());
        row.getCell(3).setCellValue(itemGrade.getTamanho());
        row.getCell(4).setCellValue(nomeCurto);
        row.getCell(5).setCellValue(itemGrade.getQtde());
        row.getCell(6).setCellValue(itemGrade.getVolumes());
        row.getCell(7).setCellValue(itemGrade.getPesoLiquido().doubleValue());
        row.getCell(8).setCellValue(itemGrade.getPesoBruto().doubleValue());
        row.getCell(9).setCellValue(itemGrade.getCubagem().toString());
    }

    private void deleteMergedRegions(XSSFSheet sheet, int rangeInit, int rangeEnd) {
        for (int i = 0; i < sheet.getNumMergedRegions(); i++) { // Remove as areas mergeds
            CellRangeAddress range = sheet.getMergedRegion(i);
            if (range.getFirstRow() >= rangeInit && range.getFirstRow() < rangeEnd) {
                sheet.removeMergedRegion(i);
            }
        }
    }

    private void criaLinhasItens(XSSFSheet sheet, int cont, SdItemEmbarque item, String nomeCurto) {
        String nomeCompleto = nomeCurto + " inacabado  para beneficiar em lavandería y agregar aviamentos tais como, botão, rebite, placas, etiquetas.";
        String descricao = criaStringDescricao(item, nomeCompleto);

        XSSFRow row = sheet.getRow(18 + cont);
        row.getCell(0).setCellValue(item.getProduto().getCodigoPy() == null || item.getProduto().getCodigoPy().equals("") ? item.getProduto().getCodigo() + "-COSTP" : item.getProduto().getCodigoPy());
        row.getCell(1).setCellValue(item.getProduto().getCodigo() + "-COSTP");
        row.getCell(2).setCellValue(item.getProduto().getCodfis().getDescricao());
//        row.getCell(3).setCellValue(nomeCompleto);
        row.getCell(3).setCellValue(descricao);
        row.getCell(5).setCellValue(item.getQtde().intValue());
        row.getCell(6).setCellValue("UNIDAD");
        row.getCell(7).setCellValue(Double.parseDouble(item.getPesoTotal().toString()));
        row.getCell(8).setCellValue(Double.parseDouble(item.getProduto().getPrecodolar().toString()));
        row.getCell(9).setCellValue(Double.parseDouble(item.getPrecoTotal().toString()));
    }

    private void calculaValoresFixos(XSSFSheet sheet, DateTimeFormatter formatter, int size, List<SdItemEmbarque> listaEmbarque, int nFatura) {
        sheet.getRow(9).getCell(3).setCellValue(LocalDate.now().format(formatter));
        sheet.getRow(7).getCell(6).setCellValue("Nº 003-001-" + StringUtils.lpad(nFatura, 7, "0")); //Código da Fatura
        XSSFRow row = sheet.getRow(18 + size);

        int qtde = getQtdeByListItens(listaEmbarque).intValue();
        BigDecimal pesoLiquido = getPesoLiquidoByListItens(listaEmbarque);
        BigDecimal pesoBruto = getPesoBrutoByListItens(listaEmbarque);
        int volumes = getVolumesByListItens(listaEmbarque);

        row.getCell(5).setCellValue(qtde);
        row.getCell(7).setCellValue(pesoLiquido.doubleValue());
        row.getCell(9).setCellValue(listaEmbarque.stream().reduce(BigDecimal.ZERO, (partial, it) -> partial.add(it.getPrecoTotal()), BigDecimal::add).doubleValue());
        sheet.getRow(18 + size + 2).getCell(9).setCellValue(pesoLiquido.doubleValue());
        sheet.getRow(18 + size + 4).getCell(9).setCellValue(pesoBruto.doubleValue());
        sheet.getRow(18 + size + 6).getCell(9).setCellValue(volumes);
        sheet.getRow(18 + size + 8).getCell(9).setCellValue(listaEmbarque.stream().reduce(0.0, (partialResult, it) -> partialResult + (it.getListGrade().size() * 0.1), Double::sum));
    }

    private String criaStringDescricao(SdItemEmbarque item, String nome) {
        String composicaoEspanhol = traduzirComposicao(item.getProduto().getComposicao());
        String comeco = String.format("Composição %s tamanhos ", composicaoEspanhol);
        StringBuilder sb = new StringBuilder();
        item.getListGrade().sort((o1, o2) -> SortUtils.sortTamanhos(o1.getTamanho(),o2.getTamanho()));
        for (int i = 0; i < item.getListGrade().size(); i++) {
            if (i == 0) {
                sb.append(item.getListGrade().get(i).getTamanho());
            } else if (i == item.getListGrade().size() - 1) {
                sb.append(" y ").append(item.getListGrade().get(i).getTamanho());
            } else {
                sb.append(", ").append(item.getListGrade().get(i).getTamanho());
            }
        }
        return nome + comeco + sb.toString();
    }

    private String traduzirComposicao(String composicao) {
        String[] partes = composicao.split(" ");
        for (int i = 1; i < partes.length; i += 2) {
            partes[i] = traduzir(partes[i]);
        }
        return Stream.of(partes).reduce("", (a, b) -> a + " " + b);
    }

    private String traduzir(String parte) {
        String newParte;
        switch (parte) {
            case "ALGODAO":
            case "COTTON":
                newParte = "ALGODON";
                break;
            case "LIOCEL":
                newParte = "LYOCEL";
                break;
            case "VISCOSE":
                newParte = "VISCOSA";
                break;
            case "ELASTOMULTIESTER":
                newParte = "ELASTOMULTÍSTER";
                break;
            default:
                return parte;
        }
        return newParte;
    }

    private String criaStringNomeCurto(SdItemEmbarque item) {
        String idade = "";

        if (item.getListGrade().stream().map(SdItemGradeEmbarque::getTamanho).collect(Collectors.toList()).contains("34")) {
            idade = "infanto-juvenil";
        } else {
            idade = "adulto";
        }

        return String.format("%s %s %s, distintos colores",
                item.getProduto().getDescricion(), item.getProduto().getGenero().toLowerCase(), idade);
    }

    //</editor-fold>

    private void criarProforma() {
        try {
            proformaSelecionada = new SdProforma(embarqueSelecionado, pis, cofins, syscomex, frete, dolar, numeroDi, refSDi);
            proformaSelecionada = new FluentDao().persist(proformaSelecionada);
            embarqueSelecionado.setCodProforma(proformaSelecionada.getCodigo());
            new FluentDao().merge(embarqueSelecionado);
            MessageBox.create(message -> {
                message.message("Proforma Criada com Sucesso!");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.show();
            });
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Geração Nota">
    private void abrirJanelaDadosDI() {
        new Fragment().show(fragment -> {

            FormFieldText dolarField = FormFieldText.create(field -> {
                field.withoutTitle();
                field.label("Dólar: USD");
            });

            FormFieldText pisField = FormFieldText.create(field -> {
                field.withoutTitle();
                field.label("PIS: R$");
            });

            FormFieldText cofinsField = FormFieldText.create(field -> {
                field.withoutTitle();
                field.label("COFINS: R$");
            });

            FormFieldText syscomexField = FormFieldText.create(field -> {
                field.withoutTitle();
                field.label("SYSCOMEX: R$");
            });

            FormFieldText freteField = FormFieldText.create(field -> {
                field.withoutTitle();
                field.label("Frete: US$");
            });

            FormFieldText numeroDiField = FormFieldText.create(field -> {
                field.withoutTitle();
                field.mask(FormFieldText.Mask.INTEGER);
                field.label("Nº DI ");
            });

            FormFieldText referenciaDI = FormFieldText.create(field -> {
                field.withoutTitle();
                field.label("Ref/S ");
            });

            fragment.title.setText("Dados DI");
            fragment.size(400.0, 150.0);

            fragment.box.getChildren().add(FormBox.create(principal -> {
                principal.vertical();
                principal.add(numeroDiField.build(), referenciaDI.build(), dolarField.build(), pisField.build(), cofinsField.build(), syscomexField.build(), freteField.build());
            }));
            fragment.buttonsBox.getChildren().add(FormButton.create(botao -> {
                botao.title("Concluir");
                botao.addStyle("success");
                botao.icon(ImageUtils.getIconButton(ImageUtils.ButtonIcon.SELECT, ImageUtils.IconSize._24));
                botao.setOnAction(event -> {
                    if (numeroDiField.value.isEmpty().get() || referenciaDI.value.isEmpty().get() || dolarField.value.isEmpty().get()
                            || pisField.value.isEmpty().get() || cofinsField.value.isEmpty().get() || syscomexField.value.isEmpty().get() || freteField.value.isEmpty().get()) {
                        MessageBox.create(message -> {
                            message.message("Preencha todos os campos para prosseguir");
                            message.type(MessageBox.TypeMessageBox.WARNING);
                            message.showAndWait();
                        });
                    } else {
                        dolar = new BigDecimal(dolarField.value.getValue().replace(",", "."));
                        pis = new BigDecimal(pisField.value.getValue().replace(",", "."));
                        cofins = new BigDecimal(cofinsField.value.getValue().replace(",", "."));
                        syscomex = new BigDecimal(syscomexField.value.getValue().replace(",", "."));
                        frete = new BigDecimal(freteField.value.getValue().replace(",", "."));
                        numeroDi = numeroDiField.value.getValue();
                        refSDi = referenciaDI.value.getValue();
                        fragment.close();
                    }
                });
            }));
        });
    }

    private void mostrarNF() {
        try {
            new ReportUtils().config()
                    .addReport(ReportUtils.ReportFile.NF_PROFORMA,
                            new ReportUtils.ParameterReport("codEmbarque", embarqueSelecionado.getCodigo())
                    ).view().toView();
        } catch (JRException | SQLException e) {
            e.printStackTrace();
        }
    }

    private void gerarNota() {

        Nota nota = new Nota();

        proformaSelecionada.setNumeroNota(getNumeroNf());

        nota.setFatura(proformaSelecionada.getNumeroNota());

        nota.setVolumes(proformaSelecionada.getVolumes());
        nota.setPecas(proformaSelecionada.getQtde());
        nota.setPesol(proformaSelecionada.getPesoLiquido());
        nota.setPesob(proformaSelecionada.getPesoBruto());
        nota.setValor(proformaSelecionada.getValorNota());
        nota.setTipfat("G");
        nota.setCif("1");
        nota.setImpresso("N");
        nota.setTransport("0023");
        nota.setRedesp("0");
        nota.setCodcli("05830");
        nota.setNatureza(new FluentDao().selectFrom(Natureza.class).where(it -> it.equal("natureza", "3101000")).singleResult());
        nota.setEspecie("VOLUMES");
        nota.setValprodutos(proformaSelecionada.getValorProdutos());
        nota.setDespesas(proformaSelecionada.getTotalDespesas());
        nota.setMensagem(createMensagem(proformaSelecionada));
        nota.setSerie("1");
        nota.setRedespcif("2");
        nota.setDocto(proformaSelecionada.getNumeroDi());
        nota.setBaiest("N");
        nota.setCodrep("0126");
        nota.setNrvolumes(String.valueOf(proformaSelecionada.getQtde()));

        List<NotaIten> listNotas = new ArrayList<>();

        int ordem = 10;
        BigDecimal valorPis = BigDecimal.ZERO;
        BigDecimal valorCofins = BigDecimal.ZERO;
        proformaSelecionada.setItens(proformaSelecionada.getItens().stream().sorted(Comparator.comparing(obj -> obj.getCodProduto().getCodigo())).collect(Collectors.toList()));

        for (SdItemProforma item : proformaSelecionada.getItens()) {
            NotaIten notaItem = new NotaIten();
            NotaItenPK itenPK = new NotaItenPK();

            VSdDadosProdutoImportacao produto = new FluentDao().selectFrom(VSdDadosProdutoImportacao.class).where(it -> it.equal("codigo", item.getCodProduto().getCodigo())).singleResult();

            itenPK.setEmpenho("N");
            itenPK.setOrdem(ordem);
            itenPK.setFatura(proformaSelecionada.getNumeroNota());

            notaItem.setId(itenPK);

            notaItem.setQtde(item.getQtde());
            notaItem.setDatamvto(LocalDate.now());
            notaItem.setPreco(item.getPrecoRealFinal());
            notaItem.setValor(item.getPreco().setScale(2, RoundingMode.HALF_UP));
            notaItem.setTamanho("0");
            notaItem.setClafis(produto.getCodfis().getCodigo());
            notaItem.setClatrib(nota.getNatureza().getClatrib());
            notaItem.setUnidade(produto.getUnidade());
            notaItem.setCor("0");
            notaItem.setCodigo(item.getCodProduto().getCodigo());
            notaItem.setDescricao(produto.getDescricao());
            notaItem.setDeposito("0001");
            notaItem.setLote("000000");
            notaItem.setNatureza(nota.getNatureza());
            notaItem.setTipoit("M");
            notaItem.setOrigem("1");
            notaItem.setCodsped("01");
            notaItem.setVolumes(item.getVolumes());
            notaItem.setTribipi(nota.getNatureza().getTribIpi());
            notaItem.setTribpis(nota.getNatureza().getTribPis());
            notaItem.setPercpis(nota.getNatureza().getAliqPis());
            notaItem.setTribcofins(nota.getNatureza().getTribCofins());
            notaItem.setPerccofins(nota.getNatureza().getAliqCofins());
            notaItem.setBasepis(item.getPreco());
            notaItem.setBasecofins(item.getPreco());
            notaItem.setCodigo(produto.getCodigo() + "-COSTP");

            listNotas.add(notaItem);
            ordem += 10;

            valorPis = valorPis.add(notaItem.getBasepis().setScale(2, RoundingMode.HALF_EVEN).multiply(notaItem.getPercpis().divide(new BigDecimal("100"), 9, RoundingMode.CEILING)));
            valorCofins = valorCofins.add(notaItem.getBasecofins().setScale(2, RoundingMode.HALF_EVEN).multiply(notaItem.getPerccofins().divide(new BigDecimal("100"), 9, RoundingMode.CEILING)));
        }
        valorPis = valorPis.setScale(2, RoundingMode.HALF_EVEN);
        valorCofins = valorCofins.setScale(2, RoundingMode.HALF_EVEN);
        nota.setItens(listNotas);

        NotaItenPK itenPisPk = new NotaItenPK();
        itenPisPk.setEmpenho("S");
        itenPisPk.setOrdem(ordem);
        itenPisPk.setFatura(proformaSelecionada.getNumeroNota());

        NotaIten itenPis = new NotaIten();
        itenPis.setId(itenPisPk);
        itenPis.setQtde(BigDecimal.ZERO);
        itenPis.setPercipi(BigDecimal.ZERO);
        itenPis.setPreco(BigDecimal.ZERO);
        itenPis.setValor(valorPis);
        itenPis.setTipo("A");
        itenPis.setTamanho("N");
        itenPis.setCor("V");
        itenPis.setCodigo("9999900");
        itenPis.setDescricao("PIS/PASEP. R$ " + valorPis);
        itenPis.setCusto(BigDecimal.ZERO);
        itenPis.setOrdped(0);

        ordem += 10;

        NotaItenPK itensCofinsPk = new NotaItenPK();
        itensCofinsPk.setEmpenho("S");
        itensCofinsPk.setOrdem(ordem);
        itensCofinsPk.setFatura(proformaSelecionada.getNumeroNota());

        NotaIten itenCofins = new NotaIten();
        itenCofins.setId(itensCofinsPk);
        itenCofins.setQtde(BigDecimal.ZERO);
        itenCofins.setPercipi(BigDecimal.ZERO);
        itenCofins.setPreco(BigDecimal.ZERO);
        itenCofins.setValor(valorCofins);
        itenCofins.setTipo("A");
        itenCofins.setTamanho("N");
        itenCofins.setCor("V");
        itenCofins.setCodigo("9999900");
        itenCofins.setDescricao("COFINS. R$ " + valorCofins);
        itenCofins.setCusto(BigDecimal.ZERO);
        itenCofins.setOrdped(0);

        nota.getItens().add(itenCofins);
        nota.getItens().add(itenPis);

        nota.setValpis(valorPis);
        nota.setValcofins(valorCofins);

        NotaIten notaItemAjuste = nota.getItens().get(0);

        BigDecimal diferenca = proformaSelecionada.getValorProdutos().subtract(BigDecimal.valueOf(nota.getItens().stream().filter(eb -> eb.getId().getEmpenho().equals("N")).mapToDouble(it -> it.getValor().doubleValue()).sum()));

        notaItemAjuste.setValor(notaItemAjuste.getValor().add(diferenca));
        notaItemAjuste.setPreco(notaItemAjuste.getValor().divide(notaItemAjuste.getQtde(), 6, RoundingMode.HALF_UP));
        notaItemAjuste.setBasecofins(notaItemAjuste.getValor());
        notaItemAjuste.setBasepis(notaItemAjuste.getValor());

        try {
            nota = new FluentDao().persist(nota);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        SysLogger.addSysDelizLog("Criação de Proforma", TipoAcao.CADASTRAR, String.valueOf(proformaSelecionada.getCodigo()), "Nota " + proformaSelecionada.getNumeroNota() + " criada");
        proformaSelecionada = new FluentDao().merge(proformaSelecionada);
    }

    private String createMensagem(SdProforma proforma) {

        String numeroDiFormatado = proforma.getNumeroDi();
        SdEmbarque embarque = new FluentDao().selectFrom(SdEmbarque.class).where(it -> it.equal("codProforma", proforma.getCodigo())).singleResult();
        try {
            MaskFormatter maskFormatter = new MaskFormatter("##/#######-#");
            maskFormatter.setValueContainsLiteralCharacters(false);
            numeroDiFormatado = maskFormatter.valueToString(numeroDiFormatado);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "ICMS DIFERIDO CONFORME TTD Nº 155000000833332" + "\n" +
                "N/Ref: " + embarque.getFaturas().substring(0, embarque.getFaturas().lastIndexOf("/")) + "\n" +
                "S/Ref: " + proforma.getRefDi() + "\n" +
                "DI :  " + numeroDiFormatado + "\n" +
                "Valor PIS : R$ " + proforma.getPis() + "\n" +
                "Valor COFINS : R$ " + proforma.getCofins() + "\n" +
                "Tx.Siscomex : R$ " + proforma.getSyscomex() + "\n" +
                "Suspensão do ICMS" + "\n" +
                "Entrada \\ Saída tributada com alíquota zero" + "\n" +
                "Qtde: " + proforma.getQtde();
//        N/Ref: 1058/1059/1060/1061
//        S/Ref: S21068412O
//        DI :  21/1097170-2 09/06/2021
//        Valor PIS : R$ 10.843,93
//        Valor COFINS : R$ 49.830,40
//        Tx.Siscomex : R$ 555,24
//        Suspensão do ICMS
//        Entrada \ Saída tributada com alíquota zero
//        Qtde: 18.210

    }

    private String getNumeroNf() {
        try {
            String numero = new FluentDao().selectFrom(NotaNumero.class).max("id.numero").get().singleResult();
            numero = String.valueOf(Integer.parseInt(numero) + 1);
            new FluentDao().persist(new NotaNumero(numero, "1"));
            return numero;
        } catch (NumberFormatException | SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    // </editor-fold>

    //<editor-fold desc="Funcoes somatorios Lista">
    private Integer getVolumesByListItens(List<SdItemEmbarque> listaEmbarque) {
        return listaEmbarque.stream().mapToInt(item -> item.getListGrade().stream().mapToInt(SdItemGradeEmbarque::getVolumes).sum()).sum();//.reduce(0, (partial, it) -> partial + it.getListGrade().stream().reduce(0, (p, eb) -> p + eb.getVolumes(), Integer::sum), Integer::sum);
    }

    private BigDecimal getPesoBrutoByListItens(List<SdItemEmbarque> listaEmbarque) {
        return listaEmbarque.stream().reduce(BigDecimal.ZERO, (partialResult, it) -> partialResult.add(it.getListGrade().stream().reduce(BigDecimal.ZERO, (p, eb) -> p.add(eb.getPesoBruto()), BigDecimal::add)), BigDecimal::add);
    }

    private BigDecimal getPesoLiquidoByListItens(List<SdItemEmbarque> listaEmbarque) {
        return listaEmbarque.stream().reduce(BigDecimal.ZERO, (partial, it) -> partial.add(it.getPesoTotal()), BigDecimal::add);
    }

    private BigDecimal getQtdeByListItens(List<SdItemEmbarque> listaEmbarque) {
        return listaEmbarque.stream().reduce(BigDecimal.ZERO, (partialResult, it) -> it.getQtde().add(partialResult), BigDecimal::add);
    }
    //</editor-fold>

}
