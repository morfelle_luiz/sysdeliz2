package sysdeliz2.views.comercial.gestaopedido;

import javafx.beans.binding.Bindings;
import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import sysdeliz.apis.web.convertr.data.enums.IdentificadorLoja;
import sysdeliz.apis.web.convertr.data.models.customers.AdditionalAttribute;
import sysdeliz.apis.web.convertr.data.models.customers.CustomerStatus;
import sysdeliz.apis.web.convertr.data.models.orders.Cart;
import sysdeliz.apis.web.convertr.data.models.orders.OrderItem;
import sysdeliz.apis.web.convertr.data.models.rest.ResponseMagento;
import sysdeliz.apis.web.convertr.services.customers.CustomersB2BMagentoWebclient;
import sysdeliz.apis.web.convertr.services.orders.CartWebclient;
import sysdeliz2.controllers.views.comercial.gestaopedidos.CadastrarPedidosController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.models.sysdeliz.*;
import sysdeliz2.models.ti.*;
import sysdeliz2.models.view.VSdEstoqueB2B;
import sysdeliz2.models.view.VSdMrpEstoqueB2b;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.ReportUtils;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.exceptions.FormValidationException;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.mails.SimpleMail;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

public class CadastrarPedidosView extends CadastrarPedidosController {
    
    private final int QTDE_PRODUTOS_PAGINA = 15;
    private final VBox boxWindow = super.box;
    private final IntegerProperty inicioPaginaProdutos = new SimpleIntegerProperty(0);
    private final IntegerProperty fimPaginaProdutos = new SimpleIntegerProperty(QTDE_PRODUTOS_PAGINA);
    private final ListProperty<ProdutoB2B> produtosBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final BooleanProperty emEdicao = new SimpleBooleanProperty(false);
    private final ListProperty<GradeCarrinho> gradeCarrinho = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<Entidade> clientesCarrinho = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final IntegerProperty qtdePecasCarrinho = new SimpleIntegerProperty(0);
    private final DoubleProperty valorTotalCarrinho = new SimpleDoubleProperty(0.0);
    
    // <editor-fold defaultstate="collapsed" desc="Fields Pré-Config">
    private final FormFieldSegmentedButton<String> tipoPedidoField = FormFieldSegmentedButton.create(field -> {
        field.title("Tipo Pedido");
        field.options(
                field.option("B2B", "B2B", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("B2C", "B2C", FormFieldSegmentedButton.Style.WARNING),
                field.option("NORMAL", "ERP", FormFieldSegmentedButton.Style.SUCCESS)
        );
        field.select(0);
        field.disable.set(true);
        field.value.addListener((observable, oldValue, newValue) -> {
            limparTelaConfiguracaoPedido();
            if (newValue != null)
                telaConfiguracaoPedido(newValue.toString());
        });
    });
    private final FormFieldSingleFind<Marca> marcaPedidoField = FormFieldSingleFind.create(Marca.class, field -> {
        field.title("Marca");
        field.width(200.0);
        field.disable.bind(emEdicao);
    });
    private final FormFieldSingleFind<Regiao> tabPrecoField = FormFieldSingleFind.create(Regiao.class, field -> {
        field.title("Tabela Preço");
        field.editable.bind(emEdicao.not());
        field.width(250.0);
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Fields Filters Produtos">
    private final FormFieldMultipleFind<Produto> produtoField = FormFieldMultipleFind.create(Produto.class, field -> {
        field.title("Produto");
    });
    private final FormFieldMultipleFind<Colecao> colecaoField = FormFieldMultipleFind.create(Colecao.class, field -> {
        field.title("Coleção");
        field.width(270.0);
    });
    private final FormFieldMultipleFind<Familia> familiaField = FormFieldMultipleFind.create(Familia.class, field -> {
        field.title("Família");
    });
    private final FormFieldMultipleFind<Linha> linhaField = FormFieldMultipleFind.create(Linha.class, field -> {
        field.title("Linha");
    });
    
    private final FormFieldSegmentedButton<String> tipoEntregaField = FormFieldSegmentedButton.create(field -> {
        field.title("Tipo Entrega");
        field.options(
                field.option("Ambos", "AMBOS", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("Imediato", "0000", FormFieldSegmentedButton.Style.SUCCESS),
                field.option("Programado", "9999", FormFieldSegmentedButton.Style.WARNING)
        );
        field.select(0);
    });
    private final FormFieldSegmentedButton<String> tipoProdutoField = FormFieldSegmentedButton.create(field -> {
        field.title("Tipo");
        field.options(
                field.option("Ambos", "A", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("Moda", "M", FormFieldSegmentedButton.Style.DANGER),
                field.option("Jeans", "J", FormFieldSegmentedButton.Style.WARNING)
        );
        field.select(0);
    });
    
    private final FormFieldText referenciaField = FormFieldText.create(field -> {
        field.title("Referência");
        field.width(250.0);
        field.toUpper();
        field.editable.bind(produtosBean.emptyProperty().not());
        field.keyReleased(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                produtosBean.set(FXCollections.observableList(filteredProdutosB2B.stream().filter(produto -> produto.getTabPreco().getId().getCodigo().getCodigo().contains(field.value.get())).collect(Collectors.toList())));
                limparPaginador(0, QTDE_PRODUTOS_PAGINA > produtosBean.size() ? produtosBean.size() : QTDE_PRODUTOS_PAGINA);
                carregarProdutos(produtosBean.get().subList(inicioPaginaProdutos.get(), fimPaginaProdutos.get()));
            }
        });
    });
    
    private final FormScrollBox boxProdutos = FormScrollBox.create(innerBoxProdutos -> {
        innerBoxProdutos.tipo(FormScrollBox.TipoBox.FLOAT);
        innerBoxProdutos.prefWidthProperty().bind(boxWindow.prefWidthProperty());
        innerBoxProdutos.verticalScroll();
        innerBoxProdutos.expanded();
        innerBoxProdutos.pannable(true);
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Fields Config Cliente">
    private final FormFieldMultipleFind<Entidade> entidadeField = FormFieldMultipleFind.create(Entidade.class, field -> {
        field.title("Clientes");
    });
    private final TitledPane configuracaoClientes = FormTitledPane.create(finalizarCadastroPedidoBox -> {
        finalizarCadastroPedidoBox.title("Finalização do cadastro do pedido");
        finalizarCadastroPedidoBox.icon(ImageUtils.getImage(ImageUtils.Icon.ADICIONAR_PEDIDO));
        finalizarCadastroPedidoBox.addStyle("warning");
        finalizarCadastroPedidoBox.expanded();
        finalizarCadastroPedidoBox.add(FormBox.create(clientes -> {
            clientes.vertical();
            clientes.add(FormBox.create(layout1 -> {
                layout1.horizontal();
                layout1.alignment(Pos.BOTTOM_LEFT);
                layout1.add(entidadeField.build());
                layout1.add(FormButton.create(btnAddCliente -> {
                    btnAddCliente.title("Adicionar Clientes");
                    btnAddCliente.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._24));
                    btnAddCliente.addStyle("success");
                    btnAddCliente.setAction(evt -> {
                        entidadeField.objectValues.forEach(cliente -> {
                            if (!clientesCarrinho.contains(cliente))
                                clientesCarrinho.add(cliente);
                        });
                    });
                }));
            }));
            clientes.add(FormTableView.create(Entidade.class, table -> {
                table.title("Clientes Adicionados");
                table.items.bind(clientesCarrinho);
                table.expanded();
                table.columns(
                        FormTableColumn.create(cln -> {
                            cln.title("Cliente");
                            cln.width(300.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<Entidade, Entidade>, ObservableValue<Entidade>>) param -> new ReadOnlyObjectWrapper(param.getValue().toString()));
                        }).build() /*Cliente*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Grupo");
                            cln.width(180.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<Entidade, Entidade>, ObservableValue<Entidade>>) param -> new ReadOnlyObjectWrapper(param.getValue().getSdEntidade().getSitcliB2b().getDescricao()));
                        }).build() /*Grupo*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Sinc.");
                            cln.width(35);
                            cln.value((Callback<TableColumn.CellDataFeatures<Entidade, Entidade>, ObservableValue<Entidade>>) param -> new ReadOnlyObjectWrapper(param.getValue().getSdEntidade().isSincB2b()));
                            cln.alignment(FormTableColumn.Alignment.CENTER);
                            cln.format(param -> {
                                return new TableCell<Entidade, Boolean>() {
                                    @Override
                                    protected void updateItem(Boolean item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        setGraphic(null);
                                        if (item != null && !empty) {
                                            setGraphic(item ? ImageUtils.getIcon(ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._16) : ImageUtils.getIcon(ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                                        }
                                    }
                                };
                            });
                        }).build() /*Sinc.*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Marcas");
                            cln.width(150.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<Entidade, Entidade>, ObservableValue<Entidade>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                            cln.format(param -> {
                                return new TableCell<Entidade, Entidade>() {
                                    @Override
                                    protected void updateItem(Entidade item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        if (item != null && !empty) {
                                            setText(item.getMarcas().stream()
                                                    .filter(marca -> marca.isAtivo())
                                                    .map(marca -> marca.getId().getMarca().getDescricao())
                                                    .distinct()
                                                    .collect(Collectors.joining("/")));
                                        }
                                    }
                                };
                            });
                        }).build() /*Marcas*/
                );
                table.tableProperties().setOnKeyPressed(event -> {
                    if (event.isControlDown() && event.getCode() == KeyCode.DELETE) {
                        Entidade clienteSelecionado = (Entidade) table.selectedItem();
                        clientesCarrinho.remove(clienteSelecionado);
                    }
                });
            }).build());
        }));
    });
    // </editor-fold>
    
    public CadastrarPedidosView() {
        super("Inclusão de Pedidos", ImageUtils.getImage(ImageUtils.Icon.INCLUIR_CARRINHO));
        init();
    }
    
    private void init() {
        
        boxWindow.getChildren().add(FormBox.create(boxPrincipal -> {
            boxPrincipal.vertical();
            boxPrincipal.expanded();
            boxPrincipal.add(FormBox.create(layout1 -> {
                layout1.horizontal();
                layout1.add(FormTitledPane.create(boxPreConfig -> {
                    boxPreConfig.title("Primeria configuração do pedido");
                    boxPreConfig.icon(ImageUtils.getImage(ImageUtils.Icon.OPERACOES));
                    boxPreConfig.addStyle("primary");
                    boxPreConfig.add(FormBox.create(fieldsFilter -> {
                        fieldsFilter.horizontal();
                        fieldsFilter.add(FormBox.create(fieldsPreConfig -> {
                            fieldsPreConfig.vertical();
                            fieldsPreConfig.add(FormBox.create(box -> {
                                box.horizontal();
                                box.add(tipoPedidoField.build(), marcaPedidoField.build());
                            }));
                            fieldsPreConfig.add(FormBox.create(box -> {
                                box.horizontal();
                                box.add(tabPrecoField.build());
                            }));
                        }));
                        fieldsFilter.add(new Separator(Orientation.VERTICAL));
                        fieldsFilter.add(FormBox.create(box -> {
                            box.vertical();
                            box.alignment(Pos.BOTTOM_RIGHT);
                            box.add(FormButton.create(btnConfig -> {
                                btnConfig.title("Iniciar Pedido");
                                btnConfig.icon(ImageUtils.getIcon(ImageUtils.Icon.OPERACOES, ImageUtils.IconSize._24));
                                btnConfig.addStyle("success");
                                btnConfig.disable.bind(emEdicao);
                                btnConfig.setAction(evt -> {
                                    configurarProdutosPedido();
                                });
                            }));
                            box.add(FormButton.create(btnCancela -> {
                                btnCancela.title("Cancelar");
                                btnCancela.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                btnCancela.addStyle("danger");
                                btnCancela.disable.bind(emEdicao.not());
                                btnCancela.setAction(evt -> {
                                    limparConfiguracao();
                                });
                            }));
                        }));
                    }));
                }));
            }));
            boxPrincipal.add(FormTabPane.create(paneTab -> {
                paneTab.expanded();
                paneTab.addTab(
                        FormTab.create(tabProdutos -> {
                            tabProdutos.title("Produtos");
                            tabProdutos.icon(ImageUtils.getIcon(ImageUtils.Icon.PRODUTO, ImageUtils.IconSize._16));
                            tabProdutos.addStyle("success");
                            tabProdutos.disabled.bind(emEdicao.not());
                            tabProdutos.add(FormBox.create(layout2 -> {
                                layout2.vertical();
                                layout2.expanded();
                                layout2.add(FormBox.create(layout3 -> {
                                    layout3.horizontal();
                                    layout3.add(FormTitledPane.create(filter -> {
                                        filter.filter();
                                        filter.collapse(false);
                                        filter.disabled.bind(emEdicao.not());
                                        filter.add(FormBox.create(layout4 -> {
                                            layout4.horizontal();
                                            layout4.add(FormBox.create(layout5 -> {
                                                layout5.vertical();
                                                layout5.add(produtoField.build());
                                                layout5.add(colecaoField.build());
                                                layout5.add(linhaField.build());
                                            }));
                                            layout4.add(FormBox.create(layout5 -> {
                                                layout5.vertical();
                                                layout5.add(tipoEntregaField.build());
                                                layout5.add(tipoProdutoField.build());
                                                layout5.add(familiaField.build());
                                            }));
                                        }));
                                        filter.find.setOnAction(evt -> {
                                            new RunAsyncWithOverlay(this).exec(task -> {
                                                filteredProdutosB2B.clear();
                                                filteredProdutosB2B.addAll(produtosB2B);
                                                if (!produtoField.isEmpty()) {
                                                    filteredProdutosB2B = filteredProdutosB2B.stream().filter(produtoB2B -> {
                                                        for (String s : produtoField.textValue.get().split(",")) {
                                                            if (s.equals(produtoB2B.getTabPreco().getId().getCodigo().getCodigo()))
                                                                return true;
                                                        }
                                                        return false;
                                                    }).collect(Collectors.toList());
                                                }
                                                if (!colecaoField.isEmpty()) {
                                                    filteredProdutosB2B = filteredProdutosB2B.stream().filter(produtoB2B -> {
                                                        for (String s : colecaoField.textValue.get().split(",")) {
                                                            if (s.equals(produtoB2B.getTabPreco().getId().getCodigo().getCodCol()))
                                                                return true;
                                                        }
                                                        return false;
                                                    }).collect(Collectors.toList());
                                                }
                                                if (!linhaField.isEmpty()) {
                                                    filteredProdutosB2B = filteredProdutosB2B.stream().filter(produtoB2B -> {
                                                        for (String s : linhaField.textValue.get().split(",")) {
                                                            if (s.equals(produtoB2B.getTabPreco().getId().getCodigo().getCodlin()))
                                                                return true;
                                                        }
                                                        return false;
                                                    }).collect(Collectors.toList());
                                                }
                                                if (!familiaField.isEmpty()) {
                                                    filteredProdutosB2B = filteredProdutosB2B.stream().filter(produtoB2B -> {
                                                        for (String s : familiaField.textValue.get().split(",")) {
                                                            if (s.equals(produtoB2B.getTabPreco().getId().getCodigo().getCodFam()))
                                                                return true;
                                                        }
                                                        return false;
                                                    }).collect(Collectors.toList());
                                                }
                                                if (!tipoProdutoField.value.get().equals("A")) {
                                                    filteredProdutosB2B = filteredProdutosB2B.stream().filter(produtoB2B -> produtoB2B.getTabPreco().getId().getCodigo().getTipo().equals(tipoProdutoField.value.get())).collect(Collectors.toList());
                                                }
                                                if (!tipoEntregaField.value.get().equals("AMBOS")) {
                                                    filteredProdutosB2B = filteredProdutosB2B.stream().filter(produtoB2B -> produtoB2B.getEstoqueProduto().stream().anyMatch(estoque -> estoque.getEntrega().equals(tipoEntregaField.value.get()) && estoque.getQtde() > 0)).collect(Collectors.toList());
                                                }
                                                return ReturnAsync.OK.value;
                                            }).addTaskEndNotification(taskReturn -> {
                                                if (taskReturn.equals(ReturnAsync.OK.value)) {
                                                    produtosBean.set(FXCollections.observableList(filteredProdutosB2B));
                                                    limparPaginador(0, QTDE_PRODUTOS_PAGINA);
                                                    carregarProdutos(produtosBean.get().subList(inicioPaginaProdutos.get(), fimPaginaProdutos.get()));
                                                }
                                            });
                                        });
                                        filter.clean.setOnAction(evt -> {
                                            limparFiltroProdutos();
                                        });
                                    }));
                                }));
                                layout2.add(FormBox.create(layout4 -> {
                                    layout4.horizontal();
                                    layout4.alignment(Pos.BOTTOM_LEFT);
                                    layout4.add(referenciaField.build());
                                    layout4.add(FormButton.create(btnLimparProducura -> {
                                        btnLimparProducura.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                        btnLimparProducura.tooltip("Limpar pesquisa");
                                        btnLimparProducura.icon(ImageUtils.getIcon(ImageUtils.Icon.LIMPAR, ImageUtils.IconSize._16));
                                        btnLimparProducura.addStyle("warning");
                                        btnLimparProducura.setAction(event -> {
                                            referenciaField.clear();
                                            produtosBean.set(FXCollections.observableList(filteredProdutosB2B));
                                            limparPaginador(0, QTDE_PRODUTOS_PAGINA);
                                            carregarProdutos(produtosBean.get().subList(inicioPaginaProdutos.get(), fimPaginaProdutos.get()));
                                        });
                                    }));
                                    layout4.add(FormBox.create(boxTotais -> {
                                        boxTotais.horizontal();
                                        boxTotais.expanded();
                                        boxTotais.alignment(Pos.BOTTOM_RIGHT);
                                        boxTotais.add(FormFieldText.create(field -> {
                                            field.withoutTitle();
                                            field.label("Total Peças:");
                                            field.width(130.0);
                                            field.alignment(FormFieldText.Alignment.CENTER);
                                            field.value.bind(qtdePecasCarrinho.asString());
                                            field.addStyle("lg").addStyle("info");
                                        }).build());
                                        boxTotais.add(FormFieldText.create(field -> {
                                            field.withoutTitle();
                                            field.width(220.0);
                                            field.label("Valor Total:");
                                            field.alignment(FormFieldText.Alignment.RIGHT);
                                            field.value.bind(Bindings.format("R$ %.2f", valorTotalCarrinho));
                                            field.addStyle("lg").addStyle("info");
                                        }).build());
                                    }));
                                }));
                                layout2.add(boxProdutos);
                                layout2.add(FormBox.create(boxPaginacao -> {
                                    boxPaginacao.horizontal();
                                    boxPaginacao.add(FormButton.create(btn -> {
                                        btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ANTERIOR, ImageUtils.IconSize._24));
                                        btn.disable.bind(inicioPaginaProdutos.isEqualTo(0));
                                        btn.setAction(evt -> {
                                            limparPaginador(inicioPaginaProdutos.subtract(QTDE_PRODUTOS_PAGINA).get(), fimPaginaProdutos.subtract(QTDE_PRODUTOS_PAGINA).get());
                                            carregarProdutos(produtosBean.get().subList(inicioPaginaProdutos.get(), fimPaginaProdutos.get()));
                                        });
                                    }));
                                    boxPaginacao.add(FormButton.create(btn -> {
                                        btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.PROXIMO, ImageUtils.IconSize._24));
                                        btn.disable.bind(fimPaginaProdutos.greaterThanOrEqualTo(produtosBean.sizeProperty().subtract(1)));
                                        btn.setAction(evt -> {
                                            limparPaginador(inicioPaginaProdutos.add(QTDE_PRODUTOS_PAGINA).get(), fimPaginaProdutos.add(QTDE_PRODUTOS_PAGINA).get());
                                            carregarProdutos(produtosBean.get().subList(inicioPaginaProdutos.get(), fimPaginaProdutos.get()));
                                        });
                                    }));
                                    boxPaginacao.add(FormBox.create(layout5 -> {
                                        layout5.horizontal();
                                        layout5.expanded();
                                        layout5.alignment(Pos.CENTER_RIGHT);
                                        layout5.add(FormFieldText.create(field -> {
                                            field.editable(false);
                                            field.width(150.0);
                                            field.withoutTitle();
                                            field.label("Total Produtos");
                                            field.value.bind(produtosBean.sizeProperty().asString());
                                        }).build());
                                    }));
                                }));
                            }));
                        }),
                        FormTab.create(tabCarrinho -> {
                            tabCarrinho.title("Carrinho");
                            tabCarrinho.icon(ImageUtils.getIcon(ImageUtils.Icon.INCLUIR_CARRINHO, ImageUtils.IconSize._16));
                            tabCarrinho.addStyle("warning");
                            tabCarrinho.disabled.bind(emEdicao.not().or(gradeCarrinho.emptyProperty()));
                            tabCarrinho.add(FormBox.create(boxFinalizacao -> {
                                boxFinalizacao.horizontal();
                                boxFinalizacao.expanded();
                                boxFinalizacao.add(FormBox.create(boxCarrinho -> {
                                    boxCarrinho.vertical();
                                    boxCarrinho.width(600.0);
                                    boxCarrinho.add(FormBox.create(layout1 -> {
                                        layout1.vertical();
                                        layout1.expanded();
                                        layout1.add(FormTitledPane.create(filter -> {
                                            filter.title("Carrinho");
                                            filter.icon(ImageUtils.getImage(ImageUtils.Icon.COMPRAS));
                                            filter.addStyle("success");
                                            filter.expanded();
                                            filter.add(FormBox.create(layout2 -> {
                                                layout2.vertical();
                                                layout2.expanded();
                                                layout2.add(FormTableView.create(GradeCarrinho.class, table -> {
                                                    table.items.bind(gradeCarrinho);
                                                    table.expanded();
                                                    table.multipleSelection();
                                                    table.columns(
                                                            FormTableColumn.create(cln -> {
                                                                cln.title("Produto");
                                                                cln.width(180.0);
                                                                cln.value((Callback<TableColumn.CellDataFeatures<GradeCarrinho, GradeCarrinho>, ObservableValue<GradeCarrinho>>) param -> new ReadOnlyObjectWrapper(param.getValue().getProduto().getId().getCodigo()));
                                                            }).build() /*Produto*/,
                                                            FormTableColumn.create(cln -> {
                                                                cln.title("Cor");
                                                                cln.width(40.0);
                                                                cln.value((Callback<TableColumn.CellDataFeatures<GradeCarrinho, GradeCarrinho>, ObservableValue<GradeCarrinho>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCor()));
                                                                cln.alignment(FormTableColumn.Alignment.CENTER);
                                                            }).build() /*Cor*/,
                                                            FormTableColumn.create(cln -> {
                                                                cln.title("Desc. Cor");
                                                                cln.width(90.0);
                                                                cln.value((Callback<TableColumn.CellDataFeatures<GradeCarrinho, GradeCarrinho>, ObservableValue<GradeCarrinho>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDescCor()));
                                                            }).build() /*Desc. Cor*/,
                                                            FormTableColumn.create(cln -> {
                                                                cln.title("Tam");
                                                                cln.width(35.0);
                                                                cln.value((Callback<TableColumn.CellDataFeatures<GradeCarrinho, GradeCarrinho>, ObservableValue<GradeCarrinho>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTam()));
                                                                cln.alignment(FormTableColumn.Alignment.CENTER);
                                                            }).build() /*Tam*/,
                                                            FormTableColumn.create(cln -> {
                                                                cln.title("Entrega");
                                                                cln.width(90.0);
                                                                cln.value((Callback<TableColumn.CellDataFeatures<GradeCarrinho, GradeCarrinho>, ObservableValue<GradeCarrinho>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDescEntrega()));
                                                            }).build() /*Entrega*/,
                                                            FormTableColumn.create(cln -> {
                                                                cln.title("Qtde");
                                                                cln.width(35.0);
                                                                cln.value((Callback<TableColumn.CellDataFeatures<GradeCarrinho, GradeCarrinho>, ObservableValue<GradeCarrinho>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                                                                cln.alignment(FormTableColumn.Alignment.CENTER);
                                                            }).build() /*Qtde*/,
                                                            FormTableColumn.create(cln -> {
                                                                cln.title("Total");
                                                                cln.width(80.0);
                                                                cln.value((Callback<TableColumn.CellDataFeatures<GradeCarrinho, GradeCarrinho>, ObservableValue<GradeCarrinho>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                                                cln.alignment(FormTableColumn.Alignment.RIGHT);
                                                                cln.format(param -> {
                                                                    return new TableCell<GradeCarrinho, GradeCarrinho>() {
                                                                        @Override
                                                                        protected void updateItem(GradeCarrinho item, boolean empty) {
                                                                            super.updateItem(item, empty);
                                                                            setText(null);
                                                                            if (item != null && !empty) {
                                                                                setText(StringUtils.toMonetaryFormat(
                                                                                        item.getQtde() * item.getProduto().getPreco00().doubleValue(), 2));
                                                                            }
                                                                        }
                                                                    };
                                                                });
                                                            }).build() /*Total*/
                                                    );
                                                    table.tableProperties().setOnKeyPressed(event -> {
                                                        if (event.isControlDown() && event.getCode() == KeyCode.DELETE) {
                                                            List<GradeCarrinho> gradeSelecionada = new ArrayList<>();
                                                            gradeSelecionada.addAll(table.selectedRegisters());
                                                            if ((Boolean) QuestionBox.build(Boolean.class, message -> {
                                                                message.message("Deseja realmente excluir os " + gradeSelecionada.size() + " itens selecionados?");
                                                                message.showAndWait();
                                                            }).value.get()) {
                                                                gradeSelecionada.forEach(grade -> {
                                                                    gradeCarrinho.remove(grade);
                                                                });
                                                                table.refresh();
                                                            }
                                                        }
                                                    });
                                                }).build());
                                            }));
                                        }));
                                    }));
                                }));
                                boxFinalizacao.add(FormBox.create(layout1 -> {
                                    layout1.vertical();
                                    layout1.expanded();
                                    layout1.add(configuracaoClientes);
                                    layout1.add(FormBox.create(layout2 -> {
                                        layout2.horizontal();
                                        layout2.add(FormFieldText.create(field -> {
                                            field.withoutTitle();
                                            field.label("Total Peças:");
                                            field.width(130.0);
                                            field.alignment(FormFieldText.Alignment.CENTER);
                                            field.value.bind(qtdePecasCarrinho.asString());
                                            field.addStyle("lg").addStyle("info");
                                        }).build());
                                        layout2.add(FormFieldText.create(field -> {
                                            field.withoutTitle();
                                            field.width(220.0);
                                            field.label("Valor Total:");
                                            field.alignment(FormFieldText.Alignment.RIGHT);
                                            field.value.bind(Bindings.format("R$ %.2f", valorTotalCarrinho));
                                            field.addStyle("lg").addStyle("info");
                                        }).build());
                                        layout2.add(FormBox.create(layout3 -> {
                                            layout3.horizontal();
                                            layout3.expanded();
                                            layout3.alignment(Pos.BOTTOM_RIGHT);
                                            layout3.add(FormButton.create(btnEnviar -> {
                                                btnEnviar.title("Enviar Pedido");
                                                btnEnviar.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._32));
                                                btnEnviar.addStyle("lg").addStyle("success");
                                                btnEnviar.disable.bind(clientesCarrinho.emptyProperty());
                                                btnEnviar.setAction(evtSender -> {
                                                    for (Entidade cliente : clientesCarrinho) {
                                                        if (!cliente.getSdEntidade().isSincB2b()) {
                                                            continue;
                                                        }
                                                        
                                                        if (cliente.getSdEntidade().getSitcliB2b().getCodigo() !=
                                                                new Integer(tabPrecoField.value.get().getRegiao().replace("B2B", ""))) {
                                                            SdMagentoSitCli sitcliMagento = new FluentDao().selectFrom(SdMagentoSitCli.class)
                                                                    .where(it -> it
                                                                            .equal("codigo", new Integer(tabPrecoField.value.get().getRegiao().replace("B2B", ""))))
                                                                    .singleResult();
                                                            cliente.getSdEntidade().setSitcliB2b(sitcliMagento);
                                                            new FluentDao().runNativeQueryUpdate(String.format("update sd_entidade_001 set sitcli_b2b = %s where codcli = %s", sitcliMagento.getCodigo(), cliente.getStringCodcli()));
                                                            CustomerStatus upadateClienteB2b = convertToCustomerStatus(cliente);
                                                            ResponseMagento response = CustomersB2BMagentoWebclient.INSTANCE.configure(IdentificadorLoja.B2B, true)
                                                                    .update(upadateClienteB2b);
                                                        }
                                                        
                                                        Object[] marcas = cliente.getMarcas().stream()
                                                                .filter(marca ->
                                                                        marca.isAtivo() &&
                                                                                (marca.getId().getMarca().getCodigo().equals("D") ||
                                                                                        marca.getId().getMarca().getCodigo().equals("F")))
                                                                .map(marca -> marca.getId().getMarca().getCodigo()).distinct().toArray();
                                                        List<GradeCarrinho> itensParaEnvio = new ArrayList<>();
                                                        if (marcas.length == 0) {
                                                            continue;
                                                        } else if (marcas.length == 1) {
                                                            itensParaEnvio = gradeCarrinho.stream()
                                                                    .filter(produto -> produto.getProduto().getId().getCodigo().getCodMarca().equals(marcas[0]))
                                                                    .collect(Collectors.toList());
                                                        } else {
                                                            itensParaEnvio = gradeCarrinho.get();
                                                        }
                                                        if (itensParaEnvio.size() == 0)
                                                            continue;
                                                        
                                                        List<OrderItem> itemsCart = new ArrayList<>();
                                                        itensParaEnvio.forEach(item -> {
                                                            itemsCart.add(new OrderItem(
                                                                    item.getProduto().getId().getCodigo().getCodigo().concat("-")
                                                                            .concat(item.getCor()).concat("-")
                                                                            .concat(item.getTam()),
                                                                    null, new BigDecimal(item.getQtde()), null, null, null,
                                                                    item.getEntrega()
                                                            ));
                                                        });
                                                        Cart cartB2b = new Cart(StringUtils.unformatCpfCnpj(cliente.getCnpj()), itemsCart);
                                                        ResponseMagento respostaServidor = CartWebclient.INSTANCE.configure(IdentificadorLoja.B2B, true).addcart(cartB2b);
                                                        SysLogger.addSysDelizLog("Digitador Pedido B2B", TipoAcao.CADASTRAR, cartB2b.getTaxvat(), "Enviado carrinho para loja B2B. Carrinho: " + itemsCart.stream().mapToInt(item -> item.getQty().intValue()).sum() + " itens - Resposta do Servidor: " + respostaServidor.getMessage());
                                                        if (respostaServidor.getError()) {
                                                            MessageBox.create(message -> {
                                                                message.message("Erro no envio de carrinho para a loja para o cliente " + cliente.getNome() + "\nErro reportado: " + respostaServidor.getMessage());
                                                                message.type(MessageBox.TypeMessageBox.ERROR);
                                                                message.showAndWait();
                                                            });
                                                        } else {
                                                            try {
                                                                
                                                                SdMagentoEnderecoBean enderecoMagento = null;
                                                                SdMagentoEntregaBean entregaMagento = null;
                                                                SdMagentoPagamentoBean pagamentoMagento = null;
                                                                SdMagentoTotaisBean totaisMagento = null;
                                                                SdMagentoClienteBean clienteMagento = new FluentDao().selectFrom(SdMagentoClienteBean.class)
                                                                        .where(it -> it
                                                                                .equal("taxvat", StringUtils.unformatCpfCnpj(cliente.getCnpj())))
                                                                        .singleResult();
                                                                if (clienteMagento == null) {
                                                                    clienteMagento = new SdMagentoClienteBean();
                                                                    clienteMagento.setDtNasc(StringUtils.toDateFormat(cliente.getDataFund() != null ? cliente.getDataFund() : LocalDate.now()));
                                                                    clienteMagento.setEmail(cliente.getEmail());
                                                                    clienteMagento.setFirstname(cliente.getNome());
                                                                    clienteMagento.setLastname(cliente.getFantasia());
                                                                    clienteMagento.setRg(cliente.getInscricao());
                                                                    clienteMagento.setTaxvat(StringUtils.unformatCpfCnpj(cliente.getCnpj()));
                                                                    clienteMagento = new FluentDao().persist(clienteMagento);
                                                                    
                                                                    enderecoMagento = new SdMagentoEnderecoBean();
                                                                    enderecoMagento.setCity(cliente.getCepEnt().getCidade().getNomeCid());
                                                                    enderecoMagento.setComplement(cliente.getCompEnt());
                                                                    enderecoMagento.setNeighborhood(cliente.getBairroEnt());
                                                                    enderecoMagento.setNumber(cliente.getNumEnt());
                                                                    enderecoMagento.setPostcode(cliente.getCepEnt().getCep());
                                                                    enderecoMagento.setRegion(cliente.getCepEnt().getCidade().getCodEst().getDescricao());
                                                                    enderecoMagento.setStreet(cliente.getEndEnt());
                                                                    enderecoMagento.setTelephone(cliente.getTelefone());
                                                                    enderecoMagento = new FluentDao().persist(enderecoMagento);
                                                                    
                                                                    entregaMagento = new SdMagentoEntregaBean();
                                                                    entregaMagento.setAddress(enderecoMagento);
                                                                    entregaMagento.setDescription("Entrega Deliz");
                                                                    entregaMagento.setFirstname(cliente.getNome());
                                                                    entregaMagento.setLastname(cliente.getFantasia());
                                                                    entregaMagento.setTotal(BigDecimal.ZERO);
                                                                    entregaMagento = new FluentDao().persist(entregaMagento);
                                                                } else {
                                                                    SdMagentoClienteBean finalClienteMagento = clienteMagento;
                                                                    List<SdMagentoOrderBean> ordersCliente = (List<SdMagentoOrderBean>) new FluentDao().selectFrom(SdMagentoOrderBean.class)
                                                                            .where(it -> it
                                                                                    .equal("customer", finalClienteMagento.getId()))
                                                                            .resultList();
                                                                    
                                                                    SdMagentoOrderBean orderCliente = ordersCliente.get(0);
                                                                    entregaMagento = orderCliente.getShipping();
                                                                }
                                                                
                                                                pagamentoMagento = new SdMagentoPagamentoBean();
                                                                pagamentoMagento.setMethod("cashondelivery");
                                                                pagamentoMagento = new FluentDao().persist(pagamentoMagento);
                                                                
                                                                for (Object marca : marcas) {
                                                                    Object[] entregasOrder = gradeCarrinho.stream()
                                                                            .filter(produto -> produto.getProduto().getId().getCodigo().getCodMarca().equals(marca))
                                                                            .map(item -> item.getEntrega()).distinct().toArray();
                                                                    List<String> orderIds = new ArrayList<>();
                                                                    for (Object entrega : entregasOrder) {
                                                                        List<GradeCarrinho> orderItens = gradeCarrinho.stream()
                                                                                .filter(produto -> produto.getProduto().getId().getCodigo().getCodMarca().equals(marca) &&
                                                                                        produto.getEntrega().equals(entrega))
                                                                                .collect(Collectors.toList());
                                                                        totaisMagento = new SdMagentoTotaisBean();
                                                                        totaisMagento.setDesconto(BigDecimal.ZERO);
                                                                        totaisMagento.setGrandTotal(new BigDecimal(orderItens.stream()
                                                                                .mapToDouble(item -> item.getQtde() * item.getProduto().getPreco00().doubleValue()).sum()));
                                                                        totaisMagento = new FluentDao().persist(totaisMagento);
                                                                        TabPrzBean periodoEntrega = new FluentDao().selectFrom(TabPrzBean.class)
                                                                                .where(it -> it
                                                                                        .equal("prazo", entrega))
                                                                                .singleResult();
                                                                        SdMagentoOrderBean orderMagento = new SdMagentoOrderBean();
                                                                        orderMagento.setEntrega(periodoEntrega);
                                                                        orderMagento.setStatus("sent");
                                                                        orderMagento.setCreatedAt(StringUtils.toDateTimeFormat(LocalDateTime.now()));
                                                                        orderMagento.setCustomer(clienteMagento);
                                                                        orderMagento.setShipping(entregaMagento);
                                                                        orderMagento.setPayment(pagamentoMagento);
                                                                        orderMagento.setTotals(totaisMagento);
                                                                        orderMagento.setMarca(marca.toString());
                                                                        orderMagento.setOrigem("ERP");
                                                                        orderMagento = new FluentDao().persist(orderMagento);
                                                                        
                                                                        for (GradeCarrinho orderIten : orderItens) {
                                                                            SdMagentoOrderItemBean itemMagento = new SdMagentoOrderItemBean();
                                                                            itemMagento.setDiscountAmount(BigDecimal.ZERO);
                                                                            itemMagento.setName(orderIten.getProduto().getId().getCodigo().getDescricao());
                                                                            itemMagento.setOrderId(orderMagento.getId());
                                                                            itemMagento.setPrice(orderIten.getProduto().getPreco00());
                                                                            itemMagento.setQty(new BigDecimal(orderIten.getQtde()));
                                                                            itemMagento.setSku(orderIten.getProduto().getId().getCodigo().getCodigo().concat("-")
                                                                                    .concat(orderIten.getCor()).concat("-")
                                                                                    .concat(orderIten.getTam()));
                                                                            itemMagento = new FluentDao().persist(itemMagento);
                                                                        }
                                                                        
                                                                        SysLogger.addSysDelizLog("Digitador Pedido B2B", TipoAcao.CADASTRAR, String.valueOf(orderMagento.getId()), "Salvando carrinho do cliente no banco. Cliente: " + clienteMagento.getFirstname() + " marca: " + orderMagento.getMarca() + " entrega: " + orderMagento.getEntrega());
                                                                        orderIds.add(String.valueOf(orderMagento.getId()));
                                                                    }
                                                                    if (orderIds.size() == 0)
                                                                        continue;
                                                                    String orders = orderIds.stream().collect(Collectors.joining(","));
                                                                    new ReportUtils().config().addReport(ReportUtils.ReportFile.ESPELHO_PEDIDO_B2B, new ReportUtils.ParameterReport[]{
                                                                            new ReportUtils.ParameterReport("order_ids", orders),
                                                                            new ReportUtils.ParameterReport("codEmpresa", Globals.getEmpresaLogada().getCodigo())
                                                                    }).view().toPdf("C:\\SysDelizLocal\\local_files\\cartb2b_" + clienteMagento.getTaxvat() + ".pdf");
                                                                    Optional<Contato> optionalContato = cliente.getContatos().stream().filter(contato -> contato.getTipo().getCodigo().equals("6")).findFirst();
                                                                    String nomeResponsavel = optionalContato.isPresent() ? optionalContato.get().getNome() : cliente.getNome();
                                                                    Optional<SdMarcasEntidade> optionalMarca = cliente.getMarcas().stream().filter(marcaCli -> marcaCli.getId().getMarca().getCodigo().equals(marca)).findFirst();
                                                                    String representanteMarca = optionalMarca.isPresent() ? optionalMarca.get().getCodrep().getRespon() : Globals.getUsuarioLogado().getApelido();
                                                                    SimpleMail.INSTANCE
                                                                            .addReplyTo(Globals.getUsuarioLogado().getEmail())
                                                                            .setSender(Globals.getUsuarioLogado().getDisplayName())
                                                                            .addDestinatario(cliente.getEmail())
//                                                                            .addDestinatario("diego@deliz.com.br")
                                                                            .addCCO("diego@deliz.com.br")
                                                                            .addAttachment("C:\\SysDelizLocal\\local_files\\cartb2b_" + clienteMagento.getTaxvat() + ".pdf")
                                                                            .comAssunto("Portal B2B - Plataforma de Compras Deliz Fashion Group")
                                                                            .comCorpoHtml(() -> {
                                                                                return "<html>\n" +
                                                                                        "<head>\n" +
                                                                                        "<meta http-equiv=Content-Type content=\"text/html; charset=iso-8859-1\">\n" +
                                                                                        "<meta name=Generator content=\"Microsoft Word 14 (filtered medium)\">\n" +
                                                                                        "<style>\n" +
                                                                                        "   @font-face\n" +
                                                                                        "   {font-family:\"Cambria Math\";\n" +
                                                                                        "   panose-1:2 4 5 3 5 4 6 3 2 4;}\n" +
                                                                                        "   @font-face\n" +
                                                                                        "   {font-family:Calibri;\n" +
                                                                                        "   panose-1:2 15 5 2 2 2 4 3 2 4;}\n" +
                                                                                        "   @font-face\n" +
                                                                                        "   {font-family:Tahoma;\n" +
                                                                                        "   panose-1:2 11 6 4 3 5 4 4 2 4;}\n" +
                                                                                        "   p.MsoNormal, li.MsoNormal, div.MsoNormal\n" +
                                                                                        "   {margin:0cm;\n" +
                                                                                        "   margin-bottom:.0001pt;\n" +
                                                                                        "   font-size:11.0pt;\n" +
                                                                                        "   font-family:\"Calibri\",\"sans-serif\";\n" +
                                                                                        "   mso-fareast-language:EN-US;}\n" +
                                                                                        "   a:link, span.MsoHyperlink\n" +
                                                                                        "   {mso-style-priority:99;\n" +
                                                                                        "   color:blue;\n" +
                                                                                        "   text-decoration:underline;}\n" +
                                                                                        "   a:visited, span.MsoHyperlinkFollowed\n" +
                                                                                        "   {mso-style-priority:99;\n" +
                                                                                        "   color:purple;\n" +
                                                                                        "   text-decoration:underline;}\n" +
                                                                                        "   p.MsoAcetate, li.MsoAcetate, div.MsoAcetate\n" +
                                                                                        "   {mso-style-priority:99;\n" +
                                                                                        "   mso-style-link:\"Texto de balão Char\";\n" +
                                                                                        "   margin:0cm;\n" +
                                                                                        "   margin-bottom:.0001pt;\n" +
                                                                                        "   font-size:8.0pt;\n" +
                                                                                        "   font-family:\"Tahoma\",\"sans-serif\";\n" +
                                                                                        "   mso-fareast-language:EN-US;}\n" +
                                                                                        "   span.EstiloDeEmail17\n" +
                                                                                        "   {mso-style-type:personal-compose;\n" +
                                                                                        "   font-family:\"Calibri\",\"sans-serif\";\n" +
                                                                                        "   color:windowtext;}\n" +
                                                                                        "   span.TextodebaloChar\n" +
                                                                                        "   {mso-style-name:\"Texto de balão Char\";\n" +
                                                                                        "   mso-style-priority:99;\n" +
                                                                                        "   mso-style-link:\"Texto de balão\";\n" +
                                                                                        "   font-family:\"Tahoma\",\"sans-serif\";}\n" +
                                                                                        "   .MsoChpDefault\n" +
                                                                                        "   {mso-style-type:export-only;\n" +
                                                                                        "   font-family:\"Calibri\",\"sans-serif\";\n" +
                                                                                        "   mso-fareast-language:EN-US;}\n" +
                                                                                        "   @page WordSection1\n" +
                                                                                        "   {size:612.0pt 792.0pt;\n" +
                                                                                        "   margin:70.85pt 3.0cm 70.85pt 3.0cm;}\n" +
                                                                                        "   div.WordSection1\n" +
                                                                                        "   {page:WordSection1;}\n" +
                                                                                        "</style>\n" +
                                                                                        "</head>\n" +
                                                                                        "<body lang=PT-BR link=blue vlink=purple>\n" +
                                                                                        "<div class=WordSection1>\n" +
                                                                                        "<p class=MsoNormal style='line-height:150%'>A <o:p></o:p></p>\n" +
                                                                                        "<p class=MsoNormal style='line-height:150%'><b>" + cliente.getNome() + "<o:p></o:p></b></p>\n" +
                                                                                        "<p class=MsoNormal style='line-height:150%'><o:p>&nbsp;</o:p></p>\n" +
                                                                                        "<p class=MsoNormal style='line-height:150%'>Bom dia Sr(a) <b>" + nomeResponsavel + ",</b><o:p></o:p></p>\n" +
                                                                                        "<p class=MsoNormal style='line-height:150%'>Gostaríamos de convida-lo a conhecer nossa nova plataforma de atendimento, agora você poderá acessa-la para comprar em qualquer hora. <o:p></o:p></p>\n" +
                                                                                        "<p class=MsoNormal style='line-height:150%'>Como este ano esta sendo muito diferente, e já estamos finalizando nossos pedidos devido a falta de matéria prima e recursos para produzir mais, <b>preferenciamos o seu atendimento.<o:p></o:p></b></p>\n" +
                                                                                        "<p class=MsoNormal style='line-height:150%'>Gostaríamos muito de continuar nossa parceria, <b>mantendo a Marca " + (marca.equals("D") ? "DLZ" : "Flor de Lis") + " em destaque na sua loja e na sua cidade.</b> <o:p></o:p></p>\n" +
                                                                                        "<p class=MsoNormal style='line-height:150%'>Liberamos na plataforma uma <b>seleção de produtos que tiveram ótima aceitação</b> e aprovação de mais de 80 dos clientes Brasil. Assim você poderá comprar com muito mais segurança.<o:p></o:p></p>\n" +
                                                                                        "<p class=MsoNormal style='line-height:150%'>Também para facilitar o seu trabalho e aproveitando nossas informações elaboramos um pedido sugestão que <b>segue em anexo.<o:p></o:p></b></p>\n" +
                                                                                        "<p class=MsoNormal style='line-height:150%'>Quando conseguir analisar, por gentileza nos de um retorno, pois ele estará reservado para seu atendimento por 5 dias. Você poderá confirmar integral ou parcial.<o:p></o:p></p>\n" +
                                                                                        "<p class=MsoNormal style='line-height:150%'>Estamos inovando nosso atendimento e ficaremos a disposição para lhe ajudar. Também se precisar o atendimento do representante sr(a). " + representanteMarca + " só nos informar que ele irá até sua loja.<o:p></o:p></p>\n" +
                                                                                        "<p class=MsoNormal style='line-height:150%'><o:p>&nbsp;</o:p></p>\n" +
                                                                                        "<p class=MsoNormal style='line-height:150%'>Segue seu acesso a Loja<o:p></o:p></p>\n" +
                                                                                        "<p class=MsoNormal style='line-height:150%'><b><a href=\"https://lojista.deliz.com.br\">https://lojista.deliz.com.br</a><o:p></o:p></b></p>\n" +
                                                                                        "<p class=MsoNormal style='line-height:150%'><b>Login: " + cliente.getCnpj() + "<o:p></o:p></b></p>\n" +
                                                                                        "<p class=MsoNormal style='line-height:150%'><b>Senha: " + StringUtils.unformatCpfCnpj(cliente.getCnpj()).substring(0, 7) + "<o:p></o:p></b></p>\n" +
                                                                                        "<p class=MsoNormal style='line-height:150%'><b><o:p>&nbsp;</o:p></b></p>\n" +
                                                                                        "<p class=MsoNormal style='line-height:150%'><b><o:p>&nbsp;</o:p></b></p>\n" +
                                                                                        "<p class=MsoNormal style='line-height:150%'>Atenciosamente,<o:p></o:p></p>\n" +
                                                                                        "<p class=MsoNormal style='line-height:150%'>" + Globals.getUsuarioLogado().getDisplayName() + "<o:p></o:p></p>\n" +
                                                                                        "<p class=MsoNormal style='line-height:150%'>E-mail:" + Globals.getUsuarioLogado().getEmail() + " - Fone: " + Globals.getUsuarioLogado().getTelefone() + "<o:p></o:p></p>\n" +
                                                                                        "<p class=MsoNormal style='line-height:150%'>Deliz Fashion Group.<o:p></o:p></p>\n" +
                                                                                        "</div>\n" +
                                                                                        "</body>\n" +
                                                                                        "</html>";
                                                                            })
                                                                            .send();
                                                                    SysLogger.addSysDelizLog("Digitador Pedido B2B", TipoAcao.CADASTRAR, clienteMagento.getTaxvat(), "Enviado e-mail para o cliente: " + clienteMagento.getFirstname() + " com os carrinhos: " + orders);
                                                                    
                                                                }
                                                                
                                                                MessageBox.create(message -> {
                                                                    message.message("Enviando carrinho para o cliente " + cliente.getNome() + "\nResposta do servidor: " + respostaServidor.getMessage());
                                                                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                                    message.position(Pos.TOP_RIGHT);
                                                                    message.notification();
                                                                });
                                                            } catch (SQLException ex) {
                                                                MessageBox.create(message -> {
                                                                    message.message("Carrinho enviado para a loja para o cliente " + cliente.getNome() + "\nPorém ocorreu um erro na persistência do carrinho no banco.");
                                                                    message.type(MessageBox.TypeMessageBox.ERROR);
                                                                    message.showAndWait();
                                                                });
                                                            } catch (IOException e) {
                                                                e.printStackTrace();
                                                                ExceptionBox.build(message -> {
                                                                    message.exception(e);
                                                                    message.showAndWait();
                                                                });
                                                            } catch (JRException e) {
                                                                e.printStackTrace();
                                                                ExceptionBox.build(message -> {
                                                                    message.exception(e);
                                                                    message.showAndWait();
                                                                });
                                                            }
                                                        }
                                                    }
                                                    limparConfiguracao();
                                                    tabs.getSelectionModel().select(0);
                                                });
                                            }));
                                        }));
                                    }));
                                }));
                            }));
                        })
                );
            }));
        }));
    }
    
    private CustomerStatus convertToCustomerStatus(Entidade entidadeSelecionada) {
        return new CustomerStatus(
                "taxvat",
                entidadeSelecionada.getNome() + " [" + entidadeSelecionada.getCodcli() + "]",
                entidadeSelecionada.getEmail(),
                entidadeSelecionada.getCnpj().replace(".", "").replace("-", "").replace("/", ""),
                entidadeSelecionada.getSdEntidade().isLiberadoB2b() ? entidadeSelecionada.getMarcasEntidade().size() > 0 ? String.valueOf(entidadeSelecionada.getSdEntidade().getSitcliB2b().getCodigo()) : "1" : "1",
                new AdditionalAttribute(entidadeSelecionada.getMarcasEntidade().stream().map(marca -> marca.getId().getMarca().getCodigo().equals("D") ? "92" : "93").collect(Collectors.joining(","))));
    }
    
    private void configurarProdutosPedido() {
        try {
            String marca = marcaPedidoField.validate().value.get().getCodigo();
            String tabPreco = tabPrecoField.validate().value.get().getRegiao();
            gradeCarrinho.clear();
            clientesCarrinho.clear();
            new RunAsyncWithOverlay(this).exec(task -> {
                getProdutos(marca, tabPreco);
                return ReturnAsync.OK.value;
            }).addTaskEndNotification(taskReturn -> {
                if (taskReturn.equals(ReturnAsync.OK.value)) {
                    produtosBean.addAll(FXCollections.observableList(filteredProdutosB2B));
                    limparPaginador(0, QTDE_PRODUTOS_PAGINA);
                    carregarProdutos(produtosBean.get().subList(inicioPaginaProdutos.get(), fimPaginaProdutos.get()));
                    emEdicao.set(true);
                }
            });
        } catch (FormValidationException ex) {
            ex.printStackTrace();
            MessageBox.create(message -> {
                message.message("Existem campos obrigatórios não preenchidos:\n" + ex.getMessage());
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            return;
        }
    }
    
    private void carregarProdutos(List<ProdutoB2B> produtosPaginados) {
        boxProdutos.clear();
        produtosPaginados.forEach(produto -> {
            boxProdutos.add(FormBox.create(boxDetalheProduto -> {
                boxDetalheProduto.vertical();
                boxDetalheProduto.title(produto.getTabPreco().getId().getCodigo().getCodigo());
                boxDetalheProduto.size(400.0, 400.0);
                final FormTableView<VSdMrpEstoqueB2b> infosProdutoEstoque = FormTableView.create(VSdMrpEstoqueB2b.class, table -> {
                    table.expanded();
                    table.items.set(produto.getEstoqueProduto());
                    table.columns(
                            FormTableColumn.create(cln -> {
                                cln.title("Entrega");
                                cln.width(80.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpEstoqueB2b, VSdMrpEstoqueB2b>, ObservableValue<VSdMrpEstoqueB2b>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDescEntrega()));
                            }).build() /*Entrega*/,
                            FormTableColumn.create(cln -> {
                                cln.title("Cor");
                                cln.width(40);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpEstoqueB2b, VSdMrpEstoqueB2b>, ObservableValue<VSdMrpEstoqueB2b>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCor()));
                            }).build() /*Cor*/
                    );
                });
                makeGrade(produto.getTabPreco().getId().getCodigo().getFaixa(), infosProdutoEstoque);
                
                // <editor-fold defaultstate="collapsed" desc="Get Image Produto">
                ImageView imgProduto = new ImageView(noPhoto());
                File files = new File("K:/Loja Virtual/imagens/produtos/" + (produto.getTabPreco().getId().getCodigo().getMarca().toUpperCase()) + "/");
                try {
                    Boolean hasFoto = false;
                    if (files.isDirectory()) {
                        List<File> fotosPasta = Arrays.asList(files.listFiles((dir, name) -> name.contains(produto.getTabPreco().getId().getCodigo().getCodigo()) && (name.toUpperCase().endsWith("_1.JPG") || name.toUpperCase().endsWith("_1.JPEG"))));
                        fotosPasta.sort(Comparator.comparing(File::getName));
                        for (File imagem : fotosPasta) {
                            imgProduto = new ImageView(new Image(new FileInputStream(imagem)));
                            hasFoto = true;
                            break;
                        }
                    }
                    imgProduto.setFitHeight(150.0);
                    imgProduto.setFitWidth(150.0);
                    imgProduto.setPickOnBounds(true);
                    imgProduto.setPreserveRatio(true);
                    if (hasFoto) {
                        imgProduto.setOnMouseClicked(evtMouse -> {
                            if (evtMouse.getClickCount() >= 2) {
                                new Fragment().show(fragment -> {
                                    fragment.title("Imagens do Produto " + produto.getTabPreco().getId().getCodigo().getCodigo());
                                    List<Node> imagesProdutos = new ArrayList<>();
                                    try {
                                        List<File> fotosPasta = Arrays.asList(files.listFiles((dir, name) -> name.contains(produto.getTabPreco().getId().getCodigo().getCodigo())));
                                        fotosPasta.sort(Comparator.comparing(File::getName));
                                        for (File imagem : fotosPasta) {
                                            ImageView imageProduto = null;
                                            imageProduto = new ImageView(new Image(new FileInputStream(imagem)));
                                            imageProduto.setFitHeight(500.0);
                                            imageProduto.setFitWidth(500.0);
                                            imageProduto.setPickOnBounds(true);
                                            imageProduto.setPreserveRatio(true);
                                            imagesProdutos.add(imageProduto);
                                        }
                                    } catch (FileNotFoundException e) {
                                        e.printStackTrace();
                                        ExceptionBox.build(message -> {
                                            message.exception(e);
                                            message.showAndWait();
                                        });
                                    }
                                    fragment.box.getChildren().add(FormCarousel.create(carousel -> {
                                        carousel.setNodesCarousel(imagesProdutos);
                                    }));
                                });
                            }
                        });
                    }
                } catch (FileNotFoundException ex) {
                    imgProduto.setImage(noPhoto());
                }
                ImageView finalImgProduto = imgProduto;
                // </editor-fold>
                boxDetalheProduto.add(new Label(produto.getTabPreco().getId().getCodigo().getDescricao()));
                boxDetalheProduto.add(FormBox.create(layout6 -> {
                    layout6.horizontal();
                    layout6.alignment(Pos.TOP_LEFT);
                    layout6.add(finalImgProduto);
                    layout6.add(FormBox.create(boxInfosProduto -> {
                        boxInfosProduto.vertical();
                        boxInfosProduto.expanded();
                        boxInfosProduto.alignment(Pos.TOP_RIGHT);
                        boxInfosProduto.add(FormBox.create(layout7 -> {
                            layout7.horizontal();
                            layout7.add(FormFieldText.create(field -> {
                                field.title(produto.getTabPreco().getDescricao());
                                field.value.set(StringUtils.toMonetaryFormat(produto.getTabPreco().getPreco00(), 2));
                                field.editable.set(false);
                            }).build());
                            layout7.add(FormBox.create(space -> {
                                space.horizontal();
                                space.expanded();
                            }));
                            layout7.add(FormButton.create(btn -> {
                                btn.title("Adicionar");
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._16));
                                btn.addStyle("success");
                                btn.setAction(event -> {
                                    new Fragment().show(fragment -> {
                                        fragment.title("Digitação de Grade de Produto");
                                        fragment.size(1100.0, 700.0);
                                        
                                        final List<GradeDigitacao> gradeDigitacao = new ArrayList<>();
                                        FXCollections.observableList(produto.getEstoqueProduto().filtered(estoque -> estoque.getQtde() > 0)).forEach(estoque -> {
                                            estoque.getTamanhos().forEach(tams -> {
                                                GradeDigitacao grade = new GradeDigitacao(produto.getTabPreco(), tams.getId().getCor(), estoque.getDescCor(), tams.getId().getTam(), tams.getId().getEntrega(), estoque.getDescEntrega(), 0);
                                                gradeDigitacao.add(grade);
                                            });
                                        });
                                        final IntegerProperty qtdeTotalPecas = new SimpleIntegerProperty();
                                        
                                        final FormFieldSegmentedButton<String> tipoEntregaGradeField = FormFieldSegmentedButton.create(field -> {
                                            field.title("Tipo Entrega");
                                            field.options(
                                                    field.option("Imediato", "0000", FormFieldSegmentedButton.Style.SUCCESS),
                                                    field.option("Programado", "9999", FormFieldSegmentedButton.Style.WARNING)
                                            );
                                            field.select(0);
                                        });
                                        final FormTableView<VSdMrpEstoqueB2b> coresProduto = FormTableView.create(VSdMrpEstoqueB2b.class, table -> {
                                            table.title("Cores Produto");
                                            table.expanded();
                                            table.items.set(FXCollections.observableList(produto.getEstoqueProduto().filtered(estoque -> estoque.getEntrega().equals(tipoEntregaGradeField.value.get()) && estoque.getQtde() > 0)));
                                            table.columns(
                                                    FormTableColumn.create(cln -> {
                                                        cln.title("Código");
                                                        cln.width(50.0);
                                                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpEstoqueB2b, VSdMrpEstoqueB2b>, ObservableValue<VSdMrpEstoqueB2b>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCor()));
                                                        cln.alignment(FormTableColumn.Alignment.CENTER);
                                                    }).build() /*Código*/,
                                                    FormTableColumn.create(cln -> {
                                                        cln.title("Descrição");
                                                        cln.width(150.0);
                                                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpEstoqueB2b, VSdMrpEstoqueB2b>, ObservableValue<VSdMrpEstoqueB2b>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDescCor()));
                                                    }).build() /*Descrição*/,
                                                    FormTableColumn.create(cln -> {
                                                        cln.title("Qtde");
                                                        cln.width(40.0);
                                                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpEstoqueB2b, VSdMrpEstoqueB2b>, ObservableValue<VSdMrpEstoqueB2b>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                                                        cln.alignment(FormTableColumn.Alignment.CENTER);
                                                    }).build() /*Qtde*/
//                                                    FormTableColumn.create(cln -> {
//                                                        cln.title("Tot. Digit.");
//                                                        cln.width(80.0);
//                                                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpEstoqueB2b, VSdMrpEstoqueB2b>, ObservableValue<VSdMrpEstoqueB2b>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtdeDigitado()));
//                                                        cln.alignment(FormTableColumn.Alignment.CENTER);
//                                                    }).build() /*Tot. Digit.*/
                                            );
                                            List<FaixaItem> grade = (List<FaixaItem>) new FluentDao().selectFrom(FaixaItem.class).where(it -> it.equal("faixaItemId.faixa", produto.getTabPreco().getId().getCodigo().getFaixa())).orderBy("posicao", OrderType.ASC).resultList();
                                            grade.forEach(tam -> {
                                                FormTableColumn<VSdMrpEstoqueB2b, VSdMrpEstoqueB2b> clnGrade = FormTableColumn.create(cln -> {
                                                    cln.title(tam.getFaixaItemId().getTamanho());
                                                    cln.alignment(FormTableColumn.Alignment.CENTER);
                                                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpEstoqueB2b, VSdMrpEstoqueB2b>, ObservableValue<VSdMrpEstoqueB2b>>) features -> {
                                                        VSdMrpEstoqueB2b estoque = features.getValue();
                                                        return new ReadOnlyObjectWrapper(estoque);
                                                    });
                                                    cln.format(param -> {
                                                        return new TableCell<VSdMrpEstoqueB2b, VSdMrpEstoqueB2b>() {
                                                            @Override
                                                            protected void updateItem(VSdMrpEstoqueB2b item, boolean empty) {
                                                                super.updateItem(item, empty);
                                                                setText(null);
                                                                setGraphic(null);
                                                                if (item != null && !empty) {
                                                                    //setText(StringUtils.toIntegerFormat(item));
                                                                    VSdEstoqueB2B estoqueTam = item.getTamanho(tam.getFaixaItemId().getTamanho());
                                                                    setGraphic(FormBox.create(boxGrade -> {
                                                                        boxGrade.vertical();
                                                                        boxGrade.add(FormFieldText.create(field -> {
                                                                            GradeDigitacao gradeDigitada = gradeDigitacao.stream()
                                                                                    .filter(tamDigitacao ->
                                                                                            tamDigitacao.getCodigo().getId().getCodigo().getCodigo().equals(estoqueTam.getId().getCodigo()) &&
                                                                                                    tamDigitacao.getCor().equals(estoqueTam.getId().getCor()) &&
                                                                                                    tamDigitacao.getEntrega().equals(estoqueTam.getId().getEntrega()) &&
                                                                                                    tamDigitacao.getTam().equals(estoqueTam.getId().getTam()))
                                                                                    .findFirst().get();
                                                                            field.withoutTitle();
                                                                            field.editable(estoqueTam.getQtd() > 0);
                                                                            field.value.set(String.valueOf(gradeDigitada.getQtde()));
                                                                            field.alignment(FormFieldText.Alignment.CENTER);
                                                                            field.mask(FormFieldText.Mask.INTEGER);
                                                                            field.addStyle("xs");
                                                                            field.width(25.0);
                                                                            field.value.addListener((observable, oldValue, newValue) -> {
                                                                                if (newValue == null || newValue.length() == 0) {
                                                                                    gradeDigitada.setQtde(0);
                                                                                } else {
                                                                                    if (Integer.parseInt(newValue) > estoqueTam.getQtd()) {
                                                                                        gradeDigitada.setQtde(estoqueTam.getQtd());
                                                                                        MessageBox.create(message -> {
                                                                                            message.message("Valor digitado maior que o estoque para cor: " + estoqueTam.getId().getCor() + " e tam: " + estoqueTam.getId().getTam() + ".\nAtribuido total do estoque como quantidade.");
                                                                                            message.type(MessageBox.TypeMessageBox.IMPORTANT);
                                                                                            message.showAndWait();
                                                                                        });
                                                                                    } else {
                                                                                        gradeDigitada.setQtde(Integer.parseInt(newValue));
                                                                                    }
                                                                                }
                                                                                item.setQtdeDigitado(gradeDigitacao.stream()
                                                                                        .filter(tamDigitacao ->
                                                                                                tamDigitacao.getCodigo().equals(estoqueTam.getId().getCodigo()) &&
                                                                                                        tamDigitacao.getCor().equals(estoqueTam.getId().getCor()) &&
                                                                                                        tamDigitacao.getEntrega().equals(estoqueTam.getId().getEntrega())).mapToInt(tamDigitacao -> tamDigitacao.getQtde()).sum());
                                                                                qtdeTotalPecas.set(gradeDigitacao.stream().mapToInt(qtdeDigitada -> qtdeDigitada.getQtde()).sum());
                                                                            });
                                                                        }).build());
                                                                        boxGrade.add(FormFieldText.create(field -> {
                                                                            field.withoutTitle();
                                                                            field.editable(false);
                                                                            field.alignment(FormFieldText.Alignment.CENTER);
                                                                            field.value.set(String.valueOf(estoqueTam.getQtd()));
                                                                            field.addStyle(estoqueTam.getQtd() <= 0 ? "danger" : "").addStyle("xs");
                                                                            field.width(25.0);
                                                                        }).build());
                                                                    }));
                                                                }
                                                            }
                                                        };
                                                    });
                                                });
                                                table.addColumn(clnGrade);
                                            });
                                        });
                                        tipoEntregaGradeField.value.addListener((observable, oldValue, newValue) -> {
                                            if (newValue != null) {
                                                coresProduto.items.set(FXCollections.observableList(produto.getEstoqueProduto().filtered(estoque -> estoque.getEntrega().equals(tipoEntregaGradeField.value.get()) && estoque.getQtde() > 0)));
                                                coresProduto.refresh();
                                            }
                                        });
                                        
                                        fragment.box.getChildren().add(FormBox.create(layout8 -> {
                                            layout8.horizontal();
                                            layout8.expanded();
                                            layout8.add(FormBox.create(dadosProduto -> {
                                                dadosProduto.vertical();
                                                dadosProduto.expanded();
                                                dadosProduto.add(FormFieldText.create(field -> {
                                                    field.withoutTitle();
                                                    field.label("Produto");
                                                    field.value.set(produto.getTabPreco().getId().getCodigo().toString());
                                                    field.editable(false);
                                                }).build());
                                                dadosProduto.add(tipoEntregaGradeField.build());
                                                dadosProduto.add(coresProduto.build());
                                            }));
                                            layout8.add(FormBox.create(fotosProduto -> {
                                                fotosProduto.vertical();
                                                fotosProduto.width(250.0);
                                                List<Node> imagesProdutos = new ArrayList<>();
                                                try {
                                                    List<File> fotosPasta = Arrays.asList(files.listFiles((dir, name) -> name.contains(produto.getTabPreco().getId().getCodigo().getCodigo())));
                                                    fotosPasta.sort(Comparator.comparing(File::getName));
                                                    for (File imagem : fotosPasta) {
                                                        ImageView imageProduto = null;
                                                        imageProduto = new ImageView(new Image(new FileInputStream(imagem)));
                                                        imageProduto.setFitHeight(500.0);
                                                        imageProduto.setFitWidth(500.0);
                                                        imageProduto.setPickOnBounds(true);
                                                        imageProduto.setPreserveRatio(true);
                                                        imagesProdutos.add(imageProduto);
                                                    }
                                                    if (imagesProdutos.size() == 0) {
                                                        ImageView imageProduto = null;
                                                        imageProduto = new ImageView(noPhoto());
                                                        imageProduto.setFitHeight(500.0);
                                                        imageProduto.setFitWidth(500.0);
                                                        imageProduto.setPickOnBounds(true);
                                                        imageProduto.setPreserveRatio(true);
                                                        imagesProdutos.add(imageProduto);
                                                    }
                                                } catch (FileNotFoundException e) {
                                                    e.printStackTrace();
                                                    ExceptionBox.build(message -> {
                                                        message.exception(e);
                                                        message.showAndWait();
                                                    });
                                                }
                                                fotosProduto.add(FormCarousel.create(carousel -> {
                                                    carousel.setNodesCarousel(imagesProdutos);
                                                }));
                                                fotosProduto.add(FormFieldText.create(field -> {
                                                    field.title("Total de Peças");
                                                    field.value.bind(qtdeTotalPecas.asString());
                                                    field.editable(false);
                                                }).build());
                                                fotosProduto.add(FormBox.create(layout9 -> {
                                                    layout9.horizontal();
                                                    layout9.add(FormFieldText.create(field -> {
                                                        field.title("Valor Total");
                                                        field.expanded();
                                                        field.value.bind(Bindings.format("R$ %.2f", qtdeTotalPecas.multiply(produto.getTabPreco().getPreco00().doubleValue())));
                                                        field.editable(false);
                                                        field.alignment(FormFieldText.Alignment.RIGHT);
                                                    }).build());
                                                    layout9.add(FormButton.create(adicionarGrade -> {
                                                        adicionarGrade.title("Adicionar Grade");
                                                        adicionarGrade.addStyle("lg").addStyle("success");
                                                        adicionarGrade.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._32));
                                                        adicionarGrade.setAction(evtAdicionar -> {
                                                            gradeDigitacao.stream().filter(item -> item.getQtde() > 0).forEach(item -> {
                                                                gradeCarrinho.add(new GradeCarrinho(item.getCodigo(), item.getCor(), item.getDescCor(), item.getEntrega(), item.getDescEntrega(), item.getTam(), item.getQtde()));
                                                            });
                                                            qtdePecasCarrinho.set(qtdePecasCarrinho.add(qtdeTotalPecas).intValue());
                                                            valorTotalCarrinho.set(valorTotalCarrinho.add(qtdeTotalPecas.multiply(produto.getTabPreco().getPreco00().doubleValue())).doubleValue());
                                                            fragment.close();
                                                        });
                                                    }));
                                                }));
                                            }));
                                        }));
                                    });
                                });
                            }));
                        }));
                        boxInfosProduto.add(FormLabel.create(labl -> {
                            labl.value.set("Grade: " + produto.getTabPreco().getId().getCodigo().getGrade());
                        }).build());
                        boxInfosProduto.add(FormLabel.create(labl -> {
                            labl.value.set("Família: " + produto.getTabPreco().getId().getCodigo().getFamilia());
                        }).build());
                        boxInfosProduto.add(FormLabel.create(labl -> {
                            labl.value.set("Linha: " + produto.getTabPreco().getId().getCodigo().getLinha());
                        }).build());
                        boxInfosProduto.add(FormLabel.create(labl -> {
                            labl.value.set("Modelagem: " + produto.getTabPreco().getId().getCodigo().getModelagem());
                        }).build());
                    }));
                }));
                boxDetalheProduto.add(infosProdutoEstoque.build());
            }));
        });
    }
    
    private Image noPhoto() {
        File loadNoImage = new File("C:\\SysDelizLocal\\local_files\\no-photo_retrato.jpg");
        try {
            return new Image(new FileInputStream(loadNoImage));
        } catch (FileNotFoundException ex1) {
            ex1.printStackTrace();
            return null;
        }
    }
    
    private void limparConfiguracao() {
        tipoEntregaField.select(0);
        tipoPedidoField.select(0);
        marcaPedidoField.clear();
        colecaoField.clear();
        tabPrecoField.clear();
        boxProdutos.clear();
        produtosBean.clear();
        limparPaginador(0, QTDE_PRODUTOS_PAGINA);
        emEdicao.set(false);
        gradeCarrinho.clear();
        clientesCarrinho.clear();
    }
    
    private void limparFiltroProdutos() {
        produtoField.clear();
        colecaoField.clear();
        familiaField.clear();
        linhaField.clear();
        tipoEntregaField.select(0);
        tipoProdutoField.select(0);
        filteredProdutosB2B.clear();
        filteredProdutosB2B.addAll(produtosB2B);
        produtosBean.set(FXCollections.observableList(filteredProdutosB2B));
        limparPaginador(0, QTDE_PRODUTOS_PAGINA);
        carregarProdutos(produtosBean.get().subList(inicioPaginaProdutos.get(), fimPaginaProdutos.get()));
    }
    
    private void telaConfiguracaoPedido(String toString) {
    }
    
    private void limparTelaConfiguracaoPedido() {
        clientesCarrinho.clear();
    }
    
    private void makeGrade(String faixa, FormTableView<VSdMrpEstoqueB2b> infosProdutoEstoque) {
        if (infosProdutoEstoque.tableProperties().getColumns().size() > 2)
            infosProdutoEstoque.removeColumn(2, infosProdutoEstoque.tableProperties().getColumns().size());
        infosProdutoEstoque.tableProperties().refresh();
        List<FaixaItem> grade = (List<FaixaItem>) new FluentDao().selectFrom(FaixaItem.class).where(it -> it.equal("faixaItemId.faixa", faixa)).orderBy("posicao", OrderType.ASC).resultList();
        grade.forEach(tam -> {
            FormTableColumn<VSdMrpEstoqueB2b, Integer> clnGrade = FormTableColumn.create(cln -> {
                cln.title(tam.getFaixaItemId().getTamanho());
                cln.alignment(FormTableColumn.Alignment.CENTER);
                cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpEstoqueB2b, Integer>, ObservableValue<Integer>>) features -> {
                    VSdEstoqueB2B estoque = features.getValue().getTamanho(tam.getFaixaItemId().getTamanho());
                    return new ReadOnlyObjectWrapper(estoque == null ? 0 : estoque.getQtd());
                });
                cln.format(param -> {
                    return new TableCell<VSdMrpEstoqueB2b, Integer>() {
                        @Override
                        protected void updateItem(Integer item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            clean();
                            
                            if (item != null && !empty) {
                                setText(StringUtils.toIntegerFormat(item));
                                if (item <= 0)
                                    getStyleClass().add("table-row-danger");
                            }
                        }
                        
                        private void clean() {
                            getStyleClass().remove("table-row-danger");
                        }
                    };
                });
            });
            infosProdutoEstoque.addColumn(clnGrade);
        });
    }
    
    private void limparPaginador(int i, int qtde_produtos_pagina) {
        inicioPaginaProdutos.set(i);
        fimPaginaProdutos.set(qtde_produtos_pagina);
    }
    
    
}
