package sysdeliz2.views.comercial.gestaopedido;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import sysdeliz2.controllers.views.comercial.gestaopedidos.MarketingPedidoController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.models.sysdeliz.SdItensPedidoRemessa;
import sysdeliz2.models.sysdeliz.comercial.SdItensMktPedido;
import sysdeliz2.models.sysdeliz.comercial.SdMktPedido;
import sysdeliz2.models.sysdeliz.comercial.SdMktPedidoPK;
import sysdeliz2.models.ti.*;
import sysdeliz2.models.view.*;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.ReportUtils;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class MarketingPedidoView extends MarketingPedidoController {

    // <editor-fold defaultstate="collapsed" desc="beans">
    private final ListProperty<Pedido> pedidosBean = new SimpleListProperty<>(FXCollections.emptyObservableList());
    private final ListProperty<SdItensPedidoRemessa> mktsRemessaBean = new SimpleListProperty<>(FXCollections.emptyObservableList());
    private final ListProperty<VSdMktPedido> mktsRegraBean = new SimpleListProperty<>(FXCollections.emptyObservableList());
    private final ListProperty<SdMktPedido> mktsManualBean = new SimpleListProperty<>(FXCollections.emptyObservableList());
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="components">
    // filter
    private final FormFieldMultipleFind<Entidade> fieldFilterClientes = FormFieldMultipleFind.create(Entidade.class, field -> {
        field.title("Cliente");
        field.width(200.0);
        field.toUpper();
    });
    private final FormFieldMultipleFind<Represen> fieldFilterRepresentantes = FormFieldMultipleFind.create(Represen.class, field -> {
        field.title("Representantes");
        field.width(200.0);
        field.toUpper();
    });
    private final FormFieldMultipleFind<Marca> fieldFilterMarcas = FormFieldMultipleFind.create(Marca.class, field -> {
        field.title("Marca");
        field.width(150.0);
        field.toUpper();
    });
    private final FormFieldMultipleFind<Colecao> fieldFilterColecoes = FormFieldMultipleFind.create(Colecao.class, field -> {
        field.title("Coleção");
        field.width(120.0);
        field.toUpper();
    });
    private final FormFieldMultipleFind<SitCli> fieldFilterSitClis = FormFieldMultipleFind.create(SitCli.class, field -> {
        field.title("Grupo");
        field.width(90.0);
        field.toUpper();
    });
    private final FormFieldDatePeriod fieldFilterEmissaoPedidos = FormFieldDatePeriod.create(field -> {
        field.title("Dt. Emissão");
        field.defaultValue(LocalDate.of(1900, 1, 1), LocalDate.of(2050, 12, 31));
    });
    private final FormFieldText fieldFilterPedidos = FormFieldText.create(field -> {
        field.title("Pedido");
        field.width(200.0);
        field.tooltip("Separe os pedidos por vírgula (,) para consultar mais de um.");
        field.toUpper();
    });
    private final FormFieldSegmentedButton<String> fieldFilterOrigemPedido = FormFieldSegmentedButton.create(field -> {
        field.title("Origem");
        field.options(
                field.option("ERP", "ERP", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("B2B", "B2B", FormFieldSegmentedButton.Style.WARNING),
                field.option("B2C", "B2C", FormFieldSegmentedButton.Style.DANGER),
                field.option("LOJAS", "B2B,B2C", FormFieldSegmentedButton.Style.SECUNDARY),
                field.option("Todos", "T", FormFieldSegmentedButton.Style.SUCCESS)
        );
        field.select(0);
    });
    // dados pedido
    private final FormFieldTextArea fieldObservacaoPedido = FormFieldTextArea.create(field -> {
        field.title("Observações");
        field.editable.set(false);
        field.expanded();
        field.height(70.0);
    });
    private final FormFieldText fieldDataEmissao = FormFieldText.create(field -> {
        field.title("Emissão");
        field.width(80.0);
        field.editable.set(false);
    });
    private final FormFieldText fieldPeriodo = FormFieldText.create(field -> {
        field.title("Período");
        field.width(90.0);
        field.editable.set(false);
    });
    private final FormFieldText fieldTabelaPreco = FormFieldText.create(field -> {
        field.title("Tabela Preço");
        field.width(220.0);
        field.editable.set(false);
    });
    private final FormFieldText fieldOrigem = FormFieldText.create(field -> {
        field.title("Origem");
        field.width(50.0);
        field.alignment(Pos.CENTER);
        field.editable.set(false);
    });
    //tables
    private final FormTableView<Pedido> tblPedidos = FormTableView.create(Pedido.class, table -> {
        table.title("Pedidos");
        table.expanded();
        table.items.bind(pedidosBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Número");
                    cln.width(90.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<Pedido, Pedido>, ObservableValue<Pedido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumero()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Número*/,
                FormTableColumn.create(cln -> {
                    cln.title("Marca");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<Pedido, Pedido>, ObservableValue<Pedido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getSdPedido().getMarca()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Marca*/,
                FormTableColumn.create(cln -> {
                    cln.title("Coleção");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<Pedido, Pedido>, ObservableValue<Pedido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getSdPedido().getColecao()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Coleção*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cliente");
                    cln.width(300.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<Pedido, Pedido>, ObservableValue<Pedido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodcli()));
                }).build() /*Cliente*/,
                FormTableColumn.create(cln -> {
                    cln.title("Grupo");
                    cln.width(40.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<Pedido, Pedido>, ObservableValue<Pedido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodcli().getSitCli().getCodigo()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Grupo*/,
                FormTableColumn.create(cln -> {
                    cln.title("Representante");
                    cln.width(200.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<Pedido, Pedido>, ObservableValue<Pedido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodrep()));
                }).build() /*Representante*/,
                FormTableColumn.create(cln -> {
                    cln.title("Pend.");
                    cln.width(60.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<Pedido, Pedido>, ObservableValue<Pedido>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<Pedido, Pedido>() {
                            @Override
                            protected void updateItem(Pedido item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(String.valueOf(item.getItens().stream().mapToInt(it -> it.getQtde()).sum()));
                                }
                            }
                        };
                    });
                }).build() /*Qtde Pend.*/
        );
        table.factoryRow(param -> {
            return new TableRow<Pedido>() {
                @Override
                protected void updateItem(Pedido item, boolean empty) {
                    super.updateItem(item, empty);
                    getStyleClass().removeAll("table-row-success", "table-row-danger");
                    if (item != null && !empty) {
//                        if (item.getStatus().equals("Cancelado")) {
//                            getStyleClass().add("table-row-danger");
//                        } else if (item.getStatus().equals("Faturado")) {
//                            getStyleClass().add("table-row-success");
//                        }
                    }
                }
            };
        });
        table.indices(
                table.indice("Pendente", "default", ""),
                table.indice("Faturado", "success", ""),
                table.indice("Cancelado", "danger", "")
        );
        table.selectionModelItem((observable, oldValue, newValue) -> {
            if (newValue != null) {
                selecionarPedido((Pedido) newValue);
            }
        });
    });
    private final FormTableView<VSdMktPedido> tblMktsRegras = FormTableView.create(VSdMktPedido.class, table -> {
        table.title("Pendentes");
        table.expanded();
        table.items.bind(mktsRegraBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Produto");
                    cln.width(350.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMktPedido, VSdMktPedido>, ObservableValue<VSdMktPedido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo()));
                }).build() /*Produto*/,
                FormTableColumn.create(cln -> {
                    cln.title("Estoque");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMktPedido, VSdMktPedido>, ObservableValue<VSdMktPedido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getEstoque()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Estoque*/
        );
    });
    private final FormTableView<SdItensPedidoRemessa> tblMktsEnviados = FormTableView.create(SdItensPedidoRemessa.class, table -> {
        table.title("Em Remessa");
        table.expanded();
        table.items.bind(mktsRemessaBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Produto");
                    cln.width(350.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItensPedidoRemessa, SdItensPedidoRemessa>, ObservableValue<SdItensPedidoRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodigo()));
                }).build() /*Produto*/,
                FormTableColumn.create(cln -> {
                    cln.title("Remessa");
                    cln.width(60.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItensPedidoRemessa, SdItensPedidoRemessa>, ObservableValue<SdItensPedidoRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getRemessa()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Remessa*/,
                FormTableColumn.create(cln -> {
                    cln.title("Status");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItensPedidoRemessa, SdItensPedidoRemessa>, ObservableValue<SdItensPedidoRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getStatusitem()));
                    cln.format(param -> {
                        return new TableCell<SdItensPedidoRemessa, String>() {
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                getStyleClass().removeAll("table-row-danger", "table-row-success");
                                if (item != null && !empty) {
                                    setText(item.equals("C") ? "Coletado" : item.equals("X") ? "Cancelado" : "Pendente");
                                    getStyleClass().add(item.equals("C") ? "table-row-success" : item.equals("X") ? "table-row-danger" : "");
                                }
                            }
                        };
                    });
                }).build() /*Status*/,
                FormTableColumn.create(cln -> {
                    cln.title("Qtde");
                    cln.width(40.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItensPedidoRemessa, SdItensPedidoRemessa>, ObservableValue<SdItensPedidoRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Qtde*/
        );
    });
    private final FormTableView<SdMktPedido> tblMktsManuais = FormTableView.create(SdMktPedido.class, table -> {
        table.title("Marketings Cadastrados");
        table.expanded();
        table.items.bind(mktsManualBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Produto");
                    cln.width(400.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMktPedido, SdMktPedido>, ObservableValue<SdMktPedido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodigo()));
                }).build() /*Produto*/,
                FormTableColumn.create(cln -> {
                    cln.title("Remessa");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMktPedido, SdMktPedido>, ObservableValue<SdMktPedido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getReserva()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<SdMktPedido, String>() {
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                getStyleClass().remove("table-row-success");
                                if (item != null && !empty) {
                                    setText(item.equals("0") ? "" : item);
                                    getStyleClass().add(!item.equals("0") ? "table-row-success" : "");
                                }
                            }
                        };
                    });
                }).build() /*Remessa*/,
                FormTableColumn.create(cln -> {
                    cln.title("Qtde");
                    cln.width(40.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMktPedido, SdMktPedido>, ObservableValue<SdMktPedido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Qtde*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cadastro");
                    cln.width(100.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMktPedido, SdMktPedido>, ObservableValue<SdMktPedido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDatacad()));
                    cln.format(param -> {
                        return new TableCell<SdMktPedido, LocalDateTime>() {
                            @Override
                            protected void updateItem(LocalDateTime item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDateTimeFormat(item));
                                }
                            }
                        };
                    });
                }).build() /*Cadastro*/,
                FormTableColumn.create(cln -> {
                    cln.title("Usuário");
                    cln.width(150.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMktPedido, SdMktPedido>, ObservableValue<SdMktPedido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getUsuario()));
                }).build() /*Usuário*/,
                FormTableColumn.create(cln -> {
                    cln.title("Itens");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMktPedido, SdMktPedido>, ObservableValue<SdMktPedido>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<SdMktPedido, SdMktPedido>() {
                            @Override
                            protected void updateItem(SdMktPedido item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    if (!item.getId().getCodigo().getSdProduto().isConfiguravel()) {
                                        setGraphic(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                    } else {
                                        setGraphic(FormButton.create(btnAbrirItens -> {
                                            btnAbrirItens.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                            btnAbrirItens.icon(ImageUtils.getIcon(ImageUtils.Icon.ABRIR, ImageUtils.IconSize._16));
                                            btnAbrirItens.addStyle("xs").addStyle("primary");
                                            btnAbrirItens.setAction(evt -> abrirItensMkt(item));
                                        }));
                                    }
                                }
                            }
                        };
                    });
                }).build() /*Itens*/,
                FormTableColumn.create(cln -> {
                    cln.title("");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(50.0);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMktPedido, SdMktPedido>, ObservableValue<SdMktPedido>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdMktPedido, SdMktPedido>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final FormButton btnExcluirMkt = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                btn.tooltip("Excluir marketing no pedido");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("danger");
                            });

                            @Override
                            protected void updateItem(SdMktPedido item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {

                                    btnExcluirMkt.disable.set(!item.getId().getReserva().equals("0"));
                                    btnExcluirMkt.setOnAction(evt -> {
                                        try {
                                            excluirMarketing(item);
                                        } catch (SQLException e) {
                                            e.printStackTrace();
                                            ExceptionBox.build(message -> {
                                                message.exception(e);
                                                message.showAndWait();
                                            });
                                        }
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnExcluirMkt);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build()
        );
        table.factoryRow(param -> {
            return new TableRow<SdMktPedido>() {
                @Override
                protected void updateItem(SdMktPedido item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item != null && !empty) {
                        setContextMenu(FormContextMenu.create(menu -> {
                            menu.addItem(itemMenu -> {
                                itemMenu.setText("Ver Observações");
                                itemMenu.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.ANEXO, ImageUtils.IconSize._16));
                                itemMenu.setOnAction(evt -> {
                                    new Fragment().show(fragment -> {
                                        fragment.title("Observações marketing pedido");
                                        fragment.size(450.0, 300.0);

                                        fragment.box.getChildren().add(FormBox.create(content -> {
                                            content.vertical();
                                            content.expanded();
                                            content.add(FormFieldTextArea.create(field -> {
                                                field.title("Observações");
                                                field.expanded();
                                                field.value.set(item.getObs());
                                            }).build());
                                        }));
                                    });
                                });
                            });
                        }));
                    }
                }
            };
        });
    });
    // </editor-fold>

    public MarketingPedidoView() {
        super("Marketing de Pedidos", ImageUtils.getImage(ImageUtils.Icon.MARKETING));
        init();
    }

    private void init() {
        super.box.getChildren().add(FormBox.create(content -> {
            content.horizontal();
            content.expanded();
            content.add(FormBox.create(left -> {
                left.vertical();
                left.expanded();
                left.add(FormBox.create(boxFilter -> {
                    boxFilter.horizontal();
                    boxFilter.add(FormTitledPane.create(filter -> {
                        filter.filter();
                        filter.add(FormBox.create(boxFields -> {
                            boxFields.horizontal();
                            boxFields.add(FormBox.create(boxField -> {
                                boxField.vertical();
                                boxField.add(fieldFilterEmissaoPedidos.build());
                                boxField.add(fieldFilterPedidos.build());
                            }));
                            boxFields.add(FormBox.create(boxField -> {
                                boxField.vertical();
                                boxField.add(fieldFilterClientes.build());
                                boxField.add(fieldFilterRepresentantes.build());
                            }));
                            boxFields.add(FormBox.create(boxField -> {
                                boxField.vertical();
                                boxField.add(fieldFilterMarcas.build());
                                boxField.add(FormBox.create(boxField1 -> {
                                    boxField1.horizontal();
                                    boxField1.add(fieldFilterColecoes.build(), fieldFilterSitClis.build());
                                }));
                                boxField.add(fieldFilterOrigemPedido.build());
                            }));
                        }));
                        filter.find.setOnAction(evt -> {
                            buscarPedidos(fieldFilterEmissaoPedidos.valueBegin.get(), fieldFilterEmissaoPedidos.valueEnd.get(),
                                    fieldFilterRepresentantes.objectValues.get(),
                                    fieldFilterMarcas.objectValues.get(),
                                    fieldFilterColecoes.objectValues.get(),
                                    fieldFilterSitClis.objectValues.get(),
                                    fieldFilterClientes.objectValues.get(),
                                    fieldFilterPedidos.value.get(),
                                    fieldFilterOrigemPedido.value.get());
                        });
                        filter.clean.setOnAction(evt -> {
                            fieldFilterPedidos.clear();
                            fieldFilterClientes.clear();
                            fieldFilterMarcas.clear();
                            fieldFilterColecoes.clear();
                            fieldFilterSitClis.clear();
                            fieldFilterRepresentantes.clear();
                            fieldFilterOrigemPedido.select(0);
                            fieldFilterEmissaoPedidos.defaultValue(LocalDate.of(1900, 1, 1), LocalDate.of(2050, 12, 31));

                            pedidosBean.clear();
                            clearInformacoesPedido();
                        });
                    }));
                }));
                left.add(tblPedidos.build());
            }));
            content.add(FormBox.create(right -> {
                right.vertical();
                right.expanded();
                right.title("Marketing do Pedido");
                right.add(FormBox.create(boxAdicionaisPedido -> {
                    boxAdicionaisPedido.horizontal();
                    boxAdicionaisPedido.title("Dados Adicionais do Pedido");
                    boxAdicionaisPedido.add(fieldObservacaoPedido.build());
                    boxAdicionaisPedido.add(FormBox.create(boxFields -> {
                        boxFields.vertical();
                        boxFields.add(FormBox.create(boxFields1 -> {
                            boxFields1.horizontal();
                            boxFields1.add(fieldDataEmissao.build(), fieldOrigem.build(), fieldPeriodo.build());
                        }));
                        boxFields.add(fieldTabelaPreco.build());
                    }));
                }));
                right.add(FormBox.create(boxMarketingsManuais -> {
                    boxMarketingsManuais.vertical();
                    boxMarketingsManuais.expanded();
                    boxMarketingsManuais.title("Marketings Manuais");
                    boxMarketingsManuais.add(FormBox.create(toolbar -> {
                        toolbar.horizontal();
                        toolbar.add(FormButton.create(btnAdicionar -> {
                            btnAdicionar.title("Adicionar");
                            btnAdicionar.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._24));
                            btnAdicionar.addStyle("success");
                            btnAdicionar.disable.bind(tblPedidos.tableProperties().getSelectionModel().selectedItemProperty().isNull());
                            btnAdicionar.setAction(evt -> adicionarMarketing(tblPedidos.selectedItem()));
                        }));
                    }));
                    boxMarketingsManuais.add(tblMktsManuais.build());
                }));
                right.add(FormBox.create(boxMarketingsRegras -> {
                    boxMarketingsRegras.horizontal();
                    boxMarketingsRegras.height(300.0);
                    boxMarketingsRegras.title("Status Marketings");
                    boxMarketingsRegras.add(tblMktsRegras.build(), tblMktsEnviados.build());
                }));
            }));
        }));
    }

    private void adicionarMarketing(Pedido pedido) {
        new Fragment().show(fragment -> {
            fragment.title("Adicionar Marketing no Pedido");
            fragment.size(600.0, 600.0);

            // <editor-fold defaultstate="collapsed" desc="beans">
            final BooleanProperty hasSubItem = new SimpleBooleanProperty(false);
            final ListProperty<SdItensMktPedido> itensMktBean = new SimpleListProperty<>(FXCollections.observableArrayList());
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="components">
            final FormFieldSingleFind<Produto> fieldAddProduto = FormFieldSingleFind.create(Produto.class, field -> {
                field.title("Produto");
                field.width(400.0);
                field.postSelected((observable, oldValue, newValue) -> {
                    if (newValue != null)
                        hasSubItem.set(((Produto) field.value.get()).getSdProduto().isConfiguravel());
                });
            });
            final FormFieldText fieldAddQtde = FormFieldText.create(field -> {
                field.title("Qtde");
                field.addStyle("lg");
                field.width(60.0);
                field.alignment(Pos.CENTER);
                field.value.set(String.valueOf(1));
                field.mask(FormFieldText.Mask.INTEGER);
            });
            final FormFieldTextArea fieldAddObservacao = FormFieldTextArea.create(field -> {
                field.title("Observações");
//                field.height(70.0);
                field.expanded();
                field.minHeight(70.0);
            });
            final FormFieldSingleFind<Produto> fieldAddProdutoSubItem = FormFieldSingleFind.create(Produto.class, field -> {
                field.title("Produto");
                field.width(350.0);
            });
            final FormTableView<SdItensMktPedido> tblItensMkt = FormTableView.create(SdItensMktPedido.class, table -> {
                table.title("Itens");
                table.expanded();
                table.items.bind(itensMktBean);
                table.columns(
                        FormTableColumn.create(cln -> {
                            cln.title("Código");
                            cln.width(200.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdItensMktPedido, SdItensMktPedido>, ObservableValue<SdItensMktPedido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getProduto()));
                        }).build() /*Código*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Cor");
                            cln.width(60.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdItensMktPedido, SdItensMktPedido>, ObservableValue<SdItensMktPedido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCor()));
                            cln.alignment(Pos.CENTER);
                        }).build() /*Cor*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Tam");
                            cln.width(40.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdItensMktPedido, SdItensMktPedido>, ObservableValue<SdItensMktPedido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getTam()));
                            cln.alignment(Pos.CENTER);
                        }).build() /*Tam*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Qtde");
                            cln.width(40.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdItensMktPedido, SdItensMktPedido>, ObservableValue<SdItensMktPedido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                            cln.alignment(Pos.CENTER);
                        }).build() /*Qtde*/
                );
            });
            // </editor-fold>

            fragment.box.getChildren().add(FormBox.create(content -> {
                content.vertical();
                content.expanded();
                content.add(FormBox.create(boxDadosProduto -> {
                    boxDadosProduto.horizontal();
                    boxDadosProduto.add(fieldAddProduto.build(), fieldAddQtde.build());
                }));
                content.add(fieldAddObservacao.build());
                content.add(FormTitledPane.create(tpane -> {
                    tpane.title("Adicionar Itens");
                    tpane.collapsabled.bind(hasSubItem);
                    tpane.expanded();
                    tpane.add(FormBox.create(contentSubItem -> {
                        contentSubItem.vertical();
                        contentSubItem.expanded();
                        contentSubItem.add(FormBox.create(boxDadosProduto -> {
                            boxDadosProduto.horizontal();
                            boxDadosProduto.add(fieldAddProdutoSubItem.build());
                            boxDadosProduto.add(FormButton.create(btnAdicionar -> {
                                btnAdicionar.title("Add. Grade");
                                btnAdicionar.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._32));
                                btnAdicionar.addStyle("lg").addStyle("success");
                                btnAdicionar.setAction(evt -> {
                                    if (fieldAddProdutoSubItem.value.get() != null)
                                        new Fragment().show(frgAddGrade -> {
                                            frgAddGrade.title("Digitação de Grade de Produto");
                                            frgAddGrade.size(1100.0, 700.0);

                                            Produto produtoAdicionado = fieldAddProdutoSubItem.value.get();
                                            File files = new File("K:/Loja Virtual/imagens/produtos/" + (produtoAdicionado.getMarca().getDescricao().toUpperCase()) + "/");
                                            List<VSdMrpEstoqueB2b> estoqueProduto = (List<VSdMrpEstoqueB2b>) new FluentDao().selectFrom(VSdMrpEstoqueB2b.class)
                                                    .where(eb -> eb.equal("codigo", produtoAdicionado.getCodigo())).resultList();

                                            final List<GradeDigitacao> gradeDigitacao = new ArrayList<>();
                                            estoqueProduto.stream().forEach(estoque -> {
                                                estoque.getTamanhos().forEach(tams -> {
                                                    GradeDigitacao grade = new GradeDigitacao(tams.getId().getCor(), estoque.getDescCor(), tams.getId().getTam(), tams.getId().getEntrega(), estoque.getDescEntrega(), 0);
                                                    gradeDigitacao.add(grade);
                                                });
                                            });
                                            final IntegerProperty qtdeTotalPecas = new SimpleIntegerProperty();
                                            final FormTableView<VSdMrpEstoqueB2b> coresProduto = FormTableView.create(VSdMrpEstoqueB2b.class, table -> {
                                                table.title("Cores Produto");
                                                table.expanded();
                                                table.items.set(FXCollections.observableList(estoqueProduto.stream().filter(estoque -> estoque.getEntrega().equals("0000")).collect(Collectors.toList())));
                                                table.columns(
                                                        FormTableColumn.create(cln -> {
                                                            cln.title("Código");
                                                            cln.width(50.0);
                                                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpEstoqueB2b, VSdMrpEstoqueB2b>, ObservableValue<VSdMrpEstoqueB2b>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCor()));
                                                            cln.alignment(FormTableColumn.Alignment.CENTER);
                                                        }).build() /*Código*/,
                                                        FormTableColumn.create(cln -> {
                                                            cln.title("Descrição");
                                                            cln.width(150.0);
                                                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpEstoqueB2b, VSdMrpEstoqueB2b>, ObservableValue<VSdMrpEstoqueB2b>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDescCor()));
                                                        }).build() /*Descrição*///,
//                                                        FormTableColumn.create(cln -> {
//                                                            cln.title("Qtde");
//                                                            cln.width(40.0);
//                                                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpEstoqueB2b, VSdMrpEstoqueB2b>, ObservableValue<VSdMrpEstoqueB2b>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
//                                                            cln.alignment(FormTableColumn.Alignment.CENTER);
//                                                        }).build() /*Qtde*/
                                                );
                                                List<FaixaItem> grade = (List<FaixaItem>) new FluentDao().selectFrom(FaixaItem.class).where(it -> it.equal("faixaItemId.faixa", produtoAdicionado.getFaixa().getCodigo())).orderBy("posicao", OrderType.ASC).resultList();
                                                grade.forEach(tam -> {
                                                    FormTableColumn<VSdMrpEstoqueB2b, VSdMrpEstoqueB2b> clnGrade = FormTableColumn.create(cln -> {
                                                        cln.title(tam.getFaixaItemId().getTamanho());
                                                        cln.alignment(FormTableColumn.Alignment.CENTER);
                                                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpEstoqueB2b, VSdMrpEstoqueB2b>, ObservableValue<VSdMrpEstoqueB2b>>) features -> {
                                                            VSdMrpEstoqueB2b estoque = features.getValue();
                                                            return new ReadOnlyObjectWrapper(estoque);
                                                        });
                                                        cln.format(param -> {
                                                            return new TableCell<VSdMrpEstoqueB2b, VSdMrpEstoqueB2b>() {
                                                                @Override
                                                                protected void updateItem(VSdMrpEstoqueB2b item, boolean empty) {
                                                                    super.updateItem(item, empty);
                                                                    setText(null);
                                                                    setGraphic(null);
                                                                    if (item != null && !empty) {
                                                                        //setText(StringUtils.toIntegerFormat(item));
                                                                        VSdEstoqueB2B estoqueTam = item.getTamanho(tam.getFaixaItemId().getTamanho());
                                                                        setGraphic(FormBox.create(boxGrade -> {
                                                                            boxGrade.vertical();
                                                                            boxGrade.add(FormFieldText.create(field -> {
                                                                                GradeDigitacao gradeDigitada = new GradeDigitacao("-",
                                                                                        "-", tam.getFaixaItemId().getTamanho(),
                                                                                        "-", "-", 0);
                                                                                if (estoqueTam != null) {
                                                                                    gradeDigitada = gradeDigitacao.stream()
                                                                                            .filter(tamDigitacao ->
                                                                                                    tamDigitacao.getCor().equals(estoqueTam.getId().getCor()) &&
                                                                                                            tamDigitacao.getEntrega().equals(estoqueTam.getId().getEntrega()) &&
                                                                                                            tamDigitacao.getTam().equals(estoqueTam.getId().getTam()))
                                                                                            .peek(gradeDigitacao1 -> System.out.println(gradeDigitacao1.getCor().concat("-").concat(gradeDigitacao1.getTam()).concat(":").concat(gradeDigitacao1.getEntrega())
                                                                                                    .concat("|").concat(estoqueTam.getId().getCor())))
                                                                                            .findFirst()
                                                                                            .orElse(new GradeDigitacao(estoqueTam.getId().getCor(), "-", estoqueTam.getId().getTam(), estoqueTam.getId().getEntrega(), "-", 0));
                                                                                }
                                                                                field.withoutTitle();
                                                                                field.value.set(String.valueOf(gradeDigitada.getQtde()));
                                                                                field.alignment(FormFieldText.Alignment.CENTER);
                                                                                field.mask(FormFieldText.Mask.INTEGER);
                                                                                field.addStyle("xs");
                                                                                field.width(25.0);
                                                                                GradeDigitacao finalGradeDigitada = gradeDigitada;
                                                                                field.value.addListener((observable, oldValue, newValue) -> {
                                                                                    if (newValue == null || newValue.length() == 0) {
                                                                                        finalGradeDigitada.setQtde(0);
                                                                                    } else {
                                                                                        finalGradeDigitada.setQtde(Integer.parseInt(newValue));
                                                                                    }
                                                                                    item.setQtdeDigitado(gradeDigitacao.stream()
                                                                                            .filter(tamDigitacao ->
                                                                                                    tamDigitacao.getCor().equals(estoqueTam.getId().getCor()) &&
                                                                                                            tamDigitacao.getEntrega().equals(estoqueTam.getId().getEntrega())).mapToInt(tamDigitacao -> tamDigitacao.getQtde()).sum());
                                                                                    qtdeTotalPecas.set(gradeDigitacao.stream().mapToInt(qtdeDigitada -> qtdeDigitada.getQtde()).sum());
                                                                                });
                                                                            }).build());
//                                                                            boxGrade.add(FormFieldText.create(field -> {
//                                                                                field.withoutTitle();
//                                                                                field.editable(false);
//                                                                                field.alignment(FormFieldText.Alignment.CENTER);
//                                                                                field.value.set(String.valueOf(estoqueTam.getQtd()));
//                                                                                field.addStyle(estoqueTam.getQtd() <= 0 ? "danger" : "").addStyle("xs");
//                                                                                field.width(25.0);
//                                                                            }).build());
                                                                        }));
                                                                    }
                                                                }
                                                            };
                                                        });
                                                    });
                                                    table.addColumn(clnGrade);
                                                });
                                            });

                                            frgAddGrade.box.getChildren().add(FormBox.create(dadosDoProduto -> {
                                                dadosDoProduto.horizontal();
                                                dadosDoProduto.expanded();
                                                dadosDoProduto.add(FormBox.create(dadosProduto -> {
                                                    dadosProduto.vertical();
                                                    dadosProduto.expanded();
                                                    dadosProduto.add(FormFieldText.create(field -> {
                                                        field.withoutTitle();
                                                        field.label("Produto");
                                                        field.value.set(produtoAdicionado.toString());
                                                        field.editable(false);
                                                    }).build());
                                                    dadosProduto.add(coresProduto.build());
                                                }));
                                                dadosDoProduto.add(FormBox.create(fotosProduto -> {
                                                    fotosProduto.vertical();
                                                    fotosProduto.width(250.0);
                                                    List<Node> imagesProdutos = new ArrayList<>();
                                                    try {
                                                        List<File> fotosPasta = Arrays.asList(files.listFiles((dir, name) -> name.contains(produtoAdicionado.getCodigo())));
                                                        fotosPasta.sort(Comparator.comparing(File::getName));
                                                        for (File imagem : fotosPasta) {
                                                            ImageView imageProduto = null;
                                                            imageProduto = new ImageView(new Image(new FileInputStream(imagem)));
                                                            imageProduto.setFitHeight(500.0);
                                                            imageProduto.setFitWidth(500.0);
                                                            imageProduto.setPickOnBounds(true);
                                                            imageProduto.setPreserveRatio(true);
                                                            imagesProdutos.add(imageProduto);
                                                            break;
                                                        }
                                                        if (imagesProdutos.size() == 0) {
                                                            ImageView imageProduto = null;
                                                            imageProduto = new ImageView(noPhoto());
                                                            imageProduto.setFitHeight(500.0);
                                                            imageProduto.setFitWidth(500.0);
                                                            imageProduto.setPickOnBounds(true);
                                                            imageProduto.setPreserveRatio(true);
                                                            imagesProdutos.add(imageProduto);
                                                        }
                                                    } catch (FileNotFoundException e) {
                                                        e.printStackTrace();
                                                        ExceptionBox.build(message -> {
                                                            message.exception(e);
                                                            message.showAndWait();
                                                        });
                                                    }
                                                    fotosProduto.add(FormCarousel.create(carousel -> {
                                                        carousel.setNodesCarousel(imagesProdutos);
                                                    }));
                                                    fotosProduto.add(FormFieldText.create(field -> {
                                                        field.title("Total de Peças");
                                                        field.value.bind(qtdeTotalPecas.asString());
                                                        field.editable(false);
                                                    }).build());
                                                    fotosProduto.add(FormBox.create(toolbarGrade -> {
                                                        toolbarGrade.horizontal();
                                                        toolbarGrade.add(FormButton.create(adicionarGrade -> {
                                                            adicionarGrade.title("Adicionar Grade");
                                                            adicionarGrade.addStyle("lg").addStyle("success");
                                                            adicionarGrade.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._32));
                                                            adicionarGrade.setAction(evtAdicionar -> {
                                                                gradeDigitacao.stream().filter(item -> item.getQtde() > 0).forEach(item -> {
                                                                    itensMktBean.get().add(new SdItensMktPedido(pedido.getNumero(), fieldAddProduto.value.get().getCodigo(), "0", produtoAdicionado, item.getCor(), item.getTam(), item.getQtde()));
                                                                });
                                                                frgAddGrade.close();
                                                                fieldAddProdutoSubItem.clear();
                                                            });
                                                        }));
                                                    }));
                                                }));
                                            }));
                                        });
                                });
                            }));
                        }));
                        contentSubItem.add(tblItensMkt.build());
                    }));
                }));
            }));
            fragment.buttonsBox.getChildren().add(FormButton.create(btnAdicionarMkt -> {
                btnAdicionarMkt.title("Adicionar");
                btnAdicionarMkt.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._24));
                btnAdicionarMkt.addStyle("success");
                btnAdicionarMkt.setAction(evt -> {
                    try {
                        if (adicionarMarketing(pedido.getNumero(), fieldAddProduto.value.get(), Integer.parseInt(fieldAddQtde.value.get()), fieldAddObservacao.value.get(), itensMktBean.get())) {
                            fragment.close();
                            SysLogger.addSysDelizLog("Marketing Pedido", TipoAcao.CADASTRAR, pedido.getNumero(), "Incluído marketing " + fieldAddProduto.value.get().getCodigo() + " no pedido.");
                            MessageBox.create(message -> {
                                message.message("Marketing cadastrado com sucesso no pedido.");
                                message.type(MessageBox.TypeMessageBox.CONFIRM);
                                message.position(Pos.TOP_RIGHT);
                                message.notification();
                            });
                        }
                    } catch (SQLException | JRException | IOException e) {
                        e.printStackTrace();
                        ExceptionBox.build(message -> {
                            message.exception(e);
                            message.showAndWait();
                        });
                    }
                });
            }));
        });
    }

    private boolean adicionarMarketing(String pedido, Produto produto, int qtde, String observacao, ObservableList<SdItensMktPedido> itens) throws SQLException, JRException, IOException {

        if (produto.getSdProduto().isConfiguravel() && itens.size() == 0) {
            MessageBox.create(message -> {
                message.message("Você não incluiu os itens para o marketing selecionado.");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showAndWait();
            });
            return false;
        }

        VSdDadosProdutoBarra produtoEstoque = new FluentDao().selectFrom(VSdDadosProdutoBarra.class)
                .where(eb -> eb
                        .equal("codigo", produto.getCodigo())
                        .equal("cor", "UN")
                        .equal("tam", "UN"))
                .singleResult();
        if (produtoEstoque == null) {
            MessageBox.create(message -> {
                message.message("Marketing sem barra cadastrada.");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showAndWait();
            });
            return false;
        }
        if (produtoEstoque.getBarra() == null || produtoEstoque.getBarra28() == null || produtoEstoque.getLocal() == null) {
            MessageBox.create(message -> {
                message.message("O marketing selecionado não está com o cadastro completo, verifique o cadastro:\nBarra 28: " + produtoEstoque.getBarra28() + "\nLocal: " + produtoEstoque.getLocal() + "\nBarra: " + produtoEstoque.getBarra());
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showAndWait();
            });
            return false;
        }

        SdMktPedido mktPedido = new SdMktPedido();
        mktPedido.setDatacad(LocalDateTime.now());
        mktPedido.setUsuario(Globals.getUsuarioLogado().getUsuario());
        mktPedido.setQtde(qtde);
        mktPedido.setObs(observacao);

        VSdDadosProduto produtoMkt = new FluentDao().selectFrom(VSdDadosProduto.class).where(eb -> eb.equal("codigo", produto.getCodigo())).singleResult();
        mktPedido.setId(new SdMktPedidoPK(pedido, produtoMkt, "0"));

        // criando as barras do produto
        List<String> ordemBarra = new ArrayList<>();
        List<SdMktPedido> mktsPedido = (List<SdMktPedido>) new FluentDao().selectFrom(SdMktPedido.class).where(eb -> eb.equal("id.pedido", pedido).equal("id.codigo.codigo", produto.getCodigo())).resultList();
        for (int i = 1; i <= qtde; i++) {
            ordemBarra.add(StringUtils.lpad(mktsPedido.stream().mapToInt(mkt -> mkt.getQtde()).sum() + i, 2, "0"));
        }

        for (SdItensMktPedido item : itens)
            new FluentDao().persist(item);

        mktPedido = new FluentDao().persist(mktPedido);
        JPAUtils.getEntityManager().refresh(mktPedido);

        if (produto.getSdProduto().isConfiguravel()) {
            entradaMktEstoque(mktPedido, produtoEstoque);
            SysLogger.addSysDelizLog("Marketing Pedido", TipoAcao.CADASTRAR, produto.getCodigo(), "Incluído produto no estoque com movimentação.");
        }

        if (itens.size() > 0) {
            if (produto.getSdProduto().isBaixaEstoque())
                for (SdItensMktPedido item : itens)
                    saidaProdutoEstoque(item, qtde);

            SysLogger.addSysDelizLog("Marketing Pedido", TipoAcao.CADASTRAR, pedido, "Incluído os itens " + itens.stream().map(item -> item.getId().getProduto().getCodigo()).distinct().collect(Collectors.joining(", ")) +
                    " no marketing " + produto.getCodigo() + " no pedido.");

            for (String barra : ordemBarra) {
                new ReportUtils().config()
                        .addReport(ReportUtils.ReportFile.ESPELHO_MARKETING,
                                new ReportUtils.ParameterReport("p_pedido", pedido),
                                new ReportUtils.ParameterReport("p_codigo", produto.getCodigo()),
                                new ReportUtils.ParameterReport("p_ordem", barra))
                        .view().printWithDialog();
            }
        }

        mktsManualBean.add(mktPedido);
        tblMktsManuais.refresh();

        return true;
    }

    private void buscarPedidos(LocalDate inicioEmissao, LocalDate fimEmissao,
                               ObservableList<Represen> represen, ObservableList<Marca> marca, ObservableList<Colecao> colecao,
                               ObservableList<SitCli> sitCli, ObservableList<Entidade> entidade, String pedido, String origem) {

        Object[] representantes = represen.stream().map(Represen::getCodRep).toArray();
        Object[] marcas = marca.stream().map(Marca::getCodigo).toArray();
        Object[] colecoes = colecao.stream().map(Colecao::getCodigo).toArray();
        Object[] sitclis = sitCli.stream().map(SitCli::getCodigo).toArray();
        Object[] clientes = entidade.stream().map(Entidade::getCodcli).toArray();
        Object[] pedidos = pedido == null ? new Object[]{} : pedido.split(",");
        Object[] origens = origem.equals("T") ? new Object[]{} : origem.split(",");

        clearInformacoesPedido();

        new RunAsyncWithOverlay(this).exec(task -> {
            getPedidos(inicioEmissao, fimEmissao, marcas, colecoes, representantes, sitclis, clientes, pedidos, origens);
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                pedidosBean.set(FXCollections.observableList(super.pedidos));
            }
        });
    }

    private void clearInformacoesPedido() {
        fieldObservacaoPedido.clear();
        fieldDataEmissao.clear();
        fieldPeriodo.clear();
        fieldTabelaPreco.clear();
        fieldOrigem.clear();

        mktsManualBean.clear();
        mktsRegraBean.clear();
        mktsRemessaBean.clear();
    }

    private void selecionarPedido(Pedido pedido) {
        clearInformacoesPedido();

        new RunAsyncWithOverlay(this).exec(task -> {
            getMktsPedido(pedido.getNumero());

            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                fieldObservacaoPedido.value.set(pedido.getObs());
                fieldDataEmissao.value.set(StringUtils.toDateFormat(pedido.getDt_emissao()));
                fieldPeriodo.value.set(pedido.getPeriodo().getPrazo());
                fieldTabelaPreco.value.set(pedido.getTabPre().toString());
                fieldOrigem.value.set(pedido.getSdPedido().getOrigem());

                mktsManualBean.set(FXCollections.observableList(super.mktsManual));
                mktsRegraBean.set(FXCollections.observableList(super.mktsRegra));
                mktsRemessaBean.set(FXCollections.observableList(super.mktsRemessa));
            }
        });


    }

    private void abrirItensMkt(SdMktPedido item) {
        new Fragment().show(fragment -> {
            fragment.title("Itens do marketing do pedido");
            fragment.size(450.0, 300.0);

            final FormTableView<SdItensMktPedido> tblItensMkt = FormTableView.create(SdItensMktPedido.class, table -> {
                table.title("Itens");
                table.expanded();
                table.items.set(FXCollections.observableList(item.getItens()));
                table.columns(
                        FormTableColumn.create(cln -> {
                            cln.title("Código");
                            cln.width(200.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdItensMktPedido, SdItensMktPedido>, ObservableValue<SdItensMktPedido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getProduto()));
                        }).build() /*Código*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Cor");
                            cln.width(60.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdItensMktPedido, SdItensMktPedido>, ObservableValue<SdItensMktPedido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCor()));
                            cln.alignment(Pos.CENTER);
                        }).build() /*Cor*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Tam");
                            cln.width(40.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdItensMktPedido, SdItensMktPedido>, ObservableValue<SdItensMktPedido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getTam()));
                            cln.alignment(Pos.CENTER);
                        }).build() /*Tam*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Qtde");
                            cln.width(40.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdItensMktPedido, SdItensMktPedido>, ObservableValue<SdItensMktPedido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                            cln.alignment(Pos.CENTER);
                        }).build() /*Qtde*/
                );
            });
            fragment.box.getChildren().add(FormBox.create(content -> {
                content.vertical();
                content.add(tblItensMkt.build());
            }));
        });
    }

    private void excluirMarketing(SdMktPedido mktPedido) throws SQLException {
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Deseja realmente excluir o marketing do pedido?");
            message.showAndWait();
        }).value.get())) {

            for (SdItensMktPedido item : mktPedido.getItens()) {
                entradaProdutoEstoque(item, mktPedido.getQtde());
                new FluentDao().delete(item);
            }
            mktPedido.getItens().clear();

            saidaMktEstoque(mktPedido);
            new FluentDao().delete(mktPedido);
            SysLogger.addSysDelizLog("Marketing Pedido", TipoAcao.EXCLUIR, mktPedido.getId().getPedido(), "Excluído o marketing " + mktPedido.getId().getCodigo().getCodigo() + " do pedido.");
            MessageBox.create(message -> {
                message.message("Marketing excluído do pedido.");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            mktsManualBean.remove(mktPedido);
            tblMktsManuais.refresh();
        }
    }

    private Image noPhoto() {
        File loadNoImage = new File("C:\\SysDelizLocal\\local_files\\no-photo_retrato.jpg");
        try {
            return new Image(new FileInputStream(loadNoImage));
        } catch (FileNotFoundException ex1) {
            ex1.printStackTrace();
            return null;
        }
    }
}
