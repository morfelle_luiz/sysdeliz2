package sysdeliz2.views.comercial.relatorios;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import sysdeliz2.controllers.views.comercial.relatorios.AtendimentoCarteiraController;
import sysdeliz2.daos.DAOFactory;
import sysdeliz2.models.ti.*;
import sysdeliz2.models.view.VSdAtendCarteiraRep;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.ReportUtils;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.sys.ExportUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class AtendimentoCarteiraView extends AtendimentoCarteiraController {

    private final double columnValorWidth = 80;
    private String[] colecoes;
    private HashMap<String, BigDecimal> colecoesTotais = new HashMap<>();

    // <editor-fold defaultstate="collapsed" desc="Totais">

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Excel ExportMode">
    protected ExportUtils.ExcelExportMode excelExportMode;
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Bean">
    private final ListProperty<VSdAtendCarteiraRep> clientesBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<String> list = new SimpleListProperty<>(FXCollections.observableArrayList());
    private List<ReportUtils.ShowReport> jaspers = new ArrayList<>();
    private HashMap<String, ReportUtils.ShowReport> mapJaspers = new HashMap<>();
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Button">
    private Button exportExcelBtn = FormButton.create(btn -> {
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EXCEL, ImageUtils.IconSize._24));
        btn.setText("Exportar Excel");
        btn.addStyle("success");
        btn.setOnAction(evt -> {
            if (!clientesBean.isEmpty()) {
                ArrayList list = selecionarLayout();
                if (list.size() > 0) {
                    String path = selecionarPathArquivo(new FileChooser.ExtensionFilter("Arquivos do Excel (.xlsx)", "*.xlsx"));
                    if (!path.equals("")) exportarExcelPadrao(path, list);
                }
            }

        });
    });

    private Button exportExcelVariableBtn = FormButton.create(btn -> {
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EXCEL, ImageUtils.IconSize._24));
        btn.setText("Exportar Excel");
        btn.addStyle("success");
        btn.setOnAction(evt -> {
            if (!clientesBean.isEmpty()) {
                String path = selecionarPathArquivo(new FileChooser.ExtensionFilter("Arquivos do Excel (.xlsx)", "*.xlsx"));
                if (!path.equals("")) exportarExcelEditable(path);
            }
        });
    });

    private Button printJasperBtn = FormButton.create(btn -> {
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.IMPRIMIR, ImageUtils.IconSize._24));
        btn.setText("Imprimir");
        btn.addStyle("info");
        btn.setOnAction(evt -> {
            if (prepararJaspers()) {
                jaspers.forEach(it -> {
                    try {
                        it.print();
                    } catch (IOException | JRException e) {
                        e.printStackTrace();
                    }
                });
            }
        });
    });

    private Button visualizarJasperBtn = FormButton.create(btn -> {
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.VISUALIZAR_PEDIDO, ImageUtils.IconSize._24));
        btn.setText("Visualizar");
        btn.addStyle("warning");
        btn.setOnAction(evt -> {
            if (prepararJaspers()) {
                jaspers.forEach(ReportUtils.ShowReport::toView);
                MessageBox.create(message -> {
                    message.message("Relatórios gerados com sucesso!");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            }
        });
    });

    private Button salvarPDFJasperBtn = FormButton.create(btn -> {
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.PDF, ImageUtils.IconSize._24));
        btn.setText("Exportar PDF");
        btn.addStyle("danger");
        btn.setOnAction(evt -> {
            if (prepararJaspers()) {
                if (jaspers.size() == 1) {
                    try {
                        jaspers.get(0).toPdf(selecionarPathArquivo(new FileChooser.ExtensionFilter("Arquivos PDF (.pdf)", "*.pdf")));
                    } catch (IOException | JRException e) {
                        e.printStackTrace();
                    }
                } else {
                    String directoryPath = new DirectoryChooser().showDialog(Globals.getMainStage()).getAbsolutePath().concat("\\Relatorio Atendimento de Carteira - Layout ");
                    try {
                        if (mapJaspers.get("padrao") != null)
                            mapJaspers.get("padrao").toPdf(directoryPath + "Padrão.pdf");
                        if (mapJaspers.get("canc") != null)
                            mapJaspers.get("canc").toPdf(directoryPath + "Valores + Cancelamento.pdf");
                        if (mapJaspers.get("val") != null) mapJaspers.get("val").toPdf(directoryPath + "Valores.pdf");
                        if (mapJaspers.get("marca") != null) mapJaspers.get("marca").toPdf(directoryPath + "Marca.pdf");
                        if (mapJaspers.get("rep") != null)
                            mapJaspers.get("rep").toPdf(directoryPath + "Representante.pdf");
                        if (mapJaspers.get("17co") != null)
                            mapJaspers.get("17co").toPdf(directoryPath + "ValorContinuo.pdf");
                    } catch (IOException | JRException e) {
                        e.printStackTrace();
                    }
                }
                MessageBox.create(message -> {
                    message.message("Relatórios gerados com sucesso!");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            }
        });
    });

    private FormFieldToggle cancelamentoToggle = FormFieldToggle.create(field -> {
        field.title("Com Cancelamento");
        field.value.setValue(false);
    });

    private FormFieldToggle representanteToggle = FormFieldToggle.create(field -> {
        field.title("Com Representante");
        field.value.setValue(false);
    });

    private FormFieldToggle marcaToggle = FormFieldToggle.create(field -> {
        field.title("Com Marca");
        field.value.setValue(false);
    });

    private FormFieldToggle valores17COToggle = FormFieldToggle.create(field -> {
        field.title("Com Valores 17CO");
        field.value.setValue(false);
    });

    private FormFieldToggle totaisToggle = FormFieldToggle.create(field -> {
        field.title("Com Totais");
        field.value.setValue(false);
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Table">
    private final FormTableView<VSdAtendCarteiraRep> tblClientesPadrao = FormTableView.create(VSdAtendCarteiraRep.class, table -> {
        table.title("Clientes");
        table.items.bind(clientesBean);
        table.height(630);
        table.expanded();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Cód.");
                    cln.width(60);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>) param -> new ReadOnlyObjectWrapper(String.format("%05d", param.getValue().getCodcli().getCodcli())));

                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Perfil");
                    cln.width(40);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getSitCli()));
                    cln.format(param -> {
                        return new TableCell<VSdAtendCarteiraRep, String>() {

                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                clear();
                                if (item != null && !empty) {
                                    setText(item);
                                    if (item.equals("1")) getStyleClass().add("sit-cli-1");
                                    if (item.equals("IN")) getStyleClass().add("sit-cli-in");
                                    if (item.equals("G1")) getStyleClass().add("sit-cli-g1");
                                    if (item.equals("G2")) getStyleClass().add("sit-cli-g2");
                                    if (item.equals("G3")) getStyleClass().add("sit-cli-g3");
                                    if (item.equals("G4")) getStyleClass().add("sit-cli-g4");
                                    if (item.equals("G5")) getStyleClass().add("sit-cli-g5");
                                }
                            }

                            private void clear() {
                                getStyleClass().removeAll("sit-cli-1", "sit-cli-in", "sit-cli-g1", "sit-cli-g2", "sit-cli-g3", "sit-cli-g4", "sit-cli-g5");
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Grupo");
                    cln.width(40);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getGrupo()));

                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Cliente");
                    cln.width(350);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodcli().getNome()));

                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Fantasia");
                    cln.width(250);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodcli().getFantasia()));

                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Endereço");
                    cln.width(250);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getEndereco()));

                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Num.");
                    cln.width(50);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNum()));

                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Bairro");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getBairro()));

                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Cidade");
                    cln.width(180);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodCid().getNomeCid()));

                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("UF");
                    cln.width(40);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodCid().getCodEst().getId().getSiglaEst()));

                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Cla.");
                    cln.width(40);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getClasseCidade()));
                    cln.format(param -> {
                        return new TableCell<VSdAtendCarteiraRep, String>() {

                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                clear();
                                if (item != null && !empty) {
                                    setText(item);
                                    if (item.equals("S")) getStyleClass().add("classe-cid-s");
                                    if (item.equals("E")) getStyleClass().add("classe-cid-e");
                                    if (item.equals("P")) getStyleClass().add("classe-cid-p");
                                    if (item.equals("V")) getStyleClass().add("classe-cid-v");
                                }
                            }

                            private void clear() {
                                getStyleClass().removeAll("classe-cid-s", "classe-cid-e", "classe-cid-p", "classe-cid-v");
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Telefone");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getFone()));

                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("E-mail");
                    cln.width(200);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getEmail()));

                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Ult.");
                    cln.width(50);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getUltimaColecao()));

                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("CodRep");
                    cln.width(50);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodRep().getCodRep()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Representante");
                    cln.width(250);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodRep().getNome()));
                }).build()
        );
    });
    private final FormTableView<VSdAtendCarteiraRep> tblClientesValores = FormTableView.create(VSdAtendCarteiraRep.class, table -> {
        table.title("Clientes");
        table.items.bind(clientesBean);
        table.expanded();
        try {
            colecoes = DAOFactory.getColecaoDAO().getTresUltimasColecoes();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        cancelamentoToggle.value.addListener((observable, oldValue, newValue) -> {
            table.refresh();
        });
        representanteToggle.value.addListener((observable, oldValue, newValue) -> {
            table.refresh();
        });
        marcaToggle.value.addListener((observable, oldValue, newValue) -> {
            table.refresh();
        });
        valores17COToggle.value.addListener((observable, oldValue, newValue) -> {
            table.refresh();
        });
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Cód.");
                    cln.width(50);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodcli() == null ? "" : String.format("%05d", param.getValue().getCodcli().getCodcli())));

                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Perfil");
                    cln.width(40);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getSitCli()));
                    cln.format(param -> {
                        return new TableCell<VSdAtendCarteiraRep, String>() {

                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                clear();
                                if (item != null && !empty) {
                                    setText(item);
                                    if (item.equals("1")) getStyleClass().add("sit-cli-1");
                                    if (item.equals("IN")) getStyleClass().add("sit-cli-in");
                                    if (item.equals("G1")) getStyleClass().add("sit-cli-g1");
                                    if (item.equals("G2")) getStyleClass().add("sit-cli-g2");
                                    if (item.equals("G3")) getStyleClass().add("sit-cli-g3");
                                    if (item.equals("G4")) getStyleClass().add("sit-cli-g4");
                                    if (item.equals("G5")) getStyleClass().add("sit-cli-g5");
                                }
                            }

                            private void clear() {
                                getStyleClass().removeAll("sit-cli-1", "sit-cli-in", "sit-cli-g1", "sit-cli-g2", "sit-cli-g3", "sit-cli-g4", "sit-cli-g5");
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Marca");
                    cln.width(60);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodMarca().getCodigo()));
                    cln.visible.bind(marcaToggle.value);
                    cln.format(param -> {
                        return new TableCell<VSdAtendCarteiraRep, String>() {
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                clear();
                                if (item != null && !empty) {
                                    setText(item);
                                    if (item.equals("D")) getStyleClass().add("marca-d");
                                    else getStyleClass().add("marca-f");
                                }
                            }

                            private void clear() {
                                getStyleClass().removeAll("marca-d", "marca-f");
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Grupo");
                    cln.width(40);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getGrupo()));

                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Cliente");
                    cln.width(330);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodcli().getNome()));

                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Cidade");
                    cln.width(180);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodCid().getNomeCid()));

                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Representante");
                    cln.width(250);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodRep().getNome()));
                    cln.visible.bind(representanteToggle.value);
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("UF");
                    cln.width(40);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodCid().getCodEst().getId().getSiglaEst()));

                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Cla.");
                    cln.width(40);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getClasseCidade()));
                    cln.format(param -> {
                        return new TableCell<VSdAtendCarteiraRep, String>() {

                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                clear();
                                if (item != null && !empty) {
                                    setText(item);
                                    if (item.equals("S")) getStyleClass().add("classe-cid-s");
                                    if (item.equals("E")) getStyleClass().add("classe-cid-e");
                                    if (item.equals("P")) getStyleClass().add("classe-cid-p");
                                    if (item.equals("V")) getStyleClass().add("classe-cid-v");
                                }
                            }

                            private void clear() {
                                getStyleClass().removeAll("classe-cid-s", "classe-cid-e", "classe-cid-p", "classe-cid-v");
                            }
                        };
                    });
                }).build(),
                FormTableColumnGroup.createGroup(grupoW20 -> {
                    grupoW20.title("WINTER 20");
                    grupoW20.getStyleClass().add("colecao-w20-header");
                    grupoW20.addColumn(FormTableColumn.create(cln -> {
                        cln.title("Peças");
                        cln.width(50);
                        cln.getStyleClass().add("colecao-w20-header");
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>) param -> new ReadOnlyObjectWrapper(
                                cancelamentoToggle.value.getValue() ? param.getValue().getQtdew20().add(param.getValue().getQtdecancw20()) : param.getValue().getQtdew20()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*Peças*/);
                    grupoW20.addColumn(FormTableColumn.create(cln -> {
                        cln.title("Valor");
                        cln.width(columnValorWidth);
                        cln.getStyleClass().add("colecao-w20-header");
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>)
                                param -> new ReadOnlyObjectWrapper(StringUtils.toMonetaryFormat(cancelamentoToggle.value.getValue() ? param.getValue().getValorw20().add(param.getValue().getValorcancw20()) : param.getValue().getValorw20(), 2)));
                        cln.alignment(Pos.CENTER_RIGHT);
                    }).build() /*Valor*/);
                }),
                FormTableColumnGroup.createGroup(grupoW20C -> {
                    grupoW20C.title("CONTÍNUA");
                    grupoW20C.getStyleClass().add("colecao-w20-header");
                    grupoW20C.addColumn(FormTableColumn.create(cln -> {
                        cln.title("Peças");
                        cln.width(50);
                        cln.getStyleClass().add("colecao-w20-header");
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>) param -> new ReadOnlyObjectWrapper(
                                cancelamentoToggle.value.getValue() ? param.getValue().getQtde17cow20().add(param.getValue().getQtdecanc17cow20()) : param.getValue().getQtde17cow20()));
                        cln.alignment(Pos.CENTER);
                        cln.visible.bind(valores17COToggle.value);
                    }).build() /*Peças*/);
                    grupoW20C.addColumn(FormTableColumn.create(cln -> {
                        cln.title("Valor");
                        cln.width(columnValorWidth);
                        cln.getStyleClass().add("colecao-w20-header");
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>)
                                param -> new ReadOnlyObjectWrapper(StringUtils.toMonetaryFormat(cancelamentoToggle.value.getValue() ? param.getValue().getValor17cow20().add(param.getValue().getValorcanc17cow20()) : param.getValue().getValor17cow20(), 2)));
                        cln.alignment(Pos.CENTER_RIGHT);
                        cln.visible.bind(valores17COToggle.value);
                    }).build() /*Valor*/);
                }),
                FormTableColumnGroup.createGroup(grupoSS21 -> {
                    grupoSS21.title("SPRING SUMMER 21");
                    grupoSS21.getStyleClass().add("colecao-ss21-header");
                    grupoSS21.addColumn(FormTableColumn.create(cln -> {
                        cln.title("Peças");
                        cln.width(50);
                        cln.getStyleClass().add("colecao-ss21-header");
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>) param -> new ReadOnlyObjectWrapper(
                                cancelamentoToggle.value.getValue() ? param.getValue().getQtdess21().add(param.getValue().getQtdecancss21()) : param.getValue().getQtdess21()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*Peças*/);
                    grupoSS21.addColumn(FormTableColumn.create(cln -> {
                        cln.title("Valor");
                        cln.width(columnValorWidth);
                        cln.getStyleClass().add("colecao-ss21-header");
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>)
                                param -> new ReadOnlyObjectWrapper(StringUtils.toMonetaryFormat(cancelamentoToggle.value.getValue() ? param.getValue().getValorss21().add(param.getValue().getValorcancss21()) : param.getValue().getValorss21(), 2)));
                        cln.alignment(Pos.CENTER_RIGHT);
                    }).build() /*Valor*/);
                }),
                FormTableColumnGroup.createGroup(grupoSS21C -> {
                    grupoSS21C.title("CONTÍNUA");
                    grupoSS21C.getStyleClass().add("colecao-ss21-header");
                    grupoSS21C.addColumn(FormTableColumn.create(cln -> {
                        cln.title("Peças");
                        cln.width(50);
                        cln.getStyleClass().add("colecao-ss21-header");
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>) param -> new ReadOnlyObjectWrapper(
                                cancelamentoToggle.value.getValue() ? param.getValue().getQtde17coss21().add(param.getValue().getQtdecanc17coss21()) : param.getValue().getQtde17coss21()));
                        cln.alignment(Pos.CENTER);
                        cln.visible.bind(valores17COToggle.value);
                    }).build() /*Peças*/);
                    grupoSS21C.addColumn(FormTableColumn.create(cln -> {
                        cln.title("Valor");
                        cln.width(columnValorWidth);
                        cln.getStyleClass().add("colecao-ss21-header");
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>)
                                param -> new ReadOnlyObjectWrapper(StringUtils.toMonetaryFormat(cancelamentoToggle.value.getValue() ? param.getValue().getValor17coss21().add(param.getValue().getValorcanc17coss21()) : param.getValue().getValor17coss21(), 2)));
                        cln.alignment(Pos.CENTER_RIGHT);
                        cln.visible.bind(valores17COToggle.value);
                    }).build() /*Valor*/);
                }),
                FormTableColumnGroup.createGroup(grupoS21 -> {
                    grupoS21.title("SUMMER 21");
                    grupoS21.getStyleClass().add("colecao-s21-header");
                    grupoS21.addColumn(FormTableColumn.create(cln -> {
                        cln.title("Peças");
                        cln.width(50);
                        cln.getStyleClass().add("colecao-s21-header");
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>) param -> new ReadOnlyObjectWrapper(
                                cancelamentoToggle.value.getValue() ? param.getValue().getQtdes21().add(param.getValue().getQtdecancs21()) : param.getValue().getQtdes21()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*Peças*/);
                    grupoS21.addColumn(FormTableColumn.create(cln -> {
                        cln.title("Valor");
                        cln.width(columnValorWidth);
                        cln.getStyleClass().add("colecao-s21-header");
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>)
                                param -> new ReadOnlyObjectWrapper(StringUtils.toMonetaryFormat(cancelamentoToggle.value.getValue() ? param.getValue().getValors21().add(param.getValue().getValorcancs21()) : param.getValue().getValors21(), 2)));
                        cln.alignment(Pos.CENTER_RIGHT);
                    }).build() /*Valor*/);
                }),
                FormTableColumnGroup.createGroup(grupoS21C -> {
                    grupoS21C.title("CONTÍNUA");
                    grupoS21C.getStyleClass().add("colecao-s21-header");
                    grupoS21C.addColumn(FormTableColumn.create(cln -> {
                        cln.title("Peças");
                        cln.width(50);
                        cln.getStyleClass().add("colecao-s21-header");
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>) param -> new ReadOnlyObjectWrapper(
                                cancelamentoToggle.value.getValue() ? param.getValue().getQtde17cos21().add(param.getValue().getQtdecanc17cos21()) : param.getValue().getQtde17cos21()));
                        cln.alignment(Pos.CENTER);
                        cln.visible.bind(valores17COToggle.value);
                    }).build() /*Peças*/);
                    grupoS21C.addColumn(FormTableColumn.create(cln -> {
                        cln.title("Valor");
                        cln.width(columnValorWidth);
                        cln.getStyleClass().add("colecao-s21-header");
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>)
                                param -> new ReadOnlyObjectWrapper(StringUtils.toMonetaryFormat(cancelamentoToggle.value.getValue() ? param.getValue().getValor17cos21().add(param.getValue().getValorcanc17cos21()) : param.getValue().getValor17cos21(), 2)));
                        cln.alignment(Pos.CENTER_RIGHT);
                        cln.visible.bind(valores17COToggle.value);
                    }).build() /*Valor*/);
                }),
                FormTableColumnGroup.createGroup(grupoW21 -> {
                    grupoW21.title("WINTER 21");
                    grupoW21.getStyleClass().add("colecao-w21-header");
                    grupoW21.addColumn(FormTableColumn.create(cln -> {
                        cln.title("Peças");
                        cln.width(50);
                        cln.getStyleClass().add("colecao-w21-header");
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>) param -> new ReadOnlyObjectWrapper(
                                cancelamentoToggle.value.getValue() ? param.getValue().getQtdew21().add(param.getValue().getQtdecancw21()) : param.getValue().getQtdew21()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*Peças*/);
                    grupoW21.addColumn(FormTableColumn.create(cln -> {
                        cln.title("Valor");
                        cln.width(columnValorWidth);
                        cln.getStyleClass().add("colecao-w21-header");
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>)
                                param -> new ReadOnlyObjectWrapper(StringUtils.toMonetaryFormat(cancelamentoToggle.value.getValue() ? param.getValue().getValorw21().add(param.getValue().getValorcancw21()) : param.getValue().getValorw21(), 2)));
                        cln.alignment(Pos.CENTER_RIGHT);
                    }).build() /*Valor*/);
                }),
                FormTableColumnGroup.createGroup(grupoW21C -> {
                    grupoW21C.title("CONTÍNUA");
                    grupoW21C.getStyleClass().add("colecao-w21-header");
                    grupoW21C.addColumn(FormTableColumn.create(cln -> {
                        cln.title("Peças");
                        cln.width(50);
                        cln.getStyleClass().add("colecao-w21-header");
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>) param -> new ReadOnlyObjectWrapper(
                                cancelamentoToggle.value.getValue() ? param.getValue().getQtde17cow21().add(param.getValue().getQtdecanc17cow21()) : param.getValue().getQtde17cow21()));
                        cln.alignment(Pos.CENTER);
                        cln.visible.bind(valores17COToggle.value);
                    }).build() /*Peças*/);
                    grupoW21C.addColumn(FormTableColumn.create(cln -> {
                        cln.title("Valor");
                        cln.width(columnValorWidth);
                        cln.getStyleClass().add("colecao-w21-header");
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdAtendCarteiraRep, VSdAtendCarteiraRep>, ObservableValue<VSdAtendCarteiraRep>>)
                                param -> new ReadOnlyObjectWrapper(StringUtils.toMonetaryFormat(cancelamentoToggle.value.getValue() ? param.getValue().getValor17cow21().add(param.getValue().getValorcanc17cow21()) : param.getValue().getValor17cow21(), 2)));
                        cln.alignment(Pos.CENTER_RIGHT);
                        cln.visible.bind(valores17COToggle.value);
                    }).build() /*Valor*/);
                })
        );
    });
    private final FormTableView<String> tblTotais = FormTableView.create(String.class, table -> {
        cancelamentoToggle.value.addListener((observable, oldValue, newValue) -> {
            table.refresh();
        });
        valores17COToggle.value.addListener((observable, oldValue, newValue) -> {
            table.refresh();
        });
        table.tableview().getStyleClass().add("column-no-noheader");
        table.height(15);
        table.withoutHeader();
        table.items.bind(list);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.width(50);
                    cln.value((Callback<TableColumn.CellDataFeatures<String, String>, ObservableValue<String>>) param -> new ReadOnlyObjectWrapper(
                            String.valueOf(clientesBean.stream().map(!cancelamentoToggle.value.getValue() ? VSdAtendCarteiraRep::getQtdew20 : it -> it.getQtdew20().add(it.getQtdecancw20())).mapToInt(BigDecimal::intValue).sum())));
//
                    cln.alignment(Pos.CENTER);
                }).build(), /*Peças*/
                FormTableColumn.create(cln -> {
                    cln.width(columnValorWidth);
                    cln.setMaxWidth(columnValorWidth);
                    try {
                        cln.value((Callback<TableColumn.CellDataFeatures<String, String>, ObservableValue<String>>)
                                param -> new ReadOnlyObjectWrapper(StringUtils.toMonetaryFormat(clientesBean.stream().map(!cancelamentoToggle.value.getValue() ? VSdAtendCarteiraRep::getValorw20 : it -> it.getValorw20().add(it.getValorcancw20())).reduce(BigDecimal::add).orElse(BigDecimal.ZERO), 2)));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    cln.alignment(Pos.CENTER_RIGHT);
                }).build(), /*Valor*/
                FormTableColumn.create(cln -> {
                    cln.width(50);
                    cln.value((Callback<TableColumn.CellDataFeatures<String, String>, ObservableValue<String>>) param -> new ReadOnlyObjectWrapper(
                            String.valueOf(clientesBean.stream().map(!cancelamentoToggle.value.getValue() ? VSdAtendCarteiraRep::getQtde17cow20 : it -> it.getQtde17cow20().add(it.getQtdecanc17cow20())).mapToInt(BigDecimal::intValue).sum())
                    ));
                    cln.alignment(Pos.CENTER);
                    cln.visible.bind(valores17COToggle.value);
                }).build(), /*Peças*/
                FormTableColumn.create(cln -> {
                    cln.width(columnValorWidth);
                    cln.setMaxWidth(columnValorWidth);
                    try {
                        cln.value((Callback<TableColumn.CellDataFeatures<String, String>, ObservableValue<String>>)
                                param -> new ReadOnlyObjectWrapper(
                                        StringUtils.toMonetaryFormat(clientesBean.stream().map(!cancelamentoToggle.value.getValue() ? VSdAtendCarteiraRep::getValor17cow20 : it -> it.getValor17cow20().add(it.getValorcanc17cow20())).reduce(BigDecimal::add).orElse(BigDecimal.ZERO), 2)
                                ));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    cln.alignment(Pos.CENTER_RIGHT);
                    cln.visible.bind(valores17COToggle.value);
                }).build(), /*Valor*/
                FormTableColumn.create(cln -> {
                    cln.width(50);
                    cln.value((Callback<TableColumn.CellDataFeatures<String, String>, ObservableValue<String>>) param -> new ReadOnlyObjectWrapper(
                            String.valueOf(clientesBean.stream().map(!cancelamentoToggle.value.getValue() ? VSdAtendCarteiraRep::getQtdess21 : it -> it.getQtdess21().add(it.getQtdecancss21())).mapToInt(BigDecimal::intValue).sum())));
                    cln.alignment(Pos.CENTER);
                }).build(), /*Peças*/
                FormTableColumn.create(cln -> {
                    cln.width(columnValorWidth);
                    cln.setMaxWidth(columnValorWidth);
                    try {
                        cln.value((Callback<TableColumn.CellDataFeatures<String, String>, ObservableValue<String>>)
                                param -> new ReadOnlyObjectWrapper(StringUtils.toMonetaryFormat(clientesBean.stream().map(!cancelamentoToggle.value.getValue() ? VSdAtendCarteiraRep::getValorss21 : it -> it.getValorss21().add(it.getValorcancss21())).reduce(BigDecimal::add).orElse(BigDecimal.ZERO), 2)));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    cln.alignment(Pos.CENTER_RIGHT);
                }).build(), /*Valor*/
                FormTableColumn.create(cln -> {
                    cln.width(50);
                    cln.value((Callback<TableColumn.CellDataFeatures<String, String>, ObservableValue<String>>) param -> new ReadOnlyObjectWrapper(
                            String.valueOf(clientesBean.stream().map(!cancelamentoToggle.value.getValue() ? VSdAtendCarteiraRep::getQtde17coss21 : it -> it.getQtde17coss21().add(it.getQtdecanc17coss21())).mapToInt(BigDecimal::intValue).sum())
                    ));
                    cln.alignment(Pos.CENTER);
                    cln.visible.bind(valores17COToggle.value);
                }).build(), /*Peças*/
                FormTableColumn.create(cln -> {
                    cln.width(columnValorWidth);
                    cln.setMaxWidth(columnValorWidth);
                    try {
                        cln.value((Callback<TableColumn.CellDataFeatures<String, String>, ObservableValue<String>>)
                                param -> new ReadOnlyObjectWrapper(
                                        StringUtils.toMonetaryFormat(clientesBean.stream().map(!cancelamentoToggle.value.getValue() ? VSdAtendCarteiraRep::getValor17coss21 : it -> it.getValor17coss21().add(it.getValorcanc17coss21())).reduce(BigDecimal::add).orElse(BigDecimal.ZERO), 2)
                                ));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    cln.alignment(Pos.CENTER_RIGHT);
                    cln.visible.bind(valores17COToggle.value);
                }).build(), /*Valor*/
                FormTableColumn.create(cln -> {
                    cln.width(50);
                    cln.value((Callback<TableColumn.CellDataFeatures<String, String>, ObservableValue<String>>) param -> new ReadOnlyObjectWrapper(
                            String.valueOf(clientesBean.stream().map(!cancelamentoToggle.value.getValue() ? VSdAtendCarteiraRep::getQtdes21 : it -> it.getQtdes21().add(it.getQtdecancs21())).mapToInt(BigDecimal::intValue).sum())));
                    cln.alignment(Pos.CENTER);
                }).build(), /*Peças*/
                FormTableColumn.create(cln -> {
                    cln.width(columnValorWidth);
                    cln.setMaxWidth(columnValorWidth);
                    try {
                        cln.value((Callback<TableColumn.CellDataFeatures<String, String>, ObservableValue<String>>)
                                param -> new ReadOnlyObjectWrapper(StringUtils.toMonetaryFormat(clientesBean.stream().map(!cancelamentoToggle.value.getValue() ? VSdAtendCarteiraRep::getValors21 : it -> it.getValors21().add(it.getValorcancs21())).reduce(BigDecimal::add).orElse(BigDecimal.ZERO), 2)));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    cln.alignment(Pos.CENTER_RIGHT);
                }).build(), /*Valor*/
                FormTableColumn.create(cln -> {
                    cln.width(50);
                    cln.value((Callback<TableColumn.CellDataFeatures<String, String>, ObservableValue<String>>) param -> new ReadOnlyObjectWrapper(
                            String.valueOf(clientesBean.stream().map(!cancelamentoToggle.value.getValue() ? VSdAtendCarteiraRep::getQtde17cos21 : it -> it.getQtde17cos21().add(it.getQtdecanc17cos21())).mapToInt(BigDecimal::intValue).sum())
                    ));
                    cln.alignment(Pos.CENTER);
                    cln.visible.bind(valores17COToggle.value);
                }).build(), /*Peças*/
                FormTableColumn.create(cln -> {
                    cln.width(columnValorWidth);
                    cln.setMaxWidth(columnValorWidth);
                    try {
                        cln.value((Callback<TableColumn.CellDataFeatures<String, String>, ObservableValue<String>>)
                                param -> new ReadOnlyObjectWrapper(
                                        StringUtils.toMonetaryFormat(clientesBean.stream().map(!cancelamentoToggle.value.getValue() ? VSdAtendCarteiraRep::getValor17cos21 : it -> it.getValor17cos21().add(it.getValorcanc17cos21())).reduce(BigDecimal::add).orElse(BigDecimal.ZERO), 2)
                                ));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    cln.alignment(Pos.CENTER_RIGHT);
                    cln.visible.bind(valores17COToggle.value);
                }).build(), /*Valor*/
                FormTableColumn.create(cln -> {
                    cln.width(50);
                    cln.value((Callback<TableColumn.CellDataFeatures<String, String>, ObservableValue<String>>) param -> new ReadOnlyObjectWrapper(
                            String.valueOf(clientesBean.stream().map(!cancelamentoToggle.value.getValue() ? VSdAtendCarteiraRep::getQtdew21 : it -> it.getQtdew21().add(it.getQtdecancw21())).mapToInt(BigDecimal::intValue).sum())));
                    cln.alignment(Pos.CENTER);
                }).build(), /*Peças*/
                FormTableColumn.create(cln -> {
                    cln.width(columnValorWidth);
                    cln.setMaxWidth(columnValorWidth);
                    try {
                        cln.value((Callback<TableColumn.CellDataFeatures<String, String>, ObservableValue<String>>)
                                param -> new ReadOnlyObjectWrapper(StringUtils.toMonetaryFormat(clientesBean.stream().map(!cancelamentoToggle.value.getValue() ? VSdAtendCarteiraRep::getValorw21 : it -> it.getValorw21().add(it.getValorcancw21())).reduce(BigDecimal::add).orElse(BigDecimal.ZERO), 2)));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    cln.alignment(Pos.CENTER_RIGHT);
                }).build(), /*Valor*/
                FormTableColumn.create(cln -> {
                    cln.width(50);
                    cln.value((Callback<TableColumn.CellDataFeatures<String, String>, ObservableValue<String>>) param -> new ReadOnlyObjectWrapper(
                            String.valueOf(clientesBean.stream().map(!cancelamentoToggle.value.getValue() ? VSdAtendCarteiraRep::getQtde17cow21 : it -> it.getQtde17cow21().add(it.getQtde17cow21())).mapToInt(BigDecimal::intValue).sum())
                    ));
                    cln.alignment(Pos.CENTER);
                    cln.visible.bind(valores17COToggle.value);
                }).build(), /*Peças*/
                FormTableColumn.create(cln -> {
                    cln.width(columnValorWidth);
                    cln.setMaxWidth(columnValorWidth);
                    try {
                        cln.value((Callback<TableColumn.CellDataFeatures<String, String>, ObservableValue<String>>)
                                param -> new ReadOnlyObjectWrapper(
                                        StringUtils.toMonetaryFormat(clientesBean.stream().map(!cancelamentoToggle.value.getValue() ? VSdAtendCarteiraRep::getValor17cow21 : it -> it.getValor17cow21().add(it.getValorcanc17cow21())).reduce(BigDecimal::add).orElse(BigDecimal.ZERO), 2)
                                ));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    cln.alignment(Pos.CENTER_RIGHT);
                    cln.visible.bind(valores17COToggle.value);
                }).build() /*Valor*/
        );
    });

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Filter">
    private final FormFieldSingleFind<Represen> codRepFilter = FormFieldSingleFind.create(Represen.class, field -> {
        field.title.setText("Cod Rep");
        field.width(350);
        field.maxLength(5);
    });
    private final FormFieldMultipleFind<Marca> marcaFilter = FormFieldMultipleFind.create(Marca.class, field -> {
        field.title("Marca");
        field.width(200);
        field.toUpper();
    });
    private final FormFieldMultipleFind<Cidade> cidadeFilter = FormFieldMultipleFind.create(Cidade.class, field -> {
        field.title("Cidade");
        field.width(200);
        field.codeReference.set("nomeCid");
        field.toUpper();
    });
    private final FormFieldMultipleFind<SitCli> perfilFilter = FormFieldMultipleFind.create(SitCli.class, field -> {
        field.title("Perfil");
        field.width(200);
    });
    private final FormFieldMultipleFind<TabUf> estadoFilter = FormFieldMultipleFind.create(TabUf.class, field -> {
        field.title("Estado");
        field.width(200);
        field.codeReference.set("sigla");
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Headers">
    private List<String> headerPadrao = new ArrayList<>(Arrays.asList("Código", "Perfil", "Grupo", "Cliente", "Fantasia", "Endereço", "Número", "Bairro", "Cidade", "UF", "Cla.", "Telefone", "Email", "Última Compra"));
    private List<String> headerValores = new ArrayList<>(Arrays.asList("Código", "Perfil", "Grupo", "Cliente", "Cidade", "UF", "Cla.", "WINTER 20 - Peças", "WINTER 20 - Preço", "Spring Summer 21 - Peças", "Spring Summer 21 - Preço", "summer 21 - Peças", "summer 21 - Preço", "WINTER 21 - Peças", "WINTER 21 - Preço"));
    private List<String> headerPersonalizado = new ArrayList<>(Arrays.asList("Código", "Perfil", "Marca", "Grupo", "Cliente", "Cidade", "UF", "Cla.", "Representante", "WINTER 20 - Peças", "WINTER 20 - Preço", "WINTER 20 Contínua - Peças", "WINTER 20 Contínua - Preço", "Spring Summer 21 - Peças", "Spring Summer 21 - Preço", "Spring Summer 21 Contínua - Peças", "Spring Summer 21 Contínua - Preço", "summer 21 - Peças", "summer 21 - Preço", "summer 21 Contínua - Peças", "summer 21 Contínua - Preço", "WINTER 21 - Peças", "WINTER 21 - Preço", "WINTER 21 Contínua - Peças", "WINTER 21 Contínua - Preço"));
    private List<String> headerRepresentante = new ArrayList<>(Arrays.asList("Código", "Perfil", "Grupo", "Cliente", "Cidade", "UF", "Cla.", "Representante", "WINTER 20 - Peças", "WINTER 20 - Preço", "Spring Summer 21 - Peças", "Spring Summer 21 - Preço", "summer 21 - Peças", "summer 21 - Preço", "WINTER 21 - Peças", "WINTER 21 - Preço"));
    private List<String> headerMarca = new ArrayList<>(Arrays.asList("Código", "Perfil", "Marca", "Grupo", "Cliente", "Cidade", "UF", "Cla.", "WINTER 20 - Peças", "WINTER 20 - Preço", "Spring Summer 21 - Peças", "Spring Summer 21 - Preço", "summer 21 - Peças", "summer 21 - Preço", "WINTER 21 - Peças", "WINTER 21 - Preço"));
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="VBox">
    private final VBox listagemTab = (VBox) super.tabs.getTabs().get(0).getContent();
    private final VBox dadosTab = (VBox) super.tabs.getTabs().get(1).getContent();
    // </editor-fold>

    public AtendimentoCarteiraView() {
        super("Atendimento de Carteira", ImageUtils.getImage(ImageUtils.Icon.CADASTROS), new String[]{"Padrão", "Valores"});
        initListagem();
        initListagemPreco();

    }

    //<editor-fold desc="Init">

    private void initListagem() {
        listagemTab.getChildren().add(FormBox.create(principal -> {
            principal.vertical();
            principal.add(FormBox.create(header -> {
                header.vertical();
                header.add(FormBox.create(boxFilter -> {
                    boxFilter.horizontal();
                    boxFilter.add(FormTitledPane.create(filter -> {
                        filter.filter();
                        filter.add(FormBox.create(filterBox -> {
                            filterBox.vertical();
                            filterBox.add(FormBox.create(linha1 -> {
                                linha1.horizontal();
                                linha1.add(codRepFilter.build(), marcaFilter.build(), perfilFilter.build());
                            }));
                            filterBox.add(FormBox.create(linha2 -> {
                                linha2.horizontal();
                                linha2.add(estadoFilter.build(), cidadeFilter.build());
                            }));
                        }));
                        filter.find.setOnAction(evt -> {
                            new RunAsyncWithOverlay(this).exec(task -> {
                                getClientes(codRepFilter.value.getValue() == null ? new Represen() : codRepFilter.value.getValue(),
                                        cidadeFilter.objectValues.getValue().stream().map(Cidade::getCodCid).toArray(),
                                        marcaFilter.objectValues.getValue().stream().map(Marca::getCodigo).toArray(),
                                        perfilFilter.objectValues.getValue().stream().map(SitCli::getCodigo).toArray(),
                                        estadoFilter.objectValues.getValue().stream().map(it -> it.getId().getCodigo()).toArray()
                                );
                                return ReturnAsync.OK.value;
                            }).addTaskEndNotification(taskReturn -> {
                                if (taskReturn.equals(ReturnAsync.OK.value)) {
                                    clientesBean.set(FXCollections.observableArrayList(clientes));
                                    list.clear();
                                    list.add("1");
                                }
                            });
                        });
                        filter.clean.setOnAction(evt -> {
                            codRepFilter.clear();
                            cidadeFilter.clear();
                            marcaFilter.clear();
                            perfilFilter.clear();

                        });
                    }));
                }));
            }));
            principal.add(FormBox.create(center -> {
                center.vertical();
                center.expanded();
                center.add(tblClientesPadrao.build());
            }));
            principal.add(FormBox.create(footer -> {
                footer.vertical();
                footer.add(FormBox.create(buttonBox -> {
                    buttonBox.horizontal();
                    buttonBox.add(FormBox.create(boxE -> {
                        boxE.horizontal();
                        boxE.add(printJasperBtn, visualizarJasperBtn, salvarPDFJasperBtn);
                    }));
                    buttonBox.add(FormBox.create(boxD -> {
                        boxD.horizontal();
                        boxD.add(exportExcelBtn);
                    }));
                }));
            }));
        }));
    }

    private void initListagemPreco() {
        dadosTab.getChildren().add(FormBox.create(principal -> {
            principal.vertical();
            principal.size(2000, 2000);
            principal.add(FormBox.create(header -> {
                header.horizontal();
                header.add(cancelamentoToggle.build(), marcaToggle.build(), representanteToggle.build(), valores17COToggle.build(), totaisToggle.build());
            }));
            principal.add(FormBox.create(center -> {
                center.vertical();
                center.expanded();
                center.add(tblClientesValores.build());
            }));
            principal.add(FormBox.create(footer -> {
                footer.horizontal();
                footer.add(FormBox.create(boxE -> {
                    boxE.horizontal();
                    boxE.width(660);
                    boxE.add(exportExcelVariableBtn);
                }));
                footer.add(FormBox.create(boxD -> {
                    boxD.horizontal();
                    boxD.visible.bind(totaisToggle.value);
                    boxD.add(FormFieldText.create(field -> {
                        field.withoutTitle();
                        field.width(50);
                        field.value.setValue("Total: ");
                    }).build());
                    boxD.add(tblTotais.build());
                }));
            }));
        }));
    }

    //</editor-fold>

    //<editor-fold desc="Layout e Seleção">
    private String selecionarPathArquivo(FileChooser.ExtensionFilter filter) {
        return GUIUtils.showSaveFileDialog(new ArrayList<>(Collections.singleton(filter)));
    }

    private ArrayList selecionarLayout() {
        SimpleListProperty<String> opcoes = new SimpleListProperty<>(FXCollections.
                observableArrayList("Layout Padrão", "Layout Valores sem Cancelamentos", "Layout Valores com Cancelamentos", "Layout Marca", "Layout Representante", "Layout Valor17CO"));
        List<String> escolhidos = new ArrayList<>();

        FormTableView tbl = FormTableView.create(String.class, table -> {
            table.items.set(opcoes);
            table.editable.set(true);
            table.multipleSelection();
            table.addColumn(FormTableColumn.create(cln -> {
                cln.title("Escolha");
                cln.width(300);
                cln.value((Callback<TableColumn.CellDataFeatures<String, String>, ObservableValue<String>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                cln.alignment(Pos.CENTER);
            }));
        });
        new Fragment().show(fragment -> {
            fragment.title("Escolha as vizualizações");
            fragment.size(400.0, 250.0);

            fragment.box.getChildren().add(FormBox.create(box -> {
                box.vertical();
                box.add(tbl.build());
            }));
            fragment.buttonsBox.getChildren().add(FormButton.create(botao -> {
                botao.title("Selecionar");
                botao.addStyle("success");
                botao.icon(ImageUtils.getIconButton(ImageUtils.ButtonIcon.SELECT, ImageUtils.IconSize._16));
                botao.setOnAction(event -> {
                    escolhidos.addAll(tbl.selectedRegisters());
                    fragment.close();
                });
            }));
            fragment.buttonsBox.getChildren().add(FormButton.create(botao -> {
                botao.title("Selecionar Todos");
                botao.addStyle("info");
                botao.icon(ImageUtils.getIconButton(ImageUtils.ButtonIcon.SEND, ImageUtils.IconSize._16));
                botao.setOnAction(event -> {
                    escolhidos.addAll(opcoes);
                    fragment.close();
                });
            }));
        });

        return new ArrayList<>(escolhidos);
    }
    //</editor-fold>

    //<editor-fold desc="Excel">
    private void exportarExcelPadrao(String pathFile, List<String> escolhas) {
        excelExportMode = new ExportUtils().excel(pathFile);

        new RunAsyncWithOverlay(this).exec(task -> {
            try {
                if (escolhas.contains("Layout Padrão"))
                    criarSheet(excelExportMode, "Padrão", headerPadrao, false);
                if (escolhas.contains("Layout Valores com Cancelamentos"))
                    criarSheet(excelExportMode, "Com Cancelamento", headerValores, true);
                if (escolhas.contains("Layout Valores sem Cancelamentos"))
                    criarSheet(excelExportMode, "Sem Cancelamento", headerValores, false);
                if (escolhas.contains("Layout Marca"))
                    criarSheet(excelExportMode, "Marca", headerMarca, false);
                if (escolhas.contains("Layout Representante"))
                    criarSheet(excelExportMode, "Representante", headerRepresentante, false);
                if (escolhas.contains("Layout Valor17CO"))
                    criarSheet(excelExportMode, "Valor17CO", headerPersonalizado, false);
                excelExportMode.export();

            } catch (IOException e) {
                e.printStackTrace();
            }
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                MessageBox.create(message -> {
                    message.message("Excel exportado com sucesso!");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.notification();
                });
            }
        });
    }

    private void exportarExcelEditable(String pathFile) {
        excelExportMode = new ExportUtils().excel(pathFile);

        new RunAsyncWithOverlay(this).exec(task -> {
            try {
                if (!marcaToggle.value.getValue()) headerPersonalizado.remove("Marca");
                if (!representanteToggle.value.getValue()) headerPersonalizado.remove("Representante");
                if (!valores17COToggle.value.getValue())
                    headerPersonalizado.removeIf(filter -> filter.contains("Contínua"));
                criarSheet(excelExportMode, "Layout Personalizado", headerPersonalizado, cancelamentoToggle.value.getValue());
                excelExportMode.export();

            } catch (IOException e) {
                e.printStackTrace();
            }
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                headerPersonalizado = new ArrayList<>(Arrays.asList("Código", "Perfil", "Marca", "Grupo", "Cliente", "Cidade", "UF", "Cla.", "Representante", "WINTER 20 - Peças", "WINTER 20 - Preço", "WINTER 20 Contínua - Peças", "WINTER 20 Contínua - Preço", "Spring Summer 21 - Peças", "Spring Summer 21 - Preço", "Spring Summer 21 Contínua - Peças", "Spring Summer 21 Contínua - Preço", "summer 21 - Peças", "summer 21 - Preço", "summer 21 Contínua - Peças", "summer 21 Contínua - Preço", "WINTER 21 - Peças", "WINTER 21 - Preço", "WINTER 21 Contínua - Peças", "WINTER 21 Contínua - Preço"));
                MessageBox.create(message -> {
                    message.message("Excel exportado com sucesso!");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.notification();
                });
            }
        });
    }

    private void headerRow(ExportUtils.ExcelSheet sheet, List<String> header) {
        Font headerFont = sheet.workbook.createFont();
        headerFont.setBold(true);
        CellStyle headerCellStyle = sheet.workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);
        Row headerRow;
        if (!header.contains("WINTER 20 - Peças")) headerRow = sheet.sheet.createRow(0);
        else headerRow = sheet.sheet.createRow(1);

        for (int i = 0; i < header.size(); i++) {
            Cell cell = headerRow.createCell(i);
            if (header.get(i).contains("Contínua -")) {
                cell.setCellValue(header.get(i).substring(header.get(i).indexOf("-") + 1));
                cell.setCellStyle(criarStyleValores(sheet, header.get(i).toUpperCase()));
            } else if (header.get(i).contains("-")) {
                cell.setCellValue(header.get(i).substring(header.get(i).indexOf("-") + 1));
                cell.setCellStyle(criarStyleValores(sheet, header.get(i).toUpperCase()));
            } else {
                cell.setCellValue(header.get(i).toUpperCase());
                cell.setCellStyle(headerCellStyle);
            }
        }
    }
    //</editor-fold>

    //<editor-fold desc="Jasper">
    private List<ReportUtils.ShowReport> getListaJasper(ArrayList escolhas) {

        List<ReportUtils.ShowReport> reports = new ArrayList<>();

        if (escolhas.contains("Layout Padrão")) {
            ReportUtils.ShowReport report = gerarJasper(ReportUtils.ReportFile.ATENDIMENTO_CARTEIRA_PADRAO);
            reports.add(report);
            mapJaspers.put("padrao", report);
        }
        if (escolhas.contains("Layout Valores com Cancelamentos")) {
            ReportUtils.ShowReport report = gerarJasper(ReportUtils.ReportFile.ATENDIMENTO_CARTEIRA_VALORES_CANC);
            reports.add(report);
            mapJaspers.put("canc", report);
        }
        if (escolhas.contains("Layout Valores sem Cancelamentos")) {
            ReportUtils.ShowReport report = gerarJasper(ReportUtils.ReportFile.ATENDIMENTO_CARTEIRA_VALORES);
            reports.add(report);
            mapJaspers.put("val", report);
        }
        if (escolhas.contains("Layout Marca")) {
            ReportUtils.ShowReport report = gerarJasper(ReportUtils.ReportFile.ATENDIMENTO_CARTEIRA_MARCA);
            reports.add(report);
            mapJaspers.put("marca", report);
        }
        if (escolhas.contains("Layout Representante")) {
            ReportUtils.ShowReport report = gerarJasper(ReportUtils.ReportFile.ATENDIMENTO_CARTEIRA_REPRESENTANTE);
            reports.add(report);
            mapJaspers.put("rep", report);
        }
        if (escolhas.contains("Layout Valor17CO")) {
            ReportUtils.ShowReport report = gerarJasper(ReportUtils.ReportFile.ATENDIMENTO_CARTEIRA_VALORES_CONTINUA);
            reports.add(report);
            mapJaspers.put("17co", report);
        }
        return new ArrayList<>(reports);
    }

    protected ReportUtils.ShowReport gerarJasper(ReportUtils.ReportFile report) {
        try {
            return new ReportUtils().config()
                    .addReport(report,
                            new ReportUtils.ParameterReport("codRep", codRepFilter.value.getValue().getCodRep()),
                            new ReportUtils.ParameterReport("codMar", marcaFilter.objectValues.getValue().stream().map(Marca::getCodigo).collect(Collectors.toList()).toString().replace("[", "").replace("]", "")),
                            new ReportUtils.ParameterReport("codCid", cidadeFilter.objectValues.getValue().stream().map(Cidade::getCodCid).collect(Collectors.toList()).toString().replace("[", "").replace("]", "")),
                            new ReportUtils.ParameterReport("sitCli", perfilFilter.objectValues.getValue().stream().map(SitCli::getCodigo).collect(Collectors.toList()).toString().replace("[", "").replace("]", "")),
                            new ReportUtils.ParameterReport("codEmpresa",Globals.getEmpresaLogada().getCodigo())
                    ).view();
        } catch (JRException | SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private boolean prepararJaspers() {
        if (!clientesBean.isEmpty()) {
            ArrayList layouts = selecionarLayout();

            if (layouts.size() > 0) {
                jaspers.clear();
                jaspers = getListaJasper(layouts);
                return jaspers.size() > 0;
            }
        }
        return false;
    }
    //</editor-fold>

    //<editor-fold desc="Add Sheets">

    private void criarSheet(ExportUtils.ExcelExportMode excelExportMode, String nome, List<String> header, boolean cancelamento) {
        try {
            excelExportMode.addSheet(nome, sheet -> {
                headerRow(sheet, header);
                int rowNum;
                if (header.contains("WINTER 20 - Peças")) {
                    rowNum = 2;
                    criarMergedHeader(sheet, header);
                } else rowNum = 1;

                for (VSdAtendCarteiraRep cliente : clientesBean) {
                    Row row = sheet.sheet.createRow(rowNum++);
                    if (header.contains("Código"))
                        sheet.cellFormatValue(row, header.indexOf("Código"), String.format("%05d", cliente.getCodcli().getCodcli()), "string");
                    if (header.contains("Perfil"))
                        sheet.cellFormatValue(row, header.indexOf("Perfil"), cliente.getSitCli(), "string");
                    if (header.contains("Marca"))
                        sheet.cellFormatValue(row, header.indexOf("Marca"), cliente.getCodMarca().getCodigo(), "string");
                    if (header.contains("Grupo"))
                        sheet.cellFormatValue(row, header.indexOf("Grupo"), cliente.getGrupo() == null ? "" : cliente.getGrupo(), "string");
                    if (header.contains("Cliente"))
                        sheet.cellFormatValue(row, header.indexOf("Cliente"), cliente.getCliente(), "string");
                    if (header.contains("Cidade"))
                        sheet.cellFormatValue(row, header.indexOf("Cidade"), cliente.getCodCid() == null ? "" : cliente.getCodCid().getNomeCid(), "string");
                    if (header.contains("UF"))
                        sheet.cellFormatValue(row, header.indexOf("UF"), cliente.getCodCid().getCodEst().getId().getSiglaEst(), "string");
                    if (header.contains("Fantasia"))
                        sheet.cellFormatValue(row, header.indexOf("Fantasia"), cliente.getCodcli().getFantasia() == null ? "" : cliente.getCodcli().getFantasia(), "string");
                    if (header.contains("Endereço"))
                        sheet.cellFormatValue(row, header.indexOf("Endereço"), cliente.getEndereco() == null ? "" : cliente.getEndereco(), "string");
                    if (header.contains("Número"))
                        sheet.cellFormatValue(row, header.indexOf("Número"), cliente.getNum() == null ? "" : cliente.getNum(), "string");
                    if (header.contains("Bairro"))
                        sheet.cellFormatValue(row, header.indexOf("Bairro"), cliente.getBairro() == null ? "" : cliente.getBairro(), "string");
                    if (header.contains("Cla."))
                        sheet.cellFormatValue(row, header.indexOf("Cla."), cliente.getClasseCidade() == null ? "" : cliente.getClasseCidade(), "string");
                    if (header.contains("Telefone"))
                        sheet.cellFormatValue(row, header.indexOf("Telefone"), cliente.getFone() == null ? "" : cliente.getFone(), "string");
                    if (header.contains("Email"))
                        sheet.cellFormatValue(row, header.indexOf("Email"), cliente.getEmail() == null ? "" : cliente.getEmail(), "string");
                    if (header.contains("Última Compra"))
                        sheet.cellFormatValue(row, header.indexOf("Última Compra"), cliente.getUltimaColecao() == null ? "" : cliente.getUltimaColecao(), "string");
                    if (header.contains("Representante"))
                        sheet.cellFormatValue(row, header.indexOf("Representante"), cliente.getCodRep() == null ? "" : cliente.getCodRep().getNome(), "string");

                    if (header.contains("WINTER 20 - Peças"))
                        sheet.cellFormatValue(row, header.indexOf("WINTER 20 - Peças"), cancelamento ? cliente.getQtdeCanceladaw20().toString() : cliente.getQtdew20().toString(), "string");
                    if (header.contains("WINTER 20 - Preço"))
                        sheet.cellFormatValue(row, header.indexOf("WINTER 20 - Preço"), StringUtils.toMonetaryFormat(cancelamento ? cliente.getValorCanceladaw20() : cliente.getValorw20(), 2), "string");

                    if (header.contains("Spring Summer 21 - Peças"))
                        sheet.cellFormatValue(row, header.indexOf("Spring Summer 21 - Peças"), cancelamento ? cliente.getQtdeCanceladass21().toString() : cliente.getQtdess21().toString(), "string");
                    if (header.contains("Spring Summer 21 - Preço"))
                        sheet.cellFormatValue(row, header.indexOf("Spring Summer 21 - Preço"), StringUtils.toMonetaryFormat(cancelamento ? cliente.getValorCanceladass21() : cliente.getValorss21(), 2), "string");

                    if (header.contains("summer 21 - Peças"))
                        sheet.cellFormatValue(row, header.indexOf("summer 21 - Peças"), cancelamento ? cliente.getQtdeCanceladas21().toString() : cliente.getQtdes21().toString(), "string");
                    if (header.contains("summer 21 - Preço"))
                        sheet.cellFormatValue(row, header.indexOf("summer 21 - Preço"), StringUtils.toMonetaryFormat(cancelamento ? cliente.getValorCanceladas21() : cliente.getValors21(), 2), "string");

                    if (header.contains("WINTER 21 - Peças"))
                        sheet.cellFormatValue(row, header.indexOf("WINTER 21 - Peças"), cancelamento ? cliente.getQtdeCanceladaw21().toString() : cliente.getQtdew21().toString(), "string");
                    if (header.contains("WINTER 21 - Preço"))
                        sheet.cellFormatValue(row, header.indexOf("WINTER 21 - Preço"), StringUtils.toMonetaryFormat(cancelamento ? cliente.getValorCanceladaw21() : cliente.getValorw21(), 2), "string");

                    if (header.contains("WINTER 20 Contínua - Peças"))
                        sheet.cellFormatValue(row, header.indexOf("WINTER 20 Contínua - Peças"), cancelamento ? cliente.getQtdeCancelada17cow20().toString() : cliente.getQtde17cow20().toString(), "string");
                    if (header.contains("WINTER 20 Contínua - Preço"))
                        sheet.cellFormatValue(row, header.indexOf("WINTER 20 Contínua - Preço"), StringUtils.toMonetaryFormat(cancelamento ? cliente.getValorCancelada17cow20() : cliente.getValor17cow20(), 2), "string");

                    if (header.contains("Spring Summer 21 Contínua - Peças"))
                        sheet.cellFormatValue(row, header.indexOf("Spring Summer 21 Contínua - Peças"), cancelamento ? cliente.getQtdeCancelada17coss21().toString() : cliente.getQtde17coss21().toString(), "string");
                    if (header.contains("Spring Summer 21 Contínua - Preço"))
                        sheet.cellFormatValue(row, header.indexOf("Spring Summer 21 Contínua - Preço"), StringUtils.toMonetaryFormat(cancelamento ? cliente.getValorCancelada17coss21() : cliente.getValor17coss21(), 2), "string");

                    if (header.contains("summer 21 Contínua - Peças"))
                        sheet.cellFormatValue(row, header.indexOf("summer 21 Contínua - Peças"), cancelamento ? cliente.getQtdeCancelada17cos21().toString() : cliente.getQtde17cos21().toString(), "string");
                    if (header.contains("summer 21 Contínua - Preço"))
                        sheet.cellFormatValue(row, header.indexOf("summer 21 Contínua - Preço"), StringUtils.toMonetaryFormat(cancelamento ? cliente.getValorCancelada17cos21() : cliente.getValor17cos21(), 2), "string");

                    if (header.contains("WINTER 21 Contínua - Peças"))
                        sheet.cellFormatValue(row, header.indexOf("WINTER 21 Contínua - Peças"), cancelamento ? cliente.getQtdeCancelada17cow21().toString() : cliente.getQtde17cow21().toString(), "string");
                    if (header.contains("WINTER 21 Contínua - Preço"))
                        sheet.cellFormatValue(row, header.indexOf("WINTER 21 Contínua - Preço"), StringUtils.toMonetaryFormat(cancelamento ? cliente.getValorCancelada17cow21() : cliente.getValor17cow21(), 2), "string");

                    if (header.contains("Marca"))
                        row.getCell(header.indexOf("Marca")).setCellStyle(ExportUtils.setCellStyleSD(cliente.getCodMarca().getCodigo(), sheet));
                    if (header.contains("Perfil"))
                        row.getCell(header.indexOf("Perfil")).setCellStyle(ExportUtils.setCellStyleSD(cliente.getSitCli(), sheet));
                    if (header.contains("Cla."))
                        row.getCell(header.indexOf("Cla.")).setCellStyle(ExportUtils.setCellStyleSD(cliente.getClasseCidade(), sheet));
                }
                if (header.contains("WINTER 20 - Peças")) criarCellTotais(sheet, header, rowNum, cancelamento);
                for (int i = 0; i <= header.size(); i++) {
                    sheet.sheet.autoSizeColumn(i);
                }
            });
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void criarCellTotais(ExportUtils.ExcelSheet sheet, List<String> header, int rowNum, boolean cancelamento) {
        Row row = sheet.sheet.createRow(rowNum + 1);
        AtomicInteger index = new AtomicInteger(header.indexOf("WINTER 20 - Peças"));
        Cell cellTitle = row.createCell(header.indexOf("WINTER 20 - Peças") - 1);
        cellTitle.setCellValue("Total: ");
        header.stream().filter(it -> it.contains("-")).forEach(coluna -> {
            Cell cell = row.createCell(index.get());
            if (coluna.contains("Peças")) {
                try {
                    cell.setCellValue(String.valueOf(
                            valoresTotais(
                                    VSdAtendCarteiraRep.class.getDeclaredMethod(
                                            gerarNomeMetodo(coluna, false)
                                    ),
                                    VSdAtendCarteiraRep.class.getDeclaredMethod(
                                            gerarNomeMetodo(coluna, true)
                                    ),
                                    cancelamento)));

                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    cell.setCellValue(StringUtils.toMonetaryFormat(
                            valoresTotais(
                                    VSdAtendCarteiraRep.class.getDeclaredMethod(
                                            gerarNomeMetodo(coluna, false)
                                    ),
                                    VSdAtendCarteiraRep.class.getDeclaredMethod(
                                            gerarNomeMetodo(coluna, true)
                                    ),
                                    cancelamento),
                            2));

                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
            }
            XSSFCellStyle style = (XSSFCellStyle) sheet.workbook.createCellStyle();
            Font fontStyle = sheet.workbook.createFont();
            fontStyle.setBold(true);
            style.setFont(fontStyle);
            cell.setCellStyle(style);
            index.getAndIncrement();
        });
    }

    private String gerarNomeMetodo(String coluna, boolean canc) {
        String part1 = "get";
        String part2;
        String part3 = "";
        String part4 = "";
        String part5;

        if (coluna.contains("Peças")) {
            part2 = "Qtde";
        } else {
            part2 = "Valor";
        }

        coluna = coluna.substring(0, coluna.indexOf("-") - 1).trim().toLowerCase();

        if (canc) part3 = "Cancelada";

        if (coluna.contains("contínua")) {
            coluna = coluna.replaceAll("contínua", "");
            part4 = "17co";
        }

        if (coluna.startsWith("w")) {
            part5 = coluna.replaceAll("inter", "").replaceAll(" ", "");

        } else part5 = coluna.replaceAll("pring", "").replaceAll("ummer", "").replaceAll(" ", "");
        return part1 + part2 + part3 + part4 + part5;
    }

    private void criarMergedHeader(ExportUtils.ExcelSheet sheet, List<String> header) {
        criarTitulosValores(sheet, header);
        header.forEach(it -> {
            if (it.contains("Peças")) {
                sheet.sheet.addMergedRegion(new CellRangeAddress(0, 0, header.indexOf(it), header.indexOf(it) + 1));
            }
        });
    }

    private void criarTitulosValores(ExportUtils.ExcelSheet sheet, List<String> header) {
        Row row = sheet.sheet.createRow(0);
        header.forEach(it -> {
            if (it.contains("Peças")) {
                String nome = it.substring(0, it.indexOf("-") - 1).toUpperCase();
                Cell cell = row.createCell(header.indexOf(it));
                cell.setCellStyle(criarStyleValores(sheet, nome));
                cell.setCellValue(it.contains("Contínua") ? "Contínua" : nome);
            }
        });
    }

    private CellStyle criarStyleValores(ExportUtils.ExcelSheet sheet, String nome) {
        XSSFCellStyle style = (XSSFCellStyle) sheet.workbook.createCellStyle();
        Font fontStyle = sheet.workbook.createFont();
        fontStyle.setBold(true);

        if (nome.contains("WINTER 20")) style.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
        else if (nome.startsWith("SP")) style.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
        else if (nome.startsWith("SU")) style.setFillForegroundColor(IndexedColors.SKY_BLUE.getIndex());
        else style.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());

        style.setAlignment(HorizontalAlignment.CENTER);
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setFont(fontStyle);
        return style;
    }

    //</editor-fold>

    protected BigDecimal valoresTotais(Method metodo, Method cancelado, boolean cancelamento) {
        BigDecimal valorTotal = BigDecimal.ZERO;
        BigDecimal valor = null;
        for (VSdAtendCarteiraRep cliente : clientesBean) {
            if (!cancelamento) {
                try {
                    valor = (BigDecimal) metodo.invoke(cliente);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    valor = (BigDecimal) cancelado.invoke(cliente);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
            valorTotal = valorTotal.add(valor);
        }
        return valorTotal;
    }

}