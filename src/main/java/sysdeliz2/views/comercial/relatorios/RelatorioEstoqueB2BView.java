package sysdeliz2.views.comercial.relatorios;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import sysdeliz2.controllers.views.comercial.relatorios.RelatorioEstoqueB2BController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.sysdeliz.SdMagentoSitCli;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.ti.Produto;
import sysdeliz2.models.ti.TabPreco;
import sysdeliz2.models.view.VB2BProdEstoqueResumido;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.ReportUtils;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;

import java.io.IOException;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class RelatorioEstoqueB2BView extends RelatorioEstoqueB2BController {

    //<editor-fold desc="Lists">
    private ListProperty<VB2BProdEstoqueResumido> produtosBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private List<FormControlButton> listModelagens = new ArrayList<>();
    private List<FormControlButton> listMarcas = new ArrayList<>();
    private List<FormControlButton> listColecoes = new ArrayList<>();
    private List<FormControlButton> listTabPreco = new ArrayList<>();
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Control">
    private BooleanProperty clearAll = new SimpleBooleanProperty(false);
    // </editor-fold>

    //<editor-fold desc="Boxes">
    private final FormBox boxFiltroMarca = FormBox.create(boxFiltroMarca -> {
        boxFiltroMarca.verticalScroll();
        boxFiltroMarca.vertical();
        boxFiltroMarca.alignment(Pos.BASELINE_CENTER);
        boxFiltroMarca.setMinSize(350, 500);
        boxFiltroMarca.border();
    });

    private final FormBox boxFiltroColecao = FormBox.create(boxFiltroMarca -> {
        boxFiltroMarca.verticalScroll();
        boxFiltroMarca.vertical();
        boxFiltroMarca.alignment(Pos.BASELINE_CENTER);
        boxFiltroMarca.setMinSize(330, 300);
        boxFiltroMarca.setMaxHeight(400);
        boxFiltroMarca.border();
    });

    private final FormBox boxFiltroTabPre = FormBox.create(boxFiltroMarca -> {
        boxFiltroMarca.verticalScroll();
        boxFiltroMarca.vertical();
        boxFiltroMarca.alignment(Pos.BASELINE_CENTER);
        boxFiltroMarca.setMinSize(360, 350);
        boxFiltroMarca.border();
    });
    //</editor-fold>

    //<editor-fold desc="Components">
    private final FormFieldMultipleFind<Produto> filterProduto = FormFieldMultipleFind.create(Produto.class, field -> {
        field.title("Produto");
        field.toUpper();
    });

    private final FormFieldMultipleFind<Colecao> filterColecao = FormFieldMultipleFind.create(Colecao.class, field -> {
        field.title("Coleção");
        field.toUpper();
    });

    private final FormFieldMultipleFind<Marca> filterMarca = FormFieldMultipleFind.create(Marca.class, field -> {
        field.title("Marca");
        field.toUpper();
    });

    private final FormTableView<VB2BProdEstoqueResumido> tblProdutos = FormTableView.create(VB2BProdEstoqueResumido.class, table -> {
        table.title("Produtos");
        table.items.bind(produtosBean);
        table.withoutCounter();
        table.indices(
                table.indice("Foto OK e Enviado", "success", ""),
                table.indice("Sem Foto ou Não Enviado", "warning", ""),
                table.indice("Sem Foto e Não Enviado", "danger", "")
        );
        table.expanded();
        table.factoryRow(param -> new TableRow<VB2BProdEstoqueResumido>() {
            @Override
            protected void updateItem(VB2BProdEstoqueResumido item, boolean empty) {
                super.updateItem(item, empty);
                clear();

                if (item != null && !empty) {
                    if (!item.isImagem() && !item.isEnviado()) {
                        getStyleClass().add("table-row-danger");
                    } else if (!item.isImagem() || !item.isEnviado()) {
                        getStyleClass().add("table-row-warning");
                    } else {
                        getStyleClass().add("table-row-success");
                    }
                }
            }

            private void clear() {
                getStyleClass().removeAll("table-row-danger", "table-row-success", "table-row-warning");
            }
        });
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<VB2BProdEstoqueResumido, VB2BProdEstoqueResumido>, ObservableValue<VB2BProdEstoqueResumido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(250);
                    cln.value((Callback<TableColumn.CellDataFeatures<VB2BProdEstoqueResumido, VB2BProdEstoqueResumido>, ObservableValue<VB2BProdEstoqueResumido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDescricao()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Marca");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<VB2BProdEstoqueResumido, VB2BProdEstoqueResumido>, ObservableValue<VB2BProdEstoqueResumido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDescmarca()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Coleção");
                    cln.width(170);
                    cln.value((Callback<TableColumn.CellDataFeatures<VB2BProdEstoqueResumido, VB2BProdEstoqueResumido>, ObservableValue<VB2BProdEstoqueResumido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getColecao()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Grupo Modelagem");
                    cln.width(120);
                    cln.value((Callback<TableColumn.CellDataFeatures<VB2BProdEstoqueResumido, VB2BProdEstoqueResumido>, ObservableValue<VB2BProdEstoqueResumido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getGrupoModelagem()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Qtde");
                    cln.width(70);
                    cln.value((Callback<TableColumn.CellDataFeatures<VB2BProdEstoqueResumido, VB2BProdEstoqueResumido>, ObservableValue<VB2BProdEstoqueResumido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Imagem");
                    cln.width(120);
                    cln.value((Callback<TableColumn.CellDataFeatures<VB2BProdEstoqueResumido, VB2BProdEstoqueResumido>, ObservableValue<VB2BProdEstoqueResumido>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<VB2BProdEstoqueResumido, VB2BProdEstoqueResumido>() {
                        @Override
                        protected void updateItem(VB2BProdEstoqueResumido item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                setGraphic(ImageUtils.getIcon(item.isImagem() ? ImageUtils.Icon.CONFIRMAR : ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                setText(item.isImagem() ? "Foto Ok" : "Sem Imagem");
                            }
                        }
                    });
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Enviado");
                    cln.width(120);
                    cln.value((Callback<TableColumn.CellDataFeatures<VB2BProdEstoqueResumido, VB2BProdEstoqueResumido>, ObservableValue<VB2BProdEstoqueResumido>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<VB2BProdEstoqueResumido, VB2BProdEstoqueResumido>() {
                        @Override
                        protected void updateItem(VB2BProdEstoqueResumido item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                setGraphic(ImageUtils.getIcon(item.isEnviado() ? ImageUtils.Icon.ENVIAR : ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                setText(item.isEnviado() ? "Enviado" : "Não Enviado");
                            }
                        }
                    });
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Status");
                    cln.width(120);
                    cln.value((Callback<TableColumn.CellDataFeatures<VB2BProdEstoqueResumido, VB2BProdEstoqueResumido>, ObservableValue<VB2BProdEstoqueResumido>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<VB2BProdEstoqueResumido, VB2BProdEstoqueResumido>() {
                        @Override
                        protected void updateItem(VB2BProdEstoqueResumido item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                setGraphic(ImageUtils.getIcon(item.isStatus() ? ImageUtils.Icon.CONFIRMAR : ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                setText(item.isStatus() ? "Ativo" : "Inativo");
                            }
                        }
                    });
                }).build() /*Código*/
        );
    });

    private final FormFieldToggle tgTabPrecos = FormFieldToggle.create(tg -> {
        tg.title("Carregar Tab Preço");
        tg.value.set(false);
    });

    private final FormButton btnImprimirSemFoto = FormButton.create(btn -> {
        btn.addStyle("primary");
        btn.title("Imprimir S/ Foto");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.IMPRIMIR, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            if (produtosBean.size() == 0) return;
            janelaImpressaoProdutosSemFoto();
        });
    });

    private final FormButton btnImprimirTodos = FormButton.create(btn -> {
        btn.addStyle("warning");
        btn.title("Imprimir Todos");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.IMPRIMIR, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            if (produtosBean.size() == 0) return;
            imprimirTodosItens();
        });
    });

    private final FormFieldToggle tgSemEntrega = FormFieldToggle.create(tg -> {
        tg.title("Mostrar \"Sem Entrega\"");
        tg.value.set(false);
    });
    //</editor-fold>

    public RelatorioEstoqueB2BView() {
        super("Relatório Estoque B2B", ImageUtils.getImage(ImageUtils.Icon.DEPOSITO));
        init();
    }

    private void init() {
        box.getChildren().add(FormBox.create(principal -> {
            principal.vertical();
            principal.expanded();
            principal.add(FormBox.create(boxHeader -> {
                boxHeader.horizontal();
                boxHeader.add(FormTitledPane.create(filter -> {
                    filter.filter();
                    filter.add(FormBox.create(boxFilter -> {
                        boxFilter.vertical();
                        boxFilter.add(l1 -> {
                            l1.horizontal();
                            l1.add(filterProduto.build(), filterMarca.build(), tgSemEntrega.build());
                        });
                        boxFilter.add(l2 -> {
                            l2.horizontal();
                            l2.add(filterColecao.build(), tgTabPrecos.build());
                        });
                    }));
                    filter.find.setOnAction(evt -> {
                        new RunAsyncWithOverlay(this).exec(task -> {
                            buscarProdutos(
                                    filterProduto.objectValues.stream().map(Produto::getCodigo).toArray(),
                                    filterMarca.objectValues.stream().map(Marca::getCodigo).toArray(),
                                    filterColecao.objectValues.stream().map(Colecao::getCodigo).toArray(),
                                    tgSemEntrega.value.getValue()
                            );
                            return ReturnAsync.OK.value;
                        }).addTaskEndNotification(taskReturn -> {
                            if (taskReturn.equals(ReturnAsync.OK.value)) {
                                produtosBean.set(FXCollections.observableArrayList(produtos));
                                tblProdutos.refresh();
                                carregaBoxFiltros();
                            }
                        });
                    });
                    filter.clean.setOnAction(evt -> {
                        filterProduto.clear();
                        filterColecao.clear();
                        filterMarca.clear();
                        clearAll.set(true);
                        clearAll.set(false);
                        tgTabPrecos.value.set(false);
                        tgSemEntrega.value.set(false);
                    });
                }));
                boxHeader.add(box -> {
                    box.vertical();
                    box.alignment(Pos.BOTTOM_LEFT);
                    box.add(btnImprimirTodos);
                    box.add(btnImprimirSemFoto);
                });
                double widthField = 200;
                boxHeader.add(FormBox.create(boxTotais -> {
                    boxTotais.vertical();
                    boxTotais.add(l1 -> {
                        l1.horizontal();
                        l1.setPadding(new Insets(5,0,5,0));
                        l1.add(boxTotal -> {
                            boxTotal.vertical();
                            boxTotal.width(widthField);
                            boxTotal.add(FormLabel.create(lbl -> {
                                lbl.setText("Total de Produtos");
                                lbl.addStyle("success");
                                lbl.width(widthField);
                                lbl.setAlignment(Pos.CENTER);
                            }));
                            boxTotal.add(FormBox.create(boxFields -> {
                                boxFields.horizontal();
                                boxFields.add(FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.alignment(Pos.CENTER);
                                    field.editable.set(false);
                                    field.value.set("0");
                                    field.label("Ref.");
                                    field.width(widthField / 2);
                                    field.addStyle("success");
                                    produtosBean.sizeProperty().addListener((observable, oldValue, newValue) -> {
                                        if (newValue != null) field.value.setValue(String.valueOf(newValue));
                                    });
                                }).build());
                                boxFields.add(FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.alignment(Pos.CENTER);
                                    field.editable.set(false);
                                    field.value.set("0");
                                    field.label("Estq");
                                    field.width(widthField / 2);
                                    field.addStyle("success");
                                    produtosBean.sizeProperty().addListener((observable, oldValue, newValue) -> {
                                        if (newValue != null)
                                            field.value.setValue(String.valueOf(produtosBean.stream().mapToInt(VB2BProdEstoqueResumido::getQtde).sum()));
                                    });
                                }).build());
                            }));
                        });
                        l1.add(boxTotal -> {
                            boxTotal.vertical();
                            boxTotal.width(widthField);
                            boxTotal.add(FormLabel.create(lbl -> {
                                lbl.setText("Produtos com Foto");
                                lbl.addStyle("primary");
                                lbl.width(widthField);
                                lbl.setAlignment(Pos.CENTER);
                            }));
                            boxTotal.add(FormBox.create(boxFields -> {
                                boxFields.horizontal();
                                boxFields.add(FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.alignment(Pos.CENTER);
                                    field.editable.set(false);
                                    field.value.set("0");
                                    field.label("Ref.");
                                    field.width(widthField / 2);
                                    field.addStyle("primary");
                                    produtosBean.sizeProperty().addListener((observable, oldValue, newValue) -> {
                                        if (newValue != null)
                                            field.value.setValue(String.valueOf(produtosBean.stream().filter(VB2BProdEstoqueResumido::isImagem).count()));
                                    });
                                }).build());
                                boxFields.add(FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.alignment(Pos.CENTER);
                                    field.editable.set(false);
                                    field.value.set("0");
                                    field.label("Estq");
                                    field.width(widthField / 2);
                                    field.addStyle("primary");
                                    produtosBean.sizeProperty().addListener((observable, oldValue, newValue) -> {
                                        if (newValue != null)
                                            field.value.setValue(String.valueOf(produtosBean.stream().filter(VB2BProdEstoqueResumido::isImagem).mapToInt(VB2BProdEstoqueResumido::getQtde).sum()));
                                    });
                                }).build());
                            }));
                        });
                        l1.add(boxTotal -> {
                            boxTotal.vertical();
                            boxTotal.width(widthField);
                            boxTotal.add(FormLabel.create(lbl -> {
                                lbl.setText("Produtos Enviados");
                                lbl.addStyle("info");
                                lbl.width(widthField);
                                lbl.setAlignment(Pos.CENTER);
                            }));
                            boxTotal.add(FormBox.create(boxFields -> {
                                boxFields.horizontal();
                                boxFields.add(FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.alignment(Pos.CENTER);
                                    field.value.set("0");
                                    field.editable.set(false);
                                    field.label("Ref.");
                                    field.width(widthField / 2);
                                    field.addStyle("info");
                                    produtosBean.sizeProperty().addListener((observable, oldValue, newValue) -> {
                                        if (newValue != null)
                                            field.value.setValue(String.valueOf(produtosBean.stream().filter(VB2BProdEstoqueResumido::isEnviado).count()));
                                    });
                                }).build());

                                boxFields.add(FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.alignment(Pos.CENTER);
                                    field.value.set("0");
                                    field.editable.set(false);
                                    field.label("Estq");
                                    field.width(widthField / 2);
                                    field.addStyle("info");
                                    produtosBean.sizeProperty().addListener((observable, oldValue, newValue) -> {
                                        if (newValue != null)
                                            field.value.setValue(String.valueOf(produtosBean.stream().filter(VB2BProdEstoqueResumido::isEnviado).mapToInt(VB2BProdEstoqueResumido::getQtde).sum()));
                                    });
                                }).build());
                            }));
                        });
                    });
                    boxTotais.add(l2 -> {
                        l2.horizontal();
                        l2.alignment(Pos.CENTER);
                        l2.add(FormBox.create(boxTotal -> {
                            boxTotal.vertical();
                            boxTotal.width(widthField);
                            boxTotal.add(FormLabel.create(lbl -> {
                                lbl.setText("Produtos Sem Foto");
                                lbl.addStyle("warning");
                                lbl.width(widthField);
                                lbl.setAlignment(Pos.CENTER);
                            }));
                            boxTotal.add(FormBox.create(boxFields -> {
                                boxFields.horizontal();
                                boxFields.add(FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.alignment(Pos.CENTER);
                                    field.value.set("0");
                                    field.addStyle("warning");
                                    field.label("Ref.");
                                    field.width(widthField / 2);
                                    field.editable.set(false);
                                    produtosBean.sizeProperty().addListener((observable, oldValue, newValue) -> {
                                        if (newValue != null)
                                            field.value.setValue(String.valueOf(produtosBean.stream().filter(it -> !it.isImagem()).count()));
                                    });
                                }).build());

                                boxFields.add(FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.alignment(Pos.CENTER);
                                    field.value.set("0");
                                    field.addStyle("warning");
                                    field.label("Estq");
                                    field.width(widthField / 2);
                                    field.editable.set(false);
                                    produtosBean.sizeProperty().addListener((observable, oldValue, newValue) -> {
                                        if (newValue != null)
                                            field.value.setValue(String.valueOf(produtosBean.stream().filter(it -> !it.isImagem()).mapToInt(VB2BProdEstoqueResumido::getQtde).sum()));
                                    });
                                }).build());
                            }));
                        }));
                        l2.add(FormBox.create(boxTotal -> {
                            boxTotal.vertical();
                            boxTotal.width(widthField);
                            boxTotal.add(FormLabel.create(lbl -> {
                                lbl.setText("Produtos Não Enviados");
                                lbl.addStyle("danger");
                                lbl.width(widthField);
                                lbl.setAlignment(Pos.CENTER);
                            }));
                            boxTotal.add(FormBox.create(boxFields -> {
                                boxFields.horizontal();
                                boxFields.add(FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.alignment(Pos.CENTER);
                                    field.value.set("0");
                                    field.addStyle("danger");
                                    field.label("Ref.");
                                    field.width(widthField / 2);
                                    field.editable.set(false);
                                    produtosBean.sizeProperty().addListener((observable, oldValue, newValue) -> {
                                        if (newValue != null)
                                            field.value.setValue(String.valueOf(produtosBean.stream().filter(it -> !it.isEnviado()).count()));
                                    });
                                }).build());

                                boxFields.add(FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.alignment(Pos.CENTER);
                                    field.value.set("0");
                                    field.addStyle("danger");
                                    field.label("Estq");
                                    field.width(widthField / 2);
                                    field.editable.set(false);
                                    produtosBean.sizeProperty().addListener((observable, oldValue, newValue) -> {
                                        if (newValue != null)
                                            field.value.setValue(String.valueOf(produtosBean.stream().filter(it -> !it.isEnviado()).mapToInt(VB2BProdEstoqueResumido::getQtde).sum()));
                                    });
                                }).build());
                            }));
                        }));
                    });
                }));
            }));
            principal.add(FormBox.create(boxTabela -> {
                boxTabela.horizontal();
                boxTabela.expanded();
                boxTabela.add(tblProdutos.build());
                boxTabela.add(FormBox.create(boxFiltros -> {
                    boxFiltros.horizontal();
                    boxFiltros.add(boxFiltroMarca.build());
                    boxFiltros.add(FormBox.create(boxVertical -> {
                        boxVertical.vertical();
                        boxVertical.add(boxFiltroColecao.build());
                        boxVertical.add(boxFiltroTabPre.build());
                    }));
                }));
            }));
        }));
    }

    private void janelaImpressaoProdutosSemFoto() {
        new Fragment().show(frag -> {
            ListProperty<VB2BProdEstoqueResumido> produtosImpBean = new SimpleListProperty<>(FXCollections.observableArrayList(produtosBean.stream().filter(it -> !it.isImagem()).collect(Collectors.toList())));
            produtosImpBean.forEach(it -> it.setSelected(true));
            final FormTableView<VB2BProdEstoqueResumido> tblProdutosImpressao = FormTableView.create(VB2BProdEstoqueResumido.class, table -> {
                table.title("Produtos");
                table.items.bind(produtosImpBean);
                table.editable.set(true);
                table.selectColumn();
                table.expanded();
                table.columns(
                        FormTableColumn.create(cln -> {
                            cln.title("Código");
                            cln.width(80);
                            cln.value((Callback<TableColumn.CellDataFeatures<VB2BProdEstoqueResumido, VB2BProdEstoqueResumido>, ObservableValue<VB2BProdEstoqueResumido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo()));
                        }).build(), /*Código*/
                        FormTableColumn.create(cln -> {
                            cln.title("Marca");
                            cln.width(100);
                            cln.value((Callback<TableColumn.CellDataFeatures<VB2BProdEstoqueResumido, VB2BProdEstoqueResumido>, ObservableValue<VB2BProdEstoqueResumido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDescmarca()));
                        }).build(), /*Código*/
                        FormTableColumn.create(cln -> {
                            cln.title("Coleção");
                            cln.width(170);
                            cln.value((Callback<TableColumn.CellDataFeatures<VB2BProdEstoqueResumido, VB2BProdEstoqueResumido>, ObservableValue<VB2BProdEstoqueResumido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getColecao()));
                        }).build(), /*Código*/
                        FormTableColumn.create(cln -> {
                            cln.title("Qtde");
                            cln.width(70);
                            cln.value((Callback<TableColumn.CellDataFeatures<VB2BProdEstoqueResumido, VB2BProdEstoqueResumido>, ObservableValue<VB2BProdEstoqueResumido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                        }).build(), /*Código*/
                        FormTableColumn.create(cln -> {
                            cln.title("Enviado");
                            cln.width(120);
                            cln.value((Callback<TableColumn.CellDataFeatures<VB2BProdEstoqueResumido, VB2BProdEstoqueResumido>, ObservableValue<VB2BProdEstoqueResumido>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                            cln.format(param -> new TableCell<VB2BProdEstoqueResumido, VB2BProdEstoqueResumido>() {
                                @Override
                                protected void updateItem(VB2BProdEstoqueResumido item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    setGraphic(null);
                                    if (item != null && !empty) {
                                        setGraphic(ImageUtils.getIcon(item.isEnviado() ? ImageUtils.Icon.ENVIAR : ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                        setText(item.isEnviado() ? "Enviado" : "Não Enviado");
                                    }
                                }
                            });
                        }).build(), /*Código*/
                        FormTableColumn.create(cln -> {
                            cln.title("Status");
                            cln.width(120);
                            cln.value((Callback<TableColumn.CellDataFeatures<VB2BProdEstoqueResumido, VB2BProdEstoqueResumido>, ObservableValue<VB2BProdEstoqueResumido>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                            cln.format(param -> new TableCell<VB2BProdEstoqueResumido, VB2BProdEstoqueResumido>() {
                                @Override
                                protected void updateItem(VB2BProdEstoqueResumido item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    setGraphic(null);
                                    if (item != null && !empty) {
                                        setGraphic(ImageUtils.getIcon(item.isStatus() ? ImageUtils.Icon.CONFIRMAR : ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                        setText(item.isStatus() ? "Ativo" : "Inativo");
                                    }
                                }
                            });
                        }).build() /*Código*/
                );
            });
            frag.size(1100.0, 800.0);
            frag.title("Impressão de Produtos sem Imagem");
            frag.box.getChildren().add(FormBox.create(box -> {
                box.vertical();
                box.expanded();
                box.add(tblProdutosImpressao.build());
            }));
            frag.buttonsBox.getChildren().add(FormButton.create(btn -> {
                btn.title("Imprimir Selecionados");
                btn.addStyle("primary");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.IMPRIMIR, ImageUtils.IconSize._24));
                btn.setOnAction(evt -> {
                    List<VB2BProdEstoqueResumido> itensImpressao = produtosImpBean.stream().filter(BasicModel::isSelected).collect(Collectors.toList());
                    if (itensImpressao.size() == 0) {
                        MessageBox.create(message -> {
                            message.message("Nenhum produto selecionado para impressão");
                            message.type(MessageBox.TypeMessageBox.WARNING);
                            message.position(Pos.TOP_RIGHT);
                            message.notification();
                        });
                        return;
                    }
                    imprimirItens(itensImpressao);
                    frag.close();
                });
            }));
        });
    }

    private void imprimirItens(List<VB2BProdEstoqueResumido> itensImpressao) {
        try {
            new ReportUtils().config().addReport(ReportUtils.ReportFile.PRODUTOS_B2B_SEM_FOTO,
                    new ReportUtils.ParameterReport("codigos", itensImpressao.stream().map(VB2BProdEstoqueResumido::getCodigo).collect(Collectors.joining(","))),
                    new ReportUtils.ParameterReport("codEmpresa", Globals.getEmpresaLogada().getCodigo())
            ).view().toPdfWithDialog();
        } catch (IOException | JRException | SQLException e) {
            e.printStackTrace();
        }
    }

    private void filtrarProdutosTipo() {

        ObservableList<VB2BProdEstoqueResumido> subentries = FXCollections.observableArrayList();
        Set<String> marcas = listMarcas.stream().filter(it -> it.isSelected.get()).map(eb -> eb.codeFilter.get()).collect(Collectors.toSet());
        Set<String> colecoes = listColecoes.stream().filter(it -> it.isSelected.get()).map(eb -> eb.codeFilter.get()).collect(Collectors.toSet());
        Set<String> modelagem = listModelagens.stream().filter(it -> it.isSelected.get()).map(eb -> eb.codeFilter.get()).collect(Collectors.toSet());
        Set<String> tabPrecos = listTabPreco.stream().filter(it -> it.isSelected.get()).map(eb -> eb.codeFilter.get()).collect(Collectors.toSet());

        if (marcas.size() != 0 || colecoes.size() != 0 || modelagem.size() != 0 || tabPrecos.size() != 0) {
            for (VB2BProdEstoqueResumido entry : produtos) {
                List<TabPreco> tabs = (List<TabPreco>) new FluentDao().selectFrom(TabPreco.class).where(it -> it.equal("id.codigo.codigo", entry.getCodigo())).resultList();
                if (
                        (marcas.stream().anyMatch(it -> it.equals(entry.getDescmarca())) || marcas.size() == 0) &&
                                (colecoes.stream().anyMatch(it -> it.equals(entry.getColecao())) || colecoes.size() == 0) &&
                                (tabPrecos.stream().anyMatch(it -> tabs.stream().anyMatch(eb -> eb.getId().getRegiao().equals(it))) || tabPrecos.size() == 0) &&
                                (modelagem.stream().anyMatch(it -> it.equals(entry.getGrupoModelagem())) || modelagem.size() == 0)
                ) {
                    subentries.add(entry);
                }
            }
        } else {
            subentries.addAll(produtos);
        }

        produtosBean.set(subentries);
        tblProdutos.refresh();
    }

    private void carregaBoxFiltros() {
        boxFiltroMarca.clear();
        boxFiltroColecao.clear();
        boxFiltroTabPre.clear();

        new TreeMap<>(produtos.stream().collect(Collectors.groupingBy(VB2BProdEstoqueResumido::getDescmarca))).forEach((marca, lista) -> {
            boxFiltroMarca.add(
                    FormBox.create(boxFiltro -> {
                        boxFiltro.horizontal();
                        boxFiltro.add(FormControlButton.create(btn -> {
                            btn.title(marca);
                            btn.width(200);
                            btn.codeFilter.set(marca);
                            btn.bgColorStandard.set("#5E87FF");
                            btn.bgColorSelected.set("#6db3c3");

                            clearAll.addListener((observable, oldValue, newValue) -> {
                                if (newValue != null && newValue) {
                                    btn.isSelected.set(false);
                                }
                            });

                            btn.isSelected.addListener((observable, oldValue, newValue) -> {
                                if (newValue != null) {
                                    if (newValue) listMarcas.add(btn);
                                    filtrarProdutosTipo();
                                }
                            });
                        }));
                        boxFiltro.add(FormFieldText.create(field -> {
                            field.withoutTitle();
                            field.editable.set(false);
                            field.width(50);
                            field.value.setValue(String.valueOf(lista.size()));
                            produtosBean.sizeProperty().addListener((observable, oldValue, newValue) -> {
                                if (newValue != null)
                                    field.value.setValue(String.valueOf(produtosBean.stream().filter(it -> it.getDescmarca().equals(marca)).count()));
                            });
                        }).build());

                        boxFiltro.add(FormFieldText.create(field -> {
                            field.withoutTitle();
                            field.editable.set(false);
                            field.width(50);
                            field.addStyle("secundary");
                            field.value.setValue(String.valueOf(produtosBean.stream().filter(it -> it.getDescmarca().equals(marca)).mapToInt(VB2BProdEstoqueResumido::getQtde).sum()));
                            produtosBean.sizeProperty().addListener((observable, oldValue, newValue) -> {
                                if (newValue != null)
                                    field.value.setValue(String.valueOf(produtosBean.stream().filter(it -> it.getDescmarca().equals(marca)).mapToInt(VB2BProdEstoqueResumido::getQtde).sum()));
                            });
                        }).build());
                    })
            );
            new TreeMap<>(lista.stream().collect(Collectors.groupingBy(VB2BProdEstoqueResumido::getGrupoModelagem))).forEach((modelagem, listaItens) -> {
                boxFiltroMarca.add(
                        FormBox.create(boxFiltro -> {
                            boxFiltro.horizontal();
                            boxFiltro.add(FormControlButton.create(btn -> {
                                btn.title(modelagem);
                                btn.width(150);
                                btn.codeFilter.set(modelagem);
                                btn.bgColorStandard.set("#AABFFF");
                                btn.bgColorSelected.set("#6db3c3");

                                clearAll.addListener((observable, oldValue, newValue) -> {
                                    if (newValue != null && newValue) {
                                        btn.isSelected.set(false);
                                    }
                                });

                                btn.isSelected.addListener((observable, oldValue, newValue) -> {
                                    if (newValue != null) {
                                        if (newValue) listModelagens.add(btn);
                                        filtrarProdutosTipo();
                                    }
                                });
                            }));

                            boxFiltro.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.editable.set(false);
                                field.width(50);
                                field.value.setValue(String.valueOf(listaItens.size()));
                                produtosBean.sizeProperty().addListener((observable, oldValue, newValue) -> {
                                    if (newValue != null)
                                        field.value.setValue(String.valueOf(produtosBean.stream().filter(it -> it.getDescmarca().equals(marca) && it.getGrupoModelagem().equals(modelagem)).count()));
                                });
                            }).build());

                            boxFiltro.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.editable.set(false);
                                field.width(50);
                                field.addStyle("secundary");
                                field.value.setValue(String.valueOf(produtosBean.stream().filter(it -> it.getDescmarca().equals(marca) && it.getGrupoModelagem().equals(modelagem)).mapToInt(VB2BProdEstoqueResumido::getQtde).sum()));
                                produtosBean.sizeProperty().addListener((observable, oldValue, newValue) -> {
                                    if (newValue != null)
                                        field.value.setValue(String.valueOf(produtosBean.stream().filter(it -> it.getDescmarca().equals(marca) && it.getGrupoModelagem().equals(modelagem)).mapToInt(VB2BProdEstoqueResumido::getQtde).sum()));
                                });
                            }).build());
                        })
                );
            });
        });
        new TreeMap<>(produtos.stream().collect(Collectors.groupingBy(VB2BProdEstoqueResumido::getColecao))).forEach((colecao, lista) -> {
            boxFiltroColecao.add(
                    FormBox.create(boxFiltro -> {
                        boxFiltro.horizontal();
                        boxFiltro.add(FormControlButton.create(btn -> {
                            btn.title(colecao);
                            btn.width(210);
                            btn.codeFilter.set(colecao);
                            btn.bgColorStandard.set("#5E87FF");
                            btn.bgColorSelected.set("#6db3c3");

                            clearAll.addListener((observable, oldValue, newValue) -> {
                                if (newValue != null && newValue) {
                                    btn.isSelected.set(false);
                                }
                            });

                            btn.isSelected.addListener((observable, oldValue, newValue) -> {
                                if (newValue != null) {
                                    if (newValue) listColecoes.add(btn);
                                    filtrarProdutosTipo();
                                }
                            });
                        }));

                        boxFiltro.add(FormFieldText.create(field -> {
                            field.withoutTitle();
                            field.editable.set(false);
                            field.width(50);
                            field.value.setValue(String.valueOf(lista.size()));
                            produtosBean.sizeProperty().addListener((observable, oldValue, newValue) -> {
                                if (newValue != null)
                                    field.value.setValue(String.valueOf(produtosBean.stream().filter(it -> it.getColecao().equals(colecao)).count()));
                            });
                        }).build());

                        boxFiltro.add(FormFieldText.create(field -> {
                            field.withoutTitle();
                            field.editable.set(false);
                            field.width(50);
                            field.addStyle("secundary");
                            field.value.setValue(String.valueOf(produtosBean.stream().filter(it -> it.getColecao().equals(colecao)).mapToInt(VB2BProdEstoqueResumido::getQtde).sum()));
                            produtosBean.sizeProperty().addListener((observable, oldValue, newValue) -> {
                                if (newValue != null)
                                    field.value.setValue(String.valueOf(produtosBean.stream().filter(it -> it.getColecao().equals(colecao)).mapToInt(VB2BProdEstoqueResumido::getQtde).sum()));
                            });
                        }).build());
                    })
            );
        });

        if (tgTabPrecos.value.getValue()) {
            carregaBotoesTabPreco();
        }
    }

    private void carregaBotoesTabPreco() {
        List<SdMagentoSitCli> sitCLis = (List<SdMagentoSitCli>) new FluentDao().selectFrom(SdMagentoSitCli.class).get().orderBy("tabPreco", OrderType.ASC).resultList();

        for (SdMagentoSitCli sitCLi : sitCLis) {
            List<TabPreco> itens = (List<TabPreco>) new FluentDao().selectFrom(TabPreco.class).where(it -> it.equal("id.regiao", sitCLi.getTabPreco())).resultList();
            boxFiltroTabPre.add(
                    FormBox.create(boxFiltro -> {
                        List<VB2BProdEstoqueResumido> listaItensTabPre = produtosBean.stream().filter(it -> itens.stream().anyMatch(eb -> eb.getId().getCodigo().getCodigo().equals(it.getCodigo()))).collect(Collectors.toList());
                        boxFiltro.horizontal();
                        boxFiltro.add(FormControlButton.create(btn -> {
                            btn.title(sitCLi.getTabPreco());
                            btn.width(210);
                            btn.codeFilter.set(sitCLi.getTabPreco());
                            btn.bgColorStandard.set("#5E87FF");
                            btn.bgColorSelected.set("#6db3c3");

                            clearAll.addListener((observable, oldValue, newValue) -> {
                                if (newValue != null && newValue) {
                                    btn.isSelected.set(false);
                                }
                            });

                            btn.isSelected.addListener((observable, oldValue, newValue) -> {
                                if (newValue != null) {
                                    if (newValue) listTabPreco.add(btn);
                                    filtrarProdutosTipo();
                                }
                            });
                        }));

                        boxFiltro.add(FormFieldText.create(field -> {
                            field.withoutTitle();
                            field.editable.set(false);
                            field.width(50);
                            field.value.setValue(String.valueOf(listaItensTabPre.size()));
                            produtosBean.sizeProperty().addListener((observable, oldValue, newValue) -> {
                                if (newValue != null)
                                    field.value.setValue(String.valueOf(produtosBean.stream().filter(it -> itens.stream().anyMatch(eb -> eb.getCodigo().equals(it.getCodigo()))).count()));
                            });
                        }).build());

                        boxFiltro.add(FormFieldText.create(field -> {
                            field.withoutTitle();
                            field.editable.set(false);
                            field.width(50);
                            field.addStyle("secundary");
                            field.value.setValue(String.valueOf(listaItensTabPre.stream().mapToInt(VB2BProdEstoqueResumido::getQtde).sum()));
                            produtosBean.sizeProperty().addListener((observable, oldValue, newValue) -> {
                                if (newValue != null)
                                    field.value.setValue(String.valueOf(produtosBean.stream().filter(it -> itens.stream().anyMatch(eb -> eb.getId().getCodigo().getCodigo().equals(it.getCodigo()))).mapToInt(VB2BProdEstoqueResumido::getQtde).sum()));
                            });
                        }).build());
                    })
            );
        }
    }

    private void imprimirTodosItens() {
        /**
         * task:  * return:
         */
        AtomicReference<ReportUtils.ShowReport> view = new AtomicReference<>();
        new RunAsyncWithOverlay(this).exec(task -> {
            try {
                view.set(new ReportUtils().config().addReport(ReportUtils.ReportFile.PRODUTOS_ESTOQUE_B2B,
                        new ReportUtils.ParameterReport("codigos", produtosBean.stream().map(VB2BProdEstoqueResumido::getCodigo).collect(Collectors.joining(","))),
                        new ReportUtils.ParameterReport("codEmpresa", Globals.getEmpresaLogada().getCodigo())
                ).view());
            } catch (JRException | SQLException e) {
                e.printStackTrace();
            }
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                try {
                    view.get().toPdfWithDialog();
                } catch (IOException | JRException e) {
                    e.printStackTrace();
                }
                MessageBox.create(message -> {
                    message.message("Relatório gerado com sucesso!");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            }
        });
    }
}
