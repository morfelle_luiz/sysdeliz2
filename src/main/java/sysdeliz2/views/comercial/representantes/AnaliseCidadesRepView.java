package sysdeliz2.views.comercial.representantes;

import javafx.application.Platform;
import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import javafx.util.StringConverter;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import sysdeliz2.controllers.views.comercial.representantes.AnaliseCidadesRepController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.sysdeliz.SdCidadeRep;
import sysdeliz2.models.sysdeliz.SdMarcasRepresen;
import sysdeliz2.models.sysdeliz.SdPcaCodRepMarca;
import sysdeliz2.models.ti.Cidade;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.ti.Represen;
import sysdeliz2.models.view.VSdDadosColecaoAtual;
import sysdeliz2.models.view.VSdPedCidadesRep;
import sysdeliz2.models.view.VSdPedDataCidadesRep;
import sysdeliz2.models.view.VSdSemanaColecao;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.sys.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static sysdeliz2.utils.sys.ExportUtils.*;

public class AnaliseCidadesRepView extends AnaliseCidadesRepController {

    private final BooleanProperty inEdition = new SimpleBooleanProperty(false);
    private List<Marca> marcasRep = new ArrayList<>();
    private boolean returnTab = false;

    // <editor-fold defaultstate="collapsed" desc="Variaveis">
    private String cidadesColAtual = "";
    private String cidadesCobertas = "";
    private String cidadesBaixaPerformance = "";
    private String cidadesEntre5e10 = "";
    private String cidadesEntre10e40 = "";
    private String cidadesEntre40e80 = "";
    private String cidadesMaior80 = "";
    private String somaTotalCidades = "";
    private String primeiroResultado = "";
    private String segundoResultado = "";
    private String terceiroResultado = "";
    private String quartoResultado = "";
    private String somaResultados = "";
    private String pvTotal = "";
    private String tfTotal = "";
    private String totalCidades = "";
    // </editor-fold>

    private final List<VSdDadosColecaoAtual> colecaoAtual = (List<VSdDadosColecaoAtual>) new FluentDao().selectFrom(VSdDadosColecaoAtual.class).get().resultList();

    // <editor-fold defaultstate="collapsed" desc="Toggle">
    private final FormFieldToggle tgGlobal = FormFieldToggle.create(tg -> {
        tg.title("Vizualização Global");
        tg.value.set(false);
    });
    private final FormFieldToggle tgGlobalSt = FormFieldToggle.create(tg -> {
        tg.title("Vizualização Global");
        tg.value.set(false);
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Filter">
    private final FormFieldSingleFind<Represen> codrepFilter = FormFieldSingleFind.create(Represen.class, field -> {
        field.title("Representante");
        field.width(397);
        field.value.setValue(null);
        field.disable.bind(tgGlobal.value);
    });

    private final FormFieldSingleFind<Represen> codrepSTFilter = FormFieldSingleFind.create(Represen.class, field -> {
        field.title("Representante");
        field.width(500);
        field.value.setValue(null);
        field.disable.bind(tgGlobalSt.value);
    });

    private final FormFieldComboBox<VSdSemanaColecao> semanaComboBox = FormFieldComboBox.create(VSdSemanaColecao.class, field -> {
        field.width(250);
        field.title("Semana");

        field.converter(new StringConverter() {
            @Override
            public String toString(Object object) {
                VSdSemanaColecao semana = (VSdSemanaColecao) object;
                return semana.getSemana() + "ª Semana (" + StringUtils.toShortDateFormat(semana.getDtInicio()) + " à " + StringUtils.toShortDateFormat(semana.getDtFim()) + ")";
            }

            @Override
            public Object fromString(String string) {
                return string.substring(0, 2);
            }
        });
    });

    private final FormFieldSingleFind<Colecao> colecaoFilter = FormFieldSingleFind.create(Colecao.class, field -> {
        field.title("Coleção");
        field.width(250);
        field.value.setValue(null);
        field.disable.bind(tgGlobalSt.value);
        field.value.addListener((observable, oldValue, newValue) -> {
            if (newValue == null)
                semanaComboBox.items.clear();
            else {

                semanaComboBox.items(FXCollections.observableArrayList((List<VSdSemanaColecao>) new FluentDao().selectFrom(VSdSemanaColecao.class).where(wr -> wr.equal("colecao.codigo", ((Colecao) newValue).getCodigo())).resultList()));
                if (semanaComboBox.items.size() > 0) {
                    VSdSemanaColecao semanaAtual = semanaComboBox.items.stream().filter(it -> DateUtils.between(LocalDate.now(), it.getDtInicio(), it.getDtFim())).findFirst().orElse(null);
                    if (semanaAtual == null)
                        semanaComboBox.select(0);
                    else
                        semanaComboBox.select(semanaAtual);
                }
            }
        });
        VSdDadosColecaoAtual colAtual = (VSdDadosColecaoAtual) new FluentDao().selectFrom(VSdDadosColecaoAtual.class).get().findFirst();

        field.value.setValue(colAtual == null ? null : colAtual.getColecao());
    });

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Box">
    private final VBox vizualizacaoTab = (VBox) super.tabs.getTabs().get(0).getContent();
    private final VBox storyTellingTab = (VBox) super.tabs.getTabs().get(1).getContent();
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Fields">
    private final double widthFields = 97;

    private final FormFieldText totalCidadesField = FormFieldText.create(field -> {
        field.withoutTitle();
//        field.label(colecoes.get(1).get(0).getColecao().getSdAbreviacao());
        field.getLabel().setStyle("-fx-background-color: #000; -fx-text-fill: #BEFC05;");
        field.addStyle("high-dark");

        field.alignment(Pos.CENTER);
        field.editable.set(false);
        field.setPadding(0);
        field.width(widthFields * 2 - 5);
    });

    private final FormFieldText cidadesColAtualField = FormFieldText.create(field -> {
        field.withoutTitle();
//        field.label(colecoes.get(1).get(0).getColecao().getSdAbreviacao());
        field.getLabel().setStyle("-fx-background-color: #000; -fx-text-fill: #BEFC05;");
        field.addStyle("high-dark");

        field.alignment(Pos.CENTER);
        field.editable.set(false);
        field.setPadding(0);
        field.width(widthFields - 10);
    });

    private final FormFieldText cidadesCobertasField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.addStyle("info");
        field.alignment(Pos.CENTER);
        field.editable.set(false);
        field.setPadding(0);
        field.width(widthFields);
    });

    private final FormFieldText cidadesBaixaPerformanceField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.addStyle("danger");
        field.alignment(Pos.CENTER);
        field.editable.set(false);
        field.setPadding(0);
        field.width(widthFields);
    });

    private final FormFieldText entre5e10TotalField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.addStyle("warning");
        field.alignment(Pos.CENTER);
        field.editable.set(false);
        field.setPadding(0);
        field.width(widthFields);
    });

    private final FormFieldText entre10e40TotalField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.addStyle("warning");
        field.alignment(Pos.CENTER);
        field.editable.set(false);
        field.setPadding(0);
        field.width(widthFields);
    });

    private final FormFieldText entre40e80TotalField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.addStyle("warning");
        field.alignment(Pos.CENTER);
        field.editable.set(false);
        field.setPadding(0);
        field.width(widthFields);
    });

    private final FormFieldText maior80TotalField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.addStyle("warning");
        field.alignment(Pos.CENTER);
        field.editable.set(false);
        field.setPadding(0);
        field.width(widthFields);
    });

    private final FormFieldText somaTotalCidadesField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.addStyle("warning");
        field.textField.setAlignment(Pos.CENTER);
        field.editable.set(false);
        field.setPadding(0);
        field.width(115);
    });

    private final FormFieldText primeiroIndiceField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.value.setValue("1");
        field.addStyle("amber");
        field.alignment(Pos.CENTER);
        field.editable.set(false);
        field.setPadding(0);
        field.width(widthFields);
    });

    private final FormFieldText segundoIndiceField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.value.setValue("1");
        field.addStyle("amber");
        field.alignment(Pos.CENTER);
        field.editable.set(false);
        field.setPadding(0);
        field.width(widthFields);
    });

    private final FormFieldText terceiroIndiceField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.value.setValue("2");
        field.addStyle("amber");
        field.alignment(Pos.CENTER);
        field.editable.set(false);
        field.setPadding(0);
        field.width(widthFields);
    });

    private final FormFieldText quartoIndiceField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.value.setValue("3");
        field.addStyle("amber");
        field.alignment(Pos.CENTER);
        field.editable.set(false);
        field.setPadding(0);
        field.width(widthFields);
    });

    private final FormFieldText primeiroResultadoField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.addStyle("success");
        field.alignment(Pos.CENTER);
        field.editable.set(false);
        field.setPadding(0);
        field.width(widthFields);
    });

    private final FormFieldText segundoResultadoField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.addStyle("success");
        field.alignment(Pos.CENTER);
        field.editable.set(false);
        field.setPadding(0);
        field.width(widthFields);
    });

    private final FormFieldText terceiroResultadoField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.addStyle("success");
        field.alignment(Pos.CENTER);
        field.editable.set(false);
        field.setPadding(0);
        field.width(widthFields);
    });

    private final FormFieldText quartoResultadoField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.addStyle("success");
        field.alignment(Pos.CENTER);
        field.editable.set(false);
        field.setPadding(0);
        field.width(widthFields);
    });

    private final FormFieldText somaResultadoTotalField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.addStyle("success");
        field.alignment(Pos.CENTER);
        field.editable.set(false);
        field.setPadding(0);
        field.width(115);
    });

    // PV TF

    private final FormFieldText pvTotalField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.addStyle("primary");
        field.alignment(Pos.CENTER);
        field.editable.set(false);
        field.setPadding(0);
        field.width(widthFields);
    });

    private final FormFieldText tfTotalField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.addStyle("primary");
        field.alignment(Pos.CENTER);
        field.editable.set(false);
        field.setPadding(0);
        field.width(widthFields);
    });

    // PCA

    private final FormFieldText prazoCurtoMesesField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.decimalField(0);
        field.alignment(Pos.CENTER);
        field.editable.set(true);
        field.setPadding(0);
        field.width(90);
        field.editable.bind(inEdition);
    });

    private final FormFieldText prazoCurtoCidadesField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.decimalField(0);
        field.alignment(Pos.CENTER);
        field.editable.set(true);
        field.setPadding(0);
        field.width(90);
        field.editable.bind(inEdition);
    });

    private final FormFieldText prazoMedioMesesField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.decimalField(0);
        field.alignment(Pos.CENTER);
        field.editable.set(true);
        field.setPadding(0);
        field.width(90);
        field.editable.bind(inEdition);
    });

    private final FormFieldText prazoMedioCidadesField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.decimalField(0);
        field.alignment(Pos.CENTER);
        field.editable.set(true);
        field.setPadding(0);
        field.width(90);
        field.editable.bind(inEdition);
    });

    private final FormFieldText prazoLongoMesesField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.decimalField(0);
        field.alignment(Pos.CENTER);
        field.editable.set(true);
        field.setPadding(0);
        field.width(90);
        field.editable.bind(inEdition);
    });

    private final FormFieldText prazoLongoCidadesField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.decimalField(0);
        field.alignment(Pos.CENTER);
        field.editable.set(true);
        field.setPadding(0);
        field.width(90);
        field.editable.bind(inEdition);
    });

    private final FormFieldTextArea textAreaPca = FormFieldTextArea.create(field -> {
        field.width(200);
        field.height(100);
        field.withoutTitle();
        field.addStyle("info");
        field.textField.setFont(Font.font("Verdana", FontWeight.BOLD, 14));
        field.alignment(Pos.CENTER);
    });

    private final FormFieldText dtCriacaoField = FormFieldText.create(field -> {
        field.width(200);
        field.withoutTitle();
        field.addStyle("warning");
        field.fontBold();
        field.clickable(false);
        field.value.setValue("Marco Zero: ");
    });

    private final FormFieldText dtUltAtualizacaoField = FormFieldText.create(field -> {
        field.width(200);
        field.withoutTitle();
        field.addStyle("warning");
        field.clickable(false);
        field.fontBold();
        field.value.setValue("Ult. Atualização: ");
    });

    private final ToggleGroup tGroup = new ToggleGroup();
    private final RadioButton rbSemana = new RadioButton("Semana");
    private final RadioButton rbMes = new RadioButton("Mês");
//    private final RadioButton rbDia = new RadioButton("Dia");

    {

        rbSemana.setId("S");
        rbMes.setId("M");
//        rbDia.setId("D");

        rbSemana.setSelected(true);

        rbSemana.setToggleGroup(tGroup);
        rbMes.setToggleGroup(tGroup);
//        rbDia.setToggleGroup(tGroup);

    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Buttons">
    private final Button btnExportExcel = FormButton.create(btn -> {
        btn.setText("Exportar Excel");
        btn.addStyle("success");
        btn.width(150);
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EXCEL, ImageUtils.IconSize._16));
        btn.setOnAction(evt -> exportarExcel());
    });

    private final Button btnSalvarPCA = FormButton.create(btn -> {
        btn.setText("Salvar PCA");
        btn.addStyle("primary");
        btn.width(150);
        btn.disable.bind(inEdition.not());
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._16));
        btn.setOnAction(evt -> salvarPca(false));
    });

    private final Button btnVoltarMarcoZero = FormButton.create(btn -> {
        btn.setText("Voltar Marco Zero");
        btn.addStyle("danger");
        btn.width(150);
        btn.disable.bind(inEdition.not());
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.RETORNAR, ImageUtils.IconSize._16));
        btn.setOnAction(evt -> carregarPcaData(pca.getDtCriacao()));
    });

    private final Button btnDiaAtual = FormButton.create(btn -> {
        btn.setText("Carregar Hoje");
        btn.addStyle("warning");
        btn.width(150);
        btn.disable.bind(inEdition.not());
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CALENDARIO_PRODUTO, ImageUtils.IconSize._16));
        btn.setOnAction(evt -> carregarPcaData(LocalDate.now()));
    });

    // </editor-fold>


    private final FormTabPane tabPane = FormTabPane.create(tabPane -> {
        tabPane.expanded();
        tabPane.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue == null) return;

            Marca marcaAntiga = null;

            if (oldValue != null) {
                marcaAntiga = marcasRep.stream().filter(it -> it.getCodigo().equals(oldValue.getId())).findFirst().orElse(null);
            }

            Marca marcaNova = marcasRep.stream().filter(it -> it.getCodigo().equals(newValue.getId())).findFirst().orElse(null);

            if (tgGlobal.value.getValue()) {
                carregaMarca(marcaNova);
            } else {
                SdPcaCodRepMarca novoPca = getNovoPca();
                SdPcaCodRepMarca pcaSalvo = marcaAntiga == null ? null : getPcaCodRepMarca(represenSelecionado, marcaAntiga);

                if (novoPca != null && pcaSalvo != null && !pcaSalvo.equals(novoPca) && !returnTab) {
                    if (!((Boolean) QuestionBox.build(Boolean.class, message -> {
                        message.message("Você está saindo da marcaNova sem salvar as alterações do PCA, se confirmar, as alterações serão perdidas. Deseja sair?");
                        message.showAndWait();
                    }).value.get())) {
                        returnTab = true;
                        tabPane.getSelectionModel().select(oldValue);
                        return;
                    }
                }
                returnTab = false;
                pca = getPcaCodRepMarca(represenSelecionado, marcaNova);
                carregaMarca(marcaNova);
//                pca = returnTab ? novoPca : getPcaCodRepMarca(represenSelecionado, marcaNova);
                inEdition.set(!marcaNova.getCodigo().equals("0"));
            }
        });
    });

    private void carregaMarca(Marca marca) {
        cidades = buscarCidades(represenSelecionado, marca, colecaoFilter.value.getValue(), semanaComboBox.value.getValue());
        marcaSelecionada.set(marca);
        carregaCampos();
    }

    private final FormTabPane tabPaneSt = FormTabPane.create(tabPane -> {
        tabPane.expanded();
    });

    public AnaliseCidadesRepView() {
        super("Análise Cidades Representante", ImageUtils.getImage(ImageUtils.Icon.CIDADE), new String[]{"Listagem", "StoryTelling"});
        initVizualizacao();
        initStoryTelling();
        Platform.runLater(() -> textAreaPca.textField.lookup(".content").setStyle("-fx-background-color: #BEE5EB;"));
    }

    private void initVizualizacao() {
        vizualizacaoTab.getChildren().add(FormBox.create(principal -> {
            principal.vertical();
            principal.expanded();
            principal.add(FormBox.create(boxHeader -> {
                boxHeader.horizontal();
                boxHeader.add(FormBox.create(boxTitledFilter -> {
                    boxTitledFilter.vertical();
                    boxTitledFilter.add(FormTitledPane.create(filter -> {
                        filter.filter();
                        filter.width(620);
                        filter.add(FormBox.create(boxFilter -> {
                            boxFilter.vertical();
                            boxFilter.add(FormBox.create(linha1 -> {
                                linha1.vertical();
                                linha1.add(l1 -> {
                                    l1.horizontal();
                                    l1.add(codrepFilter.build(), tgGlobal.build());
                                });
                                linha1.add(l2 -> {
                                    l2.horizontal();
                                    l2.add(colecaoFilter.build(), semanaComboBox.build());
                                });
                            }));
                        }));
                        filter.find.setOnAction(evt -> {
                            carregarCidadesRepMarca(colecaoFilter.value.getValue(), semanaComboBox.value.getValue());
                        });
                        semanaComboBox.value.addListener((observable, oldValue, newValue) -> {
                            if (newValue != null && codrepFilter.value.isNotNull().getValue() && colecaoFilter.value.isNotNull().getValue()) {
                                filter.find.fire();
                            }
                        });
                        filter.clean.setOnAction(evt -> {
                            codrepFilter.clear();
                            colecaoFilter.clear();
                        });
                    }));
                }));
                boxHeader.add(FormBox.create(boxBotoes -> {
                    boxBotoes.vertical();
                    boxBotoes.add(btnExportExcel, btnSalvarPCA, btnVoltarMarcoZero, btnDiaAtual);
                }));
                boxHeader.add(new Separator(Orientation.VERTICAL));
                boxHeader.add(FormBox.create(boxPrinc -> {
                    boxPrinc.vertical();
                    boxPrinc.add(FormBox.create(pca -> {
                        pca.vertical();
                        pca.add(FormBox.create(l1 -> {
                            l1.horizontal();
                            l1.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.value.setValue("PRAZO");
                                field.addStyle("info");
                                field.alignment(Pos.CENTER);
                                field.editable.set(false);
                                field.setPadding(0);
                                field.width(90);
                            }).build());

                            l1.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.value.setValue("MESES");
                                field.addStyle("info");
                                field.alignment(Pos.CENTER);
                                field.editable.set(false);
                                field.setPadding(0);
                                field.width(90);
                            }).build());

                            l1.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.value.setValue("CIDADES");
                                field.addStyle("info");
                                field.alignment(Pos.CENTER);
                                field.editable.set(false);
                                field.setPadding(0);
                                field.width(90);
                            }).build());
                        }));
                        pca.add(FormBox.create(l2 -> {
                            l2.horizontal();
                            l2.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.value.setValue("CURTO");
                                field.addStyle("info");
                                field.alignment(Pos.CENTER);
                                field.editable.set(false);
                                field.setPadding(0);
                                field.width(90);
                            }).build());
                            l2.add(prazoCurtoMesesField.build());
                            l2.add(prazoCurtoCidadesField.build());
                        }));
                        pca.add(FormBox.create(l3 -> {
                            l3.horizontal();
                            l3.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.value.setValue("MEDIO");
                                field.addStyle("info");
                                field.alignment(Pos.CENTER);
                                field.editable.set(false);
                                field.setPadding(0);
                                field.width(90);
                            }).build());
                            l3.add(prazoMedioMesesField.build());
                            l3.add(prazoMedioCidadesField.build());
                        }));
                        pca.add(FormBox.create(l4 -> {
                            l4.horizontal();
                            l4.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.value.setValue("LONGO");
                                field.addStyle("info");
                                field.alignment(Pos.CENTER);
                                field.editable.set(false);
                                field.setPadding(0);
                                field.width(90);
                            }).build());
                            l4.add(prazoLongoMesesField.build());
                            l4.add(prazoLongoCidadesField.build());
                        }));
                    }));
                }));
                boxHeader.add(FormBox.create(boxPca -> {
                    boxPca.vertical();
                    boxPca.add(FormFieldText.create(field -> {
                        field.value.setValue("PCA");
                        field.alignment(Pos.CENTER);
                        field.width(200);
                        field.withoutTitle();
                        field.addStyle("info");
                        field.editable.set(false);
                    }).build());
                    boxPca.add(textAreaPca.build());
                }));
                boxHeader.add(boxStatus -> {
                    boxStatus.vertical();
                    boxStatus.add(dtCriacaoField.build(), dtUltAtualizacaoField.build());
                });
            }));
            principal.add(FormTitledPane.create(field -> {
                field.expanded();
                field.add(FormBox.create(boxCenter -> {
                    boxCenter.vertical();
                    boxCenter.expanded();
                    boxCenter.add(tabPane);
                    boxCenter.add();
                }));
            }));
            principal.add(FormBox.create(boxFooter -> {
                boxFooter.vertical();
                boxFooter.border();
                boxFooter.alignment(Pos.BASELINE_RIGHT);
                boxFooter.add(FormBox.create(boxTotais -> {
                    boxTotais.vertical();
                    boxTotais.setPadding(new Insets(0, 0, 0, 70));
                    boxTotais.add(FormBox.create(boxl1 -> {
                        boxl1.horizontal();
                        boxl1.add(totalCidadesField.build());
                        boxl1.add(FormFieldText.create(field -> {
                            field.editable.set(false);
                            field.addStyle("info");
                            field.width(widthFields * 2);
                            field.value.setValue("Cidades Cobertas");
                            field.withoutTitle();
                        }).build());
                        boxl1.add(cidadesCobertasField.build(), cidadesBaixaPerformanceField.build(), entre5e10TotalField.build(), entre10e40TotalField.build(), entre40e80TotalField.build(), maior80TotalField.build(), somaTotalCidadesField.build(), pvTotalField.build(), tfTotalField.build(), cidadesColAtualField.build());
                    }));
                    boxTotais.add(FormBox.create(boxl2 -> {
                        boxl2.horizontal();
                        boxl2.setPadding(new Insets(0, 0, 0, widthFields * 4 + 5));
                        boxl2.add(FormFieldText.create(field -> {
                            field.editable.set(false);
                            field.addStyle("amber");
                            field.width(widthFields * 2 + 5);
                            field.value.setValue("Multiplicadores");
                            field.withoutTitle();
                        }).build());
                        boxl2.add(primeiroIndiceField.build(), segundoIndiceField.build(), terceiroIndiceField.build(), quartoIndiceField.build());
                    }));
                    boxTotais.add(FormBox.create(boxl3 -> {
                        boxl3.horizontal();
                        boxl3.setPadding(new Insets(0, 0, 0, widthFields * 4 + 5));
                        boxl3.add(FormFieldText.create(field -> {
                            field.editable.set(false);
                            field.addStyle("success");
                            field.width(widthFields * 2 + 5);
                            field.value.setValue("Possíveis Clientes");
                            field.withoutTitle();
                        }).build());
                        boxl3.add(primeiroResultadoField.build(), segundoResultadoField.build(), terceiroResultadoField.build(), quartoResultadoField.build(), somaResultadoTotalField.build());
                    }));
                }));
            }));
        }));
    }

    private void initStoryTelling() {
        storyTellingTab.getChildren().add(FormBox.create(principal -> {
            principal.expanded();
            principal.vertical();
            principal.add(FormBox.create(boxHeader -> {
                boxHeader.horizontal();
                boxHeader.alignment(Pos.BOTTOM_LEFT);
                boxHeader.add(FormBox.create(boxFilter -> {
                    boxFilter.horizontal();
                    boxFilter.add(FormTitledPane.create(filter -> {
                        filter.filter();
                        filter.add(FormBox.create(boxLinha -> {
                            boxLinha.horizontal();
                            boxLinha.add(codrepSTFilter.build(), tgGlobalSt.build());
                        }));
                        filter.find.setOnAction(evt -> {
                            if (codrepSTFilter.value.getValue() == null && !tgGlobalSt.value.get()) {
                                MessageBox.create(message -> {
                                    message.message("Selecione um representante primeiro ou marque a \"Vizualização Global\"");
                                    message.type(MessageBox.TypeMessageBox.WARNING);
                                    message.position(Pos.TOP_RIGHT);
                                    message.notification();
                                });
                                return;
                            }
                            List<Marca> marcas = new ArrayList<>();
                            tabPaneSt.getTabs().clear();
                            new RunAsyncWithOverlay(this).exec(task -> {
                                cidadesSt.clear();
                                if (tgGlobal.value.getValue()) {
                                    marcas.addAll((List<Marca>) new FluentDao().selectFrom(Marca.class).where(el -> el.isIn("codigo", new String[]{"D", "F"})).resultList());
                                } else {
                                    marcas.addAll(codrepSTFilter.value.getValue().getMarcas().stream().map(it -> it.getId().getMarca()).collect(Collectors.toList()));
                                }
                                marcas.sort(Comparator.comparing(Marca::getCodigo));
                                if (marcas.size() > 1) marcas.add(new Marca("0"));
                                return ReturnAsync.OK.value;
                            }).addTaskEndNotification(taskReturn -> {
                                if (taskReturn.equals(ReturnAsync.OK.value)) {
                                    marcas.forEach(marca -> criaVizualizacaoMarcaSt(marca));
                                }
                            });
                        });
                        filter.clean.setOnAction(evt -> {
                            codrepSTFilter.clear();
                        });
                    }));
                }));
                boxHeader.add(FormBox.create(boxToggle -> {
                    boxToggle.vertical();
                    boxToggle.title("Filtro");
                    boxToggle.add(rbSemana, rbMes);
                }));
            }));
            principal.add(tabPaneSt);
        }));
    }

    private void criaVizualizacaoMarca(Marca marca, Represen representante, Colecao colecao, VSdSemanaColecao semana) {
        tabPane.addTab(tab -> {
            ListProperty<VSdPedCidadesRep> cidadesBean = new SimpleListProperty(FXCollections.observableArrayList(buscarCidades(representante, marca, colecao, semana)));
            tab.setId(marca.getCodigo());
            tab.setText(marca.getDescricao());

            tab.setContent(FormBox.create(root -> {
                root.vertical();
                root.expanded();
                root.add(FormTableView.create(SdCidadeRep.class, table -> {
                    table.title("Cidades");
                    table.expanded();
                    table.items.bind(cidadesBean);
                    table.factoryRow(param -> new TableRow<VSdPedCidadesRep>() {
                        @Override
                        protected void updateItem(VSdPedCidadesRep item, boolean empty) {
                            super.updateItem(item, empty);
                            clear();

                            if (item != null && !empty) {
                                if (item.isColecaoAtual()) {
                                    getStyleClass().add("table-row-high-dark");
                                } else if (item.isAtiva()) {
                                    getStyleClass().add("table-row-info");
                                }
                            }
                        }

                        private void clear() {
                            getStyleClass().removeAll("table-row-high-dark", "table-row-info");
                        }
                    });
                    table.columns(
                            FormTableColumn.create(cln -> {
                                cln.title("Cidade");
                                cln.width(200);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdPedCidadesRep, VSdPedCidadesRep>, ObservableValue<VSdPedCidadesRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodCid() == null ? "" : param.getValue().getCodCid().getNomeCid()));
                            }).build(),
                            FormTableColumn.create(cln -> {
                                cln.title("UF");
                                cln.width(80);
                                cln.alignment(Pos.CENTER);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdPedCidadesRep, VSdPedCidadesRep>, ObservableValue<VSdPedCidadesRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodCid() == null ? "" : param.getValue().getCodCid().getCodEst().getSigla()));
                            }).build(),
                            FormTableColumn.create(cln -> {
                                cln.title("População");
                                cln.width(80);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdPedCidadesRep, VSdPedCidadesRep>, ObservableValue<VSdPedCidadesRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodCid() == null ? "" : param.getValue().getCodCid().getPopCid()));
                            }).build(),
                            FormTableColumn.create(cln -> {
                                cln.title(colecoes.get(4).stream().filter(it -> it.getMarca().equals(marca)).findFirst().orElse(colecoes.get(4).get(0)).getColecao().getSdAbreviacao());
                                cln.alignment(Pos.CENTER);
                                cln.width(50);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdPedCidadesRep, VSdPedCidadesRep>, ObservableValue<VSdPedCidadesRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().isColecaoPassada() ? "1" : "0"));
                            }).build(),
                            FormTableColumn.create(cln -> {
                                cln.title(colecoes.get(3).stream().filter(it -> it.getMarca().equals(marca)).findFirst().orElse(colecoes.get(3).get(0)).getColecao().getSdAbreviacao());
                                cln.alignment(Pos.CENTER);
                                cln.width(50);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdPedCidadesRep, VSdPedCidadesRep>, ObservableValue<VSdPedCidadesRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().isColecaoAntiga() ? "1" : "0"));
                            }).build(),
                            FormTableColumn.create(cln -> {
                                cln.title(colecoes.get(2).stream().filter(it -> it.getMarca().equals(marca)).findFirst().orElse(colecoes.get(2).get(0)).getColecao().getSdAbreviacao());
                                cln.alignment(Pos.CENTER);
                                cln.width(50);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdPedCidadesRep, VSdPedCidadesRep>, ObservableValue<VSdPedCidadesRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().isColecaoReferencia() ? "1" : "0"));
                            }).build(),
                            FormTableColumn.create(cln -> {
                                cln.title("Ativa");
                                cln.getStyleClass().add("header-primary");
                                cln.alignment(Pos.CENTER);
                                cln.width(100);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdPedCidadesRep, VSdPedCidadesRep>, ObservableValue<VSdPedCidadesRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().isAtiva() ? "1" : "0"));
                            }).build(),
                            FormTableColumn.create(cln -> {
                                cln.title("Menos de 5");
                                cln.getStyleClass().add("danger-header");
                                cln.alignment(Pos.CENTER);
                                cln.width(100);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdPedCidadesRep, VSdPedCidadesRep>, ObservableValue<VSdPedCidadesRep>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                cln.setCellFactory(param -> new TableCell<VSdPedCidadesRep, VSdPedCidadesRep>() {
                                    @Override
                                    protected void updateItem(VSdPedCidadesRep item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        clear();
                                        if (item != null && !empty) {
                                            if (item.getCodCid() != null && item.isMenor5() && item.isColecaoAtual()) {
                                                getStyleClass().add("dark");
                                            } else if (item.getCodCid() != null && item.isMenor5() && item.isAtiva()) {
                                                getStyleClass().add("info-column");
                                            } else if (item.getCodCid() != null && item.isMenor5()) {
                                                getStyleClass().add("danger-column");
                                                setText("1");
                                            } else {
                                                getStyleClass().add("danger-column");
                                                setText("0");
                                            }
                                        }
                                    }

                                    private void clear() {
                                        getStyleClass().removeAll("info-column", "dark", "danger-column", "table-row-high-dark");
                                    }
                                });
                            }).build(),
                            FormTableColumn.create(cln -> {
                                cln.title("Entre 5 e 10");
                                cln.getStyleClass().add("warning-header");
                                cln.alignment(Pos.CENTER);
                                cln.width(102);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdPedCidadesRep, VSdPedCidadesRep>, ObservableValue<VSdPedCidadesRep>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                cln.setCellFactory(param -> new TableCell<VSdPedCidadesRep, VSdPedCidadesRep>() {
                                    @Override
                                    protected void updateItem(VSdPedCidadesRep item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        clear();
                                        if (item != null && !empty) {
                                            if (item.getCodCid() != null && item.isEntre5e10() && item.isColecaoAtual()) {
                                                getStyleClass().add("dark");
                                            } else if (item.getCodCid() != null && item.isEntre5e10() && item.isAtiva()) {
                                                getStyleClass().add("info-column");
                                            } else if (item.getCodCid() != null && item.isEntre5e10()) {
                                                getStyleClass().add("warning-column");
                                                setText("1");
                                            } else {
                                                getStyleClass().add("warning-column");
                                                setText("0");
                                            }
                                        }
                                    }

                                    private void clear() {
                                        getStyleClass().removeAll("info-column", "dark", "warning-column", "table-row-high-dark");
                                    }
                                });
                            }).build(),
                            FormTableColumn.create(cln -> {
                                cln.title("Entre 10 e 40");
                                cln.getStyleClass().add("warning-header");
                                cln.alignment(Pos.CENTER);
                                cln.width(102);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdPedCidadesRep, VSdPedCidadesRep>, ObservableValue<VSdPedCidadesRep>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                cln.setCellFactory(param -> new TableCell<VSdPedCidadesRep, VSdPedCidadesRep>() {
                                    @Override
                                    protected void updateItem(VSdPedCidadesRep item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        clear();
                                        if (item != null && !empty) {
                                            if (item.getCodCid() != null && item.isEntre10e40() && item.isColecaoAtual()) {
                                                getStyleClass().add("dark");
                                            } else if (item.getCodCid() != null && item.isEntre10e40() && item.isAtiva()) {
                                                getStyleClass().add("info-column");
                                            } else if (item.getCodCid() != null && item.isEntre10e40()) {
                                                getStyleClass().add("warning-column");
                                                setText("1");
                                            } else {
                                                getStyleClass().add("warning-column");
                                                setText("0");
                                            }
                                        }
                                    }

                                    private void clear() {
                                        getStyleClass().removeAll("info-column", "dark", "warning-column", "table-row-high-dark");
                                    }
                                });
                            }).build(),
                            FormTableColumn.create(cln -> {
                                cln.title("Entre 40 e 80");
                                cln.getStyleClass().add("warning-header");
                                cln.alignment(Pos.CENTER);
                                cln.width(101);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdPedCidadesRep, VSdPedCidadesRep>, ObservableValue<VSdPedCidadesRep>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                cln.setCellFactory(param -> new TableCell<VSdPedCidadesRep, VSdPedCidadesRep>() {
                                    @Override
                                    protected void updateItem(VSdPedCidadesRep item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        clear();
                                        if (item != null && !empty) {
                                            if (item.getCodCid() != null && item.isEntre40e80() && item.isColecaoAtual()) {
                                                getStyleClass().add("dark");
                                            } else if (item.getCodCid() != null && item.isEntre40e80() && item.isAtiva()) {
                                                getStyleClass().add("info-column");
                                            } else if (item.getCodCid() != null && item.isEntre40e80()) {
                                                getStyleClass().add("warning-column");
                                                setText("1");
                                            } else {
                                                getStyleClass().add("warning-column");
                                                setText("0");
                                            }
                                        }
                                    }

                                    private void clear() {
                                        getStyleClass().removeAll("info-column", "dark", "warning-column", "table-row-high-dark");
                                    }
                                });
                            }).build(),
                            FormTableColumn.create(cln -> {
                                cln.title("Acima de 80");
                                cln.getStyleClass().add("warning-header");
                                cln.alignment(Pos.CENTER);
                                cln.width(102);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdPedCidadesRep, VSdPedCidadesRep>, ObservableValue<VSdPedCidadesRep>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                cln.setCellFactory(param -> new TableCell<VSdPedCidadesRep, VSdPedCidadesRep>() {
                                    @Override
                                    protected void updateItem(VSdPedCidadesRep item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        clear();

                                        if (item != null && !empty) {
                                            if (item.getCodCid() != null && item.isMaior80() && item.isColecaoAtual()) {
                                                getStyleClass().add("dark");
                                            } else if (item.getCodCid() != null && item.isMaior80() && item.isAtiva()) {
                                                getStyleClass().add("info-column");
                                            } else if (item.getCodCid() != null && item.isMaior80()) {
                                                getStyleClass().add("warning-column");
                                                setText("1");
                                            } else {
                                                getStyleClass().add("warning-column");
                                                setText("0");
                                            }
                                        }
                                    }

                                    private void clear() {
                                        getStyleClass().removeAll("info-column", "dark", "warning-column", "table-row-high-dark");
                                    }
                                });
                            }).build(),
                            FormTableColumn.create(cln -> {
                                cln.title("");
                                cln.width(125);
                                cln.alignment(Pos.CENTER);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdPedCidadesRep, VSdPedCidadesRep>, ObservableValue<VSdPedCidadesRep>>) param -> new ReadOnlyObjectWrapper(""));
                            }).build(),
                            FormTableColumn.create(cln -> {
                                cln.title("PV");
                                cln.width(100);
                                cln.alignment(Pos.CENTER);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdPedCidadesRep, VSdPedCidadesRep>, ObservableValue<VSdPedCidadesRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPv()));
                            }).build(),
                            FormTableColumn.create(cln -> {
                                cln.title("TF");
                                cln.width(100);
                                cln.alignment(Pos.CENTER);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdPedCidadesRep, VSdPedCidadesRep>, ObservableValue<VSdPedCidadesRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTf() == null ? 0 : "R$ " + param.getValue().getTf().setScale(2, RoundingMode.CEILING)));
                            }).build(),
                            FormTableColumn.create(cln -> {
                                cln.title(colecoes.get(1).stream().filter(it -> it.getMarca().equals(marca)).findFirst().orElse(colecoes.get(1).get(0)).getColecao().getSdAbreviacao());
                                cln.alignment(Pos.CENTER);
                                cln.getStyleClass().add("header-high-dark");
                                cln.width(80);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdPedCidadesRep, VSdPedCidadesRep>, ObservableValue<VSdPedCidadesRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().isColecaoAtual() ? "1" : "0"));
                            }).build()

                    );
                }).build());
            }));
        });
    }

    private void criaVizualizacaoMarcaSt(Marca marca) {
        tabPaneSt.addTab(tab -> {
            Represen represen = tgGlobalSt.value.getValue() ? null : codrepSTFilter.value.getValue();
            List<VSdPedDataCidadesRep> cidades = buscarCidadesSt(represen, marca);
            ListProperty<VSdPedDataCidadesRep> cidadesStBean = new SimpleListProperty<>(FXCollections.observableArrayList());

            atualizaTabelaCidades(0, cidadesStBean, cidades);
            final Slider slider = new Slider();
            final FormFieldDate fieldFixo = FormFieldDate.create(field -> {
                field.title("Dia Fixo");
                field.width(180);
                field.addStyle("lg");
                field.value.setValue(colecaoAtual.get(0).getInicioVend());
            });
            final FormFieldDate fieldDiaAtual = FormFieldDate.create(field -> {
                field.title("Dia Atual");
                field.width(180);
                field.addStyle("lg");
                field.value.setValue(colecaoAtual.get(0).getInicioVend());
                field.value.addListener((observable, oldValue, newValue) -> {
                    if (newValue != null) {
                        if (newValue.isAfter(LocalDate.now()) || newValue.isBefore(colecaoAtual.get(0).getInicioVend())) {
                            MessageBox.create(message -> {
                                message.message("Data informada não estão no período permitido:\n Início da coleção e Data Atual");
                                message.type(MessageBox.TypeMessageBox.WARNING);
                                message.position(Pos.TOP_RIGHT);
                                message.notification();
                            });
                            field.value.setValue(oldValue);
                            return;
                        }
                        slider.setValue(Duration.between(colecaoAtual.get(0).getInicioVend().atStartOfDay(), newValue.atStartOfDay()).toDays());
                    }
                });
                slider.valueProperty().addListener((observable, oldValue, newValue) -> {
                    if (newValue != null)
                        field.value.setValue(colecaoAtual.get(0).getInicioVend().plusDays(newValue.longValue()));
                });
            });

            final FormFieldText fieldCidadesAtivas = FormFieldText.create(field -> {
                field.title("Cidades Ativas");
                field.editable.set(false);
                field.alignment(Pos.CENTER);
                field.addStyle("lg").addStyle("info");
                field.value.setValue(String.valueOf(cidadesStBean.stream().filter(VSdPedDataCidadesRep::isAtiva).count()));
                cidadesStBean.addListener((observable, oldValue, newValue) -> {
                    if (newValue != null) {
                        field.value.setValue(String.valueOf(cidadesStBean.stream().filter(VSdPedDataCidadesRep::isAtiva).count()));
                    }
                });
            });

            final FormFieldText fieldTotalCidades = FormFieldText.create(field -> {
                field.title("Total Cidades");
                field.editable.set(false);
                field.alignment(Pos.CENTER);
                field.addStyle("lg").addStyle("warning");
                field.value.setValue(String.valueOf(cidadesStBean.size()));
            });

            final FormFieldText fieldCidadesColAtual = FormFieldText.create(field -> {
                field.title("Cidades Coleção Atual");
                field.editable.set(false);
                field.alignment(Pos.CENTER);
                field.addStyle("lg").addStyle("success");
                field.value.setValue("0");
                cidadesStBean.addListener((observable, oldValue, newValue) -> {
                    if (newValue != null) {
                        field.value.setValue(String.valueOf(newValue.stream().filter(it -> it.getPv() > 0).count()));
                    }
                });
            });

            final FormTableView tblCidades = FormTableView.create(SdCidadeRep.class, table -> {
                table.title("Cidades");
                table.expanded();
                table.withoutCounter();
                table.items.bind(cidadesStBean);
                fieldFixo.value.addListener((observable, oldValue, newValue) -> {
                    if (newValue.isAfter(LocalDate.now()) || newValue.isBefore(colecaoAtual.get(0).getInicioVend())) {
                        MessageBox.create(message -> {
                            message.message("Data informada não estão no período permitido:\n Início da coleção e Data Atual");
                            message.type(MessageBox.TypeMessageBox.WARNING);
                            message.position(Pos.TOP_RIGHT);
                            message.notification();
                        });
                        fieldFixo.value.setValue(oldValue);
                        return;
                    }
                    fixarData(cidadesStBean, cidades, fieldFixo.value.getValue());
                    table.refresh();
                });
                table.contextMenu(FormContextMenu.create(cxM -> {
                    MenuItem fixarDataMenuItem = new MenuItem();
                    fixarDataMenuItem.setText("Fixar Data");
                    fixarDataMenuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.BLOQUEAR, ImageUtils.IconSize._16));
                    fixarDataMenuItem.setOnAction(evt -> {
                        fixarData(cidadesStBean, cidades, fieldFixo.value.getValue());
                        fieldFixo.value.setValue(colecaoAtual.get(0).getInicioVend().plusDays(Double.valueOf(slider.getValue()).longValue()));
                        table.refresh();
                    });
                    MenuItem desfixarDataMenuItem = new MenuItem();
                    desfixarDataMenuItem.setText("Desfixar Data");
                    desfixarDataMenuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._16));
                    desfixarDataMenuItem.setOnAction(evt -> {
                        desfixarData(cidadesStBean);
                        fieldFixo.value.setValue(colecaoAtual.get(0).getInicioVend());
                        table.refresh();
                    });
                    cxM.addItem(fixarDataMenuItem);
                    cxM.addItem(desfixarDataMenuItem);
                }));
                table.columns(
                        FormTableColumn.create(cln -> {
                            cln.title("Cidade");
                            cln.width(300);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdPedDataCidadesRep, VSdPedDataCidadesRep>, ObservableValue<VSdPedDataCidadesRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodCid() == null ? "" : param.getValue().getCodCid().getNomeCid()));
                        }).build(),
                        FormTableColumn.create(cln -> {
                            cln.title("UF");
                            cln.width(100);
                            cln.alignment(Pos.CENTER);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdPedDataCidadesRep, VSdPedDataCidadesRep>, ObservableValue<VSdPedDataCidadesRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodCid() == null ? "" : param.getValue().getCodCid().getCodEst().getSigla()));
                        }).build(),
                        FormTableColumn.create(cln -> {
                            cln.title("População");
                            cln.width(150);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdPedDataCidadesRep, VSdPedDataCidadesRep>, ObservableValue<VSdPedDataCidadesRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodCid() == null ? "" : param.getValue().getCodCid().getPopCid()));
                        }).build(),
                        FormTableColumn.create(cln -> {
                            cln.title("Ativa");
                            cln.alignment(Pos.CENTER);
                            cln.width(70);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdPedDataCidadesRep, VSdPedDataCidadesRep>, ObservableValue<VSdPedDataCidadesRep>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                            cln.format(param -> {
                                return new TableCell<VSdPedDataCidadesRep, VSdPedDataCidadesRep>() {
                                    @Override
                                    protected void updateItem(VSdPedDataCidadesRep item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        if (item != null && !empty) {
                                            setGraphic(ImageUtils.getIcon(item.isAtiva() ? ImageUtils.Icon.STATUS_VERDE : ImageUtils.Icon.STATUS_VERMELHO, ImageUtils.IconSize._16));
                                        }
                                    }
                                };
                            });
                        }).build(),
                        FormTableColumnGroup.createGroup(group -> {
                            group.title("Data Fixada");
                            group.getStyleClass().add("header-info");
                            group.addColumn(FormTableColumn.create(cln -> {
                                cln.title("PV");
                                cln.width(100);
                                cln.getStyleClass().add("header-info");
                                cln.alignment(Pos.CENTER);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdPedDataCidadesRep, VSdPedDataCidadesRep>, ObservableValue<VSdPedDataCidadesRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPvFixo()));
                            }).build());
                            group.addColumn(FormTableColumn.create(cln -> {
                                cln.title("TF");
                                cln.width(150);
                                cln.getStyleClass().add("header-info");
                                cln.alignment(Pos.CENTER_RIGHT);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdPedDataCidadesRep, VSdPedDataCidadesRep>, ObservableValue<VSdPedDataCidadesRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValorFixo() == null ? 0 : "R$ " + param.getValue().getValorFixo().setScale(2, RoundingMode.CEILING)));
                            }).build());
                        }).build(), /*Data Fixada*/
                        FormTableColumnGroup.createGroup(group -> {
                            group.title("Data Atual");
                            group.getStyleClass().add("header-success");
                            group.addColumn(FormTableColumn.create(cln -> {
                                cln.title("PV");
                                cln.width(100);
                                cln.alignment(Pos.CENTER);
                                cln.getStyleClass().add("header-success");
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdPedDataCidadesRep, VSdPedDataCidadesRep>, ObservableValue<VSdPedDataCidadesRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPv()));
                            }).build());
                            group.addColumn(FormTableColumn.create(cln -> {
                                cln.title("TF");
                                cln.width(150);
                                cln.alignment(Pos.CENTER_RIGHT);
                                cln.getStyleClass().add("header-success");
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdPedDataCidadesRep, VSdPedDataCidadesRep>, ObservableValue<VSdPedDataCidadesRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValor() == null ? 0 : "R$ " + param.getValue().getValor().setScale(2, RoundingMode.CEILING)));
                            }).build());
                        }).build(), /*Data Fixada*/
                        FormTableColumnGroup.createGroup(group -> {
                            group.title("Diferença");
                            group.getStyleClass().add("header-warning");
                            group.addColumn(FormTableColumn.create(cln -> {
                                cln.title("PV");
                                cln.width(100);
                                cln.getStyleClass().add("header-warning");
                                cln.alignment(Pos.CENTER);
                                cln.setCellFactory(new Callback<TableColumn<VSdPedDataCidadesRep, VSdPedDataCidadesRep>, TableCell<VSdPedDataCidadesRep, VSdPedDataCidadesRep>>() {
                                    @Override
                                    public TableCell<VSdPedDataCidadesRep, VSdPedDataCidadesRep> call(TableColumn<VSdPedDataCidadesRep, VSdPedDataCidadesRep> param) {
                                        return new TableCell<VSdPedDataCidadesRep, VSdPedDataCidadesRep>() {
                                            @Override
                                            protected void updateItem(VSdPedDataCidadesRep item, boolean empty) {
                                                super.updateItem(item, empty);
                                                clear();
                                                if (!empty) {
                                                    setText(String.valueOf(item.getPv() - item.getPvFixo()));
                                                    if (!item.isAtiva())
                                                        return;

                                                    if (item.getPv() - item.getPvFixo() == 0)
                                                        setStyle(getStyle() + "-fx-background-color: #ffeeba;");
                                                    else if (item.getPv() - item.getPvFixo() > 0) {
                                                        setStyle(getStyle() + "-fx-background-color: #c3e6cb;");
                                                        setText("+ " + getText());
                                                    } else
                                                        setStyle(getStyle() + "-fx-background-color: #f5c6cb;");
                                                }
                                            }

                                            protected void clear() {
                                                setStyle("");
                                            }
                                        };
                                    }
                                });
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdPedDataCidadesRep, VSdPedDataCidadesRep>, ObservableValue<VSdPedDataCidadesRep>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                            }).build());
                            group.addColumn(FormTableColumn.create(cln -> {
                                cln.title("TF");
                                cln.width(150);
                                cln.getStyleClass().add("header-warning");
                                cln.alignment(Pos.CENTER_RIGHT);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdPedDataCidadesRep, VSdPedDataCidadesRep>, ObservableValue<VSdPedDataCidadesRep>>) param -> new ReadOnlyObjectWrapper("R$ " + (param.getValue().getValor() == null ? BigDecimal.ZERO : param.getValue().getValor()).subtract(param.getValue().getValorFixo() == null ? BigDecimal.ZERO : param.getValue().getValorFixo()).setScale(2, RoundingMode.CEILING)));
                            }).build());
                        }).build() /*Data Fixada*/
                );
            });
            tab.setId(marca.getCodigo());
            tab.setText(marca.getDescricao().toUpperCase());
            tab.setContent(FormBox.create(root -> {
                root.vertical();
                root.add(header -> {
                    header.horizontal();
                    header.add(fieldDiaAtual.build());
                    header.add(fieldFixo.build());
                    header.add(FormFieldText.create(field -> {
                        field.title("Dia Inicial");
                        field.editable.set(false);
                        field.addStyle("lg").addStyle("warning");
                        field.value.setValue(StringUtils.toDateFormat(colecaoAtual.get(0).getInicioVend()));
                    }).build());
                    header.add(box -> {
                        box.vertical();
                        box.width(250);
                    });
                    header.add(fieldTotalCidades.build(), fieldCidadesAtivas.build(), fieldCidadesColAtual.build());
                });
                root.add(FormBox.create(boxCenter -> {
                    boxCenter.vertical();
                    boxCenter.expanded();
                    boxCenter.add(tblCidades.build());
                }));
                root.add(FormBox.create(boxFooter -> {
                    boxFooter.vertical();
                    configuraSlider(cidades, cidadesStBean, slider, tblCidades);
                    boxFooter.add(slider);
                }));
            }));
        });
    }

    private void configuraSlider(List<VSdPedDataCidadesRep> cidades, ListProperty<VSdPedDataCidadesRep> cidadesStBean, Slider slider, FormTableView tblCidades) {
        slider.setMin(0);
        slider.setValue(0);
        slider.setPadding(new Insets(10));
        slider.setMax(Duration.between(colecaoAtual.get(0).getInicioVend().atStartOfDay(), LocalDateTime.now()).toDays());
        slider.setShowTickLabels(true);
        slider.setShowTickMarks(true);
        slider.setSnapToTicks(true);
        slider.setMajorTickUnit(7);
        slider.setMinorTickCount(7);
        slider.setBlockIncrement(7);
        slider.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                atualizaTabelaCidades(slider.getValue(), cidadesStBean, cidades);
                tblCidades.refresh();
            }
        });
        StringConverter<Double> stringConverter = new StringConverter() {
            @Override
            public String toString(Object object) {
                return StringUtils.toShortDateFormat(colecaoAtual.get(0).getInicioVend().plusDays(Double.valueOf(object.toString()).longValue()));
            }

            @Override
            public Double fromString(String string) {
                return null;
            }
        };
        slider.setLabelFormatter(stringConverter);
        tGroup.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            switch (((RadioButton) newValue).getId()) {
                case "S":
                    slider.setMajorTickUnit(7);
                    slider.setMinorTickCount(7);
                    slider.setBlockIncrement(7);
                    break;
                case "M":
                    slider.setMajorTickUnit(30);
                    slider.setMinorTickCount(30);
                    slider.setBlockIncrement(30);
                    break;
                case "D":
                    slider.setMajorTickUnit(1);
                    slider.setMinorTickCount(1);
                    slider.setBlockIncrement(1);
                    break;
            }
        });
    }

    private void carregaVariaveis(List<VSdPedCidadesRep> cidadesList) {
        cidadesColAtual = String.valueOf(cidadesList.stream().filter(VSdPedCidadesRep::isColecaoAtual).count());
        cidadesCobertas = String.valueOf(cidadesList.stream().filter(it -> it.isAtiva() || it.isColecaoAtual()).count());
        cidadesBaixaPerformance = String.valueOf(cidadesList.stream().filter(it -> it.isMenor5() && !it.isAtiva() && !it.isColecaoAtual()).count());
        cidadesEntre5e10 = String.valueOf(cidadesList.stream().filter(it -> it.isEntre5e10() && !it.isAtiva() && !it.isColecaoAtual()).count());
        cidadesEntre10e40 = String.valueOf(cidadesList.stream().filter(it -> it.isEntre10e40() && !it.isAtiva() && !it.isColecaoAtual()).count());
        cidadesEntre40e80 = String.valueOf(cidadesList.stream().filter(it -> it.isEntre40e80() && !it.isAtiva() && !it.isColecaoAtual()).count());
        cidadesMaior80 = String.valueOf(cidadesList.stream().filter(it -> it.isMaior80() && !it.isAtiva() && !it.isColecaoAtual()).count());
        somaTotalCidades = (Integer.parseInt(cidadesEntre5e10) + Integer.parseInt(cidadesEntre10e40) + Integer.parseInt(cidadesEntre40e80) + Integer.parseInt(cidadesMaior80)) + " Cidades";
        primeiroResultado = String.valueOf(Integer.parseInt(cidadesEntre5e10));
        segundoResultado = String.valueOf(Integer.parseInt(cidadesEntre10e40));
        terceiroResultado = String.valueOf(Integer.parseInt(cidadesEntre40e80) * 2);
        quartoResultado = String.valueOf(Integer.parseInt(cidadesMaior80) * 3);
        somaResultados = String.valueOf(Integer.parseInt(primeiroResultado) + Integer.parseInt(segundoResultado) + Integer.parseInt(terceiroResultado) + Integer.parseInt(quartoResultado)) + " Clientes";
        pvTotal = String.valueOf(cidadesList.stream().mapToInt(VSdPedCidadesRep::getPv).sum());
        tfTotal = "R$ " + BigDecimal.valueOf(cidadesList.stream().mapToDouble(it -> it.getTf().doubleValue()).sum()).setScale(2, RoundingMode.CEILING);
        totalCidades = String.valueOf(cidadesList.size());
    }

    private void carregaCampos() {
        limparCampos();
        carregaVariaveis(cidades);
        if (pca == null)
            pca = new SdPcaCodRepMarca();
        prazoCurtoMesesField.value.setValue(String.valueOf(pca.getpCurtoMes()));
        prazoCurtoCidadesField.value.setValue(String.valueOf(pca.getpCurtoCid()));
        prazoMedioMesesField.value.setValue(String.valueOf(pca.getpMedioMes()));
        prazoMedioCidadesField.value.setValue(String.valueOf(pca.getpMedioCid()));
        prazoLongoMesesField.value.setValue(String.valueOf(pca.getpLongoMes()));
        prazoLongoCidadesField.value.setValue(String.valueOf(pca.getpLongoCid()));

        cidadesColAtualField.label(colecoes.get(1).stream().filter(it -> it.getMarca().equals(marcaSelecionada.get())).findFirst().orElseGet(() -> colecoes.get(1).get(0)).getColecao().getSdAbreviacao());
        cidadesColAtualField.value.setValue(cidadesColAtual);
        cidadesCobertasField.value.setValue(cidadesCobertas);
        cidadesBaixaPerformanceField.value.setValue(cidadesBaixaPerformance);
        entre5e10TotalField.value.setValue(cidadesEntre5e10);
        entre10e40TotalField.value.setValue(cidadesEntre10e40);
        entre40e80TotalField.value.setValue(cidadesEntre40e80);
        maior80TotalField.value.setValue(cidadesMaior80);
        somaTotalCidadesField.value.setValue(somaTotalCidades);
        primeiroResultadoField.value.setValue(primeiroResultado);
        segundoResultadoField.value.setValue(segundoResultado);
        terceiroResultadoField.value.setValue(terceiroResultado);
        quartoResultadoField.value.setValue(quartoResultado);
        somaResultadoTotalField.value.setValue(somaResultados);
        textAreaPca.value.setValue("POTENCIAL ESTIMADO DE DESENVOLVER MAIS DE " + somaTotalCidades.toUpperCase());

        pvTotalField.value.setValue(pvTotal);
        tfTotalField.value.setValue(tfTotal);

        totalCidadesField.value.setValue("Total Cidades: " + totalCidades);

        dtCriacaoField.value.setValue("Marco Zero: " + StringUtils.toShortDateFormat(pca.getDtCriacao()));
        dtUltAtualizacaoField.value.setValue("Ult. Atualização: " + StringUtils.toShortDateFormat(pca.getDtUltAtualizacao()));
    }

    private void limparCampos() {
        cidadesColAtualField.clear();
        cidadesCobertasField.clear();
        cidadesBaixaPerformanceField.clear();
        entre5e10TotalField.clear();
        entre10e40TotalField.clear();
        entre40e80TotalField.clear();
        maior80TotalField.clear();
        somaTotalCidadesField.clear();
        primeiroResultadoField.clear();
        segundoResultadoField.clear();
        terceiroResultadoField.clear();
        quartoResultadoField.clear();
        somaResultadoTotalField.clear();

        prazoCurtoMesesField.clear();
        prazoCurtoCidadesField.clear();
        prazoMedioMesesField.clear();
        prazoMedioCidadesField.clear();
        prazoLongoMesesField.clear();
        prazoLongoCidadesField.clear();

        pvTotalField.clear();
        pvTotalField.clear();
    }

    private void atualizaTabelaCidades(double value, ListProperty<VSdPedDataCidadesRep> cidadesDataBean, List<VSdPedDataCidadesRep> cidadesBean) {
        Map<Cidade, List<VSdPedDataCidadesRep>> collect = cidadesBean.stream().collect(Collectors.groupingBy(VSdPedDataCidadesRep::getCodCid));
        List<VSdPedDataCidadesRep> temp = new ArrayList<>(cidadesDataBean);
        cidadesDataBean.clear();
        collect.forEach((cidade, list) -> {
            VSdPedDataCidadesRep cidadeSt = temp.stream().filter(it -> it.getCodCid().getCodCid().equals(cidade.getCodCid())).findFirst().orElse(new VSdPedDataCidadesRep(cidade));
            cidadeSt.setAtiva(list.stream().anyMatch(VSdPedDataCidadesRep::isAtiva));
            list = list.stream().filter(it -> it.getDtEmissao().isBefore(colecaoAtual.get(0).getInicioVend().plusDays((long) value)) && it.getDtEmissao().isAfter(colecaoAtual.get(0).getInicioVend())).collect(Collectors.toList());
            cidadeSt.setAtiva(cidadeSt.isAtiva() || list.size() > 0);
            cidadeSt.setValor(BigDecimal.valueOf(list.stream().mapToDouble(it -> it.getValor().doubleValue()).sum()));
            cidadeSt.setPv((int) list.stream().filter(it -> it.getCodcli() != null).map(VSdPedDataCidadesRep::getCodcli).distinct().count());
            cidadesDataBean.add(cidadeSt);
        });
        cidadesDataBean.sort(Comparator.comparing(o -> o.getCodCid().getNomeCid()));
    }

    private void salvarPca(boolean autoSave) {
        if (marcaSelecionada == null || represenSelecionado == null) {
            MessageBox.create(message -> {
                message.message("Representante e marca não selecionados!");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            return;
        }

        if (autoSave || ((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Tem certeza que deseja salvar o PCA?");
            message.showAndWait();
        }).value.get())) {
            SdPcaCodRepMarca novoPca = getNovoPca();
            if (novoPca == null) return;
            savePca(novoPca);
            dtUltAtualizacaoField.value.setValue("Ult. Atualização: " + StringUtils.toShortDateFormat(pca.getDtUltAtualizacao()));
            dtCriacaoField.value.setValue("Marco Zero: " + StringUtils.toShortDateFormat(pca.getDtCriacao()));
            MessageBox.create(message -> {
                message.message("PCA salvo com sucesso!");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
        }
    }

    private void savePca(SdPcaCodRepMarca novoPca) {
        if (novoPca.getDtCriacao() == null) {
            novoPca.setDtCriacao(LocalDate.now());
        }
        if (pca == null || pca.getCodRep() == null) {
            try {
                novoPca = new FluentDao().persist(novoPca);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            novoPca = new FluentDao().merge(novoPca);
        }

        SysLogger.addSysDelizLog("PCA", TipoAcao.SALVAR, represenSelecionado.getCodRep() + "-" + marcaSelecionada.getValue().getCodigo(), "PCA do Representante: " + represenSelecionado.getCodRep() + " na marca: " + marcaSelecionada.getValue().getCodigo() + " salvo com sucesso!");

        pca = novoPca;
    }

    private SdPcaCodRepMarca getNovoPca() {
        SdPcaCodRepMarca novoPca = new SdPcaCodRepMarca();

        if (
                (prazoCurtoCidadesField.value.isEmpty().get() || prazoCurtoCidadesField.value.isNull().get()) ||
                        (prazoCurtoMesesField.value.isEmpty().get() || prazoCurtoMesesField.value.isNull().get()) ||
                        (prazoMedioCidadesField.value.isEmpty().get() || prazoMedioCidadesField.value.isNull().get()) ||
                        (prazoMedioMesesField.value.isEmpty().get() || prazoMedioMesesField.value.isNull().get()) ||
                        (prazoLongoCidadesField.value.isEmpty().get() || prazoLongoCidadesField.value.isNull().get()) ||
                        (prazoLongoMesesField.value.isEmpty().get() || prazoLongoMesesField.value.isNull().get())
        ) {
            return null;
        }

        novoPca.setCodRep(represenSelecionado.getCodRep());
        novoPca.setMarca(marcaSelecionada.get().getCodigo());

        novoPca.setpCurtoCid(Integer.parseInt(prazoCurtoCidadesField.value.getValue()));
        novoPca.setpCurtoMes(Integer.parseInt(prazoCurtoMesesField.value.getValue()));
        novoPca.setpMedioCid(Integer.parseInt(prazoMedioCidadesField.value.getValue()));
        novoPca.setpMedioMes(Integer.parseInt(prazoMedioMesesField.value.getValue()));
        novoPca.setpLongoCid(Integer.parseInt(prazoLongoCidadesField.value.getValue()));
        novoPca.setpLongoMes(Integer.parseInt(prazoLongoMesesField.value.getValue()));

        novoPca.setDtUltAtualizacao(LocalDate.now());
        return novoPca;
    }

    private void exportarExcel() {
        if (!tgGlobal.value.getValue() && pca != null && !pca.equals(getNovoPca())) {
            if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                message.message("Para exportar o excel é necessário salvar o PCA.\nDeseja fazer isso agora?");
                message.showAndWait();
            }).value.get())) {
                salvarPca(true);
            } else return;
        }

        String filePath = GUIUtils.showSaveFileDialog(Collections.singletonList(new FileChooser.ExtensionFilter("Arquivos do Excel (.xlsx)", "*.xlsx")));
        if (filePath != null && !filePath.equals("")) {
            new RunAsyncWithOverlay(this).exec(task -> {
                try {
                    ExportUtils.ExcelExportMode workBook = new ExportUtils().excel(filePath);
                    for (Sheet sheet : workBook.getWorkbook()) {

                    }
                    for (Marca marca : marcasRep) {
                        ListProperty<VSdPedCidadesRep> cidadesBean = new SimpleListProperty(FXCollections.observableArrayList(buscarCidades(represenSelecionado, marca, colecaoFilter.value.getValue(), semanaComboBox.value.getValue())));
                        SdPcaCodRepMarca pcaCodRepMarca = getPcaCodRepMarca(represenSelecionado, marca);
                        carregaVariaveis(cidadesBean);
                        workBook.addSheet(marca.getDescricao(), builder -> generateSheet(builder, cidadesBean, pcaCodRepMarca, marca));
                    }
                    workBook.export();
                } catch (IOException | IllegalAccessException e) {
                    e.printStackTrace();
                }
                return ReturnAsync.OK.value;
            }).addTaskEndNotification(taskReturn -> {
                if (taskReturn.equals(ReturnAsync.OK.value)) {
                    MessageBox.create(message -> {
                        message.message("Excel exportado com sucesso!");
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.position(Pos.TOP_RIGHT);
                        message.notification();
                    });
                }
            });
        }
    }

    private void generateSheet(ExportUtils.ExcelSheet builder, List<VSdPedCidadesRep> listCidades, SdPcaCodRepMarca pcaCodRepMarca, Marca marca) {
        int rowNum = 0;
        if (pcaCodRepMarca == null) pcaCodRepMarca = new SdPcaCodRepMarca();
        Row rowHeader = builder.sheet.createRow(rowNum++);
        org.apache.poi.ss.usermodel.Font headerFont = builder.workbook.createFont();
        headerFont.setBold(true);

        builder.cellFormatValue(rowHeader, 0, "Cidade", "string");
        builder.cellFormatValue(rowHeader, 1, "UF", "string");
        builder.cellFormatValue(rowHeader, 2, "População", "string");
        builder.cellFormatValue(rowHeader, 3, colecoes.get(4).stream().filter(it -> it.getMarca().equals(marca)).findFirst().orElseGet(() -> colecoes.get(4).get(0)).getColecao().getSdAbreviacao(), "string");
        builder.cellFormatValue(rowHeader, 4, colecoes.get(3).stream().filter(it -> it.getMarca().equals(marca)).findFirst().orElseGet(() -> colecoes.get(3).get(0)).getColecao().getSdAbreviacao(), "string");
        builder.cellFormatValue(rowHeader, 5, colecoes.get(2).stream().filter(it -> it.getMarca().equals(marca)).findFirst().orElseGet(() -> colecoes.get(2).get(0)).getColecao().getSdAbreviacao(), "string");
        builder.cellFormatValue(rowHeader, 6, "Ativas", "string");
        builder.cellFormatValue(rowHeader, 7, "Menos de 5", "string");
        builder.cellFormatValue(rowHeader, 8, "Entre 5 e 10", "string");
        builder.cellFormatValue(rowHeader, 9, "Entre 10 e 40", "string");
        builder.cellFormatValue(rowHeader, 10, "Entre 40 e 80", "string");
        builder.cellFormatValue(rowHeader, 11, "Acima de 80", "string");
        builder.cellFormatValue(rowHeader, 12, "", "string");
        builder.cellFormatValue(rowHeader, 13, "", "string");
        builder.cellFormatValue(rowHeader, 14, "PV", "string");
        builder.cellFormatValue(rowHeader, 15, "TF", "string");
        builder.cellFormatValue(rowHeader, 16, colecoes.get(1).stream().filter(it -> it.getMarca().equals(marca)).findFirst().orElseGet(() -> colecoes.get(1).get(0)).getColecao().getSdAbreviacao(), "string");

        for (VSdPedCidadesRep cidade : listCidades) {
            Row row = builder.sheet.createRow(rowNum++);

            builder.cellFormatValue(row, 0, cidade.getCodCid().getNomeCid(), "string");
            builder.cellFormatValue(row, 1, cidade.getCodCid().getCodEst().getSigla(), "string");
            builder.cellFormatValue(row, 2, cidade.getCodCid().getPopCid().toString(), "integer");
            builder.cellFormatValue(row, 3, cidade.isColecaoReferencia() ? "1" : "0", "integer");
            builder.cellFormatValue(row, 4, cidade.isColecaoAntiga() ? "1" : "0", "integer");
            builder.cellFormatValue(row, 5, cidade.isColecaoPassada() ? "1" : "0", "integer");
            builder.cellFormatValue(row, 6, cidade.isAtiva() ? "1" : "0", "integer");
            builder.cellFormatValue(row, 7, cidade.isMenor5() ? "1" : "0", "integer");
            builder.cellFormatValue(row, 8, cidade.isEntre5e10() ? "1" : "0", "integer");
            builder.cellFormatValue(row, 9, cidade.isEntre10e40() ? "1" : "0", "integer");
            builder.cellFormatValue(row, 10, cidade.isEntre40e80() ? "1" : "0", "integer");
            builder.cellFormatValue(row, 11, cidade.isMaior80() ? "1" : "0", "integer");
            builder.cellFormatValue(row, 12, "", "string");
            builder.cellFormatValue(row, 13, "", "string");
            builder.cellFormatValue(row, 14, String.valueOf(cidade.getPv()), "integer");
            builder.cellFormatValue(row, 15, cidade.getTf().setScale(2, RoundingMode.CEILING).toString(), "bigdecimal");
            builder.cellFormatValue(row, 16, cidade.isColecaoAtual() ? "1" : "0", "integer");

            CellStyle styleEscolhido = builder.workbook.createCellStyle();

            if (cidade.isColecaoAtual()) {
                styleEscolhido = blackCenterStyle;
            } else if (cidade.isAtiva()) {
                styleEscolhido = blueStyleCenter;
            } else {
                styleEscolhido = stylePadraoCenter;
            }

            for (int i = 0; i <= 16; i++) {
                row.getCell(i).setCellStyle(styleEscolhido);
            }

            row.getCell(7).setCellStyle(redStyle);
            row.getCell(8).setCellStyle(yellowStyle);
            row.getCell(9).setCellStyle(yellowStyle);
            row.getCell(10).setCellStyle(yellowStyle);
            row.getCell(11).setCellStyle(yellowStyle);

            if (cidade.isColecaoAtual()) {
                row.getCell(0).setCellStyle(blackStyle);
                row.getCell(15).setCellStyle(blackStyle);
            } else if (cidade.isAtiva()) {
                row.getCell(0).setCellStyle(blueStyle);
                row.getCell(15).setCellStyle(blueStyle);
            } else {
                row.getCell(0).setCellStyle(null);
                row.getCell(15).setCellStyle(null);
            }

            if (cidade.isColecaoAtual() || cidade.isAtiva()) {
                if (cidade.isMenor5()) {
                    Cell cell = row.getCell(7);
                    cell.setCellStyle(styleEscolhido);
                    cell.setCellValue("");
                } else if (cidade.isEntre5e10()) {
                    Cell cell = row.getCell(8);
                    cell.setCellStyle(styleEscolhido);
                    cell.setCellValue("");
                } else if (cidade.isEntre10e40()) {
                    Cell cell = row.getCell(9);
                    cell.setCellStyle(styleEscolhido);
                    cell.setCellValue("");
                } else if (cidade.isEntre40e80()) {
                    Cell cell = row.getCell(10);
                    cell.setCellStyle(styleEscolhido);
                    cell.setCellValue("");
                } else {
                    Cell cell = row.getCell(11);
                    cell.setCellStyle(styleEscolhido);
                    cell.setCellValue("");
                }
            }
        }
        rowNum++;
        Row row = builder.sheet.createRow(rowNum++);
        builder.cellFormatValue(row, 0, "TOTAL DE CIDADES DA ÁREA:   " + listCidades.size(), "string");
        builder.cellFormatValue(row, 6, cidadesCobertas, "string");
        builder.cellFormatValue(row, 7, cidadesBaixaPerformance, "string");
        builder.cellFormatValue(row, 8, cidadesEntre5e10, "string");
        builder.cellFormatValue(row, 9, cidadesEntre10e40, "string");
        builder.cellFormatValue(row, 10, cidadesEntre40e80, "string");
        builder.cellFormatValue(row, 11, cidadesMaior80, "string");
        builder.cellFormatValue(row, 12, somaTotalCidades, "string");
        builder.cellFormatValue(row, 13, " Cidades", "string");
        builder.cellFormatValue(row, 14, pvTotal, "string");
        builder.cellFormatValue(row, 15, tfTotal, "string");
        builder.cellFormatValue(row, 16, cidadesColAtual, "string");

        row.getCell(0).setCellStyle(blueStyle);
        row.getCell(6).setCellStyle(blueStyleCenter);
        row.getCell(7).setCellStyle(redStyle);
        row.getCell(8).setCellStyle(yellowStyle);
        row.getCell(9).setCellStyle(yellowStyle);
        row.getCell(10).setCellStyle(yellowStyle);
        row.getCell(11).setCellStyle(yellowStyle);
        row.getCell(12).setCellStyle(yellowStyle);
        row.getCell(13).setCellStyle(yellowStyle);
        row.getCell(14).setCellStyle(primaryStyle);
        row.getCell(15).setCellStyle(primaryStyle);
        row.getCell(16).setCellStyle(blackCenterStyle);

        row = builder.sheet.createRow(rowNum++);
        builder.cellFormatValue(row, 8, "1", "string");
        builder.cellFormatValue(row, 9, "1", "string");
        builder.cellFormatValue(row, 10, "2", "string");
        builder.cellFormatValue(row, 11, "3", "string");

        row.getCell(8).setCellStyle(amberStyle);
        row.getCell(9).setCellStyle(amberStyle);
        row.getCell(10).setCellStyle(amberStyle);
        row.getCell(11).setCellStyle(amberStyle);

        row = builder.sheet.createRow(rowNum++);
        builder.cellFormatValue(row, 8, primeiroResultado, "string");
        builder.cellFormatValue(row, 9, segundoResultado, "string");
        builder.cellFormatValue(row, 10, terceiroResultado, "string");
        builder.cellFormatValue(row, 11, quartoResultado, "string");
        builder.cellFormatValue(row, 12, somaResultados, "string");
        builder.cellFormatValue(row, 13, " Clientes", "string");

        row.getCell(8).setCellStyle(successStyle);
        row.getCell(9).setCellStyle(successStyle);
        row.getCell(10).setCellStyle(successStyle);
        row.getCell(11).setCellStyle(successStyle);
        row.getCell(12).setCellStyle(successStyle);
        row.getCell(13).setCellStyle(successStyle);

        //<editor-fold desc="PCA">
        if (!tgGlobal.value.getValue()) {
            rowNum += 2;
            row = builder.sheet.createRow(rowNum++);

            builder.cellFormatValue(row, 0, "PCA", "string");
            builder.cellFormatValue(row, 2, "PRAZO", "string");
            builder.cellFormatValue(row, 3, "MESES", "string");
            builder.cellFormatValue(row, 4, "CIDADES", "string");

            row.getCell(0).setCellStyle(blueStyleCenter);
            row.getCell(2).setCellStyle(blueStyleCenter);
            row.getCell(3).setCellStyle(blueStyleCenter);
            row.getCell(4).setCellStyle(blueStyleCenter);

            row = builder.sheet.createRow(rowNum++);

            builder.cellFormatValue(row, 0, "POTENCIAL ESTIMADO DE DESENVOLVER MAIS DE " + somaTotalCidades.toUpperCase(), "string");
            builder.cellFormatValue(row, 2, "CURTO", "string");
            builder.cellFormatValue(row, 3, String.valueOf(pcaCodRepMarca.getpCurtoMes()), "string");
            builder.cellFormatValue(row, 4, String.valueOf(pcaCodRepMarca.getpCurtoCid()), "string");

            row.getCell(0).setCellStyle(blueStyleCenter);
            row.getCell(2).setCellStyle(blueStyleCenter);
            row.getCell(3).setCellStyle(stylePadraoCenter);
            row.getCell(4).setCellStyle(stylePadraoCenter);

            builder.sheet.addMergedRegion(new CellRangeAddress(row.getRowNum(), row.getRowNum() + 2, 0, 0));
            row.getCell(0).getCellStyle().setWrapText(true);
            row = builder.sheet.createRow(rowNum++);

            builder.cellFormatValue(row, 2, "MEDIO", "string");
            builder.cellFormatValue(row, 3, String.valueOf(pcaCodRepMarca.getpMedioMes()), "string");
            builder.cellFormatValue(row, 4, String.valueOf(pcaCodRepMarca.getpMedioCid()), "string");

            row.getCell(2).setCellStyle(blueStyleCenter);
            row.getCell(3).setCellStyle(stylePadraoCenter);
            row.getCell(4).setCellStyle(stylePadraoCenter);

            row = builder.sheet.createRow(rowNum++);

            builder.cellFormatValue(row, 2, "LONGO", "string");
            builder.cellFormatValue(row, 3, String.valueOf(pcaCodRepMarca.getpLongoMes()), "string");
            builder.cellFormatValue(row, 4, String.valueOf(pcaCodRepMarca.getpLongoCid()), "string");

            row.getCell(2).setCellStyle(blueStyleCenter);
            row.getCell(3).setCellStyle(stylePadraoCenter);
            row.getCell(4).setCellStyle(stylePadraoCenter);
        }
        //</editor-fold>

        for (Cell cell : rowHeader) {
            cell.setCellStyle(builder.workbook.createCellStyle());
            CellStyle cellStyle = cell.getCellStyle();
            cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            cellStyle.setFont(headerFont);

            switch (cell.getColumnIndex()) {
                case 6:
                    cellStyle.setFillForegroundColor(IndexedColors.LIGHT_TURQUOISE.getIndex());
                    break;
                case 7:
                    cellStyle.setFillForegroundColor(IndexedColors.CORAL.getIndex());
                    break;
                case 8:
                    cellStyle.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
                    break;
                case 9:
                    cellStyle.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
                    break;
                case 10:
                    cellStyle.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
                    break;
                case 11:
                    cellStyle.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
                    break;
                case 16:
                    cellStyle.setFillForegroundColor(IndexedColors.BLACK.getIndex());
                    cellStyle.setFont(blackStyleFont);
                    break;
                default:
                    cellStyle.setFillForegroundColor(IndexedColors.WHITE.getIndex());
                    break;
            }

            cellStyle.setAlignment(HorizontalAlignment.CENTER);
            cellStyle.setBorderBottom(BorderStyle.THIN);
            cellStyle.setBorderLeft(BorderStyle.THIN);
            cellStyle.setBorderRight(BorderStyle.THIN);
            cellStyle.setBorderTop(BorderStyle.THIN);
        }

        for (int i = 0; i < 16; i++) {
            builder.sheet.autoSizeColumn(i);
        }
    }

    private void fixarData(ListProperty<VSdPedDataCidadesRep> cidadesBean, List<VSdPedDataCidadesRep> cidades, LocalDate data) {

        Map<Cidade, List<VSdPedDataCidadesRep>> collect = cidades.stream().collect(Collectors.groupingBy(VSdPedDataCidadesRep::getCodCid));
        collect.forEach((cidade, list) -> {
            VSdPedDataCidadesRep cidadeSt = cidadesBean.stream().filter(it -> it.getCodCid().getCodCid().equals(cidade.getCodCid())).findFirst().orElse(null);
            list = list.stream().filter(it -> it.getDtEmissao() != null).filter(it -> it.getDtEmissao().isBefore(data) && it.getDtEmissao().isAfter(colecaoAtual.get(0).getInicioVend())).collect(Collectors.toList());
            cidadeSt.setAtiva(cidadeSt.isAtiva() || list.size() > 0);
            cidadeSt.setValorFixo(BigDecimal.valueOf(list.stream().mapToDouble(it -> it.getValor().doubleValue()).sum()));
            cidadeSt.setPvFixo((int) list.stream().filter(it -> it.getCodcli() != null).map(VSdPedDataCidadesRep::getCodcli).distinct().count());
        });
    }

    private void desfixarData(ListProperty<VSdPedDataCidadesRep> cidades) {
        cidades.forEach(it -> {
            it.setPvFixo(0);
            it.setValorFixo(BigDecimal.ZERO);
        });
    }

    private void carregarCidadesRepMarca(Colecao colecao, VSdSemanaColecao semana) {
        if (codrepFilter.value.getValue() == null && !tgGlobal.value.get()) {
            MessageBox.create(message -> {
                message.message("Selecione um representante primeiro ou marque a \"Vizualização Global\"");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            return;
        }
        limparCampos();
        Represen representante = tgGlobal.value.getValue() ? null : codrepFilter.value.getValue();
        new RunAsyncWithOverlay(this).exec(task -> {
            marcasRep.clear();
            if (tgGlobal.value.getValue()) {
                marcasRep.addAll((List<Marca>) new FluentDao().selectFrom(Marca.class).where(el -> el.isIn("codigo", new String[]{"D", "F"})).resultList());
            } else {
                marcasRep.addAll(representante.getMarcas().stream().map(it -> it.getId().getMarca()).collect(Collectors.toList()));
            }

            marcasRep.sort(Comparator.comparing(Marca::getCodigo));
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                tabPane.getTabs().clear();
                if (marcasRep.size() == 0) return;
                if (marcasRep.size() > 1) marcasRep.add(new Marca("0"));
                marcasRep.forEach(marca -> criaVizualizacaoMarca(marca, representante, colecao, semana));
                tabPane.getSelectionModel().select(0);
            }
        });
    }

    private void carregarPcaData(LocalDate dtSelecionada) {
        Colecao colecao = ((List<Colecao>) new FluentDao().selectFrom(Colecao.class).get().resultList()).stream().filter(it -> it.getInicioVig() != null && it.getFimVig() != null).filter(it -> (dtSelecionada.isAfter(LocalDate.parse(it.getInicioVig().toString())) || dtSelecionada.equals(LocalDate.parse(it.getInicioVig().toString()))) && (dtSelecionada.isBefore(LocalDate.parse(it.getFimVig().toString())) || dtSelecionada.equals(LocalDate.parse(it.getFimVig().toString())))).findFirst().orElse(null);

        colecaoFilter.value.setValue(colecao);
        if (semanaComboBox.items.size() > 0)
            semanaComboBox.select(semanaComboBox.items.stream().filter(it -> (dtSelecionada.equals(it.getDtInicio()) || dtSelecionada.isAfter(it.getDtInicio())) && (dtSelecionada.equals(it.getDtFim()) || dtSelecionada.isBefore(it.getDtFim()))).findFirst().orElse(semanaComboBox.items.get(0)));
    }
}

