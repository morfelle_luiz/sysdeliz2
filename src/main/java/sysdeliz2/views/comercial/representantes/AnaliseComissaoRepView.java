package sysdeliz2.views.comercial.representantes;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import sysdeliz2.controllers.views.comercial.representantes.AnaliseComissaoRepController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.ti.*;
import sysdeliz2.models.view.VSdAnaliseComissaoHeader;
import sysdeliz2.models.view.VSdAnaliseComissaoItem;
import sysdeliz2.models.view.VSdFinanceiroCliente;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.exceptions.FormValidationException;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

public class AnaliseComissaoRepView extends AnaliseComissaoRepController {

    private StackPane stackPaneMain = this;
    // <editor-fold defaultstate="collapsed" desc="Declaração: Lists">
    protected final ListProperty<VSdAnaliseComissaoHeader> pedidosBean = new SimpleListProperty<>();
    protected final ListProperty<VSdAnaliseComissaoItem> linhasPedidoBean = new SimpleListProperty<>();
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: View">
    private final VBox windowBox = super.box;
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Filters Field">
    private final FormFieldText numeroField = FormFieldText.create(numeroField -> {
        numeroField.title("Pedido");
    });
    private final FormFieldMultipleFind<Entidade> entidadeField = FormFieldMultipleFind.create(Entidade.class, entidadeField -> {
        entidadeField.title("Cliente");
        entidadeField.width(200);
    });
    private final FormFieldMultipleFind<Represen> representanteField = FormFieldMultipleFind.create(Represen.class, representanteField -> {
        representanteField.title("Representante");
        representanteField.width(200);
    });
    private final FormFieldDatePeriod emissaoField = FormFieldDatePeriod.create(emissaoField -> {
        emissaoField.title("Data Emissão");
    });
    private final FormFieldMultipleFind<Marca> marcaField = FormFieldMultipleFind.create(Marca.class, marcaField -> {
        marcaField.title("Marca");
    });
    private final FormFieldMultipleFind<Colecao> colecaoField = FormFieldMultipleFind.create(Colecao.class, colecaoField -> {
        colecaoField.title("Coleção");
    });
    private final FormFieldMultipleFind<Regiao> tabPrecoField = FormFieldMultipleFind.create(Regiao.class, tabPrecoField -> {
        tabPrecoField.title("Tabela");
    });
    private final FormFieldMultipleFind<GrupoCli> grupoCliField = FormFieldMultipleFind.create(GrupoCli.class, sitCliField -> {
        sitCliField.title("Grupo");
        sitCliField.toUpper();
    });
    private final FormFieldSegmentedButton<String> verificarField = FormFieldSegmentedButton.create(verificarField -> {
        verificarField.title("Verificar");
        verificarField.options(
                verificarField.option("Ambos", "AMBOS", FormFieldSegmentedButton.Style.PRIMARY),
                verificarField.option("Não", false, FormFieldSegmentedButton.Style.SUCCESS),
                verificarField.option("Sim", true, FormFieldSegmentedButton.Style.DANGER)
        );
        verificarField.select(0);
    });
    private final FormFieldSegmentedButton<String> bloqueioComercialField = FormFieldSegmentedButton.create(bloqueioComercialField -> {
        bloqueioComercialField.title("Liberado Comercial");
        bloqueioComercialField.options(
                bloqueioComercialField.option("Ambos", "AMBOS", FormFieldSegmentedButton.Style.PRIMARY),
                bloqueioComercialField.option("Sim", true, FormFieldSegmentedButton.Style.SUCCESS),
                bloqueioComercialField.option("Não", false, FormFieldSegmentedButton.Style.DANGER)
        );
        bloqueioComercialField.select(0);
    });
    private final FormFieldSegmentedButton<String> bloqueioFinanceiroField = FormFieldSegmentedButton.create(bloqueioFinanceiroField -> {
        bloqueioFinanceiroField.title("Liberado Financeiro");
        bloqueioFinanceiroField.options(
                bloqueioFinanceiroField.option("Ambos", "AMBOS", FormFieldSegmentedButton.Style.PRIMARY),
                bloqueioFinanceiroField.option("Sim", true, FormFieldSegmentedButton.Style.SUCCESS),
                bloqueioFinanceiroField.option("Não", false, FormFieldSegmentedButton.Style.DANGER)
        );
        bloqueioFinanceiroField.select(0);
    });
    private final FormFieldSegmentedButton<String> statusField = FormFieldSegmentedButton.create(statusField -> {
        statusField.title("Status Pedido");
        statusField.options(
                statusField.option("Ambos", "AMBOS", FormFieldSegmentedButton.Style.PRIMARY),
                statusField.option("Pendente", "PENDENTE", FormFieldSegmentedButton.Style.WARNING),
                statusField.option("Faturado", "FATURADO", FormFieldSegmentedButton.Style.SUCCESS),
                statusField.option("Cancelado", "CANCELADO", FormFieldSegmentedButton.Style.DANGER)
        );
        statusField.select(0);
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Tables">
    private final FormTableView<VSdAnaliseComissaoHeader> tblPedidos = FormTableView.create(VSdAnaliseComissaoHeader.class, table -> {
        table.title("Pedidos");
        table.expanded();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Ações");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(80);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAnaliseComissaoHeader, VSdAnaliseComissaoHeader>, ObservableValue<VSdAnaliseComissaoHeader>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<VSdAnaliseComissaoHeader, VSdAnaliseComissaoHeader>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button visualizarLinhas = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIconButton(ImageUtils.ButtonIcon.FIND, ImageUtils.IconSize._16));
                                btn.tooltip("Visualizar Linhas do Pedido");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("info");
                            });
                            final Button marcarOk = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIconButton(ImageUtils.ButtonIcon.ACTIVE, ImageUtils.IconSize._16));
                                btn.tooltip("Marcar Pedido como Analisado");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("success");
                            });

                            @Override
                            protected void updateItem(VSdAnaliseComissaoHeader item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {

                                    visualizarLinhas.setOnAction(evt -> {
                                        table.selectItem(item);
                                        abrirLinhasPedido(item);
                                    });
                                    marcarOk.setOnAction(evt -> {
                                        try {
                                            new NativeDAO().runNativeQueryUpdate(String.format("update sd_pedido_001 set analise_comissao = 'S' where numero = '%s'", item.getNumero()));
                                            SysLogger.addSysDelizLog("Comissão Pedido", TipoAcao.EDITAR, item.getNumero(), "Marcado pedido como verificado comissão");
                                            item.setVerificar(false);
                                            JPAUtils.getEntityManager().refresh(item);
                                            table.refresh();
                                            MessageBox.create(message -> {
                                                message.message("Pedido marcado como verificado.");
                                                message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                message.position(Pos.TOP_RIGHT);
                                                message.notification();
                                            });
                                        } catch (SQLException e) {
                                            e.printStackTrace();
                                            ExceptionBox.build(message -> {
                                                message.exception(e);
                                                message.showAndWait();
                                            });
                                        }
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(visualizarLinhas, marcarOk);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Verificação");
                    cln.width(70);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAnaliseComissaoHeader, VSdAnaliseComissaoHeader>, ObservableValue<VSdAnaliseComissaoHeader>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<VSdAnaliseComissaoHeader, VSdAnaliseComissaoHeader>() {
                            @Override
                            protected void updateItem(VSdAnaliseComissaoHeader item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(!item.isVerificar() ? item.getComcal().doubleValue() > 0
                                            ? ImageUtils.getIcon(ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._16)
                                            : ImageUtils.getIcon(ImageUtils.Icon.INTERMITATE, ImageUtils.IconSize._16)
                                            : ImageUtils.getIcon(ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Representante");
                    cln.width(200);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAnaliseComissaoHeader, VSdAnaliseComissaoHeader>, ObservableValue<VSdAnaliseComissaoHeader>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodrep()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Cliente");
                    cln.width(300);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAnaliseComissaoHeader, VSdAnaliseComissaoHeader>, ObservableValue<VSdAnaliseComissaoHeader>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodcli()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("UF");
                    cln.width(25);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAnaliseComissaoHeader, VSdAnaliseComissaoHeader>, ObservableValue<VSdAnaliseComissaoHeader>>) param -> {
                        String uf = param.getValue().getCodcli().getCep() == null ? "" : param.getValue().getCodcli().getCep().getCidade().getCodEst().getId().getSiglaEst();
                        return new ReadOnlyObjectWrapper(uf);
                    });
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Tipo");
                    cln.width(35);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAnaliseComissaoHeader, VSdAnaliseComissaoHeader>, ObservableValue<VSdAnaliseComissaoHeader>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodcli().getSitCli().getCodigo()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("TF");
                    cln.width(40);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAnaliseComissaoHeader, VSdAnaliseComissaoHeader>, ObservableValue<VSdAnaliseComissaoHeader>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCrescTf()));
                    cln.format(param -> {
                        return new TableCell<VSdAnaliseComissaoHeader, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                clear();
                                if (item != null && !empty) {
                                    setText(StringUtils.toPercentualFormat(item.doubleValue(), 2));
                                    getStyleClass().add(item.compareTo(BigDecimal.ZERO) == 0 ? "table-row-warning" : item.compareTo(BigDecimal.ZERO) < 0 ? "table-row-danger" : "table-row-success");
                                }
                            }

                            private void clear() {
                                getStyleClass().removeAll("table-row-success", "table-row-warning", "table-row-danger");
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Tabela");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAnaliseComissaoHeader, VSdAnaliseComissaoHeader>, ObservableValue<VSdAnaliseComissaoHeader>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTabpre()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Inscrição Est.");
                    cln.width(100);
                    cln.hide();
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAnaliseComissaoHeader, VSdAnaliseComissaoHeader>, ObservableValue<VSdAnaliseComissaoHeader>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodcli().getInscEst() == null ? "ISENTO": param.getValue().getCodcli().getInscEst()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Numero");
                    cln.width(80);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAnaliseComissaoHeader, VSdAnaliseComissaoHeader>, ObservableValue<VSdAnaliseComissaoHeader>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumero()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Emissão");
                    cln.width(70);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAnaliseComissaoHeader, VSdAnaliseComissaoHeader>, ObservableValue<VSdAnaliseComissaoHeader>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDtemissao()));
                    cln.format(param -> {
                        return new TableCell<VSdAnaliseComissaoHeader, LocalDate>() {
                            @Override
                            protected void updateItem(LocalDate item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDateFormat(item));
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Desc (%)");
                    cln.width(50);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAnaliseComissaoHeader, VSdAnaliseComissaoHeader>, ObservableValue<VSdAnaliseComissaoHeader>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPerdesc()));
                    cln.format(param -> {
                        return new TableCell<VSdAnaliseComissaoHeader, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(item + "%");
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Desc (R$)");
                    cln.width(80);
                    cln.hide();
                    cln.alignment(FormTableColumn.Alignment.RIGHT);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAnaliseComissaoHeader, VSdAnaliseComissaoHeader>, ObservableValue<VSdAnaliseComissaoHeader>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValordesc()));
                    cln.format(param -> {
                        return new TableCell<VSdAnaliseComissaoHeader, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toMonetaryFormat(item, 2));
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Qtde");
                    cln.width(50);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAnaliseComissaoHeader, VSdAnaliseComissaoHeader>, ObservableValue<VSdAnaliseComissaoHeader>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde() + param.getValue().getQtdef()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Qtde Pend");
                    cln.width(60);
                    cln.hide();
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAnaliseComissaoHeader, VSdAnaliseComissaoHeader>, ObservableValue<VSdAnaliseComissaoHeader>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Qtde Fat");
                    cln.width(60);
                    cln.hide();
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAnaliseComissaoHeader, VSdAnaliseComissaoHeader>, ObservableValue<VSdAnaliseComissaoHeader>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtdef()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Valor Pend");
                    cln.width(90);
                    cln.hide();
                    cln.alignment(FormTableColumn.Alignment.RIGHT);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAnaliseComissaoHeader, VSdAnaliseComissaoHeader>, ObservableValue<VSdAnaliseComissaoHeader>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValorpend()));
                    cln.format(param -> {
                        return new TableCell<VSdAnaliseComissaoHeader, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toMonetaryFormat(item, 2));
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Valor");
                    cln.width(80);
                    cln.alignment(FormTableColumn.Alignment.RIGHT);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAnaliseComissaoHeader, VSdAnaliseComissaoHeader>, ObservableValue<VSdAnaliseComissaoHeader>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValorpend().add(param.getValue().getValorfat())));
                    cln.format(param -> {
                        return new TableCell<VSdAnaliseComissaoHeader, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toMonetaryFormat(item, 2));
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Valor Fat");
                    cln.width(90);
                    cln.hide();
                    cln.alignment(FormTableColumn.Alignment.RIGHT);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAnaliseComissaoHeader, VSdAnaliseComissaoHeader>, ObservableValue<VSdAnaliseComissaoHeader>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValorfat()));
                    cln.format(param -> {
                        return new TableCell<VSdAnaliseComissaoHeader, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toMonetaryFormat(item, 2));
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Sit. Dup.");
                    cln.width(60);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAnaliseComissaoHeader, VSdAnaliseComissaoHeader>, ObservableValue<VSdAnaliseComissaoHeader>>) param -> new ReadOnlyObjectWrapper(param.getValue().getSitDup()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Sit. Dup.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Pagamento");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAnaliseComissaoHeader, VSdAnaliseComissaoHeader>, ObservableValue<VSdAnaliseComissaoHeader>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCondicao()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Com. Ped");
                    cln.width(60);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAnaliseComissaoHeader, VSdAnaliseComissaoHeader>, ObservableValue<VSdAnaliseComissaoHeader>>) param -> new ReadOnlyObjectWrapper(param.getValue().getComped()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Com. Calc");
                    cln.width(60);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAnaliseComissaoHeader, VSdAnaliseComissaoHeader>, ObservableValue<VSdAnaliseComissaoHeader>>) param -> new ReadOnlyObjectWrapper(param.getValue().getComcal()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Marca");
                    cln.hide();
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAnaliseComissaoHeader, VSdAnaliseComissaoHeader>, ObservableValue<VSdAnaliseComissaoHeader>>) param -> new ReadOnlyObjectWrapper(param.getValue().getMarca()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Coleção");
                    cln.hide();
                    cln.width(120);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAnaliseComissaoHeader, VSdAnaliseComissaoHeader>, ObservableValue<VSdAnaliseComissaoHeader>>) param -> new ReadOnlyObjectWrapper(param.getValue().getColecao()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Status");
                    cln.width(70);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAnaliseComissaoHeader, VSdAnaliseComissaoHeader>, ObservableValue<VSdAnaliseComissaoHeader>>) param -> new ReadOnlyObjectWrapper(param.getValue().getStatus()));
                    cln.format(param -> {
                        return new TableCell<VSdAnaliseComissaoHeader, String>() {
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                clear();
                                if (item != null && !empty) {
                                    setText(item);
                                    getStyleClass().add(item.equals("PENDENTE") ? "warning" : item.equals("CANCELADO") ? "danger" : "success");
                                }
                            }

                            private void clear() {
                                getStyleClass().remove("danger");
                                getStyleClass().remove("warning");
                                getStyleClass().remove("success");
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Comercial");
                    cln.width(60);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAnaliseComissaoHeader, VSdAnaliseComissaoHeader>, ObservableValue<VSdAnaliseComissaoHeader>>) param -> new ReadOnlyObjectWrapper(param.getValue().isComercial()));
                    cln.format(param -> {
                        return new TableCell<VSdAnaliseComissaoHeader, Boolean>() {
                            @Override
                            protected void updateItem(Boolean item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(item ? ImageUtils.getIcon(ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._16) : ImageUtils.getIcon(ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Financeiro");
                    cln.width(60);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAnaliseComissaoHeader, VSdAnaliseComissaoHeader>, ObservableValue<VSdAnaliseComissaoHeader>>) param -> new ReadOnlyObjectWrapper(param.getValue().isFinanceiro()));
                    cln.format(param -> {
                        return new TableCell<VSdAnaliseComissaoHeader, Boolean>() {
                            @Override
                            protected void updateItem(Boolean item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(item ? ImageUtils.getIcon(ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._16) : ImageUtils.getIcon(ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Observação");
                    cln.width(500);
                    cln.hide();
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAnaliseComissaoHeader, VSdAnaliseComissaoHeader>, ObservableValue<VSdAnaliseComissaoHeader>>) param -> new ReadOnlyObjectWrapper(param.getValue().getObse()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Favorito");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAnaliseComissaoHeader, VSdAnaliseComissaoHeader>, ObservableValue<VSdAnaliseComissaoHeader>>) param -> new ReadOnlyObjectWrapper(param.getValue().isFavoritado()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<VSdAnaliseComissaoHeader, Boolean>(){
                            @Override
                            protected void updateItem(Boolean item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(item ? ImageUtils.getIcon(ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._16) : ImageUtils.getIcon(ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                                }
                            }
                        };
                    });
                }).build() /*Favorito*/
        );
        table.contextMenu(FormContextMenu.create(menu -> {
            menu.addItem(item -> {
                item.setText("Alterar Condição de Pagamento");
                item.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.CONDICAO_PAGAMENTO, ImageUtils.IconSize._16));
                item.setOnAction(evt -> {
                    alterarCondicaoPagamento((VSdAnaliseComissaoHeader) table.selectedItem());
                });
            });
            menu.addItem(item -> {
                item.setText("Alterar Desconto");
                item.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.DESCONTO, ImageUtils.IconSize._16));
                item.setOnAction(evt -> {
                    alterarDesconto((VSdAnaliseComissaoHeader) table.selectedItem());
                });
            });
            menu.addItem(item -> {
                item.setText("Liberar Comercial Pedido");
                item.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.COMERCIAL, ImageUtils.IconSize._16));
                item.setOnAction(evt -> {
                    liberarComercialPedido((VSdAnaliseComissaoHeader) table.selectedItem());
                });
            });
            menu.addItem(item -> {
                item.setText("Liberar Financeiro Pedido");
                item.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.FINANCEIRO, ImageUtils.IconSize._16));
                item.setOnAction(evt -> {
                    liberarFinanceiroPedido((VSdAnaliseComissaoHeader) table.selectedItem());
                });
            });
            menu.addItem(item -> {
                item.setText("Aplicar Comissão Calculada");
                item.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.COMISSAO, ImageUtils.IconSize._16));
                item.setOnAction(evt -> {
                    aplicarComissaoCalculada((VSdAnaliseComissaoHeader) table.selectedItem());
                });
            });
        }));
        table.items.bind(pedidosBean);
        table.selectionModelItem((observable, oldValue, newValue) -> {
            super.linhasPedido.clear();
        });
    });
    private final FormTableView<VSdAnaliseComissaoItem> tblLinhasPedido = FormTableView.create(VSdAnaliseComissaoItem.class, tblLinhasPedido -> {
        tblLinhasPedido.title("Linhas do Pedidos");
        tblLinhasPedido.width(450);
        tblLinhasPedido.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Verificação");
                    cln.width(70);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAnaliseComissaoItem, VSdAnaliseComissaoItem>, ObservableValue<VSdAnaliseComissaoItem>>) param -> new ReadOnlyObjectWrapper(param.getValue().getVerificar()));
                    cln.format(param -> {
                        return new TableCell<VSdAnaliseComissaoItem, Boolean>() {
                            @Override
                            protected void updateItem(Boolean item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(!item ? ImageUtils.getIcon(ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._16) : ImageUtils.getIcon(ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Linha");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAnaliseComissaoItem, VSdAnaliseComissaoItem>, ObservableValue<VSdAnaliseComissaoItem>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLinha()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Com. Ped");
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.width(65);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAnaliseComissaoItem, VSdAnaliseComissaoItem>, ObservableValue<VSdAnaliseComissaoItem>>) param -> new ReadOnlyObjectWrapper(param.getValue().getComped()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Com. Calc");
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.width(65);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAnaliseComissaoItem, VSdAnaliseComissaoItem>, ObservableValue<VSdAnaliseComissaoItem>>) param -> new ReadOnlyObjectWrapper(param.getValue().getComcal()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Qtde Pend");
                    cln.width(65);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAnaliseComissaoItem, VSdAnaliseComissaoItem>, ObservableValue<VSdAnaliseComissaoItem>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Qtde Fat");
                    cln.width(65);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdAnaliseComissaoItem, VSdAnaliseComissaoItem>, ObservableValue<VSdAnaliseComissaoItem>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtdef()));
                }).build()
        );
        tblLinhasPedido.contextMenu(FormContextMenu.create(menu -> {
            menu.addItem(item -> {
                item.setText("Alterar Comissão da Linha");
                item.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.DESCONTO, ImageUtils.IconSize._16));
                item.setOnAction(evt -> {
                    alterarComissaoLinha((VSdAnaliseComissaoItem) tblLinhasPedido.selectedItem());
                });
            });
            menu.addItem(item -> {
                item.setText("Aplicar Comissão Calculada");
                item.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.COMISSAO, ImageUtils.IconSize._16));
                item.setOnAction(evt -> {
                    aplicarComissaoCalculadaLinha((VSdAnaliseComissaoItem) tblLinhasPedido.selectedItem());
                });
            });
            menu.addItem(item -> {
                item.setText("Alterar Comissão para todas Linhas");
                item.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.DESCONTO, ImageUtils.IconSize._16));
                item.setOnAction(evt -> {
                    alterarComissaoTodasLinha((List<VSdAnaliseComissaoItem>) tblLinhasPedido.items.get());
                });
            });
        }));
        tblLinhasPedido.items.bind(linhasPedidoBean);
    });
    // </editor-fold>

    public AnaliseComissaoRepView() {
        super("Análise de Comissão Representante", ImageUtils.getImage(ImageUtils.Icon.COMISSAO), null);
        init();
    }

    private void init() {
        windowBox.getChildren().add(FormBox.create(boxHeader -> {
            boxHeader.horizontal();
            boxHeader.add(FormTitledPane.create(filter -> {
                filter.filter();
                filter.add(FormBox.create(boxFilter -> {
                    boxFilter.horizontal();
                    boxFilter.add(FormBox.create(coluna1 -> {
                        coluna1.vertical();
                        coluna1.add(statusField.build(), verificarField.build(), bloqueioComercialField.build(), bloqueioFinanceiroField.build());
                    }));
                    boxFilter.add(FormBox.create(coluna2 -> {
                        coluna2.vertical();
                        coluna2.add(emissaoField.build(), numeroField.build(), entidadeField.build(), representanteField.build());
                    }));
                    boxFilter.add(FormBox.create(coluna3 -> {
                        coluna3.vertical();
                        coluna3.add(marcaField.build(), colecaoField.build(), tabPrecoField.build(), grupoCliField.build());
                    }));
                }));
                filter.find.setOnAction(evt -> {
                    JPAUtils.clearEntitys(pedidos);
                    new RunAsyncWithOverlay(this).addTaskEndNotification(taskReturn -> {
                        if (taskReturn.equals(ReturnAsync.OK.value)) {
                            pedidosBean.set(FXCollections.observableList(pedidos));
                        }
                    }).exec(task -> {
                        filters.set(statusField.value.getValue(), verificarField.value.getValue(), bloqueioComercialField.value.getValue(), bloqueioFinanceiroField.value.getValue(),
                                emissaoField.valueBegin.getValue(), emissaoField.valueEnd.getValue(), numeroField.value.getValue(), entidadeField.textValue.getValue(), representanteField.textValue.getValue(),
                                marcaField.textValue.getValue(), colecaoField.textValue.getValue(), tabPrecoField.textValue.getValue(), grupoCliField.textValue.getValue());
                        findPedidos(filters);
                        return ReturnAsync.OK.value;
                    });
                });
                filter.clean.setOnAction(evt -> {
                    numeroField.clear();
                    grupoCliField.clear();
                    entidadeField.clear();
                    representanteField.clear();
                    emissaoField.clear();
                    marcaField.clear();
                    colecaoField.clear();
                    tabPrecoField.clear();
                    verificarField.select(0);
                    bloqueioComercialField.select(0);
                    bloqueioFinanceiroField.select(0);
                    statusField.select(0);
                    filters.clear();
                });
            }));
        }));
        windowBox.getChildren().add(FormBox.create(boxTables -> {
            boxTables.horizontal();
            boxTables.expanded();
            boxTables.add(tblPedidos.build());
            boxTables.add(tblLinhasPedido.build());
        }));
    }

    private void abrirLinhasPedido(@NotNull VSdAnaliseComissaoHeader item) {
        new RunAsyncWithOverlay(this).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                linhasPedidoBean.set(FXCollections.observableList(linhasPedido));
            }
        }).exec(task -> {
            findLinhasPedido(item.getNumero());
            return ReturnAsync.OK.value;
        });
    }

    private void alterarCondicaoPagamento(@NotNull VSdAnaliseComissaoHeader selectedItem) {
        if (selectedItem == null) {
            return;
        }
        if ((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Deseja realmente alterar a condição de pagamento do pedido?");
            message.showAndWait();
        }).value.get()) {
            new Fragment().show(fragment -> {
                fragment.title.setText("Alterar Condição de Pagamento");
                FormFieldSingleFind<Condicao> condicaoField = FormFieldSingleFind.create(Condicao.class, field -> {
                    field.title("Condição de Pagamento");
                });
                fragment.box.getChildren().add(condicaoField.build());
                fragment.size(400.0, 250.0);

                Button salvarAlteracao = new Button("Alterar", ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                salvarAlteracao.getStyleClass().addAll("sm", "success");
                salvarAlteracao.setOnAction(evt -> {
                    if (condicaoField.value.getValue() == null) {
                        MessageBox.create(message -> {
                            message.message("Você precisa primeiro selecionar uma condição de pagamento.");
                            message.type(MessageBox.TypeMessageBox.IMPORTANT);
                            message.showAndWait();
                        });
                        return;
                    }
                    String condicao = super.alterarCondicaoPagamentoPedido(selectedItem.getNumero(), condicaoField.value.getValue());
                    SysLogger.addSysDelizLog("Análise Comissão", TipoAcao.EDITAR, selectedItem.getNumero(), "Alterado condição do pedido de " + selectedItem.getCondicao() + " para " + condicao);
                    selectedItem.setCondicao(condicao);
                    fragment.modalStage.close();
                    MessageBox.create(message -> {
                        message.message("Condição de pagamento para o pedido " + selectedItem.getNumero() + " alterado com sucesso.");
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.position(Pos.TOP_RIGHT);
                        message.notification();
                    });
                    tblPedidos.refresh();
                });
                fragment.buttonsBox.getChildren().addAll(salvarAlteracao);
            });
        }
    }

    private void alterarDesconto(@NotNull VSdAnaliseComissaoHeader selectedItem) {
        if (selectedItem == null) {
            return;
        }
        if ((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Deseja realmente alterar o desconto do pedido?");
            message.showAndWait();
        }).value.get()) {
            new Fragment().show(fragment -> {
                fragment.title.setText("Alterar Desconto do Pedido");
                FormFieldText descontoField = FormFieldText.create(field -> {
                    field.title("Desconto (%)");
                    field.mask(FormFieldText.Mask.DOUBLE);
                });
                fragment.box.getChildren().add(descontoField.build());
                fragment.size(400.0, 250.0);

                Button salvarAlteracao = new Button("Alterar", ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                salvarAlteracao.getStyleClass().addAll("sm", "success");
                salvarAlteracao.setOnAction(evt -> {
                    if (descontoField.value.getValue() == null) {
                        MessageBox.create(message -> {
                            message.message("Você precisa primeiro digitar o valor (%) para o desconto.");
                            message.type(MessageBox.TypeMessageBox.IMPORTANT);
                            message.showAndWait();
                        });
                        return;
                    }
                    super.alterarDescontoPedido(selectedItem.getNumero(), Double.valueOf(descontoField.value.getValue().replace(".", "").replace(",", ".")));
                    SysLogger.addSysDelizLog("Análise Comissão", TipoAcao.EDITAR, selectedItem.getNumero(), "Alterado desconto do pedido de " + selectedItem.getPerdesc() + " para " + descontoField.value.getValue());
                    selectedItem.setPerdesc(new BigDecimal(descontoField.value.getValue().replace(".", "").replace(",", ".")));
                    fragment.modalStage.close();
                    MessageBox.create(message -> {
                        message.message("Desconto para o pedido " + selectedItem.getNumero() + " alterado com sucesso.");
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.position(Pos.TOP_RIGHT);
                        message.notification();
                    });
                    tblPedidos.refresh();
                });
                fragment.buttonsBox.getChildren().addAll(salvarAlteracao);
            });
        }
    }

    private void liberarFinanceiroPedido(@NotNull VSdAnaliseComissaoHeader selectedItem) {
        if (selectedItem == null) {
            return;
        }
        if ((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Deseja realmente liberar o financeiro do pedido?");
            message.showAndWait();
        }).value.get()) {
            new Fragment().show(fragment -> {
                fragment.title.setText("Liberar Financeiro Pedido");
                Entidade codcli = new FluentDao().selectFrom(Entidade.class).where(it -> it.equal("codcli", selectedItem.getCodcli().getCodcli())).singleResult();
                VSdFinanceiroCliente financeiroCliente = new FluentDao().selectFrom(VSdFinanceiroCliente.class).where(it -> it.equal("codcli", StringUtils.lpad(selectedItem.getCodcli().getCodcli().toString(), 5, "0"))).singleResult();
                BigDecimal saldoCredito = codcli.getCredito().subtract(financeiroCliente.getValorreceber().add(financeiroCliente.getValorpedpend()));
                FormFieldText creditoField = FormFieldText.create(field -> {
                    field.title("Crédito");
                    field.mask(FormFieldText.Mask.DOUBLE);
                    field.label("R$");
                    field.value.set(codcli.getCredito().toString());
                });
                FormFieldText valorAReceberField = FormFieldText.create(field -> {
                    field.title("Em aberto");
                    field.editable.set(false);
                    field.label("R$");
                    field.value.set(StringUtils.toDecimalFormat(financeiroCliente.getValorreceber().doubleValue(), 2));
                });
                FormFieldText valorAtrasadoField = FormFieldText.create(field -> {
                    field.title("Atrasado");
                    field.label("R$");
                    field.editable.set(false);
                    field.value.set(StringUtils.toDecimalFormat(financeiroCliente.getValoratrasado().doubleValue(), 2));
                });
                FormFieldText valorPedidoField = FormFieldText.create(field -> {
                    field.title("Pendente Pedidos");
                    field.label("R$");
                    field.editable.set(false);
                    field.value.set(StringUtils.toDecimalFormat(financeiroCliente.getValorpedpend().doubleValue(), 2));
                });
                FormFieldText saldoCreditoField = FormFieldText.create(field -> {
                    field.title("Saldo");
                    field.label("R$");
                    field.editable.set(false);
                    field.value.set(StringUtils.toDecimalFormat(saldoCredito.doubleValue(), 2));
                    field.addStyle(saldoCredito.doubleValue() >= 0 ? "success" : "danger");
                });
                fragment.box.getChildren().add(FormBox.create(boxCredito -> {
                    boxCredito.vertical();
                    boxCredito.expanded();
                    boxCredito.add(creditoField.build());
                    boxCredito.add(FormBox.create(boxFinanceiroCliente -> {
                        boxFinanceiroCliente.horizontal();
                        boxFinanceiroCliente.expanded();
                        boxFinanceiroCliente.add(FormBox.create(box -> {
                            box.vertical();
                            box.expanded();
                            box.add(valorAReceberField.build(), valorAtrasadoField.build());
                        }));
                        boxFinanceiroCliente.add(FormBox.create(box -> {
                            box.vertical();
                            box.expanded();
                            box.add(valorPedidoField.build(), saldoCreditoField.build());
                        }));
                    }));
                }));
                fragment.size(400.0, 250.0);

                Button salvarAlteracao = new Button("Liberar Pedido", ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                salvarAlteracao.getStyleClass().addAll("sm", "success");
                salvarAlteracao.setOnAction(evt -> {
                    if (creditoField.value.getValue() == null) {
                        MessageBox.create(message -> {
                            message.message("Você precisa primeiro digitar o valor (R$) para o limite de crédito do cliente.");
                            message.type(MessageBox.TypeMessageBox.IMPORTANT);
                            message.showAndWait();
                        });
                        return;
                    }
                    super.liberarFinanceiroPedido(selectedItem.getNumero());
                    codcli.setCredito(BigDecimal.valueOf(Double.valueOf(creditoField.value.getValue().replace(".", "").replace(",", "."))));
                    new FluentDao().merge(codcli);
                    SysLogger.addSysDelizLog("Análise Comissão", TipoAcao.EDITAR, selectedItem.getNumero(), "Liberado financeiro do pedido " + selectedItem.getNumero());
                    fragment.modalStage.close();
                    MessageBox.create(message -> {
                        message.message("Liberado financeiro do pedido " + selectedItem.getNumero());
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.position(Pos.TOP_RIGHT);
                        message.notification();
                    });
                });
                fragment.buttonsBox.getChildren().addAll(salvarAlteracao);
            });
        }
    }

    private void liberarComercialPedido(@NotNull VSdAnaliseComissaoHeader selectedItem) {
        if (selectedItem == null) {
            return;
        }
        if ((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Deseja realmente liberar o comercial do pedido?");
            message.showAndWait();
        }).value.get()) {
            super.liberarComercialPedido(selectedItem.getNumero());
            SysLogger.addSysDelizLog("Análise Comissão", TipoAcao.EDITAR, selectedItem.getNumero(), "Liberado comercial do pedido " + selectedItem.getNumero());
            JPAUtils.getEntityManager().refresh(selectedItem);
            MessageBox.create(message -> {
                message.message("Pedido " + selectedItem.getNumero() + " liberado comercial.");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            tblPedidos.refresh();
        }
    }

    private void alterarComissaoLinha(@NotNull VSdAnaliseComissaoItem selectedItem) {
        if (selectedItem == null) {
            return;
        }
        if ((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Deseja realmente alterar a comissão para esta linha no pedido?");
            message.showAndWait();
        }).value.get()) {
            new Fragment().show(fragment -> {
                fragment.title.setText("Alterar Comissão Linha");
                final FormFieldText comissaoField = FormFieldText.create(field -> {
                    field.title("Comissão (%)");
                    field.mask(FormFieldText.Mask.DOUBLE);
                });
                final FormFieldDate dateField = FormFieldDate.create(field -> {
                    field.title("Mês Referência");
                    field.defaultValue(LocalDate.now());
                });
                fragment.box.getChildren().add(comissaoField.build());
                fragment.box.getChildren().add(dateField.build());
                fragment.size(400.0, 250.0);

                Button salvarAlteracao = new Button("Alterar", ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                salvarAlteracao.getStyleClass().addAll("sm", "success");
                salvarAlteracao.setOnAction(evt -> {
                    try {
                        BigDecimal novaComissaoDigitada = new BigDecimal(comissaoField.validate().value.getValue().replace(".", "").replace(",", "."));
                        LocalDate dataReferencia = LocalDate.of(dateField.validate().value.get().getYear(), dateField.validate().value.get().getMonthValue(), 1);
                        BigDecimal comissaoAnterior = selectedItem.getComped();

                        super.alterarComissaoLinhaPedido(selectedItem.getId().getNumero(), selectedItem.getId().getCodlin(), dataReferencia, novaComissaoDigitada);

                        SysLogger.addSysDelizLog("Análise Comissão", TipoAcao.EDITAR, selectedItem.getId().getNumero(), "Alterado comissão da linha " + selectedItem.getId().getCodlin() + " de " + comissaoAnterior + " para " + novaComissaoDigitada);
                        selectedItem.setComped(new BigDecimal(comissaoField.value.getValue().replace(".", "").replace(",", ".")));
                        selectedItem.setVerificar(false);
                        Double doubleComPed = linhasPedidoBean.stream().mapToDouble(t -> t.getComped().doubleValue()).average().getAsDouble();
                        BigDecimal comPedHeader = new BigDecimal(doubleComPed.toString());
                        new NativeDAO().runNativeQueryUpdate(String.format("update pedido_001 set com1 = %s where numero = '%s'", comPedHeader, selectedItem.getId().getNumero()));
                        tblPedidos.selectedItem().setComped(comPedHeader);
                        tblPedidos.selectedItem().setVerificar(false);
                        fragment.modalStage.close();
                        MessageBox.create(message -> {
                            message.message("Comissão para a linha " + selectedItem.getLinha() + " do pedido " + selectedItem.getId().getNumero() + " alterado com sucesso.");
                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                            message.position(Pos.TOP_RIGHT);
                            message.notification();
                        });
                        tblPedidos.refresh();
                        tblLinhasPedido.refresh();

                    } catch (FormValidationException ex) {
                        ex.printStackTrace();
                        MessageBox.create(message -> {
                            message.message("Existem campos obrigatórios não preenchidos:\n" + ex.getMessage());
                            message.type(MessageBox.TypeMessageBox.WARNING);
                            message.position(Pos.TOP_RIGHT);
                            message.notification();
                        });
                        return;
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                        ExceptionBox.build(message -> {
                            message.exception(throwables);
                            message.showAndWait();
                        });
                    }
                });
                fragment.buttonsBox.getChildren().addAll(salvarAlteracao);
            });
        }
    }

    private void aplicarComissaoCalculada(@NotNull VSdAnaliseComissaoHeader selectedItem) {
        if (selectedItem == null) {
            return;
        }
        if ((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Deseja realmente aplicar a comissão da regra para o pedido?");
            message.showAndWait();
        }).value.get()) {
            new Fragment().show(fragment -> {
                fragment.title.setText("Alterar Comissão Calculada");
                final FormFieldDate dateField = FormFieldDate.create(field -> {
                    field.title("Mês Referência");
                    field.defaultValue(LocalDate.now());
                });
                fragment.box.getChildren().add(dateField.build());
                fragment.size(400.0, 250.0);

                Button salvarAlteracao = new Button("Alterar", ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                salvarAlteracao.getStyleClass().addAll("sm", "success");
                salvarAlteracao.setOnAction(evt -> {
                    try {
                        LocalDate dataReferencia = LocalDate.of(dateField.validate().value.get().getYear(), dateField.validate().value.get().getMonthValue(), 1);

                        super.aplicarComissaoCalculaPedido(selectedItem.getNumero(), dataReferencia);
                        selectedItem.setVerificar(false);
                        selectedItem.setComped(selectedItem.getComcal());
                        new NativeDAO().runNativeQueryUpdate(String.format("update pedido_001 set com1 = %s where numero = '%s'", selectedItem.getComcal(), selectedItem.getNumero()));
                        SysLogger.addSysDelizLog("Análise Comissão", TipoAcao.EDITAR, selectedItem.getNumero(), "Aplicado comissão calculada das linhas do pedido:" + selectedItem.getNumero());
                        MessageBox.create(message -> {
                            message.message("Comissão definida no pedido conforme regra.");
                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                            message.position(Pos.TOP_RIGHT);
                            message.notification();
                        });
                        tblPedidos.refresh();
                        tblLinhasPedido.refresh();
                        fragment.modalStage.close();

                    } catch (FormValidationException ex) {
                        ex.printStackTrace();
                        MessageBox.create(message -> {
                            message.message("Existem campos obrigatórios não preenchidos:\n" + ex.getMessage());
                            message.type(MessageBox.TypeMessageBox.WARNING);
                            message.position(Pos.TOP_RIGHT);
                            message.notification();
                        });
                        return;
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                        ExceptionBox.build(message -> {
                            message.exception(throwables);
                            message.showAndWait();
                        });
                    }
                });
                fragment.buttonsBox.getChildren().addAll(salvarAlteracao);
            });
        }
    }

    private void aplicarComissaoCalculadaLinha(@NotNull VSdAnaliseComissaoItem selectedItem) {
        if (selectedItem == null) {
            return;
        }
        if ((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Deseja realmente aplicar a comissão da regra para a linha no pedido?");
            message.showAndWait();
        }).value.get()) {
            new Fragment().show(fragment -> {
                fragment.title.setText("Alterar Comissão Calculada");
                final FormFieldDate dateField = FormFieldDate.create(field -> {
                    field.title("Mês Referência");
                    field.defaultValue(LocalDate.now());
                });
                fragment.box.getChildren().add(dateField.build());
                fragment.size(400.0, 250.0);

                Button salvarAlteracao = new Button("Alterar", ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                salvarAlteracao.getStyleClass().addAll("sm", "success");
                salvarAlteracao.setOnAction(evt -> {
                    try {
                        LocalDate dataReferencia = LocalDate.of(dateField.validate().value.get().getYear(), dateField.validate().value.get().getMonthValue(), 1);

                        super.alterarComissaoLinhaPedido(selectedItem.getId().getNumero(), selectedItem.getId().getCodlin(), dataReferencia, selectedItem.getComcal());
                        selectedItem.setVerificar(false);
                        selectedItem.setComped(selectedItem.getComcal());
                        Double doubleComPed = linhasPedidoBean.stream().mapToDouble(t -> t.getComped().doubleValue()).average().getAsDouble();
                        BigDecimal comPedHeader = new BigDecimal(doubleComPed.toString());
                        new NativeDAO().runNativeQueryUpdate(String.format("update pedido_001 set com1 = %s where numero = '%s'", comPedHeader, selectedItem.getId().getNumero()));
                        SysLogger.addSysDelizLog("Análise Comissão", TipoAcao.EDITAR, selectedItem.getId().getNumero(), "Aplicado comissão calculada da linha: " + selectedItem.getId().getCodlin());
                        tblPedidos.selectedItem().setComped(comPedHeader);
                        tblPedidos.selectedItem().setVerificar(false);
                        MessageBox.create(message -> {
                            message.message("Comissão definida na linha conforme regra.");
                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                            message.position(Pos.TOP_RIGHT);
                            message.notification();
                        });
                        tblPedidos.refresh();
                        tblLinhasPedido.refresh();
                        fragment.modalStage.close();

                    } catch (FormValidationException ex) {
                        ex.printStackTrace();
                        MessageBox.create(message -> {
                            message.message("Existem campos obrigatórios não preenchidos:\n" + ex.getMessage());
                            message.type(MessageBox.TypeMessageBox.WARNING);
                            message.position(Pos.TOP_RIGHT);
                            message.notification();
                        });
                        return;
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                        ExceptionBox.build(message -> {
                            message.exception(throwables);
                            message.showAndWait();
                        });
                    }
                });
                fragment.buttonsBox.getChildren().addAll(salvarAlteracao);
            });

        }
    }

    private void alterarComissaoTodasLinha(@NotNull List<VSdAnaliseComissaoItem> selectedItem) {
        if (selectedItem == null) {
            return;
        }
        if ((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Deseja realmente alterar a comissão para as linhas no pedido?");
            message.showAndWait();
        }).value.get()) {
            new Fragment().show(fragment -> {
                fragment.title.setText("Alterar Comissão Linhas");
                FormFieldText comissaoField = FormFieldText.create(field -> {
                    field.title("Comissão (%)");
                    field.mask(FormFieldText.Mask.DOUBLE);
                });
                final FormFieldDate dateField = FormFieldDate.create(field -> {
                    field.title("Mês Referência");
                    field.defaultValue(LocalDate.now());
                });
                fragment.box.getChildren().add(comissaoField.build());
                fragment.size(400.0, 250.0);

                Button salvarAlteracao = new Button("Alterar", ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                salvarAlteracao.getStyleClass().addAll("sm", "success");
                salvarAlteracao.setOnAction(evt -> {
                    try {
                        String numeroPedido = selectedItem.stream().findAny().get().getId().getNumero();
                        BigDecimal comissaoNova = new BigDecimal(comissaoField.validate().value.getValue().replace(".", "").replace(",", "."));
                        LocalDate dataReferencia = LocalDate.of(dateField.validate().value.get().getYear(), dateField.validate().value.get().getMonthValue(), 1);

                        super.aplicarComissaoLinhasPedido(numeroPedido, dataReferencia, comissaoNova);
                        linhasPedidoBean.forEach(t -> {
                            t.setVerificar(false);
                            t.setComped(new BigDecimal(comissaoField.value.getValue().replace(".", "").replace(",", ".")));
                        });
                        Double doubleComPed = linhasPedidoBean.stream().mapToDouble(t -> t.getComped().doubleValue()).average().getAsDouble();
                        BigDecimal comPedHeader = new BigDecimal(doubleComPed.toString());
                        new NativeDAO().runNativeQueryUpdate(String.format("update pedido_001 set com1 = %s where numero = '%s'", comPedHeader, numeroPedido));
                        SysLogger.addSysDelizLog("Análise Comissão", TipoAcao.EDITAR, numeroPedido, "Alterado comissão das linhas do pedido: " + numeroPedido);
                        tblPedidos.selectedItem().setComped(comPedHeader);
                        tblPedidos.selectedItem().setVerificar(false);
                        fragment.modalStage.close();
                        MessageBox.create(message -> {
                            message.message("Comissão definida na linha conforme regra.");
                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                            message.position(Pos.TOP_RIGHT);
                            message.notification();
                        });
                        tblPedidos.refresh();
                        tblLinhasPedido.refresh();

                    } catch (FormValidationException ex) {
                        ex.printStackTrace();
                        MessageBox.create(message -> {
                            message.message("Existem campos obrigatórios não preenchidos:\n" + ex.getMessage());
                            message.type(MessageBox.TypeMessageBox.WARNING);
                            message.position(Pos.TOP_RIGHT);
                            message.notification();
                        });
                        return;
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                        ExceptionBox.build(message -> {
                            message.exception(throwables);
                            message.showAndWait();
                        });
                    }
                });
                fragment.buttonsBox.getChildren().addAll(salvarAlteracao);
            });
        }
    }
}