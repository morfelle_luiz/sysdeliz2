package sysdeliz2.views.comercial.representantes;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.*;
import sysdeliz2.controllers.views.comercial.representantes.ControleCidadesRepController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.sysdeliz.SdCidadeRep;
import sysdeliz2.models.sysdeliz.SdCidadeRepPK;
import sysdeliz2.models.ti.Cidade;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.ti.Represen;
import sysdeliz2.models.ti.TabUf;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.SysLogger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class ControleCidadesRepView extends ControleCidadesRepController {

    // <editor-fold defaultstate="collapsed" desc="Bean">
    private ListProperty<SdCidadeRep> cidadesBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Table">
    private FormTableView<SdCidadeRep> tblCidades = FormTableView.create(SdCidadeRep.class, table -> {
        table.title("Cidades");
        table.multipleSelection();
        table.selectColumn();
        table.editable.set(true);
        table.items.set(cidadesBean);
        table.expanded();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(40);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCidadeRep, SdCidadeRep>, ObservableValue<SdCidadeRep>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdCidadeRep, SdCidadeRep>() {
                            final HBox boxButtonsRow = new HBox(1);
                            final Button btnRemove = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                btn.tooltip("config.convertr.context");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("danger");
                            });
                            final Button btnAdd = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._16));
                                btn.tooltip("config.convertr.context");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("success");
                            });

                            @Override
                            protected void updateItem(SdCidadeRep item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    if (item.getStatus() == null) {
                                        btnRemove.setOnAction(evt -> {
                                            item.setStatus("REMOVE");
                                            table.refresh();
                                        });
                                        boxButtonsRow.getChildren().clear();
                                        boxButtonsRow.getChildren().addAll(btnRemove);
                                        setGraphic(boxButtonsRow);
                                    } else if (item.getStatus().equals("NOVO")) {
                                        btnRemove.setOnAction(evt -> {
                                            item.setStatus("REMOVET");
                                            table.refresh();
                                        });
                                        boxButtonsRow.getChildren().clear();
                                        boxButtonsRow.getChildren().addAll(btnRemove);
                                        setGraphic(boxButtonsRow);
                                    } else if (item.getStatus().equals("REMOVET")) {
                                        btnAdd.setOnAction(evt -> {
                                            item.setStatus("NOVO");
                                            table.refresh();
                                        });
                                        boxButtonsRow.getChildren().clear();
                                        boxButtonsRow.getChildren().addAll(btnAdd);
                                        setGraphic(boxButtonsRow);
                                    } else {
                                        btnAdd.setOnAction(evt -> {
                                            item.setStatus(null);
                                            table.refresh();
                                        });
                                        boxButtonsRow.getChildren().clear();
                                        boxButtonsRow.getChildren().addAll(btnAdd);
                                        setGraphic(boxButtonsRow);
                                    }
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Cód. Cidade");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCidadeRep, SdCidadeRep>, ObservableValue<SdCidadeRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodCid().getCodCid()));

                }).build(), /*Cód. Cidade*/
                FormTableColumn.create(cln -> {
                    cln.title("Cidade");
                    cln.width(250);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCidadeRep, SdCidadeRep>, ObservableValue<SdCidadeRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodCid().getNomeCid()));

                }).build(), /*Cidade*/
                FormTableColumn.create(cln -> {
                    cln.title("Estado");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCidadeRep, SdCidadeRep>, ObservableValue<SdCidadeRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodCid().getCodEst().getDescricao()));

                }).build(), /*Estado*/
                FormTableColumn.create(cln -> {
                    cln.title("Cód. Rep.");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCidadeRep, SdCidadeRep>, ObservableValue<SdCidadeRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodRep().getCodRep()));

                }).build(), /*Cód. Rep.*/
                FormTableColumn.create(cln -> {
                    cln.title("Nome Representante");
                    cln.width(250);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCidadeRep, SdCidadeRep>, ObservableValue<SdCidadeRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodRep().getNome()));

                }).build(), /*Nome Representante*/
                FormTableColumn.create(cln -> {
                    cln.title("Marca");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCidadeRep, SdCidadeRep>, ObservableValue<SdCidadeRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getMarca().getDescricao()));

                }).build() /*Marca*/
        );
        table.factoryRow(param -> {
            return new TableRow<SdCidadeRep>() {
                @Override
                protected void updateItem(SdCidadeRep item, boolean empty) {
                    super.updateItem(item, empty);
                    clear();

                    if (item != null && !empty) {
                        if (item.getStatus() != null) {
                            if (item.getStatus().equals("REMOVE")) getStyleClass().add("table-row-danger");
                            if (item.getStatus().equals("REMOVET")) getStyleClass().add("table-row-danger");
                            if (item.getStatus().equals("NOVO")) getStyleClass().add("table-row-success");
                        }
                    }
                }

                private void clear() {
                    getStyleClass().removeAll("table-row-danger", "table-row-success", "table-row-warning");
                }
            };
        });
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Filter">
    private final FormFieldSingleFind<Represen> codRepFilter = FormFieldSingleFind.create(Represen.class, field -> {
        field.title("Cód. Representante");
        field.width(300);
        field.maxLength(5);
        field.value.setValue(null);
    });
    private final FormFieldSingleFind<Marca> marcaFilter = FormFieldSingleFind.create(Marca.class, field -> {
        field.title("Marca");
        field.width(200);
        field.value.setValue(null);
        field.maxLength(3);
    });
    private final FormFieldMultipleFind<Cidade> codCidadeFilter = FormFieldMultipleFind.create(Cidade.class, field -> {
        field.title("Cidade");
        field.width(300);
        field.codeReference.set("nomeCid");
        field.toUpper();
    });
    private final FormFieldSingleFind<TabUf> codEstadoFilter = FormFieldSingleFind.create(TabUf.class, field -> {
        field.title("Estado");
        field.codeReference.set("sigla");
        field.width(200);
        field.toUpper();
    });

    private final FormFieldMultipleFind<Cidade> addCidadeFilter = FormFieldMultipleFind.create(Cidade.class, field -> {
        field.title("Cidade");
        field.width(300);
        field.codeReference.set("nomeCid");
        field.toUpper();
    });
    private final FormFieldSingleFind<TabUf> addEstadoFilter = FormFieldSingleFind.create(TabUf.class, field -> {
        field.title("Estado");
        field.codeReference.set("sigla");
        field.width(300);
        field.value.setValue(null);
        field.toUpper();
    });

    private final FormFieldText nomeCidadeFilter = FormFieldText.create(field -> {
        field.toUpper();
        field.width(400);
        field.title.setText("Filtrar Cidade pelo Nome");
        field.value.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                filtrarCidades(newValue, oldValue);
            }
        });
    });
    private final FormFieldSingleFind<Represen> codRepTransfFilter = FormFieldSingleFind.create(Represen.class, field -> {
        field.title("Cód. Representante");
        field.width(300);
        field.maxLength(5);
        field.value.setValue(null);
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Dados">
    private SdCidadeRep oldCidade = new SdCidadeRep();
    private List<SdCidadeRep> cidadesImportadas;
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Button">

    private final Button btnAddCidade = FormButton.create(btn -> {
        btn.title("Adicionar Cidade");
        btn.width(140);
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._16));
        btn.addStyle("success").addStyle("sm");
        btn.setOnAction(evt -> {
            if (addCidadeFilter.objectValues.isNotNull().get()) {
                adicionarCidades(addCidadeFilter.objectValues.getValue());
                addCidadeFilter.clear();
            }
        });
    });

    private final Button btnAddEstado = FormButton.create(btn -> {
        btn.title("Adicionar Estado");
        btn.width(140);
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._16));
        btn.addStyle("success").addStyle("sm");
        btn.setOnAction(evt -> {
            adicionarEstado();
            addEstadoFilter.clear();
        });
    });

    private final Button btnTransfRep = FormButton.create(btn -> {
        btn.title("Transferir");
        btn.addStyle("secundary").addStyle("sm");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.REPRESENTANTE, ImageUtils.IconSize._16));
        btn.disableProperty().bind(cidadesBean.emptyProperty());
        btn.disableProperty().bind(codRepTransfFilter.value.isNull().or(codRepTransfFilter.value.isEqualTo("")));
        btn.setOnAction(evt -> {
            if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                message.message("Transferir as cidades selecionadas para o representante: " + codRepTransfFilter.value.getValue().getNome() + " ?");
                message.showAndWait();
            }).value.get())) {
                List<SdCidadeRep> cidadesTransf = tblCidades.items.stream().filter(BasicModel::isSelected).collect(Collectors.toList());
                Represen represenTransf = codRepTransfFilter.value.getValue();
                if (cidadesTransf.isEmpty()) MessageBox.create(message -> {
                    message.message("Nenhuma Cidade Selecionada!");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.show();
                    message.position(Pos.CENTER);
                });
                else if (represenTransf.equals(cidadesTransf.get(0).getId().getCodRep())) MessageBox.create(message -> {
                    message.message("Mesmo Representante Selecionado!");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.show();
                    message.position(Pos.CENTER);
                });
                else {
                    transferirCidades(cidadesTransf, represenTransf);
                }

            }
        });
    });

    private final Button btnReverter = FormButton.create(btn -> {
        btn.title("Cancelar");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
        btn.addStyle("warning").addStyle("sm");
        btn.setOnAction(evt -> {
            if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                message.message("Cancelar alterações?");
                message.showAndWait();
            }).value.get())) {
                cidades.clear();
                cidades.addAll(cidadesBkp);
                cidades.forEach(it -> it.setStatus(null));
                cidadesBean.set(FXCollections.observableList(cidades));
                tblCidades.refresh();
            }
        });
    });

    private final Button btnConfirmar = FormButton.create(btn -> {
        btn.title("Confirmar");
        btn.addStyle("success").addStyle("sm");
        btn.icon(ImageUtils.getIconButton(ImageUtils.ButtonIcon.SEND, ImageUtils.IconSize._16));
        btn.setOnAction(evt -> {
            List<SdCidadeRep> toDelete = cidadesBean.stream().filter(it -> it.getStatus() != null).filter(it -> it.getStatus().equals("REMOVE") || it.getStatus().equals("REMOVET")).collect(Collectors.toList());
            List<SdCidadeRep> toAdd = cidadesBean.stream().filter(it -> it.getStatus() != null).filter(it -> it.getStatus().equals("NOVO")).collect(Collectors.toList());
            if (!toAdd.isEmpty() || !toDelete.isEmpty()) {
                if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                    message.message("Tem certeza que deseja salvar as alterações?");
                    message.showAndWait();
                }).value.get())) {
                    if (!toDelete.isEmpty()) removeCidades(toDelete);
                    if (!toAdd.isEmpty()) salvarCidades(toAdd);

                    cidadesBkp.clear();
                    cidadesBkp.addAll(cidadesBean);

                    tblCidades.items.forEach(it -> it.setStatus(null));
                    MessageBox.create(message -> {
                        message.message("Alterações realizada com Sucesso!");
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.show();
                        message.position(Pos.TOP_RIGHT);
                    });
                    tblCidades.refresh();
                }
            }
        });
    });

    private final Button btnAbrirExcel = FormButton.create(btn -> {
        btn.title("Abrir Excel");
        btn.addStyle("success").addStyle("sm");
        btn.tooltip("CODCID / CODREP / CODMARCA");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EXCEL, ImageUtils.IconSize._16));
        btn.setOnAction(evt -> {
            try {
                importExcel();
                cidadesImportadas.forEach(it -> cidades.add(0, it));
                cidadesBean.set(FXCollections.observableList(cidades));
                tblCidades.refresh();
//                new RunAsyncWithOverlay(this).exec(task -> {
//                    getCidades(codRepFilter.value.getValue(), marcaFilter.value.getValue(), codCidadeFilter.value.getValue(), codEstadoFilter.value.getValue());
//                    cidadesImportadas.forEach(it -> cidades.add(0, it));
//                    return ReturnAsync.OK.value;
//                }).addTaskEndNotification(taskReturn -> {
//                    if (taskReturn.equals(ReturnAsync.OK.value)) {
//                        cidadesBean.set(FXCollections.observableList(cidades));
//                    }
//                });
            } catch (NullPointerException e) {
                e.printStackTrace();

            }
        });
    });

    private final Button btnExcluirTodas = FormButton.create(btn -> {
        btn.title("Remover todas");
        btn.addStyle("danger");
        btn.tooltip("Marcar as Cidades selecionadas para exclusão!");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
        btn.setOnAction(evt -> {
            marcarTodasCidadesExcluir();
        });
    });

    // </editor-fold>

    public ControleCidadesRepView() {
        super("Controle Cidades dos Representantes", ImageUtils.getImage(ImageUtils.Icon.CIDADE));
        init();
    }

    private void init() {
        super.box.getChildren().add(FormBox.create(principal -> {
            principal.vertical();
            principal.add(FormBox.create(boxHeaderPanel -> {
                boxHeaderPanel.horizontal();
                boxHeaderPanel.add(FormTitledPane.create(filter -> {
                    filter.filter();
                    filter.add(FormBox.create(boxFilter -> {
                        boxFilter.vertical();
                        boxFilter.add(FormBox.create(linha1 -> {
                            linha1.horizontal();
                            linha1.add(codRepFilter.build(), marcaFilter.build());
                        }));
                        boxFilter.add(FormBox.create(linha2 -> {
                            linha2.horizontal();
                            linha2.add(codCidadeFilter.build(), codEstadoFilter.build());
                        }));
                    }));
                    filter.find.setOnAction(evt -> {
                        new RunAsyncWithOverlay(this).exec(task -> {
                            getCidades(codRepFilter.value.getValue() == null ? new Represen() : codRepFilter.value.getValue(),
                                    marcaFilter.value.getValue() == null ? new Marca() : marcaFilter.value.getValue(),
                                    codCidadeFilter.objectValues.getValue() == null ? null : codCidadeFilter.objectValues.getValue().stream().map(it -> it.getCodCid()).toArray(),
                                    codEstadoFilter.value.getValue() == null ? new TabUf() : codEstadoFilter.value.getValue());
                            return ReturnAsync.OK.value;
                        }).addTaskEndNotification(taskReturn -> {
                            if (taskReturn.equals(ReturnAsync.OK.value)) {
                                cidadesBean.set(FXCollections.observableArrayList(cidades));
                            }
                        });
                    });
                    filter.clean.setOnAction(evt -> {
                        marcaFilter.clear();
                        codEstadoFilter.clear();
                        codCidadeFilter.clear();
                        codRepFilter.clear();
                        nomeCidadeFilter.clear();
                        codRepTransfFilter.clear();
                    });
                    filter.find.disableProperty().bind(codRepFilter.value.isNull().or(marcaFilter.value.isNull()));
                }));
                boxHeaderPanel.add(FormBox.create(boxAddCidades -> {
                    boxAddCidades.vertical();
                    boxAddCidades.title("Adicionar Cidades/Estados");
                    boxAddCidades.alignment(Pos.BOTTOM_LEFT);
                    boxAddCidades.add(FormBox.create(linha1 -> {
                        linha1.horizontal();
                        linha1.alignment(Pos.BOTTOM_LEFT);
                        linha1.add(addEstadoFilter.build(), btnAddEstado);
                    }));
                    boxAddCidades.add(FormBox.create(linha2 -> {
                        linha2.horizontal();
                        linha2.alignment(Pos.BOTTOM_LEFT);
                        linha2.add(addCidadeFilter.build(), btnAddCidade);
                    }));
                }));
            }));
            principal.add(FormBox.create(boxCenterPanel -> {
                boxCenterPanel.horizontal();
                boxCenterPanel.expanded();
                boxCenterPanel.setPrefHeight(700);
                boxCenterPanel.add(tblCidades.build());
            }));
            principal.add(FormBox.create(boxFooterPanel -> {
                boxFooterPanel.horizontal();
                boxFooterPanel.add(FormBox.create(boxE -> {
                    boxE.horizontal();
                    boxE.setPrefWidth(1400);
                    boxE.alignment(Pos.BOTTOM_LEFT);
                    boxE.add(codRepTransfFilter.build(), btnTransfRep, nomeCidadeFilter.build());
                }));
                boxFooterPanel.add(FormBox.create(boxD -> {
                    boxD.horizontal();
                    boxD.alignment(Pos.BOTTOM_RIGHT);
                    boxD.add(btnExcluirTodas, btnReverter, btnConfirmar, btnAbrirExcel);
                }));
            }));
        }));
    }

    protected void removeCidades(List<SdCidadeRep> toDelete) {
        toDelete.forEach(it -> {
            try {
                oldCidade = it;
                List<SdCidadeRep> listCidades = (List<SdCidadeRep>) new FluentDao().selectFrom(SdCidadeRep.class)
                        .where(eb -> eb
                                .equal("id.codCid.codCid", it.getId().getCodCid().getCodCid())
                                .equal("marca.codigo", it.getMarca().getCodigo()))
                        .resultList();
                if (listCidades != null && listCidades.size() <= 1) {
                    new NativeDAO().runNativeQueryUpdate(String.format("UPDATE SD_CIDADE_REP_001 SET CODREP = NULL WHERE CODREP = '%s' AND MARCA = '%s' AND COD_CID = '%s'",
                            it.getId().getCodRep().getCodRep(), it.getMarca().getCodigo(), it.getId().getCodCid().getCodCid()));
                } else {
                    new NativeDAO().runNativeQueryUpdate(String.format("DELETE FROM SD_CIDADE_REP_001 WHERE CODREP = '%s' AND MARCA = '%s' AND COD_CID = '%s'",
                            it.getId().getCodRep().getCodRep(), it.getMarca().getCodigo(), it.getId().getCodCid().getCodCid()));
                }
                cidadesBean.remove(it);
                cidades.remove(it);
                SysLogger.addSysDelizLog("Controle de Cidades Representante", TipoAcao.EXCLUIR, oldCidade.getId().getCodCid().getCodCid(),
                        "Cidade " + oldCidade.getId().getCodCid().getNomeCid() + " (" + oldCidade.getId().getCodCid().getCodEst().getSigla() + ")" +
                                " removida do Representante: " +
                                it.getId().getCodRep().getCodRep() + " " +
                                oldCidade.getId().getCodRep().getNome() +
                                " na marca: " + oldCidade.getMarca().getDescricao());

            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    protected void salvarCidades(List<SdCidadeRep> cidadesToAdd) {
        cidadesToAdd.forEach(cidade -> {
            try {

                List<Object> cidades = new NativeDAO().runNativeQuery(String.format("SELECT * FROM SD_CIDADE_REP_001 WHERE MARCA = '%s' AND COD_CID = '%s'",
                        cidade.getMarca().getCodigo(), cidade.getId().getCodCid().getCodCid()));

                if (cidades.size() == 0) {
                    new NativeDAO().runNativeQueryUpdate(String.format("INSERT INTO SD_CIDADE_REP_001 VALUES ('%s','%s','%s')",
                            cidade.getId().getCodCid().getCodCid(), cidade.getId().getCodRep().getCodRep(), cidade.getMarca().getCodigo()));
                    SysLogger.addSysDelizLog("Controle de Cidades Representante", TipoAcao.CADASTRAR, cidade.getId().getCodCid().getCodCid(),
                            "Cidade " + cidade.getId().getCodCid().getNomeCid() +
                                    " adicionada ao Representante: " +
                                    cidade.getId().getCodRep().getCodRep() + " " +
                                    cidade.getId().getCodRep().getNome() +
                                    " na marca: " + cidade.getMarca().getDescricao());
                    return;
                }

                String codrep = (String) ((HashMap) cidades.get(0)).get("CODREP");

                if (cidades.size() == 2 && codrep != null && ((HashMap) cidades.get(1)).get("CODREP") != null) {
                    MessageBox.create(message -> {
                        message.message("Cidade já possui dois representantes!\nRemova de algum deles: \n- " + codrep + "\n- " + ((HashMap) cidades.get(1)).get("CODREP"));
                        message.type(MessageBox.TypeMessageBox.WARNING);
                        message.showAndWait();
                    });
                    cidades.remove(cidade);
                    cidadesBean.remove(cidade);

                } else if (cidades.size() == 1 && codrep != null && !codrep.equals(cidade.getId().getCodRep().getCodRep())) {
                    new NativeDAO().runNativeQueryUpdate(String.format("INSERT INTO SD_CIDADE_REP_001 VALUES ('%s','%s','%s')",
                            cidade.getId().getCodCid().getCodCid(), cidade.getId().getCodRep().getCodRep(), cidade.getMarca().getCodigo()));
                    SysLogger.addSysDelizLog("Controle de Cidades Representante", TipoAcao.CADASTRAR, cidade.getId().getCodCid().getCodCid(),
                            "Cidade " + cidade.getId().getCodCid().getNomeCid() +
                                    " adicionada ao Representante: " +
                                    cidade.getId().getCodRep().getCodRep() + " " +
                                    cidade.getId().getCodRep().getNome() +
                                    " na marca: " + cidade.getMarca().getDescricao());
                } else {
                    new NativeDAO().runNativeQueryUpdate(String.format("UPDATE SD_CIDADE_REP_001 SET CODREP = '%s' WHERE MARCA = '%s' AND COD_CID = '%s' AND CODREP IS NULL",
                            cidade.getId().getCodRep().getCodRep(), cidade.getMarca().getCodigo(), cidade.getId().getCodCid().getCodCid()));
                    SysLogger.addSysDelizLog("Controle de Cidades Representante", TipoAcao.CADASTRAR, cidade.getId().getCodCid().getCodCid(),
                            "Cidade " + cidade.getId().getCodCid().getNomeCid() +
                                    " adicionada ao Representante: " +
                                    cidade.getId().getCodRep().getCodRep() + " " +
                                    cidade.getId().getCodRep().getNome() +
                                    " na marca: " + cidade.getMarca().getDescricao());
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
//            new FluentDao().merge(it);

        });
    }

    protected void transferirCidades(List<SdCidadeRep> cidades, Represen representante) {
        if (cidades.stream().anyMatch(it -> it.getStatus() != null)) {
            MessageBox.create(message -> {
                message.message("Existem cidades que estão marcadas para Adicionar (Verde) ou para Exclusão (Vermerlho). \nPara realizar a transferência dessas cidades, primeiro pressione o botão confirmar!");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
                message.position(Pos.CENTER);
            });
        } else {
            cidades.forEach(it -> {
                try {
                    if (representanteJaPossui(it, representante)) {
                        MessageBox.create(message -> {
                            message.message("Representante já possui a cidade: " + it.getId().getCodCid().getNomeCid() + ", esta cidade será apagada do representante atual somente!");
                            message.type(MessageBox.TypeMessageBox.WARNING);
                            message.notification();
                            message.position(Pos.TOP_RIGHT);
                        });
                        new NativeDAO().runNativeQueryUpdate(String.format("DELETE FROM SD_CIDADE_REP_001 WHERE MARCA = '%s' AND COD_CID = '%s' AND CODREP = '%s'",
                                it.getMarca().getCodigo(), it.getId().getCodCid().getCodCid(), it.getId().getCodRep().getCodRep()));
                    } else {
                        new NativeDAO().runNativeQueryUpdate(String.format("UPDATE SD_CIDADE_REP_001 SET CODREP = '%s' WHERE MARCA = '%s' AND COD_CID = '%s' AND CODREP = '%s'",
                                representante.getCodRep(), it.getMarca().getCodigo(), it.getId().getCodCid().getCodCid(), it.getId().getCodRep().getCodRep()));
                    }
                    cidadesBean.remove(it);
                    SysLogger.addSysDelizLog("Controle de Cidades Representante", TipoAcao.EDITAR, it.getId().getCodCid().getCodCid(),
                            "Cidade " + it.getId().getCodCid().getNomeCid() +
                                    " transferida do Representante: " +
                                    it.getId().getCodRep().getCodRep() + " " +
                                    it.getId().getCodRep().getNome() +
                                    " para o Representante: " +
                                    representante.getNome() +
                                    " na marca: " + it.getMarca().getDescricao());

                } catch (SQLException e) {
                    e.printStackTrace();
                }

            });
            MessageBox.create(message -> {
                message.message("Transferência concluída com Sucesso!");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.notification();
                message.position(Pos.TOP_RIGHT);
            });
        }
    }

    protected void importExcel() {
        cidadesImportadas = new ArrayList<>();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Abrir Arquivo");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Excel File", "*.xls*", "*.xlsx"));

        try {
            Workbook workbook;
            File file = new File(fileChooser.showOpenDialog(new Stage()).getAbsolutePath());

            workbook = WorkbookFactory.create(new FileInputStream(file));

            Sheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rowIterator = sheet.rowIterator();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                SdCidadeRep cidade = new SdCidadeRep();
                SdCidadeRepPK cidadePk = new SdCidadeRepPK();

                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    switch (cell.getColumnIndex()) {
                        case 0:
                            if (cell.getCellType().equals(CellType.NUMERIC))
                                cidadePk.setCodCid(buscaCidadeDouble(cell.getNumericCellValue()));
                            else
                                cidadePk.setCodCid(buscaCidadeString(cell.getStringCellValue()));
                            break;
                        case 1:
                            try {
                                cidadePk.setCodRep(buscaRepresentanteDouble(cell.getNumericCellValue()));
                            } catch (IllegalStateException e) {
                                try {
                                    cidadePk.setCodRep(buscaRepresentanteString(cell.getStringCellValue()));
                                } catch (IllegalStateException f) {
                                    cidadePk.setCodRep(null);
                                }

                            }
                            break;
                        case 2:
                            try {
                                cidade.setMarca(buscaMarca(cell.getStringCellValue()));
                            } catch (IllegalStateException e) {
                                cidade.setMarca(null);
                            }
                            break;
                    }
                }
                cidade.setId(cidadePk);
                if (cidade.getMarca() != null && cidade.getId().getCodCid() != null && cidade.getId().getCodRep() != null)
                    cidadesImportadas.add(cidade);
            }
            workbook.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (cidadesImportadas.size() > 0) {
            cidadesImportadas.forEach(it -> it.setStatus("NOVO"));
            codRepFilter.value.setValue(cidadesImportadas.get(0).getId().getCodRep());
            marcaFilter.value.setValue(cidadesImportadas.get(0).getMarca());
            cidadesImportadas.removeIf(it -> representanteJaPossui(it, codRepFilter.value.getValue()));
            MessageBox.create(message -> {
                message.message(cidadesImportadas.size() > 0 ? "Importadas " + cidadesImportadas.size() + " cidades com sucesso.\nNão esqueça de Confirmar antes de transferir." :
                        "Nenhuma cidade importada!\nRepresentante já possui as cidades!");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.show();
                message.position(Pos.TOP_RIGHT);
            });
        } else {
            MessageBox.create(message -> {
                message.message("Documento em Formato Inválido!\nSeu excel deve ter 3 colunas com CODCID / CODREP / CODMARCA");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
                message.position(Pos.CENTER);
            });
        }

    }

    private void filtrarCidades(String newValue, String oldValue) {
        if (oldValue != null && (newValue.length() < oldValue.length())) {
            cidadesBean.set(FXCollections.observableList(cidadesBkp));
        }
        ObservableList<SdCidadeRep> subentries = FXCollections.observableArrayList();
        for (SdCidadeRep entry : cidadesBean) {
            boolean match = true;
            if (!entry.getId().getCodCid().getNomeCid().toUpperCase().contains(newValue)) {
                match = false;
                continue;
            }
            if (match) {
                subentries.add(entry);
            }
        }
        cidadesBean.set(subentries);
    }

    private void adicionarCidade(Cidade cidade) {
        SdCidadeRep newCidade = new SdCidadeRep();
        SdCidadeRepPK newCidadePk = new SdCidadeRepPK();

        newCidade.setMarca(marcaFilter.value.getValue());

        newCidadePk.setCodCid(cidade);
        newCidadePk.setCodRep(codRepFilter.value.getValue());

        newCidade.setId(newCidadePk);
        newCidade.setStatus("NOVO");

        if (cidades.stream().map(sdCidadeRep -> sdCidadeRep.getId().getCodCid().getCodCid()).noneMatch(s -> s.equals(newCidadePk.getCodCid().getCodCid())))
            cidadesBean.add(newCidade);
        else MessageBox.create(message -> {
            message.message("Cidade " + newCidade.getId().getCodCid().getNomeCid() + " - " + newCidade.getId().getCodCid().getCodEst().getId().getSiglaEst() + " já existente!");
            message.type(MessageBox.TypeMessageBox.IMPORTANT);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }

    private void adicionarCidades(List<Cidade> cidades) {
        if (codRepFilter.value.isNotNull().get())
            cidades.stream().filter(it -> addEstadoFilter.value.isNull().get() || it.getCodEst().getId().getCodigo().equals(addEstadoFilter.value.get().getId().getCodigo())).forEach(this::adicionarCidade);
    }

    private void adicionarEstado() {
        TabUf estado = addEstadoFilter.value.getValue();
        if (estado.getCidadeList() != null) {
            estado.getCidadeList().forEach(cidade -> {
                Cidade newCidade = new FluentDao().selectFrom(Cidade.class).where(it -> it.equal("codCid", cidade.getCodigo())).singleResult();
                if (newCidade != null) adicionarCidade(newCidade);
            });
        }
    }

    private void marcarTodasCidadesExcluir() {
        tblCidades.items.stream().filter(BasicModel::isSelected).forEach(eb -> eb.setStatus("REMOVET"));
        tblCidades.refresh();
    }

}
