package sysdeliz2.views.comercial.representantes;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.web.HTMLEditor;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.controllers.views.comercial.representantes.FechamentoComissaoRepresentanteController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.sysdeliz.SdPropriedade;
import sysdeliz2.models.ti.Represen;
import sysdeliz2.models.view.VSdDevolucaoRep;
import sysdeliz2.models.view.VSdFaturamentoRep;
import sysdeliz2.models.view.VSdLancamentosRep;
import sysdeliz2.models.view.VSdRecebimentosRep;
import sysdeliz2.utils.ExportExcel;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.ReportUtils;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.mails.SimpleMail;
import sysdeliz2.utils.sys.*;

import javax.swing.text.DateFormatter;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

public class FechamentoComissaoRepresentanteView extends FechamentoComissaoRepresentanteController {

    // <editor-fold defaultstate="collapsed" desc="Lists/Objects">
    private final ListProperty<HeaderFechamento> fechamentosBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<VSdFaturamentoRep> faturamentosBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<VSdRecebimentosRep> recebimentosBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<VSdDevolucaoRep> devolucaoBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<VSdLancamentosRep> lancamentosBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private BigDecimal totalFaturamento = BigDecimal.ZERO;
    private BigDecimal totalRecebimento = BigDecimal.ZERO;
    private BigDecimal totalDevolucoes = BigDecimal.ZERO;
    private BigDecimal totalLancamentos = BigDecimal.ZERO;
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Filter Fields">
    private final FormFieldMultipleFind<Represen> representanteField = FormFieldMultipleFind.create(Represen.class, field -> {
        field.title("Representante");
    });
    private final FormFieldSegmentedButton<String> statusFechamentoField = FormFieldSegmentedButton.create(statusFechamentoField -> {
        statusFechamentoField.title("Período Fechado");
        statusFechamentoField.options(
                statusFechamentoField.option("Ambos", "'N','S'", FormFieldSegmentedButton.Style.PRIMARY),
                statusFechamentoField.option("Sim", "'S'", FormFieldSegmentedButton.Style.SUCCESS),
                statusFechamentoField.option("Não", "'N'", FormFieldSegmentedButton.Style.DANGER)
        );
        statusFechamentoField.select(2);
    });
    private final FormFieldSegmentedButton<String> statusRepresentanteField = FormFieldSegmentedButton.create(statusRepresentanteField -> {
        statusRepresentanteField.title("Rep. Ativos");
        statusRepresentanteField.options(
                statusRepresentanteField.option("Ambos", "T", FormFieldSegmentedButton.Style.PRIMARY),
                statusRepresentanteField.option("Sim", "true", FormFieldSegmentedButton.Style.SUCCESS),
                statusRepresentanteField.option("Não", "false", FormFieldSegmentedButton.Style.DANGER)
        );
        statusRepresentanteField.select(1);
    });
    private final FormFieldDatePeriod periodoFechamentoField = FormFieldDatePeriod.create(field -> {
        field.title("Período Fechamento");
        field.defaultValue(LocalDate.now().withDayOfMonth(1), LocalDate.now());
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Extrato Field">
    private final FormFieldText codigoRepresenField = FormFieldText.create(field -> {
        field.title("Código");
        field.width(70.0);
        field.editable(false);
    });
    private final FormFieldText nomeRepresenField = FormFieldText.create(field -> {
        field.title("Representada");
        field.width(300.0);
        field.editable(false);
    });
    private final FormFieldText responsavelRepresenField = FormFieldText.create(field -> {
        field.title("Responsável");
        field.width(150.0);
        field.editable(false);
    });
    private final FormFieldText telefoneRepresenField = FormFieldText.create(field -> {
        field.title("Telefone");
        field.width(150.0);
        field.editable(false);
    });
    private final FormFieldText emailRepresenField = FormFieldText.create(field -> {
        field.title("E-mail");
        field.width(230.0);
        field.editable(false);
    });
    private final FormFieldText regiaoRepresenField = FormFieldText.create(field -> {
        field.title("Região");
        field.width(100.0);
        field.editable(false);
    });
    private final FormFieldText totalFaturadoField = FormFieldText.create(field -> {
        field.title("Faturado Período");
        field.addStyle("success");
        field.alignment(FormFieldText.Alignment.RIGHT);
    });
    private final FormFieldText totalRecebidoField = FormFieldText.create(field -> {
        field.title("Recebido Período");
        field.addStyle("success");
        field.alignment(FormFieldText.Alignment.RIGHT);
    });
    private final FormFieldText totalDevolvidoField = FormFieldText.create(field -> {
        field.title("Devolvido Período");
        field.addStyle("danger");
        field.alignment(FormFieldText.Alignment.RIGHT);
    });
    private final FormFieldText totalLancamentoField = FormFieldText.create(field -> {
        field.title("Lançamentos Período");
        field.alignment(FormFieldText.Alignment.RIGHT);
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Tables">
    private final FormTableView<HeaderFechamento> fechamentoTable = FormTableView.create(HeaderFechamento.class, table -> {
        table.title("Fechamentos");
        table.expanded();
        table.items.bind(fechamentosBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Cód.");
                    cln.width(35);
                    cln.value((Callback<TableColumn.CellDataFeatures<HeaderFechamento, HeaderFechamento>, ObservableValue<HeaderFechamento>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodrep().getCodRep()));
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                }).build() /*Cód.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Representante");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<HeaderFechamento, HeaderFechamento>, ObservableValue<HeaderFechamento>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodrep().getNome()));
                }).build() /*Representante*/,
                FormTableColumn.create(cln -> {
                    cln.title("Faturamen.");
                    cln.width(70);
                    cln.value((Callback<TableColumn.CellDataFeatures<HeaderFechamento, HeaderFechamento>, ObservableValue<HeaderFechamento>>) param -> new ReadOnlyObjectWrapper(param.getValue().getFaturamento()));
                    cln.alignment(FormTableColumn.Alignment.RIGHT);
                    cln.format(param -> {
                        return new TableCell<HeaderFechamento, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                clear();
                                if (item != null && !empty) {
                                    setText(StringUtils.toMonetaryFormat(item, 2));
                                    int testPositivo = item.compareTo(BigDecimal.ZERO);
                                    getStyleClass().add(testPositivo > 0 ? "financial-row-success" : testPositivo < 0 ? "financial-row-danger" : "");
                                }
                            }

                            private void clear() {
                                getStyleClass().removeAll("financial-row-danger", "financial-row-success");
                            }
                        };
                    });
                }).build() /*Faturamento*/,
                FormTableColumn.create(cln -> {
                    cln.title("Recebimen.");
                    cln.width(70);
                    cln.value((Callback<TableColumn.CellDataFeatures<HeaderFechamento, HeaderFechamento>, ObservableValue<HeaderFechamento>>) param -> new ReadOnlyObjectWrapper(param.getValue().getRecebimento()));
                    cln.alignment(FormTableColumn.Alignment.RIGHT);
                    cln.format(param -> {
                        return new TableCell<HeaderFechamento, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                clear();
                                if (item != null && !empty) {
                                    setText(StringUtils.toMonetaryFormat(item, 2));
                                    int testPositivo = item.compareTo(BigDecimal.ZERO);
                                    getStyleClass().add(testPositivo > 0 ? "financial-row-success" : testPositivo < 0 ? "financial-row-danger" : "");
                                }
                            }

                            private void clear() {
                                getStyleClass().removeAll("financial-row-danger", "financial-row-success");
                            }
                        };
                    });
                }).build() /*Recebimento*/,
                FormTableColumnGroup.createGroup(group -> {
                    group.title("Devoluções");
                    group.addColumn(FormTableColumn.create(cln -> {
                        cln.title("Total");
                        cln.width(70.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<HeaderFechamento, HeaderFechamento>, ObservableValue<HeaderFechamento>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDevolucaoTot()));
                        cln.alignment(FormTableColumn.Alignment.RIGHT);
                        cln.format(param -> {
                            return new TableCell<HeaderFechamento, BigDecimal>() {
                                @Override
                                protected void updateItem(BigDecimal item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    clear();
                                    if (item != null && !empty) {
                                        setText(StringUtils.toMonetaryFormat(item, 2));
                                        int testPositivo = item.compareTo(BigDecimal.ZERO);
                                        getStyleClass().add(testPositivo > 0 ? "financial-row-success" : testPositivo < 0 ? "financial-row-danger" : "");
                                    }
                                }

                                private void clear() {
                                    getStyleClass().removeAll("financial-row-danger", "financial-row-success");
                                }
                            };
                        });
                    }).build()); /*Total*/
                    group.addColumn(FormTableColumn.create(cln -> {
                        cln.title("Parcial");
                        cln.width(70.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<HeaderFechamento, HeaderFechamento>, ObservableValue<HeaderFechamento>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDevolucaoPar()));
                        cln.alignment(FormTableColumn.Alignment.RIGHT);
                        cln.format(param -> {
                            return new TableCell<HeaderFechamento, BigDecimal>() {
                                @Override
                                protected void updateItem(BigDecimal item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    clear();
                                    if (item != null && !empty) {
                                        setText(StringUtils.toMonetaryFormat(item, 2));
                                        int testPositivo = item.compareTo(BigDecimal.ZERO);
                                        getStyleClass().add(testPositivo > 0 ? "financial-row-success" : testPositivo < 0 ? "financial-row-danger" : "");
                                    }
                                }

                                private void clear() {
                                    getStyleClass().removeAll("financial-row-danger", "financial-row-success");
                                }
                            };
                        });
                    }).build()); /*Parcial*/
                }).build() /*Devoluções*/,
                FormTableColumnGroup.createGroup(group -> {
                    group.title("Lançamentos");
                    group.addColumn(FormTableColumn.create(cln -> {
                        cln.title("Crédito");
                        cln.width(70.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<HeaderFechamento, HeaderFechamento>, ObservableValue<HeaderFechamento>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLancCredito()));
                        cln.alignment(FormTableColumn.Alignment.RIGHT);
                        cln.format(param -> {
                            return new TableCell<HeaderFechamento, BigDecimal>() {
                                @Override
                                protected void updateItem(BigDecimal item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    clear();
                                    if (item != null && !empty) {
                                        setText(StringUtils.toMonetaryFormat(item, 2));
                                        int testPositivo = item.compareTo(BigDecimal.ZERO);
                                        getStyleClass().add(testPositivo > 0 ? "financial-row-success" : testPositivo < 0 ? "financial-row-danger" : "");
                                    }
                                }

                                private void clear() {
                                    getStyleClass().removeAll("financial-row-danger", "financial-row-success");
                                }
                            };
                        });
                    }).build());  /*Crédito*/
                    group.addColumn(FormTableColumn.create(cln -> {
                        cln.title("Débito");
                        cln.width(70.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<HeaderFechamento, HeaderFechamento>, ObservableValue<HeaderFechamento>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLancDebito()));
                        cln.alignment(FormTableColumn.Alignment.RIGHT);
                        cln.format(param -> {
                            return new TableCell<HeaderFechamento, BigDecimal>() {
                                @Override
                                protected void updateItem(BigDecimal item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    clear();
                                    if (item != null && !empty) {
                                        setText(StringUtils.toMonetaryFormat(item, 2));
                                        int testPositivo = item.compareTo(BigDecimal.ZERO);
                                        getStyleClass().add(testPositivo > 0 ? "financial-row-success" : testPositivo < 0 ? "financial-row-danger" : "");
                                    }
                                }

                                private void clear() {
                                    getStyleClass().removeAll("financial-row-danger", "financial-row-success");
                                }
                            };
                        });
                    }).build());  /*Débito*/
                }).build() /*Lançamentos*/,
                FormTableColumn.create(cln -> {
                    cln.title("Saldo");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<HeaderFechamento, HeaderFechamento>, ObservableValue<HeaderFechamento>>) param -> new ReadOnlyObjectWrapper(param.getValue().getSaldo()));
                    cln.alignment(FormTableColumn.Alignment.RIGHT);
                    cln.format(param -> {
                        return new TableCell<HeaderFechamento, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                clear();
                                if (item != null && !empty) {
                                    setText(StringUtils.toMonetaryFormat(item, 2));
                                    int testPositivo = item.compareTo(BigDecimal.ZERO);
                                    getStyleClass().add(testPositivo > 0 ? "financial-row-success" : testPositivo < 0 ? "financial-row-danger" : "");
                                }
                            }

                            private void clear() {
                                getStyleClass().removeAll("financial-row-danger", "financial-row-success");
                            }
                        };
                    });
                }).build() /*Saldo*/
        );
        table.selectionModelItem((observable, oldValue, newValue) -> {
            if (newValue != null) {
                clearExtrato();
                JPAUtils.clearEntity(((HeaderFechamento) newValue).getCodrep());
                extratoRepresentante(((HeaderFechamento) newValue).getCodrep());
            }
        });
        table.contextMenu(FormContextMenu.create(menu -> {
            menu.addItem(item -> {
                item.setText("Exportar Dados");
                item.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.EXCEL, ImageUtils.IconSize._16));
                item.setOnAction(evt -> {
                    String filePath = GUIUtils.showSaveFileDialog(new ArrayList<>(Collections.singleton(new FileChooser.ExtensionFilter("Arquivos do Excel (.xlsx)", "*.xlsx"))));
                    try {
                        new ExportExcel<HeaderFechamento>(fechamentosBean.get(), HeaderFechamento.class).exportArrayToExcel(filePath);
                        MessageBox.create(message -> {
                            message.message("Expotação de dados concluída com sucesso.");
                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                            message.position(Pos.TOP_RIGHT);
                            message.notification();
                        });
                    } catch (IOException | InvalidFormatException | NoSuchFieldException | IllegalAccessException e) {
                        e.printStackTrace();
                        ExceptionBox.build(message -> {
                            message.exception(e);
                            message.showAndWait();
                        });
                    }
                });
            });
        }));
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Tables Extrato">
    private final FormTableView<VSdFaturamentoRep> tblFaturamentos = FormTableView.create(VSdFaturamentoRep.class, table -> {
        table.title("Faturamentos");
        table.expanded();
        table.items.bind(faturamentosBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Data");
                    cln.width(70);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdFaturamentoRep, VSdFaturamentoRep>, ObservableValue<VSdFaturamentoRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDtemissao()));
                    cln.format(param -> {
                        return new TableCell<VSdFaturamentoRep, LocalDate>() {
                            @Override
                            protected void updateItem(LocalDate item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDateFormat(item));
                                }
                            }
                        };
                    });
                }).build() /*Data*/,
                FormTableColumn.create(cln -> {
                    cln.title("Fatura");
                    cln.width(65);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdFaturamentoRep, VSdFaturamentoRep>, ObservableValue<VSdFaturamentoRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getFatura()));
                }).build() /*Fatura*/,
                FormTableColumn.create(cln -> {
                    cln.title("Pedido");
                    cln.width(75);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdFaturamentoRep, VSdFaturamentoRep>, ObservableValue<VSdFaturamentoRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPedido()));
                }).build() /*Pedido*/,
                FormTableColumn.create(cln -> {
                    cln.title("Dt. Ped.");
                    cln.width(65);
                    cln.hide();
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdFaturamentoRep, VSdFaturamentoRep>, ObservableValue<VSdFaturamentoRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getEmissaoped()));
                    cln.format(param -> {
                        return new TableCell<VSdFaturamentoRep, LocalDate>() {
                            @Override
                            protected void updateItem(LocalDate item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDateFormat(item));
                                }
                            }
                        };
                    });
                }).build() /*Dt. Ped.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Tipo");
                    cln.width(50);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdFaturamentoRep, VSdFaturamentoRep>, ObservableValue<VSdFaturamentoRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTipoitem()));
                }).build() /*Tipo*/,
                FormTableColumn.create(cln -> {
                    cln.title("Tabela");
                    cln.width(50);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdFaturamentoRep, VSdFaturamentoRep>, ObservableValue<VSdFaturamentoRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTabela()));
                }).build() /*Tabela*/,
                FormTableColumn.create(cln -> {
                    cln.title("P.Méd.");
                    cln.width(50);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdFaturamentoRep, VSdFaturamentoRep>, ObservableValue<VSdFaturamentoRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPgto()));
                    cln.format(param -> {
                        return new TableCell<VSdFaturamentoRep, String>() {
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    String[] prazos = item.split("/");
                                    int sumPrazos = 0;
                                    for (String prazo : prazos) {
                                        sumPrazos += Integer.parseInt(prazo);
                                    }
                                    setText(String.valueOf(sumPrazos / prazos.length));
                                }
                            }
                        };
                    });
                }).build() /*P.Méd.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Desc.");
                    cln.width(50);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdFaturamentoRep, VSdFaturamentoRep>, ObservableValue<VSdFaturamentoRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPerdesc()));
                    cln.format(param -> {
                        return new TableCell<VSdFaturamentoRep, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toPercentualFormat(item.doubleValue() / 100, 1));
                                }
                            }
                        };
                    });
                }).build() /*Desc.*/,
                FormTableColumn.create(cln -> {
                    cln.title("V.Fat.");
                    cln.width(80);
                    cln.alignment(FormTableColumn.Alignment.RIGHT);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdFaturamentoRep, VSdFaturamentoRep>, ObservableValue<VSdFaturamentoRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPreco()));
                    cln.format(param -> {
                        return new TableCell<VSdFaturamentoRep, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toMonetaryFormat(item, 2));
                                }
                            }
                        };
                    });
                }).build() /*V.Fat.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Com.");
                    cln.width(40);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdFaturamentoRep, VSdFaturamentoRep>, ObservableValue<VSdFaturamentoRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPerccomissao()));
                    cln.format(param -> {
                        return new TableCell<VSdFaturamentoRep, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toPercentualFormat(item.doubleValue() / 100, 1));
                                }
                            }
                        };
                    });
                }).build() /*Com.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Valor");
                    cln.width(80);
                    cln.alignment(FormTableColumn.Alignment.RIGHT);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdFaturamentoRep, VSdFaturamentoRep>, ObservableValue<VSdFaturamentoRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValcomi()));
                    cln.format(param -> {
                        return new TableCell<VSdFaturamentoRep, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                getStyleClass().remove("financial-row-success");
                                if (item != null && !empty) {
                                    getStyleClass().add("financial-row-success");
                                    setText(StringUtils.toMonetaryFormat(item, 2));
                                }
                            }
                        };
                    });
                }).build() /*Valor*/
        );
    });
    private final FormTableView<VSdRecebimentosRep> tblRecebimentos = FormTableView.create(VSdRecebimentosRep.class, table -> {
        table.title("Recebimentos");
        table.expanded();
        table.items.bind(recebimentosBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Data");
                    cln.width(70);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdRecebimentosRep, VSdRecebimentosRep>, ObservableValue<VSdRecebimentosRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDtrecebe()));
                    cln.format(param -> {
                        return new TableCell<VSdRecebimentosRep, LocalDate>() {
                            @Override
                            protected void updateItem(LocalDate item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDateFormat(item));
                                }
                            }
                        };
                    });
                }).build() /*Data*/,
                FormTableColumn.create(cln -> {
                    cln.title("Duplicata");
                    cln.width(75);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdRecebimentosRep, VSdRecebimentosRep>, ObservableValue<VSdRecebimentosRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDuplicata()));
                }).build() /*Duplicata*/,
                FormTableColumn.create(cln -> {
                    cln.title("Fatura");
                    cln.width(65);
                    cln.hide();
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdRecebimentosRep, VSdRecebimentosRep>, ObservableValue<VSdRecebimentosRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getFatura()));
                }).build() /*Fatura*/,
                FormTableColumn.create(cln -> {
                    cln.title("Pedido");
                    cln.width(75);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdRecebimentosRep, VSdRecebimentosRep>, ObservableValue<VSdRecebimentosRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPedido()));
                }).build() /*Pedido*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cliente");
                    cln.width(50);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdRecebimentosRep, VSdRecebimentosRep>, ObservableValue<VSdRecebimentosRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodcli()));
                }).build() /*Cliente*/,
                FormTableColumn.create(cln -> {
                    cln.title("Dt. Ped.");
                    cln.width(65);
                    cln.hide();
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdRecebimentosRep, VSdRecebimentosRep>, ObservableValue<VSdRecebimentosRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPedemissao()));
                    cln.format(param -> {
                        return new TableCell<VSdRecebimentosRep, LocalDate>() {
                            @Override
                            protected void updateItem(LocalDate item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDateFormat(item));
                                }
                            }
                        };
                    });
                }).build() /*Dt. Ped.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Tipo");
                    cln.width(50);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdRecebimentosRep, VSdRecebimentosRep>, ObservableValue<VSdRecebimentosRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTipo()));
                }).build() /*Tipo*/,
                FormTableColumn.create(cln -> {
                    cln.title("Tabela");
                    cln.width(50);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdRecebimentosRep, VSdRecebimentosRep>, ObservableValue<VSdRecebimentosRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTabpre()));
                }).build() /*Tabela*/,
                FormTableColumn.create(cln -> {
                    cln.title("V.Rec.");
                    cln.width(80);
                    cln.alignment(FormTableColumn.Alignment.RIGHT);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdRecebimentosRep, VSdRecebimentosRep>, ObservableValue<VSdRecebimentosRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getBasecomissao()));
                    cln.format(param -> {
                        return new TableCell<VSdRecebimentosRep, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toMonetaryFormat(item, 2));
                                }
                            }
                        };
                    });
                }).build() /*V.Rec.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Com.");
                    cln.width(40);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdRecebimentosRep, VSdRecebimentosRep>, ObservableValue<VSdRecebimentosRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPerccomissao()));
                    cln.format(param -> {
                        return new TableCell<VSdRecebimentosRep, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toPercentualFormat(item.doubleValue() / 100, 1));
                                }
                            }
                        };
                    });
                }).build() /*Com.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Valor");
                    cln.width(80);
                    cln.alignment(FormTableColumn.Alignment.RIGHT);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdRecebimentosRep, VSdRecebimentosRep>, ObservableValue<VSdRecebimentosRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getComissao()));
                    cln.format(param -> {
                        return new TableCell<VSdRecebimentosRep, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                getStyleClass().remove("financial-row-success");
                                if (item != null && !empty) {
                                    getStyleClass().add("financial-row-success");
                                    setText(StringUtils.toMonetaryFormat(item, 2));
                                }
                            }
                        };
                    });
                }).build() /*Valor*/
        );
    });
    private final FormTableView<VSdLancamentosRep> tblLancamentos = FormTableView.create(VSdLancamentosRep.class, table -> {
        table.title("Lançamentos");
        table.expanded();
        table.items.bind(lancamentosBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Data");
                    cln.width(70);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdLancamentosRep, VSdLancamentosRep>, ObservableValue<VSdLancamentosRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDtlan()));
                    cln.format(param -> {
                        return new TableCell<VSdLancamentosRep, LocalDate>() {
                            @Override
                            protected void updateItem(LocalDate item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDateFormat(item));
                                }
                            }
                        };
                    });
                }).build() /*Data*/,
                FormTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(300);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdLancamentosRep, VSdLancamentosRep>, ObservableValue<VSdLancamentosRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDescricao()));
                }).build() /*Descrição*/,
                FormTableColumn.create(cln -> {
                    cln.title("Crédito");
                    cln.width(80);
                    cln.alignment(FormTableColumn.Alignment.RIGHT);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdLancamentosRep, VSdLancamentosRep>, ObservableValue<VSdLancamentosRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCredito()));
                    cln.format(param -> {
                        return new TableCell<VSdLancamentosRep, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                getStyleClass().remove("financial-row-success");
                                if (item != null && !empty) {
                                    setText(StringUtils.toMonetaryFormat(item, 2));
                                    if (item.compareTo(BigDecimal.ZERO) != 0)
                                        getStyleClass().add("financial-row-success");
                                }
                            }
                        };
                    });
                }).build() /*Crédito*/,
                FormTableColumn.create(cln -> {
                    cln.title("Débito");
                    cln.width(80);
                    cln.alignment(FormTableColumn.Alignment.RIGHT);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdLancamentosRep, VSdLancamentosRep>, ObservableValue<VSdLancamentosRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDebito()));
                    cln.format(param -> {
                        return new TableCell<VSdLancamentosRep, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                getStyleClass().remove("financial-row-danger");
                                if (item != null && !empty) {
                                    setText(StringUtils.toMonetaryFormat(item, 2));
                                    if (item.compareTo(BigDecimal.ZERO) != 0)
                                        getStyleClass().add("financial-row-danger");
                                }
                            }
                        };
                    });
                }).build() /*Débito*/
        );
        table.factoryRow(param -> {
            TableRow<VSdLancamentosRep> row = new TableRow<VSdLancamentosRep>() {

            };
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() >= 2) {
                    MessageBox.create(message -> {
                        message.message(((VSdLancamentosRep) table.selectedItem()).getDescricao());
                        message.type(MessageBox.TypeMessageBox.CLEAN);
                        message.showAndWait();
                    });
                }
            });
            return row;
        });
    });
    private final FormTableView<VSdDevolucaoRep> tblDevolucaoTot = FormTableView.create(VSdDevolucaoRep.class, table -> {
        table.title("Devoluções Totais");
        table.expanded();
        table.items.bind(devolucaoBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Dev");
                    cln.width(30);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDevolucaoRep, VSdDevolucaoRep>, ObservableValue<VSdDevolucaoRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTipodev()));
                    cln.format(param -> {
                        return new TableCell<VSdDevolucaoRep, String>() {
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                clear();
                                if (item != null && !empty) {
                                    setText(item);
                                    getStyleClass().add(item.equals("T") ? "table-row-danger" : "table-row-warning");
                                }
                            }

                            private void clear() {
                                getStyleClass().removeAll("table-row-danger", "table-row-warning");
                            }
                        };
                    });
                }).build() /*Dev*/,
                FormTableColumn.create(cln -> {
                    cln.title("Data");
                    cln.width(70);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDevolucaoRep, VSdDevolucaoRep>, ObservableValue<VSdDevolucaoRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDtemissao()));
                    cln.format(param -> {
                        return new TableCell<VSdDevolucaoRep, LocalDate>() {
                            @Override
                            protected void updateItem(LocalDate item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDateFormat(item));
                                }
                            }
                        };
                    });
                }).build() /*Data*/,
                FormTableColumn.create(cln -> {
                    cln.title("NF Dev.");
                    cln.width(65);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDevolucaoRep, VSdDevolucaoRep>, ObservableValue<VSdDevolucaoRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getFatura()));
                }).build() /*NF Dev.*/,
                FormTableColumn.create(cln -> {
                    cln.title("NF/Dup");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDevolucaoRep, VSdDevolucaoRep>, ObservableValue<VSdDevolucaoRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNfdup()));
                }).build() /*NF/Dup*/,
                FormTableColumn.create(cln -> {
                    cln.title("Tipo");
                    cln.width(50);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDevolucaoRep, VSdDevolucaoRep>, ObservableValue<VSdDevolucaoRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTipoitem()));
                }).build() /*Tipo*/,
                FormTableColumn.create(cln -> {
                    cln.title("Tabela");
                    cln.width(50);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDevolucaoRep, VSdDevolucaoRep>, ObservableValue<VSdDevolucaoRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTabela()));
                }).build() /*Tabela*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cliente");
                    cln.width(50);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDevolucaoRep, VSdDevolucaoRep>, ObservableValue<VSdDevolucaoRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodcli()));
                }).build() /*Cliente*/,
                FormTableColumn.create(cln -> {
                    cln.title("V.Total");
                    cln.width(80);
                    cln.alignment(FormTableColumn.Alignment.RIGHT);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDevolucaoRep, VSdDevolucaoRep>, ObservableValue<VSdDevolucaoRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getVtotal()));
                    cln.format(param -> {
                        return new TableCell<VSdDevolucaoRep, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toMonetaryFormat(item, 2));
                                }
                            }
                        };
                    });
                }).build() /*V.Total*/,
                FormTableColumn.create(cln -> {
                    cln.title("Com.");
                    cln.width(40);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDevolucaoRep, VSdDevolucaoRep>, ObservableValue<VSdDevolucaoRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPerccomissao()));
                    cln.format(param -> {
                        return new TableCell<VSdDevolucaoRep, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toPercentualFormat(item.doubleValue() / 100, 1));
                                }
                            }
                        };
                    });
                }).build() /*Com.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Valor");
                    cln.width(80);
                    cln.alignment(FormTableColumn.Alignment.RIGHT);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDevolucaoRep, VSdDevolucaoRep>, ObservableValue<VSdDevolucaoRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValcomi()));
                    cln.format(param -> {
                        return new TableCell<VSdDevolucaoRep, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                getStyleClass().remove("financial-row-danger");
                                if (item != null && !empty) {
                                    getStyleClass().add("financial-row-danger");
                                    setText(StringUtils.toMonetaryFormat(item, 2));
                                }
                            }
                        };
                    });
                }).build() /*Valor*/
        );
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Boxs">
    FormBox boxExtrato = FormBox.create(box -> {
        box.vertical();
        box.title("Extrato Representante");
        box.expanded();
    });
    // </editor-fold>

    private String textoSalvo = "";

    public FechamentoComissaoRepresentanteView() {
        super("Fechamento Comissão", ImageUtils.getImage(ImageUtils.Icon.BID));
        init();
    }

    private void init() {
        super.box.getChildren().add(FormBox.create(boxPrincipal -> {
            boxPrincipal.horizontal();
            boxPrincipal.expanded();
            boxPrincipal.add(FormBox.create(boxReps -> {
                boxReps.vertical();
                boxReps.add(FormTitledPane.create(filter -> {
                    filter.filter();
                    filter.add(FormBox.create(boxFilter -> {
                        boxFilter.horizontal();
                        boxFilter.add(FormBox.create(box -> {
                            box.vertical();
                            box.add(periodoFechamentoField.build());
                            box.add(representanteField.build());
                        }));
                        boxFilter.add(FormBox.create(box -> {
                            box.vertical();
                            box.add(statusFechamentoField.build());
                            box.add(statusRepresentanteField.build());
                        }));
                    }));
                    filter.find.setOnAction(evt -> {
                        if (!periodoFechamentoField.validateExistPeriodo()) {
                            MessageBox.create(message -> {
                                message.message("É necessário informar o período da consulta");
                                message.type(MessageBox.TypeMessageBox.IMPORTANT);
                                message.showAndWait();
                            });
                            return;
                        } else if (!periodoFechamentoField.validateEndGtBegin()) {
                            MessageBox.create(message -> {
                                message.message("A data final do período deve ser maior ou igual a inicial");
                                message.type(MessageBox.TypeMessageBox.IMPORTANT);
                                message.showAndWait();
                            });
                            return;
                        }
                        new RunAsyncWithOverlay(this).addTaskEndNotification(taskReturn -> {
                            if (taskReturn.equals(ReturnAsync.OK.value)) {
                                fechamentosBean.set(FXCollections.observableList(fechamentos));
                            }
                        }).exec(task -> {
                            getFechamentos(representanteField.textValue.get() != null ? representanteField.textValue.get().split(",") : null,
                                    periodoFechamentoField.valueBegin.get(),
                                    periodoFechamentoField.valueEnd.get(),
                                    statusFechamentoField.value.get(),
                                    statusRepresentanteField.value.get());
                            return ReturnAsync.OK.value;
                        });
                    });
                    filter.clean.setOnAction(evt -> {
                        representanteField.clear();
                        statusFechamentoField.select(2);
                        statusRepresentanteField.select(1);
                        periodoFechamentoField.clear();
                    });
                }));
                boxReps.add(fechamentoTable.build());
            }));
            boxPrincipal.add(boxExtrato);
        }));
        boxExtrato.add(FormBox.create(box -> {
            box.flutuante();
            box.add(codigoRepresenField.build(),
                    nomeRepresenField.build(),
                    responsavelRepresenField.build(),
                    emailRepresenField.build(),
                    telefoneRepresenField.build(),
                    regiaoRepresenField.build());
        }));
        boxExtrato.add(FormBox.create(box -> {
            box.flutuante();
            box.add(totalFaturadoField.build(),
                    totalRecebidoField.build(),
                    totalDevolvidoField.build(),
                    totalLancamentoField.build());
        }));
        boxExtrato.add(FormBox.create(box -> {
            box.horizontal();
            box.expanded();
            box.add(tblFaturamentos.build(), tblRecebimentos.build());
        }));
        boxExtrato.add(FormBox.create(box -> {
            box.horizontal();
            box.expanded();
            box.add(tblDevolucaoTot.build(), tblLancamentos.build());
        }));
        boxExtrato.add(FormBox.create(box -> {
            box.horizontal();
            box.add(FormButton.create(btn -> {
                btn.title("Imprimir Extrato");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.IMPRIMIR, ImageUtils.IconSize._24));
                btn.addStyle("primary");
                btn.setAction(evt -> {
                    HeaderFechamento repSelecionado = fechamentoTable.selectedItem();
                    new RunAsyncWithOverlay(this).addTaskEndNotification(taskReturn -> {
                        if (taskReturn.equals(ReturnAsync.OK.value)) {

                        }
                    }).exec(task -> {
                        try {
                            new ReportUtils().config().addReport(ReportUtils.ReportFile.EXTRATO_COMISSAO_FECHAMENTO,
                                    new ReportUtils.ParameterReport[]{
                                            new ReportUtils.ParameterReport("codrep", repSelecionado.getCodrep().getCodRep()),
                                            new ReportUtils.ParameterReport("dtInicio", StringUtils.toDateFormat(periodoFechamentoField.valueBegin.get())),
                                            new ReportUtils.ParameterReport("dtFim", StringUtils.toDateFormat(periodoFechamentoField.valueEnd.get()))
                                    }).view().toView();
                        } catch (JRException | SQLException e) {
                            e.printStackTrace();
                            ExceptionBox.build(message -> {
                                message.exception(e);
                                message.showAndWait();
                            });
                        }
                        return ReturnAsync.OK.value;
                    });
                });
            }));
            box.add(FormButton.create(btn -> {
                btn.title("Enviar Extrato");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._24));
                btn.addStyle("info");
                btn.setAction(evt -> {
                    new Fragment().show(fragment -> {
                        fragment.title("Enviar E-mail");
                        fragment.size(700.0, 500.0);
                        HeaderFechamento repSelecionado = fechamentoTable.selectedItem();

                        final HTMLEditor editorEmail = new HTMLEditor();
                        fragment.box.getChildren().add(editorEmail);
                        final Button btnEnviarExtrato = FormButton.create(btnEnviar -> {
                            btnEnviar.title("Enviar");
                            btnEnviar.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._24));
                            btnEnviar.addStyle("success");
                            btnEnviar.setAction(evtEnviar -> {
                                String fileName = repSelecionado.getCodrep().getCodRep() + " - " + repSelecionado.getCodrep().getRespon().concat(".pdf");
                                fileName = "K:\\SysDeliz\\Arquivos\\Extratos Representantes\\" + LocalDate.now().format(DateTimeFormatter.ofPattern("MMMM-yy")) +"\\" +  fileName;

                                String finalFileName = fileName;
                                final Exception[] printStack = {null};
                                new RunAsyncWithOverlay(fragment).addTaskEndNotification(taskReturn -> {
                                    if (taskReturn.equals(ReturnAsync.EXCEPTION.value)) {
                                        ExceptionBox.build(message -> {
                                            message.exception(printStack[0]);
                                            message.showAndWait();
                                        });
                                    } else if (taskReturn.equals(ReturnAsync.OK.value)) {
                                        SimpleMail emailSender = SimpleMail.INSTANCE;

                                        String[] mailsRep = repSelecionado.getCodrep().getEmail().split(",");
//                                            String[] mailsRep = "diego@deliz.com.br,alain@deliz.com.br,ddefaveri@gmail.com".split(",");
                                        for (String mail : mailsRep) {
                                            emailSender.addDestinatario(mail);
                                        }
                                        emailSender.addReplyTo("anabeatriz@deliz.com.br")
                                                .setSender(Globals.getUsuarioLogado().getDisplayName())
                                                .addCC("anabeatriz@deliz.com.br")
                                                .addCCO("diego@deliz.com.br")
                                                .comAssunto("Extrato Fechamento Comissão - Período " + StringUtils.toDateFormat(periodoFechamentoField.valueBegin.get()) + " - " + StringUtils.toDateFormat(periodoFechamentoField.valueEnd.get()))
                                                .comCorpoHtml(() -> editorEmail.getHtmlText())
                                                .addAttachment(finalFileName)
                                                .send();

                                        SysLogger.addSysDelizLog("Fechamento Comissao", TipoAcao.ENVIAR, repSelecionado.getCodigo(), "Enviado extrato de fechamento para o representante.");
                                        if ((Boolean) QuestionBox.build(Boolean.class, message -> {
                                            message.message("Deseja fechar o período selecionado para o representante?");
                                            message.showAndWait();
                                        }).value.get()) {
                                            fecharPeriodoRepresentante(repSelecionado, periodoFechamentoField.valueBegin.get(), periodoFechamentoField.valueEnd.get());
                                            MessageBox.create(message -> {
                                                message.message("Período fechado para o representante.");
                                                message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                message.position(Pos.TOP_RIGHT);
                                                message.notification();
                                            });
                                        }
                                        MessageBox.create(message -> {
                                            message.message("Extrato enviado para o representante.");
                                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                                            message.position(Pos.TOP_RIGHT);
                                            message.notification();
                                        });
                                        fragment.close();
                                    }
                                }).exec(task -> {
                                    try {
                                        new ReportUtils().config().addReport(ReportUtils.ReportFile.EXTRATO_COMISSAO_FECHAMENTO,
                                                new ReportUtils.ParameterReport[]{
                                                        new ReportUtils.ParameterReport("codrep", repSelecionado.getCodrep().getCodRep()),
                                                        new ReportUtils.ParameterReport("dtInicio", StringUtils.toDateFormat(periodoFechamentoField.valueBegin.get())),
                                                        new ReportUtils.ParameterReport("dtFim", StringUtils.toDateFormat(periodoFechamentoField.valueEnd.get()))
                                                }).view().toPdf(finalFileName);
                                        return ReturnAsync.OK.value;
                                    } catch (IOException | JRException | SQLException e) {
                                        e.printStackTrace();
                                        printStack[0] = e;
                                        return ReturnAsync.EXCEPTION.value;
                                    }
                                });
                            });
                        });
                        fragment.buttonsBox.getChildren().add(FormButton.create(sBtn -> {
                            sBtn.setText("Salvar Texto");
                            sBtn.addStyle("primary");
                            sBtn.icon(ImageUtils.getIcon(ImageUtils.Icon.SALVAR, ImageUtils.IconSize._24));
                            sBtn.setOnAction(event -> {
                                salvarTexto(editorEmail.getHtmlText());
                            });
                        }));
                        fragment.buttonsBox.getChildren().add(FormButton.create(sBtn -> {
                            sBtn.setText("Carregar Texto");
                            sBtn.addStyle("warning");
                            sBtn.icon(ImageUtils.getIcon(ImageUtils.Icon.RETORNAR, ImageUtils.IconSize._24));
                            sBtn.setOnAction(event -> {
                                carregarTexto(editorEmail);
                            });
                        }));
                        fragment.buttonsBox.getChildren().add(new Separator(Orientation.VERTICAL));
                        fragment.buttonsBox.getChildren().add(btnEnviarExtrato);
                    });
                });
            }));
            box.add(FormButton.create(btn -> {
                btn.title("Fechar Período");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.COMISSAO, ImageUtils.IconSize._24));
                btn.addStyle("success");
                btn.setAction(evt -> {
                    if (!((Boolean) QuestionBox.build(Boolean.class, message -> {
                        message.message("Deseja realmente fechar o período para o representante? Esse processo é irreversível.");
                        message.showAndWait();
                    }).value.get())) {
                        return;
                    }

                    HeaderFechamento repSelecionado = fechamentoTable.selectedItem();
                    fecharPeriodoRepresentante(repSelecionado, periodoFechamentoField.valueBegin.get(), periodoFechamentoField.valueEnd.get());
                    MessageBox.create(message -> {
                        message.message("Período de comissão fechado para o cliente.");
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.position(Pos.TOP_RIGHT);
                        message.notification();
                    });
                });
            }));
        }));
        new RunAsyncWithOverlay(this).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                fechamentosBean.set(FXCollections.observableList(fechamentos));
            }
        }).exec(task -> {
            getFechamentos(null, LocalDate.now().with(TemporalAdjusters.firstDayOfMonth()), LocalDate.now().with(TemporalAdjusters.lastDayOfMonth()), null, null);
            return ReturnAsync.OK.value;
        });
    }

    private void carregarTexto(HTMLEditor editorEmail) {
        if (textoSalvo == null || textoSalvo.equals("")) {
            SdPropriedade propriedade = new FluentDao().selectFrom(SdPropriedade.class).where(it -> it
                    .equal("usuario", Globals.getUsuarioLogado().getUsuario())
                    .equal("idTela", SceneMainController.getTabPaneGlobal().getSelectionModel().getSelectedItem().getId())
                    .equal("propriedade", "EMAILREP")
            ).singleResult();
            if (propriedade != null && propriedade.getValor() != null) textoSalvo = propriedade.getValor();
        }
        editorEmail.setHtmlText(textoSalvo);
    }

    private void salvarTexto(String htmlText) {
        textoSalvo = htmlText;
        SdPropriedade propriedade = new FluentDao().selectFrom(SdPropriedade.class).where(it -> it
                .equal("usuario", Globals.getUsuarioLogado().getUsuario())
                .equal("idTela", SceneMainController.getTabPaneGlobal().getSelectionModel().getSelectedItem().getId())
                .equal("propriedade", "EMAILREP")
        ).singleResult();
        if (propriedade == null) {
            propriedade = new SdPropriedade(Globals.getUsuarioLogado().getUsuario(), SceneMainController.getTabPaneGlobal().getSelectionModel().getSelectedItem().getId(), "EMAILREP", htmlText);
            try {
                propriedade = new FluentDao().persist(propriedade);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            JPAUtils.getEntityManager().refresh(propriedade);
            propriedade.setValor(htmlText);
            propriedade = new FluentDao().merge(propriedade);
        }

        MessageBox.create(message -> {
            message.message("Texto salvo com sucesso!");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }

    private void extratoRepresentante(Represen represen) {
        new RunAsyncWithOverlay(this).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                codigoRepresenField.value.set(represen.getCodRep());
                nomeRepresenField.value.set(represen.getNome());
                responsavelRepresenField.value.set(represen.getRespon());
                emailRepresenField.value.set(represen.getTelefone());
                telefoneRepresenField.value.set(represen.getEmail());
                regiaoRepresenField.value.set(represen.getRegiao());

                faturamentosBean.set(FXCollections.observableList(faturamentos));
                recebimentosBean.set(FXCollections.observableList(recebimentos));
                devolucaoBean.set(FXCollections.observableList(devolucoes));
                lancamentosBean.set(FXCollections.observableList(lancamentos));

                totalFaturadoField.value.set(StringUtils.toMonetaryFormat(totalFaturamento, 2));
                totalRecebidoField.value.set(StringUtils.toMonetaryFormat(totalRecebimento, 2));
                totalDevolvidoField.value.set(StringUtils.toMonetaryFormat(totalDevolucoes, 2));
                totalLancamentoField.value.set(StringUtils.toMonetaryFormat(totalLancamentos, 2));
                totalLancamentoField.removeStyle("danger").removeStyle("success");
                totalLancamentoField.addStyle(totalLancamentos.compareTo(BigDecimal.ZERO) < 0 ? "danger" : "success");
            }
        }).exec(task -> {
            getFaturamentos(represen.getCodRep(), periodoFechamentoField.valueBegin.get(), periodoFechamentoField.valueEnd.get());
            getRecebimentos(represen.getCodRep(), periodoFechamentoField.valueBegin.get(), periodoFechamentoField.valueEnd.get());
            getLancamentos(represen.getCodRep(), periodoFechamentoField.valueBegin.get(), periodoFechamentoField.valueEnd.get());
            getDevolucoes(represen.getCodRep(), periodoFechamentoField.valueBegin.get(), periodoFechamentoField.valueEnd.get());

            totalFaturamento = new BigDecimal(faturamentos.stream().mapToDouble(value -> value.getPreco().doubleValue()).sum());
            totalRecebimento = new BigDecimal(recebimentos.stream().mapToDouble(value -> value.getBasecomissao().doubleValue()).sum());
            totalDevolucoes = new BigDecimal(devolucoes.stream().mapToDouble(value -> value.getVtotal().doubleValue()).sum());
            totalLancamentos = new BigDecimal(lancamentos.stream().mapToDouble(value -> value.getCredito().add(value.getDebito()).doubleValue()).sum());

            return ReturnAsync.OK.value;
        });
    }

    private void clearExtrato() {
        codigoRepresenField.clear();
        nomeRepresenField.clear();
        responsavelRepresenField.clear();
        responsavelRepresenField.clear();
        emailRepresenField.clear();
        telefoneRepresenField.clear();
        regiaoRepresenField.clear();
        faturamentos.clear();
        recebimentos.clear();
    }
}
