package sysdeliz2.views.comercial.representantes;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.converter.NumberStringConverter;
import sysdeliz2.controllers.views.comercial.representantes.MostruarioRepController;
import sysdeliz2.controllers.views.expedicao.RetornoMostruarioController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.sysdeliz.SdColecaoMarca001;
import sysdeliz2.models.sysdeliz.SdItensCaixaRemessa;
import sysdeliz2.models.sysdeliz.SdProdutoColecao;
import sysdeliz2.models.sysdeliz.comercial.SdMostrRep;
import sysdeliz2.models.sysdeliz.comercial.SdMostrRepItens;
import sysdeliz2.models.sysdeliz.comercial.SdTransfProdMostruario;
import sysdeliz2.models.ti.*;
import sysdeliz2.models.view.VSdDadosProduto;
import sysdeliz2.models.view.comercial.VSdMostRetornoLiberado;
import sysdeliz2.models.view.expedicao.VSdDevolucaoMostRep;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.exceptions.ContinueException;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.SysLogger;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class MostruarioRepView extends MostruarioRepController {

    // <editor-fold defaultstate="collapsed" desc="bean">
    private final ListProperty<SdProdutoColecao> beanProdutos = new SimpleListProperty<>();
    private final ListProperty<SdMostrRep> beanMostruarios = new SimpleListProperty<>();
    private final ListProperty<SdMostrRepItens> beanItensMostruario = new SimpleListProperty<>();
    private final ListProperty<Represen> beanRepresentantes = new SimpleListProperty<>();
    private final BooleanProperty emEndicao = new SimpleBooleanProperty(false);
    private final ObjectProperty<SdMostrRep> mostruarioSelecionado = new SimpleObjectProperty<>();
    //private List<MostRep> mostReps = new ArrayList<>();
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="views">
    private VBox tabCadastro = ((VBox) super.tabs.getTabs().get(0).getContent());
    private VBox tabListagem = ((VBox) super.tabs.getTabs().get(1).getContent());
    private VBox tabManutencao = ((VBox) super.tabs.getTabs().get(2).getContent());
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fields">
    // cadastro
    private final FormFieldMultipleFind<Marca> fieldMarcaProduto = FormFieldMultipleFind.create(Marca.class, field -> {
        field.title("Marca");
        field.width(160.0);
        field.toUpper();
    });
    private final FormFieldMultipleFind<Produto> fieldProdutos = FormFieldMultipleFind.create(Produto.class, field -> {
        field.title("Produtos");
        field.width(350.0);
        field.toUpper();
    });
    private final FormFieldSingleFind<Colecao> fieldColecaoVenda = FormFieldSingleFind.create(Colecao.class, field -> {
        field.title("Coleção Venda");
        field.addStyle("lg");
        field.withoutDescription(90.0);
    });
    private final FormFieldMultipleFind<TabUf> fieldEstadoRepresentante = FormFieldMultipleFind.create(TabUf.class, field -> {
        field.title("UF");
        field.toUpper();
        field.codeReference.set("sigla");
        field.textValue.addListener((observable, oldValue, newValue) -> {
            if (newValue != null && newValue.equals("")) field.objectValues.clear();
        });
    });
    private final FormFieldMultipleFind<Cidade> fieldCidadeRepresentante = FormFieldMultipleFind.create(Cidade.class, field -> {
        field.title("Cidade");
        field.width(300.0);
    });
    private final FormFieldMultipleFind<Represen> fieldRepresentante = FormFieldMultipleFind.create(Represen.class, field -> {
        field.title("Representante");
        field.width(200.0);
    });
    private final FormFieldSegmentedButton<String> fieldAtivoRepresentante = FormFieldSegmentedButton.create(field -> {
        field.title("Ativo");
        field.options(
                field.option("Ambos", "t", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("Sim", "true", FormFieldSegmentedButton.Style.SUCCESS),
                field.option("Não", "false", FormFieldSegmentedButton.Style.DANGER)
        );
        field.select(1);
    });
    private final FormFieldMultipleFind<EtqProd> fieldModelagemProduto = FormFieldMultipleFind.create(EtqProd.class, field -> {
        field.title("Modelagem");
    });
    // listagem
    private final FormFieldMultipleFind<Colecao> fieldColecaoListagem = FormFieldMultipleFind.create(Colecao.class, field -> {
        field.title("Coleção");
    });
    private final FormFieldMultipleFind<Represen> fieldRepresentanteListagem = FormFieldMultipleFind.create(Represen.class, field -> {
        field.title("Representante");
    });
    private final FormFieldMultipleFind<Marca> fieldMarcaListagem = FormFieldMultipleFind.create(Marca.class, field -> {
        field.title("Marca");
        field.toUpper();
    });
    private final FormFieldSegmentedButton<String> fieldTipoListagem = FormFieldSegmentedButton.create(field -> {
        field.title("Tipo");
        field.options(
                field.option("Ambos", "T", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("Jeans", "J", FormFieldSegmentedButton.Style.WARNING),
                field.option("Moda", "M", FormFieldSegmentedButton.Style.SUCCESS)
        );
        field.select(0);
    });
    // manutenção
    private final FormFieldText fieldRepresentanteManutencao = FormFieldText.create(field -> {
        field.title("Representante");
        field.width(300.0);
        field.editable.set(false);
    });
    private final FormFieldText fieldTipoManutencao = FormFieldText.create(field -> {
        field.title("Tipo");
        field.width(60.0);
        field.alignment(Pos.CENTER);
        field.editable.set(false);
    });
    private final FormFieldText fieldMarcaManutencao = FormFieldText.create(field -> {
        field.title("Marca");
        field.width(120.0);
        field.editable.set(false);
    });
    private final FormFieldText fieldColecaoManutencao = FormFieldText.create(field -> {
        field.title("Coleção");
        field.width(70.0);
        field.alignment(Pos.CENTER);
        field.editable.set(false);
    });
    private final FormFieldText fieldMostruarioManutencao = FormFieldText.create(field -> {
        field.title("Mostr.");
        field.width(50.0);
        field.alignment(Pos.CENTER);
        field.editable.set(false);
    });
    private final FormFieldText fieldStatusManutencao = FormFieldText.create(field -> {
        field.title("Status");
        field.width(90.0);
        field.alignment(Pos.CENTER);
        field.editable.set(false);
    });
    private final FormFieldText fieldNumeroManutencao = FormFieldText.create(field -> {
        field.title("Pedido");
        field.width(70.0);
        field.alignment(Pos.CENTER);
        field.editable.set(false);
    });
    private final FormFieldSegmentedBar fieldDistribuicaoPecas = FormFieldSegmentedBar.create(field -> {
        field.title("Distribuição Peças");
    });
    private final FormFieldMultipleFind<VSdDadosProduto> fieldProdutoManutencao = FormFieldMultipleFind.create(VSdDadosProduto.class, field -> {
        field.title("Produto");
        field.toUpper();
        field.width(400.0);
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="tables">
    private final FormTableView<SdProdutoColecao> tblProdutos = FormTableView.create(SdProdutoColecao.class, table -> {
        table.title("Produtos");
        table.expanded();
        table.items.bind(beanProdutos);
        table.editable.set(true);
        table.selectColumn();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdProdutoColecao, SdProdutoColecao>, ObservableValue<SdProdutoColecao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodigo().getCodigo()));
                }).build() /*Código*/,
                FormTableColumn.create(cln -> {
                    cln.title("Produto");
                    cln.width(300.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdProdutoColecao, SdProdutoColecao>, ObservableValue<SdProdutoColecao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodigo().getDescricao()));
                }).build() /*Produto*/,
                FormTableColumn.create(cln -> {
                    cln.title("Linha");
                    cln.width(200.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdProdutoColecao, SdProdutoColecao>, ObservableValue<SdProdutoColecao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodigo().getLinha()));
                }).build() /*Linha*/,
                FormTableColumn.create(cln -> {
                    cln.title("Tipo");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdProdutoColecao, SdProdutoColecao>, ObservableValue<SdProdutoColecao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodigo().getTipo()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<SdProdutoColecao, String>() {
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (!empty) {
                                    setText(item == null ? "N/D" : item.equals("J") ? "JEANS" : "MODA");
                                }
                            }
                        };
                    });
                }).build() /*Tipo*/,
                FormTableColumn.create(cln -> {
                    cln.title("Modelagem");
                    cln.width(100.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdProdutoColecao, SdProdutoColecao>, ObservableValue<SdProdutoColecao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodigo().getGrupomodelagem()));
                }).build() /*Modelagem*/,
                FormTableColumn.create(cln -> {
                    cln.title("Qtde");
                    cln.width(40.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdProdutoColecao, SdProdutoColecao>, ObservableValue<SdProdutoColecao>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<SdProdutoColecao, SdProdutoColecao>() {
                            @Override
                            protected void updateItem(SdProdutoColecao item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(FormFieldText.create(field -> {
                                        field.withoutTitle();
                                        field.alignment(Pos.CENTER);
                                        field.addStyle("xs");
                                        field.value.bindBidirectional(item.qtdeMostruarioProperty(), new NumberStringConverter());
                                    }).build());
                                }
                            }
                        };
                    });
                }).build() /*Qtde*/
        );
    });
    private final FormTableView<Represen> tblRepresentantes = FormTableView.create(Represen.class, table -> {
        table.title("Representantes");
        table.expanded();
        table.items.bind(beanRepresentantes);
        table.editable.set(true);
        table.selectColumn();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(40.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<Represen, Represen>, ObservableValue<Represen>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodRep()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Código*/,
                FormTableColumn.create(cln -> {
                    cln.title("Representante");
                    cln.width(350.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<Represen, Represen>, ObservableValue<Represen>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNome()));
                }).build() /*Representante*/,
                FormTableColumn.create(cln -> {
                    cln.title("Responsável");
                    cln.width(180.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<Represen, Represen>, ObservableValue<Represen>>) param -> new ReadOnlyObjectWrapper(param.getValue().getRespon()));
                }).build() /*Responsável*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cidade");
                    cln.width(200.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<Represen, Represen>, ObservableValue<Represen>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCep()));
                    cln.format(param -> {
                        return new TableCell<Represen, CadCep>() {
                            @Override
                            protected void updateItem(CadCep item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(item.getCidade().getNomeCid());
                                } else if (item == null && !empty) {
                                    setText("CEP INVÁLIDO");
                                }
                            }
                        };
                    });
                }).build() /*Cidade*/,
                FormTableColumn.create(cln -> {
                    cln.title("UF");
                    cln.width(40.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<Represen, Represen>, ObservableValue<Represen>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCep()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<Represen, CadCep>() {
                            @Override
                            protected void updateItem(CadCep item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(item.getCidade().getCodEst().getId().getSiglaEst());
                                }
                            }
                        };
                    });
                }).build() /*UF*/,
                FormTableColumn.create(cln -> {
                    cln.title("Região");
                    cln.width(60.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<Represen, Represen>, ObservableValue<Represen>>) param -> new ReadOnlyObjectWrapper(param.getValue().getRegiao()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Região*/
        );
    });
    private final FormTableView<SdMostrRep> tblMostruarios = FormTableView.create(SdMostrRep.class, table -> {
        table.title("Mostruários");
        table.expanded();
        table.items.bind(beanMostruarios);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Coleção");
                    cln.width(60.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMostrRep, SdMostrRep>, ObservableValue<SdMostrRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getColvenda()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Coleção*/,
                FormTableColumn.create(cln -> {
                    cln.title("Representante");
                    cln.width(450.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMostrRep, SdMostrRep>, ObservableValue<SdMostrRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodrep()));
                }).build() /*Representante*/,
                FormTableColumn.create(cln -> {
                    cln.title("Marca");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMostrRep, SdMostrRep>, ObservableValue<SdMostrRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getMarca()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Marca*/,
                FormTableColumn.create(cln -> {
                    cln.title("Tipo");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMostrRep, SdMostrRep>, ObservableValue<SdMostrRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTipo()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<SdMostrRep, String>() {
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(item.equals("J") ? "Jeans" : "Moda");
                                }
                            }
                        };
                    });
                }).build() /*Tipo*/,
                FormTableColumn.create(cln -> {
                    cln.title("Mostr.");
                    cln.width(40.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMostrRep, SdMostrRep>, ObservableValue<SdMostrRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getMostruario()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Mostr.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Itens");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMostrRep, SdMostrRep>, ObservableValue<SdMostrRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getItens().size()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Itens*/,
                FormTableColumn.create(cln -> {
                    cln.title("");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(120.0);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMostrRep, SdMostrRep>, ObservableValue<SdMostrRep>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdMostrRep, SdMostrRep>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnViewMostruario = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.SEARCH, ImageUtils.IconSize._16));
                                btn.tooltip("Visualizar itens do mostruário");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("info");
                            });
                            final Button btnEditMostruario = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EDIT, ImageUtils.IconSize._16));
                                btn.tooltip("Editar itens do mostruário");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("warning");
                            });
                            final Button btnDeleteMostruario = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                btn.tooltip("Excluir mostruário");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("danger");
                            });

                            @Override
                            protected void updateItem(SdMostrRep item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {

                                    btnViewMostruario.setOnAction(evt -> {
                                        visualizarMostruario(item);
                                    });
                                    btnEditMostruario.setOnAction(evt -> {
                                        editarMostruario(item);
                                    });
                                    btnDeleteMostruario.setOnAction(evt -> {
                                        excluirMostruario(item);
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnViewMostruario, btnEditMostruario, btnDeleteMostruario);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build()
        );
        table.indices(
                table.indice("Refaturado (Acerto)", "secundary", ""),
                table.indice("Devolvido", "info", ""),
                table.indice("Finalizado", "success", ""),
                table.indice("Em Leitura", "warning", ""),
                table.indice("Pendente", "default", "")
        );
        table.factoryRow(param -> {
            return new TableRow<SdMostrRep>() {
                @Override
                protected void updateItem(SdMostrRep item, boolean empty) {
                    super.updateItem(item, empty);
                    getStyleClass().removeAll("table-row-warning", "table-row-success", "table-row-info", "table-row-secundary");
                    if (item != null && !empty) {
                        setContextMenu(FormContextMenu.create(menu -> {
                            menu.addItem(menuItem -> {
                                menuItem.setText("Marcar como refaturado (Acerto)");
                                menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._16));
                                menuItem.getStyleClass().add("success");
                                menuItem.setOnAction(evt -> {
                                    item.setStatus("R");
                                    new FluentDao().merge(item);
                                    SysLogger.addSysDelizLog("Cadastro Mostruário", TipoAcao.CADASTRAR, item.getCodrep().getCodRep(),
                                            "Marcado mostruário [" + item.getMostruario() + "] " + " para o representante na marca " + item.getMarca() + " tipo " + item.getTipo() + " na coleção " + item.getColvenda() + " com refaturado.");
                                    MessageBox.create(message -> {
                                        message.message("Mostruário marcado como refaturado (acerto).");
                                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                                        message.position(Pos.TOP_RIGHT);
                                        message.notification();
                                    });
                                    table.refresh();
                                });
                            });
                        }));
                        getStyleClass().add(item.getStatus().equals("F")
                                ? "table-row-success"
                                : item.getStatus().equals("D")
                                ? "table-row-info"
                                : item.getStatus().equals("R")
                                ? "table-row-secundary"
                                : item.getItens().stream().mapToInt(SdMostrRepItens::getQtdelido).sum() > 0
                                ? "table-row-warning" : "");
                    }
                }
            };
        });
    });
    private final FormTableView<SdMostrRepItens> tblItensMostruario = FormTableView.create(SdMostrRepItens.class, table -> {
        table.title("Produtos");
        table.expanded();
        table.items.bind(beanItensMostruario);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Produto");
                    cln.width(350.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMostrRepItens, SdMostrRepItens>, ObservableValue<SdMostrRepItens>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo()));
                }).build() /*Produto*/,
                FormTableColumn.create(cln -> {
                    cln.title("Linha");
                    cln.width(100.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMostrRepItens, SdMostrRepItens>, ObservableValue<SdMostrRepItens>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getLinha()));

                }).build() /*Linha*/,
                FormTableColumn.create(cln -> {
                    cln.title("Família");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMostrRepItens, SdMostrRepItens>, ObservableValue<SdMostrRepItens>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getFamilia()));
                }).build() /*Família*/,
                FormTableColumn.create(cln -> {
                    cln.title("");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(50.0);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMostrRepItens, SdMostrRepItens>, ObservableValue<SdMostrRepItens>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdMostrRepItens, SdMostrRepItens>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnExcluirProduto = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                btn.tooltip("Excluir referência do mostruário");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("danger");
                                btn.disable.bind(emEndicao.not());
                            });

                            @Override
                            protected void updateItem(SdMostrRepItens item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {

                                    btnExcluirProduto.setOnAction(evt -> {
                                        excluirReferenciaMostruario(item);
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnExcluirProduto);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build()
        );
        table.indices(
                table.indice("Pendente", "default", ""),
                table.indice("Separado", "secundary", ""),
                table.indice("Lido", "success", ""),
                table.indice("Transferido", "warning", ""),
                table.indice("Retornado", "primary", ""),
                table.indice("Excluído", "danger", "")
        );
        table.factoryRow(param -> {
            return new TableRow<SdMostrRepItens>() {
                @Override
                protected void updateItem(SdMostrRepItens item, boolean empty) {
                    super.updateItem(item, empty);
                    getStyleClass().removeAll("table-row-success", "table-row-danger", "table-row-primary", "table-row-warning", "table-row-secundary");
                    if (item != null && !empty && item.getStatus().equals("E")) {
                        getStyleClass().add("table-row-success");
                    } else if (item != null && !empty && item.getStatus().equals("S")) {
                        getStyleClass().add("table-row-secundary");
                    } else if (item != null && !empty && item.getStatus().equals("T")) {
                        getStyleClass().add("table-row-warning");
                    } else if (item != null && !empty && item.getStatus().equals("X")) {
                        getStyleClass().add("table-row-danger");
                    } else if (item != null && !empty && item.getStatus().equals("D")) {
                        getStyleClass().add("table-row-primary");
                    }
                }
            };
        });
    });
    // </editor-fold>

    public MostruarioRepView() {
        super("Mostruário Representante",
                ImageUtils.getImage(ImageUtils.Icon.PRIORIZAR_PRODUTO),
                new String[]{"Cadastro", "Listagem", "Manutenção"});
        init();
    }

    private void init() {
        initCadastro();
        initListagem();
        initManutencao();
    }

    private void initCadastro() {
        tabCadastro.getChildren().add(FormBox.create(container -> {
            container.horizontal();
            container.expanded();
            container.add(FormBox.create(containerProdutos -> {
                containerProdutos.vertical();
                containerProdutos.expanded();
                containerProdutos.add(fieldColecaoVenda.build());
                containerProdutos.add(new Separator(Orientation.HORIZONTAL));
                containerProdutos.add(FormBox.create(boxFilter -> {
                    boxFilter.horizontal();
                    boxFilter.add(FormTitledPane.create(filter -> {
                        filter.filter();
                        filter.add(FormBox.create(boxFieldsFilter -> {
                            boxFieldsFilter.horizontal();
                            boxFieldsFilter.add(fieldProdutos.build());
                            boxFieldsFilter.add(fieldMarcaProduto.build());
                            boxFieldsFilter.add(fieldModelagemProduto.build());
                        }));
                        filter.find.setOnAction(evt -> {
                            findProdutosColecao();
                        });
                        filter.clean.setOnAction(evt -> {
                            fieldMarcaProduto.clear();
                            fieldModelagemProduto.clear();
                            fieldProdutos.clear();
                            beanProdutos.clear();
                        });
                    }));
                }));
                containerProdutos.add(tblProdutos.build());
            }));
            container.add(FormBox.create(containerRepresentantes -> {
                containerRepresentantes.vertical();
                containerRepresentantes.expanded();
                containerRepresentantes.add(FormBox.create(boxFilter -> {
                    boxFilter.horizontal();
                    boxFilter.add(FormTitledPane.create(filter -> {
                        filter.filter();
                        filter.add(FormBox.create(boxFieldsFilter -> {
                            boxFieldsFilter.vertical();
                            boxFieldsFilter.add(FormBox.create(boxFields -> {
                                boxFields.horizontal();
                                boxFields.add(fieldRepresentante.build(), fieldCidadeRepresentante.build());
                            }));
                            boxFieldsFilter.add(FormBox.create(boxFields -> {
                                boxFields.horizontal();
                                boxFields.add(fieldEstadoRepresentante.build(), fieldAtivoRepresentante.build());
                            }));
                        }));
                        filter.find.setOnAction(evt -> {
                            findRepresentantes();
                        });
                        filter.clean.setOnAction(evt -> {
                            fieldRepresentante.clear();
                            fieldCidadeRepresentante.clear();
                            fieldEstadoRepresentante.clear();
                            fieldAtivoRepresentante.select(1);
                            beanRepresentantes.clear();
                        });
                    }));
                }));
                containerRepresentantes.add(tblRepresentantes.build());
                containerRepresentantes.add(FormBox.create(boxButtons -> {
                    boxButtons.horizontal();
                    boxButtons.add(FormBox.create(boxToolbar -> {
                        boxToolbar.horizontal();
                        boxToolbar.expanded();
                        boxToolbar.alignment(Pos.TOP_LEFT);
                        boxToolbar.add(FormButton.create(btn -> {
                            btn.title("Selecionar todos sem mostruário");
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.TODOS, ImageUtils.IconSize._16));
                            btn.addStyle("info").addStyle("xs");
                            btn.setAction(evt -> {
                                representantes.forEach(represen -> represen.setSelected(false));
                                Object[] repsComMostruario = getRepsComMostruario(fieldColecaoVenda.value.get().getCodigo(),
                                        produtos.stream().map(prd -> prd.getId().getCodigo().getCodMarca())
                                                .distinct().max((o1, o2) -> ((String) o1).compareTo((String) o2)).get());

                                representantes.forEach(represen -> {
                                    if (!Arrays.asList(repsComMostruario).contains(represen.getCodRep()))
                                        represen.setSelected(true);
                                });
                                tblRepresentantes.refresh();
                            });
                        }));
                    }));
                    boxButtons.add(FormButton.create(btnTransferirPecas -> {
                        btnTransferirPecas.title("Transferir Peças");
                        btnTransferirPecas.icon(ImageUtils.getIcon(ImageUtils.Icon.TROCAR, ImageUtils.IconSize._32));
                        btnTransferirPecas.addStyle("lg").addStyle("warning");
                        btnTransferirPecas.setAction(evt -> transferirMostruario());
                    }));
                    boxButtons.add(FormButton.create(btnSalvarMostruario -> {
                        btnSalvarMostruario.title("Salvar Mostruário");
                        btnSalvarMostruario.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._32));
                        btnSalvarMostruario.addStyle("lg").addStyle("success");
                        btnSalvarMostruario.setAction(evt -> salvarMostruario());
                    }));
                }));
            }));
        }));
    }

    private void findRepresentantes() {
        if (fieldColecaoVenda.value.get().getCodigo() == null) {
            MessageBox.create(message -> {
                message.message("Antes de carregar os representantes, é necessário que identifique a coleção de venda e os produtos.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }
        if (beanProdutos.stream().filter(SdProdutoColecao::isSelected).collect(Collectors.toList()).size() == 0) {
            MessageBox.create(message -> {
                message.message("Selecione os produtos antes de buscar os representantes.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }

        // criação dinâmica das colunas de marcas
        if (tblRepresentantes.tableProperties().getColumns().size() > 7)
            tblRepresentantes.tableProperties().getColumns().remove(7, tblRepresentantes.tableProperties().getColumns().size());
        beanProdutos.stream().filter(SdProdutoColecao::isSelected).map(prd -> prd.getId().getCodigo().getCodMarca()).distinct()
                .forEach(marca -> {
                    Marca marcaDb = new FluentDao().selectFrom(Marca.class).where(eb -> eb.equal("codigo", marca)).singleResult();
                    tblRepresentantes.addColumn(FormTableColumn.create(cln -> {
                        cln.title(marcaDb.getSdAbreviacao());
                        cln.width(50.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<Represen, Represen>, ObservableValue<Represen>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                        cln.editable.set(true);
                        cln.format(param -> {
                            return new TableCell<Represen, Represen>() {
                                @Override
                                protected void updateItem(Represen item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    setGraphic(null);
                                    if (item != null && !empty) {
                                        setGraphic(FormFieldText.create(field -> {
                                            field.withoutTitle();
                                            field.addStyle("xs");
                                            field.alignment(Pos.CENTER);
                                            AtomicReference<IntegerProperty> qtdeMost = new AtomicReference<>(new SimpleIntegerProperty(0));
                                            item.getMarcas().stream()
                                                    .filter(marc -> marc.getId().getMarca().getCodigo().equals(marca))
                                                    .findAny()
                                                    .ifPresent(marcMost -> qtdeMost.set(marcMost.qtdeMostruarioProperty()));
                                            if (item.getMarcas().stream().noneMatch(marc -> marc.getId().getMarca().getCodigo().equals(marca))) {
                                                qtdeMost.get().set(0);
                                                field.editable.set(false);
                                            }
                                            field.value.bindBidirectional(qtdeMost.get(), new NumberStringConverter());
                                        }).build());
                                    }
                                }
                            };
                        });
                        ;
                    }).build());
                });

        JPAUtils.clearEntitys(beanRepresentantes);
        beanRepresentantes.clear();
        new RunAsyncWithOverlay(this).exec(task -> {
            findRepresentantes(fieldColecaoVenda.value.get().getCodigo(),
                    beanProdutos.stream().filter(SdProdutoColecao::isSelected).map(prd -> prd.getId().getCodigo().getCodMarca()).distinct().toArray(),
                    fieldRepresentante.objectValues.get().stream().map(Represen::getCodRep).toArray(),
                    fieldCidadeRepresentante.objectValues.get().stream().map(Cidade::getCodCid).toArray(),
                    fieldEstadoRepresentante.objectValues.get().stream().map(uf -> uf.getId().getSiglaEst()).toArray(),
                    fieldAtivoRepresentante.value.get());
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                representantes.forEach(Represen::unselect);
                beanRepresentantes.set(FXCollections.observableList(representantes));
            }
        });
    }

    private void findProdutosColecao() {
        if (fieldColecaoVenda.value.get().getCodigo() == null) {
            MessageBox.create(message -> {
                message.message("Você precisa informar a coleção de venda para o mostruário para realizar a consulta");
                message.type(MessageBox.TypeMessageBox.IMPORTANT);
                message.showAndWait();
            });
            return;
        }

        JPAUtils.clearEntitys(beanRepresentantes);
        beanRepresentantes.clear();
        beanProdutos.forEach(SdProdutoColecao::unselect);
        JPAUtils.clearEntitys(beanProdutos);
        beanProdutos.clear();
        new RunAsyncWithOverlay(this).exec(task -> {
            findProdutosColecao(fieldColecaoVenda.value.get().getCodigo(),
                    fieldMarcaProduto.objectValues.get().stream().map(Marca::getCodigo).toArray(),
                    fieldProdutos.objectValues.get().stream().map(Produto::getCodigo).toArray(),
                    fieldModelagemProduto.objectValues.get().stream().map(EtqProd::getGrupo).distinct().toArray());
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                beanProdutos.set(FXCollections.observableList(produtos));
            }
        });
    }

    private void salvarMostruario() {
        if (fieldColecaoVenda.value.get().getCodigo() == null) {
            MessageBox.create(message -> {
                message.message("Você precisa definir a coleção de vendas para o mostruário.");
                message.type(MessageBox.TypeMessageBox.IMPORTANT);
                message.showAndWait();
            });
            return;
        }
        if (beanProdutos.get().stream().noneMatch(SdProdutoColecao::isSelected)) {
            MessageBox.create(message -> {
                message.message("É necessário selecionar ao menos um produto para criação do mostruário.");
                message.type(MessageBox.TypeMessageBox.IMPORTANT);
                message.showAndWait();
            });
            return;
        }
        if (beanProdutos.get().stream().noneMatch(reps -> reps.isSelected())) {
            MessageBox.create(message -> {
                message.message("É necessário selecionar ao menos um representante para criação do mostruário.");
                message.type(MessageBox.TypeMessageBox.IMPORTANT);
                message.showAndWait();
            });
            return;
        }

        AtomicReference<SQLException> exc = new AtomicReference<>();
        new RunAsyncWithOverlay(this).exec(task -> {
            Colecao colecaoVenda = fieldColecaoVenda.value.get();
            beanProdutos.stream()
                    .filter(prod -> prod.isSelected())
                    .map(prod -> prod.getId().getCodigo().getCodMarca())
                    .distinct()
                    .forEachOrdered(marca -> {
                        beanRepresentantes.stream().filter(Represen::isSelected)
                                .filter(repr -> repr.getMarcas().stream()
                                        .anyMatch(marcRep -> marcRep.isAtivo() && marcRep.getId().getMarca().getCodigo().equals(marca))
                                        && repr.getMarcas().stream()
                                        .anyMatch(marcRep -> marcRep.getColecoes().stream()
                                                .anyMatch(colec -> colec.getId().getColecao().getCodigo().equals(colecaoVenda.getCodigo()))))
                                .forEach(representante -> {
                                    for (int caixa = 1; caixa <= representante.getMarcas().stream()
                                            .filter(marc -> marc.getId().getMarca().getCodigo().equals(marca))
                                            .findAny()
                                            .get().getQtdeMostruario(); caixa++) {
                                        int finalCaixa = caixa;
                                        beanProdutos.stream()
                                                .filter(prod -> prod.isSelected() && prod.getId().getCodigo().getCodMarca().equals(marca))
                                                .map(prod -> prod.getId().getCodigo().getTipo())
                                                .distinct()
                                                .forEach(tipo -> {
                                                    Integer ultimaCaixa = (Integer) new FluentDao().selectFrom(SdMostrRep.class)
                                                            .max("mostruario")
                                                            .where(eb -> eb
                                                                    .equal("colvenda", colecaoVenda.getCodigo())
                                                                    .equal("marca", marca)
                                                                    .equal("tipo", tipo))
                                                            .singleResult();
                                                    Integer proximaCaixa = (ultimaCaixa == null ? 0 : ultimaCaixa) + 1;
                                                    SdMostrRep mostruario = new SdMostrRep();
                                                    mostruario.setMostruario(proximaCaixa);
                                                    mostruario.setTipo(tipo);
                                                    mostruario.setMarca(marca);
                                                    mostruario.setColvenda(colecaoVenda.getCodigo());
                                                    mostruario.setCodrep(representante);
                                                    beanProdutos.stream()
                                                            .filter(prod -> prod.isSelected()
                                                                    && prod.getId().getCodigo().getCodMarca().equals(marca)
                                                                    && prod.getId().getCodigo().getTipo().equals(tipo))
                                                            .forEach(prod -> {
                                                                List<SdTransfProdMostruario> produtoTransferido = (List<SdTransfProdMostruario>) new FluentDao().selectFrom(SdTransfProdMostruario.class)
                                                                        .where(eb -> eb
                                                                                .equal("status", "P")
                                                                                .equal("id.representante", representante.getCodRep())
                                                                                .equal("id.colecao", colecaoVenda.getCodigo())
                                                                                .equal("id.produto", prod.getId().getCodigo().getCodigo())
                                                                        ).resultList();
                                                                SdMostrRepItens itemMostruario = new SdMostrRepItens(mostruario,
                                                                        prod.getId().getCodigo(),
                                                                        prod.getQtdeMostruario());

                                                                if (produtoTransferido.size() > 0) {
                                                                    produtoTransferido.forEach( prdTransf -> {
                                                                        prdTransf.setStatus("T");
                                                                        new FluentDao().merge(prdTransf);
                                                                    });
                                                                    itemMostruario.setQtde(produtoTransferido.size());
                                                                    itemMostruario.setStatus("T");
                                                                }

                                                                mostruario.getItens().add(itemMostruario);
                                                            });
                                                    try {
                                                        saveMostruarioRepresentante(mostruario);
                                                        SysLogger.addSysDelizLog("Cadastro Mostruário", TipoAcao.CADASTRAR, representante.getCodRep(), "Cadastra mostruário [" + proximaCaixa + "] " +
                                                                "para o representante na marca " + marca + " tipo " + tipo + " na coleção " + colecaoVenda.getCodigo());
                                                    } catch (SQLException e) {
                                                        e.printStackTrace();
                                                        exc.set(e);
                                                        //return ReturnAsync.EXCEPTION.value;
                                                    }
                                                });
                                    }
                                });
                    });
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                beanRepresentantes.forEach(rep -> rep.setSelected(false));
                tblRepresentantes.refresh();
                MessageBox.create(message -> {
                    message.message("Mostruários salvos para os representantes selecionados.");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            }
        });
    }

    private void transferirMostruario() {
//        InputBox<Colecao> inputColecaoDestino = InputBox.build(Colecao.class, boxInput -> {
//            boxInput.message("Selecione a coleção de destino para as peças:");
//            boxInput.showAndWait();
//        });
//        if (inputColecaoDestino.value.get() == null)
//            return;

        if (!((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Deseja realmente transferir as peças para a próxima coleção?");
            message.showAndWait();
        }).value.get())) {
            return;
        }

        if (fieldColecaoVenda.value.get().getCodigo() == null) {
            MessageBox.create(message -> {
                message.message("Você precisa definir a coleção de vendas para o mostruário.");
                message.type(MessageBox.TypeMessageBox.IMPORTANT);
                message.showAndWait();
            });
            return;
        }
        if (beanProdutos.get().stream().noneMatch(SdProdutoColecao::isSelected)) {
            MessageBox.create(message -> {
                message.message("É necessário selecionar ao menos um produto para criação do mostruário.");
                message.type(MessageBox.TypeMessageBox.IMPORTANT);
                message.showAndWait();
            });
            return;
        }
        if (beanRepresentantes.get().stream().noneMatch(reps -> reps.isSelected())) {
            MessageBox.create(message -> {
                message.message("É necessário selecionar ao menos um representante para criação do mostruário.");
                message.type(MessageBox.TypeMessageBox.IMPORTANT);
                message.showAndWait();
            });
            return;
        }

        AtomicReference<Exception> except = new AtomicReference<>();
        AtomicReference<String> mensagensTransferencias = new AtomicReference<>("");
        AtomicReference<String> transferenciasMostruario = new AtomicReference<>("");
        new RunAsyncWithOverlay(this).exec(task -> {
            Object[] marcas = beanProdutos.get().stream().filter(SdProdutoColecao::isSelected)
                    .map(prod -> prod.getId().getCodigo().getCodMarca()).distinct().toArray();

            // get da próxima coleção onde a coleção atual é a anterior
            List<SdColecaoMarca001> colecaoMarcas = getProximaColecaoMarcas(marcas, fieldColecaoVenda.value.get().getCodigo());

            beanRepresentantes.stream().filter(Represen::isSelected).forEach(represen -> {
                try {
                    // verificando se o representante te uma NF devolvida para a coleção de venda carregada
                    List<VSdMostRetornoLiberado> mostRetornoLiberados = getMostRetornoLiberados(represen.getCodRep(), fieldColecaoVenda.value.get().getCodigo());
                    if (mostRetornoLiberados.size() == 0) {
                        mensagensTransferencias.set(mensagensTransferencias.get().concat(
                                "Não existem mostruários liberados para retorno no representante " + represen.getCodRep() + " na coleção " + fieldColecaoVenda.value.get().getCodigo() + System.lineSeparator()));
                        throw new ContinueException();
                    }
                    beanProdutos.stream().filter(SdProdutoColecao::isSelected).forEach(sdProdutoColecao -> {
                        // try para continue do for caso o rep não tenha o produto
                        try {
                            // verificar se o rep tem esse produto na col atual
                            List<SdMostrRepItens> produtosMostruario = produtosMostruarioRepColecao(sdProdutoColecao.getId().getCodigo().getCodigo(),
                                    represen.getCodRep(),
                                    fieldColecaoVenda.value.get().getCodigo(),
                                    mostRetornoLiberados.stream().map(mostLib -> mostLib.getId().getMostruario()).distinct().toArray());
                            if (produtosMostruario.size() == 0) {
                                mensagensTransferencias.set(mensagensTransferencias.get().concat(
                                        "O produto " + sdProdutoColecao.getId().getCodigo().getCodigo() + " não está liberado para devolução no representantante " + represen.getCodRep() + System.lineSeparator()));

                                throw new ContinueException();
                            }

                            produtosMostruario.forEach(produto -> {
                                for (int i = 0; i < produto.getQtdelido(); i++) {
                                    try {
                                        // remover o produto da col atual utilizando a rotina da tela de retorno de mostruário
                                        // utilizar a estrutura de retorno de uma peça sem tag
                                        // get da barra lida no envio da peça no mostruário atual
                                        SdItensCaixaRemessa itemCaixa = new FluentDao().selectFrom(SdItensCaixaRemessa.class)
                                                .where(eb -> eb
                                                        .equal("id.caixa.remessa.remessa", produto.getMostruario().getRemessa())
                                                        .equal("codigo.codigo", produto.getCodigo().getCodigo())
                                                        .equal("pedido", produto.getMostruario().getNumero())
                                                        .equal("status", "E"))
                                                .findFirst();
                                        if (itemCaixa == null) {
                                            mensagensTransferencias.set(mensagensTransferencias.get().concat(
                                                    "Não encontrado uma barra expedida para o produto " + produto.getCodigo().getCodigo() + " para o representante " + represen.getCodRep() +
                                                            " no mostruário " + (produtosMostruario.indexOf(produto) + 1) + System.lineSeparator()));
                                            throw new ContinueException();
                                        }

                                        // get do item para devolução
                                        SdItensCaixaRemessa finalItemCaixa = itemCaixa;
                                        VSdDevolucaoMostRep itemDevolucao = new FluentDao().selectFrom(VSdDevolucaoMostRep.class)
                                                .where(eb -> eb
                                                        .equal("colvenda", fieldColecaoVenda.value.get().getCodigo())
                                                        .equal("codrep", represen.getCodRep())
                                                        .equal("codigo.codigo", finalItemCaixa.getCodigo().getCodigo())
                                                        .equal("cor.cor", finalItemCaixa.getCor())
                                                        .equal("tam", finalItemCaixa.getTam()))
                                                .singleResult();
                                        if (itemDevolucao == null) {
                                            mensagensTransferencias.set(mensagensTransferencias.get().concat(
                                                    "Não encontrado item de devolução do produto " + produto.getCodigo().getCodigo() + " para o representante " + represen.getCodRep() +
                                                            " no mostruário " + (produtosMostruario.indexOf(produto) + 1) + System.lineSeparator()));
                                            throw new ContinueException();
                                        }

                                        try {
                                            // devolvendo barra enviada para liberar para a próxima coleção
                                            RetornoMostruarioController controllerRetorno = new RetornoMostruarioController();
                                            // devolvendo barra para leitura
                                            itemCaixa = controllerRetorno.devolverBarraParaEstoque(itemDevolucao, itemCaixa);
                                            // retornando peça no pedido
                                            controllerRetorno.retornaPecaPedido(itemDevolucao, itemCaixa);
                                            // marcando peca como devolvida no mostruario
                                            controllerRetorno.devolverItemMostruario(itemDevolucao, itemCaixa);
                                            // guardando barra lida e salvando log
                                            controllerRetorno.guardarBarraRetorno(itemCaixa.getId().getBarra(), itemDevolucao);
                                            // verificação de mostruário todo devolvido
                                            controllerRetorno.verificaMostruariosDevolvidos(itemDevolucao);

                                            // guardar a peça em um mostruário temporário para adicionar o oficial para faturar junto
                                            String codigoProximaColecao = colecaoMarcas.stream().filter(colMarca -> colMarca.getId().getMarca().getCodigo().equals(produto.getCodigo().getCodMarca())).findFirst().get().getId().getColecao().getCodigo();
                                            SdTransfProdMostruario produtoTransferencia = new SdTransfProdMostruario(codigoProximaColecao,
                                                    represen.getCodRep(),
                                                    produto.getCodigo().getCodigo(),
                                                    produtosMostruario.indexOf(produto) + 1,
                                                    produto.getQtde(),
                                                    "P",
                                                    itemCaixa.getId().getBarra());
                                            guardarPecasTransferencia(produtoTransferencia);
                                            transferenciasMostruario.set(transferenciasMostruario.get().concat(
                                                    "Transferido codigo " + produtoTransferencia.getId().getProduto() +
                                                            " da coleção " + fieldColecaoVenda.value.get().getCodigo() +
                                                            " para " + produtoTransferencia.getId().getColecao() +
                                                            " no representante " + produtoTransferencia.getId().getRepresentante() + " mostruario " + produtoTransferencia.getId().getMostruario() + System.lineSeparator()));
                                            SysLogger.addSysDelizLog("Cadastro Mostruário", TipoAcao.MOVIMENTAR, produtoTransferencia.getId().getRepresentante(), "Transferido codigo " + produtoTransferencia.getId().getProduto() +
                                                    " da coleção " + fieldColecaoVenda.value.get().getCodigo() +
                                                    " para " + produtoTransferencia.getId().getColecao() +
                                                    " no representante " + produtoTransferencia.getId().getRepresentante() + " mostruario " + produtoTransferencia.getId().getMostruario() + System.lineSeparator());
                                        } catch (SQLException e) {
                                            mensagensTransferencias.set(mensagensTransferencias.get().concat(
                                                    "Produto " + produto.getCodigo().getCodigo() + " para o representante " + represen.getCodRep() + " no mostruário " + produtosMostruario.indexOf(produto) + " não pode ser salvo para transferência. " +
                                                            "Erro: " + e.getMessage() + System.lineSeparator()
                                            ));
                                        }
                                    } catch (ContinueException keep) {
                                    }
                                }
                            });

                        } catch (ContinueException keep) {
                        }
                    });
                    // verificação de mostruário todo devolvido
                } catch (ContinueException keep) {
                }
            });
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                new Fragment().show(fragment -> {
                    fragment.title("Transferência de Referências");
                    fragment.size(550.0, 650.0);
                    fragment.box.getChildren().add(FormFieldTextArea.create(field -> {
                        field.withoutTitle();
                        field.expanded();
                        field.editable.set(false);
                        field.value.set(transferenciasMostruario.get());
                    }).build());
                    fragment.box.getChildren().add(FormFieldTextArea.create(field -> {
                        field.title("Referências Não Transferidas");
                        field.expanded();
                        field.editable.set(false);
                        field.value.set(mensagensTransferencias.get());
                    }).build());
                });

                beanRepresentantes.forEach(rep -> rep.setSelected(false));
                tblRepresentantes.refresh();
            }
        });
    }

    private void initListagem() {
        tabListagem.getChildren().add(FormBox.create(container -> {
            container.vertical();
            container.expanded();
            container.add(FormBox.create(boxFilter -> {
                boxFilter.horizontal();
                boxFilter.add(FormTitledPane.create(filter -> {
                    filter.filter();
                    filter.add(FormBox.create(boxFieldsFilter -> {
                        boxFieldsFilter.vertical();
                        boxFieldsFilter.expanded();
                        boxFieldsFilter.add(FormBox.create(boxFields -> {
                            boxFields.horizontal();
                            boxFields.add(fieldColecaoListagem.build(), fieldRepresentanteListagem.build());
                        }));
                        boxFieldsFilter.add(FormBox.create(boxFields -> {
                            boxFields.horizontal();
                            boxFields.add(fieldMarcaListagem.build(), fieldTipoListagem.build());
                        }));
                    }));
                    filter.find.setOnAction(evt -> {
                        findMostruarios();
                    });
                    filter.clean.setOnAction(evt -> {
                        fieldColecaoListagem.clear();
                        fieldRepresentante.clear();
                        fieldMarcaListagem.clear();
                        fieldTipoListagem.select(0);
                    });
                }));
            }));
            container.add(tblMostruarios.build());
        }));
    }

    private void findMostruarios() {
        JPAUtils.clearEntitys(beanMostruarios);
        new RunAsyncWithOverlay(this).exec(task -> {
            findMostruarios(fieldColecaoListagem.objectValues.stream().map(Colecao::getCodigo).toArray(),
                    fieldRepresentanteListagem.objectValues.stream().map(Represen::getCodRep).toArray(),
                    fieldMarcaListagem.objectValues.stream().map(Marca::getCodigo).toArray(),
                    fieldTipoListagem.value.get());
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                beanMostruarios.set(FXCollections.observableList(mostruarios));
                tblMostruarios.refresh();
            }
        });
    }

    private void excluirMostruario(SdMostrRep mostruario) {
        if (mostruario.getRemessa() != null) {
            MessageBox.create(message -> {
                message.message("Você não pode excluir esse mostruário pois o mesmo já se encontra em leitura.");
                message.type(MessageBox.TypeMessageBox.BLOCKED);
                message.showAndWait();
            });
            return;
        }
        deleteMostruario(mostruario);
        SysLogger.addSysDelizLog("Cadastro Mostruário", TipoAcao.CADASTRAR, mostruario.getCodrep().getCodRep(), "Excluído mostruário [" + mostruario.getMostruario() + "] para o representante na coleção " + mostruario.getColvenda());
        beanMostruarios.remove(mostruario);
        tblMostruarios.refresh();
        MessageBox.create(message -> {
            message.message("Mostruário selecionado excluído com sucesso");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }

    private void editarMostruario(SdMostrRep mostruario) {
        if (mostruario.getRemessa() != null) {
            MessageBox.create(message -> {
                message.message("Este mostruário já se encontra em leitura, informe a expedição da alteração de peças do mostruário.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
        }
        tabs.getSelectionModel().select(2);
        abrirDadosMostruario(mostruario);
        emEndicao.set(true);
    }

    private void visualizarMostruario(SdMostrRep mostruario) {
        tabs.getSelectionModel().select(2);
        abrirDadosMostruario(mostruario);
    }

    private void abrirDadosMostruario(SdMostrRep mostruario) {
        emEndicao.set(false);
        mostruario.getItens().sort((o1, o2) -> o1.getCodigo().getCodigo().compareTo(o2.getCodigo().getCodigo()));
        beanItensMostruario.set(FXCollections.observableList(mostruario.getItens()));
        tblItensMostruario.refresh();
        tblMostruarios.refresh();

        mostruarioSelecionado.set(mostruario);

        fieldColecaoManutencao.setValue(mostruario.getColvenda());
        fieldMarcaManutencao.setValue(mostruario.getMarca());
        fieldMostruarioManutencao.setValue(String.valueOf(mostruario.getMostruario()));
        fieldNumeroManutencao.setValue(mostruario.getNumero());
        fieldRepresentanteManutencao.setValue(mostruario.getCodrep().toString());
        fieldTipoManutencao.setValue(mostruario.getTipo().equals("J") ? "Jeans" : "Moda");
        fieldStatusManutencao.setValue(mostruario.getStatus().equals("F")
                ? "Finalizado"
                : mostruario.getItens().stream().mapToInt(SdMostrRepItens::getQtdelido).sum() > 0
                ? "Em Leitura" : "Pendente");
        fieldStatusManutencao.addStyle(mostruario.getStatus().equals("F")
                ? "success"
                : mostruario.getItens().stream().mapToInt(SdMostrRepItens::getQtdelido).sum() > 0
                ? "warning" : "default");

        Map<String, Long> contagemStatus = mostruario.getItens().stream().collect(Collectors.groupingBy(SdMostrRepItens::getStatus, Collectors.counting()));
        fieldDistribuicaoPecas.clear();
        contagemStatus.forEach((status, qtde) -> {
            switch (status) {
                case "P":
                    fieldDistribuicaoPecas.addItem("Pendente", qtde, FormFieldSegmentedBar.Type.DEFAULT);
                    break;
                case "S":
                    fieldDistribuicaoPecas.addItem("Separado", qtde, FormFieldSegmentedBar.Type.SECUNDARY);
                    break;
                case "E":
                    fieldDistribuicaoPecas.addItem("Em Caixa", qtde, FormFieldSegmentedBar.Type.SUCCESS);
                    break;
                case "X":
                    fieldDistribuicaoPecas.addItem("Cancelado", qtde, FormFieldSegmentedBar.Type.DANGER);
                    break;
                case "D":
                    fieldDistribuicaoPecas.addItem("Devolvido", qtde, FormFieldSegmentedBar.Type.INFO);
                    break;
                case "T":
                    fieldDistribuicaoPecas.addItem("Transferido", qtde, FormFieldSegmentedBar.Type.WARNING);
                    break;
            }
        });
    }

    private void initManutencao() {
        tabManutencao.getChildren().add(FormBox.create(container -> {
            container.horizontal();
            container.expanded();
            container.add(FormBox.create(boxDadosMostruario -> {
                boxDadosMostruario.vertical();
                boxDadosMostruario.add(FormBox.create(boxFields -> {
                    boxFields.horizontal();
                    boxFields.add(fieldColecaoManutencao.build(), fieldStatusManutencao.build());
                }));
                boxDadosMostruario.add(FormBox.create(boxFields -> {
                    boxFields.horizontal();
                    boxFields.add(fieldRepresentanteManutencao.build());
                }));
                boxDadosMostruario.add(FormBox.create(boxFields -> {
                    boxFields.horizontal();
                    boxFields.add(fieldMarcaManutencao.build(), fieldTipoManutencao.build(), fieldMostruarioManutencao.build());
                }));
                boxDadosMostruario.add(FormBox.create(boxFields -> {
                    boxFields.horizontal();
                    boxFields.add(fieldNumeroManutencao.build());
                }));
            }));
            container.add(FormBox.create(boxDadosProduto -> {
                boxDadosProduto.vertical();
                boxDadosProduto.add(fieldDistribuicaoPecas.build());
                boxDadosProduto.add(tblItensMostruario.build());
                boxDadosProduto.add(toolbarItens -> {
                    toolbarItens.horizontal();
                    toolbarItens.add(FormButton.create(btn -> {
                        btn.title("Cancelar Pendentes");
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                        btn.addStyle("xs").addStyle("danger");
                        btn.disable.bind(emEndicao.not());
                        btn.setAction(evt -> {
                            if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                                    message.message("Deseja realmente cancelar os itens pendentes?");
                                    message.showAndWait();
                                }).value.get())) {
                                beanItensMostruario.stream().filter(item -> item.getStatus().equals("P") || item.getStatus().equals("S"))
                                        .forEach(item -> {
                                            item.setStatus("X");
                                            new FluentDao().merge(item);
                                        });
                                SysLogger.addSysDelizLog("Cadastro Mostruário", TipoAcao.EDITAR, mostruarioSelecionado.get().getCodrep().getCodRep(),
                                        "Cancelado itens pendentes do mostruário " + mostruarioSelecionado.get().getMostruario() +
                                                " da marca " + mostruarioSelecionado.get().getMarca() + " tipo " + mostruarioSelecionado.get().getTipo() +
                                                " na coleção " + mostruarioSelecionado.get().getColvenda());
                            }
                        });
                    }));
                });
                boxDadosProduto.add(FormTitledPane.create(tpane -> {
                    tpane.title("Incluir Referência");
                    tpane.addStyle("info");
                    tpane.collapse(emEndicao.get());
                    tpane.disabled.bind(emEndicao.not());
                    tpane.add(FormBox.create(boxFields -> {
                        boxFields.horizontal();
                        boxFields.alignment(Pos.BOTTOM_LEFT);
                        boxFields.add(fieldProdutoManutencao.build());
                        boxFields.add(FormButton.create(btnAdicionar -> {
                            btnAdicionar.title("Adicionar");
                            btnAdicionar.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._24));
                            btnAdicionar.addStyle("success");
                            btnAdicionar.disable.bind(emEndicao.not());
                            btnAdicionar.setAction(evt -> {
                                List<SdMostrRepItens> novosProdutos = new ArrayList<>();
                                final SdMostrRep mostruario = mostruarioSelecionado.get();

                                fieldProdutoManutencao.objectValues.forEach(produto -> {
                                    if (produto.getCodMarca().equals(mostruario.getMarca())
                                            && produto.getTipo().equals(mostruario.getTipo())) {
                                        novosProdutos.add(new SdMostrRepItens(mostruario, produto, 1));
                                    }
                                });
                                try {
                                    atualizarProdutosMostruario(novosProdutos);
                                    SysLogger.addSysDelizLog("Cadastro Mostruário", TipoAcao.CADASTRAR, mostruario.getCodrep().getCodRep(),
                                            "Inclído produto " + novosProdutos.stream().map(produto -> produto.getCodigo().getCodigo()).distinct().collect(Collectors.joining(",")) +
                                                    " no mostruário [" + mostruario.getMostruario() + "] para o representante na coleção " + mostruario.getColvenda());
                                    JPAUtils.refresh(mostruario);
                                    abrirDadosMostruario(mostruario);
                                    emEndicao.set(true);
                                    fieldProdutoManutencao.clear();
                                    MessageBox.create(message -> {
                                        message.message(novosProdutos.size() + " produto(s) adicionado(s) no mostruário");
                                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                                        message.position(Pos.TOP_RIGHT);
                                        message.notification();
                                    });
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    ExceptionBox.build(message -> {
                                        message.exception(e);
                                        message.showAndWait();
                                    });
                                }
                            });
                        }));
                    }));
                }));
            }));
        }));
    }

    private void excluirReferenciaMostruario(SdMostrRepItens produto) {
        try {
            if (produto.getStatus().equals("E")) {
                MessageBox.create(message -> {
                    message.message("Não é permitido a exclusão deste produto pois o mesmo já se encontra lido pela expedição.");
                    message.type(MessageBox.TypeMessageBox.BLOCKED);
                    message.showAndWait();
                });
                return;
            }
            produto.setStatus("X");
            deleteReferenciaMostruario(produto);
            SysLogger.addSysDelizLog("Cadastro Mostruário", TipoAcao.EXCLUIR, produto.getMostruario().getCodrep().getCodRep(),
                    "Excluído produto " + produto.getCodigo().toString() + " do mostruário [" + produto.getMostruario().getMostruario() + "] para o representante na coleção " + produto.getMostruario().getColvenda());
            final SdMostrRep mostruario = mostruarioSelecionado.get();
            abrirDadosMostruario(mostruario);
            emEndicao.set(true);
            MessageBox.create(message -> {
                message.message("Produto removido do mostruário");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
        } catch (SQLException e) {
            e.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(e);
                message.showAndWait();
            });
        }
    }

}
