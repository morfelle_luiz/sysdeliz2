package sysdeliz2.views.compras;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import sysdeliz2.controllers.views.compras.GestaoFaltaMateriaisController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.ti.*;
import sysdeliz2.models.view.VSdComprasMaterial;
import sysdeliz2.models.view.VSdEstoqSugestaoSubstituto;
import sysdeliz2.models.view.VSdEstoqueMatCorBr;
import sysdeliz2.models.view.VSdGestaoMatFaltante;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public class GestaoFaltaMateriaisView extends GestaoFaltaMateriaisController {
    
    private final ListProperty<VSdGestaoMatFaltante> materiaisFaltantesObservable = new SimpleListProperty<>();
    // <editor-fold defaultstate="collapsed" desc="Declaração: Contadores">
    private final DoubleProperty totalFalta = new SimpleDoubleProperty(0);
    private final DoubleProperty totalAberto = new SimpleDoubleProperty(0);
    private final DoubleProperty totalCompra = new SimpleDoubleProperty(0);
    private final DoubleProperty totalResolvido = new SimpleDoubleProperty(0);
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Table Materiais Faltantes">
    private final FormTableView<VSdGestaoMatFaltante> tblFaltantes = FormTableView.create(VSdGestaoMatFaltante.class, table -> {
        table.title("Materiais Faltantes");
        table.expanded();
        table.items.bind(materiaisFaltantesObservable);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("D.H. Criação");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDhcriacao()));
                    cln.format(param -> {
                        return new TableCell<VSdGestaoMatFaltante, LocalDateTime>() {
                            @Override
                            protected void updateItem(LocalDateTime item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDateTimeFormat(item));
                                }
                            }
                        };
                    });
                }).build() /*D.H. Criação*/,
                FormTableColumn.create(cln -> {
                    cln.title("D.H. Análise");
                    cln.width(100);
                    cln.hide();
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDhanalise()));
                    cln.format(param -> {
                        return new TableCell<VSdGestaoMatFaltante, LocalDateTime>() {
                            @Override
                            protected void updateItem(LocalDateTime item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDateTimeFormat(item));
                                }
                            }
                        };
                    });
                }).build() /*D.H. Análise*/,
                FormTableColumn.create(cln -> {
                    cln.title("O.F.");
                    cln.width(70.0);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumero()));
                }).build() /*O.F.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Setor Atual");
                    cln.width(130.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getSetoratual()));
                }).build() /*"Desc. Setor Atual"*/,
                FormTableColumn.create(cln -> {
                    cln.title("Período");
                    cln.width(60.0);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPeriodo()));
                }).build() /*Período*/,
                FormTableColumn.create(cln -> {
                    cln.title("Ref.");
                    cln.width(60.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getCodigo()));
                }).build() /*Ref.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Produto");
                    cln.width(200.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getDescricao()));
                    cln.hide();
                }).build() /*Produto*/,
                FormTableColumn.create(cln -> {
                    cln.title("Setor");
                    cln.width(40.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getSetor().getCodigo()));
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                }).build() /*Setor*/,
                FormTableColumn.create(cln -> {
                    cln.title("Desc. Setor");
                    cln.width(120.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getSetor().getDescricao()));
                    cln.hide();
                }).build() /*Desc. Setor*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cód. Material");
                    cln.width(100.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getInsumo().getCodigo()));
                }).build() /*Cód. Material*/,
                FormTableColumn.create(cln -> {
                    cln.title("Material");
                    cln.width(330.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getInsumo().getDescricao()));
                }).build() /*Material*/,
                FormTableColumn.create(cln -> {
                    cln.title("Unid.");
                    cln.width(40.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getInsumo().getUnidade()));
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                }).build() /*U.N.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Un. Compra");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getInsumo().getUnicom()));
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.hide();
                }).build() /*Un. Compra*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cód. Cor");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCori().getCor()));
                    
                }).build() /*Cód. Cor*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cor");
                    cln.width(180.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCori().getDescricao()));
                }).build() /*Cor*/,
                FormTableColumn.create(cln -> {
                    cln.title("Subs.");
                    cln.width(40.0);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().isSubstituto()));
                    cln.format(param -> {
                        return new TableCell<VSdGestaoMatFaltante, Boolean>() {
                            @Override
                            protected void updateItem(Boolean item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(item ? ImageUtils.getIcon(ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._16) : ImageUtils.getIcon(ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                                }
                            }
                        };
                    });
                }).build() /*Subs.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Grupo");
                    cln.width(100.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getInsumo().getGrupo()));
                    cln.hide();
                }).build() /*Grupo*/,
                FormTableColumn.create(cln -> {
                    cln.title("Subgrupo");
                    cln.width(150.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getInsumo().getSubgrupo()));
                    cln.hide();
                }).build() /*Subgrupo*/,
                FormTableColumn.create(cln -> {
                    cln.title("Qtde");
                    cln.width(90.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                    cln.alignment(FormTableColumn.Alignment.RIGHT);
                    cln.format(param -> {
                        return new TableCell<VSdGestaoMatFaltante, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                                }
                            }
                        };
                    });
                }).build() /*Qtde*/,
                FormTableColumn.create(cln -> {
                    cln.title("Status");
                    cln.width(120.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.format(param -> {
                        return new TableCell<VSdGestaoMatFaltante, VSdGestaoMatFaltante>() {
                            @Override
                            protected void updateItem(VSdGestaoMatFaltante item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    table.selectItem(item);
                                    setText(item.getStatus().equals("A") ? "Aberto" : item.getStatus().equals("C") ? "Com Compra" : "Resolvido");
                                    setGraphic(ImageUtils.getIcon(item.getStatus().equals("A") ? ImageUtils.Icon.STATUS_VERMELHO : item.getStatus().equals("C") ? ImageUtils.Icon.STATUS_AMARELO : ImageUtils.Icon.STATUS_VERDE, ImageUtils.IconSize._16));
                                    if (item.getStatus().equals("C"))
                                        setGraphic(FormButton.create(btn -> {
                                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.STATUS_AMARELO, ImageUtils.IconSize._16));
                                            btn.tooltip("Visualizar Ordens de Compra");
                                            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                            btn.addStyle("xs").addStyle("info");
                                            btn.setAction(evt -> {
                                                new Fragment().show(fragment -> {
                                                    fragment.title("Visualizar Ordens de Compras");
                                                    fragment.size(500.0, 400.0);
                                                    VSdGestaoMatFaltante materialSelecionado = (VSdGestaoMatFaltante) item;
                                                    final FormTableView<VSdComprasMaterial> tblComprasMaterial = FormTableView.create(VSdComprasMaterial.class, tableCompras -> {
                                                        tableCompras.title("Ordens de Compra/Tecelagem");
                                                        tableCompras.expanded();
                                                        tableCompras.items.set(FXCollections.observableList((List<VSdComprasMaterial>) new FluentDao().selectFrom(VSdComprasMaterial.class)
                                                                .where(it -> it
                                                                        .equal("codigo", materialSelecionado.getInsumo().getCodigo())
                                                                        .equal("cor", materialSelecionado.getCori().getCor()))
                                                                .resultList()));
                                                        tableCompras.columns(
                                                                FormTableColumn.create(cln -> {
                                                                    cln.title("O.C.");
                                                                    cln.width(80.0);
                                                                    cln.value((Callback<TableColumn.CellDataFeatures<VSdComprasMaterial, VSdComprasMaterial>, ObservableValue<VSdComprasMaterial>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumero()));
                                                                    cln.alignment(FormTableColumn.Alignment.CENTER);
                                                                }).build() /*O.C.*/,
                                                                FormTableColumn.create(cln -> {
                                                                    cln.title("Depósito");
                                                                    cln.width(80.0);
                                                                    cln.value((Callback<TableColumn.CellDataFeatures<VSdComprasMaterial, VSdComprasMaterial>, ObservableValue<VSdComprasMaterial>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDeposito()));
                                                                    cln.alignment(FormTableColumn.Alignment.CENTER);
                                                                }).build() /*Depósito*/,
                                                                FormTableColumn.create(cln -> {
                                                                    cln.title("Qtde");
                                                                    cln.width(100.0);
                                                                    cln.value((Callback<TableColumn.CellDataFeatures<VSdComprasMaterial, VSdComprasMaterial>, ObservableValue<VSdComprasMaterial>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                                                                    cln.alignment(FormTableColumn.Alignment.RIGHT);
                                                                    cln.format(param -> {
                                                                        return new TableCell<VSdComprasMaterial, BigDecimal>() {
                                                                            @Override
                                                                            protected void updateItem(BigDecimal item, boolean empty) {
                                                                                super.updateItem(item, empty);
                                                                                setText(null);
                                                                                if (item != null && !empty) {
                                                                                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                                                                                }
                                                                            }
                                                                        };
                                                                    });
                                                                }).build() /*Qtde*/,
                                                                FormTableColumn.create(cln -> {
                                                                    cln.title("Faturado em");
                                                                    cln.width(90.0);
                                                                    cln.value((Callback<TableColumn.CellDataFeatures<VSdComprasMaterial, VSdComprasMaterial>, ObservableValue<VSdComprasMaterial>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDtfatura()));
                                                                    cln.alignment(FormTableColumn.Alignment.CENTER);
                                                                    cln.format(param -> {
                                                                        return new TableCell<VSdComprasMaterial, LocalDate>() {
                                                                            @Override
                                                                            protected void updateItem(LocalDate item, boolean empty) {
                                                                                super.updateItem(item, empty);
                                                                                setText(null);
                                                                                if (item != null && !empty) {
                                                                                    setText(StringUtils.toDateFormat(item));
                                                                                }
                                                                            }
                                                                        };
                                                                    });
                                                                }).build() /*Faturado em*/,
                                                                FormTableColumn.create(cln -> {
                                                                    cln.title("Ent. Prevista");
                                                                    cln.width(80.0);
                                                                    cln.value((Callback<TableColumn.CellDataFeatures<VSdComprasMaterial, VSdComprasMaterial>, ObservableValue<VSdComprasMaterial>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDtentrega()));
                                                                    cln.alignment(FormTableColumn.Alignment.CENTER);
                                                                    cln.format(param -> {
                                                                        return new TableCell<VSdComprasMaterial, LocalDate>() {
                                                                            @Override
                                                                            protected void updateItem(LocalDate item, boolean empty) {
                                                                                super.updateItem(item, empty);
                                                                                setText(null);
                                                                                if (item != null && !empty) {
                                                                                    setText(StringUtils.toDateFormat(item));
                                                                                }
                                                                            }
                                                                        };
                                                                    });
                                                                }).build() /*Ent. Prevista*/,
                                                                FormTableColumn.create(cln -> {
                                                                    cln.title("Ações");
                                                                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                                                                    cln.width(80.0);
                                                                    cln.alignment(FormTableColumn.Alignment.CENTER);
                                                                    cln.value((Callback<TableColumn.CellDataFeatures<VSdComprasMaterial, VSdComprasMaterial>, ObservableValue<VSdComprasMaterial>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                                                    cln.format(param -> {
                                                                        return new TableCell<VSdComprasMaterial, VSdComprasMaterial>() {
                                                                            final HBox boxButtonsRow = new HBox(3);
                                                                            final Button btnFaturaOc = FormButton.create(btn1 -> {
                                                                                btn1.icon(ImageUtils.getIcon(ImageUtils.Icon.FATURAR_PEDIDO, ImageUtils.IconSize._16));
                                                                                btn1.tooltip("Visualizar Pedido e Fatura");
                                                                                btn1.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                                                                btn1.addStyle("xs").addStyle("success");
                                                                            });
                                                                            final Button btnObservacaoOc = FormButton.create(btn2 -> {
                                                                                btn2.icon(ImageUtils.getIcon(ImageUtils.Icon.INFORMACAO, ImageUtils.IconSize._16));
                                                                                btn2.tooltip("Visualizar Observações");
                                                                                btn2.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                                                                btn2.addStyle("xs").addStyle("info");
                                                                            });
                                
                                                                            @Override
                                                                            protected void updateItem(VSdComprasMaterial item, boolean empty) {
                                                                                super.updateItem(item, empty);
                                                                                setText(null);
                                                                                setGraphic(null);
                                                                                if (item != null && !empty) {
                                        
                                                                                    btnFaturaOc.setOnAction(evt -> {
                                                                                        MessageBox.create(message -> {
                                                                                            message.message("Pedido: " + item.getPedfornecedor() + "\n" +
                                                                                                    "Fatura: " + item.getNffornecedor());
                                                                                            message.type(MessageBox.TypeMessageBox.INPUT);
                                                                                            message.showAndWait();
                                                                                        });
                                                                                    });
                                                                                    btnObservacaoOc.setOnAction(evt -> {
                                                                                        MessageBox.create(message -> {
                                                                                            message.message(item.getObservacao());
                                                                                            message.type(MessageBox.TypeMessageBox.INPUT);
                                                                                            message.showAndWait();
                                                                                        });
                                                                                    });
                                                                                    boxButtonsRow.getChildren().clear();
                                                                                    boxButtonsRow.getChildren().addAll(btnFaturaOc, btnObservacaoOc);
                                                                                    setGraphic(boxButtonsRow);
                                                                                }
                                                                            }
                                                                        };
                                                                    });
                                                                }).build()
                                                        );
                                                    });
                                                    fragment.box.getChildren().add(tblComprasMaterial.build());
                                                });
                                            });
                                        }));
                                }
                            }
                        };
                    });
                }).build() /*Status*/,
                FormTableColumn.create(cln -> {
                    cln.title("Prev. Ent.");
                    cln.width(85.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDtentrega()));
                    cln.format(param -> {
                        return new TableCell<VSdGestaoMatFaltante, LocalDate>(){
                            @Override
                            protected void updateItem(LocalDate item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDateFormat(item));
                                }
                            }
                        };
                    });
                }).build() /*Prev. Ent.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Fatura");
                    cln.width(85.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getFatura()));
                }).build() /*Fatura*/,
                FormTableColumn.create(cln -> {
                    cln.title("Facção");
                    cln.width(180.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getFaccao()));
                }).build() /*Facção*/,
                FormTableColumn.create(cln -> {
                    cln.title("Extras");
                    cln.width(80.0);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<VSdGestaoMatFaltante, VSdGestaoMatFaltante>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnObservacoes = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.INFORMACAO, ImageUtils.IconSize._16));
                                btn.tooltip("Ver Observações da OF");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("info");
                            });
                            final Button btnAddObservacoes = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._16));
                                btn.tooltip("Adicionar Observação Falta");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("warning");
                            });
                            
                            @Override
                            protected void updateItem(VSdGestaoMatFaltante item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    table.selectItem(item);
                                    btnObservacoes.setOnAction(evt -> {
                                        MessageBox.create(message -> {
                                            message.message(item.getObsv());
                                            message.type(MessageBox.TypeMessageBox.INPUT);
                                            message.showAndWait();
                                        });
                                    });
                                    btnAddObservacoes.setOnAction(evt -> {
                                        new Fragment().show(fragment -> {
                                            fragment.title("Adicionar Observação em Falta");
                                            fragment.size(300.0, 300.0);
                                            
                                            final TextArea fieldObservacao = new TextArea(item.getObservacao());
                                            fieldObservacao.setPrefHeight(300.0);
                                            fieldObservacao.setPrefWidth(300.0);
                                            
                                            fragment.box.getChildren().add(fieldObservacao);
                                            fragment.buttonsBox.getChildren().add(FormButton.create(btn -> {
                                                btn.title("Salvar");
                                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                                                btn.addStyle("success");
                                                btn.setAction(event -> {
                                                    new FluentDao().runNativeQueryUpdate(String.format("update sd_materiais_faltantes_001 set observacao = '%s' where numero = '%s' and insumo = '%s' and cor_i = '%s' and setor = '%s'",
                                                            fieldObservacao.getText(),
                                                            item.getNumero(),
                                                            item.getInsumo().getCodigo(),
                                                            item.getCori().getCor(),
                                                            item.getSetor().getCodigo()
                                                    ));
                                                    MessageBox.create(message -> {
                                                        message.message("Observação cadastrada na falta.");
                                                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                        message.position(Pos.TOP_RIGHT);
                                                        message.notification();
                                                    });
                                                    fragment.close();
                                                });
                                            }));
                                        });
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnObservacoes, btnAddObservacoes);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build() /*Extras */
        );
        table.factoryRow(param -> {
            return new TableRow<VSdGestaoMatFaltante>() {
                @Override
                protected void updateItem(VSdGestaoMatFaltante item, boolean empty) {
                    super.updateItem(item, empty);
                    clear();
                    
                    if (item != null && !empty) {
                        getStyleClass().add(item.getStatus().equals("A") ? "table-row-danger" : item.getStatus().equals("C") ? "table-row-warning" : "table-row-success");
                        
                    }
                }
                
                private void clear() {
                    getStyleClass().removeAll("table-row-danger", "table-row-success", "table-row-warning");
                }
            };
        });
        table.contextMenu(FormContextMenu.create(menu -> {
            menu.addItem(item -> {
                item.setText("Substituir material");
                item.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.TROCAR, ImageUtils.IconSize._16));
                item.setOnAction(evt -> {
                    VSdGestaoMatFaltante materialFaltanteSelecionado = (VSdGestaoMatFaltante) table.selectedItem();
                    new Fragment().show(fragment -> {
                        fragment.title("Substituição de Material");
                        fragment.size(900.0, 600.0);
                        final ListProperty<VSdEstoqSugestaoSubstituto> estoqSubstitutos = new SimpleListProperty<>();
                        estoqSubstitutos.set(FXCollections.observableList((List<VSdEstoqSugestaoSubstituto>) new FluentDao().selectFrom(VSdEstoqSugestaoSubstituto.class)
                                .where(it -> it
                                        .equal("id.numero", materialFaltanteSelecionado.getNumero())
                                        .equal("id.insumo", materialFaltanteSelecionado.getInsumo().getCodigo())
                                        .equal("id.cori", materialFaltanteSelecionado.getCori().getCor()))
                                .resultList()));
                        final ListProperty<VSdEstoqueMatCorBr> estoqMatCores = new SimpleListProperty<>();
                        estoqMatCores.set(FXCollections.observableList((List<VSdEstoqueMatCorBr>) new FluentDao().selectFrom(VSdEstoqueMatCorBr.class)
                                .where(it -> it
                                        .equal("codigo.codigo", materialFaltanteSelecionado.getInsumo().getCodigo()))
                                .resultList()));
                        final FormFieldSingleFind<Material> materialOriginalField = FormFieldSingleFind.create(Material.class, field -> {
                            field.title("Material Original");
                            field.editable.set(false);
                            field.setDefaultCode(materialFaltanteSelecionado.getInsumo().getCodigo());
                            field.width(350);
                            field.widthCode(80.0);
                        });
                        final FormFieldSingleFind<Cor> corIOiginalField = FormFieldSingleFind.create(Cor.class, field -> {
                            field.title("Cor Original");
                            field.editable.set(false);
                            field.setDefaultCode(materialFaltanteSelecionado.getCori().getCor());
                            field.width(250);
                            field.widthCode(80.0);
                        });
                        final FormTableView<VSdEstoqueMatCorBr> tableCoresMat = FormTableView.create(VSdEstoqueMatCorBr.class, tableCores -> {
                            tableCores.title("Cores Substitutas Material");
                            tableCores.expanded();
                            tableCores.items.bind(estoqMatCores);
                            tableCores.columns(
                                    FormTableColumn.create(cln -> {
                                        cln.title("Cor");
                                        cln.width(60.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<VSdEstoqueMatCorBr, VSdEstoqueMatCorBr>, ObservableValue<VSdEstoqueMatCorBr>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCor().getCor()));
                                    }).build() /*Cor*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Descrição");
                                        cln.width(240.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<VSdEstoqueMatCorBr, VSdEstoqueMatCorBr>, ObservableValue<VSdEstoqueMatCorBr>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCor().getDescricao()));
                                    }).build() /*Descrição*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Qtde");
                                        cln.width(90.0);
                                        cln.alignment(FormTableColumn.Alignment.RIGHT);
                                        cln.value((Callback<TableColumn.CellDataFeatures<VSdEstoqueMatCorBr, VSdEstoqueMatCorBr>, ObservableValue<VSdEstoqueMatCorBr>>) param -> new ReadOnlyObjectWrapper(StringUtils.toDecimalFormat(param.getValue().getQtde().doubleValue(), 4)));
                                    }).build() /*Qtde*/
                            );
                            tableCores.selectItem(0);
                        });
                        final FormFieldSingleFind<Material> materialSubtitutoField = FormFieldSingleFind.create(Material.class, field -> {
                            field.title("Material Substituto");
                            field.setDefaultCode(materialFaltanteSelecionado.getInsumo().getCodigo());
                            field.width(350);
                            field.widthCode(80.0);
                            field.postSelected((observable, oldValue, newValue) -> {
                                //getCoresMaterial(((Material) field.value.get()).getCodigo());
                                estoqMatCores.set(FXCollections.observableList((List<VSdEstoqueMatCorBr>) new FluentDao().selectFrom(VSdEstoqueMatCorBr.class)
                                        .where(it -> it
                                                .equal("codigo.codigo", ((Material) field.value.get()).getCodigo())
                                                .greaterThan("qtde", BigDecimal.ZERO))
                                        .resultList()));
                                VSdEstoqueMatCorBr selectedCor = getSelectedCor(estoqMatCores, corIOiginalField.value.get().getCor());
                                if (selectedCor != null)
                                    tableCoresMat.selectItem(selectedCor);
                            });
                        });
                        final FormFieldComboBox<Cor> corISubstitutoField = FormFieldComboBox.create(Cor.class, field -> {
                            field.title("Cor");
                            //getCoresMaterial(materialSubtitutoField.value.get().getCodigo());
                            field.items.bind(coresMaterial);
                            field.select(corIOiginalField.value.get());
                            field.width(300.0);
                        });
                        final FormFieldSingleFind<Cor> corISubtitutoField = FormFieldSingleFind.create(Cor.class, field -> {
                            field.title("Cor Substituta");
                            field.setDefaultCode(materialFaltanteSelecionado.getCori().getCor());
                            field.width(250);
                        });
                        final FormTableView<VSdEstoqSugestaoSubstituto> tblSugestaoSubstituto = FormTableView.create(VSdEstoqSugestaoSubstituto.class, tableSugestao -> {
                            tableSugestao.title("Sugestões Compras/Desenvolvimento");
                            tableSugestao.expanded();
                            tableSugestao.items.set(estoqSubstitutos);
                            tableSugestao.columns(
                                    FormTableColumn.create(cln -> {
                                        cln.title("Material");
                                        cln.width(280.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<VSdEstoqSugestaoSubstituto, VSdEstoqSugestaoSubstituto>, ObservableValue<VSdEstoqSugestaoSubstituto>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getInsumosubs()));
                                    }).build(),
                                    FormTableColumn.create(cln -> {
                                        cln.title("Cor");
                                        cln.width(120.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<VSdEstoqSugestaoSubstituto, VSdEstoqSugestaoSubstituto>, ObservableValue<VSdEstoqSugestaoSubstituto>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCorsub()));
                                    }).build(),
                                    FormTableColumn.create(cln -> {
                                        cln.title("Qtde");
                                        cln.width(80);
                                        cln.alignment(FormTableColumn.Alignment.RIGHT);
                                        cln.value((Callback<TableColumn.CellDataFeatures<VSdEstoqSugestaoSubstituto, VSdEstoqSugestaoSubstituto>, ObservableValue<VSdEstoqSugestaoSubstituto>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                                    }).build()
                            );
                            tableSugestao.tableProperties().setOnMouseClicked(event -> {
                                if (event.getClickCount() >= 2) {
                                    if (((VSdEstoqSugestaoSubstituto) tableSugestao.selectedItem()) != null) {
                                        materialSubtitutoField.setDefaultCode(((VSdEstoqSugestaoSubstituto) tableSugestao.selectedItem()).getId().getInsumosubs().getCodigo());
                                        //corISubtitutoField.setDefaultCode(((VSdEstoqSugestaoSubstituto) tableSugestao.selectedItem()).getId().getCorsub().getCor());
                                        //corISubstitutoField.select(((VSdEstoqSugestaoSubstituto) tableSugestao.selectedItem()).getId().getCorsub());
                                        VSdEstoqueMatCorBr selectedCor = getSelectedCor(estoqMatCores, corIOiginalField.value.get().getCor());
                                        if (selectedCor != null)
                                            tableCoresMat.selectItem(selectedCor);
                                    }
                                }
                            });
                            tableSugestao.contextMenu(FormContextMenu.create(cmenuSugestao -> {
                                cmenuSugestao.addItem(itemDepSugestao -> {
                                    itemDepSugestao.setText("Ver Depósitos");
                                    itemDepSugestao.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.DEPOSITO, ImageUtils.IconSize._16));
                                    itemDepSugestao.setOnAction(evtDepSugestao -> {
                                        new Fragment().show(fragmentDeposito -> {
                                            VSdEstoqSugestaoSubstituto substitutoSelecionado = (VSdEstoqSugestaoSubstituto) tableSugestao.selectedItem();
                                            fragmentDeposito.title("Depósitos Material");
                                            fragmentDeposito.size(300.0, 300.0);
                                            final ObservableList<MatIten> depositos = FXCollections.observableList((List<MatIten>) new FluentDao().selectFrom(MatIten.class)
                                                    .where(it -> it
                                                            .equal("id.codigo", substitutoSelecionado.getId().getInsumosubs().getCodigo())
                                                            .equal("id.cor", substitutoSelecionado.getId().getCorsub().getCor())
                                                            .equal("id.deposito.pais", "BR")
                                                            .greaterThan("qtde", BigDecimal.ZERO))
                                                    .resultList());
                                            final FormTableView<MatIten> tblDepositos = FormTableView.create(MatIten.class, tableDepositos -> {
                                                tableDepositos.expanded();
                                                tableDepositos.title("Depósitos Material");
                                                tableDepositos.items.set(depositos);
                                                tableDepositos.columns(
                                                        FormTableColumn.create(cln -> {
                                                            cln.title("Depósito");
                                                            cln.width(80);
                                                            cln.value((Callback<TableColumn.CellDataFeatures<MatIten, MatIten>, ObservableValue<MatIten>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getDeposito()));
                                                            cln.alignment(FormTableColumn.Alignment.CENTER);
                                                        }).build(),
                                                        FormTableColumn.create(cln -> {
                                                            cln.title("Qtde");
                                                            cln.width(120);
                                                            cln.value((Callback<TableColumn.CellDataFeatures<MatIten, MatIten>, ObservableValue<MatIten>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                                                            cln.alignment(FormTableColumn.Alignment.RIGHT);
                                                        }).build()
                                                );
                                            });
                                            fragmentDeposito.box.getChildren().add(tblDepositos.build());
                                        });
                                    });
                                });
                            }));
                        });
                        VSdEstoqueMatCorBr selectedCor = getSelectedCor(estoqMatCores, corIOiginalField.value.get().getCor());
                        if (selectedCor != null)
                            tableCoresMat.selectItem(selectedCor);
                        
                        fragment.box.getChildren().add(FormBox.create(boxFormulario -> {
                            boxFormulario.vertical();
                            boxFormulario.expanded();
                            boxFormulario.add(FormBox.create(boxMateriais -> {
                                boxMateriais.horizontal();
                                boxMateriais.add(FormBox.create(boxFields -> {
                                    boxFields.vertical();
                                    boxFields.expanded();
                                    boxFields.add(materialOriginalField.build(), corIOiginalField.build(), materialSubtitutoField.build(), tableCoresMat.build());
                                }));
                                boxMateriais.add(tblSugestaoSubstituto.build());
                            }));
//                            boxFormulario.add(FormBox.create(boxOriginal -> {
//                                boxOriginal.horizontal();
//                                boxOriginal.add(materialOriginalField.build(), corIOiginalField.build());
//                            }));
//                            boxFormulario.add(FormBox.create(boxSubstituto -> {
//                                boxSubstituto.vertical();
//                                boxSubstituto.add(materialSubtitutoField.build(), tableCoresMat.build());
//                            }));
//                            boxFormulario.add();
                        }));
                        
                        final Button btnSubstituir = FormButton.create(btnSubs -> {
                            btnSubs.title("Substituir");
                            btnSubs.icon(ImageUtils.getIcon(ImageUtils.Icon.SUBSTITUIR, ImageUtils.IconSize._16));
                            btnSubs.addStyle("success");
                            btnSubs.setAction(evtSubstituir -> {
                                if (tableCoresMat.selectedItem() == null || materialSubtitutoField.value.get() == null) {
                                    MessageBox.create(message -> {
                                        message.message("Você deve informar um Material e Cor para a substituição, verifique se os campos estão preenchidos e selecionados.");
                                        message.type(MessageBox.TypeMessageBox.IMPORTANT);
                                        message.showAndWait();
                                    });
                                    return;
                                }
                                substituirMaterial(materialFaltanteSelecionado, materialSubtitutoField.value.getValue(), tableCoresMat.selectedItem().getCor());
                                new FluentDao().runNativeQueryUpdate(String.format("update sd_materiais_faltantes_001 set substituto = 'S', insumo = '%s', cor_i = '%s' where numero = '%s' and insumo = '%s' and cor_i = '%s' and setor = '%s'",
                                        materialSubtitutoField.value.getValue().getCodigo(),
                                        tableCoresMat.selectedItem().getCor().getCor(),
                                        materialFaltanteSelecionado.getNumero(),
                                        materialFaltanteSelecionado.getInsumo().getCodigo(),
                                        materialFaltanteSelecionado.getCori().getCor(),
                                        materialFaltanteSelecionado.getSetor().getCodigo()));
                                materialFaltanteSelecionado.setSubstituto(true);
                                materialFaltanteSelecionado.setInsumo(materialSubtitutoField.value.getValue());
                                materialFaltanteSelecionado.setCori(tableCoresMat.selectedItem().getCor());
                                MessageBox.create(message -> {
                                    message.message("Material Subtituido com Sucesso");
                                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                                    message.position(Pos.TOP_RIGHT);
                                    message.notification();
                                });
                                fragment.modalStage.close();
                                table.refresh();
                            });
                        });
                        fragment.buttonsBox.getChildren().addAll(btnSubstituir);
                    });
                });
            });
            menu.addSeparator();
            menu.addItem(item -> {
                item.setText("Alterar Status");
                item.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.ALTERAR_STATUS, ImageUtils.IconSize._16));
                item.setOnAction(evt -> {
                    new Fragment().show(fragment -> {
                        fragment.title("Alterar Status Falta");
                        fragment.size(500.0, 400.0);
                        VSdGestaoMatFaltante materialSelecionado = (VSdGestaoMatFaltante) table.selectedItem();
                        final FormFieldSegmentedButton<String> novoStatusFaltaField = FormFieldSegmentedButton.create(field -> {
                            field.title("Novo Status");
                            field.options(
                                    field.option("Aberto", "A", FormFieldSegmentedButton.Style.DANGER),
                                    field.option("Com Compra", "C", FormFieldSegmentedButton.Style.WARNING)
                            );
                            field.select(materialSelecionado.getStatus());
                        });
                        final FormTableView<VSdComprasMaterial> tblComprasMaterial = FormTableView.create(VSdComprasMaterial.class, tableCompras -> {
                            tableCompras.title("Ordens de Compra/Tecelagem");
                            tableCompras.expanded();
                            tableCompras.items.set(FXCollections.observableList((List<VSdComprasMaterial>) new FluentDao().selectFrom(VSdComprasMaterial.class)
                                    .where(it -> it
                                            .equal("codigo", materialSelecionado.getInsumo().getCodigo())
                                            .equal("cor", materialSelecionado.getCori().getCor()))
                                    .resultList()));
                            tableCompras.columns(
                                    FormTableColumn.create(cln -> {
                                        cln.title("O.C.");
                                        cln.width(80.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<VSdComprasMaterial, VSdComprasMaterial>, ObservableValue<VSdComprasMaterial>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumero()));
                                        cln.alignment(FormTableColumn.Alignment.CENTER);
                                    }).build() /*O.C.*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Depósito");
                                        cln.width(80.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<VSdComprasMaterial, VSdComprasMaterial>, ObservableValue<VSdComprasMaterial>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDeposito()));
                                        cln.alignment(FormTableColumn.Alignment.CENTER);
                                    }).build() /*Depósito*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Qtde");
                                        cln.width(100.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<VSdComprasMaterial, VSdComprasMaterial>, ObservableValue<VSdComprasMaterial>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                                        cln.alignment(FormTableColumn.Alignment.RIGHT);
                                        cln.format(param -> {
                                            return new TableCell<VSdComprasMaterial, BigDecimal>() {
                                                @Override
                                                protected void updateItem(BigDecimal item, boolean empty) {
                                                    super.updateItem(item, empty);
                                                    setText(null);
                                                    if (item != null && !empty) {
                                                        setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                                                    }
                                                }
                                            };
                                        });
                                    }).build() /*Qtde*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Faturado em");
                                        cln.width(90.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<VSdComprasMaterial, VSdComprasMaterial>, ObservableValue<VSdComprasMaterial>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDtfatura()));
                                        cln.alignment(FormTableColumn.Alignment.CENTER);
                                        cln.format(param -> {
                                            return new TableCell<VSdComprasMaterial, LocalDate>() {
                                                @Override
                                                protected void updateItem(LocalDate item, boolean empty) {
                                                    super.updateItem(item, empty);
                                                    setText(null);
                                                    if (item != null && !empty) {
                                                        setText(StringUtils.toDateFormat(item));
                                                    }
                                                }
                                            };
                                        });
                                    }).build() /*Faturado em*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Ent. Prevista");
                                        cln.width(80.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<VSdComprasMaterial, VSdComprasMaterial>, ObservableValue<VSdComprasMaterial>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDtentrega()));
                                        cln.alignment(FormTableColumn.Alignment.CENTER);
                                        cln.format(param -> {
                                            return new TableCell<VSdComprasMaterial, LocalDate>() {
                                                @Override
                                                protected void updateItem(LocalDate item, boolean empty) {
                                                    super.updateItem(item, empty);
                                                    setText(null);
                                                    if (item != null && !empty) {
                                                        setText(StringUtils.toDateFormat(item));
                                                    }
                                                }
                                            };
                                        });
                                    }).build() /*Ent. Prevista*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Ações");
                                        cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                                        cln.width(80.0);
                                        cln.alignment(FormTableColumn.Alignment.CENTER);
                                        cln.value((Callback<TableColumn.CellDataFeatures<VSdComprasMaterial, VSdComprasMaterial>, ObservableValue<VSdComprasMaterial>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                        cln.format(param -> {
                                            return new TableCell<VSdComprasMaterial, VSdComprasMaterial>() {
                                                final HBox boxButtonsRow = new HBox(3);
                                                final Button btnFaturaOc = FormButton.create(btn -> {
                                                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.FATURAR_PEDIDO, ImageUtils.IconSize._16));
                                                    btn.tooltip("Visualizar Pedido e Fatura");
                                                    btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                                    btn.addStyle("xs").addStyle("success");
                                                });
                                                final Button btnObservacaoOc = FormButton.create(btn -> {
                                                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.INFORMACAO, ImageUtils.IconSize._16));
                                                    btn.tooltip("Visualizar Observações");
                                                    btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                                    btn.addStyle("xs").addStyle("info");
                                                });
                                                
                                                @Override
                                                protected void updateItem(VSdComprasMaterial item, boolean empty) {
                                                    super.updateItem(item, empty);
                                                    setText(null);
                                                    setGraphic(null);
                                                    if (item != null && !empty) {
                                                        
                                                        btnFaturaOc.setOnAction(evt -> {
                                                            MessageBox.create(message -> {
                                                                message.message("Pedido: " + item.getPedfornecedor() + "\n" +
                                                                        "Fatura: " + item.getNffornecedor());
                                                                message.type(MessageBox.TypeMessageBox.INPUT);
                                                                message.showAndWait();
                                                            });
                                                        });
                                                        btnObservacaoOc.setOnAction(evt -> {
                                                            MessageBox.create(message -> {
                                                                message.message(item.getObservacao());
                                                                message.type(MessageBox.TypeMessageBox.INPUT);
                                                                message.showAndWait();
                                                            });
                                                        });
                                                        boxButtonsRow.getChildren().clear();
                                                        boxButtonsRow.getChildren().addAll(btnFaturaOc, btnObservacaoOc);
                                                        setGraphic(boxButtonsRow);
                                                    }
                                                }
                                            };
                                        });
                                    }).build()
                            );
                        });
                        fragment.box.getChildren().add(novoStatusFaltaField.build());
                        fragment.box.getChildren().add(tblComprasMaterial.build());
                        final Button btnSalvarNovoStatus = FormButton.create(btn -> {
                            btn.title("Novo Status");
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                            btn.addStyle("sm").addStyle("success");
                            btn.setAction(evtSalvar -> {
                                alterarStatusFalta(materialSelecionado, novoStatusFaltaField.value.get());
                                materialSelecionado.setStatus(novoStatusFaltaField.value.get());
                                table.refresh();
                                fragment.close();
                            });
                        });
                        fragment.buttonsBox.getChildren().add(btnSalvarNovoStatus);
                    });
                });
            });
        }));
        table.indices(
                table.indice(Color.LIGHTCORAL, "Falta em Aberta", false),
                table.indice(Color.LIGHTYELLOW, "Faltas com Compra", false),
                table.indice(Color.LIGHTGREEN, "Faltas Resolvidas", false)
        );
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Filtros Faltantes">
    private final FormFieldText filtroFaltNumero = FormFieldText.create(field -> {
        field.title("O.F.");
        field.value.addListener((observable, oldValue, newValue) -> {
            if(newValue.equals("")) field.value.setValue("");
        });
    });
    private final FormFieldMultipleFind<CadFluxo> filtroFaltSetor = FormFieldMultipleFind.create(CadFluxo.class, field -> {
        field.title("Setor Consumo");
        field.textValue.addListener((observable, oldValue, newValue) -> {
            if(newValue.equals("")) field.objectValues.clear();
        });
    });
    private final FormFieldMultipleFind<CadFluxo> filtroFaltSetorMov = FormFieldMultipleFind.create(CadFluxo.class, field -> {
        field.title("Setor Movimento");
        field.textValue.addListener((observable, oldValue, newValue) -> {
            if(newValue.equals("")) field.objectValues.clear();
        });
    });
    private final FormFieldMultipleFind<Material> filtroFaltMaterial = FormFieldMultipleFind.create(Material.class, field -> {
        field.title("Material");
        field.width(450.0);
        field.textValue.addListener((observable, oldValue, newValue) -> {
            if(newValue.equals("")) field.objectValues.clear();
        });
    });
    private final FormFieldMultipleFind<Cor> filtroFaltCor = FormFieldMultipleFind.create(Cor.class, field -> {
        field.title("Cor Material");
        field.width(280.0);
        field.textValue.addListener((observable, oldValue, newValue) -> {
            if(newValue.equals("")) field.objectValues.clear();
        });
    });
    private final FormFieldMultipleFind<GrupoMa> filtroFaltGrupo = FormFieldMultipleFind.create(GrupoMa.class, field -> {
        field.title("Grupo Material");
        field.textValue.addListener((observable, oldValue, newValue) -> {
            if(newValue.equals("")) field.objectValues.clear();
        });
    });
    private final FormFieldMultipleFind<SubGrupoMa> filtroFaltSubGrupo = FormFieldMultipleFind.create(SubGrupoMa.class, field -> {
        field.title("Subgrupo Material");
        field.textValue.addListener((observable, oldValue, newValue) -> {
            if(newValue.equals("")) field.objectValues.clear();
        });
    });
    private final FormFieldMultipleFind<TabPrz> filtroFaltPeridodo = FormFieldMultipleFind.create(TabPrz.class, field -> {
        field.title("Período");
        field.textValue.addListener((observable, oldValue, newValue) -> {
            if(newValue.equals("")) field.objectValues.clear();
        });
    });
    private final FormFieldMultipleFind<Entidade> filtroFaltFaccao = FormFieldMultipleFind.create(Entidade.class, field -> {
        field.title("Faccção");
        field.textValue.addListener((observable, oldValue, newValue) -> {
            if(newValue.equals("")) field.textValue.setValue("");
        });
    });
    private final FormFieldSegmentedButton<String> filtroFaltStatus = FormFieldSegmentedButton.create(filtroFaltStatus -> {
        filtroFaltStatus.title("Status");
        filtroFaltStatus.options(
                filtroFaltStatus.option("Todos", "T", FormFieldSegmentedButton.Style.SECUNDARY),
                filtroFaltStatus.option("Aberto", "A", FormFieldSegmentedButton.Style.DANGER),
                filtroFaltStatus.option("Com Compra", "C", FormFieldSegmentedButton.Style.WARNING),
                filtroFaltStatus.option("Aberto + Compra", "D", FormFieldSegmentedButton.Style.PRIMARY),
                filtroFaltStatus.option("Resolvido", "R", FormFieldSegmentedButton.Style.SUCCESS)
        );
        filtroFaltStatus.select(0);
    });
    private final FormFieldSegmentedButton<String> filtroFaltSubstituto = FormFieldSegmentedButton.create(filtroFaltSubstituto -> {
        filtroFaltSubstituto.title("Substituto");
        filtroFaltSubstituto.options(
                filtroFaltSubstituto.option("Ambos", "T", FormFieldSegmentedButton.Style.SECUNDARY),
                filtroFaltSubstituto.option("Sim", "true", FormFieldSegmentedButton.Style.SUCCESS),
                filtroFaltSubstituto.option("Não", "false", FormFieldSegmentedButton.Style.DANGER)
        );
        filtroFaltSubstituto.select(0);
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Fields MOstradores">
    private final FormFieldText fieldTotalFalta = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Qtde Total:");
        field.value.bind(totalFalta.asString("%.2f"));
        field.editable(false);
        field.addStyle("primary");
        field.width(200.0);
        field.alignment(FormFieldText.Alignment.RIGHT);
    });
    private final FormFieldText fieldTotalAberto = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Qtde Aberto:");
        field.value.bind(totalAberto.asString("%.2f"));
        field.editable(false);
        field.addStyle("danger");
        field.width(200.0);
        field.alignment(FormFieldText.Alignment.RIGHT);
    });
    private final FormFieldText fieldTotalCompra = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Qtde Compra:");
        field.value.bind(totalCompra.asString("%.2f"));
        field.editable(false);
        field.addStyle("warning");
        field.width(200.0);
        field.alignment(FormFieldText.Alignment.RIGHT);
    });
    private final FormFieldText fieldTotalResolvido = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Qtde Resolvido:");
        field.value.bind(totalResolvido.asString("%.2f"));
        field.editable(false);
        field.addStyle("success");
        field.width(200.0);
        field.alignment(FormFieldText.Alignment.RIGHT);
    });
    // </editor-fold>
    
    private VSdEstoqueMatCorBr getSelectedCor(ObservableList<VSdEstoqueMatCorBr> list, String cor){
        Optional<VSdEstoqueMatCorBr> optSelect = list.stream().filter(i -> i.getCor().getCor().equals(cor)).distinct().findFirst();
        VSdEstoqueMatCorBr select = optSelect.isPresent() ? optSelect.get() : null;
        if (select != null) {
            int indexSelected = list.indexOf(select);
            list.remove(indexSelected);
            list.add(0, select);
        }
        return select;
    }
    
    public GestaoFaltaMateriaisView() {
        super("Gestão Falta de Materiais", ImageUtils.getImage(ImageUtils.Icon.BAIXAR_MATERIAL));
        init();
    }
    
    private void init() {
        this.initFaltasTab();
    
//        new RunAsyncWithOverlay(this).exec(task -> {
//            super.getFaltantes(null, null, null, null, null, null, null, "T", "D", null, null);
//            return ReturnAsync.OK.value;
//        }).addTaskEndNotification(taskReturn -> {
//            if (taskReturn.equals(ReturnAsync.OK.value) && materiaisFaltantes.size() > 0) {
//                materiaisFaltantesObservable.set(FXCollections.observableList(materiaisFaltantes));
//                totalFalta.set(materiaisFaltantesObservable.stream().mapToDouble(it -> it.getQtde().doubleValue()).sum());
//                totalAberto.set(materiaisFaltantesObservable.stream().filter(it -> it.getStatus().equals("A")).mapToDouble(it -> it.getQtde().doubleValue()).sum());
//                totalCompra.set(materiaisFaltantesObservable.stream().filter(it -> it.getStatus().equals("C")).mapToDouble(it -> it.getQtde().doubleValue()).sum());
//                totalResolvido.set(materiaisFaltantesObservable.stream().filter(it -> it.getStatus().equals("R")).mapToDouble(it -> it.getQtde().doubleValue()).sum());
//            }
//        });
    }
    
    private void initFaltasTab() {
        super.box.getChildren().add(FormBox.create(boxHeaderFilter -> {
            boxHeaderFilter.horizontal();
            boxHeaderFilter.add(FormTitledPane.create(filter -> {
                filter.filter();
                filter.add(FormBox.create(boxFields -> {
                    boxFields.horizontal();
                    boxFields.add(FormBox.create(box -> {
                        box.vertical();
                        box.add(filtroFaltStatus.build());
                        box.add(FormBox.create(box1 -> {
                            box1.horizontal();
                            box1.add(filtroFaltSubstituto.build());
                            box1.add(filtroFaltCor.build());
                        }));
                        box.add(filtroFaltMaterial.build());
                    }));
                    boxFields.add(FormBox.create(box -> {
                        box.vertical();
                        box.add(filtroFaltNumero.build());
                        box.add(filtroFaltPeridodo.build());
                        box.add(filtroFaltSetor.build());
                    }));
                    boxFields.add(FormBox.create(box -> {
                        box.vertical();
                        box.add(filtroFaltSetorMov.build());
                        box.add(filtroFaltGrupo.build());
                        box.add(filtroFaltSubGrupo.build());
                    }));
                    boxFields.add(FormBox.create(box -> {
                        box.vertical();
                        box.add(filtroFaltFaccao.build());
                    }));
                }));
                filter.find.setOnAction(evt -> {
                    new RunAsyncWithOverlay(this).exec(task -> {
                        super.getFaltantes(
                                filtroFaltNumero.value.get() == null || filtroFaltNumero.value.get().equals("") ? null : filtroFaltNumero.value.get().split(","),
                                filtroFaltMaterial.objectValues.stream().map(it -> it.getCodigoFilter()).toArray(),
                                filtroFaltCor.objectValues.stream().map(it -> it.getCodigoFilter()).toArray(),
                                filtroFaltSetor.objectValues.stream().map(it -> it.getCodigoFilter()).toArray(),
                                filtroFaltSetorMov.objectValues.stream().map(it -> it.getCodigoFilter()).toArray(),
                                filtroFaltGrupo.objectValues.stream().map(it -> it.getCodigoFilter()).toArray(),
                                filtroFaltSubGrupo.objectValues.stream().map(it -> it.getCodigoFilter()).toArray(),
                                filtroFaltSubstituto.value.get(),
                                filtroFaltStatus.value.get(),
                                filtroFaltFaccao.objectValues.stream().map(it -> it.getCodcli()).toArray(),
                                filtroFaltPeridodo.objectValues.stream().map(it -> it.getPrazo()).toArray()
                        );
                        return ReturnAsync.OK.value;
                    }).addTaskEndNotification(taskReturn -> {
                        if (taskReturn.equals(ReturnAsync.OK.value)) {
                            materiaisFaltantesObservable.set(FXCollections.observableList(materiaisFaltantes));
                            totalFalta.set(materiaisFaltantesObservable.stream().mapToDouble(it -> it.getQtde().doubleValue()).sum());
                            totalAberto.set(materiaisFaltantesObservable.stream().filter(it -> it.getStatus().equals("A")).mapToDouble(it -> it.getQtde().doubleValue()).sum());
                            totalCompra.set(materiaisFaltantesObservable.stream().filter(it -> it.getStatus().equals("C")).mapToDouble(it -> it.getQtde().doubleValue()).sum());
                            totalResolvido.set(materiaisFaltantesObservable.stream().filter(it -> it.getStatus().equals("R")).mapToDouble(it -> it.getQtde().doubleValue()).sum());
                        }
                    });
                    
                });
                filter.clean.setOnAction(evt -> {
                    filtroFaltCor.clear();
                    filtroFaltGrupo.clear();
                    filtroFaltMaterial.clear();
                    filtroFaltNumero.clear();
                    filtroFaltSetor.clear();
                    filtroFaltSetorMov.clear();
                    filtroFaltSubGrupo.clear();
                    filtroFaltPeridodo.clear();
                    filtroFaltStatus.select(0);
                    filtroFaltSubstituto.select(0);
                });
            }));
        }));
        super.box.getChildren().add(FormBox.create(boxTable -> {
            boxTable.vertical();
            boxTable.expanded();
            boxTable.add(tblFaltantes.build());
        }));
        super.box.getChildren().add(FormBox.create(box -> {
            box.horizontal();
            box.add(
                    fieldTotalFalta.build(),
                    fieldTotalAberto.build(),
                    fieldTotalCompra.build(),
                    fieldTotalResolvido.build()
            );
        }));
    }
}
