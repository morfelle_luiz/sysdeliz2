package sysdeliz2.views.compras;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.input.KeyCode;
import javafx.util.Callback;
import sysdeliz2.controllers.views.compras.ManutencaoDeLoteController;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.ti.Cor;
import sysdeliz2.models.ti.Material;
import sysdeliz2.models.view.VSdMateriasLote;
import sysdeliz2.utils.converters.BigDecimalAttributeConverter;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.SysLogger;

import java.math.BigDecimal;
import java.sql.SQLException;

public class ManutencaoDeLoteView extends ManutencaoDeLoteController {

    // <editor-fold defaultstate="collapsed" desc="Bean">
    private final ListProperty<VSdMateriasLote> materiasBean = new SimpleListProperty<>(FXCollections.observableArrayList());

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Filters">
    private final FormFieldMultipleFind<Cor> filterCorField = FormFieldMultipleFind.create(Cor.class, field -> {
        field.title("Cor");
    });
    private final FormFieldMultipleFind<Material> filterCodigoField = FormFieldMultipleFind.create(Material.class, field -> {
        field.title("Código");
    });
    private final FormFieldText filterLoteField = FormFieldText.create(field -> {
        field.title("Lote");
        field.toUpper();
    });
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Tables">
    private final FormTableView<VSdMateriasLote> tblMaterias = FormTableView.create(FormTableView.class, table -> {
        table.title("Materias");
        table.expanded();
        table.items.bind(materiasBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(120.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriasLote, VSdMateriasLote>, ObservableValue<VSdMateriasLote>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo()));
                }).build() /*Status Reserva*/,
                FormTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(400);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriasLote, VSdMateriasLote>, ObservableValue<VSdMateriasLote>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDescricao()));
                }).build() /**/,
                FormTableColumn.create(cln -> {
                    cln.title("Cor");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriasLote, VSdMateriasLote>, ObservableValue<VSdMateriasLote>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCor().getCor()));
                }).build() /*Status Reserva*/,
                FormTableColumn.create(cln -> {
                    cln.title("Desc. Cor");
                    cln.width(200.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriasLote, VSdMateriasLote>, ObservableValue<VSdMateriasLote>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCor().getDescricao()));
                }).build() /*Status Reserva*/,
                FormTableColumn.create(cln -> {
                    cln.title("Lote");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriasLote, VSdMateriasLote>, ObservableValue<VSdMateriasLote>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLote()));
                }).build() /*Status Reserva*/,
                FormTableColumn.create(cln -> {
                    cln.title("Observação");
                    cln.width(400);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriasLote, VSdMateriasLote>, ObservableValue<VSdMateriasLote>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<VSdMateriasLote, VSdMateriasLote>() {
                        @Override
                        protected void updateItem(VSdMateriasLote item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (!empty) {
                                setGraphic(FormFieldText.create(field -> {
                                    field.editable(false);
                                    field.mouseClicked(event -> {
                                        if (event.getClickCount() >= 2) {
                                            field.editable(true);
                                        }
                                    });
                                    field.withoutTitle();
                                    field.textField.setContextMenu(FormContextMenu.create(menu -> {
                                        menu.addItem(menuItem -> {
                                            menuItem.setText("Aplicar à todos");
                                            menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._16));
                                            menuItem.setOnAction(evt -> {
                                                if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                                                    message.message("Tem certeza que deseja alterar todos?");
                                                    message.showAndWait();
                                                }).value.get())) {
                                                    item.setObs(field.value.getValue() == null ? "" : field.value.getValue());
                                                    materiasBean.forEach(it -> {
                                                        atualizaObsLote(it,field,it.getDescricao());
                                                        it.setObs(item.getObs());
                                                    });
                                                    table.refresh();
                                                    MessageBox.create(message -> {
                                                        message.message("Alterações Salvas com sucesso!");
                                                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                        message.show();
                                                    });
                                                }
                                            });
                                        });
                                    }));
                                    field.value.bindBidirectional(item.obsProperty());
                                    String oldValue = field.value.getValue();
                                    field.keyReleased(event -> {
                                        if (event.getCode().equals(KeyCode.ENTER)) {
                                            atualizaObsLote(item, field, oldValue);
                                        }
                                    });
                                }).build());
                            }
                        }
                    });
                }).build() /*Status Reserva*/,
                FormTableColumn.create(cln -> {
                    cln.title("Largura");
                    cln.width(60.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriasLote, VSdMateriasLote>, ObservableValue<VSdMateriasLote>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<VSdMateriasLote, VSdMateriasLote>() {
                            @Override
                            protected void updateItem(VSdMateriasLote item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (!empty) {
                                    setGraphic(FormFieldText.create(field -> {
                                        field.editable(false);
                                        field.decimalField(2);
                                        field.mouseClicked(event -> {
                                            if (event.getClickCount() >= 2) {
                                                field.editable(true);
                                            }
                                        });
                                        field.withoutTitle();
                                        field.textField.setContextMenu(FormContextMenu.create(menu -> {
                                            menu.addItem(menuItem -> {
                                                menuItem.setText("Aplicar à todos");
                                                menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._16));
                                                menuItem.setOnAction(evt -> {
                                                    if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                                                        message.message("Tem certeza que deseja alterar todos?");
                                                        message.showAndWait();
                                                    }).value.get())) {
                                                        item.setLargura(field.value.getValue() == null ? BigDecimal.ZERO : new BigDecimal(field.value.getValue().replace(",",".")));
                                                        materiasBean.forEach(it -> {
                                                            atualizaLarguraLote(it,field,it.getLargura().toString());
                                                            it.setLargura(item.getLargura());
                                                        });
                                                        table.refresh();
                                                        MessageBox.create(message -> {
                                                            message.message("Alterações Salvas com sucesso!");
                                                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                            message.show();
                                                        });
                                                    }
                                                });
                                            });
                                        }));
                                        field.value.bindBidirectional(item.larguraProperty(), new BigDecimalAttributeConverter());
                                        String oldValue = field.value.getValue();
                                        field.keyReleased(event -> {
                                            if (event.getCode().equals(KeyCode.ENTER)) {
                                                atualizaLarguraLote(item, field, oldValue);
                                            }
                                        });
                                    }).build());
                                }
                            }
                        };
                    });
                }).build() /*Status Reserva*/,
                FormTableColumn.create(cln -> {
                    cln.title("Partida");
                    cln.width(120.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriasLote, VSdMateriasLote>, ObservableValue<VSdMateriasLote>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<VSdMateriasLote, VSdMateriasLote>() {
                            @Override
                            protected void updateItem(VSdMateriasLote item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (!empty) {
                                    setGraphic(FormFieldText.create(field -> {
                                        field.editable(false);
                                        field.mouseClicked(event -> {
                                            if (event.getClickCount() >= 2) {
                                                field.editable(true);
                                            }
                                        });
                                        field.withoutTitle();
                                        field.textField.setContextMenu(FormContextMenu.create(menu -> {
                                            menu.addItem(menuItem -> {
                                                menuItem.setText("Aplicar à todos");
                                                menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._16));
                                                menuItem.setOnAction(evt -> {
                                                    if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                                                        message.message("Tem certeza que deseja alterar todos?");
                                                        message.showAndWait();
                                                    }).value.get())) {
                                                        item.setPartida(field.value.getValue() == null ? "" : field.value.getValue());
                                                        materiasBean.forEach(it -> {
                                                            atualizaPartidaLote(it, field, it.getPartida());
                                                            it.setPartida(item.getPartida());
                                                        });
                                                        table.refresh();
                                                        MessageBox.create(message -> {
                                                            message.message("Alterações Salvas com sucesso!");
                                                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                            message.show();
                                                        });
                                                    }
                                                });
                                            });
                                        }));
                                        field.value.set(item.getPartida());
                                        String oldValue = field.value.getValue();
                                        field.keyReleased(event -> {
                                            if (event.getCode().equals(KeyCode.ENTER)) {
                                                atualizaPartidaLote(item, field, oldValue);
                                            }
                                        });
                                    }).build());
                                }
                            }
                        };
                    });
                }).build() /*Status Reserva*/,
                FormTableColumn.create(cln -> {
                    cln.title("Seq.");
                    cln.width(60.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriasLote, VSdMateriasLote>, ObservableValue<VSdMateriasLote>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<VSdMateriasLote, VSdMateriasLote>() {
                            @Override
                            protected void updateItem(VSdMateriasLote item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (!empty) {
                                    setGraphic(FormFieldText.create(field -> {
                                        field.editable(false);
                                        field.mouseClicked(event -> {
                                            if (event.getClickCount() >= 2) {
                                                field.editable(true);
                                            }
                                        });
                                        field.withoutTitle();
                                        field.textField.setContextMenu(FormContextMenu.create(menu -> {
                                            menu.addItem(menuItem -> {
                                                menuItem.setText("Aplicar à todos");
                                                menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._16));
                                                menuItem.setOnAction(evt -> {
                                                    if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                                                        message.message("Tem certeza que deseja alterar todos?");
                                                        message.showAndWait();
                                                    }).value.get())) {
                                                        item.setSequencia(field.value.getValue() == null ? "" : field.value.getValue());
                                                        materiasBean.forEach(it -> {
                                                            atualizaSequenciaLote(it, field, it.getSequencia());
                                                            it.setSequencia(item.getSequencia());
                                                        });
                                                        table.refresh();
                                                        MessageBox.create(message -> {
                                                            message.message("Alterações Salvas com sucesso!");
                                                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                            message.show();
                                                        });
                                                    }
                                                });
                                            });
                                        }));
                                        field.value.set(item.getSequencia());
                                        String oldValue = field.value.getValue();
                                        field.keyReleased(event -> {
                                            if (event.getCode().equals(KeyCode.ENTER)) {
                                                atualizaSequenciaLote(item, field, oldValue);
                                            }
                                        });
                                    }).build());
                                }
                            }
                        };
                    });
                }).build() /*Status Reserva*/,
                FormTableColumn.create(cln -> {
                    cln.title("Tonalidade");
                    cln.width(120.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriasLote, VSdMateriasLote>, ObservableValue<VSdMateriasLote>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<VSdMateriasLote, VSdMateriasLote>() {
                            @Override
                            protected void updateItem(VSdMateriasLote item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (!empty) {
                                    setGraphic(FormFieldText.create(field -> {
                                        field.withoutTitle();
                                        field.editable(false);
                                        field.mouseClicked(event -> {
                                            if (event.getClickCount() >= 2) {
                                                field.editable(true);
                                            }
                                        });
                                        field.textField.setContextMenu(FormContextMenu.create(menu -> {
                                            menu.addItem(menuItem -> {
                                                menuItem.setText("Aplicar à todos");
                                                menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._16));
                                                menuItem.setOnAction(evt -> {
                                                    if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                                                        message.message("Tem certeza que deseja alterar todos?");
                                                        message.showAndWait();
                                                    }).value.get())) {
                                                        item.setTonalidade(field.value.getValue() == null ? "" : field.value.getValue());
                                                        materiasBean.forEach(it -> {
                                                            atualizaTonalidadeLote(it, field, it.getTonalidade());
                                                            it.setTonalidade(item.getTonalidade());
                                                        });
                                                        table.refresh();
                                                        MessageBox.create(message -> {
                                                            message.message("Alterações Salvas com sucesso!");
                                                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                            message.show();
                                                        });
                                                    }
                                                });
                                            });
                                        }));
                                        field.value.set(item.getTonalidade());
                                        String oldValue = field.value.getValue();
                                        field.keyReleased(event -> {
                                            if (event.getCode().equals(KeyCode.ENTER)) {
                                                atualizaTonalidadeLote(item, field, oldValue);
                                            }
                                        });
                                    }).build());
                                }
                            }
                        };
                    });
                }).build() /*Status Reserva*/,
                FormTableColumn.create(cln -> {
                    cln.title("Gramatura");
                    cln.width(120.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriasLote, VSdMateriasLote>, ObservableValue<VSdMateriasLote>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<VSdMateriasLote, VSdMateriasLote>() {
                            @Override
                            protected void updateItem(VSdMateriasLote item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (!empty) {
                                    setGraphic(FormFieldText.create(field -> {
                                        field.editable(false);
                                        field.decimalField(3);
                                        field.mouseClicked(event -> {
                                            if (event.getClickCount() >= 2) {
                                                field.editable(true);
                                            }
                                        });
                                        field.withoutTitle();
                                        field.textField.setContextMenu(FormContextMenu.create(menu -> {
                                            menu.addItem(menuItem -> {
                                                menuItem.setText("Aplicar à todos");
                                                menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._16));
                                                menuItem.setOnAction(evt -> {
                                                    if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                                                        message.message("Tem certeza que deseja alterar todos?");
                                                        message.showAndWait();
                                                    }).value.get())) {
                                                        item.setGramatura(field.value.getValue() == null ? BigDecimal.ZERO : new BigDecimal(field.value.getValue().replace(",",".")));
                                                        materiasBean.forEach(it -> {
                                                            atualizaGramaturaLote(it, field, it.getGramatura().toString());
                                                            it.setGramatura(item.getGramatura());
                                                        });
                                                        table.refresh();
                                                        MessageBox.create(message -> {
                                                            message.message("Alterações Salvas com sucesso!");
                                                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                            message.show();
                                                        });
                                                    }
                                                });
                                            });
                                        }));
                                        field.value.bindBidirectional(item.gramaturaProperty(), new BigDecimalAttributeConverter());
                                        String oldValue = field.value.getValue();
                                        field.keyReleased(event -> {
                                            if (event.getCode().equals(KeyCode.ENTER)) {
                                                atualizaGramaturaLote(item, field, oldValue);
                                            }
                                        });
                                    }).build());
                                }
                            }
                        };
                    });
                }).build() /*Status Reserva*/
        );
    });


    // </editor-fold>

    public ManutencaoDeLoteView() {
        super("Manutenção de Lote", ImageUtils.getImage(ImageUtils.Icon.TECIDO));
        init();
    }

    private void init() {
        super.box.getChildren().add(FormBox.create(principal -> {
            principal.vertical();
            principal.expanded();

            principal.add(FormBox.create(boxFilter -> {
                boxFilter.horizontal();
                boxFilter.add(FormTitledPane.create(filter -> {
                    filter.find.disableProperty().bind(filterLoteField.value.isNull().or(filterLoteField.value.isEmpty())
                            .and(filterCodigoField.objectValues.emptyProperty()));
                    filter.filter();
                    filter.add(FormBox.create(lob -> {
                        lob.horizontal();
                        lob.add(filterCodigoField.build());
                        lob.add(filterCorField.build());
                        lob.add(filterLoteField.build());
                        filterLoteField.value.set("");
                    }));
                    filter.find.setOnAction(evt -> {

                        new RunAsyncWithOverlay(this).exec(task -> {
                            getItens(filterCodigoField.objectValues.stream().map(Material::getCodigo).toArray(),
                                    filterCorField.objectValues.toArray(),
                                    filterLoteField.textField.getText());
                            return ReturnAsync.OK.value;
                        }).addTaskEndNotification(taskReturn -> {
                            if (taskReturn.equals(ReturnAsync.OK.value)) {
                                materiasBean.set(FXCollections.observableList(super.materiais));
                            }
                        });
                    });
                    filter.clean.setOnAction(evt -> {
                        filterCorField.clear();
                        filterCodigoField.clear();
                        filterLoteField.clear();
                    });
                }));

            }));
            principal.add(tblMaterias.build());
        }));
    }


    private void atualizaGramaturaLote(VSdMateriasLote item, FormFieldText field, String oldValue) {
        try {
            new NativeDAO().runNativeQueryUpdate(
                    "update mat_lote_001\n" +
                            "   set gramatura = " + field.value.get().replace(',', '.') + "\n" +
                            " where codigo =  '" + item.getCodigo() + "'\n" +
                            "   and cor = '" + item.getCor().getCor() + "'\n" +
                            "   and lote = '" + item.getLote() + "'"
            );
            SysLogger.addSysDelizLog("Manutenção de Lote", TipoAcao.EDITAR, item.getLote(),
                    "Alteração na GRAMATURA do Rolo -- Código: " + item.getCodigo() +
                            " -- Cor: " + item.getCor() + " -- Lote: " + item.getLote() +
                            " --- Valor gramatura antiga: " + oldValue + " Valor gramatura nova: " + field.value.get() + " --- ");
            field.editable(false);
            MessageBox.create(m -> {
                m.message("Gramatura atualizada com Sucesso!");
                m.type(MessageBox.TypeMessageBox.CONFIRM);
                m.position(Pos.TOP_RIGHT);
                m.notification();
            });
        } catch (SQLException e) {
            e.printStackTrace();
            ExceptionBox.build(m -> {
                m.exception(e);
                m.showAndWait();
            });
        }
    }

    private void atualizaTonalidadeLote(VSdMateriasLote item, FormFieldText field, String oldValue) {
        try {
            new NativeDAO().runNativeQueryUpdate(
                    "update mat_lote_001\n" +
                            "   set tonalidade = '" + field.value.get() + "'\n" +
                            " where codigo =  '" + item.getCodigo() + "'\n" +
                            "   and cor = '" + item.getCor().getCor() + "'\n" +
                            "   and lote = '" + item.getLote() + "'"
            );
            SysLogger.addSysDelizLog("Manutenção de Lote", TipoAcao.EDITAR, item.getLote(),
                    "Alteração na TONALIDADE do Rolo -- Código: " + item.getCodigo() +
                            " -- Cor: " + item.getCor() + " -- Lote: " + item.getLote() +
                            " --- Valor tonalidade antiga: " + oldValue + " Valor tonalidade nova: " + field.value.get() + " --- ");
            field.editable(false);
            MessageBox.create(m -> {
                m.message("Tonalidade atualizada com Sucesso!");
                m.type(MessageBox.TypeMessageBox.CONFIRM);
                m.position(Pos.TOP_RIGHT);
                m.notification();
            });
        } catch (SQLException e) {
            e.printStackTrace();
            ExceptionBox.build(m -> {
                m.exception(e);
                m.showAndWait();
            });
        }
    }

    private void atualizaSequenciaLote(VSdMateriasLote item, FormFieldText field, String oldValue) {
        try {
            new NativeDAO().runNativeQueryUpdate(
                    "update mat_lote_001 \n" +
                            "   set sequencia = '" + field.value.get() + "'\n" +
                            " where codigo =  '" + item.getCodigo() + "'\n" +
                            "   and cor = '" + item.getCor().getCor() + "'\n" +
                            "   and lote = '" + item.getLote() + "'"
            );
            SysLogger.addSysDelizLog("Manutenção de Lote", TipoAcao.EDITAR, item.getLote(),
                    "Alteração na SEQUÊNCIA do Rolo -- Código: " + item.getCodigo() +
                            " -- Cor: " + item.getCor() + " -- Lote: " + item.getLote() +
                            " --- Valor sequência antiga: " + oldValue + " Valor sequência nova: " + field.value.get() + " --- ");
            field.editable(false);
            MessageBox.create(m -> {
                m.message("Sequência atualizada com Sucesso!");
                m.type(MessageBox.TypeMessageBox.CONFIRM);
                m.position(Pos.TOP_RIGHT);
                m.notification();
            });
        } catch (SQLException e) {
            e.printStackTrace();
            ExceptionBox.build(m -> {
                m.exception(e);
                m.showAndWait();
            });
        }
    }

    private void atualizaPartidaLote(VSdMateriasLote item, FormFieldText field, String oldValue) {
        try {
            new NativeDAO().runNativeQueryUpdate(
                    "update mat_lote_001\n" +
                            "   set partida = '" + field.value.get() + "'\n" +
                            " where codigo =  '" + item.getCodigo() + "'\n" +
                            "   and cor = '" + item.getCor().getCor() + "'\n" +
                            "   and lote = '" + item.getLote() + "'"
            );
            SysLogger.addSysDelizLog("Manutenção de Lote", TipoAcao.EDITAR, item.getLote(),
                    "Alteração na PARTIDA do Rolo -- Código: " + item.getCodigo() +
                            " -- Cor: " + item.getCor() + " -- Lote: " + item.getLote() +
                            " --- Valor partida antiga: " + oldValue + " Valor partida nova: " + field.value.get() + " --- ");
            field.editable(false);
            MessageBox.create(m -> {
                m.message("Partida atualizada com Sucesso!");
                m.type(MessageBox.TypeMessageBox.CONFIRM);
                m.position(Pos.TOP_RIGHT);
                m.notification();
            });
        } catch (SQLException e) {
            e.printStackTrace();
            ExceptionBox.build(m -> {
                m.exception(e);
                m.showAndWait();
            });
        }
    }

    private void atualizaLarguraLote(VSdMateriasLote item, FormFieldText field, String oldValue) {
        try {
            new NativeDAO().runNativeQueryUpdate(
                    "update mat_iten_001\n" +
                            "   set largura = " + field.value.get().replace(',', '.') + "\n" +
                            " where codigo =  '" + item.getCodigo() + "'\n" +
                            "   and cor = '" + item.getCor().getCor() + "'\n" +
                            "   and lote = '" + item.getLote() + "'"
            );
            SysLogger.addSysDelizLog("Manutenção de Lote", TipoAcao.EDITAR, item.getLote(),
                    "Alteração na LARGURA do Rolo -- Código: " + item.getCodigo() +
                            " -- Cor: " + item.getCor() + " -- Lote: " + item.getLote() +
                            " --- Valor largura antiga: " + oldValue + " Valor largura nova: " + field.value.get() + " --- ");
            field.editable(false);
            MessageBox.create(m -> {
                m.message("Largura atualizada com Sucesso!");
                m.type(MessageBox.TypeMessageBox.CONFIRM);
                m.position(Pos.TOP_RIGHT);
                m.notification();
            });
        } catch (SQLException e) {
            e.printStackTrace();
            ExceptionBox.build(m -> {
                m.exception(e);
                m.showAndWait();
            });
        }
    }

    private void atualizaObsLote(VSdMateriasLote item, FormFieldText field, String oldValue) {
        try {
            new NativeDAO().runNativeQueryUpdate(
                    "update mat_lote_001\n" +
                            "   set obs = '" + field.value.get() + "'\n" +
                            " where codigo =  '" + item.getCodigo() + "'\n" +
                            "   and cor = '" + item.getCor().getCor() + "'\n" +
                            "   and lote = '" + item.getLote() + "'"
            );
            SysLogger.addSysDelizLog("Manutenção de Lote", TipoAcao.EDITAR, item.getLote(),
                    "Alteração na OBSERVAÇÃO do Rolo -- Código: " + item.getCodigo() +
                            " -- Cor: " + item.getCor() + " -- Lote: " + item.getLote() +
                            " --- OBSERVAÇÃO antiga: " + oldValue + " OBSERVAÇÃO nova: " + field.value.get() + " --- ");
            field.editable(false);
            MessageBox.create(m -> {
                m.message("Observação atualizada com Sucesso!");
                m.type(MessageBox.TypeMessageBox.CONFIRM);
                m.position(Pos.TOP_RIGHT);
                m.notification();
            });
        } catch (SQLException e) {
            e.printStackTrace();
            ExceptionBox.build(m -> {
                m.exception(e);
                m.showAndWait();
            });
        }
    }
}
