package sysdeliz2.views.contabilidade;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import sysdeliz2.controllers.views.contabilidade.CadastroNaturezaController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.ti.*;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.hibernate.DefaultFilter;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CadastroNaturezaView extends CadastroNaturezaController {

    // <editor-fold defaultstate="collapsed" desc="Bean">
    private final ListProperty<Natureza> naturezasBean = new SimpleListProperty<>();
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Table">
    private final FormTableView<Natureza> tblNaturezas = FormTableView.create(Natureza.class, table -> {
        table.title("Naturezas");
        table.expanded();
        table.items.bind(naturezasBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Ações");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(115.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<Natureza, Natureza>, ObservableValue<Natureza>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<Natureza, Natureza>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnVisualizar = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIconButton(ImageUtils.ButtonIcon.FIND, ImageUtils.IconSize._16));
                                btn.tooltip("Visualizar Cadastro");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("info");
                            });
                            final Button btnEditar = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EDIT, ImageUtils.IconSize._16));
                                btn.tooltip("Editar Cadastro");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("warning");
                            });
                            final Button btnExcluir = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.TRASH, ImageUtils.IconSize._16));
                                btn.tooltip("Excluir Cadastro");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("danger");
                            });

                            @Override
                            protected void updateItem(Natureza item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {

                                    btnVisualizar.setOnAction(evt -> {
                                        abrirCadastro(item);
                                    });
                                    btnEditar.setOnAction(evt -> {
                                        editarCadastro(item);
                                    });
                                    btnExcluir.setOnAction(evt -> {
                                        excluirCadastro(item);
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnVisualizar, btnEditar, btnExcluir);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Natureza");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<Natureza, Natureza>, ObservableValue<Natureza>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNatureza()));
                }).build(), /**/
                FormTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(400);
                    cln.value((Callback<TableColumn.CellDataFeatures<Natureza, Natureza>, ObservableValue<Natureza>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDescricao()));
                }).build(), /**/
                FormTableColumn.create(cln -> {
                    cln.title("Ativo");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<Natureza, Natureza>, ObservableValue<Natureza>>) param -> new ReadOnlyObjectWrapper(param.getValue().isAtivo() ? "Sim" : "Não"));
                }).build(), /**/
                FormTableColumn.create(cln -> {
                    cln.title("Alíq. ICMS");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<Natureza, Natureza>, ObservableValue<Natureza>>) param -> new ReadOnlyObjectWrapper(param.getValue().getAliquota() == null ? "0" : param.getValue().getAliquota().toString()));
                }).build(), /**/
                FormTableColumn.create(cln -> {
                    cln.title("Alíq. COFINS");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<Natureza, Natureza>, ObservableValue<Natureza>>) param -> new ReadOnlyObjectWrapper(param.getValue().getAliqCofins() == null ? "0" : param.getValue().getAliqCofins().toString()));
                }).build(), /**/
                FormTableColumn.create(cln -> {
                    cln.title("Alíq. PIS");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<Natureza, Natureza>, ObservableValue<Natureza>>) param -> new ReadOnlyObjectWrapper(param.getValue().getAliqPis() == null ? "0" : param.getValue().getAliqPis().toString()));
                }).build(), /**/
                FormTableColumn.create(cln -> {
                    cln.title("ICMS");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<Natureza, Natureza>, ObservableValue<Natureza>>) param -> new ReadOnlyObjectWrapper(param.getValue().getIcms()));
                }).build(), /**/
                FormTableColumn.create(cln -> {
                    cln.title("Pis/Cofins");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<Natureza, Natureza>, ObservableValue<Natureza>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPisCofins()));
                }).build(), /**/
                FormTableColumn.create(cln -> {
                    cln.title("IPI");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<Natureza, Natureza>, ObservableValue<Natureza>>) param -> new ReadOnlyObjectWrapper(param.getValue().getIpi()));
                }).build(), /**/
                FormTableColumn.create(cln -> {
                    cln.title("Sai/Ent");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<Natureza, Natureza>, ObservableValue<Natureza>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTpBase()));
                }).build(), /**/
                FormTableColumn.create(cln -> {
                    cln.title("Tipo");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<Natureza, Natureza>, ObservableValue<Natureza>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTipo()));
                }).build(), /**/
                FormTableColumn.create(cln -> {
                    cln.title("Contabilização");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<Natureza, Natureza>, ObservableValue<Natureza>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNatCont()));
                }).build(), /**/
                FormTableColumn.create(cln -> {
                    cln.title("Gera Dup");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<Natureza, Natureza>, ObservableValue<Natureza>>) param -> new ReadOnlyObjectWrapper(param.getValue().isDupli() ? "Sim" : "Não"));
                }).build() /**/

        );
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="VBox">
    private final VBox boxListagem = (VBox) super.tabs.getTabs().get(0).getContent();
    private final VBox boxManutencao = (VBox) super.tabs.getTabs().get(1).getContent();
    private final FormNavegation<Natureza> navegation = FormNavegation.create(nav -> {
        nav.withNavegation(tblNaturezas);
        nav.withActions(true, true, true);
        nav.withReturn(evt -> super.tabs.getSelectionModel().select(0));
        nav.btnAddRegister(evt -> novoCadastro());
        nav.btnDeleteRegister(evt -> excluirCadastro((Natureza) nav.selectedItem.get()));
        nav.btnEditRegister(evt -> {
            editarCadastro((Natureza) nav.selectedItem.get());
        });
        nav.btnSave(evt -> {
            salvarCadastro();
        });
        nav.btnCancel(evt -> cancelarCadastro());
        nav.selectedItem.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                limparCampos();
                carregaDados((Natureza) newValue);
            }
        });
    });

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Filter">
    private final FormFieldSingleFind<Natureza> codigoNaturezaFilter = FormFieldSingleFind.create(Natureza.class, field -> {
        field.title("Natureza");
    });

    private final FormFieldComboBox<String> tipoFilter = FormFieldComboBox.create(String.class, field -> {
        field.title("Tipo");
        List<String> tipos = Arrays.stream(Natureza.TipoNatureza.values()).map(it -> it.name() + " - " + it.getDesc()).collect(Collectors.toList());
        tipos.set(0, "T - Todos");
        field.items(FXCollections.observableArrayList(tipos));
        field.select(0);
    });

    private final FormFieldComboBox<String> entradaSaidaFilter = FormFieldComboBox.create(String.class, field -> {
        field.title("E/S");
        field.items(FXCollections.observableArrayList(Arrays.asList("0 - Ambos", "1 - Saída", "2 - Entrada")));
        field.select(0);
    });

    private final FormFieldToggle ativoFilter = FormFieldToggle.create(field -> {
        field.title("Ativo");
    });

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Fields">
    private final FormFieldText codigoField = FormFieldText.create(field -> {
        field.title("Código");
        field.editable.bind(navegation.inEdition);
    });

    private final FormFieldText cfopField = FormFieldText.create(field -> {
        field.title("CFOP");
        field.editable.bind(navegation.inEdition);
    });

    private final FormFieldText descricaoField = FormFieldText.create(field -> {
        field.title("Descrição");
        field.editable.bind(navegation.inEdition);
        field.expanded();
    });

    private final FormFieldComboBox<Natureza.TipoNatureza> tipoField = FormFieldComboBox.create(Natureza.TipoNatureza.class, field -> {
        field.title("Tipo");
        field.editable.bind(navegation.inEdition);
        field.items(FXCollections.observableArrayList(Arrays.stream(Natureza.TipoNatureza.values()).collect(Collectors.toList())));
        field.select(0);
    });

    private final FormFieldComboBox<String> entradaSaidaField = FormFieldComboBox.create(String.class, field -> {
        field.title("S/E");
        field.editable.bind(navegation.inEdition);
        field.items(FXCollections.observableArrayList(Arrays.asList("1 - Saída", "2 - Entrada")));
        field.select(0);
    });

    private final FormFieldText codTnsField = FormFieldText.create(field -> {
        field.title("CODTNS");
        field.editable.bind(navegation.inEdition);
        field.expanded();
    });

    private final FormFieldToggle geraDupicataToggle = FormFieldToggle.create(field -> {
        field.title("Gera Duplicata");
        field.editable.bind(navegation.inEdition);
    });

    private final FormFieldSingleFind<TabTri> classTribIPIField = FormFieldSingleFind.create(TabTri.class, field -> {
        field.title("Class. Trib. (IPI)");
        field.code.setPrefWidth(40);
        field.root.setPrefWidth(600);
        field.autoFind.set(false);
        field.defaults.add(new DefaultFilter("IP", "id.tipo"));
        field.editable.bind(navegation.inEdition);
    });

    private final FormFieldSingleFind<TabTri> classTribCOFINSField = FormFieldSingleFind.create(TabTri.class, field -> {
        field.title("Class. Trib. (COFINS)");
        field.code.setPrefWidth(40);
        field.root.setPrefWidth(600);
        field.autoFind.set(false);
        field.defaults.add(new DefaultFilter("CO", "id.tipo"));
        field.editable.bind(navegation.inEdition);
    });

    private final FormFieldSingleFind<NatConta> contabilizacaoField = FormFieldSingleFind.create(NatConta.class, field -> {
        field.title("Contabilização");
        field.code.setPrefWidth(40);
        field.root.setPrefWidth(600);
        field.editable.bind(navegation.inEdition);
        field.autoFind.set(false);
    });

    private final FormFieldTextArea aplicacaoField = FormFieldTextArea.create(field -> {
        field.title("Aplicação");
        field.width(600);
        field.height(268);
        field.editable.bind(navegation.inEdition);
    });

    private final FormFieldText aliqIcmsField = FormFieldText.create(field -> {
        field.title("Alíq. ICMS");
        field.editable.bind(navegation.inEdition);
        field.expanded();
    });

    private final FormFieldText aliqPisField = FormFieldText.create(field -> {
        field.title("Alíq. PIS");
        field.editable.bind(navegation.inEdition);
        field.expanded();
    });

    private final FormFieldText aliqCofinsField = FormFieldText.create(field -> {
        field.title("Alíq. COFINS");
        field.editable.bind(navegation.inEdition);
        field.expanded();
    });

    private final FormFieldSingleFind<TabFis> classFiscalNCMField = FormFieldSingleFind.create(TabFis.class, field -> {
        field.title("Class. Fiscal (NCM)");
        field.code.setPrefWidth(50);
        field.root.setPrefWidth(580);
        field.editable.bind(navegation.inEdition);
    });

    private final FormFieldSingleFind<TabTri> classTribIcmsField = FormFieldSingleFind.create(TabTri.class, field -> {
        field.title("Class. Trib. (ICMS)");
        field.code.setPrefWidth(50);
        field.root.setPrefWidth(580);
        field.defaults.add(new DefaultFilter("IC", "id.tipo"));
        field.autoFind.set(false);
        field.editable.bind(navegation.inEdition);
    });

    private final FormFieldSingleFind<TabTri> classTribPisField = FormFieldSingleFind.create(TabTri.class, field -> {
        field.title("Class. Trib. (PIS)");
        field.code.setPrefWidth(50);
        field.root.setPrefWidth(580);
        field.defaults.add(new DefaultFilter("PI", "id.tipo"));
        field.autoFind.set(false);
        field.editable.bind(navegation.inEdition);
    });

    private final FormFieldSingleFind<TabSped> codigoSpedField = FormFieldSingleFind.create(TabSped.class, field -> {
        field.title("Código SPED");
        field.code.setPrefWidth(50);
        field.root.setPrefWidth(455);
        field.editable.bind(navegation.inEdition);
    });

    private final FormFieldText codAjusteField = FormFieldText.create(field -> {
        field.title("Cod. Ajuste");
        field.editable.bind(navegation.inEdition);
    });

    private final FormFieldTextArea mensagemNfField = FormFieldTextArea.create(field -> {
        field.title("Mensagem NF");
        field.width(600);
        field.height(268);
        field.editable.bind(navegation.inEdition);
    });

    private final CheckBox ativoCheckBox = new CheckBox("Ativo");

    private final CheckBox regraTibutariaCheckBox = new CheckBox("Regra Tibutária");

    private final CheckBox consumidorFinalCheckBox = new CheckBox("Consumidor Final");

    private final CheckBox movimentaEstoqueCheckBox = new CheckBox("Movimenta Estoque");

    private final ToggleGroup groupPisCofins = new ToggleGroup();

    private final RadioButton tributaPisCofinsRadio = new RadioButton("Tributa PIS/COFINS");

    private final RadioButton isentaPisCofinsRadio = new RadioButton("Isenta PIS/COFINS");

    private final CheckBox somaIpibaseCalculoPisCofinsCheckBox = new CheckBox("Soma IPI na Base de Cálculo");

    private final CheckBox somaDespesasBaseCalculoPisCofinsCheckBox = new CheckBox("Soma Despesas na Base Cálc.");

    private final CheckBox subtraiICMSBaseCalculoPisCofinsCheckBox = new CheckBox("Subtrai ICMS na Base de Cálc.");

    private final ToggleGroup groupIcms = new ToggleGroup();

    private final RadioButton baseIcmsRadio = new RadioButton("Base ICMS");
    private final RadioButton isentaIcmsRadio = new RadioButton("Isentas ICMS");
    private final RadioButton outrasIcmsRadio = new RadioButton("Outras ICMS");

    private final CheckBox somaIpibaseCalculoIcmsCheckBox = new CheckBox("Soma IPI na Base de Cálculo");

    private final CheckBox somaFreteBaseCalculoIcmsCheckBox = new CheckBox("Soma Frete na Base de Cálculo");

    private final CheckBox somaDespesaBaseCalculoIcmsCheckBox = new CheckBox("Soma Despesa na Base de Cálc.");

    private final ToggleGroup groupIpi = new ToggleGroup();

    private final RadioButton baseIpiRadio = new RadioButton("Base IPI");
    private final RadioButton isentaIpiRadio = new RadioButton("Isentas IPI");
    private final RadioButton outrasIpiRadio = new RadioButton("Outras IPI");
    // </editor-fold>

    public CadastroNaturezaView() {
        super("Cadastro de Natureza", ImageUtils.getImage(ImageUtils.Icon.GESTAO_PRODUCAO), new String[]{"Listagem", "Manutenção"});
        initListagem();
        initManutencao();
    }

    private void initListagem() {
        boxListagem.getChildren().add(FormBox.create(principal -> {
            principal.vertical();
            principal.expanded();
            principal.add(FormBox.create(boxHeader -> {
                boxHeader.horizontal();
                boxHeader.add(FormBox.create(boxTitledFilter -> {
                    boxTitledFilter.vertical();
                    boxTitledFilter.add(FormTitledPane.create(filter -> {
                        filter.filter();
                        filter.add(FormBox.create(boxFilter -> {
                            boxFilter.vertical();
                            boxFilter.add(FormBox.create(linha1 -> {
                                linha1.horizontal();
                                linha1.add(codigoNaturezaFilter.build(), tipoFilter.build(), entradaSaidaFilter.build(), ativoFilter.build());
                            }));
                        }));
                        filter.find.setOnAction(evt -> {
                            new RunAsyncWithOverlay(this).exec(task -> {
                                buscarNaturezas(codigoNaturezaFilter.value.getValue(), tipoFilter.value.getValue(), entradaSaidaFilter.value.getValue(), ativoFilter.value.getValue());
                                return ReturnAsync.OK.value;
                            }).addTaskEndNotification(taskReturn -> {
                                if (taskReturn.equals(ReturnAsync.OK.value)) {
                                    naturezasBean.set(FXCollections.observableArrayList(naturezas));
                                }
                            });
                        });
                        filter.clean.setOnAction(evt -> {
                            codigoNaturezaFilter.clear();
                            tipoFilter.select(0);
                            entradaSaidaFilter.select(0);
                        });
                    }));
                }));
            }));
            principal.add(FormBox.create(boxCenter -> {
                boxCenter.horizontal();
                boxCenter.expanded();
                boxCenter.add(tblNaturezas.build());
            }));
            principal.add(FormBox.create(boxFooter -> {
                boxFooter.horizontal();
                boxFooter.add();
            }));
        }));
    }

    private void initManutencao() {

        tributaPisCofinsRadio.setSelected(true);
        tributaPisCofinsRadio.setToggleGroup(groupPisCofins);
        isentaPisCofinsRadio.setToggleGroup(groupPisCofins);

        baseIcmsRadio.setSelected(true);
        baseIcmsRadio.setToggleGroup(groupIcms);
        isentaIcmsRadio.setToggleGroup(groupIcms);
        outrasIcmsRadio.setToggleGroup(groupIcms);

        baseIpiRadio.setSelected(true);
        baseIpiRadio.setToggleGroup(groupIpi);
        isentaIpiRadio.setToggleGroup(groupIpi);
        outrasIpiRadio.setToggleGroup(groupIpi);

        boxManutencao.getChildren().add(navegation);

        boxManutencao.getChildren().add(FormBox.create(principal -> {
            principal.horizontal();
            principal.add(FormBox.create(col1 -> {
                col1.vertical();
                col1.width(600);
                col1.add(FormBox.create(l1 -> {
                    l1.horizontal();
                    l1.add(codigoField.build(), cfopField.build());
                }));
                col1.add(FormBox.create(l2 -> {
                    l2.horizontal();
                    l2.add(descricaoField.build());
                }));
                col1.add(FormBox.create(l3 -> {
                    l3.horizontal();
                    l3.add(tipoField.build(), entradaSaidaField.build(), geraDupicataToggle.build(), codTnsField.build());
                }));
                col1.add(FormBox.create(l4 -> {
                    l4.horizontal();
                    l4.add(classTribIPIField.build());
                }));
                col1.add(FormBox.create(l5 -> {
                    l5.horizontal();
                    l5.add(classTribCOFINSField.build());
                }));
                col1.add(FormBox.create(l6 -> {
                    l6.horizontal();
                    l6.add(contabilizacaoField.build());
                }));
                col1.add(FormBox.create(l7 -> {
                    l7.horizontal();
                    l7.add(aplicacaoField.build());
                }));

            }));
            principal.add(FormBox.create(col2 -> {
                col2.vertical();
                col2.width(600);
                col2.setPadding(new Insets(53, 5, 5, 5));
                col2.add(FormBox.create(l1 -> {
                    l1.horizontal();
                    l1.add(aliqIcmsField.build(), aliqPisField.build(), aliqCofinsField.build());
                }));
                col2.add(FormBox.create(l2 -> {
                    l2.horizontal();
                    l2.add(classFiscalNCMField.build());
                }));
                col2.add(FormBox.create(l3 -> {
                    l3.horizontal();
                    l3.add(classTribIcmsField.build());
                }));
                col2.add(FormBox.create(l3 -> {
                    l3.horizontal();
                    l3.add(classTribPisField.build());
                }));
                col2.add(FormBox.create(l3 -> {
                    l3.horizontal();
                    l3.add(codigoSpedField.build(), codAjusteField.build());
                }));
                col2.add(FormBox.create(l3 -> {
                    l3.horizontal();
                    l3.add(mensagemNfField.build());
                }));
            }));
            principal.add(FormBox.create(col3 -> {
                col3.vertical();
                col3.width(300);
                col3.add(FormBox.create(box1 -> {
                    box1.vertical();
                    box1.border();
                    box1.disable.bind(navegation.inEdition.not());
                    box1.add(ativoCheckBox, regraTibutariaCheckBox, consumidorFinalCheckBox, movimentaEstoqueCheckBox);
                }));
                col3.add(FormBox.create(box2 -> {
                    box2.vertical();
                    box2.title("Trib. PIS / COFINS / II");
                    box2.disable.bind(navegation.inEdition.not());
                    box2.add(tributaPisCofinsRadio, isentaPisCofinsRadio, somaIpibaseCalculoPisCofinsCheckBox, somaDespesasBaseCalculoPisCofinsCheckBox, subtraiICMSBaseCalculoPisCofinsCheckBox);
                }));
                col3.add(FormBox.create(box3 -> {
                    box3.vertical();
                    box3.title("Trib. ICMS");
                    box3.disable.bind(navegation.inEdition.not());
                    box3.add(baseIcmsRadio, isentaIcmsRadio, outrasIcmsRadio, somaIpibaseCalculoIcmsCheckBox, somaFreteBaseCalculoIcmsCheckBox, somaDespesaBaseCalculoIcmsCheckBox);
                }));
                col3.add(FormBox.create(box4 -> {
                    box4.vertical();
                    box4.title("Trib. IPI");
                    box4.disable.bind(navegation.inEdition.not());
                    box4.add(baseIpiRadio, isentaIpiRadio, outrasIpiRadio);
                }));
            }));
        }));
    }

    private void carregaDados(Natureza newValue) {
        codigoField.value.setValue(newValue.getNatureza());
        cfopField.value.setValue(newValue.getCfop());
        descricaoField.value.setValue(newValue.descricaoProperty().getValueSafe());
        if (newValue.getTipo() != null) tipoField.select(Natureza.TipoNatureza.valueOf(newValue.getTipo()));
        if (newValue.getTpBase() != null) entradaSaidaField.select(newValue.getTpBase().equals("1") ? "1 - Saída" : "2 - Entrada");
        geraDupicataToggle.value.setValue(newValue.isDupli());
        codTnsField.value.setValue(newValue.getCodTns());
        classTribIPIField.value.setValue(new FluentDao().selectFrom(TabTri.class).where(it -> it.equal("id.codigo", newValue.getTribIpi()).equal("id.tipo", "IP")).singleResult());
        classTribCOFINSField.value.setValue(new FluentDao().selectFrom(TabTri.class).where(it -> it.equal("id.codigo", newValue.getTribCofins()).equal("id.tipo", "CO")).singleResult());
        contabilizacaoField.value.setValue(new FluentDao().selectFrom(NatConta.class).where(it -> it.equal("id.codigo", newValue.getNatCont())).findFirst());
        aplicacaoField.value.setValue(newValue.getComplemento());
        aliqIcmsField.value.setValue(newValue.getAliquota() == null ? "" : newValue.getAliquota().toString());
         aliqPisField.value.setValue(newValue.getAliqPis() == null ? "" : newValue.getAliqPis().toString());
        aliqCofinsField.value.setValue(newValue.getAliqCofins() == null ? "" : newValue.getAliqCofins().toString());
        classFiscalNCMField.value.setValue(new FluentDao().selectFrom(TabFis.class).where(it -> it.equal("codigo", newValue.getClafis())).singleResult());
        classTribIcmsField.value.setValue(new FluentDao().selectFrom(TabTri.class).where(it -> it.equal("id.codigo", newValue.getClatrib()).equal("id.tipo", "IC")).singleResult());
        classTribPisField.value.setValue(new FluentDao().selectFrom(TabTri.class).where(it -> it.equal("id.codigo", newValue.getTribPis()).equal("id.tipo", "PI")).singleResult());
        codigoSpedField.value.setValue(new FluentDao().selectFrom(TabSped.class).where(it -> it.equal("codigo", newValue.getCodsped())).singleResult());
        codAjusteField.value.setValue(newValue.getCodAjSped());
        mensagemNfField.value.setValue(newValue.getMensagem());
        ativoCheckBox.setSelected(newValue.isAtivo());
        regraTibutariaCheckBox.setSelected(newValue.getSubstTrib() != null && newValue.getSubstTrib().equals("S"));
        consumidorFinalCheckBox.setSelected(newValue.getConsFinal() != null && newValue.getConsFinal().equals("S"));
        movimentaEstoqueCheckBox.setSelected(newValue.getMovEst() != null && newValue.getMovEst().equals("S"));
        if (newValue.getPisCofins() != null && newValue.getPisCofins().equals("S")) tributaPisCofinsRadio.setSelected(true);
        else isentaPisCofinsRadio.setSelected(true);
        if (newValue.getCompBasePico() != null) {
            somaIpibaseCalculoPisCofinsCheckBox.setSelected(newValue.getCompBasePico().charAt(0) == 'S');
            somaDespesasBaseCalculoPisCofinsCheckBox.setSelected(newValue.getCompBasePico().charAt(1) == 'S');
            subtraiICMSBaseCalculoPisCofinsCheckBox.setSelected(newValue.getCompBasePico().charAt(2) == 'S');
        }
        if (newValue.getIcms() != null) {
            switch (newValue.getIcms()) {
                case "1":
                    baseIcmsRadio.setSelected(true);
                    break;
                case "2":
                    isentaIcmsRadio.setSelected(true);
                    break;
                case "3":
                    outrasIcmsRadio.setSelected(true);
                    break;
            }
        }

        if(newValue.getCompBaseIcms() != null) {
            somaIpibaseCalculoIcmsCheckBox.setSelected(newValue.getCompBaseIcms().charAt(0) == 'S');
            somaFreteBaseCalculoIcmsCheckBox.setSelected(newValue.getCompBaseIcms().charAt(1) == 'S');
            somaDespesaBaseCalculoIcmsCheckBox.setSelected(newValue.getCompBaseIcms().charAt(2) == 'S');
        }
        if (newValue.getIcms() != null) {
            switch (newValue.getIpi()) {
                case "1":
                    baseIpiRadio.setSelected(true);
                    break;
                case "2":
                    isentaIpiRadio.setSelected(true);
                    break;
                case "3":
                    outrasIpiRadio.setSelected(true);
                    break;
            }
        }
    }

    private void getValoresCampos() {
        Natureza natureza = navegation.selectedItem.get();
        natureza.setNatureza(codigoField.value.getValue());
        natureza.setAliquota(new BigDecimal(aliqIcmsField.value.getValue().replace(",", ".")));
        natureza.setTpBase(entradaSaidaField.value.getValue() == null ? null : String.valueOf(entradaSaidaField.value.getValue().charAt(0)));
        natureza.setDupli(geraDupicataToggle.value.getValue());
        natureza.setDescricao(descricaoField.value.getValue());
        natureza.setIpi(baseIpiRadio.isSelected() ? "1" : isentaIpiRadio.isSelected() ? "2" : "3");
        natureza.setPisCofins(tributaPisCofinsRadio.isSelected() ? "S" : "N");
        natureza.setIcms(baseIcmsRadio.isSelected() ? "1" : isentaIcmsRadio.isSelected() ? "2" : "3");
        natureza.setClafis(classFiscalNCMField.value.getValue() == null ? null : classFiscalNCMField.value.getValue().getCodigo());
        natureza.setClatrib(classTribIcmsField.value.getValue() == null ? null : classTribIcmsField.value.getValue().getId().getCodigo());
        natureza.setMensagem(mensagemNfField.value.getValue());
        natureza.setTipo(tipoField.value.getValue() == null ? null : tipoField.value.getValue().name());
        natureza.setAtivo(ativoCheckBox.isSelected());
        natureza.setNatCont(contabilizacaoField.value.getValue() == null ? null : contabilizacaoField.value.getValue().getId().getCodigo());
        natureza.setSubstTrib(regraTibutariaCheckBox.isSelected() ? "S" : "N");
        natureza.setComplemento(aplicacaoField.value.getValue());
        natureza.setTribIpi(classTribIPIField.value.getValue() == null ? null : classTribIPIField.value.getValue().getId().getCodigo());
        natureza.setTribCofins(classTribCOFINSField.value.getValue() == null ? null : classTribCOFINSField.value.getValue().getId().getCodigo());
        natureza.setAliqPis(new BigDecimal(aliqPisField.value.getValue().replace(",", ".")));
        natureza.setAliqCofins(new BigDecimal(aliqCofinsField.value.getValue().replace(",", ".")));
        natureza.setCodsped(codigoSpedField.value.getValue() == null ? null : codigoSpedField.value.getValue().getCodigo());
        natureza.setCfop(cfopField.value.getValue());

        char[] icms = {'N', 'N', 'N'};
        if (somaIpibaseCalculoIcmsCheckBox.isSelected()) icms[0] = 'S';
        if (somaFreteBaseCalculoIcmsCheckBox.isSelected()) icms[1] = 'S';
        if (somaDespesaBaseCalculoIcmsCheckBox.isSelected()) icms[2] = 'S';
        natureza.setCompBaseIcms(String.valueOf(icms));

        char[] pisConfins = {'N', 'N', 'N'};
        if (somaIpibaseCalculoPisCofinsCheckBox.isSelected()) pisConfins[0] = 'S';
        if (somaDespesasBaseCalculoPisCofinsCheckBox.isSelected()) pisConfins[1] = 'S';
        if (subtraiICMSBaseCalculoPisCofinsCheckBox.isSelected()) pisConfins[2] = 'S';
        natureza.setCompBasePico(String.valueOf(pisConfins));

        natureza.setCodAjSped(codAjusteField.value.getValue());
        natureza.setConsFinal(consumidorFinalCheckBox.isSelected() ? "S" : "N");
        natureza.setMovEst(movimentaEstoqueCheckBox.isSelected() ? "S" : "N");
        natureza.setCodTns(codTnsField.value.getValue());
    }

    private void cancelarCadastro() {
        if (navegation.selectedItem == null) return;
        navegation.inEdition.set(false);
        limparCampos();
    }

    private void limparCampos() {
        codigoField.clear();
        cfopField.clear();
        descricaoField.clear();
        tipoField.clearSelection();
        entradaSaidaField.clearSelection();
        geraDupicataToggle.value.set(false);
        codTnsField.clear();
        classTribIPIField.clear();
        classTribCOFINSField.clear();
        contabilizacaoField.clear();
        aplicacaoField.clear();
        aliqIcmsField.value.setValue("0");
        aliqPisField.value.setValue("0");
        aliqCofinsField.value.setValue("0");
        classFiscalNCMField.clear();
        classTribIcmsField.clear();
        classTribPisField.clear();
        codigoSpedField.clear();
        codAjusteField.clear();
        mensagemNfField.clear();
        ativoCheckBox.setSelected(false);
        regraTibutariaCheckBox.setSelected(false);
        consumidorFinalCheckBox.setSelected(false);
        movimentaEstoqueCheckBox.setSelected(false);
        tributaPisCofinsRadio.setSelected(true);
        somaIpibaseCalculoPisCofinsCheckBox.setSelected(false);
        somaDespesasBaseCalculoPisCofinsCheckBox.setSelected(false);
        subtraiICMSBaseCalculoPisCofinsCheckBox.setSelected(false);
        baseIcmsRadio.setSelected(true);
        somaIpibaseCalculoIcmsCheckBox.setSelected(false);
        somaFreteBaseCalculoIcmsCheckBox.setSelected(false);
        somaDespesaBaseCalculoIcmsCheckBox.setSelected(false);
        baseIpiRadio.setSelected(true);

    }

    private void salvarCadastro() {
        if (navegation.selectedItem == null) return;
        getValoresCampos();
        Natureza natureza = navegation.selectedItem.get();
        if (natureza.getNatureza() == null || natureza.getNatureza().equals("")) {
            MessageBox.create(message -> {
                message.message("Código da Natureza não pode estar vazio.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }
        if (natureza.getDescricao() == null || natureza.getDescricao().equals("")) {
            MessageBox.create(message -> {
                message.message("Descrição da Natureza não pode estar vazia.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }
        if (natureza.getTpBase() == null || natureza.getTpBase().equals("")) {
            MessageBox.create(message -> {
                message.message("Defina se a natureza é de entrada ou saída.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }
        if (natureza.getTipo() == null || natureza.getTipo().equals("")) {
            MessageBox.create(message -> {
                message.message("Defina o tipo da natureza.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }
        natureza = new FluentDao().merge(natureza);
        navegation.selectedItem.set(natureza);
        navegation.inEdition.set(false);
        MessageBox.create(message -> {
            message.message("Natureza salva com sucesso!");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }

    private void editarCadastro(Natureza natureza) {
        if (natureza == null) return;
        navegation.selectedItem.set(natureza);
        tabs.getSelectionModel().select(1);
        limparCampos();
        carregaDados(natureza);
        navegation.inEdition.set(true);
    }

    private void excluirCadastro(Natureza natureza) {
        if (natureza == null) return;
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Tem certeza que deseja exluir?");
            message.showAndWait();
        }).value.get())) {
            new FluentDao().delete(natureza);
            naturezasBean.remove(natureza);
            limparCampos();
            MessageBox.create(message -> {
                message.message("Natureza excluída com sucesso!");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
        }
    }

    private void novoCadastro() {
        navegation.inEdition.set(true);
        tabs.getSelectionModel().select(1);
        limparCampos();
        navegation.selectedItem.set(new Natureza());
    }

    private void abrirCadastro(Natureza natureza) {
        navegation.selectedItem.set(natureza);
        tabs.getSelectionModel().select(1);
        limparCampos();
        carregaDados(natureza);
    }
}
