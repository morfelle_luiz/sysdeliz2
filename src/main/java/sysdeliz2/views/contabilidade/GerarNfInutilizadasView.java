package sysdeliz2.views.contabilidade;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import sysdeliz2.controllers.views.contabilidade.GerarNfInutilizadasController;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.view.VSdNotaInutilizadaHeader;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.FormBox;
import sysdeliz2.utils.gui.components.FormButton;
import sysdeliz2.utils.gui.components.FormTableColumn;
import sysdeliz2.utils.gui.components.FormTableView;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.sys.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

public class GerarNfInutilizadasView extends GerarNfInutilizadasController {

    // <editor-fold defaultstate="collapsed" desc="Bean">
    protected final ListProperty<VSdNotaInutilizadaHeader> notasBean = new SimpleListProperty<>(FXCollections.observableList(notasFiscais));
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Table">
    private final FormTableView<VSdNotaInutilizadaHeader> tblNotas = FormTableView.create(VSdNotaInutilizadaHeader.class, table -> {
        table.expanded();
        table.withoutHeader();
        table.items.bind(notasBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Número da Nota");
                    cln.width(200);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdNotaInutilizadaHeader, VSdNotaInutilizadaHeader>, ObservableValue<VSdNotaInutilizadaHeader>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumnfi()));
                    cln.alignment(Pos.CENTER);

                }).build(),/*Número da Nota*/
                FormTableColumn.create(cln -> {
                    cln.title("Código Empresa");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdNotaInutilizadaHeader, VSdNotaInutilizadaHeader>, ObservableValue<VSdNotaInutilizadaHeader>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodemp()));
                    cln.alignment(Pos.CENTER);

                }).build() /*Código Empresa*/
                , FormTableColumn.create(cln -> {
                    cln.title("Data de Emissão");
                    cln.width(200);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdNotaInutilizadaHeader, VSdNotaInutilizadaHeader>, ObservableValue<VSdNotaInutilizadaHeader>>) param -> new ReadOnlyObjectWrapper(StringUtils.toDateFormat(param.getValue().getDataEmi())));
                    cln.alignment(Pos.CENTER);

                }).build() /*Data de Entrada*/
                , FormTableColumn.create(cln -> {
                    cln.title("Data de Saída");
                    cln.width(200);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdNotaInutilizadaHeader, VSdNotaInutilizadaHeader>, ObservableValue<VSdNotaInutilizadaHeader>>) param -> new ReadOnlyObjectWrapper(StringUtils.toDateFormat(param.getValue().getDataSai())));
                    cln.alignment(Pos.CENTER);
                }).build() /*Data de Saída*/
        );
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Button">
    private Button exportExcelBtn = FormButton.create(btn -> {
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EXCEL, ImageUtils.IconSize._24));
        btn.setText("Exportar Excel");
        btn.addStyle("success");
        btn.setOnAction(evt -> {
            String path = selecionarPathArquivo(new FileChooser.ExtensionFilter("Arquivos do Excel (.xlsx)", "*.xlsx"));
            if (!path.equals("")) exportarExcel(path);
        });
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Querry">
    String querryInsereHeader = "insert into erp_nf_venda_001\n" +
            "  (CODEMP,\n" +
            "   CODFIL,\n" +
            "   USU_NUMREG,\n" +
            "   CNPJCGC,\n" +
            "   CODCLI,\n" +
            "   NUMNFI,\n" +
            "   NUMNFF,\n" +
            "   CODSNF,\n" +
            "   CODTNS,\n" +
            "   CODAGF,\n" +
            "   DATEMI,\n" +
            "   NOPOPE,\n" +
            "   OBSNFV,\n" +
            "   DATSAI,\n" +
            "   CODEDC,\n" +
            "   VLRCTB,\n" +
            "   VLRBIC,\n" +
            "   PERICM,\n" +
            "   VLRICM,\n" +
            "   VLROIC,\n" +
            "   VLRIIC,\n" +
            "   VLRBIP,\n" +
            "   VLRIPI,\n" +
            "   VLROIP,\n" +
            "   VLRIIP,\n" +
            "   VLRBSI,\n" +
            "   VLRSIC,\n" +
            "   CODCPG,\n" +
            "   VLRDSC,\n" +
            "   SITNFV,\n" +
            "   PERCRT,\n" +
            "   VLRBCF,\n" +
            "   VLRCFF,\n" +
            "   VLRBPF,\n" +
            "   VLRPIF,\n" +
            "   VLRFRE,\n" +
            "   CIFFOB,\n" +
            "   VLRSEG,\n" +
            "   VLRDAC,\n" +
            "   PESBRU,\n" +
            "   PESLIQ,\n" +
            "   CHVDOE,\n" +
            "   SITDOE,\n" +
            "   NUMPRT,\n" +
            "   DATAUT,\n" +
            "   NUMPRC,\n" +
            "   DATCAN,\n" +
            "   VLRDZF,\n" +
            "   ICMVFC,\n" +
            "   ICMVOR,\n" +
            "   ICMVDE,\n" +
            "   TIPNFS,\n" +
            "   IMPORT_SAP,\n" +
            "   CODSEL)\n" +
            "\n" +
            "values\n" +
            "  (%s,\n" +
            "   1,\n" +
            "   999983624,\n" +
            "   '13.592.916/0001-20',\n" +
            "   3829,\n" +
            "   %s,\n" +
            "   1,\n" +
            "   'EP',\n" +
            "   '5949',\n" +
            "   'GAF',\n" +
            "   '%s',\n" +
            "   '5949',\n" +
            "   null,\n" +
            "   '%s',\n" +
            "   '55',\n" +
            "   0,\n" +
            "   0,\n" +
            "   0,\n" +
            "   0,\n" +
            "   0,\n" +
            "   null,\n" +
            "   0,\n" +
            "   0,\n" +
            "   null,\n" +
            "   null,\n" +
            "   null,\n" +
            "   null,\n" +
            "   '001',\n" +
            "   0,\n" +
            "   '2',\n" +
            "   0,\n" +
            "   0,\n" +
            "   0,\n" +
            "   0,\n" +
            "   0,\n" +
            "   0,\n" +
            "   'C',\n" +
            "   0,\n" +
            "   0,\n" +
            "   0,\n" +
            "   0,\n" +
            "   null,\n" +
            "   '8',\n" +
            "   null,\n" +
            "   null,\n" +
            "   null,\n" +
            "   null,\n" +
            "   null,\n" +
            "   null,\n" +
            "   0,\n" +
            "   0,\n" +
            "   1,\n" +
            "   'N',\n" +
            "   '1')";

    String querryInsereItens = "insert into erp_nf_i_venda_001\n" +
            "  (CODEMP,\n" +
            "   CODFIL,\n" +
            "   USU_NUMREG,\n" +
            "   USU_NUMREGITEM,\n" +
            "   CODCLI,\n" +
            "   NUMNFI,\n" +
            "   NUMNFF,\n" +
            "   CODSNF,\n" +
            "   CODTNS,\n" +
            "   SEQINV,\n" +
            "   VLRFRE,\n" +
            "   VLRSEG,\n" +
            "   PERPIF,\n" +
            "   VLRBPF,\n" +
            "   VLRPIF,\n" +
            "   PERCFF,\n" +
            "   VLRBCF,\n" +
            "   VLRCFF,\n" +
            "   QTDBIP,\n" +
            "   ALIIPI,\n" +
            "   CODPRO,\n" +
            "   CODDER,\n" +
            "   CPLPRO,\n" +
            "   CLAFIS,\n" +
            "   QTDENT,\n" +
            "   UNIMED,\n" +
            "   VLRBIC,\n" +
            "   PERICM,\n" +
            "   VLRICM,\n" +
            "   VLRIIC,\n" +
            "   VLROIC,\n" +
            "   VLRBIP,\n" +
            "   PERIPI,\n" +
            "   VLRIPI,\n" +
            "   VLRIIP,\n" +
            "   VLROIP,\n" +
            "   VLRBSI,\n" +
            "   SEQIPV,\n" +
            "   VLRDSC,\n" +
            "   CODSTR,\n" +
            "   VLRCTB,\n" +
            "   SEQNFI,\n" +
            "   CSTIPI,\n" +
            "   CSTPIS,\n" +
            "   CSTCOF,\n" +
            "   PREUNI,\n" +
            "   CODCLF,\n" +
            "   VLRDZF,\n" +
            "   CODCES,\n" +
            "   ICMBDE,\n" +
            "   ICMAFC,\n" +
            "   ICMVFC,\n" +
            "   ICMAOR,\n" +
            "   ICMADE,\n" +
            "   ICMVOR,\n" +
            "   ICMVDE,\n" +
            "   VLRDAC,\n" +
            "   VLRSIC,\n" +
            "   FILNFC,\n" +
            "   CODFOR,\n" +
            "   SNFNFC,\n" +
            "   TNSNFC,\n" +
            "   NUMNFC,\n" +
            "   NFFNFC,\n" +
            "   SEQIPC,\n" +
            "   CTARED,\n" +
            "   IMPORT_SAP)\n" +
            "\n" +
            "values\n" +
            "  (%s,\n" +
            "   1,\n" +
            "   null,\n" +
            "   null,\n" +
            "   3829,\n" +
            "   %s,\n" +
            "   1,\n" +
            "   'EP',\n" +
            "   '5949',\n" +
            "   1,\n" +
            "   0,\n" +
            "   null,\n" +
            "   0,\n" +
            "   0,\n" +
            "   0,\n" +
            "   0,\n" +
            "   0,\n" +
            "   0,\n" +
            "   null,\n" +
            "   null,\n" +
            "   null,\n" +
            "   null,\n" +
            "   null,\n" +
            "   '6206.40.00',\n" +
            "   0,\n" +
            "   'PC',\n" +
            "   0,\n" +
            "   0,\n" +
            "   0,\n" +
            "   null,\n" +
            "   0,\n" +
            "   0,\n" +
            "   0,\n" +
            "   0,\n" +
            "   null,\n" +
            "   0,\n" +
            "   null,\n" +
            "   1,\n" +
            "   0,\n" +
            "   null,\n" +
            "   0,\n" +
            "   1,\n" +
            "   null,\n" +
            "   null,\n" +
            "   null,\n" +
            "   0,\n" +
            "   '677',\n" +
            "   null,\n" +
            "   null,\n" +
            "   null,\n" +
            "   null,\n" +
            "   null,\n" +
            "   null,\n" +
            "   null,\n" +
            "   0,\n" +
            "   0,\n" +
            "   0,\n" +
            "   null,\n" +
            "   null,\n" +
            "   null,\n" +
            "   null,\n" +
            "   null,\n" +
            "   null,\n" +
            "   null,\n" +
            "   null,\n" +
            "   null,\n" +
            "   'N')";
    // </editor-fold>

    public GerarNfInutilizadasView() {
        super("Gerar NF's Inutilizadas", ImageUtils.getImage(ImageUtils.Icon.NOTA_FISCAL));
        init();
    }

    private void init() {
        new RunAsyncWithOverlay(this).exec(task -> {
            try {
                gerarNotas();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                notasBean.set(FXCollections.observableList(notasFiscais));
                super.box.getChildren().add(FormBox.create(principal -> {
                    principal.vertical();
                    principal.expanded();
                    principal.add(tblNotas.build());
                    principal.add(FormBox.create(boxBotoes -> {
                        boxBotoes.horizontal();
                        boxBotoes.add(FormButton.create(btn -> {
                            btn.title("Atualizar");
                            btn.addStyle("info");
                            btn.setOnAction(event -> atualizarTabelas());
                        }));
                        boxBotoes.add(exportExcelBtn);
                    }));
                }));
            }
        });
    }

    private void atualizarTabelas() {
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Tem certeza que deseja alterar a data de todos os itens?");
            message.showAndWait();
        }).value.get())) {
            new RunAsyncWithOverlay(this).exec(task -> {
                for (VSdNotaInutilizadaHeader nota : notasFiscais) {
                    try {
                        new NativeDAO().runNativeQueryUpdate(String.format(querryInsereHeader, nota.getCodemp(), nota.getNumnfi().toString(), StringUtils.toDateFormat(nota.getDataEmi()), StringUtils.toDateFormat(nota.getDataSai())));
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    try {
                        new NativeDAO().runNativeQueryUpdate(String.format(querryInsereItens, nota.getCodemp(), nota.getNumnfi().toString()));
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    SysLogger.addSysDelizLog("Geração de NF's Inutilizadas", TipoAcao.EDITAR, nota.getNumnfi().toString(), "Alterações de data feita na Nota: " + nota.getNumnfi().toString());
                }
                notasFiscais.clear();
                tblNotas.refresh();
                return ReturnAsync.OK.value;
            }).addTaskEndNotification(taskReturn -> {
                if (taskReturn.equals(ReturnAsync.OK.value)) {
                    MessageBox.create(message -> {
                        message.message("Tabelas atualizadas com sucesso!");
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.position(Pos.TOP_RIGHT);
                        message.notification();
                    });
                }
            });

        }
    }

    private String selecionarPathArquivo(FileChooser.ExtensionFilter filter) {
        return GUIUtils.showSaveFileDialog(new ArrayList<>(Collections.singleton(filter)));
    }

    private void exportarExcel(String pathFile) {
        ExportUtils.ExcelExportMode excelExportMode = new ExportUtils().excel(pathFile);

        new RunAsyncWithOverlay(this).exec(task -> {
            try {
                excelExportMode.addSheet("NF's Inutilizadas", VSdNotaInutilizadaHeader.class, notasFiscais);
                excelExportMode.getWorkbook().getSheetAt(0).setDefaultColumnWidth(250);
                excelExportMode.export();
            } catch (IOException | IllegalAccessException e) {
                e.printStackTrace();
            }
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                MessageBox.create(message -> {
                    message.message("Excel exportado com sucesso!");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.notification();
                });
            }
        });
    }
}
