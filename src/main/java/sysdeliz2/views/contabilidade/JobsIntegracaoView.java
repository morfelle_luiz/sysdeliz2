package sysdeliz2.views.contabilidade;

import javafx.geometry.Pos;
import sysdeliz2.controllers.views.contabilidade.JobsIntegracaoController;
import sysdeliz2.utils.gui.components.FormBox;
import sysdeliz2.utils.gui.components.FormButton;
import sysdeliz2.utils.gui.components.FormFieldText;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.sys.ImageUtils;

import java.sql.SQLException;

public class JobsIntegracaoView extends JobsIntegracaoController {
    
    // <editor-fold defaultstate="collapsed" desc="Fields">
    private final FormFieldText estadoJobNFEField = FormFieldText.create(field -> {
        field.title("Estado NF Entrada");
        field.editable(false);
        field.value.bind(super.NFEStatusJob);
        field.value.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                if (newValue.equals("EM EXECUÇÃO")) {
                    field.addStyle("danger");
                } else {
                    field.removeStyle("danger");
                }
            }
        });
    });
    
    private final FormFieldText inicioUltimaRodadaNfeField = FormFieldText.create(field -> {
        field.title("Início Última Rodada NF Entrada");
        field.editable(false);
        field.value.bind(super.NFEinicioUltimoLog);
    });
    private final FormFieldText tempoUltimaRodadaNfeField = FormFieldText.create(field -> {
        field.title("Tempo Última Rodada NF Entrada");
        field.editable(false);
        field.value.bind(super.NFETempoUltimoLog);
    });
    private final FormFieldText dataProximaRodadaNfeField = FormFieldText.create(field -> {
        field.title("Próxima Rodada NF Entrada");
        field.editable(false);
        field.value.bind(super.NFEDataProximoLog);
    });
    
    private final FormFieldText estadoJobNFSField = FormFieldText.create(field -> {
        field.title("Estado NF Saida");
        field.editable(false);
        field.value.bind(super.NFSStatusJob);
        field.value.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                if (newValue.equals("EM EXECUÇÃO")) {
                    field.addStyle("danger");
                } else {
                    field.removeStyle("danger");
                }
            }
        });
    });
    private final FormFieldText inicioUltimaRodadaNfsField = FormFieldText.create(field -> {
        field.title("Início Última Rodada NF Saida");
        field.editable(false);
        field.value.bind(super.NFSinicioUltimoLog);
    });
    private final FormFieldText tempoUltimaRodadaNfsField = FormFieldText.create(field -> {
        field.title("Tempo Última Rodada NF Saida");
        field.editable(false);
        field.value.bind(super.NFSTempoUltimoLog);
    });
    private final FormFieldText dataProximaRodadaNfsField = FormFieldText.create(field -> {
        field.title("Próxima Rodada NF Saida");
        field.editable(false);
        field.value.bind(super.NFSDataProximoLog);
    });
    // </editor-fold>
    
    public JobsIntegracaoView() {
        super("Job's Integração", ImageUtils.getImage(ImageUtils.Icon.GESTAO_MATERIAIS));
        init();
    }
    
    private void init() {
        getLogJob();
        try {
            getStatusJobNfe();
            getStatusJobNfs();
        } catch (SQLException e) {
            e.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(e);
                message.showAndWait();
            });
        }
        super.box.getChildren().add(FormBox.create(boxPrincipal -> {
            boxPrincipal.horizontal();
            boxPrincipal.add(FormBox.create(boxJob -> {
                boxJob.vertical();
                boxJob.title("Notas Fiscais Entrada");
                boxJob.add(estadoJobNFEField.build());
                boxJob.add(inicioUltimaRodadaNfeField.build());
                boxJob.add(tempoUltimaRodadaNfeField.build());
                boxJob.add(dataProximaRodadaNfeField.build());
                boxJob.add(FormBox.create(lo1 -> {
                    lo1.horizontal();
                    lo1.add(FormButton.create(btn -> {
                        btn.title("Rodar Job");
                        btn.addStyle("lg").addStyle("success");
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._32));
                        btn.setAction(event -> {
                            try {
                                getStatusJobNfe();
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                            if (NFEStatusJob.getValue().equals("EM EXECUÇÃO")) {
                                MessageBox.create(message -> {
                                    message.message("Job já está em execução!");
                                    message.type(MessageBox.TypeMessageBox.WARNING);
                                    message.showAndWait();
                                    message.position(Pos.CENTER);
                                });
                            } else {
                                try {
                                    runJobNFe();
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    ExceptionBox.build(message -> {
                                        message.exception(e);
                                        message.showAndWait();
                                    });
                                }
                            }
                        });
                    }));
                    lo1.add(FormButton.create(btn -> {
                        btn.title("Atualizar Status Job");
                        btn.addStyle("lg").addStyle("primary");
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ATUALIZAR, ImageUtils.IconSize._24));
                        btn.setAction(event -> {
                            try {
                                getStatusJobNfe();
                            } catch (SQLException e) {
                                e.printStackTrace();
                                ExceptionBox.build(message -> {
                                    message.exception(e);
                                    message.showAndWait();
                                });
                            }
                        });
                    }));
                }));
            }));
            boxPrincipal.add(FormBox.create(boxJob -> {
                boxJob.vertical();
                boxJob.title("Notas Fiscais Saída");
                boxJob.add(estadoJobNFSField.build());
                boxJob.add(inicioUltimaRodadaNfsField.build());
                boxJob.add(tempoUltimaRodadaNfsField.build());
                boxJob.add(dataProximaRodadaNfsField.build());
                boxJob.add(FormBox.create(lo1 -> {
                    lo1.horizontal();
                    lo1.add(FormButton.create(btn -> {
                        btn.title("Rodar Job");
                        btn.addStyle("lg").addStyle("success");
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._32));
                        btn.setAction(event -> {
                            try {
                                getStatusJobNfs();
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                            if (NFSStatusJob.getValue().equals("EM EXECUÇÃO")) {
                                MessageBox.create(message -> {
                                    message.message("Job já está em execução!");
                                    message.type(MessageBox.TypeMessageBox.WARNING);
                                    message.showAndWait();
                                    message.position(Pos.CENTER);
                                });
                            } else {
                                try {
                                    runJobNFs();
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    ExceptionBox.build(message -> {
                                        message.exception(e);
                                        message.showAndWait();
                                    });
                                }
                            }
                        });
                    }));
                    lo1.add(FormButton.create(btn -> {
                        btn.title("Atualizar Status Job");
                        btn.addStyle("lg").addStyle("primary");
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ATUALIZAR, ImageUtils.IconSize._24));
                        btn.setAction(event -> {
                            try {
                                getStatusJobNfs();
                            } catch (SQLException e) {
                                e.printStackTrace();
                                ExceptionBox.build(message -> {
                                    message.exception(e);
                                    message.showAndWait();
                                });
                            }
                        });
                    }));
                }));
            }));
        }));
    }
}
