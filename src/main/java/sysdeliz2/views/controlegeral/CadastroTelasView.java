package sysdeliz2.views.controlegeral;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import sysdeliz2.controllers.views.controlegeral.CadastroTelasController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.models.sysdeliz.SdAcessoTela;
import sysdeliz2.models.sysdeliz.SdTela;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class CadastroTelasView extends CadastroTelasController {

    protected MenuBar menuBar = new MenuBar();

    private ObjectProperty<SdTela> telaSelecionada = new SimpleObjectProperty<>();

    protected BooleanProperty inEdition = new SimpleBooleanProperty(false);
    protected BooleanProperty isMenu = new SimpleBooleanProperty(false);
    protected BooleanProperty isMenuPrincipal = new SimpleBooleanProperty(false);

    private final ListProperty<SdTela> telasBean = new SimpleListProperty<>(FXCollections.observableArrayList());

    // <editor-fold defaultstate="collapsed" desc="Fields">
    private final FormFieldText idField = FormFieldText.create(field -> {
        field.title("Id");
        field.width(70);
        field.editable.set(false);
    });
    private final FormFieldText codTelaField = FormFieldText.create(field -> {
        field.title("CodTela");
        field.width(70);
        field.editable.bind(inEdition);
    });
    private final FormFieldText nomeTelaField = FormFieldText.create(field -> {
        field.title("Nome Tela");
        field.width(370);
        field.editable.bind(inEdition);
    });
    private final FormFieldText pathTelaField = FormFieldText.create(field -> {
        field.title("Path");
        field.width(500);
        field.editable.set(false);
        field.textField.setOnMouseClicked(evt -> {
            if (evt.getButton().equals(MouseButton.PRIMARY) && evt.getClickCount() == 2 && inEdition.get() && isMenu.not().get() && isMenuPrincipal.not().get()) {
                buscarPathTela();
            }
        });
    });
    private final FormFieldComboBox<SdTela.TipoTela> tipoTelaField = FormFieldComboBox.create(String.class, field -> {
        field.title("Tipo");
        field.editable.bind(inEdition.and(isMenu.not()).and(isMenuPrincipal.not()));
        field.items(FXCollections.observableArrayList(Arrays.asList(SdTela.TipoTela.JV, SdTela.TipoTela.KT, SdTela.TipoTela.FX)));
        field.select(0);
        field.width(60);
    });
    private final FormFieldText iconPathField = FormFieldText.create(field -> {
        field.title("Icon");
        field.editable.set(false);
        field.textField.setOnMouseClicked(evt -> {
            if (evt.getButton().equals(MouseButton.PRIMARY) && evt.getClickCount() == 2 && inEdition.get()) {
                buscarPathIcone();
            }
        });
        field.width(250);
    });
    private final FormFieldComboBox<SdTela> menuField = FormFieldComboBox.create(SdTela.class, field -> {
        field.title("Menu");
        field.width(200);
        field.editable.bind(inEdition.and(isMenuPrincipal.not()));
    });

    private final FormFieldText ordemField = FormFieldText.create(field -> {
        field.title("Ordem");
        field.editable.bind(inEdition);
        field.width(100);
    });
    private final FormFieldComboBox<String> grupoAlDapField = FormFieldComboBox.create(String.class, field -> {
        field.title("Grupo ALDap");
        field.width(130);
        field.editable.bind(inEdition.and(isMenuPrincipal));
        List<SdTela> listGruposAlDap = (List<SdTela>) new FluentDao().selectFrom(SdTela.class).where(it -> it.isNotNull("grupoAldap")).orderBy("grupoAldap", OrderType.ASC).resultList();
        field.items(FXCollections.observableArrayList(listGruposAlDap.stream().map(it -> it.getGrupoAldap()).collect(Collectors.toList())));
    });
    private final FormFieldToggle tsMenu = FormFieldToggle.create(ts -> {
        ts.title("Menu");
        ts.editable.bind(inEdition);
        ts.changed((observable, oldValue, newValue) -> {
            handleTsMenuChange(newValue);
        });
    });

    private final FormFieldToggle tsAtivo = FormFieldToggle.create(ts -> {
        ts.title("Ativo");
        ts.editable.bind(inEdition);
    });
    private final FormFieldToggle tsMobile = FormFieldToggle.create(ts -> {
        ts.title("Mobile");
        ts.editable.bind(inEdition);
    });
    private final ImageView iconView = new ImageView();

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Buttons">
    private Button btnEditar = FormButton.create(btn -> {
        btn.setText("Editar");
        btn.addStyle("warning");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EDIT, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            editarTela();
        });
    });

    private Button btnConfirmar = FormButton.create(btn -> {
        btn.setText("Confirmar");
        btn.addStyle("success");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
        btn.disable.bind(inEdition.not());
        btn.setOnAction(evt -> {
            confirmarTela();
        });
    });

    private Button btnCancelar = FormButton.create(btn -> {
        btn.setText("Cancelar");
        btn.addStyle("danger");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
        btn.disable.bind(inEdition.not());
        btn.setOnAction(evt -> {
            cancelarTela();
        });
    });

    private Button btnExcluirTela = FormButton.create(btn -> {
        btn.setText("Excluir");
        btn.addStyle("danger");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
        btn.disable.bind(inEdition.not());
        btn.setOnAction(evt -> {
            excluirTela();
        });
    });

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Table">
    private final FormTableView<SdTela> telaTbl = FormTableView.create(SdTela.class, table -> {
        telasBean.set(FXCollections.observableArrayList((List<SdTela>) new FluentDao().selectFrom(SdTela.class).get().orderBy("codTela", OrderType.ASC).resultList()));
        table.title("Telas");
        table.expanded();
        table.disable.bind(inEdition);
        table.items.bind(telasBean);
        table.selectionModelItem((observable, oldValue, newValue) -> {
            if (telaSelecionada != null) {
                if (newValue != null) JPAUtils.getEntityManager().refresh(newValue);
                telaSelecionada.set((SdTela) newValue);
            }
        });
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Id");
                    cln.width(70);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdTela, SdTela>, ObservableValue<SdTela>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId()));
                }).build(), /*Id*/
                FormTableColumn.create(cln -> {
                    cln.title("CodTela");
                    cln.width(70);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdTela, SdTela>, ObservableValue<SdTela>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodTela()));
                }).build(), /*Id*/
                FormTableColumn.create(cln -> {
                    cln.title("Nome Tela");
                    cln.width(220);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdTela, SdTela>, ObservableValue<SdTela>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNomeTela()));
                }).build(), /*Id*/
                FormTableColumn.create(cln -> {
                    cln.title("Ativo");
                    cln.width(70);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdTela, SdTela>, ObservableValue<SdTela>>) param -> new ReadOnlyObjectWrapper(param.getValue().isAtivo() ? "Sim" : "Não"));
                }).build(), /*Id*/
                FormTableColumn.create(cln -> {
                    cln.title("Menu");
                    cln.width(70);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdTela, SdTela>, ObservableValue<SdTela>>) param -> new ReadOnlyObjectWrapper(param.getValue().isMenu() ? "Sim" : "Não"));
                }).build() /*Id*/
        );
    });

    // </editor-fold>

    public CadastroTelasView() {
        super("Cadastro de Telas", ImageUtils.getImage(ImageUtils.Icon.OPERACOES));
        init();
        carregaMenus();
    }

    private void init() {
        iconView.setFitHeight(200);
        iconView.setFitWidth(200);
        menuBar.disableProperty().bind(inEdition);
        telaSelecionada.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                carregaCampos(newValue);
            }
        });
        box.getChildren().add(FormBox.create(principal -> {
            principal.horizontal();
            principal.expanded();
            principal.add(FormBox.create(col1 -> {
                col1.vertical();
                col1.expanded();
                col1.add(FormBox.create(boxTable -> {
                    boxTable.vertical();
                    boxTable.expanded();
                    boxTable.add(telaTbl.build());
                }));
                col1.add(FormBox.create(linha -> {
                    linha.horizontal();
                    linha.add(btnEditar, btnCancelar, btnConfirmar);
                }));
            }));
            principal.add(FormBox.create(col2 -> {
                col2.vertical();
                col2.add(FormBox.create(boxMenuBar -> {
                    boxMenuBar.horizontal();
                    boxMenuBar.add(menuBar);
                }));
                col2.add(FormBox.create(boxDadosTela -> {
                    boxDadosTela.horizontal();
                    boxDadosTela.title("Dados da Tela");
                    boxDadosTela.add(FormBox.create(boxCampos -> {
                        boxCampos.vertical();
                        boxCampos.add(FormBox.create(l1 -> {
                            l1.horizontal();
                            l1.add(idField.build(), codTelaField.build(), nomeTelaField.build());
                        }));
                        boxCampos.add(FormBox.create(l2 -> {
                            l2.horizontal();
                            l2.add(pathTelaField.build());
                        }));
                        boxCampos.add(FormBox.create(l3 -> {
                            l3.horizontal();
                            l3.add(ordemField.build(), tipoTelaField.build(), grupoAlDapField.build(), menuField.build());
                        }));
                        boxCampos.add(FormBox.create(l4 -> {
                            l4.horizontal();
                            l4.add(tsMenu.build(), tsAtivo.build(), tsMobile.build());
                        }));
                    }));
                    boxDadosTela.add(FormBox.create(boxIcon -> {
                        boxIcon.vertical();
                        boxIcon.alignment(Pos.CENTER);
                        boxIcon.add(iconView, iconPathField.build());
                    }));
                    boxDadosTela.add(btnExcluirTela);
                }));
            }));
        }));
    }

    private void carregaMenus() {
        menuBar.getMenus().clear();
        List<SdTela> menusPrincipais = (List<SdTela>) new FluentDao().selectFrom(SdTela.class)
                .where(it -> it
                        .equal("menu", true)
                        .isNull("idMenu")
                ).orderBy("ordem", OrderType.ASC).resultList();

        for (SdTela telaUsuario : menusPrincipais) {
            Menu menu = new Menu(telaUsuario.getNomeTela());
            ImageView imageView;
            try {
                imageView = new ImageView(new Image(getClass().getResourceAsStream(telaUsuario.getIcone())));
            } catch (Exception e) {
                imageView = new ImageView(new Image(getClass().getResourceAsStream("/images/icons/alert warning_50.png")));
            }
            imageView.setFitHeight(30);
            imageView.setFitWidth(30);
            imageView.setPickOnBounds(true);
            imageView.setPreserveRatio(true);
            menu.setGraphic(imageView);
            menuBar.getMenus().add(menu);
            menu.setMnemonicParsing(false);
            menu.setId(String.valueOf(telaUsuario.getId()));
            gerarItens(menu, telaUsuario);
        }
    }

    private void gerarItens(Menu menu, SdTela telaUsuario) {
        List<SdTela> itens = (List<SdTela>) new FluentDao().selectFrom(SdTela.class).where(it -> it.equal("idMenu.id", telaUsuario.getId())).orderBy("ordem", OrderType.ASC).resultList();
        for (SdTela tela : itens) {
            ImageView imageView = new ImageView(new Image(getClass().getResourceAsStream("/images/icons/alert warning_50.png")));
            try {
                imageView = new ImageView(new Image(getClass().getResourceAsStream(tela.getIcone())));
            } catch (Exception e) {
                imageView = new ImageView(new Image(getClass().getResourceAsStream("/images/icons/alert warning_50.png")));
            }
            imageView.setFitHeight(20);
            imageView.setFitWidth(20);
            imageView.setPickOnBounds(true);
            imageView.setPreserveRatio(true);

            if (tela.isMenu()) {
                Menu menuFilho = new Menu(tela.getNomeTela());
                menuFilho.setGraphic(imageView);
                menuFilho.setId(String.valueOf(tela.getId()));
                menuFilho.setMnemonicParsing(false);
                menu.getItems().add(menuFilho);
                gerarItens(menuFilho, tela);
            } else {
                MenuItem menuItem = new MenuItem(tela.getCodTela() + " " + tela.getNomeTela());
                menuItem.setId(String.valueOf(tela.getId()));
                menuItem.setGraphic(imageView);
                menuItem.setMnemonicParsing(false);
                menuItem.setOnAction(evt -> telaSelecionada.set(tela));
                menu.getItems().add(menuItem);
            }
        }
        MenuItem menuItem = new MenuItem("Adicionar Menu Item");
        menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._16));
        menuItem.setMnemonicParsing(false);
        menu.getItems().add(menuItem);
        menuItem.setOnAction(evt -> {
            adicionarMenuItem(menu, false);
        });

        MenuItem menuFilho = new MenuItem("Adicionar Menu");
        menuFilho.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._16));
        menuFilho.setMnemonicParsing(false);
        menu.getItems().add(menuFilho);
        menuFilho.setOnAction(evt -> {
            adicionarMenuItem(menu, true);
        });
    }

    private void adicionarMenuItem(Menu parentMenu, boolean isMenu) {
        inEdition.set(true);
        limparCampos();
        tsMenu.editable.unbind();
        tsMenu.editable.set(isMenu);
        SdTela codMenu = new FluentDao().selectFrom(SdTela.class).where(it -> it.equal("id", parentMenu.getId())).singleResult();
        List<SdTela> telas = (List<SdTela>) new FluentDao().selectFrom(SdTela.class).where(it -> it.equal("idMenu.id", parentMenu.getId())).resultList();
        char nextCodTela = 0;
        int nextOrdem = 0;
        if (telas != null && telas.size() > 0) {

            nextCodTela = telas.stream().map(it -> it.getCodTela().charAt(it.getCodTela().length() - 1)).max((x, y) -> Character.compare(x, y)).get();
            nextCodTela = (char) (nextCodTela == 57 ? 65 : nextCodTela + 1);
            nextOrdem = telas.stream().map(it -> it.getOrdem()).max(Integer::compareTo).get() + 1;
        }
        SdTela sdTela = new SdTela(codMenu, codMenu.getCodTela() + nextCodTela);
        sdTela.setMenu(isMenu);
        sdTela.setOrdem(nextOrdem);
        telaSelecionada.set(sdTela);
    }

    private void limparCampos() {
        idField.clear();
        codTelaField.clear();
        nomeTelaField.clear();
        pathTelaField.clear();
        ordemField.clear();
        grupoAlDapField.clear();
        iconPathField.clear();
        tsAtivo.value.set(true);
        tsMobile.value.set(false);
        menuField.select(0);
        iconView.setImage(null);
    }

    private void cancelarTela() {
        inEdition.set(false);
        telaSelecionada.set(null);
        limparCampos();
    }

    private void editarTela() {
        if (telaSelecionada != null) {
            inEdition.set(true);
        }
    }

    private void excluirTela() {
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Tem certeza que deseja exluir essa tela?");
            message.showAndWait();
        }).value.get())) {
            new FluentDao().delete(telaSelecionada);
            excluirAcessos();
            telasBean.remove(telaSelecionada);
            carregaMenus();
            limparCampos();
            inEdition.set(false);
            telaSelecionada.set(null);
            MessageBox.create(message -> {
                message.message("Tela excluída com sucesso!");
                message.type(MessageBox.TypeMessageBox.SUCCESS);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
        }
    }

    private void excluirAcessos() {
        List<SdAcessoTela> acessos = (List<SdAcessoTela>) new FluentDao().selectFrom(SdAcessoTela.class).where(it -> it.equal("id.idTela.id", telaSelecionada.get().getId())).resultList();
        new FluentDao().deleteAll(acessos);
    }

    private void confirmarTela() {
        dadosCampos();

        SdTela codTelaRepetido = new FluentDao().selectFrom(SdTela.class).where(it -> it.equal("codTela", telaSelecionada.get().getCodTela()).notEqual("id", telaSelecionada.get().getId())).singleResult();
        if (codTelaRepetido != null) {
            if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                message.message("Código de tela repetido, deseja atualizar o código dos outros itens?");
                message.showAndWait();
            }).value.get())) {
                List<SdTela> listItens = (List<SdTela>) new FluentDao().selectFrom(SdTela.class).where(it -> it.equal("idMenu", telaSelecionada.get().getIdMenu()).notEqual("id", telaSelecionada.get().getId())).resultList();
                listItens.stream().filter(it -> Character.getNumericValue(it.getCodTela().charAt(it.getCodTela().length() - 1)) >= Character.getNumericValue(telaSelecionada.get().getCodTela().charAt(telaSelecionada.get().getCodTela().length() - 1)))
                        .collect(Collectors.toList()).forEach(menuItem -> {
                            menuItem.setCodTela(menuItem.getIdMenu().getCodTela() + Character.toUpperCase(Character.forDigit(Character.getNumericValue(menuItem.getCodTela().charAt(menuItem.getCodTela().length() - 1)) + 1, 16)));
                            menuItem = new FluentDao().merge(menuItem);
                        });
            }
        }

        SdTela ordemRepetida = new FluentDao().selectFrom(SdTela.class).where(it -> it.equal("ordem", telaSelecionada.get().getOrdem()).equal("idMenu", telaSelecionada.get().getIdMenu())).findAny();
        if (ordemRepetida != null) {
            List<SdTela> listItens = (List<SdTela>) new FluentDao().selectFrom(SdTela.class).where(it -> it.equal("idMenu", telaSelecionada.get().getIdMenu()).notEqual("id", telaSelecionada.get().getId())).resultList();
            listItens.stream().filter(it -> it.getOrdem() >= telaSelecionada.get().getOrdem()).collect(Collectors.toList()).forEach(menuItem -> {
                menuItem.setOrdem(menuItem.getOrdem() + 1);
                menuItem = new FluentDao().merge(menuItem);
            });
        }

        if (telaSelecionada.get().idProperty().get() == 0) {
            telaSelecionada.get().setId(Integer.parseInt(Globals.getProximoCodigo("SD_TELAS", "ID")));
            telasBean.add(telaSelecionada.get());
        }
        telaSelecionada.set(new FluentDao().merge(telaSelecionada.get()));
        inEdition.set(false);
        carregaMenus();
        carregaCampos(telaSelecionada.get());
        carregaTabela();
    }

    private void carregaTabela() {
        telasBean.set(FXCollections.observableArrayList((List<SdTela>) new FluentDao().selectFrom(SdTela.class).get().orderBy("codTela", OrderType.ASC).resultList()));
        telasBean.sort(Comparator.comparing(SdTela::getCodTela));
        telaTbl.refresh();
    }

    private void dadosCampos() {
        telaSelecionada.get().setCodTela(codTelaField.value.getValue());
        telaSelecionada.get().setNomeTela(nomeTelaField.value.getValue());
        telaSelecionada.get().setGrupoAldap(grupoAlDapField.value.getValue());
        telaSelecionada.get().setAtivo(tsAtivo.value.getValue());
        telaSelecionada.get().setMobile(tsMobile.value.getValue());
        telaSelecionada.get().setOrdem(Integer.parseInt(ordemField.value.getValue()));
        telaSelecionada.get().setPath(pathTelaField.value.getValue());
        telaSelecionada.get().setIcone(iconPathField.value.getValue());
        telaSelecionada.get().setTipo(tipoTelaField.value.getValue());
        telaSelecionada.get().setIdMenu(menuField.value.getValue());
    }

    private void carregaCampos(SdTela tela) {
        if (tela != null) {
            atualizaMenus();
            isMenu.set(tela.isMenu());
            isMenuPrincipal.set(tela.isMenu() && tela.getIdMenu() == null);
            idField.value.setValue(String.valueOf(tela.getId()));
            codTelaField.value.setValue(tela.getCodTela());
            nomeTelaField.value.setValue(tela.getNomeTela());
            pathTelaField.value.setValue(tela.getPath());
            ordemField.value.setValue(String.valueOf(tela.getOrdem()));
            grupoAlDapField.select(tela.getGrupoAldap());
            iconPathField.value.setValue(tela.getIcone());
            tsAtivo.value.setValue(tela.isAtivo());
            tsMenu.value.setValue(tela.isMenu());
            tsMobile.value.setValue(tela.isMobile());
            tipoTelaField.select(tela.getTipo());
            menuField.select(tela.getIdMenu() == null ? new SdTela("", "") : tela.getIdMenu());
            try {
                iconView.setImage(new Image(getClass().getResourceAsStream(tela.getIcone().replace("50", "100"))));
            } catch (Exception e) {
                iconView.setImage(new Image("/images/icons/alert warning_50.png"));
            }
        }
    }

    private void handleTsMenuChange(Boolean newValue) {
        if (newValue) {
            pathTelaField.clear();
            tipoTelaField.clearSelection();
            isMenu.set(true);
        } else {
            carregaCampos(telaSelecionada.get());
        }
    }

    private void buscarPathIcone() {
        FileChooser fc = new FileChooser();
        fc.setTitle("Selecionar Arquivo");
        fc.setInitialDirectory(new File(System.getProperty("user.dir") + "\\src\\main\\resources\\images\\icons\\"));
        File file = fc.showOpenDialog(new Stage());
        if (file != null) {
            String pathImage = file.getAbsolutePath().substring(file.getAbsolutePath().indexOf("\\images")).replace("50", "100").replace("\\", "/");
            iconPathField.value.setValue(pathImage);
            try {
                iconView.setImage(new Image(pathImage));
            } catch (Exception e) {
                iconView.setImage(new Image("/images/icons/alert warning_50.png"));
            }
        }
    }

    private void buscarPathTela() {
        FileChooser fc = new FileChooser();
        fc.setTitle("Selecionar Path");
        fc.setInitialDirectory(new File(System.getProperty("user.dir") + "\\src\\main\\java\\sysdeliz2\\views\\"));
        File file = fc.showOpenDialog(new Stage());
        if (file != null) {
            String pathTela = file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("\\sysdeliz2") + 1).replace("\\", ".").replace(".java", "").replace(".kt", "");
            pathTelaField.value.setValue(pathTela);
        }
    }

    private void atualizaMenus() {
        List<SdTela> listTelas = (List<SdTela>) new FluentDao().selectFrom(SdTela.class).where(it -> it.equal("menu", true)).orderBy("codTela", OrderType.ASC).resultList();
        listTelas.set(0, new SdTela("", ""));
        menuField.items.clear();
        menuField.items(FXCollections.observableArrayList(listTelas));
    }

    private void atualizaCodTela() throws IOException {
        char cont = '0';
        List<SdTela> sdTelaList = (List<SdTela>) new FluentDao().selectFrom(SdTela.class).where(it -> it.isNull("idMenu").equal("menu", true).equal("ativo", true)).orderBy("ordem", OrderType.ASC).resultList();
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("C:\\Users\\morfelle.luiz\\Desktop\\text.txt"));
        for (SdTela menu : sdTelaList) {
            menu.setOrdem(cont);
            menu.setCodTela("" + Character.toUpperCase(Character.forDigit(Character.getNumericValue(cont++), 16)));
            bufferedWriter.append(System.getProperty("line.separator")).append(menu.getCodTela() + " " + menu.getNomeTela());
            atualizaFilhos(menu, bufferedWriter, "    ");
            bufferedWriter.newLine();
        }
        bufferedWriter.close();
    }

    private void atualizaFilhos(SdTela menu, BufferedWriter bufferedWriter, String s) throws IOException {
        char contFilho = '0';
        List<SdTela> filhos = (List<SdTela>) new FluentDao().selectFrom(SdTela.class).where(it -> it.equal("idMenu.id", menu.getId())/*.equal("ativo", true)*/).orderBy("ordem", OrderType.ASC).resultList();
        for (SdTela filho : filhos) {
            filho.setOrdem(contFilho);
            filho.setCodTela(menu.getCodTela() + Character.toUpperCase(Character.forDigit(Character.getNumericValue(contFilho++), 16)));
            bufferedWriter.append(System.getProperty("line.separator")).append(s + filho.getCodTela() + " " + filho.getNomeTela());
            if (filho.isMenu()) atualizaFilhos(filho, bufferedWriter, s + "    ");
        }
    }
}
