package sysdeliz2.views.controlegeral;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.input.KeyCode;
import javafx.util.Callback;
import org.controlsfx.control.textfield.TextFields;
import org.jetbrains.annotations.NotNull;
import sysdeliz2.controllers.views.controlegeral.ControleTelasController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.SdUsuario;
import sysdeliz2.models.sysdeliz.SdAcessoTela;
import sysdeliz2.models.sysdeliz.SdTela;
import sysdeliz2.utils.LdapAuth;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.hibernate.DefaultFilter;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.SysLogger;

import javax.naming.NamingException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class ControleTelasView extends ControleTelasController {

    private SdUsuario colaborador = null;
    private List<SdTela> todasTelas = new ArrayList<>();
    private List<SdUsuario> todosUsuarios = new ArrayList<>();

    //<editor-fold desc="Listas Controle">
    private ListProperty<SdUsuario> listaUsuariosDisponiveis = new SimpleListProperty<>(FXCollections.observableArrayList());
    private ListProperty<SdUsuario> listaUsuariosSelecionados = new SimpleListProperty<>(FXCollections.observableArrayList());

    private ListProperty<SdTela> listaTelasDisponiveis = new SimpleListProperty<>(FXCollections.observableArrayList());
    private ListProperty<SdTela> listaTelasSelecionadas = new SimpleListProperty<>(FXCollections.observableArrayList());
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Filter">
    private final FormFieldSingleFind<SdTela> telaFilter = FormFieldSingleFind.create(SdTela.class, field -> {
        field.title("Tela");
        field.width(300);
        field.value.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                carregaItens(listaUsuariosDisponiveis, listaUsuariosSelecionados, todosUsuarios, getUsuariosTela((SdTela) newValue));
            }
        });
    });
    private final FormFieldText colaboradorFilter = FormFieldText.create(field -> {
        field.title("Colaborador");
        field.width(300);
        field.textField.setOnKeyReleased(evt -> {
            if (evt.getCode().equals(KeyCode.ENTER)) {
                buscarColaborador();
            }
        });
        field.textField.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) {
                buscarColaborador();
            }
        });
    });

    private final FormFieldMultipleFind<SdTela> telaAddFilter = FormFieldMultipleFind.create(SdTela.class, field -> {
        field.title.setText("Buscar Telas");
        field.width(250);
        field.defaults.add(new DefaultFilter(false, "menu"));
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="ListView"">
    private FormTableView<SdUsuario> listViewUsuariosDisponiveis = FormTableView.create(SdUsuario.class, table -> {
        table.title("USUÁRIOS DISPONÍVEIS");
        table.size(420, 700);
        table.items.bind(listaUsuariosDisponiveis);
        table.multipleSelection();
        table.items.sizeProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.intValue() > oldValue.intValue()) {
                table.items.sort(Comparator.comparing(SdUsuario::getNome));
            }
        });
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Usuário");
                    cln.width(150.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdUsuario, SdUsuario>, ObservableValue<SdUsuario>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodUsu()));
                }).build(), /*Nome*/
                FormTableColumn.create(cln -> {
                    cln.title("Nome");
                    cln.width(250);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdUsuario, SdUsuario>, ObservableValue<SdUsuario>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNome()));

                }).build() /*Nome*/
        );
    });
    private FormTableView<SdUsuario> listViewUsuariosSelecionados = FormTableView.create(SdUsuario.class, table -> {
        table.title("USUÁRIOS SELECIONADOS");
        table.size(420, 700);
        table.items.bind(listaUsuariosSelecionados);
        table.multipleSelection();
        table.items.sizeProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.intValue() > oldValue.intValue()) {
                table.items.sort(Comparator.comparing(SdUsuario::getNome));
            }
        });
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Usuário");
                    cln.width(150.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdUsuario, SdUsuario>, ObservableValue<SdUsuario>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodUsu()));
                }).build(), /*Nome*/
                FormTableColumn.create(cln -> {
                    cln.title("Nome");
                    cln.width(250);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdUsuario, SdUsuario>, ObservableValue<SdUsuario>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNome()));

                }).build() /*Nome*/
        );
    });
    private final FormListView<SdTela> listViewTelasDisponiveis = FormListView.create(field -> {
        field.title("TELAS DISPONÍVEIS");
        field.size(420, 700);
        field.multipleSelection();
        field.items.bind(listaTelasDisponiveis);
        field.withoutBtnCheckAll();
        field.items.sizeProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.intValue() != 2 && newValue.intValue() > oldValue.intValue()) {
                field.items.sort(Comparator.comparing(SdTela::getCodTela));
            }
        });
    });
    private FormListView<SdTela> listViewTelasSelecionados = FormListView.create(field -> {
        field.title("TELAS SELECIONADAS");
        field.size(420, 700);
        field.multipleSelection();
        field.items.bind(listaTelasSelecionadas);
        field.withoutBtnCheckAll();
        field.items.sizeProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.intValue() != 2 && newValue.intValue() > oldValue.intValue()) {
                field.items.sort(Comparator.comparing(SdTela::getCodTela));
            }
        });

    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Button"">
    private Button btnCopiarTela = FormButton.create(button -> {
        button.setText("Copiar Tela");
        button.getStyleClass().add("primary");
        button.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.INFORMACAO, ImageUtils.IconSize._24));
        button.setOnAction(evt -> {
            copiarUsuariosTela();
        });
    });
    private Button btnCopiarUsuario = FormButton.create(button -> {
        button.setText("Copiar Usuário");
        button.getStyleClass().add("primary");
        button.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.INFORMACAO, ImageUtils.IconSize._24));
        button.setOnAction(evt -> {
            copiarTelasUsuario();
        });
    });

    private Button btnConfirmarTelasUsuario = FormButton.create(button -> {
        button.setText("Confirmar");
        button.getStyleClass().add("success");
        button.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
        button.setOnAction(evt -> {
            if (colaboradorFilter.value.isEmpty().not().get()) salvarTelasUsuario();
        });
    });
    private Button btnConfirmarUsuariosTela = FormButton.create(button -> {
        button.setText("Confirmar");
        button.getStyleClass().add("success");
        button.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
        button.setOnAction(evt -> {
            if (telaFilter.value.getValue().getCodTela() != null) salvarUsuariosTela(telaFilter.value.getValue());
        });
    });

    private Button btnTransferirUmUsuario = FormButton.create(button -> {
        button.setText(">");
        button.getStyleClass().add("success-outline");
        button.setPrefWidth(40.0);
        button.setOnAction(evt -> {
            if (listViewUsuariosDisponiveis.selectedItem() != null)
                transfereItem(listViewUsuariosDisponiveis.selectedRegisters(), listaUsuariosDisponiveis, listaUsuariosSelecionados);
        });
    });
    private Button btnTransferirTodosUsuarios = FormButton.create(button -> {
        button.setText(">>");
        button.getStyleClass().add("success-outline");
        button.setPrefWidth(40.0);
        button.setOnAction(evt -> {
            transfereTodosItens(listaUsuariosDisponiveis, listaUsuariosSelecionados);
        });
    });
    private Button btnVoltarUmUsuario = FormButton.create(button -> {
        button.setText("<");
        button.getStyleClass().add("danger-outline");
        button.setPrefWidth(40.0);
        button.setOnAction(evt -> {
            if (listViewUsuariosSelecionados.selectedItem() != null)
                transfereItem(listViewUsuariosSelecionados.selectedRegisters(), listaUsuariosSelecionados, listaUsuariosDisponiveis);
        });
    });
    private Button btnVoltarTodosUsuarios = FormButton.create(button -> {
        button.setText("<<");
        button.getStyleClass().add("danger-outline");
        button.setPrefWidth(40.0);
        button.setOnAction(evt -> {
            transfereTodosItens(listaUsuariosSelecionados, listaUsuariosDisponiveis);
        });
    });

    private Button btnTransferirUmaTela = FormButton.create(button -> {
        button.setText(">");
        button.getStyleClass().add("success-outline");
        button.setPrefWidth(40.0);
        button.setOnAction(evt -> {
            if (listViewTelasDisponiveis.selectedItem() != null)
                transfereItem(listViewTelasDisponiveis.selectedRegisters(), listaTelasDisponiveis, listaTelasSelecionadas);
        });
    });
    private Button btnTransferirTodasTelas = FormButton.create(button -> {
        button.setText(">>");
        button.getStyleClass().add("success-outline");
        button.setPrefWidth(40.0);
        button.setOnAction(evt -> {
            transfereTodosItens(listaTelasDisponiveis, listaTelasSelecionadas);
        });
    });
    private Button btnVoltarUmaTela = FormButton.create(button -> {
        button.setText("<");
        button.getStyleClass().add("danger-outline");
        button.setPrefWidth(40.0);
        button.setOnAction(evt -> {
            if (listViewTelasSelecionados.selectedItem() != null)
                transfereItem(listViewTelasSelecionados.selectedRegisters(), listaTelasSelecionadas, listaTelasDisponiveis);
        });
    });
    private Button btnVoltarTodasTelas = FormButton.create(button -> {
        button.setText("<<");
        button.getStyleClass().add("danger-outline");
        button.setPrefWidth(40.0);
        button.setOnAction(evt -> {
            transfereTodosItens(listaTelasSelecionadas, listaTelasDisponiveis);
        });
    });

    private Button btnAddTelasFilter = FormButton.create(button -> {
        button.setText("Adicionar Telas");
        button.getStyleClass().add("warning");
        button.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._24));
        button.setOnAction(evt -> {
            if (!telaAddFilter.isEmpty() && !telaAddFilter.objectValues.get().isEmpty())
                addTela(telaAddFilter.objectValues.get());
            telaAddFilter.clear();
        });
    });

    private Button btnBuscarColaborador = FormButton.create(button -> {
        button.setText("");
        button.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.SEARCH, ImageUtils.IconSize._16));
        button.addStyle("warning");
        button.setOnAction(evt -> {
            colaborador = selecionarColaborador();
            if (colaborador != null) {
                colaboradorFilter.value.setValue(colaborador.getNome());
                carregaItens(listaTelasDisponiveis, listaTelasSelecionadas, todasTelas, getTelasUsuario(colaborador));
            }
        });
    });

    // </editor-fold>

    public ControleTelasView() {
        super("Controle de Acesso as Telas", ImageUtils.getImage(ImageUtils.Icon.OPERACOES));
        carregaTodosUsuarios();
        carregaTodasTelas();
        init();
    }

    private void init() {
        super.box.getChildren().add(FormBox.create(principal -> {
            principal.horizontal();
            principal.add(FormBox.create(coluna1 -> {
                coluna1.vertical();
                coluna1.size(1000, 1000);
                coluna1.add(FormBox.create(linhaTitle -> {
                    linhaTitle.horizontal();
                    linhaTitle.add();//Titulo
                }));
                coluna1.add(FormBox.create(linhaFilter -> {
                    linhaFilter.horizontal();
                    linhaFilter.alignment(Pos.BOTTOM_LEFT);
                    linhaFilter.add(colaboradorFilter.build(), btnBuscarColaborador);//Filter
                }));
                coluna1.add(FormBox.create(linhaTabelas -> {
                    linhaTabelas.horizontal();
                    linhaTabelas.add(FormBox.create(telasDisponiveis -> {
                        telasDisponiveis.vertical();
                        telasDisponiveis.add(listViewTelasDisponiveis.build());
                        telasDisponiveis.add(FormBox.create(boxFilters -> {
                            boxFilters.horizontal();
                            boxFilters.alignment(Pos.TOP_LEFT);
                            boxFilters.add(FormBox.create(col1 -> {
                                col1.vertical();
                                col1.size(150, 40);
                                col1.alignment(Pos.CENTER);
                                col1.add(telaAddFilter.build());
                            }));
                            boxFilters.add(FormBox.create(col2 -> {
                                col2.vertical();
                                col2.size(150, 50);
                                col2.alignment(Pos.CENTER);
                                col2.add(btnAddTelasFilter);
                            }));
                        }));
                    }));
                    linhaTabelas.add(FormBox.create(colunaBotoes -> {
                        colunaBotoes.vertical();
                        colunaBotoes.alignment(Pos.CENTER);
                        colunaBotoes.add(btnTransferirUmaTela, btnVoltarUmaTela, btnTransferirTodasTelas, btnVoltarTodasTelas);
                    }));
                    linhaTabelas.add(FormBox.create(telasDisponiveis -> {
                        telasDisponiveis.vertical();
                        telasDisponiveis.add(listViewTelasSelecionados.build());
                        telasDisponiveis.add(FormBox.create(boxBotoes -> {
                            boxBotoes.horizontal();
                            boxBotoes.add(btnCopiarUsuario, btnConfirmarTelasUsuario);
                        }));
                    }));
                }));
            }));
            principal.add(FormBox.create(coluna2 -> {
                coluna2.vertical();
                coluna2.add(FormBox.create(linhaTitle -> {
                    linhaTitle.horizontal();
                    linhaTitle.add();//Titulo
                }));
                coluna2.add(FormBox.create(linhaFilter -> {
                    linhaFilter.horizontal();
                    linhaFilter.add(telaFilter.build());//Filter
                }));
                coluna2.add(FormBox.create(linhaTabelas -> {
                    linhaTabelas.horizontal();
                    linhaTabelas.add(FormBox.create(telasDisponiveis -> {
                        telasDisponiveis.vertical();
                        telasDisponiveis.add(listViewUsuariosDisponiveis.build());
                    }));
                    linhaTabelas.add(FormBox.create(colunaBotoes -> {
                        colunaBotoes.vertical();
                        colunaBotoes.alignment(Pos.CENTER);
                        colunaBotoes.add(btnTransferirUmUsuario, btnVoltarUmUsuario, btnTransferirTodosUsuarios, btnVoltarTodosUsuarios);
                    }));
                    linhaTabelas.add(FormBox.create(telasDisponiveis -> {
                        telasDisponiveis.vertical();
                        telasDisponiveis.add(listViewUsuariosSelecionados.build());
                        telasDisponiveis.add(FormBox.create(boxBotoes -> {
                            boxBotoes.horizontal();
                            boxBotoes.add(btnCopiarTela, btnConfirmarUsuariosTela);
                        }));
                    }));
                }));
                coluna2.add(FormBox.create(linhaBotoes -> {
                    linhaBotoes.horizontal();
                    linhaBotoes.add();//Botoes
                }));
            }));
        }));
        TextFields.bindAutoCompletion(colaboradorFilter.textField, listaUsuariosDisponiveis.stream().map(SdUsuario::getNome).collect(Collectors.toList()));
    }

    //<editor-fold desc="Carrega Usuarios/Telas">
    public void carregaTodosUsuarios() {
        try {
            listaUsuariosDisponiveis.set(FXCollections.observableArrayList(LdapAuth.getAllUsers()));
            todosUsuarios.addAll(FXCollections.observableArrayList(listaUsuariosDisponiveis));
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    public void carregaTodasTelas() {
        listaTelasDisponiveis.set(FXCollections.observableArrayList((List<SdTela>) new FluentDao().selectFrom(SdTela.class).where(it -> it.equal("menu", false)).resultList()));
        todasTelas = new ArrayList<>(listaTelasDisponiveis);
    }

    private void carregaItens(ListProperty origem, ListProperty destino, List todosItens, List itensToAdd) {
        origem.clear();
        destino.clear();
        List collect = (List) todosItens.stream().filter(it -> itensToAdd.stream().noneMatch(eb -> eb.equals(it))).collect(Collectors.toList());
        origem.addAll(collect);
        destino.addAll(itensToAdd);
    }

    //</editor-fold>

    //<editor-fold desc="Controle Botões">
    private void transfereItem(ObservableList itens, ListProperty origem, ListProperty destino) {
        destino.addAll(itens);
        origem.removeAll(itens);
    }

    private void addTela(ObservableList<SdTela> sdTelas) {
        sdTelas.forEach(tela -> {
            if (!listaTelasSelecionadas.contains(tela)) listaTelasSelecionadas.add(tela);
            listaTelasDisponiveis.remove(tela);
        });
    }

    private void transfereTodosItens(ListProperty listaOrigem, ListProperty listaDestino) {
        listaDestino.addAll(listaOrigem);
        listaOrigem.clear();
    }

    //</editor-fold>

    //<editor-fold desc="Save">
    private void salvarTelasUsuario() {
        List<SdAcessoTela> telasUsuario = (List<SdAcessoTela>) new FluentDao().selectFrom(SdAcessoTela.class).where(it -> it.equal("id.nomeUsu", colaborador.getCodUsu())).resultList();
        telasUsuario.forEach(tela -> {
            if (listViewTelasSelecionados.items.stream().noneMatch(it -> it.getCodTela().equals(tela.getId().getIdTela()))) {
                new FluentDao().delete(tela);
                SysLogger.addSysDelizLog("Controle de Telas", TipoAcao.EXCLUIR,
                        tela.getId().getIdTela().getCodTela(), "Acesso a tela " + tela.getId().getIdTela() + " removido do usuário " + tela.getId().getNomeUsu() + "!");
            }
        });

        telasUsuario = (List<SdAcessoTela>) new FluentDao().selectFrom(SdAcessoTela.class).where(it -> it.equal("id.nomeUsu", colaborador.getCodUsu())).resultList();

        List<SdAcessoTela> finalTelasUsuario = telasUsuario;
        listViewTelasSelecionados.items.stream().filter(it -> finalTelasUsuario.stream().noneMatch(eb -> eb.getId().getIdTela().equals(it.getCodTela()))).forEach(tela -> {
            SdAcessoTela sdAcessoTela = new SdAcessoTela(new FluentDao().selectFrom(SdTela.class).where(it -> it.equal("codTela", tela.getCodTela())).singleResult(), colaborador.getCodUsu());
            new FluentDao().merge(sdAcessoTela);
            SysLogger.addSysDelizLog("Controle de Telas", TipoAcao.CADASTRAR,
                    tela.getCodTela(), "Acesso a tela " + tela.getCodTela() + " foi garantido ao usuário " + colaborador.getCodUsu() + "!");
        });
        MessageBox.create(message -> {
            message.message("Alterações salvas com sucesso!");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.showAndWait();
            message.position(Pos.TOP_RIGHT);
        });
    }

    private void salvarUsuariosTela(SdTela tela) {
        List<SdAcessoTela> telasUsuario = (List<SdAcessoTela>) new FluentDao().selectFrom(SdAcessoTela.class).where(it -> it.equal("id.codTela", tela.getCodTela())).resultList();
        telasUsuario.forEach(telaUsu -> {
            if (listViewUsuariosSelecionados.items.stream().noneMatch(it -> it.getCodUsu().equals(telaUsu.getId().getNomeUsu()))) {
                new FluentDao().delete(telaUsu);
                SysLogger.addSysDelizLog("Controle de Telas", TipoAcao.EXCLUIR,
                        telaUsu.getId().getIdTela().getCodTela(), "Acesso a tela " + tela.getCodTela() + " removido do usuário " + telaUsu.getId().getNomeUsu() + "!");
            }
        });

        telasUsuario = (List<SdAcessoTela>) new FluentDao().selectFrom(SdAcessoTela.class).where(it -> it.equal("id.nomeUsu", colaborador.getCodUsu())).resultList();

        List<SdAcessoTela> finalTelasUsuario = telasUsuario;

        listViewUsuariosSelecionados.items.stream().filter(it -> finalTelasUsuario.stream().noneMatch(eb -> eb.getId().getNomeUsu().equals(it.getNome()))).forEach(usuario -> {
            SdAcessoTela sdAcessoTela = new SdAcessoTela(new FluentDao().selectFrom(SdTela.class).where(it -> it.equal("codTela", tela.getCodTela())).singleResult(), usuario.getCodUsu());
            sdAcessoTela = new FluentDao().merge(sdAcessoTela);
            SysLogger.addSysDelizLog("Controle de Telas", TipoAcao.CADASTRAR,
                    tela.getCodTela(), "Acesso a tela " + tela.getNomeTela() + " foi garantido ao usuário " + usuario.getCodUsu() + "!");
        });
        MessageBox.create(message -> {
            message.message("Alterações salvas com sucesso!");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.showAndWait();
            message.position(Pos.TOP_RIGHT);
        });
    }
    //</editor-fold>

    //<editor-fold desc="Copy">
    private void copiarTelasUsuario() {
        SdUsuario usuario = selecionarColaborador();
        if (usuario.getCodUsu() != null)
            carregaItens(listaTelasDisponiveis, listaTelasSelecionadas, todasTelas, getTelasUsuario(usuario));
    }

    private void copiarUsuariosTela() {
        SdTela tela = selecionarTela();
        if (tela.getCodTela() != null) {
            tela.setCodTela(tela.getCodTela());
            tela.setNomeTela(tela.getNomeTela());
            carregaItens(listaUsuariosDisponiveis, listaUsuariosSelecionados, todosUsuarios, getUsuariosTela(tela));
        }
    }
    //</editor-fold>

    //<editor-fold desc="Utils">
    @NotNull
    private List<SdUsuario> getUsuariosTela(SdTela tela) {
        List<String> usuarios = ((List<SdAcessoTela>) new FluentDao().selectFrom(SdAcessoTela.class).where(it -> it.equal("id.idTela.id", tela.getId())).resultList()).stream().map(it -> it.getId().getNomeUsu()).collect(Collectors.toList());
        return todosUsuarios.stream().filter(it -> usuarios.stream().anyMatch(eb -> eb.equals(it.getCodUsu()))).collect(Collectors.toList());
    }

    @NotNull
    private List<SdTela> getTelasUsuario(SdUsuario colaborador) {
        return ((List<SdAcessoTela>) new FluentDao().selectFrom(SdAcessoTela.class).where(it -> it.equal("id.nomeUsu", colaborador.getCodUsu())).resultList()).stream().map(it -> it.getId().getIdTela()).collect(Collectors.toList());
    }

    private void buscarColaborador() {
        if (colaboradorFilter.value != null && colaboradorFilter.value.isEmpty().not().get()) {
            if (todosUsuarios.stream().anyMatch(it -> it.getNome().equals(colaboradorFilter.value.getValue()))) {
                colaborador = todosUsuarios.stream().filter(it -> it.getNome().equals(colaboradorFilter.value.getValue())).findFirst().get();
                carregaItens(listaTelasDisponiveis, listaTelasSelecionadas, todasTelas, getTelasUsuario(colaborador));
            } else {
                colaboradorFilter.clear();
            }
        }
    }

    private SdUsuario selecionarColaborador() {
        AtomicReference<SdUsuario> colaborador = new AtomicReference<>(new SdUsuario());
        ListProperty<SdUsuario> usuarios = new SimpleListProperty<>(FXCollections.observableArrayList(todosUsuarios));
        final FormFieldText nomeFilter = FormFieldText.create(field -> {
            field.title("Nome");
            field.toUpper();
        });
        FormTableView<SdUsuario> tbl = FormTableView.create(SdUsuario.class, table -> {
            table.title("Usuários");
            table.items.bind(usuarios);
            table.columns(
                    FormTableColumn.create(cln -> {
                        cln.title("Usuário");
                        cln.width(150);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdUsuario, SdUsuario>, ObservableValue<SdUsuario>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodUsu().toUpperCase()));
                    }).build(), /**/
                    FormTableColumn.create(cln -> {
                        cln.title("Nome");
                        cln.width(350);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdUsuario, SdUsuario>, ObservableValue<SdUsuario>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNome().toUpperCase()));
                    }).build() /**/
            );
        });
        new Fragment().show(fragment -> {
            fragment.title("Escolha o Colaborador");
            fragment.size(600.0, 400.0);
            fragment.box.getChildren().add(FormBox.create(box -> {
                box.vertical();
                box.add(FormBox.create(boxFilter -> {
                    boxFilter.horizontal();
                    boxFilter.alignment(Pos.BOTTOM_LEFT);
                    boxFilter.add(nomeFilter.build());
                    boxFilter.add(FormButton.create(button -> {
                        button.title("");
                        button.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.SEARCH, ImageUtils.IconSize._16));
                        button.addStyle("warning");
                        button.setOnAction(evt -> {
                            if (nomeFilter.value.isNotNull().get() && nomeFilter.value.getValue().equals(""))
                                usuarios.addAll(todosUsuarios);
                            else {
                                usuarios.set(FXCollections.observableArrayList(todosUsuarios.stream().filter(it -> it.getNome().toUpperCase().contains(nomeFilter.value.getValue()) ||
                                        it.getCodUsu().toUpperCase().contains(nomeFilter.value.getValue())).collect(Collectors.toList())));
                            }
                        });
                    }));
                }));
                box.add(tbl.build());
            }));
            fragment.buttonsBox.getChildren().add(FormButton.create(botao -> {
                botao.title("Selecionar");
                botao.addStyle("success");
                botao.icon(ImageUtils.getIconButton(ImageUtils.ButtonIcon.SELECT, ImageUtils.IconSize._24));
                botao.setOnAction(event -> {
                    colaborador.set(tbl.selectedItem());
                    fragment.close();
                });
            }));
        });
        return colaborador.get();
    }

    private SdTela selecionarTela() {
        AtomicReference<SdTela> tela = new AtomicReference<>(new SdTela());
        ListProperty<SdTela> telas = new SimpleListProperty<>(FXCollections.observableArrayList(todasTelas));
        final FormFieldText nomeFilter = FormFieldText.create(field -> {
            field.title("Nome");
            field.value.setValue("");
            field.toUpper();
        });
        final FormFieldText codFilter = FormFieldText.create(field -> {
            field.title("Código");
            field.value.setValue("");
        });
        FormTableView<SdTela> tbl = FormTableView.create(SdTela.class, table -> {
            table.title("Telas");
            table.items.bind(telas);
            table.columns(
                    FormTableColumn.create(cln -> {
                        cln.title("CodTela");
                        cln.width(150);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdTela, SdTela>, ObservableValue<SdTela>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodTela()));
                    }).build(), /**/
                    FormTableColumn.create(cln -> {
                        cln.title("Nome");
                        cln.width(350);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdTela, SdTela>, ObservableValue<SdTela>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNomeTela().toUpperCase()));
                    }).build() /**/
            );
        });
        new Fragment().show(fragment -> {
            fragment.title("Escolha a Tela");
            fragment.size(600.0, 400.0);
            fragment.box.getChildren().add(FormBox.create(box -> {
                box.vertical();
                box.add(FormBox.create(boxFilter -> {
                    boxFilter.horizontal();
                    boxFilter.alignment(Pos.BOTTOM_LEFT);
                    boxFilter.add(codFilter.build());
                    boxFilter.add(nomeFilter.build());
                    boxFilter.add(FormButton.create(button -> {
                        button.title("");
                        button.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.SEARCH, ImageUtils.IconSize._16));
                        button.addStyle("warning");
                        button.setOnAction(evt -> {
                            if (!nomeFilter.value.getValue().equals(""))
                                telas.set(FXCollections.observableArrayList(todasTelas.stream().filter(it -> it.getNomeTela().toUpperCase().contains(nomeFilter.value.getValue())).collect(Collectors.toList())));
                            if (!codFilter.value.getValue().equals(""))
                                telas.set(FXCollections.observableArrayList(todasTelas.stream().filter(it -> it.getCodTela().contains(codFilter.value.getValue())).collect(Collectors.toList())));

                        });
                    }));
                }));
                box.add(tbl.build());
            }));
            fragment.buttonsBox.getChildren().add(FormButton.create(botao -> {
                botao.title("Selecionar");
                botao.addStyle("success");
                botao.icon(ImageUtils.getIconButton(ImageUtils.ButtonIcon.SELECT, ImageUtils.IconSize._24));
                botao.setOnAction(event -> {
                    tela.set(tbl.selectedItem());
                    fragment.close();
                });
            }));
        });
        return tela.get();
    }
    //</editor-fold>
}
