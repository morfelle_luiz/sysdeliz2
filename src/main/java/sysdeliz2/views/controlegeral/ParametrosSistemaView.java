package sysdeliz2.views.controlegeral;

import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import sysdeliz2.controllers.views.controlegeral.ParametrosSistemaController;
import sysdeliz2.models.sysdeliz.sistema.SdParametros;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.sys.ImageUtils;

public class ParametrosSistemaView extends ParametrosSistemaController {

    // <editor-fold defaultstate="collapsed" desc="components">
    private final FormTabPane tabsGruposParamentros = FormTabPane.create(tabs -> {
        tabs.expanded();
    });
    // </editor-fold>

    public ParametrosSistemaView() {
        super("Parâmetros do Sistema", ImageUtils.getImage(ImageUtils.Icon.TODOS));
        init();
    }

    private void init() {
        super.box.getChildren().add(FormBox.create(content -> {
            content.vertical();
            content.expanded();
            content.add(tabsGruposParamentros);
        }));

        grupos.forEach(grupo -> {
            tabsGruposParamentros.addTab(FormTab.create(tab -> {
                tab.title(grupo.getDescricao());
                tab.add(FormBox.create(parametros -> {
                    parametros.vertical();
                    parametros.add(FormBox.create(headerConfigParametro -> {
                        headerConfigParametro.horizontal();
                        headerConfigParametro.add(FormFieldText.create(field -> {
                            field.withoutTitle();
                            field.editable.set(false);
                            field.addStyle("dark");
                            field.value.set("Código");
                            field.alignment(Pos.CENTER);
                            field.width(80.0);
                        }).build());
                        headerConfigParametro.add(FormFieldText.create(field -> {
                            field.withoutTitle();
                            field.editable.set(false);
                            field.addStyle("dark");
                            field.value.set("Descrição");
                            field.width(700.0);
                        }).build());
                        headerConfigParametro.add(FormFieldText.create(field -> {
                            field.withoutTitle();
                            field.editable.set(false);
                            field.addStyle("dark");
                            field.value.set("Valor");
                            field.width(150.0);
                        }).build());
                    }));
                }));
                tab.add(FormBox.create(parametros -> {
                    parametros.vertical();
                    parametros.verticalScroll();
                    parametros.expanded();
                    for (SdParametros parametro : getParametros(grupo.getCodigo())) {
                        parametros.add(FormBox.create(configParametro -> {
                            configParametro.horizontal();
                            configParametro.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.editable.set(false);
                                field.value.set(String.valueOf(parametro.getCodigo()));
                                field.alignment(Pos.CENTER);
                                field.width(80.0);
                            }).build());
                            configParametro.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.editable.set(false);
                                field.value.set(parametro.getDescricao());
                                field.width(700.0);
                            }).build());
                            if (parametro.getValores() == null) {
                                configParametro.add(FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.width(150.0);
                                    field.value.set(parametro.getValor());
                                    field.keyReleased(kvt -> {
                                        if (field.value.get() != null && field.value.get().length() > 0) {
                                            parametro.setValor(field.value.get());
                                            atualizarParametro(parametro);
                                            MessageBox.create(message -> {
                                                message.message("Novo valor atribuído para o parâmetro!");
                                                message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                message.position(Pos.TOP_RIGHT);
                                                message.notification();
                                            });
                                        }
                                    });
                                }).build());
                            } else {
                                configParametro.add(FormFieldComboBox.create(String.class, field -> {
                                    field.withoutTitle();
                                    field.width(150.0);
                                    field.items(FXCollections.observableList(parametro.getValoresA()));
                                    field.select(parametro.getValor());
                                    field.getSelectionModel((observable, oldValue, newValue) -> {
                                        if (newValue != null) {
                                            parametro.setValor((String) newValue);
                                            atualizarParametro(parametro);
                                            MessageBox.create(message -> {
                                                message.message("Novo valor atribuído para o parâmetro!");
                                                message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                message.position(Pos.TOP_RIGHT);
                                                message.notification();
                                            });
                                        }
                                    });
                                }).build());
                            }
                        }));
                    }
                }));
            }));
        });
    }
}
