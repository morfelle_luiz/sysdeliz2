package sysdeliz2.views.ecommerce.b2b;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.web.WebView;
import javafx.util.Callback;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import sysdeliz2.controllers.views.ecommerce.b2b.EnvioProdutoB2BController;
import sysdeliz2.models.sysdeliz.SdProduto;
import sysdeliz2.models.ti.*;
import sysdeliz2.models.view.b2b.*;
import sysdeliz2.utils.apis.nitroecom.ServicoNitroEcom;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class EnvioProdutoB2BView extends EnvioProdutoB2BController {

    // <editor-fold defaultstate="collapsed" desc="beans">
    private final ListProperty<VB2bProdutosEnvio> produtosBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final BooleanProperty emEdicao = new SimpleBooleanProperty(false);
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="components">
    // filter
    private final FormFieldMultipleFind<Produto> fieldFilterProduto = FormFieldMultipleFind.create(Produto.class, field -> {
        field.title("Produto");
        field.width(187.0);
        field.toUpper();
    });
    private final FormFieldMultipleFind<Marca> fieldFilterMarca = FormFieldMultipleFind.create(Marca.class, field -> {
        field.title("Marca");
        field.width(110.0);
        field.toUpper();
    });
    private final FormFieldMultipleFind<Colecao> fieldFilterColecao = FormFieldMultipleFind.create(Colecao.class, field -> {
        field.title("Coleção");
        field.width(160.0);
        field.toUpper();
    });
    private final FormFieldMultipleFind<EtqProd> fieldFilterModelagem = FormFieldMultipleFind.create(EtqProd.class, field -> {
        field.title("Modelagem");
        field.toUpper();
    });
    private final FormFieldMultipleFind<Linha> fieldFilterLinha = FormFieldMultipleFind.create(Linha.class, field -> {
        field.title("Linha");
        field.toUpper();
    });
    private final FormFieldSegmentedButton<String> fieldFilterStatus = FormFieldSegmentedButton.create(field -> {
        field.title("Status");
        field.options(
                field.option("Todos", "T", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("Pendentes", "0", FormFieldSegmentedButton.Style.WARNING),
                field.option("Enviados", "4", FormFieldSegmentedButton.Style.SUCCESS)
        );
        field.select(0);
    });
    private final FormTableView<VB2bProdutosEnvio> tblProdutosEnvio = FormTableView.create(VB2bProdutosEnvio.class, table -> {
        table.title("Produtos");
        table.items.bind(produtosBean);
        table.expanded();
        table.editable.set(true);
        table.selectColumn();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VB2bProdutosEnvio, VB2bProdutosEnvio>, ObservableValue<VB2bProdutosEnvio>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getCodigo()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Código*/,
                FormTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(270.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VB2bProdutosEnvio, VB2bProdutosEnvio>, ObservableValue<VB2bProdutosEnvio>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPronome()));
                }).build() /*Descrição*/,
                FormTableColumn.create(cln -> {
                    cln.title("Marca");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VB2bProdutosEnvio, VB2bProdutosEnvio>, ObservableValue<VB2bProdutosEnvio>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDescMarca()));
                }).build() /*Marca*/,
                FormTableColumn.create(cln -> {
                    cln.title("Coleção");
                    cln.width(130.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VB2bProdutosEnvio, VB2bProdutosEnvio>, ObservableValue<VB2bProdutosEnvio>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getColecao().getDescricao()));
                }).build() /*Coleção*/,
                FormTableColumn.create(cln -> {
                    cln.title("Foto");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VB2bProdutosEnvio, VB2bProdutosEnvio>, ObservableValue<VB2bProdutosEnvio>>) param -> new ReadOnlyObjectWrapper(param.getValue().getImagem()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<VB2bProdutosEnvio, String>() {
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(item.contains("semimg.jpg") ? "Sem Imagem" : "Foto OK");
                                }
                            }
                        };
                    });
                }).build() /*Foto*/,
                FormTableColumn.create(cln -> {
                    cln.title("Loja");
                    cln.width(40.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VB2bProdutosEnvio, VB2bProdutosEnvio>, ObservableValue<VB2bProdutosEnvio>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<VB2bProdutosEnvio, VB2bProdutosEnvio>(){
                            final HBox boxButtonsRow = new HBox(3);
                            final FormButton btnAtivarProduto = FormButton.create(btn -> {
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs");
                            });
                            @Override
                            protected void updateItem(VB2bProdutosEnvio item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {

                                    if (item.getPrositua().equals("S")) {
                                        btnAtivarProduto.addStyle("danger");
                                        btnAtivarProduto.tooltip("Inativar produto na loja");
                                        btnAtivarProduto.icon(ImageUtils.getIcon(ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                                    } else {
                                        btnAtivarProduto.addStyle("success");
                                        btnAtivarProduto.tooltip("Ativar produto na loja");
                                        btnAtivarProduto.icon(ImageUtils.getIcon(ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._16));
                                    }
                                    btnAtivarProduto.setOnAction(evt -> {
                                        trocaSituacaoLoja(item);
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.setAlignment(Pos.CENTER);
                                    boxButtonsRow.getChildren().addAll(btnAtivarProduto);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build() /*Ativo*/,
                FormTableColumn.create(cln -> {
                    cln.title("Status");
                    cln.width(110.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VB2bProdutosEnvio, VB2bProdutosEnvio>, ObservableValue<VB2bProdutosEnvio>>) param -> new ReadOnlyObjectWrapper(param.getValue().getStatus()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<VB2bProdutosEnvio, String>() {
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setText(item.equals("0") ? "Pendente" : "Enviado");
                                    setGraphic(ImageUtils.getIcon((item.equals("0") ? ImageUtils.Icon.ENVIAR : ImageUtils.Icon.LOJISTA),
                                            ImageUtils.IconSize._16));
                                }
                            }
                        };
                    });
                }).build() /*Status*/
        );
        table.factoryRow(param -> {
            return new TableRow<VB2bProdutosEnvio>() {
                @Override
                protected void updateItem(VB2bProdutosEnvio item, boolean empty) {
                    super.updateItem(item, empty);
                    getStyleClass().remove("table-row-warning");
                    if (item != null && item.getStatus() != null && !empty) {
                        if (item.getStatus().equals("0"))
                            getStyleClass().add("table-row-warning");
                    }
                }
            };
        });
        table.indices(
                table.indice("Pendentes", "warning", "")
        );
        table.selectionModelItem((observable, oldValue, newValue) -> {
            if (newValue != null)
                abrirProduto((VB2bProdutosEnvio) newValue);
        });
    });
    // detalhe produto
    private final FormFieldText fieldDadosDescricaoProduto = FormFieldText.create(field -> {
        field.title("Descrição Produto");
        field.editable.bind(emEdicao);
        field.mouseClicked(evt -> liberarEdicao(evt));
    });
    private final FormFieldTextArea fieldDadosDescricaoCompleta = FormFieldTextArea.create(field -> {
        field.title("Descrição Completa");
        field.editable.bind(emEdicao);
        field.mouseClicked(evt -> liberarEdicao(evt));
        field.width(300.0);
    });
    private final FormFieldTextArea fieldDadosComposicao = FormFieldTextArea.create(field -> {
        field.title("Composição");
        field.editable.bind(emEdicao);
        field.mouseClicked(evt -> liberarEdicao(evt));
        field.width(300.0);
    });
    private final FormFieldText fieldDadosTabelaMedida = FormFieldText.create(field -> {
        field.title("Tabela de Medidas");
        field.expanded();
        field.editable.set(false);
    });
    private final FormFieldText fieldDadosVideo = FormFieldText.create(field -> {
        field.title("Vídeo");
        field.editable.bind(emEdicao);
        field.mouseClicked(evt -> liberarEdicao(evt));
        field.width(350.0);
    });
    private final FormFieldText fieldDadosUnidadeMedida = FormFieldText.create(field -> {
        field.title("U.M.");
        field.editable.set(false);
        field.width(50.0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldDadosPeso = FormFieldText.create(field -> {
        field.title("Peso");
        field.postLabel("Kg");
        field.width(100.0);
        field.editable.set(false);
        field.alignment(Pos.CENTER);
        field.mouseClicked(evt -> lerPeso(evt));
    });
    private final WebView viewImage = new WebView();{
        viewImage.setPrefWidth(150.0);
        viewImage.setPrefHeight(200.0);
    }
    private final FormBox boxInfosImagemPrincipal = FormBox.create(boxInfosImagem -> {
        boxInfosImagem.horizontal();
    });
    private final WebView viewImageHover = new WebView();{
        viewImageHover.setPrefWidth(150.0);
        viewImageHover.setPrefHeight(200.0);
    }
    private final FormBox boxInfosImagemHover = FormBox.create(boxInfosImagem -> {
        boxInfosImagem.horizontal();
    });
    private final FormListView<VB2bProdutoTabelas> listTabelas = FormListView.create(list -> {
        list.title("Tabelas");
        list.width(150.0);
        list.height(150.0);
        list.withoutCounter();
        list.listProperties().setCellFactory(param -> {
            return new ListCell<VB2bProdutoTabelas>() {
                @Override
                protected void updateItem(VB2bProdutoTabelas item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    if (item != null && !empty) {
                        setText(item.getCodigo()+" {"+StringUtils.toMonetaryFormat(item.getPreco(), 2)+"}");
                    }
                }
            };
        });
    });
    private final FormListView<VB2bProdutoAtributos> listAtributos = FormListView.create(list -> {
        list.title("Atributos");
        list.width(250.0);
        list.height(150.0);
        list.withoutCounter();
        list.listProperties().setCellFactory(param -> {
            return new ListCell<VB2bProdutoAtributos>() {
                @Override
                protected void updateItem(VB2bProdutoAtributos item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    if (item != null && !empty) {
                        setText("[" + item.getOrdematrib() + "|" + item.getAtributo() + "] " + item.getDescatrib());
                    }
                }
            };
        });
    });
    private final FormListView<VB2bProdutoCategorias> listCategorias = FormListView.create(list -> {
        list.title("Categorias");
        list.width(300.0);
        list.height(150.0);
        list.withoutCounter();
        list.listProperties().setCellFactory(param -> {
            return new ListCell<VB2bProdutoCategorias>() {
                @Override
                protected void updateItem(VB2bProdutoCategorias item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    if (item != null && !empty) {
                        setText("[" + item.getCateg() + "] " + item.getDescCateg() +
                                (item.getSecao().matches("DEF.00") ? "" : " {"+item.getSecao()+"/"+item.getDescSecao()+"}"));
                    }
                }
            };
        });
    });
    private final FormTableView<VB2bProdutoCores> tblCores = FormTableView.create(VB2bProdutoCores.class, table -> {
        table.title("Cores");
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Cor");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VB2bProdutoCores, VB2bProdutoCores>, ObservableValue<VB2bProdutoCores>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCor()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Cor*/,
                FormTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(180.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VB2bProdutoCores, VB2bProdutoCores>, ObservableValue<VB2bProdutoCores>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDescCor()));
                }).build() /*Descrição*/,
                FormTableColumn.create(cln -> {
                    cln.title("Thumb");
                    cln.width(60.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VB2bProdutoCores, VB2bProdutoCores>, ObservableValue<VB2bProdutoCores>>) param -> new ReadOnlyObjectWrapper(param.getValue().getImageCor()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<VB2bProdutoCores, String>(){
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(FormBox.create(boxImg -> {
                                        boxImg.horizontal();
                                        boxImg.add(ImageUtils.getIcon(ImageUtils.Icon.ABRIR, ImageUtils.IconSize._16));
                                        try {
                                            if (HttpClients.createDefault().execute(new HttpGet(item)).getStatusLine().getStatusCode() == 200)
                                                boxImg.add(ImageUtils.getIcon(ImageUtils.Icon.PLANETA, ImageUtils.IconSize._16));
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                            ExceptionBox.build(message -> {
                                                message.exception(e);
                                                message.showAndWait();
                                            });
                                        }
                                    }));
                                }
                            }
                        };
                    });
                }).build() /*Thumb*/,
                FormTableColumn.create(cln -> {
                    cln.title("Imagens");
                    cln.width(200.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VB2bProdutoCores, VB2bProdutoCores>, ObservableValue<VB2bProdutoCores>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<VB2bProdutoCores, VB2bProdutoCores>(){
                            @Override
                            protected void updateItem(VB2bProdutoCores item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    if (item.getFotos().size() == 0
                                            || (item.getFotos().size() == 1 && item.getFotos().get(0).getImagem().contains("semimg.jpg"))) {
                                        setText("Sem Fotos");
                                    } else {
                                        AtomicInteger fotosFtp = new AtomicInteger(0);
                                        item.getFotos().forEach(fotos -> {
                                            try {
                                                if (HttpClients.createDefault().execute(new HttpGet(fotos.getImagem().replace(" ","%20"))).getStatusLine().getStatusCode() == 200)
                                                    fotosFtp.getAndIncrement();
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                                ExceptionBox.build(message -> {
                                                    message.exception(e);
                                                    message.showAndWait();
                                                });
                                            }
                                        });
                                        setText(item.getFotos().size() + " foto(s) {"+fotosFtp.get()+" foto(s) no site}");
                                    }
                                }
                            }
                        };
                    });
                }).build() /*Imagens*/
        );
        table.selectionModelItem((observable, oldValue, newValue) -> {
            if (newValue != null){
                String htmlScript = "" +
                        "<html>" +
                        "   <header>" +
                        "       <style>" +
                        "       .container {  \n" +
                        "           display: grid;  \n" +
                        "           grid-gap: 5px;  \n" +
                        "           grid-template-columns: repeat(auto-fit, 160px);\n" +
                        "           grid-template-rows: repeat(2, 160px);  \n" +
                        "       }" +
                        "       </style>" +
                        "   </header>" +
                        "   <body>" +
                        "       <div class=\"container\">";
                for (VB2bProdutoCorFotos foto : ((VB2bProdutoCores) newValue).getFotos()) {
                    String imagem = foto.getImagem() == null
                            ? "file:///K:/Loja Virtual/imagens/produtos/semimg.jpg"
                            : foto.getImagem().replace("https://imagens.deliz.com.br/produtos/",
                                    "file:///K:/Loja Virtual/imagens/produtos/")
                            .replace("flordelis", "Flor De Lis");
                    htmlScript += "<div><img src=\""+imagem+"\" height=\"150\"  /></div>";
                }
                htmlScript += "</div></body></html>";
                this.viewImagesCores.getEngine().loadContent(htmlScript, "text/html");
            }
        });
        table.factoryRow(param -> {
            return new TableRow<VB2bProdutoCores>() {
                @Override
                protected void updateItem(VB2bProdutoCores item, boolean empty) {
                    super.updateItem(item, empty);
                    getStyleClass().remove("table-row-primary");
                    if (item != null && !empty) {
                        if (item.isCorMost())
                            getStyleClass().add("table-row-primary");
                    }
                }
            };
        });
        table.indices(
                table.indice("Cor Most.", "primary","")
        );
    });
    private final WebView viewImagesCores= new WebView();{
        viewImagesCores.setPrefWidth(500.0);
    }
    // </editor-fold>

    public EnvioProdutoB2BView() {
        super("Envio de Produtos B2B", ImageUtils.getImage(ImageUtils.Icon.PRODUTO));
        init();
    }

    private void init() {
        box.getChildren().add(FormBox.create(content -> {
            content.horizontal();
            content.expanded();
            content.add(FormBox.create(left -> {
                left.vertical();
                left.expanded();
                left.add(FormBox.create(header -> {
                    header.horizontal();
                    header.add(FormTitledPane.create(filter -> {
                        filter.filter();
                        filter.add(FormBox.create(boxFilter -> {
                            boxFilter.vertical();
                            boxFilter.expanded();
                            boxFilter.add(FormBox.create(boxFields -> {
                                boxFields.horizontal();
                                boxFields.add(fieldFilterProduto.build(), fieldFilterMarca.build(), fieldFilterColecao.build());
                            }));
                            boxFilter.add(FormBox.create(boxFields -> {
                                boxFields.horizontal();
                                boxFields.add(fieldFilterLinha.build(), fieldFilterModelagem.build(), fieldFilterStatus.build());
                            }));
                        }));
                        filter.find.setOnAction(evt -> {
                            carregarProdutos(fieldFilterProduto.objectValues.get(),
                                    fieldFilterMarca.objectValues.get(),
                                    fieldFilterColecao.objectValues.get(),
                                    fieldFilterLinha.objectValues.get(),
                                    fieldFilterModelagem.objectValues.get(),
                                    fieldFilterStatus.value.get());
                        });
                        filter.clean.setOnAction(evt -> {
                            fieldFilterProduto.clear();
                            fieldFilterMarca.clear();
                            fieldFilterColecao.clear();
                            fieldFilterModelagem.clear();
                            fieldFilterLinha.clear();
                            fieldFilterStatus.select(0);
                            produtosBean.clear();
                        });
                    }));
                }));
                left.add(tblProdutosEnvio.build());
                left.add(FormBox.create(toolbar -> {
                    toolbar.horizontal();
                    toolbar.add(FormButton.create(btn -> {
                        btn.title("Enviar");
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._24));
                        btn.addStyle("success");
                        btn.disable.bind(produtosBean.emptyProperty());
                        btn.setAction(evt -> enviarProdutos(produtosBean.get().stream()
                                .filter(VB2bProdutosEnvio::isSelected)
                                .collect(Collectors.toList())));
                    }));
                }));
            }));
            content.add(FormBox.create(right -> {
                right.horizontal();
                right.expanded();
                right.add(FormBox.create(container -> {
                    container.vertical();
                    container.expanded();
                    container.title("Dados Produto");
                    container.add(fieldDadosDescricaoProduto.build());
                    container.add(FormBox.create(boxFields -> {
                        boxFields.horizontal();
                        boxFields.height(250.0);
                        boxFields.add(fieldDadosDescricaoCompleta.build());
                        boxFields.add(fieldDadosComposicao.build());
                        boxFields.add(FormBox.create(boxImg -> {
                            boxImg.vertical();
                            boxImg.title("Imagem Principal");
                            boxImg.add(viewImage);
                            boxImg.add(boxInfosImagemPrincipal);
                        }));
                        boxFields.add(FormBox.create(boxImg -> {
                            boxImg.vertical();
                            boxImg.title("Imagem Hover");
                            boxImg.add(viewImageHover);
                            boxImg.add(boxInfosImagemHover);
                        }));
                    }));
                    container.add(FormBox.create(boxContent -> {
                        boxContent.horizontal();
                        boxContent.add(FormBox.create(boxFields -> {
                            boxFields.horizontal();
                            boxFields.expanded();
                            boxFields.alignment(Pos.BOTTOM_LEFT);
                            boxFields.add(fieldDadosTabelaMedida.build());
                            boxFields.add(FormButton.create(btn -> {
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ABRIR, ImageUtils.IconSize._16));
                                btn.addStyle("primary");
                                btn.setAction(evt -> abrirTabelaMedida(fieldDadosTabelaMedida.value.get()));
                            }));
                        }));
                        boxContent.add(FormBox.create(boxFields -> {
                            boxFields.horizontal();
                            boxFields.expanded();
                            boxFields.alignment(Pos.BOTTOM_LEFT);
                            boxFields.add(fieldDadosVideo.build());
                            boxFields.add(FormButton.create(btn -> {
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ABRIR, ImageUtils.IconSize._16));
                                btn.addStyle("primary");
                                btn.setAction(evt -> abrirVideo(fieldDadosVideo.value.get()));
                            }));
                        }));
                    }));
                    container.add(FormBox.create(boxFields -> {
                        boxFields.horizontal();
                        boxFields.height(180.0);
                        boxFields.add(listTabelas.build());
                        boxFields.add(listAtributos.build());
                        boxFields.add(listCategorias.build());
                        boxFields.add(FormBox.create(boxFields2 -> {
                            boxFields2.vertical();
                            boxFields2.add(fieldDadosPeso.build());
                            boxFields2.add(fieldDadosUnidadeMedida.build());
                        }));
                    }));
                    container.add(FormBox.create(boxFields -> {
                        boxFields.horizontal();
                        boxFields.add(tblCores.build());
                        boxFields.add(FormBox.create(boxFotosCores -> {
                            boxFotosCores.vertical();
                            boxFotosCores.title("Fotos Cores");
                            boxFotosCores.add(viewImagesCores);
                        }));
                    }));
                    container.add(FormBox.create(toolbar -> {
                        toolbar.horizontal();
                        toolbar.add(FormButton.create(btn -> {
                            btn.title("Salvar");
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                            btn.addStyle("success");
                            btn.disable.bind(emEdicao.not());
                            btn.setAction(evt -> salvarAlteracoesProduto());
                        }));
                        toolbar.add(FormButton.create(btn -> {
                            btn.title("Cancelar");
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                            btn.addStyle("danger");
                            btn.disable.bind(emEdicao.not());
                            btn.setAction(evt -> cancelarAlteracoesProduto());
                        }));
                    }));
                }));
            }));
        }));
    }

    private void cancelarAlteracoesProduto() {
        emEdicao.set(false);
        abrirProduto(tblProdutosEnvio.selectedItem());
    }

    private void salvarAlteracoesProduto() {
        VB2bProdutosEnvio produto = tblProdutosEnvio.selectedItem();

        SdProduto produtoBd = getSdProduto(tblProdutosEnvio.selectedItem().getReferencia());
        produtoBd.setVideo(fieldDadosVideo.value.get());
        produtoBd.setLongdescription(fieldDadosDescricaoCompleta.value.get());
        produtoBd.setShortdescription(fieldDadosComposicao.value.get());
        produtoBd.setMetadescription(fieldDadosDescricaoProduto.value.get());
        produtoBd.setMetatitle(fieldDadosDescricaoProduto.value.get());
        produtoBd.setDescription(fieldDadosDescricaoProduto.value.get());

        salvarDadosProduto(produtoBd);
        SysLogger.addSysDelizLog("Man. Produto B2B", TipoAcao.EDITAR,
                produto.getReferencia(), "Salvo alterações de produto para e-comerce");
        MessageBox.create(message -> {
            message.message("Dados do produto salvos com sucesso!");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
        emEdicao.set(false);
        abrirProduto(produto);
    }

    private void abrirVideo(String video) {
        if (video != null) {
            new Fragment().show(fragment -> {
                fragment.title("Vídeo Produto");
                fragment.size(600.0, 550.0);
                WebView viewVideo = new WebView();
                viewVideo.getEngine().load(video);
                fragment.box.getChildren().add(viewVideo);
            });
        }
    }

    private String getTabelaMedida(String descricao) {
        String tabela = null;
        switch (descricao) {
            case "CAMISAS, CAMISETAS, JAQUETAS":
                tabela = "CAMISAS_CAMISETAS_JAQUETAS_D";
                break;
            case "REGULAR, SLIM, RELAXED, CONFORT":
                tabela = "REGULAR_SLIM_RELAXED_CONFORT_D";
                break;
            case "SKINNY":
                tabela = "SKINNY_D";
                break;
            case "DENIM":
                tabela = "DENIM_F";
                break;
            case "OUTROS":
                tabela = "OUTROS_F";
                break;
        }

        return tabela;
    }

    private String getDescricaoTabelaMedida(String tabela) {
        String descricao = null;
        switch (tabela) {
            case "CAMISAS_CAMISETAS_JAQUETAS_D":
                descricao = "CAMISAS, CAMISETAS, JAQUETAS";
                break;
            case "REGULAR_SLIM_RELAXED_CONFORT_D":
                descricao = "REGULAR, SLIM, RELAXED, CONFORT";
                break;
            case "SKINNY_D":
                descricao = "SKINNY";
                break;
            case "DENIM_F":
                descricao = "DENIM";
                break;
            case "OUTROS_F":
                descricao = "OUTROS";
                break;
        }

        return descricao;
    }

    private void abrirTabelaMedida(String tabela) {
        if (tabela != null) {
            new Fragment().show(fragment -> {
                fragment.title("Medidas Produto");
                fragment.size(600.0, 550.0);
                WebView viewTabelaMedida = new WebView();
                viewTabelaMedida.getEngine().load(tabela);
                fragment.box.getChildren().add(FormFieldText.create(field -> {
                    field.title("Tabela");
                    field.editable(false);
                    field.value.set(getDescricaoTabelaMedida(tabela.replace(".jpg","")
                            .replace("https://imagens.deliz.com.br/produtos/tabelademedidas/","")));
                    field.mouseClicked(evt -> {
                        if (evt.getClickCount() > 1) {
                            new Fragment().show(fragmentChoose -> {
                                fragmentChoose.title("Tabela de Medidas");
                                fragmentChoose.size(600.0, 550.0);
                                WebView viewTabelaMedidaSelect = new WebView();
                                FormFieldComboBox<String> cboxTabelas = FormFieldComboBox.create(String.class, field2 -> {
                                    field2.title("Tabela");
                                    field2.width(250.0);
                                    field2.items.set(FXCollections.observableList(Arrays.asList("SELECIONE",
                                            "CAMISAS, CAMISETAS, JAQUETAS",
                                            "REGULAR, SLIM, RELAXED, CONFORT",
                                            "SKINNY",
                                            "DENIM",
                                            "OUTROS")));
                                    field2.getSelectionModel((observable, oldValue, newValue) -> {
                                        if (newValue != null) {
                                            String tabelaMedida = getTabelaMedida((String) newValue);
                                            viewTabelaMedidaSelect.getEngine().load("file:///K:/Loja Virtual/imagens/tabela_de_medidas/" + tabelaMedida + ".jpg");
                                        }
                                    });
                                });

                                fragmentChoose.box.getChildren().add(cboxTabelas.build());
                                fragmentChoose.box.getChildren().add(viewTabelaMedidaSelect);

                                fragmentChoose.buttonsBox.getChildren().add(FormButton.create(btn -> {
                                    btn.title("Salvar");
                                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                                    btn.addStyle("success");
                                    btn.setAction(evt2 -> {
                                        salvarTabelaMedida(tblProdutosEnvio.selectedItem(), getTabelaMedida(cboxTabelas.value.get()));
                                        fragmentChoose.close();
                                        fragment.close();
                                    });
                                }));
                            });
                        }
                    });
                }).build());
                fragment.box.getChildren().add(viewTabelaMedida);
            });
        } else {
            if (tblProdutosEnvio.selectedItem() != null) {
                if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                        message.message("Não existe uma tabela de medidas cadastrada para o produto, deseja cadastrar?");
                        message.showAndWait();
                    }).value.get())) {
                    new Fragment().show(fragment -> {
                        fragment.title("Tabela de Medidas");
                        fragment.size(600.0, 550.0);
                        WebView viewTabelaMedida = new WebView();
                        FormFieldComboBox<String> cboxTabelas = FormFieldComboBox.create(String.class, field -> {
                            field.title("Tabela");
                            field.width(250.0);
                            field.items.set(FXCollections.observableList(Arrays.asList("SELECIONE",
                                    "CAMISAS, CAMISETAS, JAQUETAS",
                                    "REGULAR, SLIM, RELAXED, CONFORT",
                                    "SKINNY",
                                    "DENIM",
                                    "OUTROS")));
                            field.getSelectionModel((observable, oldValue, newValue) -> {
                                if (newValue != null) {
                                    String tabelaMedida = getTabelaMedida((String) newValue);
                                    viewTabelaMedida.getEngine().load("file:///K:/Loja Virtual/imagens/tabela_de_medidas/" + tabelaMedida + ".jpg");
                                }
                            });
                        });

                        fragment.box.getChildren().add(cboxTabelas.build());
                        fragment.box.getChildren().add(viewTabelaMedida);

                        fragment.buttonsBox.getChildren().add(FormButton.create(btn -> {
                            btn.title("Salvar");
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                            btn.addStyle("success");
                            btn.setAction(evt -> {
                                salvarTabelaMedida(tblProdutosEnvio.selectedItem(), getTabelaMedida(cboxTabelas.value.get()));
                                fragment.close();
                            });
                        }));
                    });
                }
            }
        }
    }

    private void salvarTabelaMedida(VB2bProdutosEnvio produto, String tabela) {
        SdProduto produtoBd = getSdProduto(produto.getReferencia());
        produtoBd.setTabelamedida(tabela);
        salvarDadosProduto(produtoBd);
        fieldDadosTabelaMedida.value.set("https://imagens.deliz.com.br/produtos/tabelademedidas/"+tabela+".jpg");
        SysLogger.addSysDelizLog("Man. Produto B2B", TipoAcao.CADASTRAR, produto.getReferencia(),
                "Incluído tabela de medidade "+ tabela +" no produto.");
    }

    private void limparDadosProduto() {
        fieldDadosDescricaoCompleta.clear();
        fieldDadosComposicao.clear();
        fieldDadosTabelaMedida.clear();
        fieldDadosVideo.clear();
        fieldDadosUnidadeMedida.clear();
        fieldDadosPeso.clear();
    }

    private void carregarProdutos(ObservableList<Produto> produtos,
                                  ObservableList<Marca> marcas,
                                  ObservableList<Colecao> colecoes,
                                  ObservableList<Linha> linhas,
                                  ObservableList<EtqProd> modelagens,
                                  String status) {
        AtomicReference<List<VB2bProdutosEnvio>> loadProdutos = new AtomicReference<>();
        JPAUtils.clearEntitys(produtosBean.get());
        new RunAsyncWithOverlay(this).exec(task -> {
            loadProdutos.set(carregarProdutos(produtos.stream().map(Produto::getCodigo).toArray(),
                    marcas.stream().map(Marca::getCodigo).toArray(),
                    colecoes.stream().map(Colecao::getCodigo).toArray(),
                    linhas.stream().map(Linha::getCodigo).toArray(),
                    modelagens.stream().map(EtqProd::getCodigo).toArray(),
                    status));
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                produtosBean.set(FXCollections.observableList(loadProdutos.get()));
            }
        });
    }

    private void abrirProduto(VB2bProdutosEnvio produto) {
        limparDadosProduto();
        if (produto == null)
            return;

        JPAUtils.getEntityManager().refresh(produto);
        // dados do produto
        fieldDadosDescricaoProduto.value.set(produto.getPronome());
        fieldDadosDescricaoCompleta.value.set(produto.getProcarac());
        fieldDadosComposicao.value.set(produto.getProdesc());
        fieldDadosTabelaMedida.value.set(produto.getProesptec());
        fieldDadosVideo.value.set(produto.getProvideo());
        fieldDadosUnidadeMedida.value.set(produto.getProum());
        fieldDadosPeso.value.set(StringUtils.toDecimalFormat(produto.getPropeso()));

        // imagens
        viewImage.getEngine().load(produto.getImagem()
                .replace("https://imagens.deliz.com.br/produtos/",
                        "file:///K:/Loja Virtual/imagens/produtos/")
                .replace("flordelis", "Flor De Lis"));
        boxInfosImagemPrincipal.clear();
        if (!produto.getImagem().contains("semimg.jpg")) {
            boxInfosImagemPrincipal.add(ImageUtils.getIcon(ImageUtils.Icon.ABRIR, ImageUtils.IconSize._16));
            try {
                if (HttpClients.createDefault().execute(new HttpGet(produto.getImagem().replace(" ","%20"))).getStatusLine().getStatusCode() == 200)
                    boxInfosImagemPrincipal.add(ImageUtils.getIcon(ImageUtils.Icon.PLANETA, ImageUtils.IconSize._16));
            } catch (IOException e) {
                e.printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(e);
                    message.showAndWait();
                });
            }
        }
        viewImageHover.getEngine().load(produto.getImagemHover() == null
                ? "file:///K:/Loja Virtual/imagens/produtos/semimg.jpg"
                : produto.getImagemHover().replace("https://imagens.deliz.com.br/produtos/",
                                "file:///K:/Loja Virtual/imagens/produtos/")
                        .replace("flordelis", "Flor De Lis"));
        boxInfosImagemHover.clear();
        if (produto.getImagemHover() != null && !produto.getImagemHover().contains("semimg.jpg")) {
            boxInfosImagemHover.add(ImageUtils.getIcon(ImageUtils.Icon.ABRIR, ImageUtils.IconSize._16));
            try {
                if (HttpClients.createDefault().execute(new HttpGet(produto.getImagemHover().replace(" ","%20"))).getStatusLine().getStatusCode() == 200)
                    boxInfosImagemHover.add(ImageUtils.getIcon(ImageUtils.Icon.PLANETA, ImageUtils.IconSize._16));
            } catch (IOException e) {
                e.printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(e);
                    message.showAndWait();
                });
            }
        }


        // tabelas, atributos e categorias
        listTabelas.items.set(FXCollections.observableList(produto.getTabelas()));
        listAtributos.items.set(FXCollections.observableList(produto.getAtributos()));
        listCategorias.items.set(FXCollections.observableList(produto.getCategorias()));

        // cores
        tblCores.items.set(FXCollections.observableList(produto.getReferencias()));
        viewImagesCores.getEngine().loadContent("<html><body></body></hml>", "text/html");
    }

    private void liberarEdicao(MouseEvent evt) {
        if (evt.getClickCount() > 1)
            emEdicao.set(true);
    }

    private void lerPeso(MouseEvent evt) {

    }

    private void trocaSituacaoLoja(VB2bProdutosEnvio item) {
        AtomicReference<Exception> exc = new AtomicReference<>();
        new RunAsyncWithOverlay(this).exec(task -> {
            try {
                item.setPrositua(item.getPrositua().equals("S") ? "N" : "S");
                if (!new ServicoNitroEcom().post(Arrays.asList(item)))
                    return ReturnAsync.ERROR.value;

                trocaSituacaoLoja(item.getPrositua(), item.getReferencia());
                JPAUtils.getEntityManager().refresh(item);
                SysLogger.addSysDelizLog("Man. Produto B2B", TipoAcao.EDITAR, item.getReferencia(),
                        "Alterado situação do produto " + item.getReferencia() + " na loja B2B para o status " + item.getPrositua());

                return ReturnAsync.OK.value;
            } catch (HttpResponseException e) {
                e.printStackTrace();
                exc.set(e);
                return ReturnAsync.NOT_FOUND.value;
            } catch (IOException e) {
                e.printStackTrace();
                exc.set(e);
                return ReturnAsync.EXCEPTION.value;
            } catch (SQLException e) {
                e.printStackTrace();
                exc.set(e);
                return ReturnAsync.EXCEPTION.value;
            }

        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                tblProdutosEnvio.refresh();
                MessageBox.create(message -> {
                    message.message("Aletardo status do produto na loja.");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            } else if (taskReturn.equals(ReturnAsync.ERROR.value)) {
                MessageBox.create(message -> {
                    message.message("Nenhum produto foi enviado para a loja.");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showAndWait();
                });
            } else if (taskReturn.equals(ReturnAsync.NOT_FOUND.value)) {
                MessageBox.create(message -> {
                    message.message("Ocorreu um erro ao enviar o produto para a loja. Código do ERRO: "+((HttpResponseException) exc.get()).getStatusCode());
                    message.type(MessageBox.TypeMessageBox.ERROR);
                    message.showAndWait();
                });
            }else if (taskReturn.equals(ReturnAsync.EXCEPTION.value)) {
                exc.get().printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(exc.get());
                    message.showAndWait();
                });
            }
        });
    }

    private void enviarProdutos(List<VB2bProdutosEnvio> produtosSelecionados) {
        if (produtosSelecionados.size() == 0) {
            MessageBox.create(message -> {
                message.message("É necessário selecionar os produtos que serão enviados.");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showAndWait();
            });
            return;
        }

        AtomicReference<Exception> exc = new AtomicReference<>();
        new RunAsyncWithOverlay(this).exec(task -> {
            try {
                if (!new ServicoNitroEcom().post(produtosSelecionados))
                    return ReturnAsync.ERROR.value;
            } catch (HttpResponseException e) {
                e.printStackTrace();
                exc.set(e);
                return ReturnAsync.NOT_FOUND.value;
            } catch (IOException e) {
                e.printStackTrace();
                exc.set(e);
                return ReturnAsync.EXCEPTION.value;
            }

            atualizaStatusProdutos(produtosSelecionados);
            produtosSelecionados.forEach(produto -> JPAUtils.getEntityManager().refresh(produto));
            SysLogger.addSysDelizLog("Man. Produto B2B", TipoAcao.ENVIAR, "B2B",
                    "Enviados os produto para loja: "+produtosSelecionados.stream().map(VB2bProdutosEnvio::getReferencia)
                            .collect(Collectors.joining(",")));
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                tblProdutosEnvio.refresh();
                MessageBox.create(message -> {
                    message.message("Enviado(s) " + produtosSelecionados.size() + " produto(s) para a loja.");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            } else if (taskReturn.equals(ReturnAsync.ERROR.value)) {
                MessageBox.create(message -> {
                    message.message("Nenhum produto voi enviado para a loja.");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showAndWait();
                });
            } else if (taskReturn.equals(ReturnAsync.NOT_FOUND.value)) {
                MessageBox.create(message -> {
                    message.message("Ocorreu um erro ao enviar o produto para a loja. Código do ERRO: "+((HttpResponseException) exc.get()).getStatusCode());
                    message.type(MessageBox.TypeMessageBox.ERROR);
                    message.showAndWait();
                });
            }else if (taskReturn.equals(ReturnAsync.EXCEPTION.value)) {
                exc.get().printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(exc.get());
                    message.showAndWait();
                });
            }
        });
    }
}
