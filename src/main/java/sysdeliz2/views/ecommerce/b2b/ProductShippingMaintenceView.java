package sysdeliz2.views.ecommerce.b2b;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import sysdeliz2.controllers.views.ecommerce.b2b.ProductShippingMaintenceController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.models.sysdeliz.ecommerce.b2b.SdProdutoCorTabela;
import sysdeliz2.models.sysdeliz.ecommerce.b2b.SdProdutoCorTabelaPK;
import sysdeliz2.models.ti.*;
import sysdeliz2.models.view.VSdEstoqueB2B;
import sysdeliz2.models.view.VSdMrpEstoqueB2b;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;

import javax.persistence.EntityExistsException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;


public class ProductShippingMaintenceView extends ProductShippingMaintenceController {

    // <editor-fold defaultstate="collapsed" desc="beans">
    private final StringProperty beanProdutoSelecionado = new SimpleStringProperty("");
    private final ListProperty<SdProdutoCorTabela> beanTabelasProdutoCor = new SimpleListProperty<>(FXCollections.observableArrayList());
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaration: Filters Fields">
    private final FormFieldMultipleFind<Colecao> colecaoFilterField = FormFieldMultipleFind.create(Colecao.class, field -> {
        field.title("Coleção");
        field.toUpper();
    });
    private final FormFieldMultipleFind<Marca> marcaFilterField = FormFieldMultipleFind.create(Marca.class, field -> {
        field.title("Marca");
        field.toUpper();
    });
    private final FormFieldMultipleFind<Produto> produtoFilterField = FormFieldMultipleFind.create(Produto.class, field -> {
        field.title("Produto");
        field.toUpper();
    });
    private final FormFieldMultipleFind<TabPrz> entregaFilterField = FormFieldMultipleFind.create(TabPrz.class, field -> {
        field.title("Entrega");
        field.toUpper();
    });
    private final FormFieldMultipleFind<Regiao> tabPrecoField = FormFieldMultipleFind.create(Regiao.class, field -> {
        field.title("Tab. Preço");
        field.toUpper();
    });
    private final FormLabel fieldProdutoCorSelecionado = FormLabel.create(lb -> {
        lb.boldText();
        lb.value.bind(beanProdutoSelecionado);
    });
    private final FormFieldMultipleFind<Regiao> fieldTabelaPreco = FormFieldMultipleFind.create(Regiao.class, field -> {
        field.title("Tabela Preço");
        field.toUpper();
        field.editable.bind(beanProdutoSelecionado.isEqualTo("").not());
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaration: Data Fields">
    private final FormTableView<VSdMrpEstoqueB2b> tblEntregas = FormTableView.create(VSdMrpEstoqueB2b.class, table -> {
        table.expanded();
        table.editable.set(true);
        table.items.bind(shippingProductsSelectSize);
        table.selectColumn();
        FormTableColumn<VSdMrpEstoqueB2b, String> clnProduto = FormTableColumn.create(cln -> {
            cln.title("Produto");
            cln.width(60);
            cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpEstoqueB2b, String>, ObservableValue<String>>) features -> new ReadOnlyObjectWrapper(features.getValue().getCodigo()));
        });
        FormTableColumn<VSdMrpEstoqueB2b, String> clnDescricao = FormTableColumn.create(cln -> {
            cln.title("Descrição");
            cln.width(210);
            cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpEstoqueB2b, String>, ObservableValue<String>>) features -> new ReadOnlyObjectWrapper(features.getValue().getDescricao()));
        });
        FormTableColumn<VSdMrpEstoqueB2b, String> clnCor = FormTableColumn.create(cln -> {
            cln.title("Cód. Cor");
            cln.width(50);
            cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpEstoqueB2b, String>, ObservableValue<String>>) features -> new ReadOnlyObjectWrapper(features.getValue().getCor()));
        });
        FormTableColumn<VSdMrpEstoqueB2b, String> clnDescCor = FormTableColumn.create(cln -> {
            cln.title("Cor");
            cln.width(150);
            cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpEstoqueB2b, String>, ObservableValue<String>>) features -> new ReadOnlyObjectWrapper(features.getValue().getDescCor()));
        });
        FormTableColumn<VSdMrpEstoqueB2b, String> clnEntrega = FormTableColumn.create(cln -> {
            cln.title("Entrega");
            cln.alignment(FormTableColumn.Alignment.CENTER);
            cln.width(60);
            cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpEstoqueB2b, String>, ObservableValue<String>>) features -> new ReadOnlyObjectWrapper(features.getValue().getEntrega()));
        });
        FormTableColumn<VSdMrpEstoqueB2b, String> clnDescEntrega = FormTableColumn.create(cln -> {
            cln.title("Descrição");
            cln.width(200);
            cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpEstoqueB2b, String>, ObservableValue<String>>) features -> new ReadOnlyObjectWrapper(features.getValue().getDescEntrega()));
        });
        FormTableColumn<VSdMrpEstoqueB2b, LocalDate> clnDtInicio = FormTableColumn.create(cln -> {
            cln.title("Início");
            cln.width(80);
            cln.alignment(FormTableColumn.Alignment.CENTER);
            cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpEstoqueB2b, LocalDate>, ObservableValue<LocalDate>>) features -> new ReadOnlyObjectWrapper(features.getValue().getDtInicio()));
            cln.format(param -> {
                return new TableCell<VSdMrpEstoqueB2b, LocalDate>() {
                    @Override
                    protected void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(null);
                        if (item != null && !empty)
                            setText(StringUtils.toDateFormat(item));
                    }
                };
            });
        });
        FormTableColumn<VSdMrpEstoqueB2b, LocalDate> clnDtFim = FormTableColumn.create(cln -> {
            cln.title("Fim");
            cln.width(80);
            cln.alignment(FormTableColumn.Alignment.CENTER);
            cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpEstoqueB2b, LocalDate>, ObservableValue<LocalDate>>) features -> new ReadOnlyObjectWrapper(features.getValue().getDtFim()));
            cln.format(param -> {
                return new TableCell<VSdMrpEstoqueB2b, LocalDate>() {
                    @Override
                    protected void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(null);
                        if (item != null && !empty)
                            setText(StringUtils.toDateFormat(item));
                    }
                };
            });
        });
        table.addColumn(clnProduto);
        table.addColumn(clnDescricao);
        table.addColumn(clnCor);
        table.addColumn(clnDescCor);
        table.addColumn(clnEntrega);
        table.addColumn(clnDescEntrega);
        table.addColumn(clnDtInicio);
        table.addColumn(clnDtFim);

        table.selectionModelItem((observable, oldValue, newValue) -> {
            if (newValue != null) {
                JPAUtils.clearEntitys(beanTabelasProdutoCor.get());
                beanTabelasProdutoCor.clear();
                beanProdutoSelecionado.set("");
                AtomicReference<List<SdProdutoCorTabela>> tabelasProdutoCor = new AtomicReference<>();
                new RunAsyncWithOverlay(this).exec(task -> {
                    tabelasProdutoCor.set(getTabelasProdutoCor((VSdMrpEstoqueB2b) newValue));
                    return ReturnAsync.OK.value;
                }).addTaskEndNotification(taskReturn -> {
                    if (taskReturn.equals(ReturnAsync.OK.value)) {
                        beanTabelasProdutoCor.set(FXCollections.observableList(tabelasProdutoCor.get()));
                        VSdMrpEstoqueB2b produtoCorSelecionado = (VSdMrpEstoqueB2b) newValue;
                        beanProdutoSelecionado.set("[" + produtoCorSelecionado.getCodigo() + "] " + produtoCorSelecionado.getDescricao() +
                                " - [" + produtoCorSelecionado.getCor() + "] " + produtoCorSelecionado.getDescCor());
                    }
                });
            }
        });
    });
    private final FormFieldComboBox<String> faixaField = FormFieldComboBox.create(String.class, combo -> {
        combo.title("Faixa Tam.");
        combo.items.bind(sizesProducts);
        combo.getSelectionModel((observable, oldValue, newValue) -> {
            if (newValue != null) {
                selectSize((String) newValue);
                makeGrade(((String) newValue).split(" - ")[0]);
            }
        });
    });
    private final FormTableView<SdProdutoCorTabela> tblTabelasProdutoCor = FormTableView.create(SdProdutoCorTabela.class, table -> {
        table.title("Tabelas do Produto/Cor");
        table.expanded();
        table.items.bind(beanTabelasProdutoCor);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdProdutoCorTabela, SdProdutoCorTabela>, ObservableValue<SdProdutoCorTabela>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getTabela().getRegiao()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Código*/,
                FormTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(300.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdProdutoCorTabela, SdProdutoCorTabela>, ObservableValue<SdProdutoCorTabela>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getTabela().getDescricao()));
                }).build() /*Descrição*/,
                FormTableColumn.create(cln -> {
                    cln.title("");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(50.0);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdProdutoCorTabela, SdProdutoCorTabela>, ObservableValue<SdProdutoCorTabela>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdProdutoCorTabela, SdProdutoCorTabela>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnExcluir = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                btn.tooltip("Excluir Tabela");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("danger");
                            });

                            @Override
                            protected void updateItem(SdProdutoCorTabela item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    btnExcluir.setOnAction(evt -> {
                                        if (!((Boolean) QuestionBox.build(Boolean.class, message -> {
                                            message.message("Deseja realmente excluir a tabela?");
                                            message.showAndWait();
                                        }).value.get())) {
                                            return;
                                        }
                                        excluirTabela(item);
                                        beanTabelasProdutoCor.remove(item);
                                        tblTabelasProdutoCor.refresh();
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnExcluir);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build()
        );
    });
    // </editor-fold>

    public ProductShippingMaintenceView() {
        super("Manutenção de Entrega de Produtos", ImageUtils.getImage(ImageUtils.Icon.CALENDARIO_PRODUTO));
        init();
    }

    private void init() {
        super.box.getChildren().add(FormBox.create(boxFilter -> {
            boxFilter.horizontal();
            boxFilter.width(600.0);
            boxFilter.add(FormTitledPane.create(filter -> {
                filter.filter();
                filter.add(FormBox.create(box -> {
                    box.horizontal();
                    box.add(FormBox.create(boxColecaoMarca -> {
                        boxColecaoMarca.vertical();
                        boxColecaoMarca.add(colecaoFilterField.build());
                        boxColecaoMarca.add(marcaFilterField.build());
                    }));
                    box.add(FormBox.create(boxProdutoEntrega -> {
                        boxProdutoEntrega.vertical();
                        boxProdutoEntrega.add(produtoFilterField.build());
                        boxProdutoEntrega.add(entregaFilterField.build());
                    }));
                    box.add(FormBox.create(boxTabelaPreco -> {
                        boxTabelaPreco.vertical();
                        boxTabelaPreco.add(tabPrecoField.build());
                    }));
                }));
                filter.clean.setOnAction(evt -> {
                    clearDados();
                    tabPrecoField.clear();
                    colecaoFilterField.clear();
                    marcaFilterField.clear();
                    produtoFilterField.clear();
                    entregaFilterField.clear();
                });
                filter.find.setOnAction(evt -> {
                    clearDados();
                    new RunAsyncWithOverlay(this).exec(task -> {
                        filterShippingProducts(
                                colecaoFilterField.textValue.get() == null ? new String[]{} : colecaoFilterField.textValue.get().split(","),
                                marcaFilterField.textValue.get() == null ? new String[]{} : marcaFilterField.textValue.get().split(","),
                                produtoFilterField.textValue.get() == null ? new String[]{} : produtoFilterField.textValue.get().split(","),
                                entregaFilterField.textValue.get() == null ? new String[]{} : entregaFilterField.textValue.get().split(","),
                                tabPrecoField.textValue.get() == null ? new String[]{} : tabPrecoField.textValue.get().split(","));
                        return new Integer(1);
                    });
                });
            }));
        }));
        super.box.getChildren().add(FormBox.create(boxLists -> {
            boxLists.horizontal();
            boxLists.expanded();
//            boxLists.add(FormBox.create(boxListProdutos -> {
//                boxListProdutos.title("Produtos");
//                boxListProdutos.vertical();
//                boxListProdutos.add(FormFieldToggleSingle.create(select -> {
//                    select.title("Selecionar tudo");
//                    select.value.addListener((observable, oldValue, newValue) -> {
//                        if (newValue)
//                            lviewProdutos.getSelectionModel().selectAll();
//                        else
//                            lviewProdutos.getSelectionModel().clearSelection();
//                    });
//                }).build());
//
//                VBox.setVgrow(lviewProdutos, Priority.ALWAYS);
//                lviewProdutos.itemsProperty().bind(shippingProducts);
//                lviewProdutos.setCellFactory(param -> {
//                    return new ListCell<VSdMrpEstoqueB2b>(){
//                        @Override
//                        protected void updateItem(VSdMrpEstoqueB2b item, boolean empty) {
//                            super.updateItem(item, empty);
//                            setText(null);
//
//                            if(item != null && !empty)
//                                setText(item.toProduto());
//                        }
//                    };
//                });
//                lviewProdutos.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
//                boxListProdutos.add(lviewProdutos);
//            }));
//            boxLists.add(FormBox.create(boxListCores -> {
//                boxListCores.title("Cores");
//                boxListCores.vertical();
//                boxListCores.add(FormFieldToggleSingle.create(select -> {
//                    select.title("Selecionar tudo");
//                    select.value.set(false);
//                    select.value.addListener((observable, oldValue, newValue) -> {
//                        if (newValue)
//                            lviewCores.getSelectionModel().selectAll();
//                        else
//                            lviewCores.getSelectionModel().clearSelection();
//                    });
//                }).build());
//
//                VBox.setVgrow(lviewCores, Priority.ALWAYS);
//                lviewCores.setCellFactory(param -> {
//                    return new ListCell<VSdMrpEstoqueB2b>(){
//                        @Override
//                        protected void updateItem(VSdMrpEstoqueB2b item, boolean empty) {
//                            super.updateItem(item, empty);
//                            setText(null);
//
//                            if(item != null && !empty)
//                                setText(item.toCor());
//                        }
//                    };
//                });
//                lviewCores.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
//                boxListCores.add(lviewCores);
//            }));
            boxLists.add(FormBox.create(boxListEntregas -> {
                boxListEntregas.title("Entregas");
                boxListEntregas.vertical();
                boxListEntregas.add(faixaField.build());
                VBox.setVgrow(tblEntregas.build(), Priority.ALWAYS);
                boxListEntregas.add(tblEntregas.build());
                boxListEntregas.add(FormFieldComboBox.create(String.class, field -> {
                    field.title("Selecionar");
                    field.width(300);
                    field.items.set(FXCollections.observableArrayList("Escolha...", "Somente c/ Estoque", "Somente s/ Estoque", "Desmarcar c/ Estoque", "Desmarcar s/ Estoque", "Marcar Todos", "Desmarcar Todos", "Marcar c/ Estoque ou Negativado"));
                    field.select(0);
                    field.getSelectionModel((observable, oldValue, newValue) -> {
                        if (newValue != null && !newValue.equals("Escolha...")) {
                            switch ((String) newValue) {
                                case "Somente c/ Estoque":
                                    shippingProductsSelectSize.stream().filter(prodEntrega -> prodEntrega.getTamanhos().stream().filter(it -> it.getQtdeOrig() != null).anyMatch(it -> it.getQtdeOrig() > 0)).forEach(prodEntrega -> prodEntrega.setSelected(true));
                                    break;
                                case "Somente s/ Estoque":
                                    shippingProductsSelectSize.stream().filter(prodEntrega -> prodEntrega.getTamanhos().stream().filter(it -> it.getQtdeOrig() != null).anyMatch(it -> it.getQtdeOrig() <= 0)).forEach(prodEntrega -> prodEntrega.setSelected(true));
                                    break;
                                case "Desmarcar c/ Estoque":
                                    shippingProductsSelectSize.stream().filter(prodEntrega -> prodEntrega.getTamanhos().stream().filter(it -> it.getQtdeOrig() != null).anyMatch(it -> it.getQtdeOrig() > 0)).forEach(prodEntrega -> prodEntrega.setSelected(false));
                                    break;
                                case "Desmarcar s/ Estoque":
                                    shippingProductsSelectSize.stream().filter(prodEntrega -> prodEntrega.getTamanhos().stream().filter(it -> it.getQtdeOrig() != null).anyMatch(it -> it.getQtdeOrig() <= 0)).forEach(prodEntrega -> prodEntrega.setSelected(false));
                                    break;
                                case "Marcar Todos":
                                    shippingProductsSelectSize.forEach(prodEntrega -> prodEntrega.setSelected(true));
                                    break;
                                case "Desmarcar Todos":
                                    shippingProductsSelectSize.forEach(prodEntrega -> prodEntrega.setSelected(false));
                                    break;
                                case "Marcar c/ Estoque ou Negativado":
                                    shippingProductsSelectSize.stream().filter(prodEntrega -> prodEntrega.getTamanhos().stream().filter(it -> it.getQtdeOrig() != null).anyMatch(it -> it.getQtdeOrig() > 0 || it.getQtdeOrig() < 0)).forEach(prodEntrega -> prodEntrega.setSelected(true));
                                    break;
                            }
                            //field.clearSelection();
                        }
                    });
                }).build());
            }));
            boxLists.add(FormBox.create(boxButtons -> {
                boxButtons.vertical();
                //boxButtons.alignment(Pos.BOTTOM_LEFT);
                boxButtons.add(FormBox.create(boxTabelasCor -> {
                    boxTabelasCor.vertical();
                    boxTabelasCor.expanded();
                    boxTabelasCor.title("Tabelas Produto/Cor");
                    boxTabelasCor.add(fieldProdutoCorSelecionado);
                    boxTabelasCor.add(FormBox.create(boxTabelaPreco -> {
                        boxTabelaPreco.horizontal();
                        boxTabelaPreco.alignment(Pos.BOTTOM_LEFT);
                        boxTabelaPreco.add(fieldTabelaPreco.build());
                        boxTabelaPreco.add(FormButton.create(btnAdicionar -> {
                            btnAdicionar.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._32));
                            btnAdicionar.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            btnAdicionar.addStyle("lg").addStyle("success");
                            btnAdicionar.disable.bind(beanProdutoSelecionado.isEqualTo(""));
                            btnAdicionar.setAction(evt -> adicionarTabela(tblEntregas.selectedItem(),
                                    fieldTabelaPreco.objectValues.get()));
                        }));
                    }));
                    boxTabelasCor.add(tblTabelasProdutoCor.build());
                    boxTabelasCor.add(FormButton.create(btnExcluirTodos -> {
                        btnExcluirTodos.title("Excluir Todos");
                        btnExcluirTodos.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
                        btnExcluirTodos.addStyle("danger");
                        btnExcluirTodos.width(150.0);
                        btnExcluirTodos.disable.bind(beanTabelasProdutoCor.emptyProperty());
                        btnExcluirTodos.setAction(evt -> {
                            if (!((Boolean) QuestionBox.build(Boolean.class, message -> {
                                message.message("Deseja realmente excluir todas as tabelas?");
                                message.showAndWait();
                            }).value.get())) {
                                return;
                            }
                            beanTabelasProdutoCor.forEach(this::excluirTabela);
                            beanTabelasProdutoCor.clear();
                            tblTabelasProdutoCor.refresh();
                        });
                    }));
                }));
                boxButtons.add(FormButton.create(btnSave -> {
                    btnSave.title("Salvar");
                    btnSave.disable.bind(tblEntregas.tableProperties().itemsProperty().isNull());
                    btnSave.icon(new Image(getClass().getResource("/images/icons/buttons/select (2).png").toExternalForm()));
                    btnSave.addStyle("lg");
                    btnSave.addStyle("success");
                    btnSave.setAction(evt -> {
                        AtomicReference<SQLException> exc = new AtomicReference<>();
                        new RunAsyncWithOverlay(this).exec(task -> {
                            try {
                                saveProducts();
                            } catch (SQLException e) {
                                e.printStackTrace();
                                exc.set(e);
                                return ReturnAsync.EXCEPTION.value;
                            }
                            return ReturnAsync.OK.value;
                        }).addTaskEndNotification(taskReturn -> {
                            if (taskReturn.equals(ReturnAsync.OK.value)) {
                                MessageBox.create(message -> {
                                    message.message("Entrega de produtos cadastrada com sucesso.");
                                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                                    message.position(Pos.TOP_RIGHT);
                                    message.notification();
                                });
                            } else if (taskReturn.equals(ReturnAsync.EXCEPTION.value)) {
                                ExceptionBox.build(message -> {
                                    message.exception(exc.get());
                                    message.showAndWait();
                                });
                            }
                        });
                    });
                }));
            }));
        }));
    }

    private void clearDados() {
        JPAUtils.clearEntitys(shippingProducts.get());
        JPAUtils.clearEntitys(beanTabelasProdutoCor.get());

        shippingProducts.clear();
        shippingProductsSelectSize.clear();
        sizesProducts.clear();

        tblEntregas.tableProperties().getSelectionModel().clearSelection();
        faixaField.clearSelection();

        beanTabelasProdutoCor.clear();
        beanProdutoSelecionado.set("");
    }

    private void adicionarTabela(VSdMrpEstoqueB2b produtoCor, ObservableList<Regiao> tabelas) {
        if (tabelas.size() > 0) {
            tabelas.forEach(tabela -> {
                SdProdutoCorTabelaPK idTabela = new SdProdutoCorTabelaPK();
                idTabela.setTabela(tabela);
                idTabela.setCodigo(produtoCor.getCodigo());
                idTabela.setCor(produtoCor.getCor());
                SdProdutoCorTabela novaTabela = new SdProdutoCorTabela();
                novaTabela.setId(idTabela);
                try {
                    adicionarTabela(novaTabela);
                    beanTabelasProdutoCor.add(novaTabela);
                } catch (EntityExistsException e) {
                    System.out.println("add exist table");
                } catch (SQLException e) {
                    e.printStackTrace();
                    ExceptionBox.build(message -> {
                        message.exception(e);
                        message.showAndWait();
                    });
                }
            });
        }
        tblTabelasProdutoCor.refresh();
        fieldTabelaPreco.clear();
    }

    private void excluirTabela(SdProdutoCorTabela tabela) {
        new FluentDao().delete(tabela);
    }

    private void makeGrade(String faixa) {
        if (tblEntregas.tableProperties().getColumns().size() > 9)
            tblEntregas.removeColumn(9, tblEntregas.tableProperties().getColumns().size());
        tblEntregas.tableProperties().refresh();
        List<FaixaItem> grade = (List<FaixaItem>) new FluentDao().selectFrom(FaixaItem.class).where(it -> it.equal("faixaItemId.faixa", faixa)).orderBy("posicao", OrderType.ASC).resultList();
        grade.forEach(tam -> {
            FormTableColumn<VSdMrpEstoqueB2b, Integer> clnGrade = FormTableColumn.create(cln -> {
                cln.title(tam.getFaixaItemId().getTamanho());
                cln.alignment(FormTableColumn.Alignment.CENTER);
                cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpEstoqueB2b, Integer>, ObservableValue<Integer>>) features -> {
                    VSdEstoqueB2B estoque = features.getValue().getTamanho(tam.getFaixaItemId().getTamanho());
                    return new ReadOnlyObjectWrapper(estoque == null ? 0 : estoque.getQtdeOrig());
                });
                cln.format(param -> {
                    return new TableCell<VSdMrpEstoqueB2b, Integer>() {
                        @Override
                        protected void updateItem(Integer item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            clean();

                            if (item != null && !empty) {
                                setText(StringUtils.toIntegerFormat(item));
                                if (item <= 0)
                                    getStyleClass().add("table-row-danger");
                            }
                        }

                        private void clean() {
                            getStyleClass().remove("table-row-danger");
                        }
                    };
                });
            });
            tblEntregas.addColumn(clnGrade);
        });
    }
}
