package sysdeliz2.views.ecommerce.b2c;

import javafx.application.Platform;
import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebView;
import javafx.util.Callback;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import sysdeliz2.controllers.views.ecommerce.b2c.EnvioProdutoB2CController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.sysdeliz.SdProduto;
import sysdeliz2.models.ti.*;
import sysdeliz2.models.view.b2b.*;
import sysdeliz2.models.view.b2c.VB2cProdutoCores;
import sysdeliz2.models.view.b2c.VB2cProdutosEnvio;
import sysdeliz2.utils.SortUtils;
import sysdeliz2.utils.apis.b2cEcom.ServicoB2CEcom;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.SysLogger;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.SocketTimeoutException;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class EnvioProdutoB2CView extends EnvioProdutoB2CController {

    private Thread threadImages = new Thread();
    private Thread threadQtdeImgs = new Thread();
    private Thread threadCoresImgs = new Thread();

    // <editor-fold defaultstate="collapsed" desc="Bean">
    private final ListProperty<VB2cProdutosEnvio> produtosBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<PaIten> itensBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final BooleanProperty emEdicao = new SimpleBooleanProperty(false);
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Components">
    // filter
    private final FormFieldMultipleFind<Produto> fieldFilterProduto = FormFieldMultipleFind.create(Produto.class, field -> {
        field.title("Produto");
        field.width(187.0);
        field.toUpper();
    });
    private final FormFieldMultipleFind<Marca> fieldFilterMarca = FormFieldMultipleFind.create(Marca.class, field -> {
        field.title("Marca");
        field.width(110.0);
        field.toUpper();
    });
    private final FormFieldMultipleFind<Colecao> fieldFilterColecao = FormFieldMultipleFind.create(Colecao.class, field -> {
        field.title("Coleção");
        field.width(160.0);
        field.toUpper();
    });
    private final FormFieldMultipleFind<EtqProd> fieldFilterModelagem = FormFieldMultipleFind.create(EtqProd.class, field -> {
        field.title("Modelagem");
        field.toUpper();
    });
    private final FormFieldMultipleFind<Linha> fieldFilterLinha = FormFieldMultipleFind.create(Linha.class, field -> {
        field.title("Linha");
        field.toUpper();
    });
    private final FormFieldSegmentedButton<String> fieldFilterStatus = FormFieldSegmentedButton.create(field -> {
        field.title("Status");
        field.options(
                field.option("Todos", "T", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("Pendentes", "0", FormFieldSegmentedButton.Style.WARNING),
                field.option("Enviados", "4", FormFieldSegmentedButton.Style.SUCCESS)
        );
        field.select(0);
    });
    private final FormTableView<VB2cProdutosEnvio> tblProdutosEnvio = FormTableView.create(VB2cProdutosEnvio.class, table -> {
        table.title("Produtos");
        table.items.bind(produtosBean);
        table.expanded();
        table.editable.set(true);
        table.selectColumn();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VB2cProdutosEnvio, VB2cProdutosEnvio>, ObservableValue<VB2cProdutosEnvio>>) param -> new ReadOnlyObjectWrapper(param.getValue().getReferencia()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Código*/,
                FormTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(270.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VB2cProdutosEnvio, VB2cProdutosEnvio>, ObservableValue<VB2cProdutosEnvio>>) param -> new ReadOnlyObjectWrapper(param.getValue().getRefnome()));
                }).build() /*Descrição*/,
                FormTableColumn.create(cln -> {
                    cln.title("Marca");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VB2cProdutosEnvio, VB2cProdutosEnvio>, ObservableValue<VB2cProdutosEnvio>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDescmarca()));
                }).build() /*Marca*/,
                FormTableColumn.create(cln -> {
                    cln.title("Coleção");
                    cln.width(130.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VB2cProdutosEnvio, VB2cProdutosEnvio>, ObservableValue<VB2cProdutosEnvio>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDesccolecao()));
                }).build() /*Coleção*/,
                FormTableColumn.create(cln -> {
                    cln.title("Foto");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VB2cProdutosEnvio, VB2cProdutosEnvio>, ObservableValue<VB2cProdutosEnvio>>) param -> new ReadOnlyObjectWrapper(param.getValue().getImagem()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<VB2cProdutosEnvio, String>() {
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(item.contains("semimg.jpg") ? "Sem Imagem" : "Foto OK");
                                }
                            }
                        };
                    });
                }).build() /*Foto*/,
                FormTableColumn.create(cln -> {
                    cln.title("Ativo");
                    cln.width(40.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VB2cProdutosEnvio, VB2cProdutosEnvio>, ObservableValue<VB2cProdutosEnvio>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<VB2cProdutosEnvio, VB2cProdutosEnvio>() {
                            @Override
                            protected void updateItem(VB2cProdutosEnvio item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(ImageUtils.getIcon(item.isAtivo() ? ImageUtils.Icon.STATUS_SIM : ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                                }
                            }
                        };
                    });
                    cln.alignment(Pos.CENTER);
                }).build() /*Ativo*/,
                FormTableColumn.create(cln -> {
                    cln.title("Status");
                    cln.width(110.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VB2cProdutosEnvio, VB2cProdutosEnvio>, ObservableValue<VB2cProdutosEnvio>>) param -> new ReadOnlyObjectWrapper(param.getValue().getStatus()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<VB2cProdutosEnvio, String>() {
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setText(item.equals("0") ? "Pendente" : "Enviado");
                                    setGraphic(ImageUtils.getIcon((item.equals("0") ? ImageUtils.Icon.ENVIAR : ImageUtils.Icon.LOJISTA),
                                            ImageUtils.IconSize._16));
                                }
                            }
                        };
                    });
                }).build() /*Status*/
        );
        table.factoryRow(param -> {
            return new TableRow<VB2cProdutosEnvio>() {
                @Override
                protected void updateItem(VB2cProdutosEnvio item, boolean empty) {
                    super.updateItem(item, empty);
                    getStyleClass().remove("table-row-warning");
                    if (item != null && item.getImagem() != null && !empty) {
                        if (item.getImagem().contains("semimg"))
                            getStyleClass().add("table-row-warning");
                    }
                }
            };
        });
        table.indices(
                table.indice("Pendentes", "warning", "")
        );
        table.selectionModelItem((observable, oldValue, newValue) -> {
            if (newValue != null) {
                try {
                    abrirProduto((VB2cProdutosEnvio) newValue);
                } catch (SQLException e) {
                    e.printStackTrace();
                    handleProductLoadError();
                }
            }
        });
    });

    private void handleProductLoadError() {
        MessageBox.create(message -> {
            message.message("Erro ao abrir o produto, tente novamente!");
            message.type(MessageBox.TypeMessageBox.WARNING);
            message.showAndWait();
        });
        tblProdutosEnvio.tableview().getSelectionModel().clearSelection();
        limparDadosProduto();
    }

    // detalhe produto
    private final FormFieldText fieldTituloProduto = FormFieldText.create(field -> {
        field.title("Nome Produto");
        field.editable.bind(emEdicao);
        field.mouseClicked(this::liberarEdicao);
    });
    private final FormFieldText fieldPalavrasChave = FormFieldText.create(field -> {
        field.title("Palavras Chave");
        field.editable.bind(emEdicao);
        field.mouseClicked(evt -> liberarEdicao(evt));
    });
    private final FormFieldText fieldDescricaoCEO = FormFieldText.create(field -> {
        field.title("Descrição CEO");
        field.editable.bind(emEdicao);
        field.mouseClicked(evt -> liberarEdicao(evt));
    });
    private final FormFieldText fieldDescricao = FormFieldText.create(field -> {
        field.title("Descrição");
        field.editable.bind(emEdicao);
        field.mouseClicked(evt -> liberarEdicao(evt));
    });
    private final FormFieldTextArea fieldDescricaoCurta = FormFieldTextArea.create(field -> {
        field.title("Descrição Curta");
        field.editable.bind(emEdicao);
        field.mouseClicked(evt -> liberarEdicao(evt));
        field.width(280);
        field.height(200);
    });
    private final FormFieldTextArea fieldDescricaoLonga = FormFieldTextArea.create(field -> {
        field.title("Descrição Longa");
        field.editable.bind(emEdicao);
        field.mouseClicked(evt -> liberarEdicao(evt));
        field.width(280);
        field.height(200);
    });
    private final FormFieldComboBox<String> fieldTabelaMedida = FormFieldComboBox.create(String.class, cbox -> {
        cbox.width(400);
        cbox.title("Tabela de Medidas");
    });
    private final FormBox boxInfosImagemPrincipal = FormBox.create(boxInfosImagem -> {
        boxInfosImagem.horizontal();
    });
    private final WebView imagePrincView = new WebView();

    {
        imagePrincView.setPrefWidth(200);
    }

    private final WebView imageHoverView = new WebView();

    {
        imageHoverView.setPrefWidth(200);

    }

    private final FormBox boxInfosImagemHover = FormBox.create(boxInfosImagemHover -> {
        boxInfosImagemHover.horizontal();
    });
    private final FormTableView<PaIten> tblGrade = FormTableView.create(PaIten.class, table -> {
        table.items.bind(itensBean);
        table.withoutHeader();
    });

    private final FormTableView<VB2cProdutoCores> tblCoresProduto = FormTableView.create(VB2cProdutoCores.class, table -> {
        table.title("Cores");
        table.expanded();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Cor");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<VB2cProdutoCores, VB2cProdutoCores>, ObservableValue<VB2cProdutoCores>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCor()));

                }).build(), /*Cor*/
                FormTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(200);
                    cln.value((Callback<TableColumn.CellDataFeatures<VB2cProdutoCores, VB2cProdutoCores>, ObservableValue<VB2cProdutoCores>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDescCor()));

                }).build(), /*Cor*/
                FormTableColumn.create(cln -> {
                    cln.title("Thumb");
                    cln.width(100);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VB2cProdutoCores, VB2cProdutoCores>, ObservableValue<VB2cProdutoCores>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<VB2cProdutoCores, VB2cProdutoCores>() {
                        @Override
                        protected void updateItem(VB2cProdutoCores item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                setGraphic(FormBox.create(boxImg -> {
                                    boxImg.horizontal();
                                    boxImg.alignment(Pos.CENTER);
                                    if (!item.getImageCor().contains("semimg.jpg")) boxImg.add(ImageUtils.getIcon(ImageUtils.Icon.ABRIR, ImageUtils.IconSize._16));
                                    if (item.isImagemSite()) boxImg.add(ImageUtils.getIcon(ImageUtils.Icon.PLANETA, ImageUtils.IconSize._16));
                                }));
                            }
                        }
                    });
                }).build(), /*Cor*/
                FormTableColumn.create(cln -> {
                    cln.title("Imagens");
                    cln.width(250);
                    cln.value((Callback<TableColumn.CellDataFeatures<VB2cProdutoCores, VB2cProdutoCores>, ObservableValue<VB2cProdutoCores>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<VB2cProdutoCores, VB2cProdutoCores>() {
                            @Override
                            protected void updateItem(VB2cProdutoCores item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    if (item.getFotos().size() == 0
                                            || (item.getFotos().size() == 1 && item.getFotos().get(0).getImagem().contains("semimg.jpg"))) {
                                        setText("Sem Fotos");
                                    } else {
                                        setText(item.getFotos().size() + " foto(s) {" + item.getQtdeFotosSite() + " foto(s) no site}");
                                    }
                                }
                            }
                        };
                    });
                }).build() /*Cor*/
        );
    });

    private final Button btnConfirmar = FormButton.create(btn -> {
        btn.title("Confirmar");
        btn.addStyle("success");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._16));
        btn.setOnAction(evt -> salvarAlteracoesProduto());
    });

    private final Button btnCancelar = FormButton.create(btn -> {
        btn.title("Cancelar");
        btn.addStyle("danger");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
        btn.setOnAction(evt -> cancelarAlteracoesProduto());
    });
    // </editor-fold>

    public EnvioProdutoB2CView() {
        super("Envio de Produtos B2C", ImageUtils.getImage(ImageUtils.Icon.PRODUTO));
        init();
    }

    private void init() {
        box.getChildren().add(FormBox.create(content -> {
            content.horizontal();
            content.expanded();
            content.add(FormBox.create(left -> {
                left.vertical();
                left.expanded();
                left.add(FormBox.create(header -> {
                    header.horizontal();
                    header.add(FormTitledPane.create(filter -> {
                        filter.filter();
                        filter.add(FormBox.create(boxFilter -> {
                            boxFilter.vertical();
                            boxFilter.expanded();
                            boxFilter.add(FormBox.create(boxFields -> {
                                boxFields.horizontal();
                                boxFields.add(fieldFilterProduto.build(), fieldFilterMarca.build(), fieldFilterColecao.build());
                            }));
                            boxFilter.add(FormBox.create(boxFields -> {
                                boxFields.horizontal();
                                boxFields.add(fieldFilterLinha.build(), fieldFilterModelagem.build(), fieldFilterStatus.build());
                            }));
                        }));
                        filter.find.setOnAction(evt -> {
                            carregarProdutos(fieldFilterProduto.objectValues.get(),
                                    fieldFilterMarca.objectValues.get(),
                                    fieldFilterColecao.objectValues.get(),
                                    fieldFilterLinha.objectValues.get(),
                                    fieldFilterModelagem.objectValues.get(),
                                    fieldFilterStatus.value.get());
                        });
                        filter.clean.setOnAction(evt -> {
                            fieldFilterProduto.clear();
                            fieldFilterMarca.clear();
                            fieldFilterColecao.clear();
                            fieldFilterModelagem.clear();
                            fieldFilterLinha.clear();
                            fieldFilterStatus.select(0);
                            produtosBean.clear();
                        });
                    }));
                }));
                left.add(tblProdutosEnvio.build());
                left.add(FormBox.create(toolbar -> {
                    toolbar.horizontal();
                    toolbar.add(FormButton.create(btn -> {
                        btn.title("Enviar");
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._24));
                        btn.addStyle("success");
                        btn.disable.bind(produtosBean.emptyProperty());
                        btn.setAction(evt -> enviarProdutos(produtosBean.get().stream()
                                .filter(VB2cProdutosEnvio::isSelected)
                                .collect(Collectors.toList())));
                    }));
                }));
            }));
            content.add(FormBox.create(right -> {
                right.horizontal();
                right.expanded();
                right.add(FormBox.create(container -> {
                    container.vertical();
                    container.expanded();
                    container.title("Dados Produto");
                    container.add(FormBox.create(boxHeaderProd -> {
                        boxHeaderProd.vertical();
                        boxHeaderProd.add(fieldTituloProduto.build());
                        boxHeaderProd.add(fieldPalavrasChave.build());
                    }));
                    container.add(FormBox.create(boxDescImgs -> {
                        boxDescImgs.horizontal();
                        boxDescImgs.border();
                        boxDescImgs.add(FormBox.create(boxDescricao -> {
                            boxDescricao.vertical();
                            boxDescricao.add(FormBox.create(boxFieldsDescricao -> {
                                boxFieldsDescricao.vertical();
                                boxFieldsDescricao.add(fieldDescricao.build(), fieldDescricaoCEO.build());
                            }));
                            boxDescricao.add(FormBox.create(boxAreasDesc -> {
                                boxAreasDesc.horizontal();
                                boxAreasDesc.add(fieldDescricaoCurta.build(), fieldDescricaoLonga.build());
                            }));
                        }));
                        boxDescImgs.add(FormBox.create(boxImgs -> {
                            boxImgs.horizontal();
                            boxImgs.height(320);
                            boxImgs.add(FormBadges.create(bg -> {
                                bg.badgedNode(FormBox.create(boxImg -> {
                                    boxImg.vertical();
                                    boxImg.title("Imagem Principal");
                                    boxImg.add(imagePrincView);
                                }));
                                bg.node(boxInfosImagemPrincipal);
                                bg.position(Pos.TOP_RIGHT, 0.0);
                            }));
                            boxImgs.add(FormBadges.create(bg -> {
                                bg.badgedNode(FormBox.create(boxImg -> {
                                    boxImg.vertical();
                                    boxImg.title("Imagem Hover");
                                    boxImg.add(imageHoverView);
                                }));
                                bg.node(boxInfosImagemHover);
                                bg.position(Pos.TOP_RIGHT, 0.0);
                            }));
                        }));
                    }));
                    container.add(FormBox.create(boxFooter -> {
                        boxFooter.horizontal();
                        boxFooter.add(FormBox.create(boxTabGradeCor -> {
                            boxTabGradeCor.vertical();
                            boxTabGradeCor.add(fieldTabelaMedida.build());
                            boxTabGradeCor.add(tblGrade.build());
                        }));
                        boxFooter.add(FormBox.create(boxCores -> {
                            boxCores.vertical();
                            boxCores.add(tblCoresProduto.build());
                        }));
                    }));
                    container.add(FormBox.create(boxButtons -> {
                        boxButtons.horizontal();
                        boxButtons.add(btnConfirmar, btnCancelar);
                    }));
                }));
            }));
        }));
    }

    private void cancelarAlteracoesProduto() {
        emEdicao.set(false);
        try {
            abrirProduto(tblProdutosEnvio.selectedItem());
        } catch (SQLException e) {
            e.printStackTrace();
            handleProductLoadError();
        }
    }

    private void salvarAlteracoesProduto() {
        VB2cProdutosEnvio produto = tblProdutosEnvio.selectedItem();

        SdProduto produtoBd = getSdProduto(tblProdutosEnvio.selectedItem().getReferencia());
        produtoBd.setMetakeyword(fieldPalavrasChave.value.get());
        produtoBd.setLongdescription(fieldDescricaoLonga.value.get());
        produtoBd.setShortdescription(fieldDescricaoCurta.value.get());
        produtoBd.setMetadescription(fieldTituloProduto.value.get());
        produtoBd.setMetatitle(fieldTituloProduto.value.get());
        produtoBd.setDescription(fieldDescricao.value.get());
        produtoBd.setTabelamedida(getTabelaMedida(fieldTabelaMedida.value.getValue()));
        salvarTabelaMedida(produto, produtoBd.getTabelamedida());
        SysLogger.addSysDelizLog("Man. Produto B2C", TipoAcao.EDITAR,
                produto.getReferencia(), "Salvo alterações de produto para e-comerce");
        MessageBox.create(message -> {
            message.message("Dados do produto salvos com sucesso!");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
        emEdicao.set(false);
        try {
            abrirProduto(produto);
        } catch (SQLException e) {
            e.printStackTrace();
            handleProductLoadError();
        }
    }

    private String getTabelaMedida(String descricao) {
        String tabela = null;
        switch (descricao) {
            case "CAMISAS, CAMISETAS, JAQUETAS":
                tabela = "CAMISAS_CAMISETAS_JAQUETAS_D";
                break;
            case "REGULAR, SLIM, RELAXED, CONFORT":
                tabela = "REGULAR_SLIM_RELAXED_CONFORT_D";
                break;
            case "SKINNY":
                tabela = "SKINNY_D";
                break;
            case "TRICOT":
                tabela = "TRICOT_D";
                break;
            case "DENIM":
                tabela = "DENIM_F";
                break;
            case "OUTROS":
                tabela = "OUTROS_F";
                break;
        }

        return tabela;
    }

    private String getDescricaoTabelaMedida(String tabela) {
        if (tabela == null || tabela.equals("")) return "";
        String descricao = null;
        switch (tabela) {
            case "CAMISAS_CAMISETAS_JAQUETAS_D":
                descricao = "CAMISAS, CAMISETAS, JAQUETAS";
                break;
            case "REGULAR_SLIM_RELAXED_CONFORT_D":
                descricao = "REGULAR, SLIM, RELAXED, CONFORT";
                break;
            case "SKINNY_D":
                descricao = "SKINNY";
                break;
            case "TRICOT_D":
                descricao = "TRICOT";
                break;
            case "DENIM_F":
                descricao = "DENIM";
                break;
            case "OUTROS_F":
                descricao = "OUTROS";
                break;
        }

        return descricao;
    }

    private void salvarTabelaMedida(VB2cProdutosEnvio produto, String tabela) {
        SdProduto produtoBd = getSdProduto(produto.getReferencia());
        produtoBd.setTabelamedida(tabela);
        salvarDadosProduto(produtoBd);
        SysLogger.addSysDelizLog("Man. Produto B2C", TipoAcao.CADASTRAR, produto.getReferencia(),
                "Incluído tabela de medidade " + tabela + " no produto.");
    }

    private void limparDadosProduto() {
        fieldDescricaoCurta.clear();
        fieldDescricaoLonga.clear();
        boxInfosImagemPrincipal.clear();
        boxInfosImagemHover.clear();
        imagePrincView.getEngine().load("");
        imageHoverView.getEngine().load("");
        fieldTabelaMedida.clear();
        tblGrade.items.clear();
        tblGrade.clearColumns();
        itensBean.clear();

        if (threadImages.isAlive()) threadImages.interrupt();
        if (threadQtdeImgs.isAlive()) threadQtdeImgs.interrupt();
        if (threadCoresImgs.isAlive()) threadCoresImgs.interrupt();
    }

    private void carregarProdutos(ObservableList<Produto> produtos,
                                  ObservableList<Marca> marcas,
                                  ObservableList<Colecao> colecoes,
                                  ObservableList<Linha> linhas,
                                  ObservableList<EtqProd> modelagens,
                                  String status) {
        AtomicReference<List<VB2cProdutosEnvio>> loadProdutos = new AtomicReference<>();
        JPAUtils.clearEntitys(produtosBean.get());
        new RunAsyncWithOverlay(this).exec(task -> {
            loadProdutos.set(carregarProdutos(produtos.stream().map(Produto::getCodigo).toArray(),
                    marcas.stream().map(Marca::getCodigo).toArray(),
                    colecoes.stream().map(Colecao::getCodigo).toArray(),
                    linhas.stream().map(Linha::getCodigo).toArray(),
                    modelagens.stream().map(EtqProd::getCodigo).toArray(),
                    status));
            if (loadProdutos.get().size() == 0) return ReturnAsync.NOT_FOUND.value();
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                produtosBean.set(FXCollections.observableList(loadProdutos.get()));
                produtosBean.stream().forEach(it -> it.selectedProperty().addListener((observable, oldValue, newValue) -> {
                    if (newValue) {
                        if (
                                it.getSdProduto() == null ||
                                        (it.getSdProduto().getMetatitle() == null || it.getSdProduto().getMetatitle().equals("")) ||
                                        (it.getSdProduto().getMetakeyword() == null || it.getSdProduto().getMetakeyword().equals("")) ||
                                        (it.getSdProduto().getDescription() == null || it.getSdProduto().getDescription().equals("")) ||
                                        (it.getSdProduto().getMetadescription() == null || it.getSdProduto().getMetadescription().equals("")) ||
                                        (it.getSdProduto().getShortdescription() == null || it.getSdProduto().getShortdescription().equals("")) ||
                                        (it.getSdProduto().getLongdescription() == null || it.getSdProduto().getLongdescription().equals("")) ||
                                        (it.getSdProduto().getTabelamedida() == null || it.getSdProduto().getTabelamedida().equals(""))
                        ) {
                            MessageBox.create(message -> {
                                message.message("Para enviar o produto preencha todos os campos do produto.");
                                message.type(MessageBox.TypeMessageBox.WARNING);
                                message.position(Pos.TOP_RIGHT);
                                message.notification();
                            });
                            it.setSelected(false);
                        }
                    }
                }));
            }
            if (taskReturn.equals(ReturnAsync.NOT_FOUND.value)) {
                MessageBox.create(message -> {
                    message.message("Ocorreu um erro ao carregar os produtos, por favor tente novamente daqui alguns minutos.");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showAndWait();
                });
            }
        });
    }

    private void abrirProduto(VB2cProdutosEnvio produto) throws SQLException {
        limparDadosProduto();
        if (produto == null)
            return;

        Objects.requireNonNull(JPAUtils.getEntityManager()).refresh(produto);

        fieldTituloProduto.value.set(produto.getSdProduto().getMetatitle());
        fieldPalavrasChave.value.setValue(produto.getSdProduto().getMetakeyword());
        fieldDescricao.value.setValue(produto.getSdProduto().getDescription());
        fieldDescricaoCEO.value.setValue(produto.getSdProduto().getMetadescription());
        fieldDescricaoCurta.value.set(produto.getSdProduto().getShortdescription());
        fieldDescricaoLonga.value.set(produto.getSdProduto().getLongdescription());
        if (produto.getRegiao().equals("B2CD")) {
            fieldTabelaMedida.items(FXCollections.observableArrayList(Arrays.asList(
                    "CAMISAS_CAMISETAS_JAQUETAS_D",
                    "REGULAR_SLIM_RELAXED_CONFORT_D",
                    "SKINNY_D",
                    "TRICOT_D"
            )));
        } else {
            fieldTabelaMedida.items(FXCollections.observableArrayList(Arrays.asList(
                    "DENIM_F",
                    "OUTROS_F"
            )));
        }
        fieldTabelaMedida.items.set(FXCollections.observableArrayList(fieldTabelaMedida.items.stream().map(this::getDescricaoTabelaMedida).collect(Collectors.toList())));
        fieldTabelaMedida.select(getDescricaoTabelaMedida(produto.getSdProduto().getTabelamedida()));

        List<PaIten> estoque = (List<PaIten>) new FluentDao().selectFrom(PaIten.class).where(it -> it.equal("id.codigo", produto.getReferencia()).equal("id.deposito", "0023")).resultList();
//        List<PedReservaBean> pedReserva = (List<PedReservaBean>) new FluentDao().selectFrom(PedReservaBean.class).where(it -> it.equal("id.codigo", produto.getReferencia()).equal("id.deposito", "0023")).resultList();

        if (estoque != null && estoque.size() > 0) {
            tblGrade.addColumn(FormTableColumn.create(cln -> {
                cln.title("Cor");
                cln.setId("cor");
                cln.width(60);
                cln.value((Callback<TableColumn.CellDataFeatures<PaIten, PaIten>, ObservableValue<PaIten>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCor()));
            }).build());

            Map<String, Map<String, List<PaIten>>> sortedMap = new TreeMap<>(
                    estoque.stream().collect(Collectors.groupingBy((PaIten it) -> it.getId().getCor(), Collectors.groupingBy(it -> it.getId().getTam()))));
            sortedMap.forEach((cor, listTam) -> {
                itensBean.add(new PaIten(cor));
                Comparator comparator = (o1, o2) -> SortUtils.sortTamanhos(o1.toString(), o2.toString());
                SortedMap<String, List<PaIten>> keys = new TreeMap<String, List<PaIten>>(comparator);
                keys.putAll(listTam);
                keys.forEach((tam, itens) -> {
                    if (tblGrade.tableview().getColumns().stream().noneMatch(it -> it.getId().equals(tam))) {
                        tblGrade.addColumn(FormTableColumn.create(cln -> {
                            cln.title(tam);
                            cln.setId(tam);
                            cln.width(35);
                            cln.alignment(Pos.CENTER);
                            cln.value((Callback<TableColumn.CellDataFeatures<PaIten, PaIten>, ObservableValue<PaIten>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                            cln.format(param -> new TableCell<PaIten, PaIten>() {
                                @Override
                                protected void updateItem(PaIten item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    if (item != null && !empty) {
                                        setText("0");
//                                        BigDecimal desconto = pedReserva.stream().filter(it -> it.getId().getCor().equals(item.getId().getCor()) && it.getId().getTam().equals(tam)).map(PedReservaBean::getQtde).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
                                        estoque.stream().filter(it -> it.getId().getCor().equals(item.getId().getCor()) && it.getId().getTam().equals(tam)).findFirst().ifPresent(it -> setText(it.getQuantidade().toString()));
                                    }
                                }
                            });
                        }).build());
                    }
                });
            });
        }

        tblCoresProduto.items.set(FXCollections.observableList(produto.getCores()));

        threadImages = new Thread(() -> {
            Platform.runLater(() -> {
                if (produto.getImagem() == null) return;
                try {
                    imagePrincView.getEngine().load(produto.getImagem()
                            .replace("https://imagens.deliz.com.br/produtos/",
                                    "file:///K:/Loja Virtual/imagens/produtos/")
                            .replace("flordelis", "Flor De Lis"));

                    imageHoverView.getEngine().load(produto.getImagemhover() == null
                            ? "file:///K:/Loja Virtual/imagens/produtos/semimg.jpg"
                            : produto.getImagemhover().replace("https://imagens.deliz.com.br/produtos/",
                                    "file:///K:/Loja Virtual/imagens/produtos/")
                            .replace("flordelis", "Flor De Lis"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        });

        //Thread que verifica a quantidade de imagens no site
        threadQtdeImgs = new Thread(() -> {
            for (VB2cProdutoCores cor : produto.getCores()) {
                AtomicInteger fotosFtp = new AtomicInteger(0);
                cor.getFotos().forEach(fotos -> {
                    try {
                        if (HttpClients.createDefault().execute(new HttpGet(fotos.getImagem().replace(" ", "%20"))).getStatusLine().getStatusCode() == 200)
                            fotosFtp.getAndIncrement();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    cor.setQtdeFotosSite(fotosFtp.get());
                });
                Platform.runLater(tblCoresProduto::refresh);
            }
        });

        //Thread que verifica se as cores tem imagem no site
        threadCoresImgs = new Thread(() -> {
            for (VB2cProdutoCores cor : produto.getCores()) {
                try {
                    if (HttpClients.createDefault().execute(new HttpGet(cor.getImageCor())).getStatusLine().getStatusCode() == 200)
                        cor.setImagemSite(true);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Platform.runLater(tblCoresProduto::refresh);
            }
        });

        //Thread que verifica se a imagem principal está no site
        new Thread(() -> {
            AtomicBoolean hasOnline = new AtomicBoolean(false);
            if (produto.getImagem() == null) return;
            if (produto.getImagem().contains("semimg.jpg")) {
                return;
            }
            try {
                if (requestImagem(produto.getImagem()) == 200) hasOnline.set(true);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Platform.runLater(() -> {
                boxInfosImagemPrincipal.clear();
                boxInfosImagemPrincipal.add(ImageUtils.getIcon(ImageUtils.Icon.ABRIR, ImageUtils.IconSize._16));
                if (hasOnline.get())
                    boxInfosImagemPrincipal.add(ImageUtils.getIcon(ImageUtils.Icon.PLANETA, ImageUtils.IconSize._16));
            });
        }).start();

        //Thread que verifica se a imagem hover está no site
        new Thread(() -> {
            AtomicBoolean hasOnline = new AtomicBoolean(false);
            if (produto.getImagemhover() == null || produto.getImagemhover().contains("semimg.jpg")) {
                return;
            }
            try {
                if (requestImagem(produto.getImagemhover()) == 200) hasOnline.set(true);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Platform.runLater(() -> {
                boxInfosImagemHover.clear();
                boxInfosImagemHover.add(ImageUtils.getIcon(ImageUtils.Icon.ABRIR, ImageUtils.IconSize._16));
                if (hasOnline.get())
                    boxInfosImagemHover.add(ImageUtils.getIcon(ImageUtils.Icon.PLANETA, ImageUtils.IconSize._16));
            });
        }).start();

        threadCoresImgs.start();
        threadImages.start();
        threadQtdeImgs.start();

    }

    private int requestImagem(String imagem) throws IOException {
        return HttpClients.createDefault().execute(new HttpGet(imagem.replace(" ", "%20"))).getStatusLine().getStatusCode();
    }

    private void liberarEdicao(MouseEvent evt) {
        if (evt.getClickCount() > 1)
            emEdicao.set(true);
    }

    private void enviarProdutos(List<VB2cProdutosEnvio> produtosSelecionados) {

        if (produtosSelecionados.size() == 0) {
            MessageBox.create(message -> {
                message.message("É necessário selecionar os produtos que serão enviados.");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showAndWait();
            });
            return;
        }

        AtomicReference<Exception> exc = new AtomicReference<>();
        new RunAsyncWithOverlay(this).exec(task -> {
            try {
                if (!new ServicoB2CEcom().sendProducts(produtosSelecionados))
                    return ReturnAsync.ERROR.value;
            } catch (HttpResponseException e) {
                e.printStackTrace();
                exc.set(e);
                return ReturnAsync.NOT_FOUND.value;
            } catch (SocketTimeoutException e) {
                e.printStackTrace();
                return ReturnAsync.CONNECTION_ERROR.value();
            } catch (IOException e) {
                e.printStackTrace();
                exc.set(e);
                return ReturnAsync.EXCEPTION.value;
            }

            atualizaStatusProdutos(produtosSelecionados);
            produtosSelecionados.forEach(produto -> JPAUtils.getEntityManager().refresh(produto));
            SysLogger.addSysDelizLog("Man. Produto B2C", TipoAcao.ENVIAR, "B2C",
                    "Enviados os produto para loja: " + produtosSelecionados.stream().map(VB2cProdutosEnvio::getReferencia)
                            .collect(Collectors.joining(",")));
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                tblProdutosEnvio.refresh();
                MessageBox.create(message -> {
                    message.message("Enviado(s) " + produtosSelecionados.size() + " produto(s) para a loja.");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
                janelaRelatorioEnvio(produtosSelecionados);
            } else if (taskReturn.equals(ReturnAsync.ERROR.value)) {
                MessageBox.create(message -> {
                    message.message("Nenhum produto voi enviado para a loja.");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showAndWait();
                });
            } else if (taskReturn.equals(ReturnAsync.NOT_FOUND.value)) {
                MessageBox.create(message -> {
                    message.message("Ocorreu um erro ao enviar o produto para a loja. Código do ERRO: " + ((HttpResponseException) exc.get()).getStatusCode());
                    message.type(MessageBox.TypeMessageBox.ERROR);
                    message.showAndWait();
                });
            } else if (taskReturn.equals(ReturnAsync.EXCEPTION.value)) {
                exc.get().printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(exc.get());
                    message.showAndWait();
                });
            } else if (taskReturn.equals(ReturnAsync.CONNECTION_ERROR.value)) {
                exc.get().printStackTrace();
                MessageBox.create(message -> {
                    message.message("Ocorreu um erro ao enviar o produto para a loja. O Serviço de recebimento do site se encontra offline.");
                    message.type(MessageBox.TypeMessageBox.ERROR);
                    message.showAndWait();
                });
            }
        });
    }

    private void janelaRelatorioEnvio(List<VB2cProdutosEnvio> produtosSelecionados) {
        new Fragment().show(fg -> {
            fg.size(1500.0, 800.0);
            fg.title("Relatório de Envio");
            final FormTableView<VB2cProdutosEnvio> tblStatus = FormTableView.create(VB2cProdutosEnvio.class, table -> {
                table.title("Produtos");
                table.expanded();
                table.items.bind(new SimpleListProperty(FXCollections.observableArrayList(produtosSelecionados.stream().sorted(Comparator.comparing(it -> it.getStatusEnvioProduto().getStatusCode())).collect(Collectors.toList()))));
                table.columns(
                        FormTableColumn.create(cln -> {
                            cln.title("Código");
                            cln.width(100);
                            cln.value((Callback<TableColumn.CellDataFeatures<VB2cProdutosEnvio, VB2cProdutosEnvio>, ObservableValue<VB2cProdutosEnvio>>) param -> new ReadOnlyObjectWrapper(param.getValue().getReferencia()));
                        }).build(),
                        FormTableColumn.create(cln -> {
                            cln.title("Status Envio");
                            cln.width(100);
                            cln.value((Callback<TableColumn.CellDataFeatures<VB2cProdutosEnvio, VB2cProdutosEnvio>, ObservableValue<VB2cProdutosEnvio>>) param -> new ReadOnlyObjectWrapper(param.getValue().getStatusEnvioProduto().isEnviado() ? "Enviado" : "Não Enviado"));
                        }).build(),
                        FormTableColumn.create(cln -> {
                            cln.title("Código de Envio");
                            cln.width(100);
                            cln.value((Callback<TableColumn.CellDataFeatures<VB2cProdutosEnvio, VB2cProdutosEnvio>, ObservableValue<VB2cProdutosEnvio>>) param -> new ReadOnlyObjectWrapper(param.getValue().getStatusEnvioProduto().getStatusCode()));
                        }).build(),
                        FormTableColumn.create(cln -> {
                            cln.title("Mensagem de Erro");
                            cln.width(700);
                            cln.value((Callback<TableColumn.CellDataFeatures<VB2cProdutosEnvio, VB2cProdutosEnvio>, ObservableValue<VB2cProdutosEnvio>>) param -> new ReadOnlyObjectWrapper(param.getValue().getStatusEnvioProduto().getMensagem()));
                        }).build()
                );
            });

            fg.box.getChildren().add(tblStatus.build());
        });
    }
}
