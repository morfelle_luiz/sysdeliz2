package sysdeliz2.views.ecommerce.b2c;

import javafx.application.Platform;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import org.apache.http.client.HttpResponseException;
import sysdeliz2.controllers.views.ecommerce.b2c.ManutencaoPedidosB2CController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.sysdeliz.SdPedidosRemessa;
import sysdeliz2.models.sysdeliz.SdStatusPedidoB2C;
import sysdeliz2.models.ti.Entidade;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.ti.PaIten;
import sysdeliz2.models.ti.PedIten;
import sysdeliz2.models.view.VSdDadosPedidoB2C;
import sysdeliz2.utils.apis.b2cEcom.ServicoB2CEcom;
import sysdeliz2.utils.apis.b2cEcom.models.*;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.awt.*;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class ManutencaoPedidosB2CView extends ManutencaoPedidosB2CController {

    // <editor-fold defaultstate="collapsed" desc="Bean">
    private final ListProperty<PedIten> itensPedidoBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<VSdDadosPedidoB2C> pedidosBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Filter">
    private final FormFieldMultipleFind<Marca> marcaFilter = FormFieldMultipleFind.create(Marca.class, field -> {
        field.title("Marca");
        field.toUpper();
    });
    private final FormFieldMultipleFind<Entidade> clienteFilter = FormFieldMultipleFind.create(Entidade.class, field -> {
        field.title("Cliente");
        field.width(200);
    });
    private final FormFieldText codRastreioFilter = FormFieldText.create(field -> {
        field.title("Código de Rastreio");
        field.width(250);
    });
    private final FormFieldText numeroPedidoFilter = FormFieldText.create(field -> {
        field.title("Número Pedido (Magento)");
        field.width(300);
        field.toUpper();
    });
    private final FormFieldText numeroPedidoErpFilter = FormFieldText.create(field -> {
        field.title("Número Pedido ERP");
        field.width(300);
        field.toUpper();
    });

    private FormFieldCheckBoxGroup groupStatusFilter = FormFieldCheckBoxGroup.create(check -> {
        check.title("Status");
        check.flutuanteMode();
        check.getBox().setMaxWidth(300);
        check.items(
                ((List<SdStatusPedidoB2C>) new FluentDao().selectFrom(SdStatusPedidoB2C.class).get().resultList()).stream().map(it -> check.option(it.getStatus(), it.getCodigo(), it.isPadrao())).collect(Collectors.toList())
        );
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Table">

    private final FormTableView<VSdDadosPedidoB2C> tblPedidos = FormTableView.create(VSdDadosPedidoB2C.class, table -> {
        table.items.bind(pedidosBean);
        table.expanded();
        table.title("Pedidos");
        table.selectionModelItem((observable, oldValue, newValue) -> {
            if (newValue != null) {
                new RunAsyncWithOverlay(this).exec(task -> {
                    buscarItensPedido((VSdDadosPedidoB2C) newValue);
                    return ReturnAsync.OK.value;
                }).addTaskEndNotification(taskReturn -> {
                    if (taskReturn.equals(ReturnAsync.OK.value)) {
                        itensPedidoBean.set(FXCollections.observableArrayList(itensPedido));
                    }
                });
            }
        });
        table.factoryRow(param -> new TableRow<VSdDadosPedidoB2C>() {
            @Override
            protected void updateItem(VSdDadosPedidoB2C item, boolean empty) {
                super.updateItem(item, empty);
                clear();

                if (item != null && !empty) {
                    switch (item.getCodStatus().getCodigo()) {
                        case 2:
                            getStyleClass().add("table-row-warning");
                            break;
                        case 3:
                            getStyleClass().add("table-row-success");
                            break;
                        case 4:
                            getStyleClass().add("table-row-danger");
                            break;
                    }
                }
            }

            private void clear() {
                getStyleClass().removeAll("table-row-danger", "table-row-success", "table-row-warning");
            }
        });
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Ações");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(140);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosPedidoB2C, VSdDadosPedidoB2C>, ObservableValue<VSdDadosPedidoB2C>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<VSdDadosPedidoB2C, VSdDadosPedidoB2C>() {
                        final HBox boxButtons = new HBox(3);

                        final FormButton btnEnviar = FormButton.create(btn -> {
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._16));
                            btn.tooltip("Marcar pedido como enviado");
                            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            btn.addStyle("warning");
                        });

                        final FormButton btnEntregue = FormButton.create(btn -> {
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._16));
                            btn.tooltip("Marcar pedido como entregue");
                            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            btn.addStyle("success");
                        });

                        final FormButton btnCancelar = FormButton.create(btn -> {
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                            btn.tooltip("Cancelar Pedido");
                            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            btn.addStyle("danger");
                        });

                        final FormButton btnRastrear = FormButton.create(btn -> {
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR_ENTREGA, ImageUtils.IconSize._16));
                            btn.tooltip("Rastrear entrega");
                            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            btn.addStyle("primary");
                        });

                        @Override
                        protected void updateItem(VSdDadosPedidoB2C item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                btnEnviar.setOnAction(evt -> {
                                    tblPedidos.tableview().getSelectionModel().select(item);
                                    if (item.getTransporte().getCodigo() == "3") {
                                        enviarPedido(item, "", false);
                                        return;
                                    }
                                    Platform.runLater(() -> janelaEnvioPedido(item));
                                });

                                btnCancelar.setOnAction(evt -> {
                                    alterarStatusPedido(item, getStatusById(4));
                                });

                                btnRastrear.setOnAction(evt -> {
                                    try {
                                        if (item.getCodrastreio().equals("")) {
                                            MessageBox.create(message -> {
                                                message.message("Código de rastreio inválido");
                                                message.type(MessageBox.TypeMessageBox.WARNING);
                                                message.position(Pos.TOP_RIGHT);
                                                message.notification();
                                            });
                                            return;
                                        }

                                        Desktop.getDesktop().browse(new URI(item.getTransporte().getCodigo().equals("1") ? "https://www.linkcorreios.com.br/?id=" + item.getCodrastreio() : "https://blue.braspress.com/site/w/tracking/v3/search?cpfCnpj=42480841049&pedidoNf=" + item.getCodrastreio()));
                                    } catch (IOException | URISyntaxException e) {
                                        e.printStackTrace();
                                    }
                                });

                                btnEntregue.setOnAction(evt -> {
                                    alterarStatusPedido(item, getStatusById(3));
                                });

                                btnCancelar.disable.setValue(item.getCodStatus().getCodigo() == 3 || item.getCodStatus().getCodigo() == 4);
                                btnEnviar.disable.setValue(item.getCodStatus().getCodigo() != 1);
                                btnRastrear.disable.setValue(item.getCodStatus().getCodigo() != 2);
                                btnEntregue.disable.setValue(item.getCodStatus().getCodigo() != 2);

                                boxButtons.getChildren().clear();
                                boxButtons.getChildren().addAll(btnEnviar, btnRastrear, btnEntregue, btnCancelar);
                                boxButtons.setAlignment(Pos.CENTER);
                                setGraphic(boxButtons);
                            }
                        }
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Número");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosPedidoB2C, VSdDadosPedidoB2C>, ObservableValue<VSdDadosPedidoB2C>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumero().getNumero()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Número ERP");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosPedidoB2C, VSdDadosPedidoB2C>, ObservableValue<VSdDadosPedidoB2C>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumeroerp().getNumero()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Status");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosPedidoB2C, VSdDadosPedidoB2C>, ObservableValue<VSdDadosPedidoB2C>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodStatus().getStatus()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Marca");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosPedidoB2C, VSdDadosPedidoB2C>, ObservableValue<VSdDadosPedidoB2C>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.setCellFactory(param -> new TableCell<VSdDadosPedidoB2C, VSdDadosPedidoB2C>() {
                        @Override
                        protected void updateItem(VSdDadosPedidoB2C item, boolean empty) {
                            super.updateItem(item, empty);
                            clear();
                            if (!empty) {
                                setText(item.getMarca().getDescricao());

                                if (item.getMarca().getCodigo().equals("D")) {
                                    setStyle("-fx-background-color: skyblue;");
                                } else {
                                    setStyle("-fx-background-color: violet;");
                                }
                            }
                        }

                        private void clear() {
                            setText(null);
                            setStyle("");
                        }
                    });
                }).build(),
                FormTableColumnGroup.createGroup(group -> {
                    group.title("Cliente");
                    group.addColumn(FormTableColumn.create(cln -> {
                        cln.title("Cliente");
                        cln.width(170);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosPedidoB2C, VSdDadosPedidoB2C>, ObservableValue<VSdDadosPedidoB2C>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNome()));
                    }).build());
                    group.addColumn(FormTableColumn.create(cln -> {
                        cln.title("Email");
                        cln.width(150);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosPedidoB2C, VSdDadosPedidoB2C>, ObservableValue<VSdDadosPedidoB2C>>) param -> new ReadOnlyObjectWrapper(param.getValue().getEmail()));
                    }).build());
                }),
                FormTableColumnGroup.createGroup(group -> {
                    group.title("Pagamento");
                    group.addColumn(
                            FormTableColumn.create(cln -> {
                                cln.title("Tipo Pgto");
                                cln.width(100);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosPedidoB2C, VSdDadosPedidoB2C>, ObservableValue<VSdDadosPedidoB2C>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTipopgto()));
                            }).build(),
                            FormTableColumn.create(cln -> {
                                cln.title("Frete");
                                cln.width(75);
                                cln.alignment(Pos.CENTER_RIGHT);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosPedidoB2C, VSdDadosPedidoB2C>, ObservableValue<VSdDadosPedidoB2C>>) param -> new ReadOnlyObjectWrapper(StringUtils.toMonetaryFormat(param.getValue().getFrete(), 2)));
                            }).build(),
                            FormTableColumn.create(cln -> {
                                cln.title("Valor");
                                cln.width(75);
                                cln.alignment(Pos.CENTER_RIGHT);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosPedidoB2C, VSdDadosPedidoB2C>, ObservableValue<VSdDadosPedidoB2C>>) param -> new ReadOnlyObjectWrapper(StringUtils.toMonetaryFormat(param.getValue().getValor(), 2)));
                            }).build(),
                            FormTableColumn.create(cln -> {
                                cln.title("Desconto");
                                cln.width(75);
                                cln.alignment(Pos.CENTER_RIGHT);

                                cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosPedidoB2C, VSdDadosPedidoB2C>, ObservableValue<VSdDadosPedidoB2C>>) param -> new ReadOnlyObjectWrapper(StringUtils.toMonetaryFormat(param.getValue().getValordesc(), 2)));
                            }).build()
                    );
                }),
                FormTableColumn.create(cln -> {
                    cln.title("Rastreio");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosPedidoB2C, VSdDadosPedidoB2C>, ObservableValue<VSdDadosPedidoB2C>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodrastreio()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Transporte");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosPedidoB2C, VSdDadosPedidoB2C>, ObservableValue<VSdDadosPedidoB2C>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTipoTransporte()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Dt Emissão");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosPedidoB2C, VSdDadosPedidoB2C>, ObservableValue<VSdDadosPedidoB2C>>) param -> new ReadOnlyObjectWrapper(StringUtils.toDateFormat(param.getValue().getDtemissao())));
                }).build()
        );
    });

    private final FormTableView<PedIten> tblItensPedido = FormTableView.create(PedIten.class, table -> {
        table.items.bind(itensPedidoBean);
        table.title("Produtos");
        table.factoryRow(param -> new TableRow<PedIten>() {
            @Override
            protected void updateItem(PedIten item, boolean empty) {
                super.updateItem(item, empty);
                clear();

                if (item != null && !empty) {
                    PaIten estoque = new FluentDao().selectFrom(PaIten.class).where(it -> it.equal("id.codigo", item.getId().getCodigo().getCodigo()).equal("id.cor", item.getId().getCor().getCor()).equal("id.tam", item.getId().getTam()).equal("id.deposito", "0023")).singleResult();
                    int qtdeEstoque = estoque == null ? 0 : estoque.getQuantidade().intValue();
                    if (qtdeEstoque < item.getQtde()) {
                        getStyleClass().add("table-row-danger");
                    } else if (item.getQtde_f() == item.getQtde_orig()) {
                        getStyleClass().add("table-row-success");
                    }
                }
            }

            private void clear() {
                getStyleClass().removeAll("table-row-danger", "table-row-success", "table-row-warning");
            }
        });
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("SKU");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<PedIten, PedIten>, ObservableValue<PedIten>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodigo().getCodigo() + "-" + param.getValue().getId().getCor().getCor() + "-" + param.getValue().getId().getTam()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<PedIten, PedIten>, ObservableValue<PedIten>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodigo().getDescricao()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Qtde");
                    cln.width(60);
                    cln.value((Callback<TableColumn.CellDataFeatures<PedIten, PedIten>, ObservableValue<PedIten>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde_f() + "  ( " + param.getValue().getQtde_orig() + " )"));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Preço");
                    cln.width(80);
                    cln.alignment(Pos.CENTER_RIGHT);
                    cln.value((Callback<TableColumn.CellDataFeatures<PedIten, PedIten>, ObservableValue<PedIten>>) param -> new ReadOnlyObjectWrapper(StringUtils.toMonetaryFormat(param.getValue().getPreco(), 2)));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Valor Total");
                    cln.width(80);
                    cln.alignment(Pos.CENTER_RIGHT);
                    cln.value((Callback<TableColumn.CellDataFeatures<PedIten, PedIten>, ObservableValue<PedIten>>) param -> new ReadOnlyObjectWrapper(StringUtils.toMonetaryFormat(param.getValue().getPreco().multiply(new BigDecimal(param.getValue().getQtde_orig())), 2)));
                }).build()
        );
    });
    // </editor-fold>

    public ManutencaoPedidosB2CView() {
        super("Manutenção de Pedidos", ImageUtils.getImage(ImageUtils.Icon.PEDIDO));
        init();
    }

    private void init() {
        super.box.getChildren().add(FormBox.create(root -> {
            root.vertical();
            root.expanded();
            root.add(header -> {
                header.horizontal();
                header.add(FormTitledPane.create(filter -> {
                    filter.filter();
                    filter.add(FormBox.create(boxFilter -> {
                        boxFilter.horizontal();
                        boxFilter.add(col1 -> {
                            col1.vertical();
                            col1.add(l1 -> {
                                l1.horizontal();
                                l1.add(marcaFilter.build(), clienteFilter.build(), codRastreioFilter.build());
                            });
                            col1.add(l2 -> {
                                l2.horizontal();
                                l2.add(numeroPedidoFilter.build(), numeroPedidoErpFilter.build());
                            });
                        });
                        boxFilter.add(col2 -> {
                            col2.vertical();
                            col2.add(groupStatusFilter.build());
                        });
                    }));
                    filter.find.setOnAction(evt -> {
                        new RunAsyncWithOverlay(this).exec(task -> {
                            buscarPedidos(
                                    marcaFilter.objectValues.stream().map(Marca::getCodigo).toArray(),
                                    clienteFilter.objectValues.stream().map(Entidade::getCodcli).toArray(),
                                    codRastreioFilter.value.getValueSafe(),
                                    groupStatusFilter.items.stream().filter((it) -> ((FormFieldCheckBoxGroup.OptionCheckBox) it).isSelected()).map((it) -> ((FormFieldCheckBoxGroup.OptionCheckBox) it).valueOption).toArray(),
                                    numeroPedidoFilter.value.getValueSafe(),
                                    numeroPedidoErpFilter.value.getValueSafe()
                            );
                            return ReturnAsync.OK.value;
                        }).addTaskEndNotification(taskReturn -> {
                            if (taskReturn.equals(ReturnAsync.OK.value)) {
                                pedidosBean.set(FXCollections.observableArrayList(pedidos));
                            }
                        });
                    });
                    filter.clean.setOnAction(evt -> {
                        marcaFilter.clear();
                        clienteFilter.clear();
                        codRastreioFilter.clear();
                        numeroPedidoFilter.clear();
                        numeroPedidoErpFilter.clear();
                    });
                }));
            });
            root.add(content -> {
                content.horizontal();
                content.expanded();
                content.add(boxTbl -> {
                    boxTbl.vertical();
                    boxTbl.add(tblPedidos.build());
                });
                content.add(boxListItens -> {
                    boxListItens.vertical();
                    boxListItens.add(tblItensPedido.build());
                });
            });
        }));
    }

    private void alterarStatusPedido(VSdDadosPedidoB2C item, SdStatusPedidoB2C statusNovo) {
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Tem certeza que deseja alterar o status do pedido para '" + statusNovo.getStatus() + "'?");
            message.showAndWait();
        }).value.get())) {
            try {
                item.getNumero().setCodStatus(statusNovo.getCodigo());
                item.setNumero(new FluentDao().merge(item.getNumero()));
                if (statusNovo.getCodigo() != 2)
                    new ServicoB2CEcom().updateOrderStatus(item.getMarca().getCodigo(), item.getNumero().getIdMagento(), new StatusHistory("Status do Pedido alterado para " + statusNovo.getStatus(), statusNovo.getCodMagento()));
                JPAUtils.getEntityManager().refresh(item);
                tblPedidos.refresh();
                SysLogger.addSysDelizLog("Manutenção Pedidos B2C", TipoAcao.EDITAR, item.getNumeroerp().getNumero(), "Statuso do pedido " + item.getNumeroerp().getNumero() + " foi alterado para " + statusNovo.toString());
                MessageBox.create(message -> {
                    message.message("Status do pedido atualizado com sucesso!");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            } catch (NullPointerException | IOException e) {
                MessageBox.create(message -> {
                    message.message("Houve um erro ao modificar o status do pedido! \nNão foi possível localizar seu pedido!");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
                e.printStackTrace();
            }
        }
    }

    private void janelaEnvioPedido(VSdDadosPedidoB2C item) {
        final FormFieldText codigoRastreioField = FormFieldText.create(field -> {
            field.title("Código de Rastreio");
            field.requestFocus();
            field.width(220);
            if (item.getTransporte().getCodigo().equals("2")) {
                SdPedidosRemessa pedidosRemessa = new FluentDao().selectFrom(SdPedidosRemessa.class).where(it -> it.equal("id.numero.numero", item.getNumeroerp().getNumero()).isNotNull("nota")).singleResult();
                if (pedidosRemessa != null && pedidosRemessa.getNota() != null) {
                    field.editable.set(false);
                    field.value.setValue(pedidosRemessa.getNota().getFatura());
                }
            }
        });
        final FormFieldToggle notificarToggleField = FormFieldToggle.create(field -> {
            field.title("Notificar Cliente");
            field.value.setValue(true);
        });
        new Fragment().show(fg -> {
            fg.size(350.0, 100.0);
            fg.title("Código de Rastreio");
            fg.buttonsBox.getChildren().add(FormButton.create(btn -> {
                btn.addStyle("success");
                btn.title("Confirmar");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                btn.setOnAction(evt -> {
                    if (codigoRastreioField.value.getValueSafe().equals("")) {
                        MessageBox.create(message -> {
                            message.message("Código de Rastreio não pode ser vazio");
                            message.type(MessageBox.TypeMessageBox.WARNING);
                            message.position(Pos.TOP_RIGHT);
                            message.notification();
                        });
                        return;
                    }
                    enviarPedido(item, codigoRastreioField.value.getValue(), notificarToggleField.value.getValue());
                    fg.close();
                });
            }));
            fg.box.getChildren().add(FormBox.create(root -> {
                root.vertical();
                root.add(boxInfosEnvio -> {
                    boxInfosEnvio.horizontal();
                    boxInfosEnvio.add(codigoRastreioField.build());
                    boxInfosEnvio.add(notificarToggleField.build());
                });
            }));
        });
    }

    private void enviarPedido(VSdDadosPedidoB2C item, String codRastreio, Boolean notificar) {
        try {
            String mensagem = item.getTransporte().getMensagemPadrao();
            if (item.getTransporte().getCodigo().equals("2")) {
                mensagem = mensagem.replace("[NUMERO_NF]", codRastreio);
            }
            List<OrderItem> itensOrder = new ServicoB2CEcom().getItensOrder(item.getNumero().getIdMagento());
            OrderShip orderShip = new OrderShip(notificar, true, new CommentOrder(mensagem, notificar ? 1 : 0));
            orderShip.setTracks(Collections.singletonList(new TrackOrderShip(codRastreio, "Pedido enviado com sucesso!", item.getTransporte().getCarrierCode())));

//            if (itensPedidoBean.stream().anyMatch(itemPed -> {
//                String sku = itemPed.getId().getCodigo().getCodigo() + "-" + itemPed.getId().getCor().getCor() + "-" + itemPed.getId().getTam();
//                return itensOrder.stream().noneMatch(it -> it.getSku().equals(sku));
//            })) {
//                MessageBox.create(message -> {
//                    message.message("Itens do pedido não são os mesmos do pedido na plataforma magento.\nImpossível colocar o pedido como em transporte.");
//                    message.type(MessageBox.TypeMessageBox.WARNING);
//                    message.showAndWait();
//                });
//                return;
//            }

            itensPedidoBean.forEach(itemPed -> {
                String sku = itemPed.getId().getCodigo().getCodigo() + "-" + itemPed.getId().getCor().getCor() + "-" + itemPed.getId().getTam();
                itensOrder.stream().filter(it -> it.getSku().equals(sku)).findFirst().ifPresent(itemOrder -> orderShip.getItems().add(new ItemOrderShip(itemOrder.getItemId().intValue(), itemOrder.getQtyOrdered().intValue())));
            });

            new ServicoB2CEcom().shipOrder(orderShip, item);
            item.getNumero().setCodRastreio(codRastreio);
            alterarStatusPedido(item, getStatusById(2));
        } catch (HttpResponseException e) {
            MessageBox.create(message -> {
                message.message("Houve um erro ao autalizar o status do pedido, código: " + e.getStatusCode());
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
