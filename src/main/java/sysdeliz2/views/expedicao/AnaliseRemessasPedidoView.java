package sysdeliz2.views.expedicao;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.print.PrinterJob;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import org.jetbrains.annotations.NotNull;
import sysdeliz2.controllers.views.expedicao.AnaliseRemessasPedidoController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.models.sysdeliz.SdCaixaRemessa;
import sysdeliz2.models.sysdeliz.SdNumeroCaixa;
import sysdeliz2.models.sysdeliz.SdRemessaCliente;
import sysdeliz2.models.sysdeliz.SdStatusRemessa;
import sysdeliz2.models.ti.Entidade;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.ti.TabUf;
import sysdeliz2.models.view.*;
import sysdeliz2.models.view.expedicao.VSdResGradeRefCor;
import sysdeliz2.models.view.expedicao.VSdResPedidoCliente;
import sysdeliz2.models.view.expedicao.VSdResRefCorPedido;
import sysdeliz2.models.view.expedicao.VSdReservasAlocadas;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.ReportUtils;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.exceptions.FormValidationException;
import sysdeliz2.utils.gui.ValidationForm;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.InputBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import javax.persistence.EntityNotFoundException;
import javax.print.*;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class AnaliseRemessasPedidoView extends AnaliseRemessasPedidoController {

    // <editor-fold defaultstate="collapsed" desc="Bean">
    private final ListProperty<VSdReservasAlocadas> reservasBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<VSdResPedidoCliente> pedidosClienteBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final StringProperty beanNumeroCaixa = new SimpleStringProperty();
    private final List<String> liberadoImpressao = new ArrayList<>();
    private final BigDecimal _MAX_RESERVA = new BigDecimal(999999);
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Fields">
    // filter
    private final FormFieldText filterRemessaField = FormFieldText.create(field -> {
        field.title("Remessa");
    });
    private final FormFieldText filterEmissorField = FormFieldText.create(field -> {
        field.title("Emissor");
    });
    private final FormFieldMultipleFind<Entidade> filterClienteField = FormFieldMultipleFind.create(Entidade.class, field -> {
        field.title("Cliente");
    });
    private final FormFieldMultipleFind<SdStatusRemessa> filterStatusRemessaField = FormFieldMultipleFind.create(SdStatusRemessa.class, field -> {
        field.title("Status");
        field.toUpper();
    });
    private final FormFieldMultipleFind<TabUf> filterEstadoField = FormFieldMultipleFind.create(TabUf.class, field -> {
        field.title("Estado");
    });
    private final FormFieldText filterPedidoField = FormFieldText.create(field -> {
        field.title("Pedido");
    });
    private final FormFieldDatePeriod filterDataEntregaField = FormFieldDatePeriod.create(field -> {
        field.title("Dt. Entrega");
        field.valueEnd.set(LocalDate.of(2050, 12, 31));
        field.valueBegin.set(LocalDate.of(1980, 1, 1));
    });
    private final FormFieldSegmentedButton<String> filterTipoReservaField = FormFieldSegmentedButton.create(field -> {
        field.title("Tipo Reserva");
        field.options(
                field.option("Ambos", "ambos", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("Automático", "true", FormFieldSegmentedButton.Style.SUCCESS),
                field.option("Manual", "false", FormFieldSegmentedButton.Style.DANGER)
        );
        field.select(0);
    });
    private final FormFieldSegmentedButton<String> filterReservaBloqueadaField = FormFieldSegmentedButton.create(field -> {
        field.title("Bloqueada");
        field.options(
                field.option("Ambos", "ambos", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("Sim", "true", FormFieldSegmentedButton.Style.SUCCESS),
                field.option("Não", "false", FormFieldSegmentedButton.Style.DANGER)
        );
        field.select(0);
    });
    private final FormFieldSegmentedButton<String> filterReservaField = FormFieldSegmentedButton.create(field -> {
        field.title("Reserva");
        field.options(
                field.option("Ambos", "ambos", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("Sim", "true", FormFieldSegmentedButton.Style.SUCCESS),
                field.option("Não", "false", FormFieldSegmentedButton.Style.DANGER)
        );
        field.select(0);
    });
    private final FormFieldSegmentedButton<String> filterPrioridadeReservaField = FormFieldSegmentedButton.create(field -> {
        field.title("Prioridade");
        field.options(
                field.option("Ambos", "ambos", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("Sim", "true", FormFieldSegmentedButton.Style.SUCCESS),
                field.option("Não", "false", FormFieldSegmentedButton.Style.DANGER)
        );
        field.select(0);
    });
    private final FormFieldSegmentedButton<Object[]> filterNaoFaturadasField = FormFieldSegmentedButton.create(field -> {
        field.title("Desconsiderar");
        field.options(
                field.option("Fats./Cancel.", new Object[]{"F", "Z"}, FormFieldSegmentedButton.Style.WARNING),
                field.option("Faturadas", new Object[]{"F"}, FormFieldSegmentedButton.Style.SUCCESS),
                field.option("Canceladas", new Object[]{"Z"}, FormFieldSegmentedButton.Style.DANGER),
                field.option("Considerar Todas", new Object[]{}, FormFieldSegmentedButton.Style.PRIMARY)
        );
        field.select(0);
    });
    private final FormFieldRange filterQtdePecasRemessaField = FormFieldRange.create(field -> {
        field.title("Qtde Peças");
        field.width(200.0);
        field.defaultValue(0,9999);
    });
    // dados pedido
    private final FormFieldTextArea observacaoPedidoField = FormFieldTextArea.create(field -> {
        field.title("Obs. Pedido");
        field.editable(false);
        field.height(200.0);
        field.expanded();
    });
    private final FormFieldText entregasMesField = FormFieldText.create(field -> {
        field.title("Ent. " + LocalDate.now().format(DateTimeFormatter.ofPattern("MMM")).toUpperCase());
        field.editable(false);
        field.alignment(Pos.CENTER);
        field.addStyle("primary");
        field.width(85.0);
    });
    private final FormFieldText estoquesField = FormFieldText.create(field -> {
        field.title("Estoques");
        field.editable(false);
        field.width(75.0);
    });
    private final FormFieldText entregasTotaisField = FormFieldText.create(field -> {
        field.title("Ent. COLEÇÃO");
        field.editable(false);
        field.alignment(Pos.CENTER);
        field.addStyle("info");
        field.width(85.0);
    });
    private final FormFieldText dataUltimaFaturaField = FormFieldText.create(field -> {
        field.title("Ult. Fatura");
        field.editable(false);
        field.alignment(Pos.CENTER_LEFT);
        field.width(120.0);
    });
    private final FormFieldText coletorRemessa = FormFieldText.create(field -> {
        field.title("Coletor");
        field.editable(false);
    });
    // gerador de caixa
    private final FormFieldText fieldNumeroCaixa = FormFieldText.create(field -> {
        field.title("Próximo Número");
        field.editable(false);
        field.alignment(Pos.CENTER);
        field.width(150.0);
        field.value.bind(beanNumeroCaixa);
    });
    private final FormFieldText fieldQtdeEtiquetas = FormFieldText.create(field -> {
        field.title("Qtde");
        field.alignment(Pos.CENTER);
        field.width(150.0);
        field.mask(FormFieldText.Mask.INTEGER);
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Tables">
    private final FormTreeTableView<ItensTreeView> tblItensPedido = FormTreeTableView.create(ItensTreeView.class, table -> {
        table.title("Itens Pedido");
        table.expanded();
        table.columns(
                FormTreeTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(320.0);
                    cln.value((Callback<TreeTableColumn.CellDataFeatures<ItensTreeView, ItensTreeView>, ObservableValue<ItensTreeView>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValue().codigo.get()));
                }).build(),
                FormTreeTableColumn.create(cln -> {
                    cln.title("Cor");
                    cln.width(180.0);
                    cln.value((Callback<TreeTableColumn.CellDataFeatures<ItensTreeView, ItensTreeView>, ObservableValue<ItensTreeView>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValue().cor.get()));
                }).build(),
                FormTreeTableColumn.create(cln -> {
                    cln.title("Tam");
                    cln.width(40.0);
                    cln.value((Callback<TreeTableColumn.CellDataFeatures<ItensTreeView, ItensTreeView>, ObservableValue<ItensTreeView>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValue().tam.get()));
                    cln.alignment(Pos.CENTER);
                }).build(),
                FormTreeTableColumn.create(cln -> {
                    cln.title("Dt. Entrega");
                    cln.width(90.0);
                    cln.value((Callback<TreeTableColumn.CellDataFeatures<ItensTreeView, ItensTreeView>, ObservableValue<ItensTreeView>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValue().dtEntrega.get()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TreeTableCell<ItensTreeView, LocalDate>() {
                            @Override
                            protected void updateItem(LocalDate item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDateFormat(item));
                                }
                            }
                        };
                    });
                }).build(),
                FormTreeTableColumn.create(cln -> {
                    cln.title("Qtde");
                    cln.width(60.0);
                    cln.value((Callback<TreeTableColumn.CellDataFeatures<ItensTreeView, ItensTreeView>, ObservableValue<ItensTreeView>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValue().qtde.get()));
                    cln.alignment(Pos.CENTER);
                }).build(),
                FormTreeTableColumn.create(cln -> {
                    cln.title("Valor");
                    cln.width(80.0);
                    cln.value((Callback<TreeTableColumn.CellDataFeatures<ItensTreeView, ItensTreeView>, ObservableValue<ItensTreeView>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValue().valor.get()));
                    cln.alignment(Pos.CENTER_RIGHT);
                    cln.format(param -> {
                        return new TreeTableCell<ItensTreeView, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toMonetaryFormat(item, 2));
                                }
                            }
                        };
                    });
                }).build(),
                FormTreeTableColumn.create(cln -> {
                    cln.title("Canc.");
                    cln.width(60.0);
                    cln.value((Callback<TreeTableColumn.CellDataFeatures<ItensTreeView, ItensTreeView>, ObservableValue<ItensTreeView>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValue().qtdec.get()));
                    cln.alignment(Pos.CENTER);
                }).build(),
                FormTreeTableColumn.create(cln -> {
                    cln.title("");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(40.0);
                    cln.value((Callback<TreeTableColumn.CellDataFeatures<ItensTreeView, ItensTreeView>, ObservableValue<ItensTreeView>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TreeTableCell<TreeItem<ItensTreeView>, TreeItem<ItensTreeView>>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnExcluir = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                btn.tooltip("Excluir/Cancelar Item");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs");
                            });

                            @Override
                            protected void updateItem(TreeItem<ItensTreeView> item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    btnExcluir.getStyleClass().removeAll("danger", "warning");
                                    btnExcluir.getStyleClass().add(item.getValue().tam.get().equals("-") ? "danger" : "warning");
                                    btnExcluir.setOnAction(evt -> {
                                        //testar o produto família
                                        if (!((Boolean) QuestionBox.build(Boolean.class, message -> {
                                            message.message("Deseja realmente excluir/cancelar o item?");
                                            message.showAndWait();
                                        }).value.get())) {
                                            return;
                                        }
                                        removerItem(item);
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    if (item.getValue().qtde.get() > 0 && !item.getValue().status.get().equals("C"))
                                        boxButtonsRow.getChildren().addAll(btnExcluir);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build()
        );
        table.indices(
                table.indice("Item Cancelado", "danger", ""),
                table.indice("Material Marketing", "info", ""),
                table.indice("Marcado Falta", "warning", ""),
                table.indice("Coletado", "success", ""),
                table.indice("Problemas Cadastrais", "dark", "")
        );
        table.factoryRow(param -> {
            return new TreeTableRow<ItensTreeView>() {
                @Override
                protected void updateItem(ItensTreeView item, boolean empty) {
                    super.updateItem(item, empty);
                    clearStyle();
                    if (item != null && !empty) {
                        setContextMenu(FormContextMenu.create(menu -> {
                            menu.addItem(menuItem -> {
                                menuItem.setText("Expandir Tudo");
                                menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.EXPANDIR, ImageUtils.IconSize._16));
                                menuItem.setOnAction(evt -> {
                                    table.properties().getRoot().getChildren().forEach(node -> ((TreeItem<ItensTreeView>) node).setExpanded(true));
                                });
                            });
                            menu.addItem(menuItem -> {
                                menuItem.setText("Recolher Tudo");
                                menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.RECOLHER, ImageUtils.IconSize._16));
                                menuItem.setOnAction(evt -> {
                                    table.properties().getRoot().getChildren().forEach(node -> ((TreeItem<ItensTreeView>) node).setExpanded(false));
                                });
                            });
                            menu.addSeparator();
                            menu.addItem(menuItem -> {
                                menuItem.setText("Observação");
                                menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.ABRIR, ImageUtils.IconSize._16));
                                menuItem.setOnAction((evt -> adicionarObsItem(item)));
                            });
                        }));
                        String style =
                                item.barra28.get() == null || item.faltaBarra.get() > 0 ? "tree-table-row-dark" :
                                        item.status.get().equals("C") ? "tree-table-row-success" :
                                                (item.qtdec.get() > 0 && item.qtde.get() == 0) ? "tree-table-row-danger" :
                                                        item.status.get().equals("F") ? "tree-table-row-warning" :
                                                                (item.tipo.get().equals("MKT")) ? "tree-table-row-info" : "";
                        getStyleClass().add(style);
                    }
                }

                void clearStyle() {
                    getStyleClass().removeAll("tree-table-row-danger", "tree-table-row-success",
                            "tree-table-row-warning", "tree-table-row-info", "tree-table-row-primary",
                            "tree-table-row-secundary", "tree-table-row-dark", "tree-table-row-amber");
                }
            };
        });
    });
    private final FormTableView<VSdResPedidoCliente> tblPedidosCliente = FormTableView.create(VSdResPedidoCliente.class, table -> {
        table.title("Pedidos Cliente");
        table.items.bind(pedidosClienteBean);
        table.expanded();
        table.height(300.0);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Pedido");
                    cln.width(90.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdResPedidoCliente, VSdResPedidoCliente>, ObservableValue<VSdResPedidoCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getNumero().getNumero()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Pedido*/,
                FormTableColumn.create(cln -> {
                    cln.title("Dt. Entrega");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdResPedidoCliente, VSdResPedidoCliente>, ObservableValue<VSdResPedidoCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDtentrega()));
                    cln.format(param -> {
                        return new TableCell<VSdResPedidoCliente, LocalDate>() {
                            @Override
                            protected void updateItem(LocalDate item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDateFormat(item));
                                }
                            }
                        };
                    });
                    cln.alignment(Pos.CENTER);
                }).build() /*Dt. Entrega*/,
                FormTableColumn.create(cln -> {
                    cln.title("Dt. Base");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdResPedidoCliente, VSdResPedidoCliente>, ObservableValue<VSdResPedidoCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDtBase()));
                    cln.format(param -> {
                        return new TableCell<VSdResPedidoCliente, LocalDate>() {
                            @Override
                            protected void updateItem(LocalDate item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDateFormat(item));
                                }
                            }
                        };
                    });
                    cln.alignment(Pos.CENTER);
                }).build() /*Dt. Base*/,
                FormTableColumn.create(cln -> {
                    cln.title("Marca");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdResPedidoCliente, VSdResPedidoCliente>, ObservableValue<VSdResPedidoCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getMarca() == null ? "-" : param.getValue().getMarca().getDescricao()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Marca*/,
                FormTableColumn.create(cln -> {
                    cln.title("Peças");
                    cln.width(60.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdResPedidoCliente, VSdResPedidoCliente>, ObservableValue<VSdResPedidoCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Peças*/,
                FormTableColumn.create(cln -> {
                    cln.title("Att.");
                    cln.width(40.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdResPedidoCliente, VSdResPedidoCliente>, ObservableValue<VSdResPedidoCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPercAtend()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<VSdResPedidoCliente, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toPercentualFormat(item.doubleValue(), 0));
                                }
                            }
                        };
                    });
                }).build() /*%*/,
                FormTableColumn.create(cln -> {
                    cln.title("Valor");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdResPedidoCliente, VSdResPedidoCliente>, ObservableValue<VSdResPedidoCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValor()));
                    cln.format(param -> {
                        return new TableCell<VSdResPedidoCliente, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toMonetaryFormat(item, 2));
                                }
                            }
                        };
                    });
                    cln.alignment(Pos.CENTER_RIGHT);
                }).build() /*Valor*/,
                FormTableColumn.create(cln -> {
                    cln.title("Desc");
                    cln.width(35.0);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdResPedidoCliente, VSdResPedidoCliente>, ObservableValue<VSdResPedidoCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue().isAplicadoDesconto()));
                    cln.format(param -> {
                        return new TableCell<VSdResPedidoCliente, Boolean>() {
                            @Override
                            protected void updateItem(Boolean item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(item ? ImageUtils.getIcon(ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._16) : ImageUtils.getIcon(ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                                }
                            }
                        };
                    });
                }).build() /*Desc*/,
                FormTableColumn.create(cln -> {
                    cln.title("Pend.");
                    cln.width(60.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdResPedidoCliente, VSdResPedidoCliente>, ObservableValue<VSdResPedidoCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtdePend()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Pend.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Canc.");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdResPedidoCliente, VSdResPedidoCliente>, ObservableValue<VSdResPedidoCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtdec()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Canc.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Transp.");
                    cln.width(180.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdResPedidoCliente, VSdResPedidoCliente>, ObservableValue<VSdResPedidoCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getNumero().getTabTrans() != null ? param.getValue().getId().getNumero().getTabTrans().getNome() : "SEM TRANSP. NO PEDIDO"));
                    cln.format(param -> {
                        return new TableCell<VSdResPedidoCliente, String>() {
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                getStyleClass().removeAll("table-row-amber", "table-row-deliz", "table-row-braspress");
                                if (item != null && !empty) {
                                    setText(item);
                                    getStyleClass().add(item.contains("DELIZ") ? "table-row-deliz" : item.contains("BRASPRESS") ? "table-row-braspress" : "table-row-amber");
                                }
                            }
                        };
                    });
                }).build() /*Transp.*/,
                FormTableColumn.create(cln -> {
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(40);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdResPedidoCliente, VSdResPedidoCliente>, ObservableValue<VSdResPedidoCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<VSdResPedidoCliente, VSdResPedidoCliente>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnExcluir = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                btn.tooltip("Excluir/Cancelar Pedido");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("danger");
                            });

                            @Override
                            protected void updateItem(VSdResPedidoCliente item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {

                                    btnExcluir.setOnAction(evt -> {
                                        table.selectItem(item);
                                        removerPedido(item);
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnExcluir);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build()
        );
        table.selectionModelItem((observable, oldValue, newValue) -> {
            tblItensPedido.clear();
            observacaoPedidoField.clear();
            if (newValue != null) {
                adicionarItensPedidoTable((VSdResPedidoCliente) newValue);
                observacaoPedidoField.value.set(((VSdResPedidoCliente) newValue).getId().getNumero().getObs());
            }
        });
        table.factoryRow((Callback<TableView, TableRow>) param -> {
            TableRow<VSdResPedidoCliente> row = new TableRow<VSdResPedidoCliente>() {
                @Override
                protected void updateItem(VSdResPedidoCliente item, boolean empty) {
                    super.updateItem(item, empty);
                    getStyleClass().remove("table-row-danger");
                    if (item != null && !empty) {
                        setContextMenu(getContextResumoPedido(item));
                        getStyleClass().add(item.getQtde() - item.getQtdec() <= 0 ? "table-row-danger" : "");
                    }
                }
            };
            return row;
        });
    });
    private final FormTableView<VSdReservasAlocadas> tblReservasLiberadas = FormTableView.create(VSdReservasAlocadas.class, table -> {
        table.expanded();
        table.title("Reservas Liberadas");
        table.items.bind(reservasBean);
        table.editable.set(true);
        table.selectColumn();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Aut.");
                    cln.width(30.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdReservasAlocadas, VSdReservasAlocadas>, ObservableValue<VSdReservasAlocadas>>) param -> new ReadOnlyObjectWrapper(param.getValue().isResautomatica()));
                    cln.format(param -> {
                        return new TableCell<VSdReservasAlocadas, Boolean>() {
                            @Override
                            protected void updateItem(Boolean item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(item ? ImageUtils.getIcon(ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._16) : ImageUtils.getIcon(ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                                }
                            }
                        };
                    });
                    cln.alignment(Pos.CENTER);
                }).build() /*Autom.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Stat");
                    cln.width(35.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdReservasAlocadas, VSdReservasAlocadas>, ObservableValue<VSdReservasAlocadas>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getResstatus().getCodigo()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Status Reserva*/,
                FormTableColumn.create(cln -> {
                    cln.title("Prior.");
                    cln.width(30.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdReservasAlocadas, VSdReservasAlocadas>, ObservableValue<VSdReservasAlocadas>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPrioridadecoleta()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<VSdReservasAlocadas, Integer>() {
                            @Override
                            protected void updateItem(Integer item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(item.compareTo(999) == 0 ? "" : "Sim");
                                }
                            }
                        };
                    });
                }).build() /*Prior.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Remessa");
                    cln.width(55.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdReservasAlocadas, VSdReservasAlocadas>, ObservableValue<VSdReservasAlocadas>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getRemessa()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Remessa*/,
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(55.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdReservasAlocadas, VSdReservasAlocadas>, ObservableValue<VSdReservasAlocadas>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodcli().getCodcli()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Código*/,
                FormTableColumn.create(cln -> {
                    cln.title("Nome");
                    cln.width(280.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdReservasAlocadas, VSdReservasAlocadas>, ObservableValue<VSdReservasAlocadas>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodcli()));
                    cln.format(param -> {
                        return new TableCell<VSdReservasAlocadas, VSdDadosCliente>(){
                            @Override
                            protected void updateItem(VSdDadosCliente item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(FormBox.create(icons -> {
                                        icons.horizontal();
                                        if (item.getMaximoReserva().compareTo(_MAX_RESERVA) != 0)
                                            icons.add(ImageUtils.getIcon(ImageUtils.Icon.FATURAMENTO, ImageUtils.IconSize._16));
                                    }));
                                    setText(item.getRazaosocial());
                                }
                            }
                        };
                    });
                }).build() /*Nome*/,
                FormTableColumn.create(cln -> {
                    cln.title("UF");
                    cln.width(35.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdReservasAlocadas, VSdReservasAlocadas>, ObservableValue<VSdReservasAlocadas>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodcli().getCepentrega().getCidade().getCodEst().getId().getSiglaEst()));
                    cln.alignment(Pos.CENTER);
                }).build() /*UF*/,
                FormTableColumn.create(cln -> {
                    cln.title("Dt. Entrega");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdReservasAlocadas, VSdReservasAlocadas>, ObservableValue<VSdReservasAlocadas>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDtentrega()));
                    cln.format(param -> {
                        return new TableCell<VSdReservasAlocadas, LocalDate>() {
                            @Override
                            protected void updateItem(LocalDate item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDateFormat(item));
                                }
                            }
                        };
                    });
                    cln.alignment(Pos.CENTER);
                }).build() /*Dt. Entrega*/,
                FormTableColumn.create(cln -> {
                    cln.title("Depósito");
                    cln.width(60.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdReservasAlocadas, VSdReservasAlocadas>, ObservableValue<VSdReservasAlocadas>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDeposito()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Depósito*/,
                FormTableColumn.create(cln -> {
                    cln.title("Pedidos");
                    cln.width(65.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdReservasAlocadas, VSdReservasAlocadas>, ObservableValue<VSdReservasAlocadas>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtdepedidos()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Pedidos*/,
                FormTableColumn.create(cln -> {
                    cln.title("Marcas");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdReservasAlocadas, VSdReservasAlocadas>, ObservableValue<VSdReservasAlocadas>>) param -> new ReadOnlyObjectWrapper(param.getValue().getMarcas()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Marcas*/,
                FormTableColumn.create(cln -> {
                    cln.title("Peças");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdReservasAlocadas, VSdReservasAlocadas>, ObservableValue<VSdReservasAlocadas>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Peças*/,
                FormTableColumn.create(cln -> {
                    cln.title("Valor");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdReservasAlocadas, VSdReservasAlocadas>, ObservableValue<VSdReservasAlocadas>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValor()));
                    cln.format(param -> {
                        return new TableCell<VSdReservasAlocadas, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toMonetaryFormat(item, 2));
                                }
                            }
                        };
                    });
                    cln.alignment(Pos.CENTER_RIGHT);
                }).build() /*Valor*/,
                FormTableColumn.create(cln -> {
                    cln.title("Obs.");
                    cln.width(30.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdReservasAlocadas, VSdReservasAlocadas>, ObservableValue<VSdReservasAlocadas>>) param -> new ReadOnlyObjectWrapper(param.getValue().getObservacao()));
                    cln.format(param -> {
                        return new TableCell<VSdReservasAlocadas, String>() {
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(item != null ? "Sim" : "");
                                }
                            }
                        };
                    });
                    cln.alignment(Pos.CENTER);
                }).build() /*Obs*/,
                FormTableColumn.create(cln -> {
                    cln.title("Bloq.");
                    cln.width(35.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdReservasAlocadas, VSdReservasAlocadas>, ObservableValue<VSdReservasAlocadas>>) param -> new ReadOnlyObjectWrapper(param.getValue().isReslocked()));
                    cln.format(param -> {
                        return new TableCell<VSdReservasAlocadas, Boolean>() {
                            @Override
                            protected void updateItem(Boolean item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(item ? ImageUtils.getIcon(ImageUtils.Icon.BLOQUEAR, ImageUtils.IconSize._16) : null);
                                }
                            }
                        };
                    });
                }).build() /*Bloq.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Emissor");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdReservasAlocadas, VSdReservasAlocadas>, ObservableValue<VSdReservasAlocadas>>) param -> new ReadOnlyObjectWrapper(param.getValue().getEmissor()));
                    cln.hide();
                }).build() /*Emissor*/,
                FormTableColumn.create(cln -> {
                    cln.title("Valor Max.");
                    cln.width(150.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdReservasAlocadas, VSdReservasAlocadas>, ObservableValue<VSdReservasAlocadas>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodcli().getMaximoReserva()));
                    cln.alignment(Pos.CENTER_RIGHT);
                    cln.hide();
                    cln.format(param -> {
                        return new TableCell<VSdReservasAlocadas, BigDecimal>(){
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toMonetaryFormat(item, 2));
                                }
                            }
                        };
                    });
                }).build() /*Valor Max.*/
        );
        table.selectionModelItem((observable, oldValue, newValue) -> {
            pedidosClienteBean.clear();
            tblItensPedido.clear();
            tblPedidosCliente.tableProperties().getSelectionModel().clearSelection();
            if (newValue != null) {
                ((VSdReservasAlocadas) newValue).refresh();
                if (((VSdReservasAlocadas) newValue).getPedidos() != null) {
                    ((VSdReservasAlocadas) newValue).getPedidos().forEach(VSdResPedidoCliente::refresh);
                    pedidosClienteBean.addAll(((VSdReservasAlocadas) newValue).getPedidos());
                    String estoques = (pedidosClienteBean.get().stream().anyMatch(VSdResPedidoCliente::isInferior) ? "0 / " : "") +
                            (pedidosClienteBean.get().stream().anyMatch(VSdResPedidoCliente::isSuperior) ? "1 / " : "") +
                            (pedidosClienteBean.get().stream().anyMatch(VSdResPedidoCliente::isNaoDefinido) ? "N / " : "");
                    estoquesField.setValue(((VSdReservasAlocadas) newValue).getId().getRemessa().equals("0")
                            ? estoques.length() > 0 ? estoques.substring(0,estoques.length() - 3) : ""
                            : "");

                    AtomicReference<Exception> exc = new AtomicReference<>();
                    new RunAsyncWithOverlay(this).exec(task -> {
                        try {
                            Map<String, Object> entregasCliente = getDadosAdicionaisCliente((VSdReservasAlocadas) newValue);
                            entregasMesField.value.set(entregasCliente.get("ENT_MES").toString());
                            entregasTotaisField.value.set(entregasCliente.get("ENT_TOTAL").toString());
                            dataUltimaFaturaField.value.set(entregasCliente.get("DT_ULT_FAT").toString());
                        } catch (SQLException e) {
                            e.printStackTrace();
                            exc.set(e);
                            return ReturnAsync.EXCEPTION.value;
                        }
                        return ReturnAsync.OK.value;
                    }).addTaskEndNotification(taskReturn -> {
                        if (taskReturn.equals(ReturnAsync.OK.value)) {
                            if (pedidosClienteBean.size() > 0)
                                tblPedidosCliente.selectItem(0);
                            coletorRemessa.value.set(((VSdReservasAlocadas) newValue).getColetor() == null ? "" : ((VSdReservasAlocadas) newValue).getColetor().toString());
                        } else if (taskReturn.equals(ReturnAsync.EXCEPTION.value)) {
                            exc.get().printStackTrace();
                            ExceptionBox.build(message -> {
                                message.exception(exc.get());
                                message.showAndWait();
                            });
                        }
                    });
                }
            }
        });
        table.factoryRow(param -> {
            return new TableRow<VSdReservasAlocadas>() {
                @Override
                protected void updateItem(VSdReservasAlocadas item, boolean empty) {
                    super.updateItem(item, empty);
                    clearStyle();

                    if (item != null && !empty) {
                        setContextMenu(getContextMenuReserva(item));
                        if (item.getId().getResstatus().getCor() != null)
                            getStyleClass().add("table-row-" + item.getId().getResstatus().getCor());
                    }
                }

                void clearStyle() {
                    getStyleClass().removeAll("table-row-danger", "table-row-success", "table-row-warning", "table-row-info", "table-row-primary", "table-row-secundary", "table-row-dark", "table-row-amber");
                }

            };
        });
    });
    // </editor-fold>

    public AnaliseRemessasPedidoView() {
        super("Análise Remessas para Coleta", ImageUtils.getImage(ImageUtils.Icon.PROGRAMAR_COLETA));
        init();
        loadData();
    }

    private void loadData() {
        new RunAsyncWithOverlay(this).exec(task -> {
            getReservas();
            getCaixas();
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                reservasBean.set(FXCollections.observableList(super.reservas));
                criaIndiceRemessas();
                if (caixas.size() == 0) {
                    beanNumeroCaixa.set("200001");
                } else {
                    beanNumeroCaixa.set(String.valueOf(caixas.get(0).getId().getFim() + 1));
                }
            }
        });
    }

    private void init() {
        ((VBox) super.tabs.getTabs().get(0).getContent()).getChildren().add(FormBox.create(principal -> {
            principal.horizontal();
            principal.expanded();
            principal.add(FormBox.create(boxReservas -> {
                boxReservas.vertical();
                boxReservas.add(FormBox.create(boxFilterReservas -> {
                    boxFilterReservas.horizontal();
                    boxFilterReservas.add(FormTitledPane.create(filter -> {
                        filter.filter();
                        filter.add(FormBox.create(boxFieldsFilter -> {
                            boxFieldsFilter.vertical();
                            boxFieldsFilter.expanded();
                            boxFieldsFilter.add(FormBox.create(lob -> {
                                lob.horizontal();
                                lob.add(filterDataEntregaField.build());
                                lob.add(filterQtdePecasRemessaField.build());
                                lob.add(filterTipoReservaField.build());
                            }));
                            boxFieldsFilter.add(FormBox.create(lob -> {
                                lob.horizontal();
                                lob.add(filterRemessaField.build());
                                lob.add(filterClienteField.build());
                                lob.add(filterStatusRemessaField.build());
                                lob.add(filterEstadoField.build());
                                lob.add(filterEmissorField.build());
                            }));
                            boxFieldsFilter.add(FormBox.create(lob -> {
                                lob.horizontal();
                                lob.add(filterReservaBloqueadaField.build());
                                lob.add(filterReservaField.build());
                                lob.add(filterNaoFaturadasField.build());
                                lob.add(filterPrioridadeReservaField.build());
                            }));
                        }));
                        filter.find.setOnAction(evt -> {
                            AtomicReference<EntityNotFoundException> exc = new AtomicReference<>();
                            new RunAsyncWithOverlay(this).exec(task -> {
                                try {
                                    getReservas(
                                            filterRemessaField.value.get(),
                                            filterClienteField.objectValues.stream().map(it -> it.getCodcli()).toArray(),
                                            filterDataEntregaField.valueBegin.get(),
                                            filterDataEntregaField.valueEnd.get(),
                                            filterStatusRemessaField.objectValues.stream().map(SdStatusRemessa::getCodigo).toArray(),
                                            filterTipoReservaField.value.get(),
                                            filterReservaBloqueadaField.value.get(),
                                            filterReservaField.value.get(),
                                            filterPrioridadeReservaField.value.get(),
                                            filterNaoFaturadasField.value.getValue(),
                                            filterEstadoField.objectValues.stream().map(TabUf::getSigla).toArray(),
                                            filterQtdePecasRemessaField.valueBegin.get(),
                                            filterQtdePecasRemessaField.valueEnd.get(),
                                            filterEmissorField.value.get());
                                } catch (EntityNotFoundException ex) {
                                    exc.set(ex);
                                    return ReturnAsync.NOT_FOUND.value;
                                }
                                return ReturnAsync.OK.value;
                            }).addTaskEndNotification(taskReturn -> {
                                if (taskReturn.equals(ReturnAsync.OK.value)) {
                                    reservasBean.set(FXCollections.observableList(super.reservas));
                                    criaIndiceRemessas();
                                } else if (taskReturn.equals(ReturnAsync.NOT_FOUND.value)) {
                                    MessageBox.create(message -> {
                                        message.message("Cliente com problemas cadastrais, verifique os dados do cliente.\n" +
                                                exc.get().getMessage()
                                                        .replaceAll(".*with ", "")
                                                        .replace("id", "Código/CEP"));
                                        message.type(MessageBox.TypeMessageBox.NO_ENTRY);
                                        message.showAndWait();
                                    });
                                }
                            });
                        });
                        filter.clean.setOnAction(evt -> {
                            filterDataEntregaField.valueEnd.set(LocalDate.of(2050, 12, 31));
                            filterDataEntregaField.valueBegin.set(LocalDate.of(1980, 1, 1));
                            filterClienteField.clear();
                            filterRemessaField.clear();
                            filterStatusRemessaField.clear();
                            filterReservaBloqueadaField.select(0);
                            filterReservaField.select(0);
                            filterTipoReservaField.select(0);
                            filterPrioridadeReservaField.select(0);
                            filterNaoFaturadasField.select(0);
                            filterQtdePecasRemessaField.defaultValue(0,9999);
                            reservasBean.clear();
                            pedidosClienteBean.clear();
                            filterEmissorField.clear();
                        });
                    }));
                }));
                boxReservas.add(FormBox.create(icons -> {
                    icons.horizontal();
                    icons.add(FormLabel.create(label -> {
                        label.value.set("Valor Máximo Reserva");
                        label.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.FATURAMENTO, ImageUtils.IconSize._16));
                    }).build());
                }));
                boxReservas.add(tblReservasLiberadas.build());
                boxReservas.add(FormBox.create(boxToolBar -> {
                    boxToolBar.horizontal();
                    boxToolBar.add(FormButton.create(btnEnviar -> {
                        btnEnviar.title("Enviar Remessa");
                        btnEnviar.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._24));
                        btnEnviar.addStyle("primary");
                        btnEnviar.setAction(evt -> enviarReserva());
                    }));
                    boxToolBar.add(FormButton.create(btnImprimir -> {
                        btnImprimir.title("Imprimir Remessa");
                        btnImprimir.icon(ImageUtils.getIcon(ImageUtils.Icon.IMPRIMIR, ImageUtils.IconSize._24));
                        btnImprimir.addStyle("info");
                        btnImprimir.disable.set(_LIBERA_IMPRESSAO.getValor().equals("NAO"));
                        btnImprimir.setAction(evt -> imprimirRemessa());
                    }));
                    boxToolBar.add(FormButton.create(btnBloquear -> {
                        btnBloquear.title("Bloquear Reserva");
                        btnBloquear.icon(ImageUtils.getIcon(ImageUtils.Icon.BLOQUEAR, ImageUtils.IconSize._24));
                        btnBloquear.addStyle("warning");
                        btnBloquear.setAction(evt -> bloquearReserva());
                    }));
                    boxToolBar.add(FormButton.create(btnExcluir -> {
                        btnExcluir.title("Excluir Remessa");
                        btnExcluir.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
                        btnExcluir.addStyle("danger");
                        btnExcluir.setAction(evt -> excluirRemessa());
                    }));
                }));
            }));
            principal.add(FormBox.create(boxDetalhesCliente -> {
                boxDetalhesCliente.vertical();
                boxDetalhesCliente.add(FormBox.create(boxDadosCliente -> {
                    boxDadosCliente.vertical();
                    boxDadosCliente.height(250.0);
                    boxDadosCliente.add(tblPedidosCliente.build());
                    boxDadosCliente.add(FormBox.create(boxDadosPedidos -> {
                        boxDadosPedidos.horizontal();
                        boxDadosPedidos.add(observacaoPedidoField.build());
                        boxDadosPedidos.add(FormBox.create(boxTblPedido -> {
                            boxTblPedido.vertical();
                            boxTblPedido.width(400.0);
                            boxTblPedido.add(coletorRemessa.build());
                            boxTblPedido.add(FormBox.create(boxEntregasCliente -> {
                                boxEntregasCliente.horizontal();
                                boxEntregasCliente.add(entregasMesField.build(), entregasTotaisField.build(),
                                        dataUltimaFaturaField.build(), estoquesField.build());
                            }));
                        }));
                    }));
                }));
                boxDetalhesCliente.add(tblItensPedido.build());
            }));

        }));
        ((VBox) super.tabs.getTabs().get(1).getContent()).getChildren().add(FormBox.create(principal -> {
            principal.horizontal();
            principal.expanded();
            principal.add(FormBox.create(fieldsBox -> {
                fieldsBox.vertical();
                fieldsBox.add(fieldNumeroCaixa.build());
                fieldsBox.add(fieldQtdeEtiquetas.build());
                fieldsBox.add(FormButton.create(btn -> {
                    btn.title("Imprimir");
                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.IMPRIMIR_TAG, ImageUtils.IconSize._24));
                    btn.addStyle("success");
                    btn.setAction(evt -> {
                        try {
                            ValidationForm.validationEmpty(fieldQtdeEtiquetas);
                            Integer inicio = Integer.parseInt(fieldNumeroCaixa.value.get());
                            Integer fim = (inicio - 1) + Integer.parseInt(fieldQtdeEtiquetas.value.get());
                            SdNumeroCaixa configImpressao = new SdNumeroCaixa("E", inicio, fim);
                            new FluentDao().persist(configImpressao);
                            beanNumeroCaixa.set(String.valueOf(fim + 1));

                            Integer qtdeEtiquetas = new BigDecimal(Integer.parseInt(fieldQtdeEtiquetas.value.get())).divide(new BigDecimal(3), RoundingMode.UP).intValue();
                            String zplCode = "";
                            int numero = inicio - 1;
                            for (int i = 1; i <= qtdeEtiquetas; i++) {
                                zplCode += "^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR5,5~SD15^JUS^LRN^CI0^XZ\n" +
                                        "^XA\n" +
                                        "^MMT\n" +
                                        "^PW812\n" +
                                        "^LL0472\n" +
                                        "^LS0";
                                numero++;
                                if (numero <= fim) {
                                    zplCode += "^FT28,358^BQN,2,10\n" +
                                            "^FH\\^FDMA,CX" + numero + "^FS\n" +
                                            "^FT75,97^A0N,45,45^FH\\^FDCAIXA^FS\n" +
                                            "^FT37,405^A0N,68,67^FH\\^FD" + numero + "^FS";
                                }
                                numero++;
                                if (numero <= fim) {
                                    zplCode += "^FT284,358^BQN,2,10\n" +
                                            "^FH\\^FDMA,CX" + numero + "^FS\n" +
                                            "^FT331,97^A0N,45,45^FH\\^FDCAIXA^FS\n" +
                                            "^FT293,405^A0N,68,67^FH\\^FD" + numero + "^FS";
                                }
                                numero++;
                                if (numero <= fim) {
                                    zplCode += "^FT539,358^BQN,2,10\n" +
                                            "^FH\\^FDMA,CX" + numero + "^FS\n" +
                                            "^FT586,97^A0N,45,45^FH\\^FDCAIXA^FS\n" +
                                            "^FT548,405^A0N,68,67^FH\\^FD" + numero + "^FS";
                                }
                                zplCode += "^PQ1,0,1,Y^XZ";
                            }
                            PrinterJob jobPrinterSelect = PrinterJob.createPrinterJob();
                            if (!jobPrinterSelect.showPrintDialog(this.getScene().getWindow())) {
                                return;
                            }

                            String printerName = jobPrinterSelect.getPrinter().getName();
                            PrintService ps = null;
                            byte[] by = zplCode.getBytes();
                            DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
                            PrintService pss[] = PrintServiceLookup.lookupPrintServices(null, null);

                            if (pss.length == 0) {
                                throw new RuntimeException("No printer services available.");
                            }

                            for (PrintService psRead : pss) {
                                if (psRead.getName().contains(printerName)) {
                                    ps = psRead;
                                    break;
                                }
                            }

                            if (ps == null) {
                                throw new RuntimeException("No printer mapped in TS.");
                            }

                            DocPrintJob job = ps.createPrintJob();
                            Doc doc = new SimpleDoc(by, flavor, null);

                            job.print(doc, null);

                            fieldQtdeEtiquetas.clear();
                            MessageBox.create(message -> {
                                message.message("Etiquetas enviadas para impressora.");
                                message.type(MessageBox.TypeMessageBox.SUCCESS);
                                message.position(Pos.TOP_RIGHT);
                                message.notification();
                            });
                        } catch (FormValidationException ev) {
                            MessageBox.create(message -> {
                                message.message(ev.getMessage());
                                message.type(MessageBox.TypeMessageBox.IMPORTANT);
                                message.showAndWait();
                            });
                        } catch (SQLException | PrintException e) {
                            e.printStackTrace();
                            ExceptionBox.build(message -> {
                                message.exception(e);
                                message.showAndWait();
                            });
                        }
                    });
                }));
            }));
        }));
    }

    private void adicionarItensPedidoTable(VSdResPedidoCliente newValue) {
        TreeItem<ItensTreeView> rootNode = new TreeItem<>(new ItensTreeView());
        newValue.getItens().forEach(itens -> {
            TreeItem<ItensTreeView> itemCodigo = new TreeItem<>(
                    new ItensTreeView(itens.getId().getPedido().getId().getNumero().getNumero(),
                            itens.getId().getPedido().getId().getRemessa().getId().getRemessa(),
                            itens.getId().getCodigo(),
                            itens.getId().getCor(),
                            "-", itens.getDtentrega(), itens.getQtde(), itens.getValor(), itens.getQtdec(), itens.getTipo(), itens.getStatusitem(), itens, "item", itens.getFaltaBarra()));
            itens.getGrade().forEach(grade -> {
                itemCodigo.getChildren().add(new TreeItem<ItensTreeView>(
                        new ItensTreeView(itens.getId().getPedido().getId().getNumero().getNumero(),
                                itens.getId().getPedido().getId().getRemessa().getId().getRemessa(),
                                itens.getId().getCodigo(),
                                itens.getId().getCor(),
                                grade.getId().getTam(),
                                grade.getDtentrega(), grade.getQtde(), grade.getValor(), grade.getQtdec(), grade.getTipo(), grade.getStatusitem(), grade, grade.getBarra28(), 0)));
            });
            rootNode.getChildren().add(itemCodigo);
        });
        tblItensPedido.properties().setRoot(rootNode);
        tblItensPedido.refresh();
    }

    private void adicionarObsRemessa(VSdReservasAlocadas item) {
        new Fragment().show(fragment -> {
            fragment.title("Observações de Remessa");
            fragment.size(300.0, 250.0);

            String observacaoOld = item.getObservacao();
            final FormFieldTextArea observacaoRemessaField = FormFieldTextArea.create(field -> {
                field.title("Observação");
                field.expanded();
                field.height(250.0);
                field.value.bindBidirectional(item.observacaoProperty());
            });
            fragment.box.getChildren().add(observacaoRemessaField.build());
            fragment.buttonsBox.getChildren().add(FormButton.create(btn -> {
                btn.title("Salvar");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.SALVAR, ImageUtils.IconSize._24));
                btn.addStyle("primary");
                btn.setAction(evt -> {
                    try {
                        super.adicionarObservacaoRemessa(item);
                        tblReservasLiberadas.refresh();
                        MessageBox.create(message -> {
                            message.message("Observação cadastrada com sucesso para a remessa: " + item.getId().getRemessa());
                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                            message.position(Pos.TOP_RIGHT);
                            message.notification();
                        });
                        fragment.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                        ExceptionBox.build(message -> {
                            message.exception(e);
                            message.showAndWait();
                        });
                    } catch (FormValidationException e) {
                        item.setObservacao(observacaoOld);
                        MessageBox.create(message -> {
                            message.message(e.getMessage());
                            message.type(MessageBox.TypeMessageBox.ALERT);
                            message.showAndWait();
                        });
                        tblReservasLiberadas.refresh();
                        fragment.close();
                    }
                });
            }));
        });
    }

    private void adicionarObsItem(ItensTreeView item) {
        new Fragment().show(fragment -> {
            fragment.title("Observações de Produto");
            fragment.size(300.0, 250.0);

            try {
                String observacaoItem = getObservacaoItem(item.remessa.get(), item.numero.get(), item.codigo.get().getCodigo(), item.cor.get().getCor());

                final FormFieldTextArea observacaoRemessaField = FormFieldTextArea.create(field -> {
                    field.title("Observação");
                    field.expanded();
                    field.height(250.0);
                    field.value.set(observacaoItem);
                });
                fragment.box.getChildren().add(observacaoRemessaField.build());
                fragment.buttonsBox.getChildren().add(FormButton.create(btn -> {
                    btn.title("Salvar");
                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.SALVAR, ImageUtils.IconSize._24));
                    btn.addStyle("primary");
                    btn.setAction(evt -> {
                        try {
                            super.adicionarObservacaoItem(item, observacaoRemessaField.value.get());
                            MessageBox.create(message -> {
                                message.message("Observação cadastrada com sucesso no item.");
                                message.type(MessageBox.TypeMessageBox.CONFIRM);
                                message.position(Pos.TOP_RIGHT);
                                message.notification();
                            });
                            fragment.close();
                        } catch (SQLException e) {
                            e.printStackTrace();
                            ExceptionBox.build(message -> {
                                message.exception(e);
                                message.showAndWait();
                            });
                        }
                    });
                }));

            } catch (SQLException e) {
                e.printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(e);
                    message.showAndWait();
                });
            }
        });
    }

    private void imprimirRemessaEmFila(VSdReservasAlocadas item) {
        getLiberaImpressao();
        if (_LIBERA_IMPRESSAO.getValor().equals("NAO")) {
            MessageBox.create(message -> {
                message.message("Não é permitido a impressão de remessas");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }
        JPAUtils.getEntityManager().refresh(item.getId().getCodcli());
        if (!item.getPedidos().stream().allMatch(ped -> liberadoImpressao.contains(ped.getId().getNumero().getSdPedido().getOrigem())) && !item.getId().getCodcli().isImprimeRemessa()) {
            MessageBox.create(message -> {
                message.message("Essa remessa não pode ser impressa.\n" +
                        "É permitido somente impressões de remessas com pedidos emitidos " + (
                        _LIBERA_IMPRESSAO.getValor().equals("TODOS") ? "em qualquer plataforma." :
                                _LIBERA_IMPRESSAO.getValor().equals("LOJA") ? "nas plataformas B2B ou B2C" :
                                        ("na plataforma " + _LIBERA_IMPRESSAO.getValor())
                ));
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showAndWait();
            });
            return;
        }

        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Deseja realmente migrar remessa em fila para coleta por impressão?");
            message.showAndWait();
        }).value.get())) {
            try {
                migrarStatusRemessa(item, "I");
                imprimirRemessa(item);
                MessageBox.create(message -> {
                    message.message("Remessa impressa.");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            } catch (SQLException | IOException | JRException e) {
                e.printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(e);
                    message.showAndWait();
                });
            } catch (FormValidationException e) {
                MessageBox.create(message -> {
                    message.message(e.getMessage());
                    message.type(MessageBox.TypeMessageBox.ALERT);
                    message.showAndWait();
                });
            }
            tblReservasLiberadas.refresh();
        }
    }

    private void atualizarPrioridadeRemessa(VSdReservasAlocadas item) {
        new Fragment().show(fragment -> {
            fragment.title("Alterar Prioridade de Coleta");
            fragment.size(200.0, 200.0);
            final FormFieldText prioridadeField = FormFieldText.create(field -> {
                field.title("Prioridade");
                field.mask(FormFieldText.Mask.INTEGER);
                field.value.set(String.valueOf(item.getPrioridadecoleta()));
            });
            fragment.box.getChildren().add(prioridadeField.build());
            fragment.buttonsBox.getChildren().add(FormButton.create(btn -> {
                btn.title("Priorizar");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.RECOLHER, ImageUtils.IconSize._24));
                btn.addStyle("primary");
                btn.setAction(evt -> {
                    try {
                        priorizarColeta(item, prioridadeField.value.get());
                        MessageBox.create(message -> {
                            message.message("Remessa priorizada com sucesso.");
                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                            message.position(Pos.TOP_RIGHT);
                            message.notification();
                        });
                    } catch (SQLException e) {
                        e.printStackTrace();
                        ExceptionBox.build(message -> {
                            message.exception(e);
                            message.showAndWait();
                        });
                    } catch (FormValidationException e) {
                        MessageBox.create(message -> {
                            message.message(e.getMessage());
                            message.type(MessageBox.TypeMessageBox.ALERT);
                            message.showAndWait();
                        });
                    }
                    tblReservasLiberadas.refresh();
                    fragment.close();
                });
            }));
        });
    }

    private void enviarRemessaImpressa(VSdReservasAlocadas item) {
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Deseja realmente migrar remessa impressa para fila de coleta?");
            message.showAndWait();
        }).value.get())) {
            try {
                migrarStatusRemessa(item, "X");
                MessageBox.create(message -> {
                    message.message("Remessa migrada para fila de coleta.");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            } catch (SQLException e) {
                e.printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(e);
                    message.showAndWait();
                });
            } catch (FormValidationException e) {
                MessageBox.create(message -> {
                    message.message(e.getMessage());
                    message.type(MessageBox.TypeMessageBox.ALERT);
                    message.showAndWait();
                });
            }
            tblReservasLiberadas.refresh();
        }
    }

    private void excluirRemessa() {
        reservasBean.stream().filter(it -> it.getId().getResstatus().isExcremessa()).forEach(res -> res.refresh());
        if (reservasBean.stream().filter(it -> it.getId().getResstatus().isExcremessa()).noneMatch(VSdReservasAlocadas::isSelected)) {
            MessageBox.create(message -> {
                message.message("Você precisa seleiconar ao menos uma remessa para exclusão.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }

        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Deseja realmente excluir a(s) remessa(s) selecionada(s)?");
            message.showAndWait();
        }).value.get())) {
            for (VSdReservasAlocadas reserva : reservasBean.stream().filter(it -> it.getId().getResstatus().isExcremessa() && it.isSelected()).collect(Collectors.toList())) {
                boolean liberaExcluir = false;
                if (!reserva.getEmissor().equals(Globals.getUsuarioLogado().getUsuario())){
                    if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                            message.message("Essa remessa foi criada por outro usuário (" + reserva.getEmissor() + "), deseja realmente excluir a remessa?");
                            message.showAndWait();
                        }).value.get())) {
                        liberaExcluir = true;
                    }
                } else
                    liberaExcluir = true;

                if (liberaExcluir) {
                    AtomicReference<Exception> excRwo = new AtomicReference<>();
                    new RunAsyncWithOverlay(this).exec(task -> {
                        try {
                            excluirRemessa(reserva);
                            refreshGetReservas();
                        } catch (Exception e) {
                            e.printStackTrace();
                            excRwo.set(e);
                            return ReturnAsync.EXCEPTION.value;
                        }
                        return ReturnAsync.OK.value;
                    }).addTaskEndNotification(taskReturn -> {
                        if (taskReturn.equals(ReturnAsync.OK.value)) {
                            reservasBean.set(FXCollections.observableList(super.reservas));
                            criaIndiceRemessas();
                            tblReservasLiberadas.refresh();
                            MessageBox.create(message -> {
                                message.message("Finalizado exclusão de remessas");
                                message.type(MessageBox.TypeMessageBox.CONFIRM);
                                message.position(Pos.TOP_RIGHT);
                                message.notification();
                            });
                        } else if (taskReturn.equals(ReturnAsync.EXCEPTION.value)) {
                            if (excRwo.get() instanceof FormValidationException) {
                                MessageBox.create(message -> {
                                    message.message(excRwo.get().getMessage());
                                    message.type(MessageBox.TypeMessageBox.ALERT);
                                    message.show();
                                });
                            } else {
                                ExceptionBox.build(ebox -> {
                                    ebox.exception(excRwo.get());
                                    ebox.showAndWait();
                                });
                            }
                        }
                    });
                }
            }
        }
    }

    private void bloquearReserva() {
        // validações
        if (reservasBean.stream().noneMatch(VSdReservasAlocadas::isSelected)) {
            MessageBox.create(message -> {
                message.message("É necessário selecionar as reservas que serão bloqueadas.");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showAndWait();
            });
            return;
        }

        reservasBean.stream().filter(VSdReservasAlocadas::isSelected).forEach(reserva -> {
            if (reserva.getId().getResstatus().isReserva()) {
                try {
                    super.setReservaBloqueada(reserva, true);
                    reserva.setReslocked(true);
                } catch (SQLException e) {
                    e.printStackTrace();
                    ExceptionBox.build(message -> {
                        message.exception(e);
                        message.showAndWait();
                    });
                }
            }
        });

        MessageBox.create(message -> {
            message.message("Reservas bloqueadas. Na próxima rodada de atendimento essa reserva não será recalculada.");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
        tblReservasLiberadas.refresh();
    }

    private void getLiberaImpressao() {
        _LIBERA_IMPRESSAO.refresh();
        liberadoImpressao.clear();
        if (_LIBERA_IMPRESSAO.getValor().equals("TODOS")) {
            liberadoImpressao.addAll(Arrays.asList("ERP","B2B","B2C"));
        } else if (_LIBERA_IMPRESSAO.getValor().equals("LOJA")) {
            liberadoImpressao.addAll(Arrays.asList("B2B", "B2C"));
        } else {
            liberadoImpressao.add(_LIBERA_IMPRESSAO.getValor());
        }
    }

    private void imprimirRemessa() {
        getLiberaImpressao();
        if (_LIBERA_IMPRESSAO.getValor().equals("NAO")) {
            MessageBox.create(message -> {
                message.message("Não é permitido a impressão de remessas");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }
        if (reservasBean.stream().filter(VSdReservasAlocadas::isSelected)
                .filter(it -> {
                    JPAUtils.getEntityManager().refresh(it.getId().getCodcli());
                    return it.getId().getResstatus().isReserva() &&
                            (it.getPedidos().stream().allMatch(ped -> liberadoImpressao.contains(ped.getId().getNumero().getSdPedido().getOrigem())) || it.getId().getCodcli().isImprimeRemessa());})
                .count() == 0) {
            MessageBox.create(message -> {
                message.message("Você precisa selecionar ao menos uma reserva para impressão de remessa para coleta.\n" +
                        "É permitido somente impressões de remessas para clientes configurados ou com pedidos emitidos " + (
                                _LIBERA_IMPRESSAO.getValor().equals("TODOS") ? "em qualquer plataforma." :
                                        _LIBERA_IMPRESSAO.getValor().equals("LOJA") ? "nas plataformas B2B ou B2C" :
                                                ("na plataforma " + _LIBERA_IMPRESSAO.getValor())
                        ));
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }
        if (reservasBean.stream().filter(it -> it.getId().getResstatus().isReserva() && it.isSelected() && (it.getPedidos().stream().allMatch(ped -> liberadoImpressao.contains(ped.getId().getNumero().getSdPedido().getOrigem())) || it.getId().getCodcli().isImprimeRemessa()))
                .anyMatch(it -> it.getPedidos().stream().anyMatch(ped -> ped.getMarca() == null))) {
            MessageBox.create(message -> {
                message.message("Existem pedidos para essa reserva que estão sem a marca definida no pedido, verifique a marca antes de enviar.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }

        for (VSdReservasAlocadas reserva : reservasBean.stream()
                .filter(it -> it.getId().getResstatus().isReserva() && it.isSelected()
                            && (it.getPedidos().stream().allMatch(ped -> liberadoImpressao.contains(ped.getId().getNumero().getSdPedido().getOrigem())) || it.getId().getCodcli().isImprimeRemessa()))
                .collect(Collectors.toList())) {
            AtomicReference<Exception> excRwo = new AtomicReference<>();
            AtomicReference<List<Object>> codigosDuplicados = new AtomicReference<>();
            AtomicReference<VSdDadosCliente> clienteCodigosDuplicados = new AtomicReference<>();
            new RunAsyncWithOverlay(this).exec(task -> {
                try {
                    codigosDuplicados.set(getProdutosDuplicados(reserva.getPedidos().stream()
                            .map(pedido -> pedido.getId().getNumero().getNumero())
                            .distinct()
                            .collect(Collectors.joining("','"))));
                    if (codigosDuplicados.get().size() == 0) {
                        SdRemessaCliente remessaCriada = criarRemessa(reserva, _STATUS_IMPRESSAO);
                        String numeroRemessa = String.valueOf(remessaCriada.getRemessa());
                        refreshGetReservas();
                        AtomicReference<VSdReservasAlocadas> remessaParaImpressa = new AtomicReference<>(null);
                        super.reservas.stream().filter(remessa -> remessa.getId().getRemessa().equals(numeroRemessa)).findFirst().ifPresent(remessa -> remessaParaImpressa.set(remessa));
                        if (remessaParaImpressa.get() != null)
                            imprimirRemessa(remessaParaImpressa.get());
                    } else {
                        clienteCodigosDuplicados.set(reserva.getId().getCodcli());
                        return ReturnAsync.NOT_FOUND.value;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    excRwo.set(e);
                    return ReturnAsync.EXCEPTION.value;
                }
                return ReturnAsync.OK.value;
            }).addTaskEndNotification(taskReturn -> {
                if (taskReturn.equals(ReturnAsync.OK.value)) {
                    reservasBean.set(FXCollections.observableList(super.reservas));
                    criaIndiceRemessas();
                    tblReservasLiberadas.refresh();
                    MessageBox.create(message -> {
                        message.message("Impressão de remessas concluído");
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.position(Pos.TOP_RIGHT);
                        message.notification();
                    });
                }
                else if (taskReturn.equals(ReturnAsync.EXCEPTION.value)) {
                    if (excRwo.get() instanceof FormValidationException) {
                        MessageBox.create(message -> {
                            message.message(excRwo.get().getMessage());
                            message.type(MessageBox.TypeMessageBox.ALERT);
                            message.show();
                        });
                    } else {
                        ExceptionBox.build(ebox -> {
                            ebox.exception(excRwo.get());
                            ebox.showAndWait();
                        });
                    }
                }
                else if (taskReturn.equals(ReturnAsync.NOT_FOUND.value)) {
                    AtomicReference<String> descCodigosDuplicados = new AtomicReference<>("");
                    codigosDuplicados.get().forEach(produtos -> {
                        Map<String, Object> produtoPedido = (Map<String, Object>) produtos;
                        descCodigosDuplicados.set(descCodigosDuplicados.get() +
                                "Pedido: " + produtoPedido.get("NUMERO") +
                                " - Código: " + produtoPedido.get("CODIGO") +
                                " - Cor: " + produtoPedido.get("COR") +
                                " - Tam: " + produtoPedido.get("TAM") + "\n");
                    });
                    MessageBox.create(message -> {
                        message.message("Existem produtos duplicados para o cliente " + clienteCodigosDuplicados.get().toString() + " : \n" +
                                descCodigosDuplicados.get());
                        message.type(MessageBox.TypeMessageBox.ALERT);
                        message.show();
                    });
                }
            });
        }
    }

    private void enviarReserva() {
        if (reservasBean.stream().filter(it -> it.getId().getResstatus().isReserva()).noneMatch(VSdReservasAlocadas::isSelected)) {
            MessageBox.create(message -> {
                message.message("Você precisa seleiconar ao menos uma reserva para envio de remessa para coleta.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }
        if (reservasBean.stream().filter(it -> it.getId().getResstatus().isReserva() && it.isSelected())
                .anyMatch(it -> it.getPedidos().stream().anyMatch(ped -> ped.getMarca() == null))) {
            MessageBox.create(message -> {
                message.message("Existem pedidos para essa reserva que estão sem a marca definida no pedido, verifique a marca antes de enviar.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }


        List<VSdReservasAlocadas> reservasParaEnvio = reservasBean.stream().filter(it -> it.getId().getResstatus().isReserva() && it.isSelected()).collect(Collectors.toList());
        AtomicReference<Exception> excRwo = new AtomicReference<>();
        AtomicReference<List<Object>> codigosDuplicados = new AtomicReference<>();
        AtomicReference<VSdDadosCliente> clienteCodigosDuplicados = new AtomicReference<>();
        new RunAsyncWithOverlay(this).exec(task -> {
            for (VSdReservasAlocadas reserva : reservasParaEnvio) {
                try {
                    codigosDuplicados.set(getProdutosDuplicados(reserva.getPedidos().stream()
                            .map(pedido -> pedido.getId().getNumero().getNumero())
                            .distinct()
                            .collect(Collectors.joining("','"))));
                    if (codigosDuplicados.get().size() == 0) {
                        criarRemessa(reserva, _STATUS_TABLETS);
                    } else {
                        clienteCodigosDuplicados.set(reserva.getId().getCodcli());
                        return ReturnAsync.NOT_FOUND.value;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    excRwo.set(e);
                    return ReturnAsync.EXCEPTION.value;
                }
            }
            refreshGetReservas();
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                reservasBean.set(FXCollections.observableList(super.reservas));
                criaIndiceRemessas();
                tblReservasLiberadas.refresh();
                MessageBox.create(message -> {
                    message.message("Envio de remessas concluído");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            }
            else if (taskReturn.equals(ReturnAsync.EXCEPTION.value)) {
                if (excRwo.get() instanceof FormValidationException) {
                    MessageBox.create(message -> {
                        message.message(excRwo.get().getMessage());
                        message.type(MessageBox.TypeMessageBox.ALERT);
                        message.show();
                    });
                } else {
                    ExceptionBox.build(ebox -> {
                        ebox.exception(excRwo.get());
                        ebox.showAndWait();
                    });
                }
            }
            else if (taskReturn.equals(ReturnAsync.NOT_FOUND.value)) {
                AtomicReference<String> descCodigosDuplicados = new AtomicReference<>("");
                codigosDuplicados.get().forEach(produtos -> {
                    Map<String, Object> produtoPedido = (Map<String, Object>) produtos;
                    descCodigosDuplicados.set(descCodigosDuplicados.get() +
                            "Pedido: " + produtoPedido.get("NUMERO") +
                            " - Código: " + produtoPedido.get("CODIGO") +
                            " - Cor: " + produtoPedido.get("COR") +
                            " - Tam: " + produtoPedido.get("TAM") + "\n");
                });
                MessageBox.create(message -> {
                    message.message("Existem produtos duplicados para o cliente " + clienteCodigosDuplicados.get().toString() + " : \n" +
                            descCodigosDuplicados.get());
                    message.type(MessageBox.TypeMessageBox.ALERT);
                    message.show();
                });
            }
        });
    }

    private void criaIndiceRemessas() {
        tblReservasLiberadas.clearIndices();
        List<FormTableView<VSdReservasAlocadas>.ItemIndice> indices = new ArrayList<>();
        reservasBean.stream().map(reserva -> reserva.getId().getResstatus()).distinct().forEach(status -> {
            indices.add(tblReservasLiberadas.indice("[" + status.getCodigo() + "] " + status.getDescricao(), status.getCor(), ""));
        });
        tblReservasLiberadas.indices(indices);
    }

//    private void removerItemRemessaManual(TreeItem<ItensTreeView> itemSelecionado) {
//        if (itemSelecionado != null) {
//            VSdReservasAlocadas reservaSelecionada = tblReservasLiberadas.selectedItem();
//            VSdResPedidoCliente pedidoSelecionado = tblPedidosCliente.selectedItem();
//
//            // validação se é permitido excluir ou cancelar um item do pedido
//            if (!reservaSelecionada.getResstatus().isCancelamento() && !reservaSelecionada.getResstatus().isExclusao()) {
//                MessageBox.create(message -> {
//                    message.message("Você não pode excluir ou cancelar itens em remessas expedidas ou faturadas.");
//                    message.type(MessageBox.TypeMessageBox.WARNING);
//                    message.showAndWait();
//                });
//                return;
//            }
//            AtomicReference<Exception> excRwo = new AtomicReference<>();
//            new RunAsyncWithOverlay(this).exec(task -> {
//                try {
//                    // verificando se é do tipo reserva
//                    if (reservaSelecionada.getResstatus().isReserva()) {
//                        excluirItemReserva(itemSelecionado, reservaSelecionada, pedidoSelecionado);
//                    } else {
//                        // cancelando item em uma remessa
//                        cancelarItemRemessa(itemSelecionado, reservaSelecionada, pedidoSelecionado);
//                    }
//                } catch (SQLException e) {
//                    e.printStackTrace();
//                    excRwo.set(e);
//                    return ReturnAsync.EXCEPTION.value;
//                }
//                return ReturnAsync.OK.value;
//            }).addTaskEndNotification(taskReturn -> {
//                if (taskReturn.equals(ReturnAsync.OK.value)) {
//                    // refresh de tela
//                    tblReservasLiberadas.refresh();
//                    tblPedidosCliente.refresh();
//                    tblItensPedido.refresh();
//                } else if (taskReturn.equals(ReturnAsync.EXCEPTION.value)) {
//                    ExceptionBox.build(message -> {
//                        message.exception(excRwo.get());
//                        message.showAndWait();
//                    });
//                }
//            });
//        }
//    }

    private void removerItem(TreeItem<ItensTreeView> itemSelecionado) {
        if (itemSelecionado != null) {
            VSdReservasAlocadas reservaSelecionada = tblReservasLiberadas.selectedItem();
            VSdResPedidoCliente pedidoSelecionado = tblPedidosCliente.selectedItem();

            // validação se é permitido excluir ou cancelar um item do pedido
            if (!reservaSelecionada.getId().getResstatus().isCancelamento() && !reservaSelecionada.getId().getResstatus().isExclusao()) {
                MessageBox.create(message -> {
                    message.message("Você não pode excluir ou cancelar itens em remessas expedidas ou faturadas.");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showAndWait();
                });
                return;
            }

            // validação de peças lá lida na remessa em coleta
            if (!reservaSelecionada.getId().getResstatus().isReserva()) {
                if (itemSelecionado.getValue().tam.get().equals("-")) {
                    ((VSdResRefCorPedido) itemSelecionado.getValue().object.get()).getGrade().forEach(VSdResGradeRefCor::refresh);
                    if (((VSdResRefCorPedido) itemSelecionado.getValue().object.get()).getGrade().stream().anyMatch(grade -> grade.getStatusitem().equals("C"))) {
                        MessageBox.create(message -> {
                            message.message("Você está cancelando um item que contém coleta, você deve primeiro retirar as peças da caixa.");
                            message.type(MessageBox.TypeMessageBox.WARNING);
                            message.showAndWait();
                        });
                        return;
                    }
                } else {
                    ((VSdResGradeRefCor) itemSelecionado.getValue().object.get()).refresh();
                    if (((VSdResGradeRefCor) itemSelecionado.getValue().object.get()).getStatusitem().equals("C")) {
                        MessageBox.create(message -> {
                            message.message("Você está cancelando um item que contém coleta, você deve primeiro retirar as peças da caixa.");
                            message.type(MessageBox.TypeMessageBox.WARNING);
                            message.showAndWait();
                        });
                        return;
                    }
                }
            }

            AtomicReference<Exception> excRwo = new AtomicReference<>();
            new RunAsyncWithOverlay(this).exec(task -> {
                try {
                    // verificando se é do tipo reserva
                    if (reservaSelecionada.getId().getResstatus().isReserva()) {
                        // cancelando item em uma remessa
                        if (itemSelecionado.getValue().tam.get().equals("-")) {
                            for (VSdResGradeRefCor grade : ((VSdResRefCorPedido) itemSelecionado.getValue().object.get()).getGrade()) {
                                excluirGradeReserva(grade, reservaSelecionada);
                            }
                        } else {
                            excluirGradeReserva(((VSdResGradeRefCor) itemSelecionado.getValue().object.get()), reservaSelecionada);
                        }
                    } else {
                        // cancelando item em uma remessa
                        if (itemSelecionado.getValue().tam.get().equals("-")) {
                            for (VSdResGradeRefCor grade : ((VSdResRefCorPedido) itemSelecionado.getValue().object.get()).getGrade()) {
                                cancelarGradeRemessa(grade);
                                grade.refresh();
                            }
                        } else {
                            cancelarGradeRemessa(((VSdResGradeRefCor) itemSelecionado.getValue().object.get()));
                            ((VSdResGradeRefCor) itemSelecionado.getValue().object.get()).refresh();
                        }
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                    excRwo.set(e);
                    return ReturnAsync.EXCEPTION.value;
                }
                return ReturnAsync.OK.value;
            }).addTaskEndNotification(taskReturn -> {
                if (taskReturn.equals(ReturnAsync.OK.value)) {
                    try {
                        if (!reservaSelecionada.getId().getResstatus().getCodigo().equals(confirmaStatusRemessa(reservaSelecionada).getCodigo())) {
                            tblItensPedido.clear();
                            pedidosClienteBean.clear();
                            reservasBean.remove(reservaSelecionada);
                        } else {
                            pedidoSelecionado.refresh();
                            pedidoSelecionado.getItens().forEach(VSdResRefCorPedido::refresh);
                            reservaSelecionada.refresh();
                            adicionarItensPedidoTable(pedidoSelecionado);

                            // refresh de tela
                            tblReservasLiberadas.refresh();
                            tblPedidosCliente.refresh();
                            tblItensPedido.refresh();
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                        ExceptionBox.build(message -> {
                            message.exception(e);
                            message.showAndWait();
                        });
                    }

                    MessageBox.create(message -> {
                        message.message("Exclusão/cancelamento de item realizado com sucesso.");
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.position(Pos.TOP_RIGHT);
                        message.notification();
                    });
                } else if (taskReturn.equals(ReturnAsync.EXCEPTION.value)) {
                    ExceptionBox.build(message -> {
                        message.exception(excRwo.get());
                        message.showAndWait();
                    });
                }
            });
        }
    }

//    private void excluirPedidoRemessaManual(VSdResPedidoCliente pedido) {
//        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
//            message.message("Deseja realmente excluir o pedido " + pedido.getNumero().getNumero());
//            message.showAndWait();
//        }).value.get())) {
//            VSdReservasAlocadas reservaSelecionada = tblReservasLiberadas.selectedItem();
//            List<TreeItem<ItensTreeView>> paraRemover = new ArrayList<>(tblItensPedido.properties().getRoot().getChildren());
//            paraRemover.forEach(item -> removerItemRemessaManual(item));
//            if (reservaSelecionada.getResstatus().isReserva()) {
//                if (reservaSelecionada.getPedidos().size() > 1)
//                    reservaSelecionada.getPedidos().remove(pedido);
//            }
//            MessageBox.create(message -> {
//                message.message("Finalizado exclusão/cancelamento do pedido.");
//                message.type(MessageBox.TypeMessageBox.CONFIRM);
//                message.position(Pos.TOP_RIGHT);
//                message.notification();
//            });
//        }
//    }

    private void removerPedido(VSdResPedidoCliente pedido) {
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Deseja realmente excluir o pedido " + pedido.getId().getNumero().getNumero());
            message.showAndWait();
        }).value.get())) {
            VSdReservasAlocadas reservaSelecionada = tblReservasLiberadas.selectedItem();

            // validação se tem itens coletados no pedido remessa
            if (!reservaSelecionada.getId().getResstatus().isReserva() && reservaSelecionada.getPedidos().stream().mapToLong(pedRemessa -> pedRemessa.getItens().stream().filter(item -> item.getStatusitem().equals("C")).count()).sum() > 0) {
                MessageBox.create(message -> {
                    message.message("Existem itens coletados neste pedido, o mesmo não pode ser cancelado. Você pode entrar no pedido e cancelar as peças pendentes.");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showAndWait();
                });
                return;
            }

            // validação se é permitido excluir ou cancelar um item do pedido
            if (!reservaSelecionada.getId().getResstatus().isCancelamento() && !reservaSelecionada.getId().getResstatus().isExclusao()) {
                MessageBox.create(message -> {
                    message.message("Você não pode excluir ou cancelar itens em remessas expedidas ou faturadas.");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showAndWait();
                });
                return;
            }

            AtomicReference<Exception> excRwo = new AtomicReference<>();
            new RunAsyncWithOverlay(this).exec(task -> {
                try {
                    // verificando se é do tipo reserva
                    if (reservaSelecionada.getId().getResstatus().isReserva()) {
                        // cancelando item em uma remessa
                        for (VSdResRefCorPedido item : pedido.getItens()) {
                            for (VSdResGradeRefCor grade : item.getGrade()) {
                                excluirGradeReserva(grade, reservaSelecionada);
                            }
                        }
                    } else {
                        // cancelando item em uma remessa
                        for (VSdResRefCorPedido item : pedido.getItens()) {
                            for (VSdResGradeRefCor grade : item.getGrade()) {
                                cancelarGradeRemessa(grade);
                                grade.refresh();
                            }
                        }
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                    excRwo.set(e);
                    return ReturnAsync.EXCEPTION.value;
                }
                return ReturnAsync.OK.value;
            }).addTaskEndNotification(taskReturn -> {
                if (taskReturn.equals(ReturnAsync.OK.value)) {
                    try {
                        if (!reservaSelecionada.getId().getResstatus().getCodigo().equals(confirmaStatusRemessa(reservaSelecionada).getCodigo())) {
                            tblItensPedido.clear();
                            pedidosClienteBean.clear();
                            reservasBean.remove(reservaSelecionada);
                        } else {
                            pedido.refresh();
                            pedido.getItens().forEach(VSdResRefCorPedido::refresh);
                            reservaSelecionada.refresh();
                            if (tblPedidosCliente.selectedItem() != null)
                                adicionarItensPedidoTable(tblPedidosCliente.selectedItem());

                            // refresh de tela
                            tblReservasLiberadas.refresh();
                            tblPedidosCliente.refresh();
                            tblItensPedido.refresh();
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                        ExceptionBox.build(message -> {
                            message.exception(e);
                            message.showAndWait();
                        });
                    }

                    MessageBox.create(message -> {
                        message.message("Finalizado exclusão/cancelamento do pedido.");
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.position(Pos.TOP_RIGHT);
                        message.notification();
                    });
                } else if (taskReturn.equals(ReturnAsync.EXCEPTION.value)) {
                    ExceptionBox.build(message -> {
                        message.exception(excRwo.get());
                        message.showAndWait();
                    });
                }
            });
        }
    }

    private void cancelarRemessa(VSdReservasAlocadas remessa) {
        List<SdCaixaRemessa> caixasRemessa = (List<SdCaixaRemessa>) new FluentDao().selectFrom(SdCaixaRemessa.class).where(eb -> eb.equal("remessa.remessa", remessa.getId().getRemessa())).resultList();
        if (caixasRemessa.size() > 0) {
            MessageBox.create(message -> {
                message.message("Esta remessa não pode ser cancelada pois existem caixas vinculadas a ela.");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showAndWait();
            });
            return;
        }
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Deseja realmente cancelar a remessa " + remessa.getId().getRemessa());
            message.showAndWait();
        }).value.get())) {
            boolean liberaExcluir = false;
            if (remessa != null && !remessa.getEmissor().equals(Globals.getUsuarioLogado().getUsuario())){
                if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                    message.message("Essa remessa foi criada por outro usuário (" + remessa.getEmissor() + "), deseja realmente excluir a remessa?");
                    message.showAndWait();
                }).value.get())) {
                    liberaExcluir = true;
                    SysLogger.addSysDelizLog("Análise de Remessas", TipoAcao.EXCLUIR, remessa.getId().getRemessa(), "Usuário cancelando/excluindo remessa de outro usuário (" + remessa.getEmissor() + ")");
                }
            } else
                liberaExcluir = true;

            if (liberaExcluir) {
                AtomicReference<Exception> excRwo = new AtomicReference<>();
                new RunAsyncWithOverlay(this).exec(task -> {
                    try {
                        // cancelando item em uma remessa
                        for (VSdResPedidoCliente pedido : remessa.getPedidos()) {
                            for (VSdResRefCorPedido item : pedido.getItens()) {
                                for (VSdResGradeRefCor grade : item.getGrade()) {
                                    cancelarGradeRemessa(grade);
                                    grade.refresh();
                                }
                            }
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                        excRwo.set(e);
                        return ReturnAsync.EXCEPTION.value;
                    }
                    return ReturnAsync.OK.value;
                }).addTaskEndNotification(taskReturn -> {
                    if (taskReturn.equals(ReturnAsync.OK.value)) {
                        reservasBean.remove(remessa);

                        MessageBox.create(message -> {
                            message.message("Finalizado cancelamento da remessa.");
                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                            message.position(Pos.TOP_RIGHT);
                            message.notification();
                        });
                    } else if (taskReturn.equals(ReturnAsync.EXCEPTION.value)) {
                        ExceptionBox.build(message -> {
                            message.exception(excRwo.get());
                            message.showAndWait();
                        });
                    }
                });
            }
        }
    }

    private void dividirRemessa(VSdResPedidoCliente item) {
        VSdReservasAlocadas reservaOriginal = tblReservasLiberadas.selectedItem();
        AtomicReference<SQLException> excAssync = new AtomicReference<>();
        /**
         * task: divideRemessa(reservaOriginal, item); * return: atualização array reservas
         */
        new RunAsyncWithOverlay(this).exec(task -> {
            try {
                divideRemessa(reservaOriginal, item);
            } catch (SQLException e) {
                e.printStackTrace();
                excAssync.set(e);
            }
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                reservasBean.set(FXCollections.observableList(reservas));
                tblReservasLiberadas.refresh();
                tblReservasLiberadas.selectItem(0);
            } else {
                ExceptionBox.build(message -> {
                    message.exception(excAssync.get());
                    message.showAndWait();
                });
            }
        });
    }

    private void incluirDataBase(VSdResPedidoCliente item) {
        LocalDate dataBaseFatura = (LocalDate) InputBox.build(LocalDate.class, boxInput -> {
            boxInput.message("Insira a data base para o faturamento:");
            boxInput.showAndWait();
        }).value.getValue();

        if (dataBaseFatura != null) {
            try {
                incluirDataBase(item, dataBaseFatura);
                item.refresh();
                tblPedidosCliente.refresh();
                MessageBox.create(message -> {
                    message.message("Data base incluída no pedido: " + item.getId().getNumero().getNumero());
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            } catch (SQLException e) {
                e.printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(e);
                    message.showAndWait();
                });
            }
        }
    }

    private void incluirDesconto(VSdResPedidoCliente pedido) {
        new Fragment().show(fragment -> {
            fragment.title("Desc. Remessa");
            fragment.size(300.0, 200.0);

            final FormFieldText fieldValorDesconto = FormFieldText.create(field -> {
                field.title("Desconto");
                field.label("R$");
                field.mask(FormFieldText.Mask.DOUBLE);
                field.value.set("0");
            });
            fragment.box.getChildren().add(FormBox.create(boxPrincipal -> {
                boxPrincipal.vertical();
                boxPrincipal.add(fieldValorDesconto.build());
            }));

            fragment.buttonsBox.getChildren().add(FormButton.create(btnConceder -> {
                btnConceder.title("Aplicar Desconto");
                btnConceder.icon(ImageUtils.getIcon(ImageUtils.Icon.DESCONTO, ImageUtils.IconSize._24));
                btnConceder.addStyle("success");
                btnConceder.setAction(evt -> {
                    try {
                        fieldValorDesconto.validate();

                        BigDecimal desconto = new BigDecimal(fieldValorDesconto.value.get().replace(",", "."));
                        incluirDesconto(pedido, desconto);
                        MessageBox.create(message -> {
                            message.message("Desconto concedido no pedido!");
                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                            message.position(Pos.TOP_RIGHT);
                            message.notification();
                        });
                        fragment.close();
                        pedido.refresh();
                        tblPedidosCliente.refresh();
                    } catch (FormValidationException validate) {
                        MessageBox.create(message -> {
                            message.message(validate.getMessage());
                            message.type(MessageBox.TypeMessageBox.IMPORTANT);
                            message.showAndWait();
                        });
                    } catch (SQLException e) {
                        e.printStackTrace();
                        ExceptionBox.build(message -> {
                            message.exception(e);
                            message.showAndWait();
                        });
                    }
                });
            }));
        });
    }

    private void incluirObservacao(VSdResPedidoCliente pedido) {

        new Fragment().show(fragment -> {
            fragment.title("Observações de Produto");
            fragment.size(300.0, 250.0);

            final FormFieldTextArea observacaoRemessaField = FormFieldTextArea.create(field -> {
                field.title("Observação");
                field.expanded();
                field.height(250.0);
            });
            fragment.box.getChildren().add(observacaoRemessaField.build());
            fragment.buttonsBox.getChildren().add(FormButton.create(btn -> {
                btn.title("Salvar");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.SALVAR, ImageUtils.IconSize._24));
                btn.addStyle("primary");
                btn.setAction(evt -> {
                    try {
                        for (VSdResRefCorPedido item : pedido.getItens()) {
                            adicionarObservacaoPedido(item, observacaoRemessaField.value.get());
                        }
                        MessageBox.create(message -> {
                            message.message("Observação cadastrada com sucesso no item.");
                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                            message.position(Pos.TOP_RIGHT);
                            message.notification();
                        });
                        fragment.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                        ExceptionBox.build(message -> {
                            message.exception(e);
                            message.showAndWait();
                        });
                    }
                });
            }));

        });

    }

    @NotNull
    private ContextMenu getContextResumoPedido(VSdResPedidoCliente item) {
        return FormContextMenu.create(menu -> {
            // abrir resumo do pedido
            menu.addItem(menuItem -> {
                menuItem.setText("Resumo Pedido");
                menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.VISUALIZAR_PEDIDO, ImageUtils.IconSize._16));
                menuItem.setOnAction(evt -> {
                    new Fragment().show(fragment -> {
                        fragment.title("Resumo do Pedido");
                        fragment.size(500.0, 300.0);
                        fragment.box.getChildren().add(FormBox.create(boxResumoPedido -> {
                            boxResumoPedido.vertical();
                            boxResumoPedido.expanded();
                            List<VSdDadosPedidoData> dadosPediddo = (List<VSdDadosPedidoData>) new FluentDao().selectFrom(VSdDadosPedidoData.class)
                                    .where(it -> it.equal("id.numero", item.getId().getNumero().getNumero()))
                                    .orderBy("id.entrega", OrderType.ASC)
                                    .resultList();

                            boxResumoPedido.add(FormTableView.create(VSdReservasAlocadas.class, tableResumo -> {
                                tableResumo.title("Resumo");
                                tableResumo.items.set(FXCollections.observableList(dadosPediddo));
                                tableResumo.expanded();
                                tableResumo.columns(
                                        FormTableColumn.create(cln -> {
                                            cln.title("Dt. Entrega");
                                            cln.width(90.0);
                                            cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosPedidoData, VSdDadosPedidoData>, ObservableValue<VSdDadosPedidoData>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getEntrega()));
                                            cln.alignment(Pos.CENTER);
                                            cln.format(param -> {
                                                return new TableCell<VSdDadosPedidoData, LocalDate>() {
                                                    @Override
                                                    protected void updateItem(LocalDate item, boolean empty) {
                                                        super.updateItem(item, empty);
                                                        setText(null);
                                                        if (item != null && !empty) {
                                                            setText(StringUtils.toDateFormat(item));
                                                        }
                                                    }
                                                };
                                            });
                                        }).build() /*Dt. Entrega*/,
                                        FormTableColumn.create(cln -> {
                                            cln.title("Pend.");
                                            cln.width(60.0);
                                            cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosPedidoData, VSdDadosPedidoData>, ObservableValue<VSdDadosPedidoData>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                                            cln.alignment(Pos.CENTER);
                                        }).build() /*Pend.*/,
                                        FormTableColumn.create(cln -> {
                                            cln.title("Valor Pend.");
                                            cln.width(100.0);
                                            cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosPedidoData, VSdDadosPedidoData>, ObservableValue<VSdDadosPedidoData>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValorpend()));
                                            cln.alignment(Pos.CENTER_RIGHT);
                                            cln.format(param -> {
                                                return new TableCell<VSdDadosPedidoData, BigDecimal>() {
                                                    @Override
                                                    protected void updateItem(BigDecimal item, boolean empty) {
                                                        super.updateItem(item, empty);
                                                        setText(null);
                                                        if (item != null && !empty) {
                                                            setText(StringUtils.toMonetaryFormat(item, 2));
                                                        }
                                                    }
                                                };
                                            });
                                        }).build() /*Valor Pend.*/,
                                        FormTableColumn.create(cln -> {
                                            cln.title("Fatur.");
                                            cln.width(60.0);
                                            cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosPedidoData, VSdDadosPedidoData>, ObservableValue<VSdDadosPedidoData>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtdef()));
                                            cln.alignment(Pos.CENTER);
                                        }).build() /*Fatur.*/,
                                        FormTableColumn.create(cln -> {
                                            cln.title("Valor Fat.");
                                            cln.width(100.0);
                                            cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosPedidoData, VSdDadosPedidoData>, ObservableValue<VSdDadosPedidoData>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValorfat()));
                                            cln.alignment(Pos.CENTER_RIGHT);
                                            cln.format(param -> {
                                                return new TableCell<VSdDadosPedidoData, BigDecimal>() {
                                                    @Override
                                                    protected void updateItem(BigDecimal item, boolean empty) {
                                                        super.updateItem(item, empty);
                                                        setText(null);
                                                        if (item != null && !empty) {
                                                            setText(StringUtils.toMonetaryFormat(item, 2));
                                                        }
                                                    }
                                                };
                                            });
                                        }).build() /*Valor Fat.*/
                                );
                            }).build());
                        }));
                    });
                });
            });
            if (item.getMarca() == null) {
                menu.addItem(menuItem -> {
                    menuItem.setText("Incluir Marca do Pedido");
                    menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.MARCA, ImageUtils.IconSize._16));
                    menuItem.setOnAction(evt -> {
                        InputBox<Marca> marcaPedido = InputBox.build(Marca.class, boxInput -> {
                            boxInput.message("Selecione a marca do pedido:");
                            boxInput.showAndWait();
                        });
                        if (marcaPedido.value.get() != null) {
                            try {
                                incluirMarcaPedido(item, marcaPedido.value.get());
                            } catch (SQLException throwables) {
                                throwables.printStackTrace();
                                ExceptionBox.build(message -> {
                                    message.exception(throwables);
                                    message.showAndWait();
                                });
                            }
                        }
                    });
                });
            }
            if (!tblReservasLiberadas.selectedItem().getId().getResstatus().isReserva()) {
                if ((item.getQtde() - item.getQtdec() > 0)
                        && tblReservasLiberadas.selectedItem().getPedidos().size() > 1
                        && tblReservasLiberadas.selectedItem().getId().getResstatus().isExcremessa()) {
                    menu.addItem(menuItem -> {
                        menuItem.setText("Alocar Nova Remessa");
                        menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR_PEDIDO, ImageUtils.IconSize._16));
                        menuItem.setOnAction(evt -> {
                            dividirRemessa(item);
                        });
                    });
                }
                menu.addItem(menuItem -> {
                    menuItem.setText("Incluir Data Base");
                    menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR_CALENDARIO, ImageUtils.IconSize._16));
                    menuItem.setOnAction(evt -> {
                        incluirDataBase(item);
                    });
                });
                menu.addItem(menuItem -> {
                    menuItem.setText("Incluir Desconto");
                    menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.DESCONTO, ImageUtils.IconSize._16));
                    menuItem.setOnAction(evt -> {
                        incluirDesconto(item);
                    });
                });
                menu.addItem(menuItem -> {
                    menuItem.setText("Observação Itens");
                    menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.ABRIR, ImageUtils.IconSize._16));
                    menuItem.setOnAction(evt -> {
                        incluirObservacao(item);
                    });
                });
            }
        });
    }

    @NotNull
    private ContextMenu getContextMenuReserva(VSdReservasAlocadas item) {
        return FormContextMenu.create(menu -> {
            // Atualizar Remessa sem status definido
            if (item.getId().getResstatus().getCodigo().equals("0"))
                menu.addItem(menuItem -> {
                    menuItem.setText("Redefinir Status Remessa");
                    menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.ALTERAR_STATUS, ImageUtils.IconSize._16));
                    menuItem.setOnAction(evt -> {
                        try {
                            alterarStatusRemessa(item);
                            item.refresh();
                            tblReservasLiberadas.refresh();
                        } catch (SQLException e) {
                            e.printStackTrace();
                            ExceptionBox.build(message -> {
                                message.exception(e);
                                message.showAndWait();
                            });
                        }
                    });
                });
            // Adicionar observação em remessa
            if (!item.getId().getResstatus().isReserva() && Arrays.asList("X", "I").contains(item.getId().getResstatus().getCodigo()))
                menu.addItem(menuItem -> {
                    menuItem.setText("Observação");
                    menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.ABRIR, ImageUtils.IconSize._16));
                    menuItem.setOnAction(evt -> adicionarObsRemessa(item));
                });
            // Transferir remessa da fila para impressão
            if (!item.getId().getResstatus().isReserva() && item.getId().getResstatus().getCodigo().equals("X") && !_LIBERA_IMPRESSAO.getValor().equals("NAO")) {
                menu.addItem(menuItem -> {
                    menuItem.setText("Imprimir Remessa");
                    menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.IMPRIMIR, ImageUtils.IconSize._16));
                    menuItem.setOnAction(evt -> imprimirRemessaEmFila(item));
                });
                menu.addItem(menuItem -> {
                    menuItem.setText("Alterar Prioridade");
                    menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.RECOLHER, ImageUtils.IconSize._16));
                    menuItem.setOnAction(evt -> atualizarPrioridadeRemessa(item));
                });
            }
            // Transferir remessa da impressao para fila
            // Re-imprimi uma remessa
            if (!item.getId().getResstatus().isReserva() && item.getId().getResstatus().getCodigo().equals("I")) {
                menu.addItem(menuItem -> {
                    menuItem.setText("Re-imprimir Remessa");
                    menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.IMPRIMIR, ImageUtils.IconSize._16));
                    menuItem.setOnAction(evt -> {
                        AtomicReference<Exception> exc = new AtomicReference<>();
                        new RunAsyncWithOverlay(this).exec(task -> {
                            try {
                                new ReportUtils()
                                        .config().addReport(ReportUtils.ReportFile.ROMANEIO_COLETA_REMESSA, new ReportUtils.ParameterReport("pRemessa", item.getId().getRemessa()), new ReportUtils.ParameterReport("codEmpresa", Globals.getEmpresaLogada().getCodigo()))
                                        .view().printWithDialog();
                            } catch (IOException | JRException | SQLException e) {
                                e.printStackTrace();
                                exc.set(e);
                            }
                            return ReturnAsync.OK.value;
                        }).addTaskEndNotification(taskReturn -> {
                            if (taskReturn.equals(ReturnAsync.EXCEPTION.value)) {
                                ExceptionBox.build(message -> {
                                    message.exception(exc.get());
                                    message.showAndWait();
                                });
                            }
                        });
                    });
                });
                menu.addItem(menuItem -> {
                    menuItem.setText("Enviar Remessa Fila");
                    menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._16));
                    menuItem.setOnAction(evt -> enviarRemessaImpressa(item));
                });
            }
            // cancelar remessa
            if (item.getId().getResstatus().isCancelamento()) {
                menu.addItem(menuItem -> {
                    menuItem.setText("Cancelar Remessa");
                    menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                    menuItem.setOnAction(evt -> cancelarRemessa(item));
                });
            }
            // Desbloquear uma reserva
            if (item.isReslocked() && item.getId().getResstatus().isReserva())
                menu.addItem(menuItem -> {
                    menuItem.setText("Desbloquear Reserva");
                    menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.BLOQUEAR, ImageUtils.IconSize._16));
                    menuItem.setOnAction(evt -> {
                        try {
                            setReservaBloqueada(item, false);
                            tblReservasLiberadas.refresh();
                        } catch (SQLException e) {
                            e.printStackTrace();
                            ExceptionBox.build(message -> {
                                message.exception(e);
                                message.showAndWait();
                            });
                        }
                    });
                });
            // Liberar remessa em revisão
            if (item.getId().getResstatus().getCodigo().equals("J"))
                menu.addItem(menuItem -> {
                    menuItem.setText("Liberar Remessa");
                    menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._16));
                    menuItem.setOnAction(evt -> {
                        try {
                            liberarRemessaEmRevisao(item);
                            item.refresh();
                            tblReservasLiberadas.refresh();
                        } catch (SQLException e) {
                            e.printStackTrace();
                            ExceptionBox.build(message -> {
                                message.exception(e);
                                message.showAndWait();
                            });
                        }
                    });
                });
        });
    }

    private void alterarStatusRemessa(VSdReservasAlocadas remessa) throws SQLException {
        new Fragment().show(fragment -> {
            fragment.title("Observações de Remessa");
            fragment.size(300.0, 250.0);

            final FormFieldSingleFind<SdStatusRemessa> fieldStatusRemessa = FormFieldSingleFind.create(SdStatusRemessa.class, field -> {
                field.title("Status");
            });
            fragment.box.getChildren().add(fieldStatusRemessa.build());
            fragment.buttonsBox.getChildren().add(FormButton.create(btn -> {
                btn.title("Atualizar");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ATUALIZAR, ImageUtils.IconSize._24));
                btn.addStyle("primary");
                btn.setAction(evt -> {
                    try {
                        fieldStatusRemessa.validate();
                        alterarStatusRemessa(remessa, fieldStatusRemessa.value.get());
                        remessa.refresh();
                        tblReservasLiberadas.tableProperties().getSelectionModel().clearSelection();
                        tblReservasLiberadas.refresh();
                        tblReservasLiberadas.selectItem(remessa);
                        MessageBox.create(message -> {
                            message.message("Atualizado status da remessa para: " + fieldStatusRemessa.value.get().getDescricao());
                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                            message.position(Pos.TOP_RIGHT);
                            message.notification();
                        });
                        fragment.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                        ExceptionBox.build(message -> {
                            message.exception(e);
                            message.showAndWait();
                        });
                    } catch (FormValidationException e) {
                        MessageBox.create(message -> {
                            message.message(e.getMessage());
                            message.type(MessageBox.TypeMessageBox.ALERT);
                            message.showAndWait();
                        });
                    }
                });
            }));
        });
    }
}
