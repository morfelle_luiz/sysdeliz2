package sysdeliz2.views.expedicao;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import sysdeliz2.controllers.views.expedicao.AnaliseSolicitacaoPecasController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.sysdeliz.*;
import sysdeliz2.models.sysdeliz.solicitacaoExp.*;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.ti.Produto;
import sysdeliz2.models.view.VSdDadosProdutoBarra;
import sysdeliz2.utils.ReportUtils;
import sysdeliz2.utils.SortUtils;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class AnaliseSolicitacaoPecasView extends AnaliseSolicitacaoPecasController {

    //<editor-fold desc="Lists Bean">
    private final ListProperty<SdSolicitacaoExp> solicitacoesBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<SdItemSolicitacaoExp> itensSolicitacaoBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<SdLogSysdeliz001> itensLogBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private List<SdStatusSolicitacaoExp> listStatus = (List<SdStatusSolicitacaoExp>) new FluentDao().selectFrom(SdStatusSolicitacaoExp.class).get().orderBy("codigo", OrderType.ASC).resultList();

    //</editor-fold>

    private SdSolicitacaoExp solicitacaoEscolhida = null;

    //<editor-fold desc="Field Text">
    private final FormFieldText numeroSolicitacaoField = FormFieldText.create(field -> {
        field.title("Código");
        field.editable.set(false);
    });

    private final FormFieldText fieldDataSolicitacao = FormFieldText.create(field -> {
        field.title("Data de Solicitação");
        field.editable.set(false);
        field.width(160);
    });

    private final FormFieldText fieldDataEnvioProduto = FormFieldText.create(field -> {
        field.title("Data de Envio");
        field.editable.set(false);
        field.width(160);
    });

    private final FormFieldText fieldDataDevolucao = FormFieldText.create(field -> {
        field.title("Data de Devolução");
        field.editable.set(false);
        field.width(160);
    });

    private final FormFieldSingleFind<SdTipoSolicitacaoExp> tipoSolicitacaoField = FormFieldSingleFind.create(SdTipoSolicitacaoExp.class, field -> {
        field.title("Tipo");
        field.width(470.0);
        field.withoutSearchButton();
        field.code.setPrefWidth(100);
    });

    private final FormFieldText qtdeProdutosField = FormFieldText.create(field -> {
        field.title("Qtde Produtos");
        field.editable.set(false);
        field.width(120);
    });

    private final FormFieldText qtdeTotalItensField = FormFieldText.create(field -> {
        field.title("Qtde Total");
        field.editable.set(false);
        field.width(120);
    });

    private final FormFieldText fieldStatus = FormFieldText.create(field -> {
        field.title("Status");
        field.editable.set(false);
        field.width(220);
    });

    private final FormFieldText fieldUsuario = FormFieldText.create(field -> {
        field.title("Usuário");
        field.editable.set(false);
        field.width(120);
    });

    private final FormFieldTextArea fieldDescricao = FormFieldTextArea.create(field -> {
        field.title("Descrição");
        field.editable.set(false);
    });

    //</editor-fold>

    //<editor-fold desc="Filter">
    private final FormFieldDatePeriod filterDataSolicitacao = FormFieldDatePeriod.create(field -> {
        field.title("Data");
        field.setPadding(new Insets(-5, 5, 5, 5));
        field.withLabels();
        field.valueBegin.setValue(LocalDate.now().minusMonths(1));
        field.valueEnd.setValue(LocalDate.now().plusMonths(1));
    });

    private final FormFieldSingleFind<SdTipoSolicitacaoExp> filterTipoSolicitacao = FormFieldSingleFind.create(SdTipoSolicitacaoExp.class, field -> {
        field.title("Tipo");
        field.dividedWidth(400);
    });

    private final FormFieldSingleFind<SdStatusSolicitacaoExp> filterStatusSolicitacao = FormFieldSingleFind.create(SdStatusSolicitacaoExp.class, field -> {
        field.title("Status");
        field.dividedWidth(250);
    });

    private final FormFieldMultipleFind<Produto> filterProduto = FormFieldMultipleFind.create(Produto.class, field -> {
        field.title("Produto");
        field.width(150);
    });

    private final FormFieldMultipleFind<SdSolicitacaoExp> filterSolicitacaoExp = FormFieldMultipleFind.create(SdSolicitacaoExp.class, field -> {
        field.title("Código");
        field.width(150);
    });

    private final FormFieldMultipleFind<Colecao> filterColecao = FormFieldMultipleFind.create(Colecao.class, field -> {
        field.title("Coleção");
        field.width(150);
    });

    private final FormFieldMultipleFind<Marca> filterMarca = FormFieldMultipleFind.create(Marca.class, field -> {
        field.title("Marca");
        field.width(150);
    });

    private final FormFieldText filterUsuario = FormFieldText.create(field -> {
        field.title("Usuario");
        field.width(150);
    });

    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="TableView">
    private final FormTableView<SdLogSysdeliz001> tblFluxoSolicitacao = FormTableView.create(SdLogSysdeliz001.class, table -> {
        table.title("Fluxo");
        table.items.bind(itensLogBean);
        table.expanded();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(700);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdLogSysdeliz001, SdLogSysdeliz001>, ObservableValue<SdLogSysdeliz001>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDescricao()));
                }).build(), /*Descrição*/
                FormTableColumn.create(cln -> {
                    cln.title("Data");
                    cln.width(130);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdLogSysdeliz001, SdLogSysdeliz001>, ObservableValue<SdLogSysdeliz001>>) param -> new ReadOnlyObjectWrapper(StringUtils.toDateTimeFormat(param.getValue().getDataLog())));
                }).build(), /*Descrição*/
                FormTableColumn.create(cln -> {
                    cln.title("Usuário");
                    cln.width(130);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdLogSysdeliz001, SdLogSysdeliz001>, ObservableValue<SdLogSysdeliz001>>) param -> new ReadOnlyObjectWrapper(param.getValue().getUsuario()));
                }).build()
        );
    });

    private final FormTableView<SdSolicitacaoExp> tblSolicitacao = FormTableView.create(SdSolicitacaoExp.class, table -> {
        table.title("Solicitações");
        table.expanded();
        table.items.bind(solicitacoesBean);
        table.factoryRow(param -> new TableRow<SdSolicitacaoExp>() {
            @Override
            protected void updateItem(SdSolicitacaoExp item, boolean empty) {
                super.updateItem(item, empty);
                clear();

                if (item != null && !empty) {
                    getStyleClass().add("table-row-" + item.getStatus().getStyle());
                }
            }

            private void clear() {
                getStyleClass().removeIf(it -> it.contains("table-row-") && !it.equals("table-row-cell"));
            }
        });
        listStatus.forEach(it -> table.addIndice(table.indice(it.getStatus(), it.getStyle())));
        table.selectionModelItem((observable, oldValue, newValue) -> {
            if (newValue != null) carregaSolicitacao((SdSolicitacaoExp) newValue);
        });
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Ações");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(120);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdSolicitacaoExp, SdSolicitacaoExp>, ObservableValue<SdSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdSolicitacaoExp, SdSolicitacaoExp>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnImprimirSolicitacao = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.IMPRIMIR, ImageUtils.IconSize._16));
                                btn.tooltip("Imprimir Solicitação");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("primary");
                            });
                            final Button btnVizualizar = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.VISUALIZAR_PEDIDO, ImageUtils.IconSize._16));
                                btn.tooltip("Vizualizar solicitação");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("dark");
                            });

                            @Override
                            protected void updateItem(SdSolicitacaoExp item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    Button btnLiberarColeta = FormButton.create(btn -> {
                                        btn.icon(ImageUtils.getIcon(item.getStatus().equals(STATUS_LIBERADO_LEITURA) ? ImageUtils.Icon.CANCEL : ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._16));
                                        btn.tooltip(item.getStatus().equals(STATUS_LIBERADO_LEITURA) ? "Cancelar liberação para leitura" : "Liberar para Coleta");
                                        btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                        btn.addStyle("xs").addStyle(item.getStatus().equals(STATUS_LIBERADO_LEITURA) ? "danger" : "success");
                                    });

                                    boxButtonsRow.setAlignment(Pos.CENTER);

                                    btnLiberarColeta.setOnAction(evt -> {
                                        if (item.getStatus().equals(STATUS_LIBERADO_LEITURA)) {
                                            cancelarLiberacao(item);
                                        } else {
                                            liberarColeta(item);
                                        }
                                    });
                                    btnImprimirSolicitacao.setOnAction(evt -> {
                                        imprimirOrdem(item);
                                    });
                                    btnVizualizar.setOnAction(evt -> {
                                        table.tableview().getSelectionModel().select(item);
                                        tabs.getSelectionModel().select(1);
                                    });

                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnLiberarColeta, btnImprimirSolicitacao, btnVizualizar);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(120);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdSolicitacaoExp, SdSolicitacaoExp>, ObservableValue<SdSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId()));
                }).build(), /**/
                FormTableColumn.create(cln -> {
                    cln.title("Tipo");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdSolicitacaoExp, SdSolicitacaoExp>, ObservableValue<SdSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTipo().getTipo()));
                }).build(), /**/
                FormTableColumn.create(cln -> {
                    cln.title("Descrição Tipo");
                    cln.width(400);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdSolicitacaoExp, SdSolicitacaoExp>, ObservableValue<SdSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTipo().getDescricao()));
                }).build(), /**/
                FormTableColumn.create(cln -> {
                    cln.title("Dt Solicitação");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdSolicitacaoExp, SdSolicitacaoExp>, ObservableValue<SdSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(StringUtils.toShortDateFormat(param.getValue().getDtSolicitacao())));
                }).build(), /**/
                FormTableColumn.create(cln -> {
                    cln.title("Dt Envio Produto");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdSolicitacaoExp, SdSolicitacaoExp>, ObservableValue<SdSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(StringUtils.toShortDateFormat(param.getValue().getDtEnvioProduto())));
                }).build(), /**/
                FormTableColumn.create(cln -> {
                    cln.title("Dt Devolução");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdSolicitacaoExp, SdSolicitacaoExp>, ObservableValue<SdSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(StringUtils.toShortDateFormat(param.getValue().getDtDevolucao())));
                }).build(), /**/
                FormTableColumn.create(cln -> {
                    cln.title("Status");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdSolicitacaoExp, SdSolicitacaoExp>, ObservableValue<SdSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(param.getValue().getStatus().getStatus()));
                }).build(), /**/
                FormTableColumn.create(cln -> {
                    cln.title("Usuário");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdSolicitacaoExp, SdSolicitacaoExp>, ObservableValue<SdSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(param.getValue().getUsuario()));
                }).build() /**/
        );
    });

    private final FormTableView<SdItemSolicitacaoExp> tblPrdutos = FormTableView.create(SdItemSolicitacaoExp.class, table -> {
        table.title("Produtos");
        table.items.bind(itensSolicitacaoBean);
        table.expanded();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemSolicitacaoExp, SdItemSolicitacaoExp>, ObservableValue<SdItemSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(param.getValue().getProduto().getCodigo()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Cor");
                    cln.width(120);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemSolicitacaoExp, SdItemSolicitacaoExp>, ObservableValue<SdItemSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(param.getValue().getProduto().getCor()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Tam");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemSolicitacaoExp, SdItemSolicitacaoExp>, ObservableValue<SdItemSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(param.getValue().getProduto().getTam()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Qtde");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemSolicitacaoExp, SdItemSolicitacaoExp>, ObservableValue<SdItemSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                }).build() /*Código*/
        );
    });

    // </editor-fold>

    //<editor-fold desc="VBox">
    private final VBox tabListagem = (VBox) super.tabs.getTabs().get(0).getContent();
    private final VBox tabInfos = (VBox) super.tabs.getTabs().get(1).getContent();
    private final FormNavegation<SdSolicitacaoExp> navegation = FormNavegation.create(nav -> {
        nav.withNavegation(tblSolicitacao);
        nav.withActions(false, false, false);
        nav.withReturn(evt -> super.tabs.getSelectionModel().select(0));
        nav.selectedItem.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                solicitacaoEscolhida = (SdSolicitacaoExp) newValue;
                carregaCampos();
            }
        });
    });
    //</editor-fold>

    public AnaliseSolicitacaoPecasView() {
        super("Análise Solicitação de Peças", ImageUtils.getImage(ImageUtils.Icon.EXPEDICAO), new String[]{"Listagem", "Informações"});
        initListagem();
        initInformacoes();
        solicitacoesBean.set(FXCollections.observableArrayList((List<SdSolicitacaoExp>) new FluentDao().selectFrom(SdSolicitacaoExp.class).where(it -> it.isIn("status.codigo", new String[]{"2","3"})).resultList()));
        atualizaStatusSolicitacoes(solicitacoesBean);
        tblSolicitacao.refresh();
    }

    private void initListagem() {
        tabListagem.getChildren().add(FormBox.create(principal -> {
            principal.vertical();
            principal.expanded();
            principal.add(FormBox.create(boxHeader -> {
                boxHeader.horizontal();
                boxHeader.add(FormBox.create(boxTitledFilter -> {
                    boxTitledFilter.vertical();
                    boxTitledFilter.add(FormTitledPane.create(filter -> {
                        filter.filter();
                        filter.width(1200);
                        filter.add(FormBox.create(boxFilter -> {
                            boxFilter.vertical();
                            boxFilter.add(FormBox.create(linha1 -> {
                                linha1.horizontal();
                                linha1.add(filterSolicitacaoExp.build(), filterTipoSolicitacao.build(), filterUsuario.build(), filterStatusSolicitacao.build());
                            }));
                            boxFilter.add(FormBox.create(linha2 -> {
                                linha2.horizontal();
                                linha2.add(filterProduto.build(), filterColecao.build(), filterMarca.build(), filterDataSolicitacao.build());
                            }));
                        }));
                        filter.find.setOnAction(evt -> {
                            new RunAsyncWithOverlay(this).exec(task -> {
                                buscarSolicitacoes(
                                        filterSolicitacaoExp.objectValues.stream().map(SdSolicitacaoExp::getId).toArray(),
                                        filterTipoSolicitacao.value.get() == null ? new SdTipoSolicitacaoExp() : filterTipoSolicitacao.value.get(),
                                        filterDataSolicitacao.valueBegin.getValue(),
                                        filterDataSolicitacao.valueEnd.getValue(),
                                        filterProduto.objectValues.stream().map(Produto::getCodigo).toArray(),
                                        filterColecao.objectValues.stream().map(Colecao::getCodigo).toArray(),
                                        filterMarca.objectValues.stream().map(Marca::getCodigo).toArray(),
                                        filterUsuario.value.getValueSafe(),
                                        filterStatusSolicitacao.value.getValue() == null ? new SdStatusSolicitacaoExp() : filterStatusSolicitacao.value.getValue()
                                );
                                return ReturnAsync.OK.value;
                            }).addTaskEndNotification(taskReturn -> {
                                if (taskReturn.equals(ReturnAsync.OK.value)) {
                                    solicitacoesBean.set(FXCollections.observableArrayList(solicitacoesExp));
                                }
                            });
                        });
                        filter.clean.setOnAction(evt -> {
                            filterSolicitacaoExp.clear();
                            filterTipoSolicitacao.clear();
                            filterDataSolicitacao.clear();
                            filterProduto.clear();
                            filterColecao.clear();
                            filterMarca.clear();
                            filterUsuario.clear();
                        });
                    }));
                }));
            }));
            principal.add(FormBox.create(boxCenter -> {
                boxCenter.horizontal();
                boxCenter.expanded();
                boxCenter.add(tblSolicitacao.build());
            }));
        }));
    }

    private void initInformacoes() {
        tabInfos.getChildren().add(FormBox.create(root -> {
            root.vertical();
            root.expanded();
            root.add(navegation);
            root.add(FormBox.create(header -> {
                header.horizontal();
                header.expanded();
                header.add(FormBox.create(boxInfosSoli -> {
                    boxInfosSoli.vertical();
                    boxInfosSoli.add(FormBox.create(linha -> {
                        linha.horizontal();
                        linha.add(numeroSolicitacaoField.build(), fieldStatus.build(), fieldUsuario.build());
                    }));
                    boxInfosSoli.add(FormBox.create(linha -> {
                        linha.horizontal();
                        linha.add(tipoSolicitacaoField.build());
                    }));
                    boxInfosSoli.add(FormBox.create(linha -> {
                        linha.horizontal();
                        linha.add(qtdeProdutosField.build(), qtdeTotalItensField.build());
                    }));
                    boxInfosSoli.add(FormBox.create(linha -> {
                        linha.horizontal();
                        linha.add(fieldDataSolicitacao.build());
                    }));
                    boxInfosSoli.add(FormBox.create(linha -> {
                        linha.horizontal();
                        linha.add(fieldDataEnvioProduto.build());
                    }));
                    boxInfosSoli.add(FormBox.create(linha -> {
                        linha.horizontal();
                        linha.add(fieldDataDevolucao.build());
                    }));
                    boxInfosSoli.add(FormBox.create(linha -> {
                        linha.horizontal();
                        linha.add(fieldDescricao.build());
                    }));
                }));
                header.add(FormBox.create(boxFluxoPecas -> {
                    boxFluxoPecas.horizontal();
                    boxFluxoPecas.expanded();
                    boxFluxoPecas.add(tblPrdutos.build(), tblFluxoSolicitacao.build());
                }));
            }));
        }));
    }

    private void carregaSolicitacao(SdSolicitacaoExp newValue) {
        solicitacaoEscolhida = newValue;
        carregaCampos();
    }

    private void carregaCampos() {
        numeroSolicitacaoField.value.setValue(String.valueOf(solicitacaoEscolhida.getId()));
        fieldDataSolicitacao.value.setValue(StringUtils.toShortDateFormat(solicitacaoEscolhida.getDtSolicitacao()));
        fieldDataEnvioProduto.value.setValue(StringUtils.toShortDateFormat(solicitacaoEscolhida.getDtEnvioProduto()));
        fieldDataDevolucao.value.setValue(StringUtils.toShortDateFormat(solicitacaoEscolhida.getDtDevolucao()));
        fieldStatus.value.setValue(solicitacaoEscolhida.getStatus().getStatus());
        fieldUsuario.value.setValue(solicitacaoEscolhida.getUsuario());
        tipoSolicitacaoField.value.setValue(solicitacaoEscolhida.getTipo());
        fieldDescricao.value.setValue(solicitacaoEscolhida.getObservacao());
        qtdeProdutosField.value.setValue(String.valueOf(solicitacaoEscolhida.getItens().stream().map(it -> it.getProduto().getCodigo() + it.getProduto().getCor()).distinct().count()));
        qtdeTotalItensField.value.setValue(String.valueOf(solicitacaoEscolhida.getItens().stream().mapToInt(SdItemSolicitacaoExp::getQtde).sum()));
        itensSolicitacaoBean.set(FXCollections.observableArrayList(solicitacaoEscolhida.getItens()));
        fieldStatus.textField.getStyleClass().removeIf(it -> listStatus.stream().anyMatch(eb -> eb.getStyle().equals(it)));
        fieldStatus.addStyle(solicitacaoEscolhida.getStatus().getStyle());
        carregaFluxoSolicitacao();
    }

    private void carregaFluxoSolicitacao() {
        itensLogBean.set(FXCollections.observableArrayList((List<SdLogSysdeliz001>) new FluentDao().selectFrom(SdLogSysdeliz001.class).where(it -> it.equal("referencia", solicitacaoEscolhida.getId()).equal("tela", "Analise Solicitação de Peças")).orderBy("seq", OrderType.ASC).resultList()));
    }

    private void imprimirOrdem(SdSolicitacaoExp item) {
        if (item.getStatus().equals(STATUS_VISUALIZADA)) {
            liberarColeta(item);
        }
        SysLogger.addSysDelizLog("Analise Solicitação de Peças", TipoAcao.CADASTRAR, String.valueOf(item.getId()), "Solicitação " + item.getId() + " impressa");

        try {
            new ReportUtils().config().addReport(ReportUtils.ReportFile.SOLICITACAO_EXP,
                    new ReportUtils.ParameterReport("solicitacao", String.valueOf(item.getId()))).view().printWithDialog();
        } catch (JRException | SQLException | IOException e) {
            e.printStackTrace();
        }
        MessageBox.create(message -> {
            message.message("Etiqueta impressa com sucesso");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }

    private void liberarColeta(SdSolicitacaoExp item) {
        if (!item.getStatus().equals(STATUS_VISUALIZADA)) {
            SdSolicitacaoExp finalItem = item;
            MessageBox.create(message -> {
                message.message("Solicitação tem que estar no status de VIZUALIZADA para ser libarada para coleta, porém está no status de " + finalItem.getStatus().getStatus());
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }

        SysLogger.addSysDelizLog("Analise Solicitação de Peças", TipoAcao.EDITAR, String.valueOf(item.getId()), "Solicitação " + item.getId() + " liberada para leitura");
        MessageBox.create(message -> {
            message.message("Solicitação Liberada para coleta");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
        item.setStatus(STATUS_LIBERADO_LEITURA);
        item = new FluentDao().merge(item);
        tblSolicitacao.refresh();
    }

    private void cancelarLiberacao(SdSolicitacaoExp item) {
        SysLogger.addSysDelizLog("Analise Solicitação de Peças", TipoAcao.EDITAR, String.valueOf(item.getId()), "Solicitação " + item.getId() + " tem a liberação de leitura cancelada");
        MessageBox.create(message -> {
            message.message("Solicitação tem leitura cancelada");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
        item.setStatus(STATUS_VISUALIZADA);
        item = new FluentDao().merge(item);
        tblSolicitacao.refresh();
    }

}
