package sysdeliz2.views.expedicao;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Callback;
import org.apache.log4j.Logger;
import sysdeliz2.controllers.views.expedicao.ColetaExpedicaoController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.sysdeliz.*;
import sysdeliz2.models.view.VSdItensRemessa;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.StaticVersion;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.exceptions.FormValidationException;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.*;

import javax.persistence.EntityNotFoundException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class ColetaExpedicaoView extends ColetaExpedicaoController {

    // <editor-fold defaultstate="collapsed" desc="BeanController">
    private static final Logger logger = Logger.getLogger(ColetaExpedicaoView.class);
    private final BooleanProperty emColeta = new SimpleBooleanProperty(false);
    private final BooleanProperty hasCaixaAberta = new SimpleBooleanProperty(false);
    private final BooleanProperty isExcluirBarra = new SimpleBooleanProperty(false);
    private final BooleanProperty isApontamentoFalta = new SimpleBooleanProperty(false);
    private final ListProperty<SdItensCaixaRemessa> beanBarrasLidas = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final IntegerProperty totalRemessa = new SimpleIntegerProperty(0);
    private final IntegerProperty saldoRemessa = new SimpleIntegerProperty(0);
    private final IntegerProperty lidoRemessa = new SimpleIntegerProperty(0);
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Fields/Components">
    // dados da remesa
    private final FormFieldText remessaField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Remessa");
        field.editable(false);
        field.alignment(Pos.CENTER);
        field.width(150.0);
    });
    private final FormFieldText origemRemessaField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Origem");
        field.editable(false);
        field.alignment(Pos.CENTER);
        field.width(100.0);
    });
    private final FormFieldText clienteField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Cliente");
        field.editable(false);
        field.expanded();
    });
    private final FormFieldText totalPecasField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Total");
        field.addStyle("lg").addStyle("primary");
        field.editable(false);
        field.alignment(Pos.CENTER);
        field.value.bind(totalRemessa.asString());
        field.width(105.0);
        field.setPadding(-1);
    });
    private final FormFieldText saldoPecasField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Saldo");
        field.editable(false);
        field.alignment(Pos.CENTER);
        field.addStyle("lg").addStyle("warning");
        field.value.bind(saldoRemessa.asString());
        field.width(105.0);
        field.setPadding(-1);
    });
    private final FormFieldText lidoPecasField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Lido");
        field.editable(false);
        field.addStyle("lg").addStyle("success");
        field.value.bind(lidoRemessa.asString());
        field.alignment(Pos.CENTER);
        field.width(105.0);
        field.setPadding(-1);
    });
    private final FormFieldTextArea observacaoRemessaField = FormFieldTextArea.create(field -> {
        field.title("Observações");
        field.editable(false);
        field.height(60.0);
        field.width(150.0);
    });
    // dados caixa
    private final FormFieldText caixaField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label(ImageUtils.getIcon(ImageUtils.Icon.LEITURA_BARRA, ImageUtils.IconSize._24));
        field.addStyle("lg").addStyle("barcodeinput");
        field.toUpper();
        field.alignment(Pos.CENTER);
        field.editable.bind(emColeta.and(hasCaixaAberta.not()));
        field.width(170.0);
        field.keyReleased(event -> criarCaixa(event));
    });
    private final FormBox boxCaixasRemessa = FormBox.create(box -> {
        box.flutuante();
        box.expanded();
        box.verticalScroll();
    });
    // dados produto
    private final FormFieldText localField = FormFieldText.create(field -> {
        field.title("Local");
        field.addStyle("xl");
        field.editable(false);
        field.alignment(Pos.CENTER);
        field.width(190.0);
    });
    private final FormFieldText pisoField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Piso");
        field.addStyle("xl").addStyle("amber");
        field.editable(false);
        field.alignment(Pos.CENTER);
        field.width(80.0);
        field.fontBold();
        field.fontSize(55.0);
        field.setPadding(-1);
    });
    private final FormFieldText ruaField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Rua");
        field.addStyle("xl").addStyle("primary");
        field.editable(false);
        field.alignment(Pos.CENTER);
        field.width(80.0);
        field.fontBold();
        field.fontSize(50.0);
        field.setPadding(-1);
    });
    private final FormFieldText codigoField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Ref.");
        field.addStyle("lg");
        field.editable(false);
        field.alignment(Pos.CENTER);
        field.width(160.0);
        field.setPadding(-1);
        field.fontSize(26.0);
    });
    private final FormFieldText corField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Cor");
        field.alignment(Pos.CENTER);
        field.addStyle("lg");
        field.editable(false);
        field.width(110.0);
        field.setPadding(-1);
        field.fontSize(26.0);
    });
    private final FormFieldText descProdutoField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.editable(false);
    });
    private final FormFieldTextArea observacaoProdutoField = FormFieldTextArea.create(field -> {
        field.title("Observação");
        field.expanded();
        field.editable(false);
        field.height(54.0);
        field.width(50.0);
    });
    private final FormFieldText barraLeituraField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label(ImageUtils.getIcon(ImageUtils.Icon.LEITURA_BARRA, ImageUtils.IconSize._32));
        field.addStyle("xl").addStyle("barcodeinput");
        field.alignment(Pos.CENTER);
        field.expanded();
        field.editable.bind(hasCaixaAberta);
        field.keyReleased(event -> leituraBarra(event));
    });
    // buttons
    private final FormButton btnTipoBarra = FormButton.create(btnTipoBarra -> {
        btnTipoBarra.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._48));
        btnTipoBarra.addStyle("xl").addStyle("success");
        btnTipoBarra.disable.bind(hasCaixaAberta.not().or(isApontamentoFalta));
        btnTipoBarra.setAction(evt -> trocarTipoLeitura());
    });
    private final FormButton btnFecharCaixa = FormButton.create(btnFecharCaixa -> {
        //btnFecharCaixa.title("Fechar");
        btnFecharCaixa.icon(ImageUtils.getIcon(ImageUtils.Icon.FECHAR_CAIXA, ImageUtils.IconSize._32));
        btnFecharCaixa.addStyle("primary");
        btnFecharCaixa.disable.bind(hasCaixaAberta.not());
        btnFecharCaixa.setAction(event -> fecharCaixa());
    });
    private final FormButton btnTrocarCaixa = FormButton.create(btnFecharCaixa -> {
        //btnFecharCaixa.title("Trocar");
        btnFecharCaixa.icon(ImageUtils.getIcon(ImageUtils.Icon.TROCAR, ImageUtils.IconSize._32));
        btnFecharCaixa.addStyle("warning");
        btnFecharCaixa.disable.bind(hasCaixaAberta.not());
        btnFecharCaixa.setAction(event -> trocarCaixa());
    });
    private final FormButton btnFalta = FormButton.create(btnFalta -> {
        //btnFalta.title("Falta");
        btnFalta.icon(ImageUtils.getIcon(ImageUtils.Icon.PRODUTO_FALTANTE, ImageUtils.IconSize._48));
        btnFalta.addStyle("xl").addStyle("dark");
        btnFalta.disable.bind(hasCaixaAberta.not());
        btnFalta.setAction(event -> apontarFaltaProduto());
    });
    private final FormButton btnCarregarRemessas = FormButton.create(btnCarregar -> {
        btnCarregar.title("  RECARREGAR REMESSA      ");
        btnCarregar.icon(ImageUtils.getIcon(ImageUtils.Icon.ATUALIZAR_CARRINHO, 40.0));
        btnCarregar.addStyle("lg").addStyle("success");
        btnCarregar.setAlignment(Pos.CENTER);
        btnCarregar.setAction(evt -> {
            atualizarDadosTablet();
        });
    });
    private final FormButton btnAvisarReposicao = FormButton.create(btnReposicao -> {
        btnReposicao.title("REPOR");
        btnReposicao.icon(ImageUtils.getIcon(ImageUtils.Icon.AVISAR, 34.0));
        btnReposicao.addStyle("xl").addStyle("warning");
        btnReposicao.disable.bind(emColeta.not());
        btnReposicao.setAction(evt -> {
            /** * task: avisarReposicao(referenciaEmColeta) * return:  */
            AtomicReference<SQLException> excpAssync = new AtomicReference<>();
            new RunAsync().exec(task -> {
                try {
                    avisarReposicao(referenciaEmColeta, "A");
                } catch (SQLException e) {
                    e.printStackTrace();
                    excpAssync.set(e);
                    return ReturnAsync.EXCEPTION.value;
                }
                return ReturnAsync.OK.value;
            }).addTaskEndNotification(returnTask -> {
                if (returnTask.equals(ReturnAsync.OK.value)) {
                    MessageBox.create(message -> {
                        message.message("Aviso de reposição entregue!");
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.position(Pos.BOTTOM_RIGHT);
                        message.notification();
                    });
                } else {
                    ExceptionBox.build(message -> {
                        message.exception(excpAssync.get());
                        message.showAndWait();
                    });
                }
            });
        });
    });
    // table barras
    private final FormTableView<SdItensCaixaRemessa> tblBarraLidas = FormTableView.create(SdItensCaixaRemessa.class, table -> {
        table.title("Barras Lidas");
        table.expanded();
        table.fontSize(14);
        table.disable.set(true);
        table.items.bind(beanBarrasLidas);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Barra");
                    cln.width(280.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItensCaixaRemessa, SdItensCaixaRemessa>, ObservableValue<SdItensCaixaRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getBarra()));
                    cln.format(param -> {
                        return new TableCell<SdItensPedidoRemessa, String>() {
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(item);
                                    getStyleClass().add("lg");
                                }
                            }
                        };
                    });
                }).build() /*Barra*/,
                FormTableColumn.create(cln -> {
                    cln.title("Caixa");
                    cln.width(110.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItensCaixaRemessa, SdItensCaixaRemessa>, ObservableValue<SdItensCaixaRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCaixa().getNumero()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Caixa*/
        );
    });
    // proximo
    private final FormFieldText proximoPisoField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Piso");
        field.alignment(Pos.CENTER);
        field.addStyle("lg");
        field.addStyle("amber");
        field.editable(false);
        field.width(70.0);
        field.fontSize(30.0);
        field.setPadding(-1);
    });
    private final FormFieldText proximoRuaField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Rua");
        field.alignment(Pos.CENTER);
        field.addStyle("lg");
        field.addStyle("primary");
        field.editable(false);
        field.width(70.0);
        field.fontSize(30.0);
        field.setPadding(-1);
    });
    private final FormFieldText proximoPosicaoField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Posição");
        field.alignment(Pos.CENTER);
        field.addStyle("lg");
        field.editable(false);
        field.width(110.0);
        field.fontSize(24.0);
        field.setPadding(-1);
    });
    private final FormFieldText proximoCodigoField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Refer.");
        field.editable(false);
        field.addStyle("lg");
        field.alignment(Pos.CENTER);
        field.width(170.0);
        field.fontSize(24.0);
        field.setPadding(-1);
    });
    // grade
    private final FormBox boxGrades = FormBox.create(boxGrade -> {
        boxGrade.vertical();
        boxGrade.expanded();
        boxGrade.verticalScroll();
        //boxGrade.title("Grade");
    });
    // </editor-fold>

    public ColetaExpedicaoView() {
        super("Coleta de Remessa", ImageUtils.getImage(ImageUtils.Icon.COLETA), true);
        logger.info("init tela");
        JPAUtils.getEntityManager();

        coletor = getColetor(Globals.getNomeUsuario());
        logger.info("init colaborador");
        if (coletor == null) {
            MessageBox.create(message -> {
                message.message("Usuário não habilitado para coleta de produtos.");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showAndWait();
            });
            throw new FormValidationException("Coletor não existe!");
        } else {
            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.ENTRAR, String.valueOf(coletor.getCodigo()), "Coletor " + coletor.getNome() + " iniciou tela de coleta.");
            init();
        }
    }
    public void init() {
        super.box.getChildren().add(FormBox.create(content -> {
            content.horizontal();
            content.expanded();
            content.add(FormBox.create(left -> {
                left.vertical();
                left.add(FormBox.create(boxRemessaCliente -> {
                    boxRemessaCliente.vertical();
                    boxRemessaCliente.title("Dados da Remessa");
                    boxRemessaCliente.add(FormBox.create(boxDadosRemessa -> {
                        boxDadosRemessa.horizontal();
                        boxDadosRemessa.add(remessaField.build(),
                                origemRemessaField.build(),
                                FormButton.create(btnCarregarProdutos -> {
                            btnCarregarProdutos.title("PRODUTOS");
                            btnCarregarProdutos.icon(ImageUtils.getIcon(ImageUtils.Icon.PRODUTO, ImageUtils.IconSize._16));
                            btnCarregarProdutos.addStyle("warning");
                            btnCarregarProdutos.disable.bind(emColeta.not());
                            btnCarregarProdutos.setAction(event -> carregarProdutoRemessa());
                        }));
                    }));
                    boxRemessaCliente.add(FormBox.create(boxDadosCliente -> {
                        boxDadosCliente.horizontal();
                        boxDadosCliente.add(clienteField.build());
                    }));
                    boxRemessaCliente.add(btnCarregarRemessas);
                    boxRemessaCliente.add(observacaoRemessaField.build());
                    boxRemessaCliente.add(FormBox.create(boxDadosColeta -> {
                        boxDadosColeta.horizontal();
                        boxDadosColeta.add(totalPecasField.build(), lidoPecasField.build(), saldoPecasField.build());
                    }));
                }));
                left.add(FormBox.create(boxCaixas -> {
                    boxCaixas.vertical();
                    boxCaixas.expanded();
                    boxCaixas.title("Caixas da Remessa");
                    boxCaixas.add(boxCaixasRemessa);
                    boxCaixas.add(FormBox.create(boxAcoesCaixa -> {
                        boxAcoesCaixa.horizontal();
                        boxAcoesCaixa.alignment(Pos.CENTER_LEFT);
                        boxAcoesCaixa.add(caixaField.build());
                        boxAcoesCaixa.add(FormBox.create(boxFecharCaixa -> {
                            boxFecharCaixa.horizontal();
                            boxFecharCaixa.alignment(Pos.CENTER_LEFT);
                            boxFecharCaixa.add(btnFecharCaixa);
//                            ImageView barcodeFecharCaixa = new ImageView(ImageUtils.getImage(getClass().getResource("/images/barcodeAct0001.png").toExternalForm()));
//                            barcodeFecharCaixa.setFitHeight(40.0);
//                            barcodeFecharCaixa.setFitWidth(130.0);
//                            boxFecharCaixa.add(barcodeFecharCaixa);
                        }));
                        boxAcoesCaixa.add(FormBox.create(boxBarras -> {
                            boxBarras.horizontal();
                            boxBarras.alignment(Pos.CENTER_LEFT);
                            boxBarras.add(btnTrocarCaixa);
//                        ImageView barcodeFecharCaixa = new ImageView(ImageUtils.getImage(getClass().getResource("/images/barcodeAct0004.png").toExternalForm()));
//                        barcodeFecharCaixa.setFitHeight(40.0);
//                        barcodeFecharCaixa.setFitWidth(130.0);
//                        boxBarras.add(barcodeFecharCaixa);
                        }));
                    }));
                }));
                left.add(FormLabel.create(label -> {
                    label.value.set("Versão: " + StaticVersion.versaoSistema);
                }).build());
            }));
            content.add(FormBox.create(center -> {
                center.vertical();
                center.add(FormBox.create(boxProdutoColeta -> {
                    boxProdutoColeta.vertical();
                    boxProdutoColeta.expanded();
                    boxProdutoColeta.title("Dados do Produto");
                    boxProdutoColeta.add(FormBox.create(boxLocalProduto -> {
                        boxLocalProduto.horizontal();
                        boxLocalProduto.alignment(Pos.BOTTOM_LEFT);
                        boxLocalProduto.add(FormBox.create(boxDescricaoProduto -> {
                            boxDescricaoProduto.vertical();
                            boxDescricaoProduto.expanded();
                            boxDescricaoProduto.alignment(Pos.BOTTOM_LEFT);
                            boxDescricaoProduto.add(FormBox.create(box -> {
                                box.horizontal();
                                box.add(pisoField.build());
                                box.add(ruaField.build());
                                box.add(codigoField.build());
                                box.add(corField.build());
                            }));
//                                boxDescricaoProduto.add(descProdutoField.build());
                        }));
                    }));
                    boxProdutoColeta.add(boxGrades);
                    boxProdutoColeta.add(FormBox.create(boxProximoProduto -> {
                        boxProximoProduto.horizontal();
                        boxProximoProduto.title("Próxima Coleta");
                        boxProximoProduto.add(proximoPisoField.build());
                        boxProximoProduto.add(proximoRuaField.build());
                        boxProximoProduto.add(proximoPosicaoField.build());
                        boxProximoProduto.add(proximoCodigoField.build());
                    }));
                }));
            }));
            content.add(FormBox.create(right -> {
                right.vertical();
                right.expanded();
                right.add(FormBox.create(boxObsFalta -> {
                    boxObsFalta.horizontal();
                    boxObsFalta.alignment(Pos.BOTTOM_LEFT);
                    boxObsFalta.add(observacaoProdutoField.build());
                    boxObsFalta.add(btnFalta);
                }));
                right.add(FormBox.create(boxBarra -> {
                    boxBarra.horizontal();
                    boxBarra.alignment(Pos.BOTTOM_LEFT);
                    boxBarra.add(btnTipoBarra, barraLeituraField.build());
                }));
                right.add(FormBox.create(boxBarrasLidasProxProduto -> {
                    boxBarrasLidasProxProduto.horizontal();
                    boxBarrasLidasProxProduto.expanded();
                    boxBarrasLidasProxProduto.add(tblBarraLidas.build());
                }));
                right.add(FormBox.create(boxIndicadores -> {
                    boxIndicadores.vertical();
                    boxIndicadores.title("Indicadores do Coletor");
                    boxIndicadores.add(FormLabel.create(label -> {
                        label.value.set(coletor.toString());
                        label.addStyle("sm");
                    }).build());
                    boxIndicadores.add(FormBox.create(boxDadosColeta -> {
                        boxDadosColeta.flutuante();
                        boxDadosColeta.add(FormFieldText.create(field -> {
                            field.title("Remes.");
                            field.value.bind(qtdeTotalRemessa.asString());
                            field.alignment(Pos.CENTER);
                            field.editable(false);
                            field.width(60.0);
                            field.setPadding(-1);
                            field.addStyle("lg");
                        }).build());
                        boxDadosColeta.add(FormFieldText.create(field -> {
                            field.title("Peças");
                            field.value.bind(qtdeTotalPecas.asString());
                            field.alignment(Pos.CENTER);
                            field.editable(false);
                            field.width(70.0);
                            field.setPadding(-1);
                            field.addStyle("lg");
                        }).build());
                        boxDadosColeta.add(FormFieldText.create(field -> {
                            field.title("M. Pçs");
                            field.value.bind(medTotalPecas.asString());
                            field.alignment(Pos.CENTER);
                            field.editable(false);
                            field.width(60.0);
                            field.setPadding(-1);
                            field.addStyle("lg");
                        }).build());
                        boxDadosColeta.add(FormFieldText.create(field -> {
                            field.title("M. Coleta");
                            field.value.bind(medMinColeta.asString());
                            field.alignment(Pos.CENTER);
                            field.editable(false);
                            field.postLabel("min");
                            field.width(80.0);
                            field.setPadding(-1);
                            field.addStyle("lg");
                        }).build());
                        boxDadosColeta.add(FormFieldText.create(field -> {
                            field.title("Méd. Peças");
                            field.value.bind(medSegColetaPeca.asString());
                            field.alignment(Pos.CENTER);
                            field.editable(false);
                            field.postLabel("seg");
                            field.width(80.0);
                            field.setPadding(-1);
                            field.addStyle("lg");
                        }).build());
                    }));
                }));
                right.add(FormBox.create(boxRelogioVersao -> {
                    boxRelogioVersao.horizontal();
                    boxRelogioVersao.alignment(Pos.BOTTOM_LEFT);
                    boxRelogioVersao.add(btnAvisarReposicao);
                    boxRelogioVersao.add(FormBox.create(boxRelogio -> {
                        boxRelogio.horizontal();
                        boxRelogio.expanded();
                        boxRelogio.title("Relógio");
                        boxRelogio.alignment(Pos.CENTER);
                        boxRelogio.add(FormLabel.create(label -> {
                            label.value.set(StringUtils.toDateFormat(LocalDate.now()));
                            label.addStyle("lg");
                        }).build());
                        boxRelogio.add(FormClock.create(label -> {
                            label.addStyle("lg");
                        }).build());
                    }));
                }));
            }));
        }));
        /**
         * task: carregarRemessa() * return: exibirRemessa()
         */
        carregarRemessas();

        saldoRemessa.bind(totalRemessa.subtract(lidoRemessa));
    }

    private String verificaSituacaoRemessa() {
        List<SdItensPedidoRemessa> itensParaAnalise = new ArrayList();
        itensParaAnalise.addAll(referenciasParaColeta);
        itensParaAnalise.addAll(referenciasEmFalta);
        itensParaAnalise.forEach(SdItensPedidoRemessa::refresh);
        if (itensParaAnalise.stream().map(SdItensPedidoRemessa::getStatusitem).anyMatch(s -> s.equals("P"))) {
            return "P";
        } else if (itensParaAnalise.stream().map(SdItensPedidoRemessa::getStatusitem).anyMatch(s -> s.equals("F"))) {
            return "F";
        }
        return "C";
    }

    /**
     * Procedure para ação do botão de atualizar dados do tablet
     */
    private void atualizarDadosTablet() {
        SysLogger.addSysDelizLog("Coleta Expedição", TipoAcao.EDITAR, String.valueOf(coletor.getCodigo()), "Atualizando remessa no tablet.");
        if (remessaEmColeta == null) {
            SysLogger.addSysDelizLog("Coleta Expedição", TipoAcao.EDITAR, String.valueOf(coletor.getCodigo()), "Atualizando lista de itens para coleta a partir do botão atualizar sem nenhuma remessa carregada.");
            carregarRemessas();
        } else {
            SysLogger.addSysDelizLog("Coleta Expedição", TipoAcao.EDITAR, String.valueOf(coletor.getCodigo()), "Atualizando objetos dos itens da remessa " + remessaEmColeta.getRemessa() + " para atualização do tablet.");
            String situacaoRemessa = verificaSituacaoRemessa();
            if (situacaoRemessa.equals("P")) {
                MessageBox.create(message -> {
                    message.message("Existem referências pendentes para coleta nesta remessa.");
                    message.type(MessageBox.TypeMessageBox.ALERT);
                    message.showFullScreen();
                });
                return;
            } else if (situacaoRemessa.equals("F")) {
                MessageBox.create(message -> {
                    message.message("Existem peças para coleta marcadas como faltante nesta remessa, procure o atendimento para verificar o cancelamento das referências na remessa.");
                    message.type(MessageBox.TypeMessageBox.ALERT);
                    message.showFullScreen();
                });
                return;
            } else {
                SysLogger.addSysDelizLog("Coleta Expedição", TipoAcao.EDITAR, String.valueOf(coletor.getCodigo()), "Finalizando remessa " + remessaEmColeta.getRemessa() + " após ação de atualização da remessa no tablet.");
                finalizarColeta();
            }
        }

    }

    /**
     * Procedure com a carga das remessas disponíveis
     */
    private void carregarRemessas() {
        /**
         * task: carregarRemessa() * return: exibirRemessa()
         */
        AtomicReference<Exception> exc = new AtomicReference<>();
        new RunAsyncWithOverlay(this).exec(task -> {
            try {
                carregarRemessa();
                atualizaProducaoColaborador();
            } catch (EntityNotFoundException enf) {
                exc.set(enf);
                return ReturnAsync.NOT_FOUND.value;
            }
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                if (remessaEmColeta != null) {
                    lidoRemessa.set(remessaEmColeta.getPedidos().stream()
                            .mapToInt(pedido -> pedido.getItens().stream()
                                    .mapToInt(item -> item.getGrade().stream()
                                            .mapToInt(SdGradeItemPedRem::getQtdel).sum()).sum()).sum());
                    caixasRemessa.forEach(caixa -> beanBarrasLidas.addAll(caixa.getItens()));
                    if (remessaEmColeta.getPedidos().stream()
                            .anyMatch(pedido -> pedido.getItens().stream()
                                    .filter(item -> !item.getStatusitem().equals("X"))
                                    .anyMatch(item -> item.getGrade().stream()
                                            .filter(grade -> !grade.getStatusitem().equals("X"))
                                            .anyMatch(grade -> grade.getBarra28() == null)))) {
                        MessageBox.create(message -> {
                            message.message("Existem produtos nesta remessa com problemas de cadastro, " +
                                    "verifique se o produto está com a BARRA28 cadastrada no estoque.");
                            message.type(MessageBox.TypeMessageBox.ERROR);
                            message.showFullScreen();
                        });
                    }
                }
                exibirRemessa();
            } else if (taskReturn.equals(ReturnAsync.NOT_FOUND.value)) {
                MessageBox.create(message -> {
                    message.message(exc.get().getMessage());
                    message.type(MessageBox.TypeMessageBox.RISK);
                    message.showFullScreen();
                });
            }
        });
    }

    /**
     * regra com controle de remessa aberta para exibição para o coletor
     * indicador de em coleta controlado
     */
    private void exibirRemessa() {
        limparDadosCaixaRemessa();
        if (remessaEmColeta != null) {
            // verifica se cliente da remessa é o mesmo ainda
            if (!verificaClienteRemessa()) {
                if (remessaEmColeta.getPedidos().stream()
                        .mapToInt(pedido -> pedido.getItens().stream()
                                .mapToInt(item -> item.getGrade().stream()
                                        .mapToInt(SdGradeItemPedRem::getQtdel).sum()).sum()).sum() == 0) {
                    remessaEmColeta.setColetor(null);
                    remessaEmColeta.setDtIniColeta(null);
                    remessaEmColeta.setStatus(statusParaRevisao);

                    new FluentDao().merge(remessaEmColeta);
                    SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                            "Liberando remessa do coletor para troca de cliente no atendimento e cadastrando nova coleta.");

                    carregarRemessas();
                } else {
                    MessageBox.create(message -> {
                        message.message("Um dos pedidos desta remessa foi alterado o cliente, verifique com o atendimento para substituição do pedido.");
                        message.type(MessageBox.TypeMessageBox.ALERT);
                        message.showFullScreen();
                    });

                    exibirDadosRemessa();
                    return;
                }
            }

            // marca como o coletor está em coleta
            emColeta.set(true);
            // exibe os dados da remessa que está sendo coletada
            remessaEmColeta.refresh();
            exibirDadosRemessa();

            // exibição das caixas da remessa
            caixasRemessa.forEach(this::exibirDadosCaixaRemessa);
            if (caixaAberta != null) {
                // exibindo dados da caixa
                exibirDadosCaixaAberta();
                hasCaixaAberta.set(true);
            } else {
                // enviando focus para a caixa
                caixaField.requestFocus();
            }

            if (referenciasParaColeta.size() > 0) {
                referenciaEmColeta = referenciasParaColeta.get(0);
                carregarProximoProduto(referenciaEmColeta);
            } else if (referenciasEmFalta.size() > 0) {
                referenciasParaColeta.addAll(referenciasEmFalta);
                referenciasEmFalta.clear();
                exibirRemessa();
            } else {
                finalizarColeta();
            }
        } else if (coletor.isSegundoColetor()) {
            emColeta.set(true);
            hasCaixaAberta.set(false);
            caixaField.clear();
            caixaField.requestFocus();
        }
    }

    /**
     * Leitura do código de barras principal
     *
     * @param event
     */
    private void leituraBarra(KeyEvent event) {
        if (event.getCode().equals(KeyCode.ENTER)) {
            String barra = barraLeituraField.value.get();

            // teste para verificar se é uma barra de ação
            if (barra.startsWith("act")) {
                if (barra.equals("act0002")) {
                    apontarFaltaProduto();
                    return;
                } else if (barra.equals("act0003")) {
                    trocarTipoLeitura();
                    return;
                } else if (barra.equals("act0001")) {
                    barraLeituraField.clear();
                    fecharCaixa();
                    return;
                } else if (barra.equals("act0004")) {
                    barraLeituraField.clear();
                    trocarCaixa();
                    return;
                }
            }

            if (barra.length() != 16 && barra.length() != 18) {
                MessageBox.create(message -> {
                    message.message("Barra (" + barra + ") inválida, leia novamente!");
                    message.type(MessageBox.TypeMessageBox.ERROR);
                    message.showFullScreen();
                });

                SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                        "Tentativa de incluir barra " + barra + " do produto na caixa " + caixaAberta.getNumero() + " porém barra não é válidade para a qtde caracteres!");

                barraLeituraField.clear();
                barraLeituraField.requestFocus();
                return;
            }

            if (isExcluirBarra.not().get()) {
                incluirBarra(barra);
            } else {
                removerBarra(barra);
            }

            barraLeituraField.clear();
            barraLeituraField.requestFocus();
        }
    }
    private void trocarTipoLeitura() {
        if (isApontamentoFalta.not().get()) {

            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                    "Trocando tipo de leitura de barra para " + (isExcluirBarra.not().get() ? "ENTRADA DE BARRAS" : "EXCLUSÃO DE BARRAS"));
            isExcluirBarra.set(isExcluirBarra.not().get());
            btnTipoBarra.removeStyle(isExcluirBarra.get() ? "success" : "danger");
            btnTipoBarra.addStyle(isExcluirBarra.get() ? "danger" : "success");
            btnTipoBarra.icon(ImageUtils.getIcon(isExcluirBarra.get() ? ImageUtils.Icon.CANCEL : ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._48));
            barraLeituraField.removeStyle(isExcluirBarra.get() ? "barcodeinput" : "barcodeoutput");
            barraLeituraField.addStyle(!isExcluirBarra.get() ? "barcodeinput" : "barcodeoutput");
            barraLeituraField.clear();
            barraLeituraField.requestFocus();
        }
    }
    private boolean incluirBarra(String barra) {
        try {
            // leitura de codigo de barra de produto
            String barra28 = barra.substring(0, 6);

            // <editor-fold defaultstate="collapsed" desc="Controle se o produto tem que validar o pedido na BARRA">
            if (referenciaEmColeta.getId().getCodigo().getSdProduto().isValidaPedido()) {
                String pedidoBarra = barra.substring(barra.length() - 10, barra.length());
                if (!pedidoBarra.equals(referenciaEmColeta.getId().getNumero())) {
                    MessageBox.create(message -> {
                        message.message("Esta barra não pertence ao pedido em leitura.");
                        message.type(MessageBox.TypeMessageBox.ERROR);
                        message.showFullScreen();
                    });
                    SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                            "Tentativa de incluir barra " + barra + " do produto na caixa " + caixaAberta.getNumero() + " porém barra não pertence ao pedido em coleta");

                    barraLeituraField.clear();
                    barraLeituraField.requestFocus();
                    return false;
                }
            }
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Controle se a barra é de uma referência da remessa">
            AtomicBoolean isDaRemessa = new AtomicBoolean(false);
            remessaEmColeta.getPedidos().forEach(pedido -> {
                pedido.getItens().stream().filter(item -> !item.getStatusitem().equals("X")).forEach(item -> {
                    item.getGrade().stream().filter(grade -> !grade.getStatusitem().equals("X")).forEach(grade -> {
                        if (grade.getBarra28().equals(barra28))
                            isDaRemessa.set(true);
                    });
                });
            });
            if (!isDaRemessa.get()) {
                MessageBox.create(message -> {
                    message.message("A referência lida não pertence a esta remessa.");
                    message.type(MessageBox.TypeMessageBox.ERROR);
                    message.showFullScreen();
                });
                SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                        "Tentativa de incluir barra " + barra + " do produto na caixa " + caixaAberta.getNumero() + " porém barra não pertence a remessa em coleta");

                barraLeituraField.clear();
                barraLeituraField.requestFocus();
                return false;
            }
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Controle se a barra é da referência em coleta">
            if (referenciaEmColeta.getGrade().stream().noneMatch(grade -> grade.getBarra28().equals(barra28))) {
                MessageBox.create(message -> {
                    message.message("Barra lida não pertence a grade da referência em coleta.");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showFullScreen();
                });

                SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                        "Tentativa de incluir barra " + barra + " do produto " + referenciaEmColeta.getId().getCodigo().getCodigo() + " na cor " + referenciaEmColeta.getId().getCor()
                                + " na caixa " + caixaAberta.getNumero() + " porém barra não pertence a referência em coleta");

                barraLeituraField.clear();
                barraLeituraField.requestFocus();
                return false;
            }
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Controle de barra em caixa">
            AtomicReference<SdItensCaixaRemessa> barraEmCaixa = new AtomicReference<>(new FluentDao().selectFrom(SdItensCaixaRemessa.class)
                    .where(eb -> eb
                            .equal("id.barra", barra)
                            .equal("status", "E"))
                    .singleResult());
            if (barraEmCaixa.get() != null || caixaAberta.getItens().stream().anyMatch(barraCaixa -> barraCaixa.getId().getBarra().equals(barra))) {
                caixaAberta.getItens().stream().filter(barraCaixa -> barraCaixa.getId().getBarra().equals(barra)).findAny().ifPresent(barraCaixa -> barraEmCaixa.set(barraCaixa));
                MessageBox.create(message -> {
                    message.message("Barra lida já consta em uma caixa. Caixa: " + barraEmCaixa.get().getId().getCaixa().getNumero());
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showFullScreen();
                });

                SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                        "Tentativa de incluir barra " + barra + " do produto " + referenciaEmColeta.getId().getCodigo().getCodigo() + " na cor " + referenciaEmColeta.getId().getCor()
                                + " na caixa " + caixaAberta.getNumero() + " porém já consta na caixa " + barraEmCaixa.get().getId().getCaixa().getNumero());
                barraLeituraField.clear();
                barraLeituraField.requestFocus();
                return false;
            }
            // </editor-fold>

            // adicionando referência na caixa
            AtomicReference<SdGradeItemPedRem> gradeBarra = new AtomicReference<>(null);
            referenciaEmColeta.getGrade().stream()
                    .filter(grade -> grade.getBarra28().equals(barra28))
                    .findAny()
                    .ifPresent(grade -> gradeBarra.set(grade));
            referenciaEmColeta.getGrade().forEach(SdGradeItemPedRem::refresh);
            if (gradeBarra.get() == null) {
                MessageBox.create(message -> {
                    message.message("Ocorreu um erro no coletor. Informe o TI o problema. Problema: não foi possível obter a grade da lista de grade da referência.");
                    message.type(MessageBox.TypeMessageBox.SIREN);
                    message.showFullScreen();
                });
                return false;
            }
            // <editor-fold defaultstate="collapsed" desc="Controle se todos os tamanhos da barra foram lidos">
            if (remessaEmColeta.getPedidos().stream()
                    .mapToLong(pedido -> pedido.getItens().stream()
                            .mapToLong(item -> item.getGrade().stream()
                                    .filter(grade -> grade.getId().getNumero().equals(gradeBarra.get().getId().getNumero())
                                            && grade.getId().getCodigo().equals(gradeBarra.get().getId().getCodigo())
                                            && grade.getId().getCor().equals(gradeBarra.get().getId().getCor())
                                            && grade.getId().getTam().equals(gradeBarra.get().getId().getTam()))
                                    .mapToLong(grade -> grade.getQtdep())
                                    .sum())
                            .sum())
                    .sum() == 0) {
                MessageBox.create(message -> {
                    message.message("Já foi lida toda a quantidade do tamanho " + gradeBarra.get().getId().getTam());
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showFullScreen();
                });

                SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                        "Tentativa de incluir barra " + barra + " do produto " + referenciaEmColeta.getId().getCodigo().getCodigo() + " na cor " + referenciaEmColeta.getId().getCor()
                                + " na caixa " + caixaAberta.getNumero() + " porém grade da barra já completa");
                barraLeituraField.clear();
                barraLeituraField.requestFocus();
                return false;
            }
            // </editor-fold>

            int qtdeParaSalvar = 1;
            // verifica se é um produto de KIT
            if (referenciaEmColeta.getTipo().equals("KIT"))
                qtdeParaSalvar = referenciaEmColeta.getId().getCodigo().getMinimo();

            // atualizando os contadores de coleta da grade
            gradeBarra.get().setQtdel(gradeBarra.get().getQtdel() + 1);
            gradeBarra.get().setQtdep(gradeBarra.get().getQtdep() - 1);
            gradeBarra.get().setStatusitem(gradeBarra.get().getQtdep() == 0 ? "C" : gradeBarra.get().getStatusitem());
            gradeBarra.get().setColetor(gradeBarra.get().getQtdep() == 0 ? coletor : null);
            gradeBarra.get().setDhColeta(LocalDateTime.now());
            new FluentDao().merge(gradeBarra.get());

            // incluindo peça na caixa
            SdItensCaixaRemessa itemCaixa = new SdItensCaixaRemessa(caixaAberta,
                    barra,
                    referenciaEmColeta.getId().getCodigo(),
                    gradeBarra.get().getId().getCor(),
                    gradeBarra.get().getId().getTam(),
                    "E",
                    gradeBarra.get().getId().getNumero());
            new FluentDao().merge(itemCaixa);
            caixaAberta.getItens().add(itemCaixa);
            caixaAberta.setQtde(caixaAberta.getQtde() + 1);
            caixaAberta.setPeso(caixaAberta.getPeso().add(referenciaEmColeta.getId().getCodigo().getPeso()));
            beanBarrasLidas.add(0, itemCaixa);
            new FluentDao().merge(caixaAberta);

            // expedição do item no Excia. Update qtde na PA_ITEN. Merge do item na PEDIDO3 e Inclusão da PA_MOV
            expedirExcia(referenciaEmColeta, gradeBarra.get());
            lidoRemessa.set(lidoRemessa.add(1).get());

            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                    "Incluindo barra " + barra + " do produto " + referenciaEmColeta.getId().getCodigo().getCodigo() + " na cor " + referenciaEmColeta.getId().getCor() + " na caixa " + caixaAberta.getNumero());

            // verificação da leitura de toda a grade da referência e get da próxima referência
            referenciaEmColeta.refresh();
//            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
//                    "Recarregado dados da referencia em coleta");
            if (referenciaEmColeta.getGrade().stream().noneMatch(grade -> grade.getQtdep() > 0)) {
                referenciasColetadas.add(referenciaEmColeta); // adiciona referencia em coleta para lista de coletados
                referenciasParaColeta.remove(referenciaEmColeta); // remove referencia em coleta da lista para coletar
                referenciaEmColeta.setStatusitem("C");
                new FluentDao().merge(referenciaEmColeta);
//                SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
//                        "Atualizando dados da referencia em coleta");
                getProximoProduto();
            }
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            ExceptionBox.build(box -> {
                box.exception(ex);
                box.message("Aconteceu um erro durante a inclusão do produto na caixa. " +
                        "Por favor, verificar com o setor do TI para solução do problema.");
                box.showAndWait();
            });
            return false;
        }
    }
    private boolean removerBarra(String barra) {
        try {
            // leitura de codigo de barra de produto
            String barra28 = barra.substring(0, 6);
            AtomicReference<SdCaixaRemessa> caixaBarra = new AtomicReference<>(null);
            AtomicReference<SdItensCaixaRemessa> barraCaixaBarra = new AtomicReference<>(null);

            beanBarrasLidas.clear();

            // <editor-fold defaultstate="collapsed" desc="Controle se a barra é de uma referência da remessa">
            AtomicBoolean isDaRemessa = new AtomicBoolean(false);
            remessaEmColeta.getPedidos().forEach(pedido -> {
                pedido.getItens().forEach(item -> {
                    item.getGrade().forEach(grade -> {
                        if (grade.getBarra28().equals(barra28))
                            isDaRemessa.set(true);
                    });
                });
            });
            if (!isDaRemessa.get()) {
                MessageBox.create(message -> {
                    message.message("A referência lida não pertence a esta remessa.");
                    message.type(MessageBox.TypeMessageBox.ERROR);
                    message.showFullScreen();
                });

                SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                        "Tentativa de exclusão barra " + barra + " porém barra não pertence a remessa em coleta");
                barraLeituraField.clear();
                barraLeituraField.requestFocus();
                return false;
            }
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Quando falta: Controle se a barra é da referencia em coleta">
            if (isApontamentoFalta.get()) {
                if (referenciaEmColeta.getGrade().stream().noneMatch(grade -> grade.getBarra28().equals(barra28))) {
                    MessageBox.create(message -> {
                        message.message("Barra lida não pertence a grade da referência em coleta.");
                        message.type(MessageBox.TypeMessageBox.WARNING);
                        message.showFullScreen();
                    });

                    SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                            "Tentativa de exclusão barra " + barra + " porém barra não pertence ao produto " + referenciaEmColeta.getId().getCodigo().getCodigo() + " na cor " + referenciaEmColeta.getId().getCor());
                    barraLeituraField.clear();
                    barraLeituraField.requestFocus();
                    return false;
                }
            }
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Controle de barra em caixa">
            AtomicBoolean isEmCaixa = new AtomicBoolean(false);
            caixasRemessa.forEach(caixa -> caixa.getItens().forEach(barraCaixa -> {
                if (barraCaixa.getId().getBarra().equals(barra)) {
                    isEmCaixa.set(true);
                    caixaBarra.set(caixa);
                    barraCaixaBarra.set(barraCaixa);
                }
            }));
            if (!isEmCaixa.get()) {
                MessageBox.create(message -> {
                    message.message("Barra lida não consta em uma caixa desta remessa");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showFullScreen();
                });

                SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                        "Tentativa de exclusão barra " + barra + " porém barra consta em uma caixa");
                barraLeituraField.clear();
                barraLeituraField.requestFocus();
                return false;
            }
            // </editor-fold>

            // removendo barra da caixa
            AtomicReference<SdItensPedidoRemessa> itemBarra = new AtomicReference<>(null);
            AtomicReference<SdGradeItemPedRem> gradeBarra = new AtomicReference<>(null);
            referenciasColetadas.forEach(item -> {
                item.getGrade().stream().filter(grade -> grade.getBarra28().equals(barra28)
                                && grade.getId().getNumero().equals(barraCaixaBarra.get().getPedido()))
                        .findAny().ifPresent(grade -> {
                            itemBarra.set(item);
                            gradeBarra.set(grade);
                        });
            });
            if (gradeBarra.get() == null || itemBarra.get() == null) {
                referenciaEmColeta.getGrade().stream().filter(grade -> grade.getBarra28().equals(barra28)).findAny().ifPresent(grade -> {
                    itemBarra.set(referenciaEmColeta);
                    gradeBarra.set(grade);
                });
            }
            if (gradeBarra.get() == null || itemBarra.get() == null) {
                MessageBox.create(message -> {
                    message.message("Ocorreu um erro no coletor. Informe o TI o problema. Problema: não foi possível obter a grade ou item da lista de referências coletadas.");
                    message.type(MessageBox.TypeMessageBox.SIREN);
                    message.showFullScreen();
                });
                return false;
            }
            if (gradeBarra.get().getPiso() != coletor.getPisoColeta()) {
                MessageBox.create(message -> {
                    message.message("A referência desta barra foi coletada em outro piso e não pode ser removida.");
                    message.type(MessageBox.TypeMessageBox.ERROR);
                    message.showFullScreen();
                });

                SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                        "Tentativa de exclusão barra " + barra + " porém barra não pertence ao piso do coletor");
                barraLeituraField.clear();
                barraLeituraField.requestFocus();
                return false;
            }


            int qtdeParaSalvar = 1;
            // verifica se é um produto de KIT
            if (itemBarra.get().getTipo().equals("KIT"))
                qtdeParaSalvar = referenciaEmColeta.getId().getCodigo().getMinimo();

            // atualizando status do item
            itemBarra.get().setStatusitem(itemBarra.get().getStatusitem().equals("C") ? "P" : itemBarra.get().getStatusitem());
            new FluentDao().merge(itemBarra.get());

            // atualizando os contadores de coleta da grade
            if (isApontamentoFalta.not().get()) {
                itemBarra.get().setStatusitem("P");
                if (!referenciasParaColeta.contains(itemBarra.get()))
                    referenciasParaColeta.add(itemBarra.get());
            } else {
                itemBarra.get().setStatusitem("F");
            }
            gradeBarra.get().setQtdel(gradeBarra.get().getQtdel() - 1);
            gradeBarra.get().setQtdep(gradeBarra.get().getQtdep() + 1);
            gradeBarra.get().setStatusitem(isApontamentoFalta.not().get() ? "F" : "P");
            new FluentDao().merge(gradeBarra.get());

            // removendo peça na caixa
            new FluentDao().delete(barraCaixaBarra.get());
            caixaBarra.get().refresh();
            caixaBarra.get().setQtde(caixaBarra.get().getQtde() - 1);
            caixaBarra.get().setPeso(caixaBarra.get().getPeso().subtract(barraCaixaBarra.get().getCodigo().getPeso()));
            new FluentDao().merge(caixaBarra.get());

            // expedição do item no Excia. Update qtde na PA_ITEN. Merge do item na PEDIDO3 e Inclusão da PA_MOV
            estornoExcia(itemBarra.get(), gradeBarra.get(), caixaBarra.get());
            lidoRemessa.set(lidoRemessa.subtract(1).get());

            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                    "Exclusão barra " + barra + " do produto " + itemBarra.get().getId().getCodigo().getCodigo() + " na cor " + itemBarra.get().getId().getCor() + " na caixa " + caixaBarra.get().getNumero());

            // verificação da leitura de todas as referencias em falta para retirada da caixa
            referenciaEmColeta.refresh();
            if (isApontamentoFalta.get()) {
                if (referenciaEmColeta.getGrade().stream().mapToInt(SdGradeItemPedRem::getQtdel).sum() == 0) {
                    referenciasFaltantesParaRemover.remove(referenciaEmColeta);
                    if (referenciasFaltantesParaRemover.size() > 0) {
                        referenciaEmColeta = referenciasFaltantesParaRemover.get(0);
                        carregarProximoProduto(referenciaEmColeta);
                    } else {
                        isApontamentoFalta.set(false);
                        trocarTipoLeitura();
                        // caso ainda existam referências para leitura
                        getProximoProduto();
                    }
                }
            }
            return true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            ExceptionBox.build(box -> {
                box.exception(ex);
                box.showAndWait();
            });
            return false;
        }
    }

    /**
     * Finalizar coleta da remessa
     */
    private void finalizarColeta() {
        remessaEmColeta.refresh();
        if (!verificaSituacaoRemessa().equals("C")) {
            MessageBox.create(message -> {
                message.message("Ainda existem peças pendentes na remessa para coleta.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showFullScreen();
            });
            return;
        }

        if (!remessaEmColeta.getStatus().getCodigo().equals("Z")) {
            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                    "Finalizando coleta de remessa.");

            // verifica se cliente da remessa é o mesmo ainda
            if (!verificaClienteRemessa()) {
                MessageBox.create(message -> {
                    message.message("Um dos pedidos desta remessa foi alterado o cliente, verifique com o atendimento para substituição do pedido.");
                    message.type(MessageBox.TypeMessageBox.ALERT);
                    message.showFullScreen();
                });

                return;
            }

            // fechando a caixa caso esta esteja aberta
            String caixasFinalizadas = caixasRemessa.stream().map(caixa -> String.valueOf(caixa.getNumero())).collect(Collectors.joining(", "));
            if (caixaAberta != null)
                fecharCaixa();
            caixasRemessa.clear();
            beanBarrasLidas.clear();

            referenciaEmColeta = null;
            referenciasEmFalta.clear();
            referenciasParaColeta.clear();
            referenciasColetadas.clear();
            referenciasFaltantesParaRemover.clear();

            if (coletor.getPisoColeta() == 0) {
                remessaEmColeta.setStatus(statusColetaFinalizada);
                remessaEmColeta.setDtFimColeta(LocalDateTime.now());
            }
            remessaEmColeta.setPisoFinalizado(true);
            new FluentDao().merge(remessaEmColeta);
            MessageBox.create(message -> {
                message.message("Coleta da remessa " + remessaEmColeta.getRemessa() + " finalizada! Encaminhe a(s) caixa(s): "
                        + caixasFinalizadas + " para área de "
                        + (coletor.getPisoColeta() != 0 ? "transferência para piso 0." : "fechamento."));
                message.type(MessageBox.TypeMessageBox.SUCCESS);
                message.showFullScreen();
            });
        }
        limparDadosRemessa();
        limparDadosCaixaRemessa();
        limparDadosCaixaAberta();
        limparDadosProduto();
        emColeta.set(false);
        hasCaixaAberta.set(false);
        isExcluirBarra.set(false);
        /**
         * task: carregarRemessa() * return: exibirRemessa()
         */
        carregarRemessas();
    }

    /**
     * Get do proximo produto para ser lido
     */
    private void getProximoProduto() {
        // caso ainda existam referências para leitura
        referenciasParaColeta.sort(Comparator.comparing(SdItensPedidoRemessa::getLocal));
        referenciaEmColeta = null;

//        SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
//                "Carregando próximo produto para coleta");
        while (referenciaEmColeta == null && referenciasParaColeta.size() > 0) {
            referenciaEmColeta = referenciasParaColeta.get(0); // get da próxima referência
            referenciaEmColeta.refresh();
            if (referenciaEmColeta.getStatusitem().equals("X")) {
                referenciasParaColeta.remove(referenciaEmColeta);
                referenciaEmColeta = null;
            }
        }

        if (referenciaEmColeta != null) {

//            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
//                    "Carregando dados do próximo produto para coleta");
            carregarProximoProduto(referenciaEmColeta);
        } else {

//            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
//                    "Verificando produtos em faltas para carga");

            // verifica as referências enviadas para falta foram resolvidas
            referenciasEmFalta.forEach(SdItensPedidoRemessa::refresh);
            if (referenciasEmFalta.size() > 0) {
                referenciasParaColeta.addAll(referenciasEmFalta.stream().collect(Collectors.toList()));
                referenciasEmFalta.clear();

//                SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
//                        "Carregando produto marcado como falta para coleta");
                getProximoProduto();
            } else {

//                SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
//                        "Último produto já lido, indo para finalização da coleta");
                remessaEmColeta.getPedidos().forEach(pedido -> pedido.getItens().forEach(SdItensPedidoRemessa::refresh));
                prepararItensParaColeta();
                if (referenciasParaColeta.size() > 0 || referenciasEmFalta.size() > 0) {

//                    SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
//                            "Verificado alteração nos pedidos e enviado para carregar próximo produto");
                    getProximoProduto();
                }

//                SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
//                        "Iniciando processo de finalização da remessa.");
                finalizarColeta();
            }
        }

    }

    /**
     * Carregando dados do próximo item para coleta
     *
     * @param item
     */
    private void carregarProximoProduto(SdItensPedidoRemessa item) {
        item.refresh();

        // exibe os dados do produto em coleta
        String[] local = item.getLocal().split("-");
        pisoField.value.set(local[0]);
        ruaField.value.set(local[1]);

        localField.value.set(item.getLocal());
        codigoField.value.set(item.getId().getCodigo().getCodigo());
        corField.value.set(item.getId().getCor());
        descProdutoField.value.set(item.getId().getCodigo().getDescricao());
        localField.removeStyle("danger").removeStyle("info");
        codigoField.removeStyle("danger").removeStyle("info");
        corField.removeStyle("danger").removeStyle("info");

        // pinta os componentes conforme o status do item (VERMELHO - FALTANTE / AZUL - PENDENTE COLETA)
        if (isApontamentoFalta.get()) {
            localField.addStyle("danger");
            codigoField.addStyle("danger");
            corField.addStyle("danger");
        } else {
            localField.addStyle("info");
            codigoField.addStyle("info");
            corField.addStyle("info");
        }

        String obsExpedicao = item.getId().getCodigo().getSdProduto().getExpedicao() != null
                ? item.getId().getCodigo().getSdProduto().getExpedicao() + " / "
                : "";
        String obsUnicoCaixa = item.getId().getCodigo().getSdProduto().getUnicocaixa()
                ? "ESTE PRODUTO DEVE SER ÚNICO NA CAIXA. / "
                : "";
        String obsAtendimento = item.getObservacao() != null
                ? item.getObservacao()
                : "";
        String obsProduto = (item.getStatusitem().equals("F")
                ? "*** RE-LEITURA DE ITEM MARCADO COMO FALTANTE ***"
                : (item.getStatusitem().equals("X") ? "*** ITEM CANCELADO DA REMESSA ***" : ""))
                + obsExpedicao + obsUnicoCaixa + obsAtendimento;
        observacaoProdutoField.value.set(obsProduto);

        boxGrades.clear();
        item.getGrade().sort(Comparator.comparingInt(SdGradeItemPedRem::getOrdem));
        for (SdGradeItemPedRem sdGradeItemPedRem : item.getGrade()) {
            if (!sdGradeItemPedRem.getStatusitem().equals("X")) {
                String[] splitLocal = sdGradeItemPedRem.getLocal().split("-");
                String ladoGrade = splitLocal.length > 3 ? splitLocal[3] + " - " : "";
                String localGrade = splitLocal[2];
                if (splitLocal.length > 4)
                    localGrade = splitLocal[2] + " | " + splitLocal[4];
                String finalLocalGrade = localGrade;
                boxGrades.add(FormBox.create(boxDadosGrade -> {
                    boxDadosGrade.horizontal();
                    boxDadosGrade.add(FormFieldText.create(field -> {
                        field.withoutTitle();
                        field.editable(false);
                        field.alignment(Pos.CENTER);
                        field.width(220.0);
                        field.addStyle("lg")
                                .addStyle(ladoGrade.length() == 0
                                        ? "secundary"
                                        : ladoGrade.contains("D") ? "success" : "amber");
                        field.value.set(ladoGrade + finalLocalGrade);
                        field.textField.setPadding(new Insets(-1));
                        field.fontSize(25.0);
                    }).build());
                    boxDadosGrade.add(FormFieldText.create(field -> {
                        field.withoutTitle();
                        field.editable(false);
                        field.alignment(Pos.CENTER);
                        field.addStyle("lg")
                                .addStyle(sdGradeItemPedRem.getStatusitem().equals("X")
                                        ? "warning"
                                        : (isApontamentoFalta.get() || sdGradeItemPedRem.getBarra28() == null)
                                        ? "danger" : "info");
                        field.value.set(sdGradeItemPedRem.getId().getTam());
                        field.textField.setPadding(new Insets(-1));
                        field.fontSize(35.0);
                        field.width(90.0);
                    }).build());
                    boxDadosGrade.add(FormBadges.create(bg -> {
                        bg.badgedNode(FormFieldText.create(field -> {
                            field.withoutTitle();
                            field.editable(false);
                            field.alignment(Pos.CENTER);
                            field.addStyle("lg");
                            if (sdGradeItemPedRem.getQtdep() == 0)
                                field.addStyle("success");
                            field.value.bind(sdGradeItemPedRem.qtdepProperty().asString());
                            field.textField.setPadding(new Insets(-1));
                            sdGradeItemPedRem.qtdepProperty().addListener((observable, oldValue, newValue) -> {
                                if (newValue != null) {
                                    field.removeStyle("success");
                                    if (newValue.intValue() == 0)
                                        field.addStyle("success");
                                }
                            });
                            field.width(85.0);
                            field.fontSize(35.0);
                            field.fontBold();
                        }).build());
                        bg.text(lb -> {
                            lb.value.bind(sdGradeItemPedRem.qtdelProperty().asString());
                            lb.addStyle("warning");
                            lb.sizeText(14);
                            lb.boldText();
                            lb.borderRadius(50);
                        });
                        bg.visible.bind(sdGradeItemPedRem.qtdelProperty().greaterThan(0));
                        bg.position(Pos.TOP_RIGHT, -4.0);
                    }));
                }));
            }
        }

        if (isApontamentoFalta.not().get()) {
            if (item.getGrade().stream().anyMatch(grade -> grade.getBarra28() == null)) {
                MessageBox.create(message -> {
                    message.message("Este produto consta com uma grade sem a BARRA28 cadastrada, favor verifique essa informação com o setor de TI.");
                    message.type(MessageBox.TypeMessageBox.ERROR);
                    message.showFullScreen();
                });
            }
        }

        SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                "Carregando próximo produto " + referenciaEmColeta.getId().getCodigo().getCodigo() + " na cor " + referenciaEmColeta.getId().getCor() + " para coleta");

        // exibe os dados do próximo produto caso tenha
        if (referenciasParaColeta.size() >= 2) {
            String[] localProx = referenciasParaColeta.get(1).getLocal().split("-");
            proximoPisoField.value.set(localProx[0]);
            proximoRuaField.value.set(localProx[1]);
            proximoPosicaoField.value.set(localProx[2]);
            proximoCodigoField.value.set(referenciasParaColeta.get(1).getId().getCodigo().getCodigo());
        } else if (referenciasEmFalta.size() > 0) {
            String[] localProx = referenciasEmFalta.get(0).getLocal().split("-");
            proximoPisoField.value.set(localProx[0]);
            proximoRuaField.value.set(localProx[1]);
            proximoPosicaoField.value.set(localProx[2]);
            proximoCodigoField.value.set(referenciasEmFalta.get(0).getId().getCodigo().getCodigo());
        } else {
            proximoPisoField.value.set("-");
            proximoRuaField.value.set("-");
            proximoPosicaoField.value.set("-");
            proximoCodigoField.value.set("-");
        }
    }
    private void limparDadosProduto() {
        localField.clear();
        codigoField.clear();
        corField.clear();
        descProdutoField.clear();
        observacaoProdutoField.clear();
        localField.removeStyle("danger").removeStyle("primary");
        codigoField.removeStyle("danger").removeStyle("primary");
        corField.removeStyle("danger").removeStyle("primary");
        boxGrades.clear();
    }

    /**
     * Rotina para apontar falta da referência em coleta
     */
    private void apontarFaltaProduto() {
        try {
            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                    "Apontando falta do produto " + referenciaEmColeta.getId().getCodigo().getCodigo() + " na cor " + referenciaEmColeta.getId().getCor());
            faltaProduto();
            if (referenciasFaltantesParaRemover.size() > 0) {
                MessageBox.create(message -> {
                    message.message("O tablet está entrando em modo de remoção de peças devido o apontamento de falta já ter peças lidas. " +
                            "Remova as peças lidas até que o tablet entre em modo de inclusão de pelas, com o botão (+) em verde.");
                    message.type(MessageBox.TypeMessageBox.ALERT);
                    message.showFullScreen();
                });
                if (isExcluirBarra.not().get())
                    trocarTipoLeitura();
                isApontamentoFalta.set(true);
                referenciaEmColeta = referenciasFaltantesParaRemover.get(0);
                carregarProximoProduto(referenciaEmColeta);
            } else {
                // caso ainda existam referências para leitura
                getProximoProduto();
            }
            barraLeituraField.clear();
            barraLeituraField.requestFocus();
        } catch (SQLException e) {
            e.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(e);
                message.showAndWait();
            });
        }
    }

    /**
     * Criar, Abrir e Fechar Caixa
     */
    private void criarCaixa(KeyEvent event) {
        if (event.getCode().equals(KeyCode.ENTER)) {
            if (caixaField.value.get().startsWith("CX") && caixaField.value.get().length() == 8) {
                if (remessaEmColeta != null) {
                    // verificação de confirmação de remessa atribuida para o coletor
                    remessaEmColeta.refresh();
                    if (remessaEmColeta.getColetor().getCodigo() != coletor.getCodigo() && !remessaEmColeta.isPisoFinalizado()) {
                        MessageBox.create(message -> {
                            message.message("Essa remessa foi atribuída para outro coletor: " + remessaEmColeta.getColetor().toString() + ". Carregando nova remessa...");
                            message.type(MessageBox.TypeMessageBox.ERROR);
                            message.showFullScreen();
                        });

                        SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.EDITAR, String.valueOf(coletor.getCodigo()),
                                "Carregando nova remessa para o coletor por atribuição de remessa simultânea. Remessa: " + remessaEmColeta.getRemessa());

                        limparDadosProduto();
                        limparDadosCaixaAberta();
                        limparDadosCaixaRemessa();
                        limparDadosRemessa();
                        carregarRemessas();
                    }
                    caixaField.value.set(caixaField.value.get().replace("CX", ""));
                    // verificação de caixa existente
                    SdCaixaRemessa validacaoCaixa = varificaCaixaExistente(caixaField.value.get());
                    if (validacaoCaixa != null) {
                        validacaoCaixa.refresh();
                        if (validacaoCaixa.getRemessa().getRemessa().compareTo(remessaEmColeta.getRemessa()) != 0
                                && coletor.isSegundoColetor() && caixasRemessa.size() == 0) {
                            SdRemessaCliente remessaColetaDupla = validacaoCaixa.getRemessa();

                            if (remessaColetaDupla.getStatus().getCodigo().equals("S")) {
                                List<SdCaixaRemessa> caixasDaRemessaColetaDupla = getCaixasRemessa(remessaColetaDupla.getRemessa());
                                MessageBox.create(message -> {
                                    message.message("Você está abrindo a remessa " + remessaColetaDupla.getRemessa()
                                            + " do piso " + remessaColetaDupla.getPiso()
                                            + ", caixas da remessa: " + caixasDaRemessaColetaDupla.stream()
                                            .map(caixa -> caixa.getNumero().toString()).distinct().collect(Collectors.joining(", "))
                                            + " para conclusão e envio para fechamento.");
                                    message.type(MessageBox.TypeMessageBox.IMPORTANT);
                                    message.showFullScreen();
                                });
                                SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaColetaDupla.getRemessa()),
                                        "Abrindo remessa coletada no piso 1 para finalização.");

                                clearRemessaAlocaSemCaixa();
                                if (!remessaColetaDupla.isColetaDupla() && remessaColetaDupla.isPisoFinalizado()) {
                                    remessaColetaDupla.setColetor(coletor);
                                    remessaColetaDupla.setPisoFinalizado(false);
                                    new FluentDao().merge(remessaColetaDupla);
                                    carregarRemessa();
                                    finalizarColeta();
                                } else {
                                    remessaColetaDupla.setColetor(coletor);
                                    remessaColetaDupla.setPisoFinalizado(false);
                                    new FluentDao().merge(remessaColetaDupla);
                                    caixaField.clear();
                                    carregarRemessas();
                                }
                            } else {
                                MessageBox.create(message -> {
                                    message.message("A caixa " + validacaoCaixa.getNumero() + " está vincula a remessa " + remessaColetaDupla.getRemessa() + " coletada no piso 1 e já finalizada.\n" +
                                            "Esta caixa deve estar na área de fechamento de caixas no piso 0.");
                                    message.type(MessageBox.TypeMessageBox.ERROR);
                                    message.showFullScreen();
                                });

                                caixaField.clear();
                                caixaField.requestFocus();
                            }
                        } else if (validacaoCaixa.getRemessa().getRemessa().compareTo(remessaEmColeta.getRemessa()) != 0) {
                            MessageBox.create(message -> {
                                message.message("Esse número de caixa já consta em outra remessa. Verifique o código de barras lido e faça a substituição. Remessa alocada: " + validacaoCaixa.getRemessa().getRemessa() +
                                        " Coletor: " + (validacaoCaixa.getRemessa().getColetor() == null ? "COLETA MANUAL" : validacaoCaixa.getRemessa().getColetor().getNome()));
                                message.type(MessageBox.TypeMessageBox.WARNING);
                                message.showFullScreen();
                            });
                            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                                    "Tentativa de inclusão de uma caixa já existente em outra remessa. Barra lida: " + caixaField.value.get());
                            caixaField.clear();
                            caixaField.requestFocus();
                        } else if (validacaoCaixa.getFechada()) {
                            MessageBox.create(message -> {
                                message.message("Esse número de caixa já consta nesta remessa, porém a caixa se encontra como fechada. Você pode abrir essa caixa se desejar.");
                                message.type(MessageBox.TypeMessageBox.IMPORTANT);
                                message.showFullScreen();
                            });
                            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                                    "Tentativa de inclusão de uma caixa já existente na remessa e marcada como fechada. Barra lida: " + caixaField.value.get());
                            caixaField.clear();
                            caixaField.requestFocus();
                        } else {
                            caixaAberta = validacaoCaixa;
                            limparDadosCaixaAberta();
                            exibirDadosCaixaAberta();
                            hasCaixaAberta.set(true);
                        }
                    } else {
                        ultimaCaixaAberta = caixaAberta;
                        incluirCaixa(new SdCaixaRemessa(Integer.parseInt(caixaField.value.get()), remessaEmColeta));
                        limparDadosCaixaAberta();
                        exibirDadosCaixaAberta();
                        limparDadosCaixaRemessa();
                        caixasRemessa.forEach(this::exibirDadosCaixaRemessa);
                        hasCaixaAberta.set(true);

                        SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                                "Incluída a caixa " + caixaAberta.getNumero() + " na remessa. Barra lida: " + caixaField.value.get());
                    }
                } else {
                    if (!coletor.isSegundoColetor()) {
                        MessageBox.create(message -> {
                            message.message("Você precisa estar cadastrado como coletor duplo para leitura de caixas sem remessa.");
                            message.type(MessageBox.TypeMessageBox.WARNING);
                            message.showFullScreenMobile();
                        });
                        caixaField.clear();
                        caixaField.requestFocus();
                        return;
                    }
                    caixaField.value.set(caixaField.value.get().replace("CX", ""));
                    SdCaixaRemessa validacaoCaixa = varificaCaixaExistente(caixaField.value.get());
                    if (validacaoCaixa != null) {
                        validacaoCaixa.refresh();
                        if (validacaoCaixa.getRemessa().isColetaDupla() || (coletor.getPisoColeta() == 0 && validacaoCaixa.getRemessa().getPiso() == 1)) {
                            SdRemessaCliente remessaColetaDupla = validacaoCaixa.getRemessa();

                            if (remessaColetaDupla.getStatus().getCodigo().equals("S")) {
                                List<SdCaixaRemessa> caixasDaRemessaColetaDupla = getCaixasRemessa(remessaColetaDupla.getRemessa());
                                MessageBox.create(message -> {
                                    message.message("Você está abrindo a remessa " + remessaColetaDupla.getRemessa()
                                            + " do piso " + remessaColetaDupla.getPiso()
                                            + ", caixas da remessa: " + caixasDaRemessaColetaDupla.stream()
                                            .map(caixa -> caixa.getNumero().toString()).distinct().collect(Collectors.joining(", "))
                                            + " para conclusão e envio para fechamento.");
                                    message.type(MessageBox.TypeMessageBox.IMPORTANT);
                                    message.showFullScreen();
                                });
                                SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaColetaDupla.getRemessa()),
                                        "Abrindo remessa coletada no piso 1 para finalização.");

                                clearRemessaAlocaSemCaixa();
                                if (!remessaColetaDupla.isColetaDupla() && remessaColetaDupla.isPisoFinalizado()) {
                                    remessaColetaDupla.setColetor(coletor);
                                    remessaColetaDupla.setPisoFinalizado(false);
                                    new FluentDao().merge(remessaColetaDupla);
                                    carregarRemessa();
                                    finalizarColeta();
                                } else {
                                    remessaColetaDupla.setColetor(coletor);
                                    remessaColetaDupla.setPisoFinalizado(false);
                                    new FluentDao().merge(remessaColetaDupla);
                                    caixaField.clear();
                                    carregarRemessas();
                                }
                            } else if (remessaColetaDupla.getStatus().getCodigo().equals("T")) {
                                MessageBox.create(message -> {
                                    message.message("A caixa " + validacaoCaixa.getNumero() + " está vincula a remessa " + remessaColetaDupla.getRemessa() + " coletada no piso 1 e já finalizada.\n" +
                                            "Esta caixa deve estar na área de fechamento de caixas no piso 0.");
                                    message.type(MessageBox.TypeMessageBox.ERROR);
                                    message.showFullScreen();
                                });

                                caixaField.clear();
                                caixaField.requestFocus();
                            } else {
                                MessageBox.create(message -> {
                                    message.message("A remessa " + remessaColetaDupla.getRemessa() + " coletada está com o status "+remessaColetaDupla.getStatus().getCodigo()+".\n" +
                                            "É necessário a verificação com o responsável pelas remessas.");
                                    message.type(MessageBox.TypeMessageBox.ERROR);
                                    message.showFullScreen();
                                });

                                caixaField.clear();
                                caixaField.requestFocus();
                            }
                        } else {
                            MessageBox.create(message -> {
                                message.message("Caixa " + caixaField.value.get() + " não pertence a uma remessa com coleta dupla. Remessa da caixa: " + validacaoCaixa.getRemessa().getRemessa());
                                message.type(MessageBox.TypeMessageBox.ALERT);
                                message.showFullScreenMobile();
                            });
                            caixaField.clear();
                            caixaField.requestFocus();
                        }
                    } else {
                        MessageBox.create(message -> {
                            message.message("Caixa " + caixaField.value.get() + " não encontrada em uma remessa.");
                            message.type(MessageBox.TypeMessageBox.ALERT);
                            message.showFullScreenMobile();
                        });
                        caixaField.clear();
                        caixaField.requestFocus();
                    }
                }
            } else {
                MessageBox.create(message -> {
                    message.message("O código de barras lido não é válido para a caixa, verifique a etiqueta.");
                    message.type(MessageBox.TypeMessageBox.ERROR);
                    message.showFullScreen();
                });

                SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                        "Tentativa de inclusão de uma caixa com código de barras inválido. Barra lida: " + caixaField.value.get());
                caixaField.clear();
                caixaField.requestFocus();
            }
        }
    }
    private void abrirCaixa(SdCaixaRemessa caixa) {
        new Fragment().show(fragment -> {
            fragment.title("LEITURA DO CÓDIGO DE BARRAS DA CAIXA");
            fragment.size(400.0, 100.0);

            final FormFieldText fieldBarcodeCaixa = FormFieldText.create(field -> {
                field.withoutTitle();
                field.expanded();
                field.toUpper();
                field.label(ImageUtils.getIcon(ImageUtils.Icon.LEITURA_BARRA, ImageUtils.IconSize._24));
                field.addStyle("lg");
                field.alignment(Pos.CENTER);
                field.keyReleased(event -> {
                    if (event.getCode().equals(KeyCode.ENTER))
                        if (field.value.get().startsWith("CX") && field.value.get().length() == 8) {
                            field.value.set(field.value.get().replace("CX", ""));
                        } else {
                            MessageBox.create(message -> {
                                message.message("O código de barras lido não é válido para a caixa, verifique a etiqueta.");
                                message.type(MessageBox.TypeMessageBox.ERROR);
                                message.showFullScreen();
                            });

                            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.MOVIMENTAR, String.valueOf(remessaEmColeta.getRemessa()),
                                    "Tentativa de confirmação de uma caixa com código de barra inválido. Barra lida: " + field.value.get());
                            field.clear();
                            field.requestFocus();
                        }
                });
            });
            fieldBarcodeCaixa.requestFocus();
            fragment.box.getChildren().add(fieldBarcodeCaixa.build());
            fragment.buttonsBox.getChildren().add(FormButton.create(btn -> {
                btn.title("EXCLUIR");
                btn.addStyle("lg").addStyle("warning");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EXCLUIR_CAIXA, ImageUtils.IconSize._32));
                btn.setAction(evt -> {
                    if (caixa.getNumero().compareTo(Integer.parseInt(fieldBarcodeCaixa.value.get().replace("CX", ""))) == 0) {
                        // verificação de caixa existente
                        if (caixa.getItens().size() == 0) {
                            new FluentDao().delete(caixa);

                            caixasRemessa.remove(caixa);
                            limparDadosCaixaRemessa();
                            caixasRemessa.forEach(this::exibirDadosCaixaRemessa);

                            if (caixaAberta.getNumero().compareTo(caixa.getNumero()) == 0) {
                                caixaAberta = null;
                                hasCaixaAberta.set(false);
                                caixaField.clear();
                                caixaField.requestFocus();
                            }

                            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.EXCLUIR, String.valueOf(remessaEmColeta.getRemessa()),
                                    "Excluindo caixa vazia da remessa. Barra lida: " + fieldBarcodeCaixa.value.get());
                        } else {
                            MessageBox.create(message -> {
                                message.message("Só é permitido excluir uma caixa vazia, você deve primeiro excluir os itens da caixa para sua exclusão.");
                                message.type(MessageBox.TypeMessageBox.ERROR);
                                message.showFullScreen();
                            });

                            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.EXCLUIR, String.valueOf(remessaEmColeta.getRemessa()),
                                    "Tentativa de exclusão de uma caixa com itens. Barra caixa: " + caixa.getNumero() + " Barra lida: " + fieldBarcodeCaixa.value.get());
                        }
                        fragment.close();
                    } else {
                        MessageBox.create(message -> {
                            message.message("O código lido não confere com a caixa que está sendo excluida, verifique o número da caixa e leia novamente.");
                            message.type(MessageBox.TypeMessageBox.ERROR);
                            message.showFullScreen();
                        });

                        SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.EXCLUIR, String.valueOf(remessaEmColeta.getRemessa()),
                                "Tentativa de exclusão de uma caixa com o número diferente da caixa. Barra caixa: " + caixa.getNumero() + " Barra lida: " + fieldBarcodeCaixa.value.get());
                        fieldBarcodeCaixa.clear();
                        fieldBarcodeCaixa.requestFocus();
                    }
                });
            }));
            fragment.buttonsBox.getChildren().add(FormButton.create(btn -> {
                btn.title("ABRIR");
                btn.addStyle("lg").addStyle("success");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CAIXA_ABERTA, ImageUtils.IconSize._32));
                btn.setAction(evt -> {
                    if (caixa.getNumero().compareTo(Integer.parseInt(fieldBarcodeCaixa.value.get())) == 0) {
                        // verificação de caixa existente
                        if (caixaAberta != null)
                            if (!fecharCaixa()) {
                                fragment.close();
                                return;
                            }

                        caixaAberta = caixa;
                        caixaAberta.setFechada(false);
                        hasCaixaAberta.set(true);
                        new FluentDao().merge(caixaAberta);
                        limparDadosCaixaRemessa();
                        caixasRemessa.forEach(this::exibirDadosCaixaRemessa);
                        exibirDadosCaixaAberta();

                        SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                                "Abrindo caixa para inclusão de peças. Barra lida: " + fieldBarcodeCaixa.value.get());
                        fragment.close();
                    } else {
                        MessageBox.create(message -> {
                            message.message("O código lido não confere com a caixa que está sendo aberta, verifique o número da caixa e leia novamente.");
                            message.type(MessageBox.TypeMessageBox.ERROR);
                            message.showFullScreen();
                        });

                        SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                                "Tentativa de abertura de uma caixa com o número diferente da caixa. Barra caixa: " + caixa.getNumero() + " Barra lida: " + fieldBarcodeCaixa.value.get());
                        fieldBarcodeCaixa.clear();
                        fieldBarcodeCaixa.requestFocus();
                    }
                });
            }));
        });
        if (caixaAberta == null) {
            hasCaixaAberta.set(false);
            caixaField.clear();
            caixaField.requestFocus();
        }
    }
    private void trocarCaixa() {
        new Fragment().show(fragment -> {
            fragment.title("SUBSTITUIÇÃO DE CAIXAS");
            fragment.size(300.0, 100.0);

            final FormFieldText fieldBarcodeCaixa = FormFieldText.create(field -> {
                field.withoutTitle();
                field.expanded();
                field.toUpper();
                field.label(ImageUtils.getIcon(ImageUtils.Icon.LEITURA_BARRA, ImageUtils.IconSize._24));
                field.addStyle("lg");
                field.alignment(Pos.CENTER);
            });
            fieldBarcodeCaixa.requestFocus();
            fragment.box.getChildren().add(fieldBarcodeCaixa.build());
            fragment.buttonsBox.getChildren().add(FormButton.create(btn -> {
                btn.title("TROCAR");
                btn.addStyle("lg").addStyle("success");
                btn.defaultButton(true);
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.TROCAR, ImageUtils.IconSize._32));
                btn.setAction(evt -> {
                    if (fieldBarcodeCaixa.value.get().startsWith("CX") && fieldBarcodeCaixa.value.get().length() == 8) {
                        fieldBarcodeCaixa.value.set(fieldBarcodeCaixa.value.get().replace("CX", ""));
                        try {
                            // verificação de caixa existente
                            SdCaixaRemessa validacaoCaixa = varificaCaixaExistente(fieldBarcodeCaixa.value.get());
                            if (validacaoCaixa != null) {
                                validacaoCaixa.refresh();
                                if (validacaoCaixa.getRemessa().getRemessa().compareTo(remessaEmColeta.getRemessa()) != 0) {
                                    MessageBox.create(message -> {
                                        message.message("Esse número de caixa já consta em outra remessa. Verifique o código de barras lido e faça a substituição. Remessa alocada: " + validacaoCaixa.getRemessa().getRemessa() +
                                                " Coletor: " + validacaoCaixa.getRemessa().getColetor().getNome());
                                        message.type(MessageBox.TypeMessageBox.WARNING);
                                        message.showFullScreen();
                                    });
                                    SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                                            "Tentativa de troca de caixa por uma caixa já existente em outra remessa. Barra lida: " + caixaField.value.get());
                                    caixaField.clear();
                                    caixaField.requestFocus();
                                } else if (validacaoCaixa.getFechada()) {
                                    MessageBox.create(message -> {
                                        message.message("Esse número de caixa já consta nesta remessa, porém a caixa se encontra como fechada. Você pode abrir essa caixa se desejar.");
                                        message.type(MessageBox.TypeMessageBox.IMPORTANT);
                                        message.showFullScreen();
                                    });
                                    SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                                            "Tentativa de troca de caixa por uma caixa já existente na remessa e marcada como fechada. Barra lida: " + caixaField.value.get());
                                    caixaField.clear();
                                    caixaField.requestFocus();
                                }
                            } else {
                                SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                                        "Trocando a caixa " + caixaAberta.getNumero() + " para a barra " + fieldBarcodeCaixa.value.get());

                                caixaAberta = substituirCaixa(fieldBarcodeCaixa.value.get());
                                limparDadosCaixaRemessa();
                                caixasRemessa.forEach(this::exibirDadosCaixaRemessa);
                                fragment.close();
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                            ExceptionBox.build(message -> {
                                message.exception(e);
                                message.showAndWait();
                            });
                        }
                    } else {
                        MessageBox.create(message -> {
                            message.message("O código de barras lido não é válido para a caixa, verifique a etiqueta.");
                            message.type(MessageBox.TypeMessageBox.ERROR);
                            message.showFullScreen();
                        });

                        SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                                "Tentativa de troca de caixa com código de barras inválido. Barra lida: " + fieldBarcodeCaixa.value.get());
                        fieldBarcodeCaixa.clear();
                        fieldBarcodeCaixa.requestFocus();
                    }
                });
            }));
        });
        if (caixaAberta == null) {
            hasCaixaAberta.set(false);
            caixaField.clear();
            caixaField.requestFocus();
        } else {
            exibirDadosCaixaAberta();
            limparDadosCaixaRemessa();
            caixasRemessa.forEach(this::exibirDadosCaixaRemessa);
        }
    }
    private boolean fecharCaixa() {
        if (caixaAberta.getItens().size() == 0) {
            MessageBox.create(message -> {
                message.message("Você não pode fechar a caixa pois ela não contém nenhum item dentro dela. Você pode utilizar a opção de trocar caixa caso não queira mais utilizar essa.");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showFullScreen();
            });

            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                    "Tentativa de fechar a caixa " + caixaAberta.getNumero() + " sem itens na caixa.");
            return false;
        }


        SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                "Fechando a caixa " + caixaAberta.getNumero() + " com " + caixaAberta.getItens().size() + " itens.");
        caixaAberta.setFechada(true);
        new FluentDao().merge(caixaAberta);
        hasCaixaAberta.set(false);
        limparDadosCaixaAberta();
        caixaAberta = null;
        limparDadosCaixaRemessa();
        caixasRemessa.forEach(this::exibirDadosCaixaRemessa);
        caixaField.clear();
        caixaField.requestFocus();
        return true;
    }

    /**
     * Carrega os produtos da remessa e exibe por:
     * - Acumulado por GrupoModelagem
     * - Acumulado por LInha
     * - Acumulado por Família
     */
    private void carregarProdutoRemessa() {
        AtomicReference<List<VSdItensRemessa>> itensRemessa = new AtomicReference<>();
        new RunAsyncWithOverlay(this).exec(task -> {
            itensRemessa.set(getItensRemessa(remessaEmColeta.getRemessa()));
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                new Fragment().show(fragment -> {
                    fragment.title("Produtos da Remessa");
                    fragment.size(800.0, 500.0);
                    ObservableList<ResumoRemessa> resumoModelagem = FXCollections.observableArrayList();
                    ObservableList<ResumoRemessa> resumoLinha = FXCollections.observableArrayList();
                    ObservableList<ResumoRemessa> resumoFamilia = FXCollections.observableArrayList();
                    itensRemessa.get().stream().collect(Collectors.groupingBy(VSdItensRemessa::getGrupomodelagem, Collectors.summingInt(VSdItensRemessa::getQtde))).forEach((s, i) -> resumoModelagem.add(new ResumoRemessa(s, i)));
                    itensRemessa.get().stream().collect(Collectors.groupingBy(VSdItensRemessa::getLinha, Collectors.summingInt(VSdItensRemessa::getQtde))).forEach((s, i) -> resumoLinha.add(new ResumoRemessa(s, i)));
                    itensRemessa.get().stream().collect(Collectors.groupingBy(VSdItensRemessa::getFamilia, Collectors.summingInt(VSdItensRemessa::getQtde))).forEach((s, i) -> resumoFamilia.add(new ResumoRemessa(s, i)));
                    final FormTableView<ResumoRemessa> tblModelagens = FormTableView.create(ResumoRemessa.class, table -> {
                        table.title("MODELAGENS");
                        table.items.set(resumoModelagem);
                        table.columns(
                                FormTableColumn.create(cln -> {
                                    cln.title("Modelagem");
                                    cln.width(150.0);
                                    cln.value((Callback<TableColumn.CellDataFeatures<ResumoRemessa, ResumoRemessa>, ObservableValue<ResumoRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().descricao));
                                }).build() /*Modelagem*/,
                                FormTableColumn.create(cln -> {
                                    cln.title("Qtde");
                                    cln.width(60);
                                    cln.value((Callback<TableColumn.CellDataFeatures<ResumoRemessa, ResumoRemessa>, ObservableValue<ResumoRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().qtde));
                                    cln.alignment(Pos.CENTER);
                                }).build() /*Qtde*/
                        );
                    });
                    final FormTableView<ResumoRemessa> tblLinhas = FormTableView.create(ResumoRemessa.class, table -> {
                        table.title("LINHAS");
                        table.items.set(resumoLinha);
                        table.columns(
                                FormTableColumn.create(cln -> {
                                    cln.title("Linha");
                                    cln.width(150.0);
                                    cln.value((Callback<TableColumn.CellDataFeatures<ResumoRemessa, ResumoRemessa>, ObservableValue<ResumoRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().descricao));
                                }).build() /*Linha*/,
                                FormTableColumn.create(cln -> {
                                    cln.title("Qtde");
                                    cln.width(60);
                                    cln.value((Callback<TableColumn.CellDataFeatures<ResumoRemessa, ResumoRemessa>, ObservableValue<ResumoRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().qtde));
                                    cln.alignment(Pos.CENTER);
                                }).build() /*Qtde*/
                        );
                    });
                    final FormTableView<ResumoRemessa> tblFamilia = FormTableView.create(ResumoRemessa.class, table -> {
                        table.title("FAMÍLIAS");
                        table.items.set(resumoFamilia);
                        table.columns(
                                FormTableColumn.create(cln -> {
                                    cln.title("Família");
                                    cln.width(170.0);
                                    cln.value((Callback<TableColumn.CellDataFeatures<ResumoRemessa, ResumoRemessa>, ObservableValue<ResumoRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().descricao));
                                }).build() /*Linha*/,
                                FormTableColumn.create(cln -> {
                                    cln.title("Qtde");
                                    cln.width(60);
                                    cln.value((Callback<TableColumn.CellDataFeatures<ResumoRemessa, ResumoRemessa>, ObservableValue<ResumoRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().qtde));
                                    cln.alignment(Pos.CENTER);
                                }).build() /*Qtde*/
                        );
                    });
                    List<SdItensPedidoRemessa> itensPedRemessa = new ArrayList<>();
                    remessaEmColeta.getPedidos().forEach(pedido -> itensPedRemessa.addAll(pedido.getItens()));
                    final FormTableView<SdItensPedidoRemessa> tblPecasRemessa = FormTableView.create(SdItensPedidoRemessa.class, table -> {
                        table.title("Peças");
                        table.items.set(FXCollections.observableList(itensPedRemessa));
                        table.columns(
                                FormTableColumn.create(cln -> {
                                    cln.title("Código");
                                    cln.width(90);
                                    cln.value((Callback<TableColumn.CellDataFeatures<SdItensPedidoRemessa, SdItensPedidoRemessa>, ObservableValue<SdItensPedidoRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodigo().getCodigo()));
                                }).build() /*Código*/,
                                FormTableColumn.create(cln -> {
                                    cln.title("Cor");
                                    cln.width(60);
                                    cln.value((Callback<TableColumn.CellDataFeatures<SdItensPedidoRemessa, SdItensPedidoRemessa>, ObservableValue<SdItensPedidoRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCor()));
                                    cln.alignment(Pos.CENTER);
                                }).build() /*Cor*/,
                                FormTableColumn.create(cln -> {
                                    cln.title("Local");
                                    cln.width(100);
                                    cln.value((Callback<TableColumn.CellDataFeatures<SdItensPedidoRemessa, SdItensPedidoRemessa>, ObservableValue<SdItensPedidoRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLocal()));
                                    cln.alignment(Pos.CENTER);
                                }).build() /*Local*/,
                                FormTableColumn.create(cln -> {
                                    cln.title("Qtde");
                                    cln.width(60);
                                    cln.value((Callback<TableColumn.CellDataFeatures<SdItensPedidoRemessa, SdItensPedidoRemessa>, ObservableValue<SdItensPedidoRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                                    cln.alignment(Pos.CENTER);
                                }).build() /*Qtde*/
                        );
                        table.factoryRow(param -> {
                            return new TableRow<SdItensPedidoRemessa>() {
                                @Override
                                protected void updateItem(SdItensPedidoRemessa item, boolean empty) {
                                    super.updateItem(item, empty);
                                    getStyleClass().removeAll("danger","warning","success");
                                    if (item != null && !empty) {
                                        getStyleClass().add(item.getStatusitem().equals("X") ? "danger" : item.getStatusitem().equals("F") ? "warning" :  item.getStatusitem().equals("C") ? "success" : "");
                                    }
                                }
                            };
                        });
                        table.indices(
                                table.indice("Coletado", "success", ""),
                                table.indice("Cancelado", "danger", ""),
                                table.indice("Falta", "warning", "")
                        );
                    });
                    fragment.box.getChildren().add(FormBox.create(box -> {
                        box.horizontal();
                        box.add(FormBox.create(box1 -> {
                            box1.vertical();
                            box1.add(tblModelagens.build(), tblLinhas.build());
                        }));
                        box.add(tblFamilia.build());
                        box.add(FormBox.create(box1 -> {
                            box1.vertical();
                            box1.add(FormFieldText.create(field -> {
                                field.title("Pedidos");
                                field.value.set(itensPedRemessa.stream().map(item -> item.getId().getNumero()).distinct().collect(Collectors.joining(", ")));
                            }).build());
                            box1.add(tblPecasRemessa.build());
                        }));
                    }));
                });
            }
        });
    }

    /**
     * Exibição e limpeza dos fields com os dados da remessa (cabeçalho) que está sendo feita.
     */
    private void exibirDadosRemessa() {
        remessaField.value.set(String.valueOf(remessaEmColeta.getRemessa()));
        origemRemessaField.value.set(remessaEmColeta.getPedidos().stream().max((o1, o2) -> o1.getQtde().compareTo(o2.getQtde())).get().getId().getNumero().getNumero().startsWith("W") ? "B2C" :
                remessaEmColeta.getPedidos().stream().max((o1, o2) -> o1.getQtde().compareTo(o2.getQtde())).get().getId().getNumero().getNumero().startsWith("B") ? "B2B" : "ERP");
        origemRemessaField.removeStyle("danger","primary");
        origemRemessaField.addStyle(origemRemessaField.value.get().equals("B2C") ? "danger" : "primary");

        totalRemessa.set(remessaEmColeta.getPedidos().stream().mapToInt(pedido -> pedido.getItens().stream().mapToInt(item -> item.getGrade().stream().mapToInt(SdGradeItemPedRem::getQtde).sum()).sum()).sum());
        observacaoRemessaField.value.set(remessaEmColeta.getObservacao());
        if (remessaEmColeta.getCodcli() == null) {
            MessageBox.create(message -> {
                message.message("A remessa carregada está com problemas cadastrais no cliente. " +
                        "Verifique com a remessa para ajuste e coleta.\n REMESSA: " + remessaEmColeta.getRemessa());
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showFullScreen();
            });

            return;
        }
        clienteField.value.set(remessaEmColeta.getCodcli().toString());
    }
    private void limparDadosRemessa() {
        remessaField.clear();
        origemRemessaField.clear();
        clienteField.clear();
        observacaoRemessaField.clear();
        remessaEmColeta = null;
        totalRemessa.set(0);
        lidoRemessa.set(0);
    }

    /**
     * exibição e limpeza dos fields com os dados da caixa aberta e caixas
     */
    private void exibirDadosCaixaAberta() {
        caixaField.value.set(String.valueOf(caixaAberta.getNumero()));
        barraLeituraField.requestFocus();
    }
    private void limparDadosCaixaAberta() {
        caixaField.clear();
    }

    /**
     * exibição e limpeza das caixas da remessa
     */
    private void exibirDadosCaixaRemessa(SdCaixaRemessa caixa) {
        boxCaixasRemessa.add(FormBadges.create(bg -> {
            bg.position(Pos.TOP_RIGHT, -3.0);
            bg.text(lb -> {
                lb.boldText();
                lb.sizeText(14);
                lb.addStyle("info");
                lb.borderRadius(50);
                lb.value.bind(caixa.qtdeProperty().asString());
            });
            bg.badgedNode(FormOverlap.create(ovl -> {
                ovl.add(ImageUtils.getIcon(caixa.getFechada() ? ImageUtils.Icon.CAIXA : ImageUtils.Icon.CAIXA_ABERTA, ImageUtils.IconSize._64));
                ovl.add(FormLabel.create(lb -> {
                    lb.value.set(String.valueOf(caixa.getNumero()));
                    lb.mouseClicked(event -> abrirCaixa(caixa));
                }));
            }));
        }));
    }
    private void limparDadosCaixaRemessa() {
        boxCaixasRemessa.clear();
    }

    @Override
    public void closeWindow() {
        if (isExcluirBarra.not().get()) {
            // testa se existe uma remessa aberta porém sem caixa, aberta para o coletor
            clearRemessaAlocaSemCaixa();
            Globals.getMainStage().close();
        } else {
            MessageBox.create(message -> {
                message.message("Seu tablet está no modo de remoção de peças, você não pode fechar. " +
                        "Remova as peças solicitadas ou mude o tipo de leitura para adicionar barra.");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showFullScreen();
            });
        }
    }

    private void clearRemessaAlocaSemCaixa() {
        if (emColeta.get() && remessaEmColeta != null && caixasRemessa.size() == 0) {
            // caso existe uma remessa aberta sem caixa, libera essa remessa para outro coletar
            remessaEmColeta.setColetor(null);
            remessaEmColeta.setDtIniColeta(null);
            remessaEmColeta.setStatus(statusEmRemessa);
            remessaEmColeta.getPedidos().forEach(pedido -> pedido.getItens().stream()
                    .filter(item -> !item.getStatusitem().equals("X")).forEach(item -> {
                        item.getGrade().stream().filter(grade -> !grade.getStatusitem().equals("X")).forEach(grade -> {
                            grade.setStatusitem("P");
                            new FluentDao().merge(grade);
                        });
                        item.setStatusitem("P");
                        new FluentDao().merge(item);
                    }));
            new FluentDao().merge(remessaEmColeta);
        }
    }

    private class ResumoRemessa {

        String descricao;
        Integer qtde;

        public ResumoRemessa(String descricao, Integer qtde) {
            this.descricao = descricao;
            this.qtde = qtde;
        }
    }
}
