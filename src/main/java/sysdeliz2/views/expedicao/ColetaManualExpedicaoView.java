package sysdeliz2.views.expedicao;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Callback;
import org.apache.log4j.Logger;
import sysdeliz2.controllers.views.expedicao.ColetaExpedicaoController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.sysdeliz.*;
import sysdeliz2.models.view.VSdItensRemessa;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.exceptions.BreakException;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.*;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class ColetaManualExpedicaoView extends ColetaExpedicaoController {
    
    // <editor-fold defaultstate="collapsed" desc="BeanController">
    private static final Logger logger = Logger.getLogger(ColetaManualExpedicaoView.class);
    private final BooleanProperty emColeta = new SimpleBooleanProperty(false);
    private final BooleanProperty hasCaixaAberta = new SimpleBooleanProperty(false);
    private final BooleanProperty isExcluirBarra = new SimpleBooleanProperty(false);
    private final BooleanProperty isApontamentoFalta = new SimpleBooleanProperty(false);
    private final ListProperty<SdItensCaixaRemessa> beanBarrasLidas = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final IntegerProperty totalRemessa = new SimpleIntegerProperty(0);
    private final IntegerProperty saldoRemessa = new SimpleIntegerProperty(0);
    private final IntegerProperty lidoRemessa = new SimpleIntegerProperty(0);
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Fields/Components">
    // dados da remesa
    private final FormFieldText remessaField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Remessa");
        field.editable(false);
        field.addStyle("lg");
        field.alignment(Pos.CENTER);
        field.width(150.0);
    });
    private final FormFieldText clienteField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Cliente");
        field.editable(false);
        field.expanded();
    });
    private final FormFieldText totalPecasField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Total");
        field.addStyle("lg").addStyle("primary");
        field.editable(false);
        field.alignment(Pos.CENTER);
        field.value.bind(totalRemessa.asString());
        field.width(140.0);
    });
    private final FormFieldText saldoPecasField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Saldo");
        field.editable(false);
        field.alignment(Pos.CENTER);
        field.addStyle("lg").addStyle("warning");
        field.value.bind(saldoRemessa.asString());
        field.width(140.0);
    });
    private final FormFieldText lidoPecasField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Lido");
        field.editable(false);
        field.addStyle("lg").addStyle("success");
        field.value.bind(lidoRemessa.asString());
        field.alignment(Pos.CENTER);
        field.width(140.0);
    });
    private final FormFieldTextArea observacaoRemessaField = FormFieldTextArea.create(field -> {
        field.title("Observações");
        field.editable(false);
        field.height(60.0);
    });
    // dados caixa
    private final FormFieldText caixaField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label(ImageUtils.getIcon(ImageUtils.Icon.LEITURA_BARRA, ImageUtils.IconSize._24));
        field.addStyle("lg");
        field.toUpper();
        field.alignment(Pos.CENTER);
        field.editable.bind(emColeta.and(hasCaixaAberta.not()));
        field.width(170.0);
        field.keyReleased(event -> criarCaixa(event));
    });
    private final FormBox boxCaixasRemessa = FormBox.create(box -> {
        box.flutuante();
        box.expanded();
    });
    // dados produto
    private final FormFieldText localField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Local");
        field.addStyle("lg");
        field.editable(false);
        field.alignment(Pos.CENTER);
        field.width(190.0);
    });
    private final FormFieldText codigoField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Ref.");
        field.addStyle("lg");
        field.editable(false);
        field.alignment(Pos.CENTER);
        field.width(150.0);
    });
    private final FormFieldText corField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Cor");
        field.alignment(Pos.CENTER);
        field.addStyle("lg");
        field.editable(false);
        field.width(120.0);
    });
    private final FormFieldText descProdutoField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.editable(false);
        field.width(150.0);
    });
    private final FormFieldTextArea observacaoProdutoField = FormFieldTextArea.create(field -> {
        field.title("Observação");
        field.expanded();
        field.editable(false);
        field.height(63.0);
    });
    private final FormFieldText barraLeituraField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label(ImageUtils.getIcon(ImageUtils.Icon.LEITURA_BARRA, ImageUtils.IconSize._24));
        field.addStyle("lg");
        field.alignment(Pos.CENTER);
        field.expanded();
        field.editable.bind(hasCaixaAberta);
        field.keyReleased(event -> leituraBarra(event));
    });
    // buttons
    private final FormButton btnTipoBarra = FormButton.create(btnTipoBarra -> {
        btnTipoBarra.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._32));
        btnTipoBarra.addStyle("lg").addStyle("success");
        btnTipoBarra.disable.bind(hasCaixaAberta.not());
        btnTipoBarra.setAction(evt -> trocarTipoLeitura());
    });
    private final FormButton btnFecharCaixa = FormButton.create(btnFecharCaixa -> {
        //btnFecharCaixa.title("Fechar");
        btnFecharCaixa.icon(ImageUtils.getIcon(ImageUtils.Icon.FECHAR_CAIXA, ImageUtils.IconSize._32));
        btnFecharCaixa.addStyle("primary");
        btnFecharCaixa.disable.bind(hasCaixaAberta.not());
        btnFecharCaixa.setAction(event -> fecharCaixa());
    });
    private final FormButton btnTrocarCaixa = FormButton.create(btnFecharCaixa -> {
        //btnFecharCaixa.title("Trocar");
        btnFecharCaixa.icon(ImageUtils.getIcon(ImageUtils.Icon.TROCAR, ImageUtils.IconSize._32));
        btnFecharCaixa.addStyle("warning");
        btnFecharCaixa.disable.bind(hasCaixaAberta.not());
        btnFecharCaixa.setAction(event -> trocarCaixa());
    });
    private final FormButton btnFalta = FormButton.create(btnFalta -> {
        //btnFalta.title("Falta");
        btnFalta.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._48));
        btnFalta.addStyle("xl").addStyle("danger");
        btnFalta.disable.bind(hasCaixaAberta.not());
        btnFalta.setAction(event -> apontarFaltaProduto());
    });
    private final FormButton btnCarregarRemessas = FormButton.create(btnCarregar -> {
        btnCarregar.title("Carregar Remessa");
        btnCarregar.icon(ImageUtils.getIcon(ImageUtils.Icon.ATUALIZAR, ImageUtils.IconSize._48));
        btnCarregar.addStyle("xl").addStyle("info");
        btnCarregar.disable.bind(emColeta);
        btnCarregar.setAction(evt -> {
            carregarNumeroRemessa();
        });
    });
    private final FormButton btnEncerrarColeta = FormButton.create(btnEncerrar -> {
        btnEncerrar.title("Encerrar Leitura");
        btnEncerrar.icon(ImageUtils.getIcon(ImageUtils.Icon.FECHAR_CAIXA, ImageUtils.IconSize._32));
        btnEncerrar.addStyle("lg").addStyle("warning");
        btnEncerrar.disable.bind(emColeta.not());
        btnEncerrar.setAction(evt -> {
            finalizarColeta();
        });
    });
    private final FormButton btnAvisarReposicao = FormButton.create(btnReposicao -> {
        btnReposicao.title("Avisar Reposição");
        btnReposicao.icon(ImageUtils.getIcon(ImageUtils.Icon.AVISAR, ImageUtils.IconSize._48));
        btnReposicao.addStyle("xl").addStyle("warning");
        btnReposicao.setAction(evt -> {
            /** * task: avisarReposicao(referenciaEmColeta) * return:  */
            AtomicReference<SQLException> excpAssync = new AtomicReference<>();
            new RunAsync().exec(task -> {
                try {
                    avisarReposicao(referenciaEmColeta, "A");
                } catch (SQLException e) {
                    e.printStackTrace();
                    excpAssync.set(e);
                    return ReturnAsync.EXCEPTION.value;
                }
                return ReturnAsync.OK.value;
            }).addTaskEndNotification(returnTask -> {
                if (returnTask.equals(ReturnAsync.OK.value)) {
                    MessageBox.create(message -> {
                        message.message("Aviso de reposição entregue!");
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.position(Pos.BOTTOM_RIGHT);
                        message.notification();
                    });
                } else {
                    ExceptionBox.build(message -> {
                        message.exception(excpAssync.get());
                        message.showAndWait();
                    });
                }
            });
        });
    });
    // table barras
    private final FormTableView<SdItensCaixaRemessa> tblBarraLidas = FormTableView.create(SdItensCaixaRemessa.class, table -> {
        table.title("Barras Lidas");
        table.height(350.0);
        table.expanded();
//        table.disable.set(true);
        table.items.bind(beanBarrasLidas);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Caixa");
                    cln.width(100.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItensCaixaRemessa, SdItensCaixaRemessa>, ObservableValue<SdItensCaixaRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCaixa().getNumero()));
                    cln.format(param -> {
                        return new TableCell<SdItensPedidoRemessa, Integer>() {
                            @Override
                            protected void updateItem(Integer item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(String.valueOf(item));
                                    getStyleClass().add("lg");
                                }
                            }
                        };
                    });
                }).build() /*Caixa*/,
                FormTableColumn.create(cln -> {
                    cln.title("Barra");
                    cln.width(400.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItensCaixaRemessa, SdItensCaixaRemessa>, ObservableValue<SdItensCaixaRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getBarra()));
                    cln.format(param -> {
                        return new TableCell<SdItensPedidoRemessa, String>() {
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(item);
                                    getStyleClass().add("lg");
                                }
                            }
                        };
                    });
                    cln.alignment(Pos.CENTER);
                }).build() /*Barra*/,
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(100.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItensCaixaRemessa, SdItensCaixaRemessa>, ObservableValue<SdItensCaixaRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getCodigo()));
                    cln.format(param -> {
                        return new TableCell<SdItensPedidoRemessa, String>() {
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(item);
                                    getStyleClass().add("lg");
                                }
                            }
                        };
                    });
                    cln.alignment(Pos.CENTER);
                }).build() /*Código*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cor");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItensCaixaRemessa, SdItensCaixaRemessa>, ObservableValue<SdItensCaixaRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCor()));
                    cln.format(param -> {
                        return new TableCell<SdItensPedidoRemessa, String>() {
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(item);
                                    getStyleClass().add("lg");
                                }
                            }
                        };
                    });
                    cln.alignment(Pos.CENTER);
                }).build() /*Cor*/,
                FormTableColumn.create(cln -> {
                    cln.title("Tam");
                    cln.width(60.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItensCaixaRemessa, SdItensCaixaRemessa>, ObservableValue<SdItensCaixaRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTam()));
                    cln.format(param -> {
                        return new TableCell<SdItensPedidoRemessa, String>() {
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(item);
                                    getStyleClass().add("lg");
                                }
                            }
                        };
                    });
                    cln.alignment(Pos.CENTER);
                }).build() /*Tam*/
        );
    });
    // proximo
    private final FormFieldText proximoLocalField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Local");
        field.alignment(Pos.CENTER);
        field.addStyle("lg");
        field.addStyle("amber");
        field.editable(false);
        field.width(200.0);
    });
    private final FormFieldText proximoCodigoField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Ref.");
        field.addStyle("lg");
        field.editable(false);
        field.addStyle("amber");
        field.alignment(Pos.CENTER);
        field.width(170.0);
    });
    private final FormFieldText proximoCorField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Cor");
        field.addStyle("lg");
        field.editable(false);
        field.addStyle("amber");
        field.alignment(Pos.CENTER);
        field.width(110.0);
    });
    // grade
    private final FormBox boxGrades = FormBox.create(boxGrade -> {
        boxGrade.horizontal();
    });
    // </editor-fold>
    
    public ColetaManualExpedicaoView() {
        super("Coleta Manual de Remessa", ImageUtils.getImage(ImageUtils.Icon.LEITURA_BARRA), false);
        init();
    }
    
    public void init() {
        super.box.getChildren().add(FormBox.create(principal -> {
            principal.horizontal();
            principal.expanded();
            principal.add(FormBox.create(boxEsquerda -> {
                boxEsquerda.vertical();
                boxEsquerda.add(FormBox.create(boxDadosRemessa -> {
                    boxDadosRemessa.vertical();
                    boxDadosRemessa.title("Dados da Remessa");
                    boxDadosRemessa.add(FormBox.create(boxDadosCliente -> {
                        boxDadosCliente.horizontal();
                        boxDadosCliente.add(remessaField.build());
                    }));
                    boxDadosRemessa.add(FormBox.create(boxDadosCliente -> {
                        boxDadosCliente.horizontal();
                        boxDadosCliente.add(clienteField.build());
                    }));
                    boxDadosRemessa.add(observacaoRemessaField.build());
                    boxDadosRemessa.add(FormBox.create(boxDadosCliente -> {
                        boxDadosCliente.horizontal();
                        boxDadosCliente.add(totalPecasField.build(), lidoPecasField.build(), saldoPecasField.build());
                    }));
                }));
                boxEsquerda.add(FormBox.create(boxDadosCaixa -> {
                    boxDadosCaixa.vertical();
                    boxDadosCaixa.title("Dados da Caixa");
                    boxDadosCaixa.add(FormBox.create(boxDados -> {
                        boxDados.horizontal();
                        boxDados.alignment(Pos.CENTER_LEFT);
                        boxDados.add(caixaField.build());
                        boxDados.add(FormBox.create(boxBadges -> {
                            boxBadges.horizontal();
                            boxBadges.add(FormBadges.create(badges -> {
                                badges.badgedNode(btnFecharCaixa);
                                badges.position(Pos.TOP_RIGHT, -5.0);
                                badges.text(lb -> {
                                    lb.value.set("F4");
                                    lb.addStyle("default");
                                    lb.sizeText(10);
                                    lb.boldText();
                                    lb.borderRadius(50);
                                });
                            }));
                            boxBadges.add(FormBadges.create(badges -> {
                                badges.badgedNode(btnTrocarCaixa);
                                badges.position(Pos.TOP_RIGHT, -5.0);
                                badges.text(lb -> {
                                    lb.value.set("F5");
                                    lb.addStyle("default");
                                    lb.sizeText(10);
                                    lb.boldText();
                                    lb.borderRadius(50);
                                });
                            }));
                        }));
                    }));
                    boxDadosCaixa.add(boxCaixasRemessa);
                }));
                boxEsquerda.add(FormBox.create(boxIndicadores -> {
                    boxIndicadores.horizontal();
                    boxIndicadores.expanded();
                    boxIndicadores.title("Indicadores de Coleta");
                }));
                boxEsquerda.add(FormBox.create(boxRelogio -> {
                    boxRelogio.horizontal();
                    boxRelogio.alignment(Pos.BOTTOM_LEFT);
                    boxRelogio.add(FormLabel.create(label -> {
                        label.value.set(StringUtils.toDateFormat(LocalDate.now()));
                        label.addStyle("lg");
                    }).build());
                    boxRelogio.add(FormClock.create(label -> {
                        label.addStyle("lg");
                    }).build());
                }));
            }));
            principal.add(FormBox.create(boxDireita -> {
                boxDireita.horizontal();
                boxDireita.expanded();
                boxDireita.add(FormBox.create(boxProduto -> {
                    boxProduto.vertical();
                    boxProduto.expanded();
                    boxProduto.add(FormBox.create(boxDadosProduto -> {
                        boxDadosProduto.vertical();
                        boxDadosProduto.expanded();
                        boxDadosProduto.title("Dados do Produto");
                        boxDadosProduto.add(FormBox.create(boxBarra -> {
                            boxBarra.horizontal();
                            boxBarra.alignment(Pos.BOTTOM_LEFT);
                            boxBarra.add(FormBox.create(boxBadges -> {
                                boxBadges.horizontal();
                                boxBadges.add(FormBadges.create(badges -> {
                                    badges.badgedNode(btnTipoBarra);
                                    badges.position(Pos.TOP_RIGHT, -5.0);
                                    badges.text(lb -> {
                                        lb.value.set("F7");
                                        lb.addStyle("default");
                                        lb.sizeText(10);
                                        lb.boldText();
                                        lb.borderRadius(50);
                                    });
                                }));
                            }));
                            boxBarra.add(barraLeituraField.build());
                        }));
                        boxDadosProduto.add(FormBox.create(boxProdutoBarra -> {
                            boxProdutoBarra.horizontal();
                            boxProdutoBarra.alignment(Pos.TOP_LEFT);
                            boxProdutoBarra.add(FormBox.create(boxDescricaoProduto -> {
                                boxDescricaoProduto.vertical();
                                boxDescricaoProduto.alignment(Pos.TOP_LEFT);
                                boxDescricaoProduto.add(FormBox.create(box -> {
                                    box.horizontal();
                                    box.add(codigoField.build());
                                    box.add(corField.build());
                                    box.add(localField.build());
                                }));
                                boxDescricaoProduto.add(descProdutoField.build());
                            }));
                            boxProdutoBarra.add(observacaoProdutoField.build());
//                            boxObsFalta.add(FormBox.create(boxBadges -> {
//                                boxBadges.horizontal();
//                                boxBadges.add(FormBadges.create(badges -> {
//                                    badges.badgedNode(btnFalta);
//                                    badges.position(Pos.TOP_RIGHT, -5.0);
//                                    badges.text(lb -> {
//                                        lb.value.set("F9");
//                                        lb.addStyle("default");
//                                        lb.sizeText(10);
//                                        lb.boldText();
//                                        lb.borderRadius(50);
//                                    });
//                                }));
//                            }));
                        }));
                        boxDadosProduto.add(boxGrades);
                        boxDadosProduto.add(tblBarraLidas.build());
                    }));
                    boxProduto.add(FormBox.create(boxAcoes -> {
                        boxAcoes.vertical();
                        boxAcoes.title("Ações");
                        boxAcoes.add(FormBox.create(boxBotoesAcoes -> {
                            boxBotoesAcoes.flutuante();
                            boxBotoesAcoes.expanded();
                            boxBotoesAcoes.add(btnCarregarRemessas);
                            boxBotoesAcoes.add(FormBox.create(box -> {
                                box.horizontal();
                                box.add(FormBox.create(boxBadges -> {
                                    boxBadges.horizontal();
                                    boxBadges.add(FormBadges.create(badges -> {
                                        badges.badgedNode(btnEncerrarColeta);
                                        badges.position(Pos.TOP_RIGHT, -5.0);
                                        badges.text(lb -> {
                                            lb.value.set("F12");
                                            lb.addStyle("default");
                                            lb.sizeText(10);
                                            lb.boldText();
                                            lb.borderRadius(50);
                                        });
                                    }));
                                }));
                            }));
                        }));
                    }));
                }));
            }));
        }));
        saldoRemessa.bind(totalRemessa.subtract(lidoRemessa));
        coletor = null;
    }
    
    /**
     * regra com controle de remessa aberta para exibição para o coletor
     * indicador de em coleta controlado
     */
    private void exibirRemessa() {
        limparDadosCaixaRemessa();
        if (remessaEmColeta != null) {
            // verifica se cliente da remessa é o mesmo ainda
            if (!verificaClienteRemessa()) {
                MessageBox.create(message -> {
                    message.message("Um dos pedidos desta remessa foi alterado o cliente, verifique com o atendimento para substituição do pedido.");
                    message.type(MessageBox.TypeMessageBox.ALERT);
                    message.showFullScreen();
                });
                
                exibirDadosRemessa();
                return;
            }
            
            // marca como o coletor está em coleta
            emColeta.set(true);
            // exibe os dados da remessa que está sendo coletada
            exibirDadosRemessa();
            
            lidoRemessa.set(remessaEmColeta.getPedidos().stream().mapToInt(pedido -> pedido.getItens().stream().mapToInt(item -> item.getGrade().stream().mapToInt(SdGradeItemPedRem::getQtdel).sum()).sum()).sum());
            caixasRemessa.forEach(this::exibirDadosCaixaRemessa);
            if (caixaAberta != null) {
                // exibindo dados da caixa
                exibirDadosCaixaAberta();
                hasCaixaAberta.set(true);
                barraLeituraField.requestFocus();
            } else {
                // enviando focus para a caixa
                caixaField.requestFocus();
            }
            caixasRemessa.forEach(caixa -> beanBarrasLidas.addAll(caixa.getItens()));
        }
    }
    
    /**
     * Leitura do código de barras principal
     *
     * @param event
     */
    private void leituraBarra(KeyEvent event) {
        if (event.getCode().equals(KeyCode.F4)) {
            barraLeituraField.clear();
            fecharCaixa();
            return;
        } else if (event.getCode().equals(KeyCode.F5)) {
            barraLeituraField.clear();
            trocarCaixa();
            return;
        } else if (event.getCode().equals(KeyCode.F7)) {
            barraLeituraField.clear();
            trocarTipoLeitura();
            return;
        } else if (event.getCode().equals(KeyCode.F9)) {
            barraLeituraField.clear();
//            apontarFaltaProduto();
            return;
        } else if (event.getCode().equals(KeyCode.F12)) {
            barraLeituraField.clear();
            finalizarColeta();
            return;
        } else if (event.getCode().equals(KeyCode.ENTER)) {
            if (barraLeituraField.value.get() != null) {
                String barra = barraLeituraField.value.get().toUpperCase();
                
                if (isExcluirBarra.not().get()) {
                    incluirBarra(barra);
                } else {
                    removerBarra(barra);
                }
            }
            barraLeituraField.clear();
            barraLeituraField.requestFocus();
        }
    }
    private void trocarTipoLeitura() {
        if (isApontamentoFalta.not().get()) {
            
            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                    "Trocando tipo de leitura de barra para " + (isExcluirBarra.not().get() ? "ENTRADA DE BARRAS" : "EXCLUSÃO DE BARRAS"));
            isExcluirBarra.set(isExcluirBarra.not().get());
            btnTipoBarra.removeStyle(isExcluirBarra.get() ? "success" : "danger");
            btnTipoBarra.addStyle(isExcluirBarra.get() ? "danger" : "success");
            btnTipoBarra.icon(ImageUtils.getIcon(isExcluirBarra.get() ? ImageUtils.Icon.CANCEL : ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._32));
            barraLeituraField.clear();
            barraLeituraField.requestFocus();
        }
    }
    private boolean incluirBarra(String barra) {
        try {
            // leitura de codigo de barra de produto
            String barra28 = barra.substring(0, 6);
            
            // get da referencia da barra
            referenciaEmColeta = null;
            for (SdPedidosRemessa pedido : remessaEmColeta.getPedidos()) {
                for (SdItensPedidoRemessa item : pedido.getItens()) {
                    for (SdGradeItemPedRem grade : item.getGrade()) {
                        if (grade.getBarra28().equals(barra28) && grade.getQtdep() > 0) {
                            referenciaEmColeta = item;
                            break;
                        }
                    }
                }
            }
            if (referenciaEmColeta == null) {
                for (SdPedidosRemessa pedido : remessaEmColeta.getPedidos()) {
                    for (SdItensPedidoRemessa item : pedido.getItens()) {
                        for (SdGradeItemPedRem grade : item.getGrade()) {
                            if (grade.getBarra28().equals(barra28)) {
                                referenciaEmColeta = item;
                                break;
                            }
                        }
                    }
                }
            }
            
            // <editor-fold defaultstate="collapsed" desc="Controle se a barra é de uma referência da remessa">
            AtomicBoolean isDaRemessa = new AtomicBoolean(false);
            remessaEmColeta.getPedidos().forEach(pedido -> {
                pedido.getItens().forEach(item -> {
                    item.getGrade().forEach(grade -> {
                        if (grade.getBarra28().equals(barra28))
                            isDaRemessa.set(true);
                    });
                });
            });
            if (!isDaRemessa.get()) {
                MessageBox.create(message -> {
                    message.message("A referência lida não pertence a esta remessa.");
                    message.type(MessageBox.TypeMessageBox.ERROR);
                    message.showFullScreen();
                });
                SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                        "Tentativa de incluir barra " + barra + " do produto na caixa " + caixaAberta.getNumero() + " porém barra não pertence a remessa em coleta");
                
                barraLeituraField.clear();
                barraLeituraField.requestFocus();
                return false;
            }
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Controle de barra em caixa">
            AtomicReference<SdItensCaixaRemessa> barraEmCaixa = new AtomicReference<>(new FluentDao().selectFrom(SdItensCaixaRemessa.class)
                    .where(eb -> eb
                            .equal("id.barra", barra)
                            .equal("status", "E"))
                    .singleResult());
            if (barraEmCaixa.get() != null || caixaAberta.getItens().stream().anyMatch(barraCaixa -> barraCaixa.getId().getBarra().equals(barra))) {
                caixaAberta.getItens().stream().filter(barraCaixa -> barraCaixa.getId().getBarra().equals(barra)).findAny().ifPresent(barraCaixa -> barraEmCaixa.set(barraCaixa));
                MessageBox.create(message -> {
                    message.message("Barra lida já consta em uma caixa. Caixa: " + barraEmCaixa.get().getId().getCaixa().getNumero());
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showFullScreen();
                });
                
                SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                        "Tentativa de incluir barra " + barra + " do produto " + referenciaEmColeta.getId().getCodigo().getCodigo() + " na cor " + referenciaEmColeta.getId().getCor()
                                + " na caixa " + caixaAberta.getNumero() + " porém já consta na caixa " + barraEmCaixa.get().getId().getCaixa().getNumero());
                barraLeituraField.clear();
                barraLeituraField.requestFocus();
                return false;
            }
            // </editor-fold>
            
            // adicionando referência na caixa
            AtomicReference<SdGradeItemPedRem> gradeBarra = new AtomicReference<>(null);
            referenciaEmColeta.getGrade().stream().filter(grade -> grade.getBarra28().equals(barra28)).findAny().ifPresent(grade -> gradeBarra.set(grade));
            if (gradeBarra.get() == null) {
                MessageBox.create(message -> {
                    message.message("Ocorreu um erro no coletor. Informe o TI o problema. Problema: não foi possível obter a grade da lista de grade da referência.");
                    message.type(MessageBox.TypeMessageBox.SIREN);
                    message.showFullScreen();
                });
                return false;
            }
            // <editor-fold defaultstate="collapsed" desc="Controle se todos os tamanhos da barra foram lidos">
            if (remessaEmColeta.getPedidos().stream()
                    .mapToLong(pedido -> pedido.getItens().stream()
                            .mapToLong(item -> item.getGrade().stream()
                                    .filter(grade -> grade.getId().getCodigo().equals(gradeBarra.get().getId().getCodigo())
                                            && grade.getId().getCor().equals(gradeBarra.get().getId().getCor())
                                            && grade.getId().getTam().equals(gradeBarra.get().getId().getTam()))
                                    .mapToLong(grade -> grade.getQtdep())
                                    .sum())
                            .sum())
                    .sum() == 0) {
                MessageBox.create(message -> {
                    message.message("Já foi lida toda a quantidade do tamanho " + gradeBarra.get().getId().getTam());
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showFullScreen();
                });
                
                SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                        "Tentativa de incluir barra " + barra + " do produto " + referenciaEmColeta.getId().getCodigo().getCodigo() + " na cor " + referenciaEmColeta.getId().getCor()
                                + " na caixa " + caixaAberta.getNumero() + " porém grade da barra já completa");
                barraLeituraField.clear();
                barraLeituraField.requestFocus();
                return false;
            }
            // </editor-fold>
            
            int qtdeParaSalvar = 1;
            // verifica se é um produto de KIT
            if (referenciaEmColeta.getTipo().equals("KIT"))
                qtdeParaSalvar = referenciaEmColeta.getId().getCodigo().getMinimo();
            
            for (int i = 0; i < qtdeParaSalvar; i++) {
                // atualizando os contadores de coleta da grade
                gradeBarra.get().setQtdel(gradeBarra.get().getQtdel() + 1);
                gradeBarra.get().setQtdep(gradeBarra.get().getQtdep() - 1);
                gradeBarra.get().setStatusitem("C");
                new FluentDao().merge(gradeBarra.get());
    
                // expedição do item no Excia. Update qtde na PA_ITEN. Merge do item na PEDIDO3 e Inclusão da PA_MOV
                expedirExcia(referenciaEmColeta, gradeBarra.get());
                lidoRemessa.set(lidoRemessa.add(1).get());
            }
            
            // atualizando status do item
            if (referenciaEmColeta.getGrade().stream().noneMatch(grade -> grade.getQtdep() > 0)) {
                referenciaEmColeta.setStatusitem("C");
                new FluentDao().merge(referenciaEmColeta);
            }
            
            // incluindo peça na caixa
            SdItensCaixaRemessa itemCaixa = new SdItensCaixaRemessa(caixaAberta,
                    barra,
                    referenciaEmColeta.getId().getCodigo(),
                    gradeBarra.get().getId().getCor(),
                    gradeBarra.get().getId().getTam(),
                    "E",
                    gradeBarra.get().getId().getNumero());
            new FluentDao().merge(itemCaixa);
            caixaAberta.getItens().add(itemCaixa);
            caixaAberta.setQtde(caixaAberta.getQtde() + 1);
            caixaAberta.setPeso(caixaAberta.getPeso().add(referenciaEmColeta.getId().getCodigo().getPeso()));
            beanBarrasLidas.add(0, itemCaixa);
            new FluentDao().merge(caixaAberta);
            
            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                    "Incluindo barra " + barra + " do produto " + referenciaEmColeta.getId().getCodigo().getCodigo() + " na cor " + referenciaEmColeta.getId().getCor() + " na caixa " + caixaAberta.getNumero());
            
            carregarProximoProduto(referenciaEmColeta);
            
            // verificação se concluiu a remessa
            if (remessaEmColeta.getPedidos().stream()
                    .mapToInt(pedido -> pedido.getItens().stream()
                            .mapToInt(item -> item.getGrade().stream().mapToInt(SdGradeItemPedRem::getQtdep).sum()).sum()).sum() == 0) {
                finalizarColeta();
            }
            return true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            ExceptionBox.build(box -> {
                box.exception(ex);
                box.showAndWait();
            });
            return false;
        }
    }
    private boolean removerBarra(String barra) {
        try {
            // leitura de codigo de barra de produto
            String barra28 = barra.substring(0, 6);
            AtomicReference<SdCaixaRemessa> caixaBarra = new AtomicReference<>(null);
            AtomicReference<SdItensCaixaRemessa> barraCaixaBarra = new AtomicReference<>(null);
            
            // get da referencia da barra
            referenciaEmColeta = null;
            for (SdPedidosRemessa pedido : remessaEmColeta.getPedidos()) {
                for (SdItensPedidoRemessa item : pedido.getItens()) {
                    for (SdGradeItemPedRem grade : item.getGrade()) {
                        if (grade.getBarra28().equals(barra28)) {
                            referenciaEmColeta = item;
                            break;
                        }
                    }
                }
            }
            
            // <editor-fold defaultstate="collapsed" desc="Controle se a barra é de uma referência da remessa">
            AtomicBoolean isDaRemessa = new AtomicBoolean(false);
            remessaEmColeta.getPedidos().forEach(pedido -> {
                pedido.getItens().forEach(item -> {
                    item.getGrade().forEach(grade -> {
                        if (grade.getBarra28().equals(barra28))
                            isDaRemessa.set(true);
                    });
                });
            });
            if (!isDaRemessa.get()) {
                MessageBox.create(message -> {
                    message.message("A referência lida não pertence a esta remessa.");
                    message.type(MessageBox.TypeMessageBox.ERROR);
                    message.showFullScreen();
                });
                
                SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                        "Tentativa de exclusão barra " + barra + " porém barra não pertence a remessa em coleta");
                barraLeituraField.clear();
                barraLeituraField.requestFocus();
                return false;
            }
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Quando falta: Controle se a barra é da referencia em coleta">
            if (isApontamentoFalta.get()) {
                if (referenciaEmColeta.getGrade().stream().noneMatch(grade -> grade.getBarra28().equals(barra28))) {
                    MessageBox.create(message -> {
                        message.message("Barra lida não pertence a grade da referência em coleta.");
                        message.type(MessageBox.TypeMessageBox.WARNING);
                        message.showFullScreen();
                    });
                    
                    SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                            "Tentativa de exclusão barra " + barra + " porém barra não pertence ao produto " + referenciaEmColeta.getId().getCodigo().getCodigo() + " na cor " + referenciaEmColeta.getId().getCor());
                    barraLeituraField.clear();
                    barraLeituraField.requestFocus();
                    return false;
                }
            }
            // </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="Controle de barra em caixa">
            AtomicBoolean isEmCaixa = new AtomicBoolean(false);
            caixasRemessa.forEach(caixa -> caixa.getItens().forEach(barraCaixa -> {
                if (barraCaixa.getId().getBarra().equals(barra)) {
                    isEmCaixa.set(true);
                    caixaBarra.set(caixa);
                    barraCaixaBarra.set(barraCaixa);
                }
            }));
            if (!isEmCaixa.get()) {
                MessageBox.create(message -> {
                    message.message("Barra lida não consta em uma caixa desta remessa");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showFullScreen();
                });
                
                SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                        "Tentativa de exclusão barra " + barra + " porém barra consta em uma caixa");
                barraLeituraField.clear();
                barraLeituraField.requestFocus();
                return false;
            }
            // </editor-fold>
            
            // removendo barra da caixa
            AtomicReference<SdItensPedidoRemessa> itemBarra = new AtomicReference<>(null);
            AtomicReference<SdGradeItemPedRem> gradeBarra = new AtomicReference<>(null);
            referenciasColetadas.forEach(item -> {
                item.getGrade().stream().filter(grade -> grade.getBarra28().equals(barra28)).findAny().ifPresent(grade -> {
                    itemBarra.set(item);
                    gradeBarra.set(grade);
                });
            });
            if (gradeBarra.get() == null || itemBarra.get() == null) {
                referenciaEmColeta.getGrade().stream().filter(grade -> grade.getBarra28().equals(barra28)).findAny().ifPresent(grade -> {
                    itemBarra.set(referenciaEmColeta);
                    gradeBarra.set(grade);
                });
            }
            if (gradeBarra.get() == null || itemBarra.get() == null) {
                MessageBox.create(message -> {
                    message.message("Ocorreu um erro no coletor. Informe o TI o problema. Problema: não foi possível obter a grade ou item da lista de referências coletadas.");
                    message.type(MessageBox.TypeMessageBox.SIREN);
                    message.showFullScreen();
                });
                return false;
            }
            
            int qtdeParaSalvar = 1;
            // verifica se é um produto de KIT
            if (referenciaEmColeta.getTipo().equals("KIT"))
                qtdeParaSalvar = referenciaEmColeta.getId().getCodigo().getMinimo();
            
            for (int i = 0; i < qtdeParaSalvar; i++) {
                // atualizando os contadores de coleta da grade
                if (isApontamentoFalta.not().get()) {
                    itemBarra.get().setStatusitem("P");
                    if (!referenciasParaColeta.contains(itemBarra.get()))
                        referenciasParaColeta.add(itemBarra.get());
                }
                gradeBarra.get().setQtdel(gradeBarra.get().getQtdel() - 1);
                gradeBarra.get().setQtdep(gradeBarra.get().getQtdep() + 1);
                gradeBarra.get().setStatusitem("P");
    
                // expedição do item no Excia. Update qtde na PA_ITEN. Merge do item na PEDIDO3 e Inclusão da PA_MOV
                estornoExcia(referenciaEmColeta, gradeBarra.get(), caixaBarra.get());
                lidoRemessa.set(lidoRemessa.subtract(1).get());
            }
            
            // atualizando status do item
            if (referenciaEmColeta.getStatusitem().equals("C")) {
                referenciaEmColeta.setStatusitem(isApontamentoFalta.get() ? "F" : "P");
                new FluentDao().merge(referenciaEmColeta);
            }
            
            // removendo peça na caixa
            beanBarrasLidas.remove(barraCaixaBarra.get());
            new FluentDao().delete(barraCaixaBarra.get());
            caixaBarra.get().refresh();
            caixaBarra.get().setQtde(caixaBarra.get().getQtde() - 1);
            caixaBarra.get().setPeso(caixaBarra.get().getPeso().subtract(barraCaixaBarra.get().getCodigo().getPeso()));
            new FluentDao().merge(caixaBarra.get());
            
            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                    "Exclusão barra " + barra + " do produto " + itemBarra.get().getId().getCodigo().getCodigo() + " na cor " + itemBarra.get().getId().getCor() + " na caixa " + caixaBarra.get().getNumero());
            beanBarrasLidas.remove(gradeBarra);
            
            // verificação da leitura de todas as referencias em falta para retirada da caixa
            if (isApontamentoFalta.get()) {
                if (referenciaEmColeta.getGrade().stream().mapToInt(SdGradeItemPedRem::getQtdel).sum() == 0) {
                    referenciasFaltantesParaRemover.remove(referenciaEmColeta);
                    if (referenciasFaltantesParaRemover.size() > 0) {
                        referenciaEmColeta = referenciasFaltantesParaRemover.get(0);
                        carregarProximoProduto(referenciaEmColeta);
                    } else {
                        isApontamentoFalta.set(false);
                        trocarTipoLeitura();
                        // caso ainda existam referências para leitura
                        getProximoProduto();
                    }
                }
            }
            return true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            ExceptionBox.build(box -> {
                box.exception(ex);
                box.showAndWait();
            });
            return false;
        }
    }
    private boolean removerBarra(SdCaixaRemessa caixa, SdItensCaixaRemessa barra) {
        try {
            // leitura de codigo de barra de produto
            String barra28 = barra.getId().getBarra().substring(0, 6);
            SdCaixaRemessa caixaBarra = caixa;
            SdItensCaixaRemessa barraCaixaBarra = barra;
            
            // get da referencia da barra
            referenciaEmColeta = null;
            for (SdPedidosRemessa pedido : remessaEmColeta.getPedidos()) {
                for (SdItensPedidoRemessa item : pedido.getItens()) {
                    for (SdGradeItemPedRem grade : item.getGrade()) {
                        if (grade.getBarra28().equals(barra28)) {
                            referenciaEmColeta = item;
                            break;
                        }
                    }
                }
            }
            
            // removendo barra da caixa
            AtomicReference<SdItensPedidoRemessa> itemBarra = new AtomicReference<>(null);
            AtomicReference<SdGradeItemPedRem> gradeBarra = new AtomicReference<>(null);
            referenciasColetadas.forEach(item -> {
                item.getGrade().stream().filter(grade -> grade.getBarra28().equals(barra28)).findAny().ifPresent(grade -> {
                    itemBarra.set(item);
                    gradeBarra.set(grade);
                });
            });
            if (gradeBarra.get() == null || itemBarra.get() == null) {
                referenciaEmColeta.getGrade().stream().filter(grade -> grade.getBarra28().equals(barra28)).findAny().ifPresent(grade -> {
                    itemBarra.set(referenciaEmColeta);
                    gradeBarra.set(grade);
                });
            }
            if (gradeBarra.get() == null || itemBarra.get() == null) {
                MessageBox.create(message -> {
                    message.message("Ocorreu um erro no coletor. Informe o TI o problema. Problema: não foi possível obter a grade ou item da lista de referências coletadas.");
                    message.type(MessageBox.TypeMessageBox.SIREN);
                    message.showFullScreen();
                });
                return false;
            }
            
            int qtdeParaSalvar = 1;
            // verifica se é um produto de KIT
            if (itemBarra.get().getTipo().equals("KIT"))
                qtdeParaSalvar = itemBarra.get().getId().getCodigo().getMinimo();
            
            itemBarra.get().setStatusitem("P");
            new FluentDao().merge(itemBarra.get());
            if (!referenciasParaColeta.contains(itemBarra.get()))
                referenciasParaColeta.add(itemBarra.get());
            
            for (int i = 0; i < qtdeParaSalvar; i++) {
                gradeBarra.get().setQtdel(gradeBarra.get().getQtdel() - 1);
                gradeBarra.get().setQtdep(gradeBarra.get().getQtdep() + 1);
                gradeBarra.get().setStatusitem(isApontamentoFalta.get() ? "F" : "P");
                new FluentDao().merge(gradeBarra.get());
    
                // expedição do item no Excia. Update qtde na PA_ITEN. Merge do item na PEDIDO3 e Inclusão da PA_MOV
                estornoExcia(itemBarra.get(), gradeBarra.get(), caixaBarra);
            }
            
            // removendo peça na caixa
            new FluentDao().delete(barraCaixaBarra);
            caixaBarra.refresh();
            new FluentDao().merge(caixaBarra);
            
            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.EXCLUIR, String.valueOf(remessaEmColeta.getRemessa()),
                    "Exclusão barra " + barra + " do produto " + itemBarra.get().getId().getCodigo().getCodigo() + " na cor " + itemBarra.get().getId().getCor() + " na caixa " + caixaBarra.getNumero());
            
            return true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    /**
     * Finalizar coleta da remessa
     */
    private void finalizarColeta() {
        AtomicReference<Boolean> encerrar = new AtomicReference<>(false);
        if (remessaEmColeta.getPedidos().stream()
                .mapToInt(pedido -> pedido.getItens().stream()
                        .mapToInt(item -> item.getGrade().stream().mapToInt(SdGradeItemPedRem::getQtdep).sum()).sum()).sum() > 0) {
            ObservableList<SdGradeItemPedRem> itensNaoLidos = FXCollections.observableArrayList();
            remessaEmColeta.getPedidos().forEach(pedido -> pedido.getItens().forEach(item -> item.getGrade().forEach(grade -> {
                if (grade.getQtdep() > 0)
                    itensNaoLidos.add(grade);
            })));
            new Fragment().show(fragment -> {
                fragment.title("Peças não Lidas na Remessa");
                fragment.size(400.0, 800.0);
                
                final FormFieldTextArea mensagem = FormFieldTextArea.create(field -> {
                    field.withoutTitle();
                    field.editable(false);
                    field.height(60.0);
                    field.value.set("Os itens abaixo listados não foram lidos e pertencem a essa remessa, a remessa não pode ser finalizada até que todos os itens sejam lidos ou cancelados?");
                });
                final FormTableView<SdGradeItemPedRem> tblItensNaoLidos = FormTableView.create(SdGradeItemPedRem.class, table -> {
                    table.title("Itens não Lidos");
                    table.expanded();
                    table.items.set(itensNaoLidos);
                    table.columns(
                            FormTableColumn.create(cln -> {
                                cln.title("Pedido");
                                cln.width(120.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdGradeItemPedRem, SdGradeItemPedRem>, ObservableValue<SdGradeItemPedRem>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getNumero()));
                                cln.alignment(Pos.CENTER);
                            }).build() /*Pedido*/,
                            FormTableColumn.create(cln -> {
                                cln.title("Código");
                                cln.width(100.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdGradeItemPedRem, SdGradeItemPedRem>, ObservableValue<SdGradeItemPedRem>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodigo()));
                                cln.alignment(Pos.CENTER);
                            }).build() /*Código*/,
                            FormTableColumn.create(cln -> {
                                cln.title("Cor");
                                cln.width(80.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdGradeItemPedRem, SdGradeItemPedRem>, ObservableValue<SdGradeItemPedRem>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCor()));
                                cln.alignment(Pos.CENTER);
                            }).build() /*Cor*/,
                            FormTableColumn.create(cln -> {
                                cln.title("Tam");
                                cln.width(60.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdGradeItemPedRem, SdGradeItemPedRem>, ObservableValue<SdGradeItemPedRem>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getTam()));
                                cln.alignment(Pos.CENTER);
                            }).build() /*Tam*/,
                            FormTableColumn.create(cln -> {
                                cln.title("Qtde");
                                cln.width(70.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdGradeItemPedRem, SdGradeItemPedRem>, ObservableValue<SdGradeItemPedRem>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtdep()));
                                cln.alignment(Pos.CENTER);
                            }).build() /*Qtde*/
                    );
                });
                fragment.box.getChildren().add(mensagem.build());
                fragment.box.getChildren().add(tblItensNaoLidos.build());
            });
        } else {
            encerrar.set(true);
        }
        
        if (encerrar.get()) {
            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                    "Finalizando coleta de remessa.");
            
            // verifica se cliente da remessa é o mesmo ainda
            if (!verificaClienteRemessa()) {
                MessageBox.create(message -> {
                    message.message("Um dos pedidos desta remessa foi alterado o cliente, verifique com o atendimento para substituição do pedido.");
                    message.type(MessageBox.TypeMessageBox.ALERT);
                    message.showFullScreen();
                });
                return;
            }
            
            // fechando a caixa caso esta esteja aberta
            String caixasFinalizadas = caixasRemessa.stream().map(caixa -> String.valueOf(caixa.getNumero())).collect(Collectors.joining(", "));
            if (caixaAberta != null)
                fecharCaixa();
            caixasRemessa.clear();
            
            referenciaEmColeta = null;
            referenciasEmFalta.clear();
            referenciasParaColeta.clear();
            referenciasColetadas.clear();
            beanBarrasLidas.clear();
            
            remessaEmColeta.setStatus(statusColetaFinalizada);
            remessaEmColeta.setDtFimColeta(LocalDateTime.now());
            new FluentDao().merge(remessaEmColeta);
            MessageBox.create(message -> {
                message.message("Coleta da remessa " + remessaEmColeta.getRemessa() + " finalizada! Encaminhe a(s) caixa(s): "
                        + caixasFinalizadas + " para área de fechamento.");
                message.type(MessageBox.TypeMessageBox.SUCCESS);
                message.showFullScreen();
            });
            limparDadosRemessa();
            limparDadosCaixaAberta();
            limparDadosCaixaRemessa();
            limparDadosProduto();
            remessaEmColeta = null;
            emColeta.set(false);
            isExcluirBarra.set(false);
        } else {
            JPAUtils.getEntityManager().refresh(remessaEmColeta);
        }
    }
    
    /**
     * Get do proximo produto para ser lido
     */
    private void getProximoProduto() {
        // caso ainda existam referências para leitura
        referenciasParaColeta.sort(Comparator.comparing(SdItensPedidoRemessa::getLocal));
        referenciaEmColeta = null;
        while (referenciaEmColeta == null && referenciasParaColeta.size() > 0) {
            referenciaEmColeta = referenciasParaColeta.get(0); // get da próxima referência
            referenciaEmColeta.refresh();
            if (referenciaEmColeta.getStatusitem().equals("X")) {
                referenciasParaColeta.remove(referenciaEmColeta);
                referenciaEmColeta = null;
            }
        }
        
        if (referenciaEmColeta != null) {
            carregarProximoProduto(referenciaEmColeta);
        } else {
            // verifica as referências enviadas para falta foram resolvidas
            referenciasEmFalta.forEach(SdItensPedidoRemessa::refresh);
            if (referenciasEmFalta.size() > 0) {
                referenciasParaColeta.addAll(referenciasEmFalta.stream().collect(Collectors.toList()));
                referenciaEmColeta = referenciasParaColeta.get(0); // get da próxima referência
                referenciasEmFalta.clear();
                carregarProximoProduto(referenciaEmColeta);
            } else {
                finalizarColeta();
            }
        }
        
        beanBarrasLidas.clear();
    }
    
    /**
     * Carregando dados do próximo item para coleta
     *
     * @param item
     */
    private void carregarProximoProduto(SdItensPedidoRemessa item) {
        // exibe os dados do produto em coleta
        localField.value.set(item.getLocal());
        codigoField.value.set(item.getId().getCodigo().getCodigo());
        corField.value.set(item.getId().getCor());
        localField.removeStyle("danger").removeStyle("primary");
        codigoField.removeStyle("danger").removeStyle("primary");
        corField.removeStyle("danger").removeStyle("primary");
        
        // pinta os componentes conforme o status do item (VERMELHO - FALTANTE / AZUL - PENDENTE COLETA)
        if (isApontamentoFalta.get()) {
            localField.addStyle("danger");
            codigoField.addStyle("danger");
            corField.addStyle("danger");
        } else {
            localField.addStyle("primary");
            codigoField.addStyle("primary");
            corField.addStyle("primary");
        }
        
        String obsExpedicao = item.getId().getCodigo().getSdProduto().getExpedicao() != null ? item.getId().getCodigo().getSdProduto().getExpedicao() + " / " : "";
        String obsUnicoCaixa = item.getId().getCodigo().getSdProduto().getUnicocaixa() ? "ESTE PRODUTO DEVE SER ÚNICO NA CAIXA. / " : "";
        String obsAtendimento = item.getObservacao() != null ? item.getObservacao() : "";
        String obsProduto = obsExpedicao + obsUnicoCaixa + obsAtendimento;
        observacaoProdutoField.value.set(obsProduto);
        boxGrades.clear();
        item.getGrade().sort(Comparator.comparingInt(SdGradeItemPedRem::getOrdem));
        for (SdGradeItemPedRem sdGradeItemPedRem : item.getGrade()) {
            boxGrades.add(FormBox.create(boxDadosGrade -> {
                boxDadosGrade.vertical();
                boxDadosGrade.width(70.0);
                boxDadosGrade.add(FormFieldText.create(field -> {
                    field.withoutTitle();
                    field.editable(false);
                    field.alignment(Pos.CENTER);
                    field.width(70.0);
                    field.addStyle("sm").addStyle(sdGradeItemPedRem.getStatusitem().equals("X") ? "warning" : isApontamentoFalta.get() ? "danger" : "primary");
                    field.value.set(sdGradeItemPedRem.getId().getTam());
                }).build());
                boxDadosGrade.add(FormBadges.create(bg -> {
                    bg.badgedNode(FormFieldText.create(field -> {
                        field.withoutTitle();
                        field.editable(false);
                        field.width(70.0);
                        field.alignment(Pos.CENTER);
                        field.addStyle("lg");
                        if (sdGradeItemPedRem.getQtdep() == 0)
                            field.addStyle("success");
                        field.value.bind(sdGradeItemPedRem.qtdepProperty().asString());
                        sdGradeItemPedRem.qtdepProperty().addListener((observable, oldValue, newValue) -> {
                            if (newValue != null) {
                                field.removeStyle("success");
                                if (newValue.intValue() == 0) {
                                    field.addStyle("success");
                                }
                            }
                        });
                    }).build());
                    bg.text(lb -> {
                        lb.value.bind(sdGradeItemPedRem.qtdelProperty().asString());
                        lb.addStyle("warning");
                        lb.sizeText(14);
                        lb.boldText();
                        lb.borderRadius(50);
                    });
                    bg.visible.bind(sdGradeItemPedRem.qtdelProperty().greaterThan(0));
                    bg.position(Pos.TOP_RIGHT, -4.0);
                }));
            }));
        }
    }
    
    private void limparDadosProduto() {
        localField.clear();
        codigoField.clear();
        corField.clear();
        descProdutoField.clear();
        observacaoProdutoField.clear();
        localField.removeStyle("danger").removeStyle("primary");
        codigoField.removeStyle("danger").removeStyle("primary");
        corField.removeStyle("danger").removeStyle("primary");
        boxGrades.clear();
    }
    
    /**
     * Rotina para apontar falta da referência em coleta
     */
    private void apontarFaltaProduto() {
        try {
            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                    "Apontando falta do produto " + referenciaEmColeta.getId().getCodigo().getCodigo() + " na cor " + referenciaEmColeta.getId().getCor());
            faltaProduto();
            if (referenciasFaltantesParaRemover.size() > 0) {
                if (isExcluirBarra.not().get())
                    trocarTipoLeitura();
                isApontamentoFalta.set(true);
                referenciaEmColeta = referenciasFaltantesParaRemover.get(0);
                carregarProximoProduto(referenciaEmColeta);
            } else {
                // caso ainda existam referências para leitura
                getProximoProduto();
            }
            barraLeituraField.clear();
            barraLeituraField.requestFocus();
        } catch (SQLException e) {
            e.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(e);
                message.showAndWait();
            });
        }
    }
    
    /**
     * Criar, Abrir e Fechar Caixa
     */
    private void criarCaixa(KeyEvent event) {
        if (event.getCode().equals(KeyCode.ENTER)) {
            if (caixaField.value.get().startsWith("CX") && caixaField.value.get().length() == 8) {
                caixaField.value.set(caixaField.value.get().replace("CX", ""));
                // verificação de caixa existente
                SdCaixaRemessa validacaoCaixa = varificaCaixaExistente(caixaField.value.get());
                if (validacaoCaixa != null) {
                    validacaoCaixa.refresh();
                    if (validacaoCaixa.getRemessa().getRemessa().compareTo(remessaEmColeta.getRemessa()) != 0) {
                        MessageBox.create(message -> {
                            message.message("Esse número de caixa já consta em outra remessa. Verifique o código de barras lido e faça a substituição. Remessa alocada: " + validacaoCaixa.getRemessa().getRemessa() +
                                    " Coletor: " + (validacaoCaixa.getRemessa().getColetor() != null ? validacaoCaixa.getRemessa().getColetor().getNome() : "MANUAL"));
                            message.type(MessageBox.TypeMessageBox.WARNING);
                            message.showFullScreen();
                        });
                        SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                                "Tentativa de inclusão de uma caixa já existente em outra remessa. Barra lida: " + caixaField.value.get());
                        caixaField.clear();
                        caixaField.requestFocus();
                    } else if (validacaoCaixa.getFechada()) {
                        MessageBox.create(message -> {
                            message.message("Esse número de caixa já consta nesta remessa, porém a caixa se encontra como fechada. Você pode abrir essa caixa se desejar.");
                            message.type(MessageBox.TypeMessageBox.IMPORTANT);
                            message.showFullScreen();
                        });
                        SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                                "Tentativa de inclusão de uma caixa já existente na remessa e marcada como fechada. Barra lida: " + caixaField.value.get());
                        caixaField.clear();
                        caixaField.requestFocus();
                    } else {
                        caixaAberta = validacaoCaixa;
                        limparDadosCaixaAberta();
                        exibirDadosCaixaAberta();
                        hasCaixaAberta.set(true);
                    }
                } else {
                    ultimaCaixaAberta = caixaAberta;
                    incluirCaixa(new SdCaixaRemessa(Integer.parseInt(caixaField.value.get()), remessaEmColeta));
                    limparDadosCaixaAberta();
                    exibirDadosCaixaAberta();
                    limparDadosCaixaRemessa();
                    caixasRemessa.forEach(this::exibirDadosCaixaRemessa);
                    hasCaixaAberta.set(true);
                    
                    SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                            "Incluída a caixa " + caixaAberta.getNumero() + " na remessa. Barra lida: " + caixaField.value.get());
                }
            } else {
                MessageBox.create(message -> {
                    message.message("O código de barras lido não é válido para a caixa, verifique a etiqueta.");
                    message.type(MessageBox.TypeMessageBox.ERROR);
                    message.showFullScreen();
                });
                
                SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                        "Tentativa de inclusão de uma caixa com código de barras inválido. Barra lida: " + caixaField.value.get());
                caixaField.clear();
                caixaField.requestFocus();
            }
        }
    }
    private void abrirCaixa(SdCaixaRemessa caixa) {
        new Fragment().show(fragment -> {
            fragment.title("LEITURA DO CÓDIGO DE BARRAS DA CAIXA");
            fragment.size(300.0, 100.0);
            
            final FormFieldText fieldBarcodeCaixa = FormFieldText.create(field -> {
                field.withoutTitle();
                field.expanded();
                field.toUpper();
                field.label(ImageUtils.getIcon(ImageUtils.Icon.LEITURA_BARRA, ImageUtils.IconSize._24));
                field.addStyle("lg");
                field.alignment(Pos.CENTER);
            });
            fieldBarcodeCaixa.requestFocus();
            fragment.box.getChildren().add(fieldBarcodeCaixa.build());
            fragment.buttonsBox.getChildren().add(FormButton.create(btn -> {
                btn.title("ABRIR");
                btn.addStyle("lg").addStyle("success");
                btn.defaultButton(true);
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CAIXA_ABERTA, ImageUtils.IconSize._32));
                btn.setAction(evt -> {
                    if (fieldBarcodeCaixa.value.get().startsWith("CX") && fieldBarcodeCaixa.value.get().length() == 8) {
                        fieldBarcodeCaixa.value.set(fieldBarcodeCaixa.value.get().replace("CX", ""));
                        if (caixa.getNumero().compareTo(Integer.parseInt(fieldBarcodeCaixa.value.get())) == 0) {
                            // verificação de caixa existente
                            ultimaCaixaAberta = caixaAberta;
                            ultimaCaixaAberta.setFechada(true);
                            caixaAberta = caixa;
                            caixaAberta.setFechada(false);
                            new FluentDao().merge(caixaAberta);
                            limparDadosCaixaRemessa();
                            caixasRemessa.forEach(this::exibirDadosCaixaRemessa);
                            
                            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                                    "Abrindo caixa para inclusão de peças. Barra lida: " + fieldBarcodeCaixa.value.get());
                            fragment.close();
                        } else {
                            MessageBox.create(message -> {
                                message.message("O código lido não confere com a caixa que está sendo aberta, verifique o número da caixa e leia novamente.");
                                message.type(MessageBox.TypeMessageBox.ERROR);
                                message.showFullScreen();
                            });
                            
                            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                                    "Tentativa de abertura de uma caixa com o número diferente da caixa. Barra caixa: " + caixa.getNumero() + " Barra lida: " + fieldBarcodeCaixa.value.get());
                            fieldBarcodeCaixa.clear();
                            fieldBarcodeCaixa.requestFocus();
                        }
                    } else {
                        MessageBox.create(message -> {
                            message.message("O código de barras lido não é válido para a caixa, verifique a etiqueta.");
                            message.type(MessageBox.TypeMessageBox.ERROR);
                            message.showFullScreen();
                        });
                        
                        SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                                "Tentativa de abertura de uma caixa com código de barra inválido. Barra lida: " + fieldBarcodeCaixa.value.get());
                        fieldBarcodeCaixa.clear();
                        fieldBarcodeCaixa.requestFocus();
                    }
                });
            }));
        });
        if (caixaAberta == null) {
            hasCaixaAberta.set(false);
            caixaField.clear();
            caixaField.requestFocus();
        }
    }
    private void trocarCaixa() {
        new Fragment().show(fragment -> {
            fragment.title("SUBSTITUIÇÃO DE CAIXAS");
            fragment.size(300.0, 100.0);
            
            final FormFieldText fieldBarcodeCaixa = FormFieldText.create(field -> {
                field.withoutTitle();
                field.expanded();
                field.toUpper();
                field.label(ImageUtils.getIcon(ImageUtils.Icon.LEITURA_BARRA, ImageUtils.IconSize._24));
                field.addStyle("lg");
                field.alignment(Pos.CENTER);
            });
            fieldBarcodeCaixa.requestFocus();
            fragment.box.getChildren().add(fieldBarcodeCaixa.build());
            fragment.buttonsBox.getChildren().add(FormButton.create(btn -> {
                btn.title("TROCAR");
                btn.addStyle("lg").addStyle("success");
                btn.defaultButton(true);
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.TROCAR, ImageUtils.IconSize._32));
                btn.setAction(evt -> {
                    if (fieldBarcodeCaixa.value.get().startsWith("CX") && fieldBarcodeCaixa.value.get().length() == 8) {
                        fieldBarcodeCaixa.value.set(fieldBarcodeCaixa.value.get().replace("CX", ""));
                        try {
                            // verificação de caixa existente
                            SdCaixaRemessa validacaoCaixa = varificaCaixaExistente(fieldBarcodeCaixa.value.get());
                            if (validacaoCaixa != null) {
                                validacaoCaixa.refresh();
                                if (validacaoCaixa.getRemessa().getRemessa().compareTo(remessaEmColeta.getRemessa()) != 0) {
                                    MessageBox.create(message -> {
                                        message.message("Esse número de caixa já consta em outra remessa. Verifique o código de barras lido e faça a substituição. Remessa alocada: " + validacaoCaixa.getRemessa().getRemessa() +
                                                " Coletor: " + validacaoCaixa.getRemessa().getColetor().getNome());
                                        message.type(MessageBox.TypeMessageBox.WARNING);
                                        message.showFullScreen();
                                    });
                                    SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                                            "Tentativa de troca de caixa por uma caixa já existente em outra remessa. Barra lida: " + fieldBarcodeCaixa.value.get());
                                    fieldBarcodeCaixa.clear();
                                    fieldBarcodeCaixa.requestFocus();
                                } else if (validacaoCaixa.getFechada()) {
                                    MessageBox.create(message -> {
                                        message.message("Esse número de caixa já consta nesta remessa, porém a caixa se encontra como fechada. Você pode abrir essa caixa se desejar.");
                                        message.type(MessageBox.TypeMessageBox.IMPORTANT);
                                        message.showFullScreen();
                                    });
                                    SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                                            "Tentativa de troca de caixa por uma caixa já existente na remessa e marcada como fechada. Barra lida: " + fieldBarcodeCaixa.value.get());
                                    fieldBarcodeCaixa.clear();
                                    fieldBarcodeCaixa.requestFocus();
                                }
                            } else {
                                SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                                        "Trocando a caixa " + caixaAberta.getNumero() + " para a barra " + fieldBarcodeCaixa.value.get());
                                
                                caixaAberta = substituirCaixa(fieldBarcodeCaixa.value.get());
                                limparDadosCaixaRemessa();
                                caixasRemessa.forEach(this::exibirDadosCaixaRemessa);
                                fragment.close();
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                            ExceptionBox.build(message -> {
                                message.exception(e);
                                message.showAndWait();
                            });
                        }
                    } else {
                        MessageBox.create(message -> {
                            message.message("O código de barras lido não é válido para a caixa, verifique a etiqueta.");
                            message.type(MessageBox.TypeMessageBox.ERROR);
                            message.showFullScreen();
                        });
                        
                        SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                                "Tentativa de troca de caixa com código de barras inválido. Barra lida: " + fieldBarcodeCaixa.value.get());
                        fieldBarcodeCaixa.clear();
                        fieldBarcodeCaixa.requestFocus();
                    }
                });
            }));
        });
        if (caixaAberta == null) {
            hasCaixaAberta.set(false);
            caixaField.clear();
            caixaField.requestFocus();
        } else {
            exibirDadosCaixaAberta();
            limparDadosCaixaRemessa();
            caixasRemessa.forEach(this::exibirDadosCaixaRemessa);
        }
    }
    private void fecharCaixa() {
        if (caixaAberta.getItens().size() == 0) {
            MessageBox.create(message -> {
                message.message("Você não pode fechar a caixa pois ela não contém nenhum item dentro dela. Você pode utilizar a opção de trocar caixa caso não queira mais utilizar essa.");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showFullScreen();
            });
            
            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                    "Tentativa de fechar a caixa " + caixaAberta.getNumero() + " sem itens na caixa.");
            return;
        }
        
        
        SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
                "Fechando a caixa " + caixaAberta.getNumero() + " com " + caixaAberta.getItens().size() + " itens.");
        caixaAberta.setFechada(true);
        new FluentDao().merge(caixaAberta);
        hasCaixaAberta.set(false);
        limparDadosCaixaAberta();
        caixaAberta = null;
        limparDadosCaixaRemessa();
        caixasRemessa.forEach(this::exibirDadosCaixaRemessa);
        caixaField.clear();
        caixaField.requestFocus();
    }
    private void excluirCaixa(SdCaixaRemessa caixa) {
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Deseja realmente excluir a caixa " + caixa.getNumero() + "?");
            message.showAndWait();
        }).value.get())) {
            caixa.getItens().forEach(barra -> beanBarrasLidas.removeIf(it -> it.getId().getBarra().equals(barra.getId().getBarra())));
            /**
             * task:  * return:
             */
            new RunAsyncWithOverlay(this).exec(task -> {
                lidoRemessa.set(lidoRemessa.subtract(caixa.getQtde()).get());
                caixa.getItens().forEach(barra -> beanBarrasLidas.remove(barra));
                caixa.getItens().forEach(barra -> removerBarra(caixa, barra));
                caixasRemessa.remove(caixa);
                new FluentDao().delete(caixa);
                SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.EXCLUIR, String.valueOf(remessaEmColeta.getRemessa()),
                        "Exclusão da caixa " + caixa.getNumero());
                return ReturnAsync.OK.value;
            }).addTaskEndNotification(taskReturn -> {
                if (taskReturn.equals(ReturnAsync.OK.value)) {
                    tblBarraLidas.refresh();
                    limparDadosCaixaRemessa();
                    caixasRemessa.forEach(this::exibirDadosCaixaRemessa);
                }
            });
        }
    }
    
    /**
     * Carrega os produtos da remessa e exibe por:
     * - Acumulado por GrupoModelagem
     * - Acumulado por LInha
     * - Acumulado por Família
     */
    private void carregarProdutoRemessa() {
        AtomicReference<List<VSdItensRemessa>> itensRemessa = new AtomicReference<>();
        new RunAsyncWithOverlay(this).exec(task -> {
            itensRemessa.set(getItensRemessa(remessaEmColeta.getRemessa()));
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                new Fragment().show(fragment -> {
                    fragment.title("Produtos da Remessa");
                    fragment.size(800.0, 500.0);
                    ObservableList<ResumoRemessa> resumoModelagem = FXCollections.observableArrayList();
                    ObservableList<ResumoRemessa> resumoLinha = FXCollections.observableArrayList();
                    ObservableList<ResumoRemessa> resumoFamilia = FXCollections.observableArrayList();
                    itensRemessa.get().stream().collect(Collectors.groupingBy(VSdItensRemessa::getGrupomodelagem, Collectors.summingInt(VSdItensRemessa::getQtde))).forEach((s, i) -> resumoModelagem.add(new ResumoRemessa(s, i)));
                    itensRemessa.get().stream().collect(Collectors.groupingBy(VSdItensRemessa::getLinha, Collectors.summingInt(VSdItensRemessa::getQtde))).forEach((s, i) -> resumoLinha.add(new ResumoRemessa(s, i)));
                    itensRemessa.get().stream().collect(Collectors.groupingBy(VSdItensRemessa::getFamilia, Collectors.summingInt(VSdItensRemessa::getQtde))).forEach((s, i) -> resumoFamilia.add(new ResumoRemessa(s, i)));
                    final FormTableView<ResumoRemessa> tblModelagens = FormTableView.create(ResumoRemessa.class, table -> {
                        table.title("MODELAGENS");
                        table.items.set(resumoModelagem);
                        table.columns(
                                FormTableColumn.create(cln -> {
                                    cln.title("Modelagem");
                                    cln.width(200.0);
                                    cln.value((Callback<TableColumn.CellDataFeatures<ResumoRemessa, ResumoRemessa>, ObservableValue<ResumoRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().descricao));
                                }).build() /*Modelagem*/,
                                FormTableColumn.create(cln -> {
                                    cln.title("Qtde");
                                    cln.width(60);
                                    cln.value((Callback<TableColumn.CellDataFeatures<ResumoRemessa, ResumoRemessa>, ObservableValue<ResumoRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().qtde));
                                    cln.alignment(Pos.CENTER);
                                }).build() /*Qtde*/
                        );
                    });
                    final FormTableView<ResumoRemessa> tblLinhas = FormTableView.create(ResumoRemessa.class, table -> {
                        table.title("LINHAS");
                        table.items.set(resumoLinha);
                        table.columns(
                                FormTableColumn.create(cln -> {
                                    cln.title("Linha");
                                    cln.width(200.0);
                                    cln.value((Callback<TableColumn.CellDataFeatures<ResumoRemessa, ResumoRemessa>, ObservableValue<ResumoRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().descricao));
                                }).build() /*Linha*/,
                                FormTableColumn.create(cln -> {
                                    cln.title("Qtde");
                                    cln.width(60);
                                    cln.value((Callback<TableColumn.CellDataFeatures<ResumoRemessa, ResumoRemessa>, ObservableValue<ResumoRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().qtde));
                                    cln.alignment(Pos.CENTER);
                                }).build() /*Qtde*/
                        );
                    });
                    final FormTableView<ResumoRemessa> tblFamilia = FormTableView.create(ResumoRemessa.class, table -> {
                        table.title("FAMÍLIAS");
                        table.items.set(resumoFamilia);
                        table.columns(
                                FormTableColumn.create(cln -> {
                                    cln.title("Família");
                                    cln.width(200.0);
                                    cln.value((Callback<TableColumn.CellDataFeatures<ResumoRemessa, ResumoRemessa>, ObservableValue<ResumoRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().descricao));
                                }).build() /*Linha*/,
                                FormTableColumn.create(cln -> {
                                    cln.title("Qtde");
                                    cln.width(60);
                                    cln.value((Callback<TableColumn.CellDataFeatures<ResumoRemessa, ResumoRemessa>, ObservableValue<ResumoRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().qtde));
                                    cln.alignment(Pos.CENTER);
                                }).build() /*Qtde*/
                        );
                    });
                    fragment.box.getChildren().add(FormBox.create(box -> {
                        box.horizontal();
                        box.add(tblModelagens.build(), tblLinhas.build(), tblFamilia.build());
                    }));
                });
            }
        });
    }
    
    /**
     * Exibição e limpeza dos fields com os dados da remessa (cabeçalho) que está sendo feita.
     */
    private void exibirDadosRemessa() {
        remessaField.value.set(String.valueOf(remessaEmColeta.getRemessa()));
        clienteField.value.set(remessaEmColeta.getCodcli().toString());
        totalRemessa.set(remessaEmColeta.getPedidos().stream().mapToInt(pedido -> pedido.getItens().stream().mapToInt(item -> item.getGrade().stream().mapToInt(SdGradeItemPedRem::getQtde).sum()).sum()).sum());
        observacaoRemessaField.value.set(remessaEmColeta.getObservacao());
    }
    private void limparDadosRemessa() {
        remessaField.clear();
        clienteField.clear();
        totalRemessa.set(0);
        lidoRemessa.set(0);
        observacaoRemessaField.clear();
    }
    
    /**
     * exibição e limpeza dos fields com os dados da caixa aberta e caixas
     */
    private void exibirDadosCaixaAberta() {
        caixaField.value.set(String.valueOf(caixaAberta.getNumero()));
        barraLeituraField.requestFocus();
    }
    private void limparDadosCaixaAberta() {
        caixaField.clear();
    }
    
    /**
     * exibição e limpeza das caixas da remessa
     */
    private void exibirDadosCaixaRemessa(SdCaixaRemessa caixa) {
        boxCaixasRemessa.add(FormBadges.create(bg -> {
            bg.position(Pos.TOP_RIGHT, -3.0);
            bg.text(lb -> {
                lb.boldText();
                lb.sizeText(14);
                lb.addStyle("info");
                lb.borderRadius(50);
                lb.value.bind(caixa.qtdeProperty().asString());
            });
            bg.badgedNode(FormBadges.create(bgCont -> {
                bgCont.position(Pos.BOTTOM_LEFT, -3.0);
                bgCont.badgedNode(FormOverlap.create(ovl -> {
                    ovl.add(ImageUtils.getIcon(caixa.getFechada() ? ImageUtils.Icon.CAIXA : ImageUtils.Icon.CAIXA_ABERTA, ImageUtils.IconSize._64));
                    ovl.add(FormLabel.create(lb -> {
                        lb.value.set(String.valueOf(caixa.getNumero()));
                        lb.mouseClicked(event -> abrirCaixa(caixa));
                    }));
                }));
                bgCont.text(lb -> {
                    lb.boldText();
                    lb.sizeText(14);
                    lb.addStyle("danger");
                    lb.fontColor("#FFF");
                    lb.borderRadius(50);
                    lb.value.set("x");
                    lb.mouseClicked(evt -> excluirCaixa(caixa));
                });
            }));
        }));
    }
    private void limparDadosCaixaRemessa() {
        boxCaixasRemessa.clear();
    }
    private void carregarNumeroRemessa() {
        new Fragment().show(fragment -> {
            fragment.title("Carregar Remessa para Leitura");
            fragment.size(300.0, 150.0);
            
            final FormFieldText fieldRemessaLeitura = FormFieldText.create(field -> {
                field.title("Remessa");
                field.alignment(Pos.CENTER);
                field.addStyle("lg");
                field.expanded();
                field.mask(FormFieldText.Mask.INTEGER);
            });
            fragment.box.getChildren().add(fieldRemessaLeitura.build());
            fragment.buttonsBox.getChildren().add(FormButton.create(btn -> {
                btn.title("Carregar");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                btn.addStyle("success");
                btn.defaultButton(true);
                btn.setAction(evt -> {
                    /**
                     * task: carregarRemessa(remessa) * return: exibirRemessa()
                     */
                    AtomicReference<Exception> excReturn = new AtomicReference<>();
                    new RunAsyncWithOverlay(this).exec(task -> {
                        try {
                            carregarRemessa(fieldRemessaLeitura.value.get());
                            return ReturnAsync.OK.value;
                        } catch (BreakException ex) {
                            excReturn.set(ex);
                            return ReturnAsync.NOT_FOUND.value;
                        }
                    }).addTaskEndNotification(taskReturn -> {
                        if (taskReturn.equals(ReturnAsync.OK.value)) {
                            if (remessaEmColeta.getStatus() == null) {
                                MessageBox.create(message -> {
                                    message.message("Essa remessa está sem um STATUS definido, verifique com o atendimento para atualizar o status da remessa.");
                                    message.type(MessageBox.TypeMessageBox.SIREN);
                                    message.showFullScreen();
                                });
                            } else {
                                if (remessaEmColeta.getPedidos().stream()
                                        .anyMatch(pedido -> pedido.getItens().stream()
                                                .filter(item -> !item.getStatusitem().equals("X"))
                                                .anyMatch(item -> item.getGrade().stream()
                                                        .filter(grade -> !grade.getStatusitem().equals("X"))
                                                        .anyMatch(grade -> grade.getBarra28() == null)))) {
                                    MessageBox.create(message -> {
                                        message.message("Existem produtos nesta remessa com problemas de cadastro, " +
                                                "verifique se o produto está com a BARRA28 cadastrada no estoque.");
                                        message.type(MessageBox.TypeMessageBox.ERROR);
                                        message.showFullScreen();
                                    });
                                }
                                exibirRemessa();
                            }
                            fragment.close();
                        } else if (taskReturn.equals(ReturnAsync.NOT_FOUND.value)) {
                            MessageBox.create(message -> {
                                message.message(excReturn.get().getMessage());
                                message.type(MessageBox.TypeMessageBox.ALERT);
                                message.showFullScreen();
                            });

                            remessaEmColeta = null;
                            fieldRemessaLeitura.clear();
                            fieldRemessaLeitura.requestFocus();
                        }
                    });
                });
            }));
            fieldRemessaLeitura.requestFocus();
        });
    }
    
    @Override
    public void closeWindow() {
        // testa se existe uma remessa aberta porém sem caixa, aberta para o coletor
        if (emColeta.get() && caixasRemessa.size() == 0) {
            // caso existe uma remessa aberta sem caixa, libera essa remessa para outro coletar
            remessaEmColeta.setColetor(null);
            remessaEmColeta.setDtIniColeta(null);
            remessaEmColeta.setStatus(statusEmRemessa);
            new FluentDao().merge(remessaEmColeta);
        }
    }
    
    private class ResumoRemessa {
        
        String descricao;
        Integer qtde;
        
        public ResumoRemessa(String descricao, Integer qtde) {
            this.descricao = descricao;
            this.qtde = qtde;
        }
    }
}
