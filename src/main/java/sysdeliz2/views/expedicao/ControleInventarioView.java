package sysdeliz2.views.expedicao;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.stage.Popup;
import javafx.util.Callback;
import sysdeliz2.controllers.views.expedicao.ControleInventarioController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.Marca;
import sysdeliz2.models.sysdeliz.Inventario.*;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Cor;
import sysdeliz2.models.ti.PaIten;
import sysdeliz2.models.view.VSdDadosProduto;
import sysdeliz2.models.view.VSdLocalExpedicao;
import sysdeliz2.models.view.VSdProdutoInventario;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.SortUtils;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.awt.*;
import java.time.LocalDate;
import java.util.List;
import java.util.*;
import java.util.stream.Collectors;

public class ControleInventarioView extends ControleInventarioController {

    // <editor-fold defaultstate="collapsed" desc="Bean">
    ListProperty<SdInventario> inventariosBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    ListProperty<VSdProdutoInventario> produtosToAddBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    ListProperty<SdItemInventario> itensInventarioBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Table">
    private final FormTableView<SdInventario> tblInventarios = FormTableView.create(SdInventario.class, table -> {
        table.title("Inventários");
        table.items.bind(inventariosBean);
        table.expanded();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Ações");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(120);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdInventario, SdInventario>, ObservableValue<SdInventario>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdInventario, SdInventario>() {
                            final HBox boxButtonsRow = new HBox(4);

                            final Button btnVizualizar = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ABRIR, ImageUtils.IconSize._16));
                                btn.tooltip("Vizualizar Inventário");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("primary");
                            });
                            final Button btnEditar = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EDIT, ImageUtils.IconSize._16));
                                btn.tooltip("Editar Inventário");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("warning");
                            });
                            final Button btnExcluir = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                btn.tooltip("Excluir Inventário");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("danger");
                            });

                            @Override
                            protected void updateItem(SdInventario item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {

                                    btnVizualizar.setOnAction(evt -> {
                                        visualizarInventario(item);
                                    });
                                    btnEditar.setOnAction(evt -> {
                                        editarInventario(item);
                                    });
                                    btnExcluir.setOnAction(evt -> {
                                        excluirInvantario(item);
                                    });

                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnVizualizar, btnEditar, btnExcluir);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdInventario, SdInventario>, ObservableValue<SdInventario>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Dt. Criação");
                    cln.width(120);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdInventario, SdInventario>, ObservableValue<SdInventario>>) param -> new ReadOnlyObjectWrapper(StringUtils.toShortDateFormat(param.getValue().getDtcriacao())));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Dt Leitura");
                    cln.width(120);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdInventario, SdInventario>, ObservableValue<SdInventario>>) param -> new ReadOnlyObjectWrapper(StringUtils.toShortDateFormat(param.getValue().getDtleitura())));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Qtde Itens");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdInventario, SdInventario>, ObservableValue<SdInventario>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtdeprodutos()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Status");
                    cln.width(170);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdInventario, SdInventario>, ObservableValue<SdInventario>>) param -> new ReadOnlyObjectWrapper(param.getValue().getStatus().getStatus()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Acertos");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdInventario, SdInventario>, ObservableValue<SdInventario>>) param -> new ReadOnlyObjectWrapper(param.getValue().getAcertos()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Erros");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdInventario, SdInventario>, ObservableValue<SdInventario>>) param -> new ReadOnlyObjectWrapper(param.getValue().getErros()));
                }).build() /*Código*/
        );
    });

    private final FormTableView<SdItemInventario> tblProdutosInventario = FormTableView.create(SdItemInventario.class, table -> {
        table.title("Referências");
        table.expanded();
        table.items.bind(itensInventarioBean);
        table.factoryRow(param -> new TableRow<SdItemInventario>() {
            @Override
            protected void updateItem(SdItemInventario item, boolean empty) {
                super.updateItem(item, empty);
                clear();
                if (item != null && !empty) {
                    if (item.getStatus().getCodigo() == 1) getStyleClass().add("table-row-danger");
                    else if (item.getStatus().getCodigo() == 2) getStyleClass().add("table-row-info");
                    else if (item.getStatus().getCodigo() == 3) getStyleClass().add("table-row-warning");
                    else if (item.getStatus().getCodigo() == 4) getStyleClass().add("table-row-primary");
                    else if (item.getStatus().getCodigo() == 5) getStyleClass().add("table-row-success");
                    else if (item.getStatus().getCodigo() == 6) getStyleClass().add("table-row-danger");
                }
            }

            private void clear() {
                getStyleClass().removeAll("table-row-danger", "table-row-info", "table-row-primary", "table-row-success", "table-row-warning");
            }
        });
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(90);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemInventario, SdItemInventario>, ObservableValue<SdItemInventario>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getCodigo()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(200);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemInventario, SdItemInventario>, ObservableValue<SdItemInventario>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getDescricao()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Cor");
                    cln.width(70);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemInventario, SdItemInventario>, ObservableValue<SdItemInventario>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getCor()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Coleção");
                    cln.width(70);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemInventario, SdItemInventario>, ObservableValue<SdItemInventario>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getColecao()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Qtde");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemInventario, SdItemInventario>, ObservableValue<SdItemInventario>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getEstoque()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Status");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemInventario, SdItemInventario>, ObservableValue<SdItemInventario>>) param -> new ReadOnlyObjectWrapper(param.getValue().getStatus().getStatus()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Acertos");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemInventario, SdItemInventario>, ObservableValue<SdItemInventario>>) param -> new ReadOnlyObjectWrapper(param.getValue().getAcertos()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Erros");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemInventario, SdItemInventario>, ObservableValue<SdItemInventario>>) param -> new ReadOnlyObjectWrapper(param.getValue().getErros()));
                }).build() /*Código*/
        );
    });

    private final FormTableView<SdItemInventario> tblProdutosInventarioManutencao = FormTableView.create(SdItemInventario.class, table -> {
        table.title("Produtos");
        table.expanded();
        table.items.bind(itensInventarioBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._24));
                    cln.width(80);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemInventario, SdItemInventario>, ObservableValue<SdItemInventario>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdItemInventario, SdItemInventario>() {
                            final HBox boxButtonsRow = new HBox(1);

                            final Button btnRemoverProdutoInventario = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                btn.tooltip("Remover Produto");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("danger");
                                btn.disable.bind(navegation.inEdition.not());
                            });

                            final Button btnVisualizarGrade = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.VISUALIZAR_PEDIDO, ImageUtils.IconSize._16));
                                btn.tooltip("Visualizar Grade");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("primary");
                                btn.disable.bind(navegation.inEdition.not());
                            });

                            @Override
                            protected void updateItem(SdItemInventario item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    boxButtonsRow.setAlignment(Pos.CENTER);
                                    btnRemoverProdutoInventario.setOnAction(evt -> {
                                        removerProdutoInventario(item);
                                    });
                                    btnVisualizarGrade.setOnAction(evt -> {
                                        vizualizarGrade(item.getCodigo());
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnRemoverProdutoInventario, btnVisualizarGrade);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemInventario, SdItemInventario>, ObservableValue<SdItemInventario>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getCodigo()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(200);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemInventario, SdItemInventario>, ObservableValue<SdItemInventario>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getDescricao()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Cor");
                    cln.width(70);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemInventario, SdItemInventario>, ObservableValue<SdItemInventario>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getCor()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Coleção");
                    cln.width(70);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemInventario, SdItemInventario>, ObservableValue<SdItemInventario>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getColecao()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Qtde");
                    cln.width(70);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemInventario, SdItemInventario>, ObservableValue<SdItemInventario>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getEstoque()));
                }).build() /*Código*/
        );
    });

    private final FormTableView<VSdProdutoInventario> tblProdutosToAdd = FormTableView.create(VSdProdutoInventario.class, table -> {
        table.title("Produtos");
        table.items.bind(produtosToAddBean);
        table.expanded();
        table.factoryRow(param -> new TableRow<VSdProdutoInventario>() {
            @Override
            protected void updateItem(VSdProdutoInventario item, boolean empty) {
                super.updateItem(item, empty);
                clear();
                if (item != null && !empty) {
                    if (item.getStatus().getCodigo() == 1) getStyleClass().add("table-row-danger");
                    else if (item.getStatus().getCodigo() == 2) getStyleClass().add("table-row-info");
                    else if (item.getStatus().getCodigo() == 3) getStyleClass().add("table-row-warning");
                    else if (item.getStatus().getCodigo() == 4) getStyleClass().add("table-row-primary");
                    else if (item.getStatus().getCodigo() == 5) getStyleClass().add("table-row-success");
                    else if (item.getStatus().getCodigo() == 6) getStyleClass().add("table-row-danger");
                }
            }

            private void clear() {
                getStyleClass().removeAll("table-row-danger", "table-row-info", "table-row-primary", "table-row-success", "table-row-warning");
            }
        });
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._24));
                    cln.width(80);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdProdutoInventario, VSdProdutoInventario>, ObservableValue<VSdProdutoInventario>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<VSdProdutoInventario, VSdProdutoInventario>() {
                            final HBox boxButtonsRow = new HBox(1);

                            final Button btnAdicionarProdutoInventario = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._16));
                                btn.tooltip("Adicionar Produto");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("success");
                                btn.disable.bind(navegation.inEdition.not());
                            });

                            final Button btnVisualizarGrade = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.VISUALIZAR_PEDIDO, ImageUtils.IconSize._16));
                                btn.tooltip("Visualizar Grade");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("primary");
                                btn.disable.bind(navegation.inEdition.not());
                            });

                            @Override
                            protected void updateItem(VSdProdutoInventario item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    boxButtonsRow.setAlignment(Pos.CENTER);
                                    btnAdicionarProdutoInventario.setOnAction(evt -> {
                                        adicionarProduto(item);
                                    });

                                    btnVisualizarGrade.setOnAction(evt -> {
                                        vizualizarGrade(item);
                                    });

                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnAdicionarProdutoInventario, btnVisualizarGrade);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdProdutoInventario, VSdProdutoInventario>, ObservableValue<VSdProdutoInventario>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(300);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdProdutoInventario, VSdProdutoInventario>, ObservableValue<VSdProdutoInventario>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDescricao()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Cor");
                    cln.width(70);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdProdutoInventario, VSdProdutoInventario>, ObservableValue<VSdProdutoInventario>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCor()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Coleção");
                    cln.width(70);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdProdutoInventario, VSdProdutoInventario>, ObservableValue<VSdProdutoInventario>>) param -> new ReadOnlyObjectWrapper(param.getValue().getColecao()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Qtde");
                    cln.width(70);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdProdutoInventario, VSdProdutoInventario>, ObservableValue<VSdProdutoInventario>>) param -> new ReadOnlyObjectWrapper(param.getValue().getEstoque()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Status");
                    cln.width(220);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdProdutoInventario, VSdProdutoInventario>, ObservableValue<VSdProdutoInventario>>) param -> new ReadOnlyObjectWrapper(param.getValue().getStatus().getStatus()));
                }).build() /*Código*/
        );
    });

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Vbox">
    private final VBox listagemTab = (VBox) super.tabs.getTabs().get(0).getContent();
    private final VBox manutencaoTab = (VBox) super.tabs.getTabs().get(1).getContent();
    private final VBox vizualizacaoTab = (VBox) super.tabs.getTabs().get(2).getContent();
    private final FormNavegation<SdInventario> navegation = FormNavegation.create(nav -> {
        nav.withNavegation(tblInventarios);
        nav.withActions(true, true, true);
        nav.withReturn(evt -> super.tabs.getSelectionModel().select(0));
        nav.btnAddRegister(evt -> novoInventario());
        nav.btnDeleteRegister(evt -> excluirInvantario((SdInventario) nav.selectedItem.get()));
        nav.btnEditRegister(evt -> {
            editarInventario((SdInventario) nav.selectedItem.get());
        });
        nav.btnSave(evt -> {
            salvarInventario();
        });
        nav.btnCancel(evt -> cancelarInventario());
        nav.selectedItem.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                carregarInventario((SdInventario) newValue);
            }
        });
    });

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Filter">
    private final FormFieldMultipleFind<SdInventario> codInventarioFilter = FormFieldMultipleFind.create(SdInventario.class, field -> {
        field.title("Código");
        field.width(200);
    });

    private final FormFieldMultipleFind<VSdDadosProduto> produtoFilter = FormFieldMultipleFind.create(VSdDadosProduto.class, field -> {
        field.title("Produto");
        field.width(200);
    });

    private final FormFieldMultipleFind<Colecao> colecaoFilter = FormFieldMultipleFind.create(Colecao.class, field -> {
        field.title("Coleção");
        field.width(200);
    });

    private final FormFieldMultipleFind<Marca> marcaFilter = FormFieldMultipleFind.create(Marca.class, field -> {
        field.title("Marca");
        field.width(200);
    });

    private final FormFieldDate dataCriacaoInicioFilter = FormFieldDate.create(field -> {
        field.withoutTitle();
        field.width(150);
        field.value.set(LocalDate.now().minusWeeks(1));
    });

    private final FormFieldDate dataCriacaoFimFilter = FormFieldDate.create(field -> {
        field.withoutTitle();
        field.width(150);
        field.value.set(LocalDate.now().plusWeeks(1));
    });

    private final FormFieldDate dataLeituraInicioFilter = FormFieldDate.create(field -> {
        field.withoutTitle();
        field.width(150);
        field.value.set(LocalDate.now().minusWeeks(1));
    });

    private final FormFieldDate dataLeituraFimFilter = FormFieldDate.create(field -> {
        field.withoutTitle();
        field.width(150);
        field.value.set(LocalDate.now().plusWeeks(1));
    });

    private final FormFieldMultipleFind<VSdDadosProduto> codigoProdutoFilter = FormFieldMultipleFind.create(VSdDadosProduto.class, field -> {
        field.title("Código");
        field.width(220);
        field.toUpper();
    });

    private final FormFieldMultipleFind<Cor> corProdutoFilter = FormFieldMultipleFind.create(Cor.class, field -> {
        field.title("Cor");
        field.width(220);
        field.toUpper();
    });

    private final FormFieldMultipleFind<Colecao> colecaoProdutoFilter = FormFieldMultipleFind.create(Colecao.class, field -> {
        field.title("Coleção");
        field.width(220);
        field.toUpper();
    });

    private final FormFieldMultipleFind<Marca> marcaProdutoFilter = FormFieldMultipleFind.create(Marca.class, field -> {
        field.title("Marca");
        field.width(220);
        field.toUpper();
    });

    private final FormFieldToggle tsDataPedido = FormFieldToggle.create(fft -> {
        fft.value.set(true);
        fft.editable.bind(navegation.inEdition);
        fft.withoutTitle();
    });

    private final FormFieldDate dataPedidoInicioFilter = FormFieldDate.create(field -> {
        field.withoutTitle();
        field.width(130);
        field.value.set(LocalDate.now().minusWeeks(1));
        field.editable.bind(tsDataPedido.value);
    });

    private final FormFieldDate dataPedidoFimFilter = FormFieldDate.create(field -> {
        field.withoutTitle();
        field.width(130);
        field.value.set(LocalDate.now().plusWeeks(1));
        field.editable.bind(tsDataPedido.value);
    });

    private final FormFieldToggle tsQtdeEstoque = FormFieldToggle.create(fft -> {
        fft.value.set(true);
        fft.editable.bind(navegation.inEdition);
        fft.withoutTitle();
    });

    private final FormFieldText qtdeEstqoueMinFilter = FormFieldText.create(field -> {
        field.withoutTitle();
        field.width(100);
        field.decimalField(5);
        field.value.setValue("0");
        field.editable.bind(tsQtdeEstoque.value);
    });

    private final FormFieldText qtdeEstqoueMaxFilter = FormFieldText.create(field -> {
        field.withoutTitle();
        field.width(100);
        field.decimalField(5);
        field.value.setValue("100");
        field.editable.bind(tsQtdeEstoque.value);
    });

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Fields">
    private final FormFieldText codigoInventarioField = FormFieldText.create(field -> {
        field.title("Código");
        field.editable.set(false);
        field.width(250);
    });

    private final FormFieldText acertosField = FormFieldText.create(field -> {
        field.title("Acertos");
        field.editable.set(false);
    });

    private final FormFieldText errosField = FormFieldText.create(field -> {
        field.title("Erros");
        field.editable.set(false);
    });

    private final FormFieldText dataCriacaoField = FormFieldText.create(field -> {
        field.title("Dt Leitura");
        field.editable.set(false);
        field.width(250);
    });

    private final FormFieldText dataLeituraField = FormFieldText.create(field -> {
        field.title("Dt Criação");
        field.editable.set(false);
        field.width(250);
    });

    private final FormFieldText idInventarioField = FormFieldText.create(field -> {
        field.title("Codigo");
        field.editable.set(false);
        field.width(250);
    });

    private final FormFieldText dtCriacaoInventarioField = FormFieldText.create(field -> {
        field.title("Dt. Criação");
        field.editable.set(false);
        field.width(250);
    });

    private final FormFieldText dtLeituraInventarioField = FormFieldText.create(field -> {
        field.title("Dt. Leitura");
        field.editable.set(false);
        field.width(250);
    });

    private final FormFieldText statusInventarioField = FormFieldText.create(field -> {
        field.title("Status");
        field.editable.set(false);
        field.width(250);
    });

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Buttons">

    // </editor-fold>

    public ControleInventarioView() {
        super("Controle de Inventário", ImageUtils.getImage(ImageUtils.Icon.ALMOXARIFADO), new String[]{"Listagem", "Manutenção", "Estoque"});
        initListagem();
        initManutencao();
        super.tabs.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (super.tabs.getTabs().indexOf(newValue) == 2 && vizualizacaoTab.getChildren().size() == 0) {
                initVizualizacao();
            }
        });
    }

    private void initListagem() {
        listagemTab.getChildren().add(FormBox.create(principal -> {
            principal.horizontal();
            principal.expanded();
            principal.add(FormBox.create(coluna1 -> {
                coluna1.vertical();
                coluna1.expanded();
                coluna1.maxWidthSize(1100);
                coluna1.add(FormBox.create(filterBox -> {
                    filterBox.vertical();
                    filterBox.add(FormTitledPane.create(filter -> {
                        filter.filter();
                        filter.add(FormBox.create(boxFilter -> {
                            boxFilter.horizontal();
                            boxFilter.expanded();
                            boxFilter.add(FormBox.create(col1 -> {
                                col1.vertical();
                                col1.add(codInventarioFilter.build(), produtoFilter.build());
                            }));
                            boxFilter.add(new Separator(Orientation.VERTICAL));
                            boxFilter.add(FormBox.create(col2 -> {
                                col2.vertical();
                                col2.add(colecaoFilter.build(), marcaFilter.build());
                            }));
                            boxFilter.add(new Separator(Orientation.VERTICAL));
                            boxFilter.add(FormBox.create(col3 -> {
                                col3.vertical();
                                col3.add(FormBox.create(boxDataCriacao -> {
                                    boxDataCriacao.horizontal();
                                    boxDataCriacao.title("Data de Criação");
                                    boxDataCriacao.add(FormLabel.create(lb -> {
                                        lb.setText("De: ");
                                    }));
                                    boxDataCriacao.add(dataCriacaoInicioFilter.build());
                                    boxDataCriacao.add(FormLabel.create(lb -> {
                                        lb.setText("Até: ");
                                    }));
                                    boxDataCriacao.add(dataCriacaoFimFilter.build());
                                }));
                                col3.add(FormBox.create(boxDataLeitura -> {
                                    boxDataLeitura.horizontal();
                                    boxDataLeitura.title("Data de Leitura");
                                    boxDataLeitura.add(FormLabel.create(lb -> {
                                        lb.setText("De: ");
                                    }));
                                    boxDataLeitura.add(dataLeituraInicioFilter.build());
                                    boxDataLeitura.add(FormLabel.create(lb -> {
                                        lb.setText("Até: ");
                                    }));
                                    boxDataLeitura.add(dataLeituraFimFilter.build());
                                }));
                            }));
                        }));
                        filter.find.setOnAction(evt -> {
                            new RunAsyncWithOverlay(this).exec(task -> {
                                buscarInventarios(
                                        codInventarioFilter.objectValues.isNull().get() ? new String[]{} : codInventarioFilter.objectValues.stream().map(SdInventario::getId).toArray(),
                                        produtoFilter.objectValues.isNull().get() ? new String[]{} : produtoFilter.objectValues.stream().map(VSdDadosProduto::getCodigo).toArray(),
                                        colecaoFilter.objectValues.isNull().get() ? new String[]{} : colecaoFilter.objectValues.stream().map(Colecao::getCodigo).toArray(),
                                        marcaFilter.objectValues.isNull().get() ? new String[]{} : marcaFilter.objectValues.stream().map(Marca::getCodigo).toArray(),
                                        dataCriacaoInicioFilter.value.getValue(),
                                        dataCriacaoFimFilter.value.getValue(),
                                        dataLeituraInicioFilter.value.getValue(),
                                        dataLeituraFimFilter.value.getValue()
                                );
                                return ReturnAsync.OK.value;
                            }).addTaskEndNotification(taskReturn -> {
                                if (taskReturn.equals(ReturnAsync.OK.value)) {
                                    inventariosBean.set(FXCollections.observableArrayList(inventarios));
                                }
                            });
                        });
                        filter.clean.setOnAction(evt -> {
                            codInventarioFilter.clear();
                            produtoFilter.clear();
                            colecaoFilter.clear();
                            dataCriacaoInicioFilter.value.setValue(LocalDate.now().minusWeeks(1));
                            dataCriacaoFimFilter.value.setValue(LocalDate.now().plusWeeks(1));
                            dataLeituraInicioFilter.value.setValue(LocalDate.now().minusWeeks(1));
                            dataLeituraFimFilter.value.setValue(LocalDate.now().plusWeeks(1));
                        });
                    }));
                }));
                coluna1.add(tblInventarios.build());
            }));
            principal.add(FormBox.create(coluna2 -> {
                coluna2.vertical();
                coluna2.expanded();
                coluna2.add(FormBox.create(boxUltInventario -> {
                    boxUltInventario.vertical();
                    boxUltInventario.add(FormBox.create(lin1 -> {
                        lin1.horizontal();
                        lin1.add(codigoInventarioField.build(), acertosField.build(), errosField.build());
                    }));
                    boxUltInventario.add(FormBox.create(lin2 -> {
                        lin2.horizontal();
                        lin2.add(dataCriacaoField.build(), dataLeituraField.build());
                    }));
                }));
                coluna2.add(FormBox.create(boxTblProdutos -> {
                    boxTblProdutos.vertical();
                    boxTblProdutos.expanded();
                    boxTblProdutos.add(tblProdutosInventario.build());
                }));
            }));
        }));
    }

    private void initManutencao() {
        manutencaoTab.getChildren().add(navegation);
        manutencaoTab.getChildren().add(FormBox.create(principal -> {
            principal.horizontal();
            principal.expanded();
            principal.add(FormBox.create(coluna1 -> {
                coluna1.vertical();
                coluna1.expanded();
                coluna1.add(FormBox.create(boxResumoInventario -> {
                    boxResumoInventario.vertical();
                    boxResumoInventario.add(FormBox.create(l1 -> {
                        l1.horizontal();
                        l1.add(idInventarioField.build(), statusInventarioField.build());
                    }));
                    boxResumoInventario.add(FormBox.create(l2 -> {
                        l2.horizontal();
                        l2.add(dtCriacaoInventarioField.build(), dtLeituraInventarioField.build());
                    }));
                }));
                coluna1.add(FormBox.create(boxTblProdutos -> {
                    boxTblProdutos.vertical();
                    boxTblProdutos.expanded();
                    boxTblProdutos.add(tblProdutosInventarioManutencao.build());
                }));
            }));
            principal.add(FormBox.create(coluna2 -> {
                coluna2.vertical();
                coluna2.expanded();
                coluna2.add(FormBox.create(boxFiltrarProdutos -> {
                    boxFiltrarProdutos.vertical();
                    boxFiltrarProdutos.add(FormTitledPane.create(filter -> {
                        filter.filter();
                        filter.add(FormBox.create(boxFilters -> {
                            boxFilters.horizontal();
                            boxFilters.add(FormBox.create(boxCodigoMarca -> {
                                boxCodigoMarca.vertical();
                                boxCodigoMarca.add(codigoProdutoFilter.build(), marcaProdutoFilter.build());
                            }));
                            boxFilters.add(new Separator(Orientation.VERTICAL));
                            boxFilters.add(FormBox.create(boxColecaoCor -> {
                                boxColecaoCor.vertical();
                                boxColecaoCor.add(colecaoProdutoFilter.build(), corProdutoFilter.build());
                            }));
                            boxFilters.add(new Separator(Orientation.VERTICAL));
                            boxFilters.add(FormBox.create(boxColecaoCor -> {
                                boxColecaoCor.vertical();
                                boxColecaoCor.add(FormBox.create(l1 -> {
                                    l1.horizontal();
                                    l1.title("Qtde Estoque");
                                    l1.add(tsQtdeEstoque.build());
                                    l1.add(FormLabel.create(lbl -> lbl.setText("De: ")));
                                    l1.add(qtdeEstqoueMinFilter.build());
                                    l1.add(FormLabel.create(lbl -> lbl.setText("Até: ")));
                                    l1.add(qtdeEstqoueMaxFilter.build());
                                }));
                                boxColecaoCor.add(FormBox.create(l2 -> {
                                    l2.horizontal();
                                    l2.title("Dt. Pedido");
                                    l2.add(tsDataPedido.build());
                                    l2.add(FormLabel.create(lbl -> lbl.setText("De: ")));
                                    l2.add(dataPedidoInicioFilter.build());
                                    l2.add(FormLabel.create(lbl -> lbl.setText("Até: ")));
                                    l2.add(dataPedidoFimFilter.build());
                                }));
                            }));
                        }));
                        filter.disabled.bind(navegation.inEdition.not());
                        filter.find.setOnAction(evt -> {
                            new RunAsyncWithOverlay(this).exec(task -> {
                                buscarProdutos(
                                        codigoProdutoFilter.objectValues.isNull().get() ? new String[]{} : codigoProdutoFilter.objectValues.stream().map(VSdDadosProduto::getCodigo).toArray(),
                                        corProdutoFilter.objectValues.isNull().get() ? new String[]{} : corProdutoFilter.objectValues.stream().map(Cor::getCor).toArray(),
                                        colecaoProdutoFilter.objectValues.isNull().get() ? new String[]{} : colecaoProdutoFilter.objectValues.stream().map(Colecao::getCodigo).toArray(),
                                        marcaProdutoFilter.objectValues.isNull().get() ? new String[]{} : marcaProdutoFilter.objectValues.stream().map(Marca::getCodigo).toArray(),
                                        dataPedidoInicioFilter.value.getValue(),
                                        dataPedidoFimFilter.value.getValue(),
                                        qtdeEstqoueMinFilter.value.getValue().isEmpty() ? 0 : Integer.parseInt(qtdeEstqoueMinFilter.value.getValue()),
                                        qtdeEstqoueMaxFilter.value.getValue().isEmpty() ? 0 : Integer.parseInt(qtdeEstqoueMaxFilter.value.getValue()),
                                        tsQtdeEstoque.value.getValue(),
                                        tsDataPedido.value.getValue()

                                );
                                return ReturnAsync.OK.value;
                            }).addTaskEndNotification(taskReturn -> {
                                if (taskReturn.equals(ReturnAsync.OK.value)) {
                                    produtosToAddBean.set(FXCollections.observableArrayList(produtos));
                                }
                            });

                        });
                        filter.clean.setOnAction(evt -> {
                            codigoProdutoFilter.clear();
                            corProdutoFilter.clear();
                            colecaoProdutoFilter.clear();
                            marcaProdutoFilter.clear();
                        });
                    }));
                }));
                coluna2.add(FormBox.create(boxTableGrade -> {
                    boxTableGrade.horizontal();
                    boxTableGrade.expanded();
                    boxTableGrade.add(FormBox.create(boxTabela -> {
                        boxTabela.vertical();
                        boxTabela.expanded();
                        boxTabela.add(tblProdutosToAdd.build());
                    }));
                }));
            }));
        }));
    }

    private void initVizualizacao() {
        new RunAsyncWithOverlay(this).exec(task -> ReturnAsync.OK.value).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                Map<String, Map<String, Map<String, List<VSdLocalExpedicao>>>> collect = ((List<VSdLocalExpedicao>) new FluentDao().selectFrom(VSdLocalExpedicao.class).get().resultList())
                        .stream().filter(it -> it.getLocal() != null && it.getRua() != null && it.getPosicao() != null && it.getLado() != null).collect(Collectors.groupingBy(VSdLocalExpedicao::getRua, Collectors.groupingBy(VSdLocalExpedicao::getPosicao, Collectors.groupingBy(VSdLocalExpedicao::getLado))));
                vizualizacaoTab.getChildren().add(
                        FormBox.create(principal -> {
                            principal.expanded();
                            principal.horizontal();
                            principal.add(FormBadges.create(bg -> {
                                bg.node(FormTitledPane.create(boxLegenda -> {
                                    List<SdStatusItemInventario> statusList = (List<SdStatusItemInventario>) new FluentDao().selectFrom(SdStatusItemInventario.class).get().orderBy("codigo", OrderType.ASC).resultList();
                                    boxLegenda.title("Legenda");
                                    for (SdStatusItemInventario status : statusList) {
                                        boxLegenda.add(FormBox.create(linha -> {
                                            linha.horizontal();
                                            linha.add(FormBox.create(boxColor -> {
                                                boxColor.horizontal();
                                                boxColor.width(15);
                                                boxColor.alignment(Pos.CENTER);
                                                boxColor.add(new Circle(7, Color.valueOf(status.getCor())));
                                            }));
                                            linha.add(FormBox.create(boxDesc -> {
                                                boxDesc.horizontal();
                                                boxDesc.add(FormLabel.create(txt -> txt.setText(status.getStatus())));
                                            }));
                                        }));
                                    }
                                    boxLegenda.add(FormBox.create(linha -> {
                                        linha.horizontal();
                                        linha.add(FormBox.create(boxColor -> {
                                            boxColor.horizontal();
                                            boxColor.width(15);
                                            boxColor.alignment(Pos.CENTER);
                                            boxColor.add(new Circle(7, Color.INDIANRED));
                                        }));
                                        linha.add(FormBox.create(boxDesc -> {
                                            boxDesc.horizontal();
                                            boxDesc.add(FormLabel.create(txt -> txt.setText("SEM PRODUTO")));
                                        }));
                                    }));
                                }));
                                bg.position(Pos.BOTTOM_RIGHT, 10.0);
                                bg.badgedNode(FormBox.create(boxPrinci -> {
                                    boxPrinci.expanded();
                                    boxPrinci.horizontal();
                                    boxPrinci.horizontalScroll();
                                    collect.forEach((rua, mapPosicao) -> boxPrinci.add(FormBox.create(ruaBox -> {
                                        ruaBox.vertical();
                                        ruaBox.title(rua);
                                        ruaBox.setPadding(new Insets(-2));
                                        boolean invertido = mapPosicao.entrySet().stream().findFirst().get().getValue().entrySet().stream().findFirst().get().getValue().get(0).getIndice() % 2 != 0;

                                        Map<String, Map<String, List<VSdLocalExpedicao>>> sortedPosicaoMap;
                                        if (invertido) {
                                            sortedPosicaoMap = mapPosicao.entrySet().stream().sorted(Map.Entry.comparingByKey(Comparator.reverseOrder())).collect(Collectors.toMap(
                                                    Map.Entry::getKey,
                                                    Map.Entry::getValue,
                                                    (oldValue, newValue) -> oldValue, LinkedHashMap::new
                                            ));
                                        } else {
                                            sortedPosicaoMap = mapPosicao.entrySet().stream().sorted(Map.Entry.comparingByKey()).collect(Collectors.toMap(
                                                    Map.Entry::getKey,
                                                    Map.Entry::getValue,
                                                    (oldValue, newValue) -> oldValue, LinkedHashMap::new
                                            ));
                                        }
                                        sortedPosicaoMap.forEach((posicao, mapLado) -> ruaBox.add(FormBox.create(boxPosicao -> {
                                            boxPosicao.horizontal();
                                            boxPosicao.setMaxHeight(37);
                                            boxPosicao.setPadding(new Insets(-2));
                                            if (!mapLado.containsKey("E")) {
                                                mapLado.put("E", new ArrayList<>(Collections.singletonList(new VSdLocalExpedicao("E", "S", "S COR"))));
                                            }
                                            if (!mapLado.containsKey("D")) {
                                                mapLado.put("D", new ArrayList<>(Collections.singletonList(new VSdLocalExpedicao("D", "S", "S COR"))));
                                            }

                                            Map<String, List<VSdLocalExpedicao>> sortedLadoMap;
                                            if (invertido) {
                                                sortedLadoMap = mapLado.entrySet().stream().sorted(Map.Entry.comparingByKey(Comparator.reverseOrder())).collect(Collectors.toMap(
                                                        Map.Entry::getKey,
                                                        Map.Entry::getValue,
                                                        (oldValue, newValue) -> oldValue, LinkedHashMap::new
                                                ));
                                            } else {
                                                sortedLadoMap = mapLado.entrySet().stream().sorted(Map.Entry.comparingByKey()).collect(Collectors.toMap(
                                                        Map.Entry::getKey,
                                                        Map.Entry::getValue,
                                                        (oldValue, newValue) -> oldValue, LinkedHashMap::new
                                                ));
                                            }

                                            sortedLadoMap.forEach((lado, list) -> boxPosicao.add(FormBox.create(boxLado -> {
                                                boxLado.width(35);
                                                boxLado.alignment(Pos.CENTER);
                                                boxLado.vertical();
                                                boxLado.border();
                                                boxLado.setPadding(new Insets(-2));

                                                Popup popup = new Popup();
                                                popup.getContent().add(FormBox.create(popupPrincipal -> {
                                                    popupPrincipal.vertical();
                                                    popupPrincipal.expanded();
                                                    popupPrincipal.setBackground(new Background(new BackgroundFill(getColorStatus(list.get(0)), CornerRadii.EMPTY, Insets.EMPTY)));
                                                    for (VSdLocalExpedicao produto : list) {
                                                        popupPrincipal.add(FormBox.create(boxPane -> {
                                                            boxPane.vertical();
                                                            boxPane.expanded();
                                                            boxPane.border();
                                                            boxPane.alignment(Pos.CENTER);
                                                            boxPane.add(FormLabel.create(lbl -> {
                                                                lbl.alignment(Pos.CENTER);
                                                                lbl.setFont(javafx.scene.text.Font.font("Verdana", 10));
                                                                lbl.setPadding(new Insets(-5));
                                                                lbl.value.setValue(rua + " - " + posicao + " - " + lado);
                                                            }));
                                                            boxPane.add(FormLabel.create(lbl -> {
                                                                lbl.alignment(Pos.CENTER);
                                                                lbl.setFont(javafx.scene.text.Font.font("Verdana", FontWeight.BOLD, 20));
                                                                lbl.setPadding(new Insets(-5));
                                                                lbl.value.setValue(produto.getProduto());
                                                            }));
                                                            boxPane.add(FormLabel.create(lbl -> {
                                                                lbl.alignment(Pos.CENTER);
                                                                lbl.setFont(javafx.scene.text.Font.font("Verdana", FontPosture.ITALIC, 10));
                                                                lbl.setPadding(new Insets(-5));
                                                                lbl.value.setValue(produto.getCor());
                                                            }));
                                                        }));
                                                    }
                                                }));

                                                boxLado.hoverProperty().addListener((observable, oldValue, newValue) -> {
                                                    Point mouseLocation = MouseInfo.getPointerInfo().getLocation();
                                                    popup.setX(mouseLocation.getX());
                                                    popup.setY(mouseLocation.getY());
                                                    if (newValue && !list.get(0).getProduto().equals("S"))
                                                        popup.show(boxLado, mouseLocation.getX(), mouseLocation.getY());
                                                    else popup.hide();
                                                });

                                                if (list.size() > 0) {
                                                    if (list.get(0).getProduto().equals("S PROD")) {
                                                        boxLado.setBackground(new Background(new BackgroundFill(Color.INDIANRED, CornerRadii.EMPTY, Insets.EMPTY)));
                                                    }
                                                    if (!list.get(0).getProduto().equals("S")) {
                                                        boxLado.add(FormLabel.create(lbl -> {
                                                            lbl.alignment(Pos.CENTER);
                                                            lbl.setFont(javafx.scene.text.Font.font("Verdana", FontWeight.BOLD, 8.3));
                                                            lbl.setPadding(new Insets(0, -5, 0, -5));
                                                            lbl.value.setValue(posicao + "-" + lado);
                                                        }));
                                                        boxLado.setBackground(new Background(new BackgroundFill(getColorStatus(list.get(0)), CornerRadii.EMPTY, Insets.EMPTY)));
                                                    }

                                                }
                                            })));
                                        })));
                                    })));
                                }));
                            }));

                        })
                );
            }
        });

    }

    private Paint getColorStatus(VSdLocalExpedicao vSdLocalExpedicao) {
        VSdProdutoInventario produto = new FluentDao().selectFrom(VSdProdutoInventario.class).where(it -> it
                .equal("codigo", vSdLocalExpedicao.getProduto())
                .equal("cor", vSdLocalExpedicao.getCor())
        ).singleResult();

        if (produto == null) {
            return Color.INDIANRED;
        }
        return Color.valueOf(produto.getStatus().getCor());

    }

    private void excluirInvantario(SdInventario inventario) {
        if (inventario != null) {
            if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                message.message("Tem certeza que deseja exluir?");
                message.showAndWait();
            }).value.get())) {
                if (navegation.selectedItem.isNotNull().get() && navegation.selectedItem.get().equals(inventario)) {
                    navegation.selectedItem.set(null);
                    limparCampos();
                }
            }
            inventariosBean.remove(inventario);
            new FluentDao().delete(inventario);
            MessageBox.create(message -> {
                message.message("Inventário Excluído com Sucesso");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            SysLogger.addSysDelizLog("Controle Inventario", TipoAcao.EXCLUIR, String.valueOf(inventario.getId()), "Inventário " + inventario.getId() + " excluído!");
        }

    }

    private void editarInventario(SdInventario item) {
        if (item.getStatus().getCodigo() == 3 || item.getStatus().getCodigo() == 4) {
            MessageBox.create(message -> {
                message.message("Inventário já concluído ou em Leitura, impossível editar.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }
        navegation.selectedItem.set(item);
        navegation.inEdition.set(true);
        super.tabs.getSelectionModel().select(1);
    }

    private void carregarInventario(SdInventario inventario) {
        itensInventarioBean.set(FXCollections.observableArrayList(inventario.getItens()));
        idInventarioField.value.set(inventario.getId() == null ? "" : String.valueOf(inventario.getId()));
        statusInventarioField.value.set(inventario.getStatus().getStatus());
        dtCriacaoInventarioField.value.set(StringUtils.toShortDateFormat(inventario.getDtcriacao()));
        dtLeituraInventarioField.value.set(inventario.getDtleitura() == null ? "" : StringUtils.toShortDateFormat(inventario.getDtleitura()));
    }

    private void cancelarInventario() {
        navegation.selectedItem.set(null);
        navegation.inEdition.set(false);
        limparCampos();
    }

    private void limparCampos() {
        navegation.selectedItem.set(null);
        itensInventarioBean.clear();
        idInventarioField.clear();
        codigoInventarioField.clear();
        statusInventarioField.clear();
        dtCriacaoInventarioField.clear();
        dtLeituraInventarioField.clear();
        codigoProdutoFilter.clear();
        marcaProdutoFilter.clear();
        colecaoProdutoFilter.clear();
        corProdutoFilter.clear();
        qtdeEstqoueMinFilter.value.setValue("0");
        qtdeEstqoueMaxFilter.value.setValue("100");
        dataPedidoInicioFilter.value.set(LocalDate.now().minusWeeks(1));
        dataPedidoFimFilter.value.set(LocalDate.now().plusWeeks(1));
        produtosToAddBean.clear();
    }

    private void salvarInventario() {
        navegation.inEdition.set(false);
        SdInventario inventario = navegation.selectedItem.get();
        for (SdItemInventario item : inventario.getItens()) {
            item.setStatus(new FluentDao().selectFrom(SdStatusItemInventario.class).where(it -> it.equal("codigo", 3)).singleResult());
            item.generateListGrade();
            item = new FluentDao().merge(item);
        }
        if (inventario.getDtleitura() == null) inventario.setDtleitura(LocalDate.now());
        inventario.setQtdeprodutos(inventario.getItens().size());
        inventario.setStatus(new FluentDao().selectFrom(SdStatusInventario.class).where(it -> it.equal("codigo", 2)).singleResult());
        inventario = new FluentDao().merge(inventario);
        carregarInventario(inventario);
        MessageBox.create(message -> {
            message.message("Inventário cadastrado com Sucesso!");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
        SysLogger.addSysDelizLog("Controle Inventario", TipoAcao.CADASTRAR, String.valueOf(inventario.getId()), "Inventário " + inventario.getId() + " cadastrado!");
    }

    private void novoInventario() {
        SdInventario inventario = new SdInventario();
        inventario.setStatus(new FluentDao().selectFrom(SdStatusInventario.class).where(it -> it.equal("codigo", 1)).singleResult());
        inventario = new FluentDao().merge(inventario);
        navegation.selectedItem.set(inventario);
        navegation.inEdition.set(true);

        carregarInventario(inventario);
    }

    private void adicionarProduto(VSdProdutoInventario item) {
        if (item.getStatus().getCodigo() != 1 && item.getStatus().getCodigo() == 6) {
            MessageBox.create(message -> {
                message.message("Item já pertence a um inventário. \nStatus do Item: " + item.getStatus().getStatus());
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }
        SdInventario inventario = navegation.selectedItem.get();
        SdItemInventario itemInventario = new SdItemInventario(inventario, item);
        itemInventario.setStatus(new FluentDao().selectFrom(SdStatusItemInventario.class).where(it -> it.equal("codigo", 2)).singleResult());
        itemInventario = new FluentDao().merge(itemInventario);
        itensInventarioBean.add(itemInventario);
        inventario.getItens().add(itemInventario);
        inventario = new FluentDao().merge(inventario);
        produtosToAddBean.remove(item);
        MessageBox.create(message -> {
            message.message("Produto adicionado com Sucesso!");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
        SysLogger.addSysDelizLog("Controle Inventario", TipoAcao.CADASTRAR, itemInventario.getCodigo().getCodigo(), "Produto " + itemInventario.getCodigo().getCodigo() + " adicionado ao inventário " + inventario.getId() + "!");
    }

    private void removerProdutoInventario(SdItemInventario item) {
        SdInventario inventario = navegation.selectedItem.get();
        inventario.getItens().remove(item);
        itensInventarioBean.remove(item);
        new FluentDao().delete(item);
        inventario = new FluentDao().merge(inventario);
        MessageBox.create(message -> {
            message.message("Produto removido com Sucesso!");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
        SysLogger.addSysDelizLog("Controle Inventario", TipoAcao.EXCLUIR, item.getCodigo().getCodigo(), "Produto " + item.getCodigo().getCodigo() + " removido do inventário " + inventario.getId() + "!");
    }

    private void vizualizarGrade(VSdProdutoInventario item) {
        new Fragment().show(frag -> {
            frag.size(300.0, 450.0);
            frag.title.setText("Grade Item");

            List<PaIten> itensList = (List<PaIten>) new FluentDao().selectFrom(PaIten.class)
                    .where(it -> it
                            .equal("id.codigo", item.getCodigo())
                            .equal("id.cor", item.getCor())
                            .and(eb -> eb
                                    .equal("id.deposito", "0005", TipoExpressao.AND, true)
                                    .equal("id.deposito", "0023", TipoExpressao.OR, true)
                            )
                    ).resultList();

            List<PaIten> listPaItem = new ArrayList<>();
            itensList.stream().collect(Collectors.groupingBy(it -> it.getId().getTam())).forEach((tam, list) -> {
                PaIten iten = list.get(0);
                listPaItem.add(new PaIten(list.stream().mapToInt(eb -> eb.getQuantidade().intValue()).sum(), tam, iten.getId().getCodigo(), iten.getId().getCor()));
            });
            listPaItem.sort((o1, o2) -> SortUtils.sortTamanhos(o1.getId().getTam(), o2.getId().getTam()));

            final FormTableView<PaIten> produtos = FormTableView.create(PaIten.class, table -> {
                table.items.set(FXCollections.observableArrayList(listPaItem));
                table.withoutHeader();
                table.columns(
                        FormTableColumn.create(cln -> {
                            cln.title("Tam");
                            cln.width(100);
                            cln.value((Callback<TableColumn.CellDataFeatures<PaIten, PaIten>, ObservableValue<PaIten>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                            cln.format(param -> new TableCell<PaIten, PaIten>() {
                                @Override
                                protected void updateItem(PaIten item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    setGraphic(null);
                                    if (item != null && !empty) {
                                        setText(item.getId().getTam());
                                        getStyleClass().add("lg");
                                    }
                                }
                            });
                        }).build(), /*Tam*/
                        FormTableColumn.create(cln -> {
                            cln.title("Qtde");
                            cln.width(100);
                            cln.value((Callback<TableColumn.CellDataFeatures<PaIten, PaIten>, ObservableValue<PaIten>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                            cln.format(param -> new TableCell<PaIten, PaIten>() {
                                @Override
                                protected void updateItem(PaIten item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    setGraphic(null);
                                    if (item != null && !empty) {
                                        setText(item.getQuantidade().toString());
                                        getStyleClass().add("lg");
                                    }
                                }
                            });
                        }).build() /*QTDE*/
                );
            });
            frag.box.getChildren().add(produtos.build());
        });
    }

    private void visualizarInventario(SdInventario item) {
        super.tabs.getSelectionModel().select(1);
        carregarInventario(item);
    }

}
