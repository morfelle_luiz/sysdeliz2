package sysdeliz2.views.expedicao;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import org.apache.log4j.Logger;
import sysdeliz2.controllers.views.expedicao.EntradaProdutosAcabadosController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.sysdeliz.EntradaPA.*;
import sysdeliz2.models.sysdeliz.SdColaborador;
import sysdeliz2.models.ti.Defeito;
import sysdeliz2.models.ti.Entidade;
import sysdeliz2.models.ti.PaIten;
import sysdeliz2.models.ti.Produto;
import sysdeliz2.models.view.*;
import sysdeliz2.utils.FuncoesEntradaPA;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.ReportUtils;
import sysdeliz2.utils.SortUtils;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.counting;
import static sysdeliz2.utils.SortUtils.distinctByKey;

public class EntradaProdutosAcabadosView extends EntradaProdutosAcabadosController {

    private static final Logger logger = Logger.getLogger(EntradaProdutosAcabadosView.class);

    //<editor-fold desc="Status">
    private final SdStatusLotePA STATUS_CRIADO = new FluentDao().selectFrom(SdStatusLotePA.class).where(it -> it.equal("codigo", 1)).singleResult();
    private final SdStatusLotePA STATUS_EM_LEITURA = new FluentDao().selectFrom(SdStatusLotePA.class).where(it -> it.equal("codigo", 2)).singleResult();
    private final SdStatusLotePA STATUS_ENCERRADO = new FluentDao().selectFrom(SdStatusLotePA.class).where(it -> it.equal("codigo", 3)).singleResult();
    private final SdStatusLotePA STATUS_EM_TRANSPORTE = new FluentDao().selectFrom(SdStatusLotePA.class).where(it -> it.equal("codigo", 4)).singleResult();
    private final SdStatusLotePA STATUS_EM_RECEBIMENTO = new FluentDao().selectFrom(SdStatusLotePA.class).where(it -> it.equal("codigo", 5)).singleResult();

    private final SdStatusItemLotePA STATUS_ITEM_ADICIONADO = new FluentDao().selectFrom(SdStatusItemLotePA.class).where(it -> it.equal("codigo", 1)).singleResult();
    private final SdStatusItemLotePA STATUS_ITEM_EM_EDICAO = new FluentDao().selectFrom(SdStatusItemLotePA.class).where(it -> it.equal("codigo", 2)).singleResult();
    private final SdStatusItemLotePA STATUS_ITEM_CONCLUIDA = new FluentDao().selectFrom(SdStatusItemLotePA.class).where(it -> it.equal("codigo", 3)).singleResult();
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Controle">
    private SdLotePa loteEscolhido = null;
    private SdItemLotePA ordemEscolhida = null;
    private SdCaixaPA caixaEscolhida = null;
    private SdBarraCaixaPA barraSelecionada = null;
    private Defeito defeitoEscolhido = null;
    private final String usuario;
    private List<VSdDadosOfOsPendenteSKU> listProdutosOrdem = new ArrayList<>();
    private List<SdStatusLotePA> listStatusLote = new ArrayList<>();
    private List<SdStatusItemLotePA> listStatusItemLote = new ArrayList<>();
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Boolean Control">
    private final BooleanProperty emAdicaoOps = new SimpleBooleanProperty(false);
    private final BooleanProperty isExcluirBarra = new SimpleBooleanProperty(false);
    private final BooleanProperty emLeituraItens = new SimpleBooleanProperty(false);
    private final BooleanProperty isLeituraSegunda = new SimpleBooleanProperty(false);
    private final BooleanProperty isConclusaoLeitura = new SimpleBooleanProperty(false);
    private final BooleanProperty isApenasLeitura = new SimpleBooleanProperty(false);
    private final BooleanProperty isLoteNulo = new SimpleBooleanProperty(true);
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Totais">
    private final IntegerProperty totalOp = new SimpleIntegerProperty(0);
    private final IntegerProperty saldoOp = new SimpleIntegerProperty(0);
    private final IntegerProperty lidoOP = new SimpleIntegerProperty(0);
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Bean">
    private final ListProperty<SdBarraCaixaPA> barrasBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Table">
    private final FormTableView<SdBarraCaixaPA> tblListBarrasLidas = FormTableView.create(SdBarraCaixaPA.class, table -> {
        table.title("Barras Lidas");
        table.expanded();
        table.items.bind(barrasBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Produto");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBarraCaixaPA, SdBarraCaixaPA>, ObservableValue<SdBarraCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<SdBarraCaixaPA, SdBarraCaixaPA>() {
                        @Override
                        protected void updateItem(SdBarraCaixaPA item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            if (item != null && !empty) {
                                setText(item.getProduto());
                                getStyleClass().add("lg");
                            }
                        }
                    });
                }).build(), /*Produto*/
                FormTableColumn.create(cln -> {
                    cln.title("Barra");
                    cln.width(250);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBarraCaixaPA, SdBarraCaixaPA>, ObservableValue<SdBarraCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<SdBarraCaixaPA, SdBarraCaixaPA>() {
                        @Override
                        protected void updateItem(SdBarraCaixaPA item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            if (item != null && !empty) {
                                setText(item.getBarra());
                                getStyleClass().add("lg");
                            }
                        }
                    });
                }).build(), /*Barra*/
                FormTableColumn.create(cln -> {
                    cln.title("Cor");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBarraCaixaPA, SdBarraCaixaPA>, ObservableValue<SdBarraCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<SdBarraCaixaPA, SdBarraCaixaPA>() {
                        @Override
                        protected void updateItem(SdBarraCaixaPA item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            if (item != null && !empty) {
                                setText(item.getCor());
                                getStyleClass().add("lg");
                            }
                        }
                    });
                }).build(), /*Cor*/
                FormTableColumn.create(cln -> {
                    cln.title("Tamanho");
                    cln.width(80);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBarraCaixaPA, SdBarraCaixaPA>, ObservableValue<SdBarraCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<SdBarraCaixaPA, SdBarraCaixaPA>() {
                        @Override
                        protected void updateItem(SdBarraCaixaPA item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            if (item != null && !empty) {
                                setText(item.getTam());
                                getStyleClass().add("lg");
                            }
                        }
                    });
                }).build() /*Tamanho*/
        );
        table.factoryRow(param -> {
            return new TableRow<SdBarraCaixaPA>() {
                @Override
                protected void updateItem(SdBarraCaixaPA item, boolean empty) {
                    super.updateItem(item, empty);
                    clear();

                    if (item != null && !empty) {
                        if (isLeituraSegunda.get()) {
                            if (table.items.indexOf(item) % 2 == 0) {
                                getStyleClass().add("table-row-info");
                            } else {
                                getStyleClass().add("table-row-info-dark");
                            }
                        }
                        if (isExcluirBarra.get()) {
                            if (table.items.indexOf(item) % 2 == 0) {
                                getStyleClass().add("table-row-danger");
                            } else {
                                getStyleClass().add("table-row-danger-dark");
                            }
                        }
                    }
                }

                private void clear() {
                    getStyleClass().removeAll("table-row-info", "table-row-info-dark", "table-row-danger", "table-row-danger-dark");
                }
            };
        });
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Fields/Components">
    private final FormFieldText loteField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Lote");
        field.editable.bind((emAdicaoOps.not().and(emLeituraItens.not()).and(isLoteNulo)).or(isApenasLeitura));
        field.addStyle("lg");
        field.alignment(Pos.CENTER);
        field.width(150.0);
        field.tooltip("Clique com o botão direito para buscar lote.");
        field.keyReleased(evt -> {
            if (evt.getCode() == KeyCode.ENTER) {
                buscaLoteDigitado();
            }
        });
        field.textField.setOnMouseClicked(evt -> {
            if (evt.getButton().equals(MouseButton.SECONDARY) && !field.textField.isDisable()) {
                SdLotePa lote = janelaBuscaLote();
                if (lote != null && lote.getId() != 0) {
                    loteEscolhido = lote;
                    carregaLote();
                }
            }
        });
    });

    private final FormFieldText usuarioField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Usuário");
        field.editable(false);
        field.expanded();
    });
    private final FormFieldText dataLoteField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Dt. Envio");
        field.editable(false);
        field.expanded();
        field.textField.setContextMenu(FormContextMenu.create(cmenu -> {
            cmenu.addItem(menuItem -> {
                menuItem.setText("Alterar data de envio do lote");
                menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.EDIT, ImageUtils.IconSize._16));
                menuItem.setOnAction(evt -> {
                    LocalDate localDate = janelaDataLote();
                    if (localDate != null) {
                        loteEscolhido.setDtenvio(localDate);
                        loteEscolhido = new FluentDao().merge(loteEscolhido);
                        carregaCamposLote();
                        MessageBox.create(message -> {
                            message.message("Data de envio atualizada com sucesso!");
                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                            message.position(Pos.TOP_RIGHT);
                            message.notification();
                        });
                    }
                });
            });
        }));
    });
    private final FormFieldText fornecedorField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Forn.");
        field.editable(false);
        field.addStyle("md");
        field.alignment(Pos.CENTER_LEFT);
        field.width(270.0);
    });

    private final FormFieldText opLidoField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Lido");
        field.editable(false);
        field.addStyle("lg").addStyle("success");
        field.value.bind(lidoOP.asString());
        field.alignment(Pos.CENTER);
        field.width(140.0);
    });
    private final FormFieldText opTotalField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Total");
        field.addStyle("lg").addStyle("primary");
        field.editable(false);
        field.alignment(Pos.CENTER);
        field.value.bind(totalOp.asString());
        field.width(140.0);
    });
    private final FormFieldText opSaldoField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Saldo");
        field.editable(false);
        field.alignment(Pos.CENTER);
        field.addStyle("lg").addStyle("warning");
        field.value.bind(saldoOp.asString());
        field.width(140.0);
    });

    private final FormFieldText caixaField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label(ImageUtils.getIcon(ImageUtils.Icon.LEITURA_BARRA, ImageUtils.IconSize._24));
        field.addStyle("lg");
        field.toUpper();
        field.alignment(Pos.CENTER);
        field.editable.bind(emAdicaoOps.and(emLeituraItens.not()));
        field.width(170.0);
        field.keyReleased(this::buscarCaixa);
    });

    private final FormBox boxCaixasLote = FormBox.create(box -> {
        box.flutuante();
        box.expanded();
        box.verticalScroll();
    });

    private final FormFieldText opField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label(ImageUtils.getIcon(ImageUtils.Icon.LEITURA_BARRA, ImageUtils.IconSize._24));
        field.addStyle("lg");
        field.toUpper();
        field.alignment(Pos.CENTER);
        field.editable.bind(emAdicaoOps.and(emLeituraItens.not()));
        field.width(170.0);
        field.keyReleased(evt -> {
            if (evt.getCode().equals(KeyCode.ENTER)) {
                adicionarOP();
                field.clear();
            }
        });
    });

    private final FormBox boxOpsLote = FormBox.create(box -> {
        box.flutuante();
        box.setPrefHeight(230);
        box.verticalScroll();
    });

    private final FormFieldText tamanhoField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Tam");
        field.addStyle("lg");
        field.editable(false);
        field.alignment(Pos.CENTER_LEFT);
        field.width(110);
    });
    private final FormFieldText codigoField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Ref.");
        field.addStyle("md");
        field.editable(false);
        field.alignment(Pos.CENTER_LEFT);
        field.width(130);
    });
    private final FormFieldText corField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Cor");
        field.alignment(Pos.CENTER_LEFT);
        field.addStyle("lg");
        field.editable(false);
        field.width(110);
    });
    private final FormFieldText descProdutoField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.editable(false);
        field.width(150.0);
    });
    private final FormFieldTextArea observacaoProdutoField = FormFieldTextArea.create(field -> {
        field.title("Observação");
        field.expanded();
        field.editable(false);
        field.height(63.0);
    });
    private final FormFieldText barraLeituraField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label(ImageUtils.getIcon(ImageUtils.Icon.LEITURA_BARRA, ImageUtils.IconSize._24));
        field.addStyle("lg");
        field.alignment(Pos.CENTER);
        field.toUpper();
        field.width(500);
        field.editable.bind(emLeituraItens);
        field.keyReleased(event -> {
            try {
                leituraBarra(event);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        });
    });
    private final FormFieldText defeitoField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.addStyle("lg");
        field.alignment(Pos.CENTER);
        field.width(350);
        field.editable.set(false);
        field.addStyle("warning");
    });

    private final FormFieldText lblStatusLeitura = FormFieldText.create(field -> {
        field.withoutTitle();
        field.addStyle("lg");
        field.alignment(Pos.CENTER);
        field.width(100);
        field.editable.set(false);
        field.addStyle("success");
        field.value.setValue("ADICIONANDO");

        field.mouseClicked(evt -> {
            if (emLeituraItens.get()) {
                isExcluirBarra.set(isExcluirBarra.not().get());
            }
            barraLeituraField.requestFocus();
        });
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Button">
    private final FormButton btnFecharCaixa = FormButton.create(btnFecharCaixa -> {
        btnFecharCaixa.icon(ImageUtils.getIcon(ImageUtils.Icon.FECHAR_CAIXA, ImageUtils.IconSize._32));
        btnFecharCaixa.addStyle("primary");
        btnFecharCaixa.disable.bind(emLeituraItens.not().or(isApenasLeitura));
        btnFecharCaixa.setAction(event -> fecharCaixa());
        btnFecharCaixa.tooltip("Fechar caixa");
    });
    private final FormButton btnAdicionarCaixa = FormButton.create(btnAdicionarCaixa -> {
        btnAdicionarCaixa.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._32));
        btnAdicionarCaixa.addStyle("success");
        btnAdicionarCaixa.disable.bind(emLeituraItens.or(emAdicaoOps).or(isApenasLeitura));
        btnAdicionarCaixa.setAction(event -> adicionarNovaCaixa());
    });

    private final FormButton btnAdicionaOp = FormButton.create(btnAddOp -> {
        btnAddOp.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._32));
        btnAddOp.addStyle("success");
        btnAddOp.setAction(event -> liberarAdiciaoOp());
        btnAddOp.disable.bind(isApenasLeitura);
    });
    private final FormButton btnConcluirLeituraOp = FormButton.create(btnConcluirLeituraOp -> {
        btnConcluirLeituraOp.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._32));
        btnConcluirLeituraOp.setText("Concluir Leitura");
        btnConcluirLeituraOp.addStyle("primary").addStyle("lg");
        btnConcluirLeituraOp.setAction(event -> concluirLeituraOp());
        btnConcluirLeituraOp.setMaxHeight(10);
        btnConcluirLeituraOp.disable.bind(isApenasLeitura);
    });

    private final FormButton btnNovoLote = FormButton.create(btnNovoLote -> {
        btnNovoLote.title("Novo Lote");
        btnNovoLote.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._32));
        btnNovoLote.addStyle("lg").addStyle("success");
        btnNovoLote.setAction(evt -> {
            criarNovoLote();
        });
    });
    private final FormButton btnSalvarLote = FormButton.create(btnCarregar -> {
        btnCarregar.title("Salvar Lote");
        btnCarregar.icon(ImageUtils.getIcon(ImageUtils.Icon.SALVAR, ImageUtils.IconSize._32));
        btnCarregar.addStyle("lg").addStyle("info");
        btnCarregar.setAction(evt -> {
            salvarLote();
        });
        btnCarregar.disable.bind(isApenasLeitura);
    });
    private final FormButton btnCancelarLote = FormButton.create(btnCarregar -> {
        btnCarregar.title("Cancelar Lote");
        btnCarregar.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._32));
        btnCarregar.addStyle("lg").addStyle("danger");
        btnCarregar.setAction(evt -> {
            cancelarLote();
        });
        btnCarregar.disable.bind(isApenasLeitura);
    });
    private final FormButton btnEncerrarLote = FormButton.create(btnEncerrar -> {
        btnEncerrar.title("Encerrar Lote");
        btnEncerrar.icon(ImageUtils.getIcon(ImageUtils.Icon.FECHAR_CAIXA, ImageUtils.IconSize._32));
        btnEncerrar.addStyle("lg").addStyle("warning");
        btnEncerrar.setAction(evt -> {
            encerrarLote();
        });
        btnEncerrar.disable.bind(isApenasLeitura);
    });

    private final FormButton btnImprimirRomaneio = FormButton.create(btnImprimirRomaneio -> {
        btnImprimirRomaneio.icon(ImageUtils.getIcon(ImageUtils.Icon.NOTA_FISCAL, ImageUtils.IconSize._32));
        btnImprimirRomaneio.tooltip("Imprimir Romaneio");
        btnImprimirRomaneio.addStyle("warning");
        btnImprimirRomaneio.setAction(evt -> {
            if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                message.message("Tem certeza que deseja imprmir o romaneio?");
                message.showAndWait();
            }).value.get())) {
                if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                    message.message("Deseja imprimir a grade?");
                    message.showAndWait();
                }).value.get())) {
                    imprimirRomaneioGrade();
                } else {
                    imprimirRomaneio();
                }
            }
        });
        btnImprimirRomaneio.disable.bind(isApenasLeitura.not());
    });
    // </editor-fold>

    //<editor-fold desc="TabPane">
    private final FormTabPane tabPaneCores = FormTabPane.create(tabPane -> {
        tabPane.setPrefWidth(200);
        tabPane.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (emLeituraItens.get() && oldValue != null)
                MessageBox.create(message -> {
                    message.message("Você está mudando para a cor " + newValue.getText());
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showFullScreen();
                });
        });
    });
    //</editor-fold>

    public EntradaProdutosAcabadosView() {
        super("Entrada de Produtos Acabados", ImageUtils.getImage(ImageUtils.Icon.PRODUTO), true);
        this.usuario = Globals.getNomeUsuario();
        colaborador = new FluentDao().selectFrom(SdColaborador.class).where(it -> it.equal("usuario", usuario).equal("codigoFuncao", "101")).singleResult();
        if (colaborador == null) {
            MessageBox.create(message -> {
                message.message("Usuário não habilitado para realizar entrada de produtos acabados.");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showAndWait();
            });
            Globals.getMainStage().close();
            System.exit(0);
        } else {
            init();
        }
    }

    private void init() {
        super.box.getChildren().add(FormBox.create(principal -> {
            principal.horizontal();
            principal.expanded();
            principal.add(FormBox.create(boxEsquerda -> {
                boxEsquerda.vertical();
                boxEsquerda.width(500);
                boxEsquerda.add(FormBox.create(boxDadosLote -> {
                    boxDadosLote.vertical();
                    boxDadosLote.title("Dados do Lote");
                    boxDadosLote.add(FormBox.create(boxDadosCliente -> {
                        boxDadosCliente.horizontal();
                        boxDadosCliente.add(loteField.build());
                        boxDadosCliente.add(fornecedorField.build());
                        boxDadosCliente.add(btnImprimirRomaneio);
                    }));
                    boxDadosLote.add(FormBox.create(boxDadosUsuario -> {
                        boxDadosUsuario.horizontal();
                        boxDadosUsuario.add(usuarioField.build(), dataLoteField.build());
                        usuarioField.value.setValue(usuario);
                    }));
                    boxDadosLote.add(FormBox.create(boxDadosOp -> {
                        boxDadosOp.vertical();
                        boxDadosOp.title("Ordem de Produção");
                        boxDadosOp.expanded();
                        boxDadosOp.add(FormBox.create(boxDados -> {
                            boxDados.horizontal();
                            boxDados.alignment(Pos.CENTER_LEFT);
                            boxDados.add(opField.build());
                            boxDados.add(FormBox.create(boxBadges -> {
                                boxBadges.horizontal();
                                boxBadges.add(FormBadges.create(badges -> {
                                    badges.badgedNode(btnAdicionaOp);
                                    badges.position(Pos.TOP_RIGHT, -5.0);
                                    badges.text(lb -> {
                                        lb.value.set("F2");
                                        lb.addStyle("default");
                                        lb.sizeText(10);
                                        lb.boldText();
                                        lb.borderRadius(50);
                                    });
                                }));
                            }));
                        }));
                        boxDadosOp.add(boxOpsLote);
                    }));
                }));
                boxEsquerda.add(FormBox.create(boxDadosRemessa -> {
                    boxDadosRemessa.vertical();
                    boxDadosRemessa.add(FormBox.create(boxDadosCliente -> {
                        boxDadosCliente.horizontal();
                        boxDadosCliente.add(opTotalField.build(), opLidoField.build(), opSaldoField.build());
                    }));
                }));
                boxEsquerda.add(FormBox.create(boxDadosCaixa -> {
                    boxDadosCaixa.vertical();
                    boxDadosCaixa.title("Dados da Caixa");
                    boxDadosCaixa.expanded();
                    boxDadosCaixa.add(FormBox.create(boxDados -> {
                        boxDados.horizontal();
                        boxDados.alignment(Pos.CENTER_LEFT);
                        boxDados.add(caixaField.build());
                        boxDados.add(FormBox.create(boxBadges -> {
                            boxBadges.horizontal();
                            boxBadges.add(FormBadges.create(badges -> {
                                badges.badgedNode(btnFecharCaixa);
                                badges.position(Pos.TOP_RIGHT, -5.0);
                                badges.text(lb -> {
                                    lb.value.set("F4");
                                    lb.addStyle("default");
                                    lb.sizeText(10);
                                    lb.boldText();
                                    lb.borderRadius(50);
                                });
                            }));
                            boxBadges.add(FormBadges.create(badges -> {
                                badges.badgedNode(btnAdicionarCaixa);
                                badges.position(Pos.TOP_RIGHT, -5.0);
                                badges.text(lb -> {
                                    lb.value.set("F5");
                                    lb.addStyle("default");
                                    lb.sizeText(10);
                                    lb.boldText();
                                    lb.borderRadius(50);
                                });
                            }));
                        }));
                    }));
                    boxDadosCaixa.add(boxCaixasLote);
                }));
                boxEsquerda.add(FormBox.create(boxRelogio -> {
                    boxRelogio.horizontal();
                    boxRelogio.alignment(Pos.BOTTOM_LEFT);
                    boxRelogio.add(FormLabel.create(label -> {
                        label.value.set(StringUtils.toDateFormat(LocalDate.now()));
                        label.addStyle("lg");
                    }).build());
                    boxRelogio.add(FormClock.create(label -> {
                        label.addStyle("lg");
                    }).build());
                }));
            }));
            principal.add(FormBox.create(boxDireita -> {
                boxDireita.horizontal();
                boxDireita.add(FormBox.create(boxProduto -> {
                    boxProduto.vertical();
                    boxProduto.expanded();
                    boxProduto.add(FormBox.create(boxDadosProduto -> {
                        boxDadosProduto.vertical();
                        boxDadosProduto.expanded();
                        boxDadosProduto.title("Dados do Produto");
                        boxDadosProduto.add(FormBox.create(boxBarra -> {
                            boxBarra.horizontal();
                            boxBarra.alignment(Pos.BOTTOM_LEFT);
                            boxBarra.add(barraLeituraField.build());
                            boxBarra.add(FormBadges.create(badges -> {
                                badges.badgedNode(lblStatusLeitura.build());
                                badges.position(Pos.TOP_RIGHT, -5.0);
                                badges.text(lb -> {
                                    lb.value.set("F3");
                                    lb.addStyle("default");
                                    lb.sizeText(10);
                                    lb.boldText();
                                    lb.borderRadius(50);
                                });
                            }));
                        }));
                        boxDadosProduto.add(FormBox.create(boxProdutoBarra -> {
                            boxProdutoBarra.horizontal();
                            boxProdutoBarra.expanded();
                            boxProdutoBarra.alignment(Pos.TOP_LEFT);
                            boxProdutoBarra.setMaxHeight(10);
                            boxProdutoBarra.add(FormBox.create(boxDescricaoProduto -> {
                                boxDescricaoProduto.vertical();
                                boxDescricaoProduto.alignment(Pos.TOP_LEFT);
                                boxDescricaoProduto.add(FormBox.create(box -> {
                                    box.horizontal();
                                    box.add(codigoField.build());
                                    box.add(corField.build());
                                    box.add(tamanhoField.build());
                                }));
                                boxDescricaoProduto.add(descProdutoField.build());
                            }));
                            boxProdutoBarra.add(FormBox.create(boxBotoes -> {
                                boxBotoes.horizontal();
                                boxBotoes.add(defeitoField.build());
                                boxBotoes.add(FormBadges.create(badges -> {
                                    badges.setMaxHeight(10);
                                    badges.badgedNode(btnConcluirLeituraOp.build());
                                    badges.position(Pos.TOP_RIGHT, -5.0);
                                    badges.text(lb -> {
                                        lb.value.set("F10");
                                        lb.addStyle("default");
                                        lb.sizeText(10);
                                        lb.boldText();
                                        lb.borderRadius(50);
                                    });
                                }));
                            }));
                        }));
                        boxDadosProduto.add(FormBox.create(boxContent -> {
                            boxContent.horizontal();
                            boxContent.expanded();
                            boxContent.add(FormBox.create(boxTbl -> {
                                boxTbl.horizontal();
                                boxTbl.width(600);
                                boxTbl.add(tblListBarrasLidas.build());
                            }));
                            boxContent.add(FormBox.create(col2 -> {
                                col2.vertical();
                                col2.width(400);
                                col2.add(tabPaneCores);
                            }));
                        }));
                    }));
                    boxProduto.add(FormBox.create(boxAcoes -> {
                        boxAcoes.vertical();
                        boxAcoes.title("Ações");
                        boxAcoes.add(FormBox.create(boxBotoesAcoes -> {
                            boxBotoesAcoes.flutuante();
                            boxBotoesAcoes.expanded();
                            boxBotoesAcoes.add(FormBadges.create(badges -> {
                                badges.badgedNode(btnNovoLote);
                                badges.position(Pos.TOP_RIGHT, -5.0);
                                badges.text(lb -> {
                                    lb.value.set("F1");
                                    lb.addStyle("default");
                                    lb.sizeText(10);
                                    lb.boldText();
                                    lb.borderRadius(50);
                                });
                            }));
                            boxBotoesAcoes.add(btnCancelarLote);
                            boxBotoesAcoes.add(FormBadges.create(badges -> {
                                badges.badgedNode(btnSalvarLote);
                                badges.position(Pos.TOP_RIGHT, -5.0);
                                badges.text(lb -> {
                                    lb.value.set("F6");
                                    lb.addStyle("default");
                                    lb.sizeText(10);
                                    lb.boldText();
                                    lb.borderRadius(50);
                                });
                            }));
                            boxBotoesAcoes.add(FormBox.create(box -> {
                                box.horizontal();
                                box.add(FormBox.create(boxBadges -> {
                                    boxBadges.horizontal();
                                    boxBadges.add(FormBadges.create(badges -> {
                                        badges.badgedNode(btnEncerrarLote);
                                        badges.position(Pos.TOP_RIGHT, -5.0);
                                        badges.text(lb -> {
                                            lb.value.set("F12");
                                            lb.addStyle("default");
                                            lb.sizeText(10);
                                            lb.boldText();
                                            lb.borderRadius(50);
                                        });
                                    }));
                                }));
                            }));
                        }));
                    }));
                }));
            }));
        }));
        box.requestFocus();
        box.setOnKeyReleased(event -> {
            if (event.getCode().equals(KeyCode.F1)) {
                if (!btnNovoLote.isDisable())
                    btnNovoLote.fire();
            }
            if (event.getCode().equals(KeyCode.F2)) {
                if (!btnAdicionaOp.isDisable())
                    btnAdicionaOp.fire();
            }
            if (event.getCode().equals(KeyCode.F3)) {
                if (emLeituraItens.get())
                    isExcluirBarra.set(isExcluirBarra.not().get());
            }
            if (event.getCode().equals(KeyCode.F4)) {
                if (!btnFecharCaixa.isDisable())
                    btnFecharCaixa.fire();
            }
            if (event.getCode().equals(KeyCode.F5)) {
                if (!btnAdicionarCaixa.isDisable())
                    btnAdicionarCaixa.fire();
            }
            if (event.getCode().equals(KeyCode.F6)) {
                if (!btnSalvarLote.isDisable())
                    btnSalvarLote.fire();
            }
            if (event.getCode().equals(KeyCode.F10)) {
                if (!btnConcluirLeituraOp.isDisable())
                    btnConcluirLeituraOp.fire();
            }
            if (event.getCode().equals(KeyCode.F12)) {
                if (!btnEncerrarLote.isDisable())
                    btnEncerrarLote.fire();
            }
        });
        listStatusLote = ((List<SdStatusLotePA>) new FluentDao().selectFrom(SdStatusLotePA.class).get().resultList());
        listStatusItemLote = ((List<SdStatusItemLotePA>) new FluentDao().selectFrom(SdStatusItemLotePA.class).get().resultList());
        isLeituraSegunda.addListener((observable, oldValue, newValue) -> {
            handleStatusLeitura();
        });
        isExcluirBarra.addListener((observable, oldValue, newValue) -> {
            handleStatusLeitura();
        });
        isApenasLeitura.addListener((observable, oldValue, newValue) -> {
            handleStatusLeitura();
        });
    }

    // <editor-fold defaultstate="collapsed" desc="Operações Lote">
    private void criarNovoLote() {
        if (loteEscolhido != null && isApenasLeitura.not().get()) {
            return;
        }

        try {
            LocalDate dataLote = colaborador.isEntradaDireta() ? LocalDate.now() : janelaDataLote();
            if (dataLote != null) {
                loteEscolhido = new SdLotePa(usuario);
                loteEscolhido.setDtemissao(LocalDate.now());
                loteEscolhido.setDtenvio(dataLote);
                loteEscolhido.setStatus(STATUS_CRIADO);
                loteEscolhido.setCodfor(new FluentDao().selectFrom(Entidade.class).where(it -> it.equal("codcli", colaborador.getCodfor())).singleResult());
                loteEscolhido = new FluentDao().persist(loteEscolhido);
                carregaLote();
                SysLogger.addSysDelizLog("Entrada Produtos Acabados", TipoAcao.CADASTRAR, loteEscolhido.getId().toString(), "Novo Lote " + loteEscolhido.getId() + " criado!");
            }
        } catch (SQLException e) {
            ExceptionBox.build(ex -> {
                ex.exception(e);
                ex.sendAutomaticMail();
                ex.showAndWait();
            });
            e.printStackTrace();
        }
    }

    private void carregaCamposLote() {
        loteField.value.set(loteEscolhido.getId().toString());
        dataLoteField.value.setValue(StringUtils.toShortDateFormat(loteEscolhido.getDtenvio()));
        fornecedorField.value.setValue(loteEscolhido.getCodfor() != null ? loteEscolhido.getCodfor().getNome() : "");
    }

    private void carregaLote() {
        isApenasLeitura.set(false);
        isLoteNulo.set(false);
        limparBox();

        if (loteEscolhido.getCodfor() == null) {
            loteEscolhido.setCodfor(new FluentDao().selectFrom(Entidade.class).where(it -> it.equal("codcli", colaborador.getCodfor())).singleResult());
            loteEscolhido = new FluentDao().merge(loteEscolhido);
        }

        if (loteEscolhido.getStatus().getCodigo() > 3) {
            isApenasLeitura.set(true);
        } else {
            emAdicaoOps.set(true);
        }

        exibirOpsLote();
        carregaCamposLote();

        if (isApenasLeitura.get()) {
            return;
        }

        opField.requestFocus();
        loteEscolhido.getItensLote().stream().filter(it -> (!it.getStatus().equals(STATUS_ITEM_CONCLUIDA) && it.isSelecionado()) || !it.getStatus().equals(STATUS_ITEM_CONCLUIDA)).findFirst().ifPresent(this::abrirOrdem);
    }

    private void salvarLote() {
        if (loteEscolhido == null) {
            return;
        }
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Tem certeza que deseja salvar o lote?");
            message.showAndWait();
        }).value.get())) {
            loteEscolhido.setStatus(STATUS_CRIADO);
            calcularTodosOsTotais();
            MessageBox.create(message -> {
                message.message("Lote salvo com sucesso!");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            SysLogger.addSysDelizLog("Entrada Produtos Acabados", TipoAcao.SALVAR, loteEscolhido.getId().toString(), "Lote " + loteEscolhido.getId() + " salvo!");
            limparBeans();
            limparBox();
        }
    }

    private void cancelarLote() {
        if (loteEscolhido == null) {
            return;
        }
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Tem certeza que deseja cancelar e exluir este lote?");
            message.showAndWait();
        }).value.get())) {
            SysLogger.addSysDelizLog("Entrada Produtos Acabados", TipoAcao.EXCLUIR, loteEscolhido.getId().toString(), "Lote " + loteEscolhido.getId() + " excluído!");
            new FluentDao().delete(loteEscolhido);
            loteField.value.setValue("");
            limparBeans();
            limparBox();
        }
    }

    private void encerrarLote() {

        if (loteEscolhido == null) {
            return;
        }
        if (loteEscolhido.getItensLote().stream().anyMatch(it -> it.getStatus().getCodigo() != 3)) {
            MessageBox.create(message -> {
                message.message("Para encerrar o lote, a leitura de todas as ordens tem que ser concluída!");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showFullScreen();
            });
            return;
        }

        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Tem certeza que deseja encerrar?");
            message.showAndWait();
        }).value.get())) {
            if (colaborador.isEntradaDireta() || janelaEncerramentoLote()) {
                calcularTodosOsTotais();
                loteEscolhido.setDtemissao(LocalDate.now());
                loteEscolhido = new FluentDao().merge(loteEscolhido);
                if (colaborador.isEntradaDireta()) {
                    if (FuncoesEntradaPA.recebeLote(usuario, loteEscolhido)) {
                        SysLogger.addSysDelizLog("Entrada Produtos Acabados", TipoAcao.SALVAR, loteEscolhido.getId().toString(), "Lote " + loteEscolhido.getId() + " recebido!");
                        limparBeans();
                        limparBox();
                    }
                } else {
                    MessageBox.create(message -> {
                        message.message("Lote salvo com sucesso!");
                        message.type(MessageBox.TypeMessageBox.SUCCESS);
                        message.showFullScreen();
                    });
                    SysLogger.addSysDelizLog("Entrada Produtos Acabados", TipoAcao.SALVAR, loteEscolhido.getId().toString(), "Lote " + loteEscolhido.getId() + " encerrado!");
                    limparBeans();
                    limparBox();
                }
            }
        }
    }

    private void buscaLoteDigitado() {
        if (loteField.value.isNotEmpty().get()) {
            loteEscolhido = new FluentDao().selectFrom(SdLotePa.class).where(it -> it.equal("id", loteField.value.getValueSafe())).singleResult();
            if (loteEscolhido == null) {
                MessageBox.create(message -> {
                    message.message("Código de lote " + loteField.value.getValueSafe() + " não existe!");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.show();
                });
                loteField.clear();
            } else {
                carregaLote();
            }
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Operações Ordens de Produção">
    private void abrirOrdem(SdItemLotePA itemLote) {
        ordemEscolhida = null;
        caixaEscolhida = null;
        if (ofEmLeituraExpedicao(itemLote.getNumero())) {
            MessageBox.create(message -> {
                message.message("Of se encontra em recebimento na expedição, favor entrar em contato com o responsável pelo recebimento!");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showFullScreen();
            });
            return;
        }
        atualizarQtdeOf(itemLote);
        isLeituraSegunda.set(false);
        tblListBarrasLidas.clear();
        ordemEscolhida = itemLote;
        emAdicaoOps.set(false);
        loteEscolhido.getItensLote().stream().filter(it -> !it.getStatus().equals(STATUS_ITEM_CONCLUIDA)).forEach(it -> it.setStatus(STATUS_ITEM_ADICIONADO));
        loteEscolhido.getItensLote().forEach(eb -> eb.setSelecionado(false));
        ordemEscolhida.setStatus(STATUS_ITEM_EM_EDICAO);
        ordemEscolhida.setSelecionado(true);
        loteEscolhido.setStatus(STATUS_EM_LEITURA);
        loteEscolhido = new FluentDao().merge(loteEscolhido);
        exibirOpsLote();
        carregarCaixas();
        criaTabsCores(ordemEscolhida);
        carregaCamposTotaisOp();
        if (ordemEscolhida.getListCaixas().stream().anyMatch(it -> !it.isFechada()))
            abrirCaixa(ordemEscolhida.getListCaixas().stream().filter(it -> !it.isFechada()).findFirst().get());
    }

    private void atualizarQtdeOf(SdItemLotePA itemLote) {
        VSdQuantRestanteOf of = new FluentDao().selectFrom(VSdQuantRestanteOf.class).where(it -> it.equal("numero", itemLote.getNumero())).singleResult();
        itemLote.setQtdeTotal(of == null || of.getQtde() < 0 ? 0 : of.getQtde());
    }

    private void adicionarOP() {
        try {

            List<VSdDadosOfOsPendente> ofs = (List<VSdDadosOfOsPendente>) new FluentDao().selectFrom(VSdDadosOfOsPendente.class)
                    .where(it -> it
                            .equal("numero", opField.value.getValueSafe())
                    )
                    .resultList();

            if (ofs == null || ofs.size() == 0) {
                MessageBox.create(message -> {
                    message.message("Of não encontrada, digite uma Of pendente válida!");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showFullScreen();
                });
                return;
            }

            if (ofs.stream().noneMatch(it -> it.getSetor().getCodigo().equals("111") || it.getSetor().getCodigo().equals("118"))) {
                MessageBox.create(message -> {
                    message.message("Of está no setor " + ofs.get(0).getSetor().getCodigo() + " " + ofs.get(0).getSetor().getDescricao() + ", movimente para o setor de acabamento para prosseguir!");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showFullScreen();
                });
                return;
            }

            VSdDadosOfOsPendente of = ofs.stream().filter(it -> it.getSetor().getCodigo().equals("111") || it.getSetor().getCodigo().equals("118")).findFirst().get();

            if (loteEscolhido.getItensLote().stream().anyMatch(it -> it.getNumero().equals(of.getNumero()))) {
                MessageBox.create(message -> {
                    message.message("Op já adicionada a este lote!");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.notification();
                });
                return;
            }

            SdItemLotePA itemOpIgual = new FluentDao().selectFrom(SdItemLotePA.class).where(it -> it.equal("numero", of.getNumero()).notEqual("lote.id", loteEscolhido.getId()).notEqual("lote.status.codigo", 6)).singleResult();
            if (itemOpIgual != null && (itemOpIgual.getLote().getStatus().getCodigo() == 1 || itemOpIgual.getLote().getStatus().getCodigo() == 2 || itemOpIgual.getLote().getStatus().getCodigo() == 3)) {

                SdItemLotePA finalItemOpIgual = itemOpIgual;
                MessageBox.create(message -> {
                    message.message("Ordem de Produção, já está no lote " + finalItemOpIgual.getLote().getId() + " e não se encontra em trânsito!\nSe deseja alterar a OP, altere o lote que possui ela, ou envie o lote!");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showAndWait();
                });
                return;
            }

            SdItemLotePA itemLote = new SdItemLotePA(loteEscolhido, of);
            itemLote = new FluentDao().persist(itemLote);
            loteEscolhido.getItensLote().add(itemLote);
            abrirOrdem(itemLote);
            SysLogger.addSysDelizLog("Entrada Produtos Acabados", TipoAcao.CADASTRAR, ordemEscolhida.getNumero(), "Ordem de Producão " + ordemEscolhida.getId() + " adicionada ao Lote " + loteEscolhido.getId() + "!");
        } catch (SQLException e) {
            ExceptionBox.build(message -> {
                message.exception(e);
                message.showAndWait();
            });
            e.printStackTrace();
        }
    }

    private void removerOrdem(SdItemLotePA itemLote) {
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Tem certeza que deseja excluir a OP?");
            message.showAndWait();
        }).value.get())) {
            loteEscolhido.getItensLote().remove(itemLote);
            SysLogger.addSysDelizLog("Entrada Produtos Acabados", TipoAcao.EXCLUIR, itemLote.getNumero(), "Ordem de Producão " + itemLote.getNumero() + " removida do Lote " + loteEscolhido.getId() + "!");
            new FluentDao().delete(itemLote);
            loteEscolhido = new FluentDao().merge(loteEscolhido);
            exibirOpsLote();
            if (ordemEscolhida != null && ordemEscolhida.equals(itemLote)) {
                boxCaixasLote.clear();
                tabPaneCores.getTabs().clear();
                emLeituraItens.set(false);
                limparCamposTotais();
            }
        }
    }

    private void concluirLeituraOp() {
        if (ordemEscolhida == null) {
            return;
        }
        if (ordemEscolhida.getListCaixas().stream().anyMatch(it -> !it.isFechada())) {
            MessageBox.create(message -> {
                message.message("Para concluir a leitura, feche todas as caixas primeiro");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }
        janelaConclusaoleituraOf();
    }

    private void finalizarLeituraOp() {
        SysLogger.addSysDelizLog("Entrada Produtos Acabados", TipoAcao.SALVAR, ordemEscolhida.getNumero(), "Leitura Concuída da Ordem de Producão " + ordemEscolhida.getNumero() + " do Lote " + loteEscolhido.getId() + "!");
        ordemEscolhida.setPeso(BigDecimal.valueOf(ordemEscolhida.getListCaixas().stream().mapToDouble(it -> it.getPeso().doubleValue()).sum()));
        ordemEscolhida.setQtdecaixas(ordemEscolhida.getListCaixas().size());
        ordemEscolhida.setQtdeLida(ordemEscolhida.getListCaixas().stream().mapToInt(this::getQtdLidaCaixa).sum());
        ordemEscolhida.setSelecionado(false);
        ordemEscolhida.getListCaixas().forEach(cx -> cx.setVolume(ordemEscolhida.getListCaixas().indexOf(cx)));
        ordemEscolhida = new FluentDao().merge(ordemEscolhida);
        isConclusaoLeitura.setValue(false);
        isLeituraSegunda.setValue(false);
        boxOpsLote.disable.set(false);
        emLeituraItens.set(false);
        ordemEscolhida = null;
        emAdicaoOps.set(true);
        exibirOpsLote();
        boxCaixasLote.clear();
        limparCampos();
        MessageBox.create(message -> {
            message.message("Leitura da OP concluída com sucesso!");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }

    private void exibirOpsLote() {
        boxOpsLote.clear();
        loteEscolhido.getItensLote().stream().sorted(Comparator.comparing(SdItemLotePA::getNumero)).forEach(itemLote -> {
            boxOpsLote.add(FormBadges.create(bg -> {
                bg.position(Pos.TOP_RIGHT, -3.0);
                bg.text(lb -> {
                    lb.boldText();
                    lb.sizeText(14);
                    lb.addStyle("info");
                    lb.borderRadius(50);
                    lb.value.bind(itemLote.qtdeTotalProperty().asString());
                });
                bg.badgedNode(FormBadges.create(bgCont -> {
                    bgCont.position(Pos.BOTTOM_LEFT, -3.0);
                    bgCont.badgedNode(FormOverlap.create(ovl -> {
                        ovl.add(ImageUtils.getIcon(getOrdemIcon(itemLote), ImageUtils.IconSize._64));
                        ovl.add(FormLabel.create(lb -> {
                            lb.value.set(String.valueOf(itemLote.getNumero()));
                            lb.boldText();
                            lb.setContextMenu(FormContextMenu.create(cm -> {
                                cm.addItem(menuItem -> {
                                    menuItem.setText("Conferir OF");
                                    menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.ORDEM_COMPRA, ImageUtils.IconSize._16));
                                    menuItem.setDisable(!itemLote.getStatus().equals(STATUS_ITEM_CONCLUIDA));
                                    menuItem.setOnAction(evt -> {
                                        janelaConferirOf(itemLote);
                                    });
                                });

                                cm.addItem(menuItem -> {
                                    menuItem.setText("Imprimir Resumo");
                                    menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.IMPRIMIR, ImageUtils.IconSize._16));
                                    menuItem.setDisable(!itemLote.getStatus().equals(STATUS_ITEM_CONCLUIDA));
                                    menuItem.setOnAction(evt -> {
                                        imprimirGradeOf(itemLote);
                                    });
                                });
                            }));
                            lb.sizeText(12);
                        }));
                    }));
                    bgCont.text(lb -> {
                        lb.setVisible(isApenasLeitura.not().get());
                        lb.boldText();
                        lb.sizeText(14);
                        lb.addStyle("danger");
                        lb.fontColor("#FFF");
                        lb.borderRadius(8);
                        lb.value.set("x");
                        lb.mouseClicked(evt -> removerOrdem(itemLote));
                    });
                }));
                bg.setOnMouseReleased(evt -> {
                    if (itemLote == ordemEscolhida || isApenasLeitura.getValue()) return;

                    if (evt.getButton().equals(MouseButton.PRIMARY) && evt.getClickCount() == 2) {
                        if ((itemLote.getStatus().getCodigo() != 3) || ((itemLote.getStatus().getCodigo() == 3) && ((Boolean) QuestionBox.build(Boolean.class, message -> {
                            message.message("Essa Op já está concluída, deseja abri-la novamente?");
                            message.showAndWait();
                        }).value.get()))) {
                            abrirOrdem(itemLote);
                        }
                    }
                });
            }));
        });
    }

    private ImageUtils.Icon getOrdemIcon(SdItemLotePA itemLote) {
        if (itemLote.getStatus().getCodigo() == 3) return ImageUtils.Icon.ORDEM_LIDA;
        else if (itemLote.isSelecionado()) return ImageUtils.Icon.VISUALIZAR_PEDIDO;
        else return ImageUtils.Icon.NOTA_FISCAL;
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Operações Caixas">
    private void excluirCaixa(SdCaixaPA caixa) {
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Tem certeza que deseja excluir a caixa?");
            message.showAndWait();
        }).value.get())) {
            ordemEscolhida.getListCaixas().remove(caixa);
            SysLogger.addSysDelizLog("Entrada Produtos Acabados", TipoAcao.EXCLUIR, caixa.getId().toString(),
                    "Caixa " + caixa.getId() + " removida da Ordem " + ordemEscolhida.getNumero() + " no Lote " + loteEscolhido.getId() + "!");
            new FluentDao().delete(caixa);
            exibirCaixasOp();
            carregaCamposTotaisOp();
            if (caixa.equals(caixaEscolhida)) {
                barrasBean.clear();
                caixaEscolhida = null;
                emLeituraItens.set(false);
                isExcluirBarra.set(false);
            }
            if (isConclusaoLeitura.not().get()) {
                isLeituraSegunda.set(false);
            }
        }
    }

    private void abrirCaixa(SdCaixaPA caixa) {
        if (ordemEscolhida.getListCaixas().stream().filter(eb -> !eb.equals(caixa)).anyMatch(cx -> !cx.isFechada())) {
            MessageBox.create(message -> {
                message.message("Outra caixa já está aberta!");
                message.type(MessageBox.TypeMessageBox.BLOCKED);
                message.show();
            });
            return;
        }
        if (isApenasLeitura.not().get()) {
            emLeituraItens.set(true);
        }

        caixaEscolhida = caixa;
        ordemEscolhida.getListCaixas().forEach(eb -> eb.setFechada(true));
        caixaEscolhida.setFechada(false);
        exibirCaixasOp();
        refreshTblBarras();
        isLeituraSegunda.set(caixaEscolhida.isSegunda());
        barraLeituraField.requestFocus();
        caixaEscolhida = new FluentDao().merge(caixaEscolhida);
        if (isLeituraSegunda.get()) {
            isExcluirBarra.set(false);
        }
    }

    private void carregarCaixas() {
        if (ordemEscolhida.getListCaixas().size() == 0) {
            adicionarNovaCaixa();
        }
        exibirCaixasOp();
    }

    private void adicionarNovaCaixa() {
        if (ordemEscolhida != null) {
            if (ordemEscolhida.getListCaixas().stream().anyMatch(it -> !it.isFechada())) {
                MessageBox.create(message -> {
                    message.message("Você possui uma caixa aberta, feche antes de abrir outra!");
                    message.type(MessageBox.TypeMessageBox.BLOCKED);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
                return;
            }
            if (ordemEscolhida.getListCaixas().stream().filter(it -> !it.isIncompleta() && !it.isPerdida()).anyMatch(it -> it.getItensCaixa().size() == 0)) {
                MessageBox.create(message -> {
                    message.message("Você já possui uma caixa nova vazia!");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
                return;
            }
            try {
                SdCaixaPA caixa = new SdCaixaPA(ordemEscolhida);
                caixa.setSegunda(isLeituraSegunda.get());
                caixa = new FluentDao().persist(caixa);
                ordemEscolhida.getListCaixas().add(caixa);
                if (ordemEscolhida.getListCaixas().stream().allMatch(SdCaixaPA::isFechada)) abrirCaixa(caixa);
                SysLogger.addSysDelizLog("Entrada Produtos Acabados", TipoAcao.CADASTRAR, caixa.getId().toString(),
                        "Caixa " + caixa.getId() + " adicionada a Ordem " + ordemEscolhida.getNumero() + " no Lote " + loteEscolhido.getId() + "!");
                MessageBox.create(message -> {
                    message.message("Nova Caixa adicionada!");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    private void fecharCaixa() {
        if (caixaEscolhida != null) {
            try {
                BigDecimal peso = new BigDecimal(colaborador.isEntradaDireta() ? "0" : janelaPesoCaixa().replace(",", "."));
                caixaEscolhida.setFechada(true);
                caixaEscolhida.setPeso(peso);
                caixaEscolhida.getItensCaixa().forEach(it -> it.setQtde(it.getListBarra().size()));
                caixaEscolhida.setQtde(caixaEscolhida.getItensCaixa().stream().mapToInt(SdItemCaixaPA::getQtde).sum());
                emLeituraItens.set(false);
                isExcluirBarra.set(false);
                exibirCaixasOp();
                barrasBean.clear();
                caixaEscolhida = new FluentDao().merge(caixaEscolhida);
                SysLogger.addSysDelizLog("Entrada Produtos Acabados", TipoAcao.SALVAR, caixaEscolhida.getId().toString(),
                        "Caixa " + caixaEscolhida.getId() + " fechada com o peso de " + caixaEscolhida.getPeso().toString() + " KG, da Ordem " + ordemEscolhida.getNumero() + " no Lote " + loteEscolhido.getId() + "!");
                imprimirEtiquetaCaixa(caixaEscolhida);
                caixaEscolhida = null;
                if (isConclusaoLeitura.not().get()) {
                    isLeituraSegunda.set(false);
                }
                MessageBox.create(message -> {
                    message.message("Caixa fechada com sucesso!");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            } catch (NumberFormatException e) {
                e.printStackTrace();
                MessageBox.create(message -> {
                    message.message("Número digitado para o peso está no formato inválido!\nLetras não são permitidas!");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showFullScreen();
                });
            }
        }
    }

    private void exibirCaixasOp() {
        boxCaixasLote.clear();
        ordemEscolhida.getListCaixas()
                .stream()
                .sorted(Comparator.comparing(SdCaixaPA::getId))
                .filter(cx -> !cx.isIncompleta() && !cx.isPerdida())
                .forEach(caixa -> {
                    caixa.setQtde(caixa.getItensCaixa().stream().mapToInt(it -> it.getListBarra().size()).sum());
                    boxCaixasLote.add(FormBadges.create(bg -> {
                        if (!caixa.isSegunda() && isLeituraSegunda.get() && isConclusaoLeitura.get()) {
                            bg.setDisable(true);
                        }
                        bg.position(Pos.TOP_RIGHT, -3.0);
                        bg.text(lb -> {
                            lb.boldText();
                            lb.sizeText(14);
                            lb.addStyle("info");
                            lb.borderRadius(50);
                            lb.value.bind(caixa.qtdeProperty().asString());
                        });
                        bg.badgedNode(FormBadges.create(bgCont -> {
                            bgCont.position(Pos.BOTTOM_LEFT, -3.0);
                            bgCont.badgedNode(FormOverlap.create(ovl -> {
                                ovl.add(ImageUtils.getIcon(getCaixaIcon(caixa), ImageUtils.IconSize._64));
                                ovl.add(FormLabel.create(lb -> {
                                    lb.alignment(Pos.CENTER);
                                    lb.boldText();
                                    lb.sizeText(12);
                                    lb.setContextMenu(FormContextMenu.create(cMenu -> {
                                        cMenu.addItem(menuItem -> {
                                            menuItem.setText("Limpar Caixa");
                                            menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.LIMPAR, ImageUtils.IconSize._16));
                                            menuItem.setOnAction(evt -> {
                                                limparCaixa(caixa);
                                            });
                                            menuItem.setDisable(isApenasLeitura.get());
                                        });
                                        cMenu.addItem(menuItem -> {
                                            menuItem.setText("Excluir Caixa");
                                            menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.EXCLUIR_CAIXA, ImageUtils.IconSize._16));
                                            menuItem.setOnAction(evt -> {
                                                excluirCaixa(caixa);
                                            });
                                            menuItem.setDisable(isApenasLeitura.get());
                                        });
                                        cMenu.addSeparator();
                                        cMenu.addItem(menuItem -> {
                                            menuItem.setText("Imprimir Etiqueta");
                                            menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.IMPRIMIR, ImageUtils.IconSize._16));
                                            menuItem.setDisable(!caixa.isFechada());
                                            menuItem.setOnAction(evt -> {
                                                imprimirEtiquetaCaixa(caixa);
                                            });
                                            menuItem.setDisable(isApenasLeitura.get());
                                        });
                                    }));
                                    lb.setPadding(new Insets(10, 0, 0, 0));
                                    lb.value.set("\t" + caixa.getId() + "\n" + caixa.getProduto());
                                    lb.mouseClicked(event -> {
                                        if (event.getButton().equals(MouseButton.PRIMARY) && event.getClickCount() == 1 && ordemEscolhida.getListCaixas().stream().noneMatch(it -> !it.isFechada())) {
                                            barrasBean.set(FXCollections.observableArrayList(caixa.getItensCaixa().stream().flatMap(it -> it.getListBarra().stream()).collect(Collectors.toList())));
                                        }
                                        if (event.getButton().equals(MouseButton.PRIMARY) && event.getClickCount() == 2 && (caixa != caixaEscolhida || !caixa.isFechada())) {
                                            if (verificaAberturaCaixa(caixa))
                                                abrirCaixa(caixa);
                                        }
                                    });
                                }));
                            }));
                            bgCont.text(lb -> {
                                lb.setVisible(isApenasLeitura.not().get());
                                lb.boldText();
                                lb.sizeText(12);
                                lb.addStyle("danger");
                                lb.fontColor("#FFF");
                                lb.setPadding(new Insets(0, -5, -10, 0));
                                lb.borderRadius(8);
                                lb.value.set("x");
                                lb.mouseClicked(evt -> excluirCaixa(caixa));
                            });
                        }));
                    }));
                });
    }

    private void limparCaixa(SdCaixaPA caixa) {
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Tem certeza que deseja remover todos os itens da caixa?");
            message.showAndWait();
        }).value.get())) {
            SysLogger.addSysDelizLog("Entrada Produtos Acabados", TipoAcao.EXCLUIR, caixa.getId().toString(),
                    "Removido todos os itens da Caixa " + caixa.getId() + ", da Ordem " + ordemEscolhida.getNumero() + " no Lote " + loteEscolhido.getId() + "!");
            for (SdItemCaixaPA itemCaixa : caixa.getItensCaixa()) {
                new FluentDao().delete(itemCaixa);
            }
            caixa.getItensCaixa().clear();
            caixa = new FluentDao().merge(caixa);
            exibirCaixasOp();
            carregaCamposTotaisOp();
            refreshTblBarras();
        }
    }

    private boolean verificaAberturaCaixa(SdCaixaPA caixa) {
        if (ordemEscolhida.getListCaixas().stream().filter(eb -> !eb.equals(caixa)).anyMatch(it -> !it.isFechada())) {
            MessageBox.create(message -> {
                message.message("Você possui uma caixa aberta, feche antes de abrir outra!");
                message.type(MessageBox.TypeMessageBox.BLOCKED);
                message.showAndWait();
            });
            return false;
        }
        if ((caixa.isSegunda() && isLeituraSegunda.not().get()) && (!((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Você está abrindo uma caixa de segunda, tem certeza?");
            message.showAndWait();
        }).value.get()))) {
            return false;
        }
        return (caixa.getPeso() == BigDecimal.ZERO) || ((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Esta caixa já foi fechada, tem certeza que deseja abri-la novamente?");
            message.showAndWait();
        }).value.get());
    }

    private ImageUtils.Icon getCaixaIcon(SdCaixaPA caixa) {
        if (caixa.isSegunda()) {
            if (caixa.isFechada()) return ImageUtils.Icon.CAIXA2;
            return ImageUtils.Icon.CAIXA_ABERTA2;
        } else if (caixa.isIncompleta() || caixa.isPerdida()) {
            return ImageUtils.Icon.CAIXA_INCOMPLETA;
        } else {
            if (caixa.isFechada()) return ImageUtils.Icon.CAIXA;
            return ImageUtils.Icon.CAIXA_ABERTA;
        }
    }

    private void criarCaixaIncompletaPerdidaRestante(boolean isPerdida) {
        if (isPerdida) removerCaixaPerdida();
        else removerCaixaIncompleta();
        SdCaixaPA caixa = new SdCaixaPA(ordemEscolhida);
        if (isPerdida) caixa.setPerdida(true);
        else caixa.setIncompleta(true);
        caixa.setFechada(true);

        try {
            VSdDadosOfOsPendente of = getOfPendente(ordemEscolhida);
            of.getCores().forEach(itemOf -> {
                List<SdItemLotePA> itensEmTransito = (List<SdItemLotePA>) new FluentDao().selectFrom(SdItemLotePA.class).where(it -> it
                        .equal("numero", of.getNumero())
                        .isIn("lote.status.codigo", new String[]{"4", "5"})
                        .notEqual("lote.id", loteEscolhido.getId())).resultList();
                int qtdeDesconto = 0;

                if (itensEmTransito != null && itensEmTransito.size() > 0) {

                    for (SdItemLotePA itemEmTransito : itensEmTransito) {
                        qtdeDesconto += itemEmTransito.getListCaixas().stream()
                                .filter(it -> !it.isRecebida())
                                .mapToInt(it -> it.getItensCaixa().stream()
                                        .filter(eb ->
                                                eb.getCor().equals(itemOf.getId().getCor()) &&
                                                        eb.getTam().equals(itemOf.getId().getTam()) &&
                                                        eb.getProduto().equals(itemOf.getId().getCodigo())
                                        ).mapToInt(SdItemCaixaPA::getQtde).sum()).sum();
                    }
                }
                int qtdeLida = ordemEscolhida.getListCaixas()
                        .stream()
                        .filter(cx -> cx.getProduto().equals(itemOf.getId().getCodigo()))
                        .flatMap(rb -> rb.getItensCaixa().stream().filter(eb ->
                                eb.getCor().equals(itemOf.getId().getCor()) &&
                                        eb.getTam().equals(itemOf.getId().getTam())
                        )).mapToInt(SdItemCaixaPA::getQtde).sum();
                if (itemOf.getQtde() > (qtdeLida + qtdeDesconto)) {
                    caixa.getItensCaixa().add(new SdItemCaixaPA(caixa, (itemOf.getQtde() - (qtdeLida + qtdeDesconto)), itemOf.getId().getCodigo(), itemOf.getId().getTam(), itemOf.getId().getCor()));
                }
            });
            caixa.setQtde(caixa.getItensCaixa().stream().mapToInt(SdItemCaixaPA::getQtde).sum());
            ordemEscolhida.getListCaixas().add(caixa);
            try {
                SysLogger.addSysDelizLog("Entrada Produtos Acabados", TipoAcao.CADASTRAR, caixa.getId().toString(),
                        "Caixa " + caixa.getId().toString() + " de peças " + (isPerdida ? "PERDIDAS" : "INCOMPLETAS") + " adicionada, na Ordem " + ordemEscolhida.getNumero() + " no Lote " + loteEscolhido.getId() + "!");
                new FluentDao().persist(caixa);
                new FluentDao().merge(ordemEscolhida);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            MessageBox.create(message -> {
                message.message("Of digitada não está pendente.");
                message.type(MessageBox.TypeMessageBox.BLOCKED);
                message.showAndWait();
            });
        }
    }

    private void criarCaixaIncompletaPerdida(List<SdBarraCaixaPA> barras, boolean isPerdida) {
        if (barras.size() == 0) return;
        SdCaixaPA caixa = new SdCaixaPA(ordemEscolhida);
        caixa.setIncompleta(!isPerdida);
        caixa.setPerdida(isPerdida);
        caixa.setRecebida(true);

        Map<String, Map<String, List<SdBarraCaixaPA>>> mapCorTam = barras.stream().collect(Collectors.groupingBy(SdBarraCaixaPA::getCor, Collectors.groupingBy(SdBarraCaixaPA::getTam)));
        mapCorTam.forEach((cor, mapTam) -> {
            mapTam.forEach((tam, list) -> {
                SdItemCaixaPA itemCaixa = new SdItemCaixaPA(caixa, list.size(), list.get(0).getProduto(), tam, cor);
                caixa.getItensCaixa().add(itemCaixa);
            });
        });
        ordemEscolhida.getListCaixas().add(caixa);
        try {
            SysLogger.addSysDelizLog("Entrada Produtos Acabados", TipoAcao.CADASTRAR, caixa.getId().toString(),
                    "Caixa " + caixa.getId().toString() + " de peças " + (isPerdida ? "PERDIDAS" : "INCOMPLETAS") + " adicionada, na Ordem " + ordemEscolhida.getNumero() + " no Lote " + loteEscolhido.getId() + "!");
            new FluentDao().persist(caixa);
            new FluentDao().merge(ordemEscolhida);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private void removerCaixaIncompleta() {
        SdCaixaPA cx = ordemEscolhida.getListCaixas().stream().filter(SdCaixaPA::isIncompleta).findFirst().orElse(null);
        if (cx != null) {
            ordemEscolhida.getListCaixas().remove(cx);
            new FluentDao().delete(cx);
        }
    }

    private void removerCaixaPerdida() {
        SdCaixaPA cx = ordemEscolhida.getListCaixas().stream().filter(SdCaixaPA::isPerdida).findFirst().orElse(null);
        if (cx != null) {
            ordemEscolhida.getListCaixas().remove(cx);
            new FluentDao().delete(cx);
        }
    }

    private void buscarCaixa(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            SdCaixaPA caixa = ordemEscolhida.getListCaixas().stream().filter(cx -> !cx.isIncompleta() && !cx.isPerdida()).filter(it -> it.getId().equals(Integer.valueOf(caixaField.value.getValue()))).findFirst().orElseGet(null);
            if (caixa == null) {
                MessageBox.create(message -> {
                    message.message("Caixa não encontrada!");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showFullScreen();
                });
            } else {
                if (verificaAberturaCaixa(caixa))
                    abrirCaixa(caixa);
            }
            caixaField.clear();
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Barra">
    private void leituraBarra(KeyEvent event) throws SQLException {
        if (event.getCode().equals(KeyCode.ENTER) && emLeituraItens.get()) {
            barraSelecionada = null;
            String barraLida = barraLeituraField.value.getValue();
            barraSelecionada = new FluentDao().selectFrom(SdBarraCaixaPA.class).where(it -> it.equal("barra", barraLida)).singleResult();

            if (isLeituraSegunda.get() && barraLeituraField.value.getValue().contains("DEF")) {
                try {
                    defeitoEscolhido = new FluentDao().selectFrom(Defeito.class).where(it -> it.equal("codigo", barraLeituraField.value.getValue().replace("DEF", ""))).singleResult();
                    defeitoField.value.setValue(defeitoEscolhido.getCodigo() + " - " + defeitoEscolhido.getDescricao());
                } catch (NullPointerException e) {
                    MessageBox.create(message -> {
                        message.message("Nenhum defeito encontrado com esse código!");
                        message.type(MessageBox.TypeMessageBox.WARNING);
                        message.showFullScreen();
                    });
                    e.printStackTrace();
                }
                barraLeituraField.clear();
                return;
            }

            if (isExcluirBarra.not().get()) {
                if (isLeituraSegunda.get() && defeitoEscolhido == null) {
                    MessageBox.create(message -> {
                        message.message("Para adicionar um item de segunda selecione o defeito!");
                        message.type(MessageBox.TypeMessageBox.WARNING);
                        message.showFullScreen();
                    });
                    barraLeituraField.clear();
                    barraLeituraField.requestFocus();
                    return;
                }
                adicaoBarraLida(barraLida);
            } else {
                leituraBarraRemocao();
            }
            barraLeituraField.clear();
        }
    }

    private void leituraBarraRemocao() {
        if (barraSelecionada == null) {
            MessageBox.create(message -> {
                message.message("Barra não encontrada!");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showFullScreen();
            });
            return;
        }
        if (!barraSelecionada.getItemcaixa().getCaixa().getItemlote().getLote().equals(loteEscolhido)) {
            MessageBox.create(message -> {
                message.message("Barra não pertence ao lote selecionado!\nBarra se encontra no lote " + loteEscolhido.getId());
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showFullScreen();
            });
            return;
        }

        if (!barraSelecionada.getItemcaixa().getCaixa().getItemlote().equals(ordemEscolhida)) {
            MessageBox.create(message -> {
                message.message("Barra não pertence a ordem de produção selecionada!\nBarra se encontra na OP " + ordemEscolhida.getNumero());
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showFullScreen();
            });
            return;
        }

        if (!barraSelecionada.getItemcaixa().getCaixa().equals(caixaEscolhida)) {
            MessageBox.create(message -> {
                message.message("Barra não pertence a caixa selecionada!\nBarra se encontra na caixa " + caixaEscolhida.getId());
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showFullScreen();
            });
            return;
        }

        SdItemCaixaPA itemcaixa = barraSelecionada.getItemcaixa();
        removeCaixaEItensCascata();
        carregaCamposTotaisOp();
        if (caixaEscolhida != null) caixaEscolhida.setQtde(getQtdLidaCaixa(caixaEscolhida));
        tabPaneCores.getSelectionModel().select(tabPaneCores.getTabs().stream().filter(it -> it.getId().equals(barraSelecionada.getCor())).findFirst().get());
        refreshTblBarras();
    }

    private void removeCaixaEItensCascata() {
        SdItemCaixaPA itemCaixa = barraSelecionada.getItemcaixa();
        SdCaixaPA caixa = itemCaixa.getCaixa();

        if (itemCaixa.getListBarra().size() == 1) {
            if (caixa.getItensCaixa().size() == 1 && isLeituraSegunda.not().get()) {
                excluirCaixa(caixa);
            } else {
                caixa.getItensCaixa().remove(itemCaixa);
                new FluentDao().delete(itemCaixa);
            }
        } else {
            itemCaixa.getListBarra().remove(barraSelecionada);
            new FluentDao().delete(barraSelecionada);
        }
        ordemEscolhida = new FluentDao().merge(ordemEscolhida);
    }

    private void adicaoBarraLida(String barraLida) throws SQLException {
        if (barraSelecionada != null) {
            mostrarMensagemErro();
            return;
        }

        if (!barraLida.substring(10).equals(ordemEscolhida.getNumero())) {
            MessageBox.create(message -> {
                message.message("Barra lida não pertence a OP");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showFullScreen();
            });
            barraLeituraField.requestFocus();
            return;
        }

        try {
            PaIten paIten = new FluentDao().selectFrom(PaIten.class).where(it -> it.equal("barra28", barraLida.substring(0, 6)).equal("id.deposito", "0001")).singleResult();
//            VSdDadosProdutoBarra pBarra = new FluentDao().selectFrom(VSdDadosProdutoBarra.class).where(it -> it.equal("barra28", barraLida.substring(0, 6))).singleResult();
            preencherCamposProduto(paIten);
            VSdDadosOfPendenteSKU ofSku = new FluentDao().selectFrom(VSdDadosOfPendenteSKU.class)
                    .where(it -> it
                            .equal("id.numero", ordemEscolhida.getNumero())
                            .equal("id.codigo", paIten.getId().getCodigo())
                            .equal("id.tam", paIten.getId().getTam())
                            .equal("id.cor", paIten.getId().getCor())
                            .equal("id.setor", ordemEscolhida.getSetor())
                    )
                    .singleResult();
            if (ofSku != null) {
                adicionarBarra(barraLida, paIten);
            } else {
                MessageBox.create(message -> {
                    message.message("Tamanho do produto não pertence a OP");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showFullScreen();
                    message.position(Pos.TOP_RIGHT);
                });
            }
        } catch (NullPointerException e) {
            MessageBox.create(message -> {
                message.message("Barra Lida não pertence a Ordem de Produção selecionada!");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showFullScreen();
                message.position(Pos.TOP_RIGHT);
            });
        }
    }

    private void mostrarMensagemErro() {
        String mensagem = "";
        if (barraSelecionada.getItemcaixa().getCaixa().equals(caixaEscolhida)) {
            mensagem = "Barra " + barraSelecionada.getBarra() + " dupicada na caixa!";
        } else if (barraSelecionada.getItemcaixa().getCaixa().getItemlote().getLote().equals(loteEscolhido)) {
            mensagem = "Barra " + barraSelecionada.getBarra() + " já lida na caixa " + barraSelecionada.getItemcaixa().getCaixa().getId() + ", nesse mesmo lote!";
        } else {
            mensagem = "Barra " + barraSelecionada.getBarra() + " já lida na caixa " + barraSelecionada.getItemcaixa().getCaixa().getId() + " e no lote " + barraSelecionada.getItemcaixa().getCaixa().getItemlote().getLote().getId() + "!";
        }
        String finalMensagem = mensagem;
        MessageBox.create(message -> {
            message.message(finalMensagem);
            message.type(MessageBox.TypeMessageBox.WARNING);
            message.showFullScreen();
            message.position(Pos.TOP_RIGHT);
        });
    }

    private void adicionarBarra(String barraLida, PaIten paIten) throws SQLException {
        SdItemCaixaPA itemCaixa = new FluentDao().selectFrom(SdItemCaixaPA.class)
                .where(it -> it
                        .equal("produto", paIten.getId().getCodigo())
                        .equal("cor", paIten.getId().getCor())
                        .equal("tam", paIten.getId().getTam())
                        .equal("caixa.id", caixaEscolhida.getId()))
                .singleResult();
        if (itemCaixa == null) {
            itemCaixa = new SdItemCaixaPA(paIten, caixaEscolhida);
            caixaEscolhida.getItensCaixa().add(itemCaixa);
        }

        SdBarraCaixaPA sdBarraCaixaPA = new SdBarraCaixaPA(paIten, barraLida, itemCaixa);
        sdBarraCaixaPA.setSegunda(isLeituraSegunda.get());
        if (isLeituraSegunda.get()) sdBarraCaixaPA.setDefeito(defeitoEscolhido);
        itemCaixa.getListBarra().add(sdBarraCaixaPA);
//        sdBarraCaixaPA.setItemcaixa(itemCaixa);

        caixaEscolhida.setQtde(getQtdLidaCaixa(caixaEscolhida));
        caixaEscolhida = new FluentDao().merge(caixaEscolhida);
        String corBarraLida = sdBarraCaixaPA.getCor();
        tabPaneCores.getSelectionModel().select(tabPaneCores.getTabs().stream().filter(it -> it.getId().equals(corBarraLida)).findFirst().get());
        carregaCamposTotaisOp();
        barrasBean.add(sdBarraCaixaPA);
    }
    // </editor-fold>

    //<editor-fold desc="Limpar">
    private void limparBox() {
        boxCaixasLote.clear();
        boxOpsLote.clear();
    }

    private void limparBeans() {
        emLeituraItens.set(false);
        emAdicaoOps.set(false);
        isLeituraSegunda.set(false);
        isExcluirBarra.set(false);
        loteEscolhido = null;
        isLoteNulo.set(true);
        ordemEscolhida = null;
        defeitoEscolhido = null;
        limparCampos();
        limparCamposTotais();
    }

    private void limparCampos() {
        barrasBean.clear();
        tabPaneCores.getTabs().clear();
        codigoField.clear();
        corField.clear();
        opField.clear();
        caixaField.clear();
        tamanhoField.clear();
        descProdutoField.clear();
        defeitoField.clear();
    }

    private void limparCamposTotais() {
        totalOp.setValue(0);
        lidoOP.setValue(0);
        saldoOp.setValue(0);
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Soma">
    private int getQtdLidaCaixa(SdCaixaPA caixa) {
        return caixa.getItensCaixa().stream().mapToInt(it -> it.getListBarra().size()).sum();
    }

    private void carregaCamposTotaisOp() {
        totalOp.set(ordemEscolhida.qtdeTotalProperty().get());
        lidoOP.set(ordemEscolhida.getListCaixas().stream().mapToInt(this::getQtdLidaCaixa).sum());
        saldoOp.set(Math.max(totalOp.getValue() - lidoOP.getValue(), 0));
    }

    private void calcularTodosOsTotais() {
        loteEscolhido.getItensLote().forEach(itemLote -> {
            itemLote.getListCaixas().stream().filter(caixa -> !caixa.isIncompleta() && !caixa.isPerdida()).forEach(caixa -> {
                caixa.getItensCaixa().forEach(itemCaixa -> {
                    itemCaixa.setQtde(itemCaixa.getListBarra().size());
                });
                caixa.setQtde(caixa.getItensCaixa().stream().mapToInt(SdItemCaixaPA::getQtde).sum());
            });
            itemLote.setPeso(BigDecimal.valueOf(itemLote.getListCaixas().stream().mapToDouble(it -> it.getPeso().doubleValue()).sum()));
            itemLote.setQtdecaixas((int) itemLote.getListCaixas().stream().filter(it -> !it.isIncompleta() && !it.isPerdida()).count());
            itemLote.setQtdeLida(itemLote.getListCaixas().stream().filter(it -> !it.isIncompleta() && !it.isPerdida()).mapToInt(this::getQtdLidaCaixa).sum());
        });
        loteEscolhido.setQtde(loteEscolhido.getItensLote().stream().mapToInt(SdItemLotePA::getQtdeLida).sum());
        loteEscolhido.setVolumes(loteEscolhido.getItensLote().stream().mapToInt(SdItemLotePA::getQtdecaixas).sum());
        loteEscolhido.setPeso(BigDecimal.valueOf(loteEscolhido.getItensLote().stream().mapToDouble(it -> it.getPeso().doubleValue()).sum()));
        loteEscolhido = new FluentDao().merge(loteEscolhido);
    }

    private int getSomaBarrasLidasTotal(VSdDadosOfOsPendenteSKU item) {
        return ordemEscolhida.getListCaixas().stream()
                .mapToInt(eb -> eb.getItensCaixa().stream()
                        .filter(ob -> ob.getProduto().equals(item.getId().getCodigo()) &&
                                ob.getCor().equals(item.getId().getCor()) &&
                                ob.getTam().equals(item.getId().getTam()))
                        .mapToInt(el -> el.getListBarra().size()).sum()).sum();
    }

    private int getSomaBarrasLidasBoas(VSdDadosOfOsPendenteSKU item) {
        return ordemEscolhida.getListCaixas().stream()
                .filter(el -> !el.isSegunda())
                .mapToInt(eb -> eb.getItensCaixa().stream()
                        .filter(ob -> ob.getProduto().equals(item.getId().getCodigo()) &&
                                ob.getCor().equals(item.getId().getCor()) &&
                                ob.getTam().equals(item.getId().getTam()))
                        .mapToInt(el -> el.getListBarra().size()).sum()).sum();
    }

    private int getSomaBarrasLidasSegunda(VSdDadosOfOsPendenteSKU item) {
        return ordemEscolhida.getListCaixas().stream()
                .mapToInt(eb -> eb.getItensCaixa().stream()
                        .filter(ob -> ob.getProduto().equals(item.getId().getCodigo()) &&
                                ob.getCor().equals(item.getId().getCor()) &&
                                ob.getTam().equals(item.getId().getTam()))
                        .mapToInt(ol -> (int) ol.getListBarra().stream().filter(SdBarraCaixaPA::isSegunda).count()).sum()).sum();
    }

    // </editor-fold>

    //<editor-fold desc="Style">
    private void removeStyleAllFields(String style) {
        barraLeituraField.removeStyle(style);
        codigoField.removeStyle(style);
        corField.removeStyle(style);
        tamanhoField.removeStyle(style);
        observacaoProdutoField.removeStyle(style);
        descProdutoField.removeStyle(style);
    }

    private void addStyleAllFields(String style) {
        barraLeituraField.addStyle(style);
        codigoField.addStyle(style);
        corField.addStyle(style);
        tamanhoField.addStyle(style);
        observacaoProdutoField.addStyle(style);
        descProdutoField.addStyle(style);
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Impressão">
    private void imprimirEtiquetaCaixa(SdCaixaPA caixa) {

        if (caixa.isSegunda()) {
            try {
                ReportUtils.ShowReport showReport = new ReportUtils().config().addReport(ReportUtils.ReportFile.ETIQUETA_CAIXA_SEGUNDA_PA,
                        new ReportUtils.ParameterReport("caixa", caixa.getId())).view();

                if (colaborador.isImpressoraPadrao()) {
                    showReport.print();
                } else {
                    showReport.printWithDialog();
                }
            } catch (JRException | SQLException | IOException e) {
                e.printStackTrace();
            }
        } else {
            ReportUtils.ShowReport showReport = null;
            try {
                showReport = new ReportUtils().config().addReport(ReportUtils.ReportFile.ETIQUETA_CAIXA_PA,
                        new ReportUtils.ParameterReport("indice", caixa.getItemlote().getListCaixas().indexOf(caixa) + 1),
                        new ReportUtils.ParameterReport("itemCaixa", caixa.getItensCaixa().stream().map(it -> it.getId()).collect(Collectors.toList()))
                ).view();
                if (colaborador.isImpressoraPadrao()) {
                    showReport.print();
                } else {
                    showReport.printWithDialog();
                }
            } catch (SQLException | IOException | JRException e) {
                e.printStackTrace();
            }
        }
        MessageBox.create(message -> {
            message.message("Etiqueta impressa com sucesso!");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }

    private void imprimirRomaneio() {
        if (loteEscolhido == null) {
            return;
        }
        try {
            new ReportUtils().config().addReport(ReportUtils.ReportFile.ROMANEIO_PRODUTOS_ACABADOS,
                    new ReportUtils.ParameterReport("codEmpresa", "1000"),
                    new ReportUtils.ParameterReport("lote", loteEscolhido.getId().toString()),
                    new ReportUtils.ParameterReport("faccao", loteEscolhido.getCodfor().getCodcli() + " - " + loteEscolhido.getCodfor().getNome()),
                    new ReportUtils.ParameterReport("dtEmissao", StringUtils.toShortDateFormat(loteEscolhido.getDtemissao())),
                    new ReportUtils.ParameterReport("dtEnvio", StringUtils.toShortDateFormat(loteEscolhido.getDtenvio())),
                    new ReportUtils.ParameterReport("usuario", loteEscolhido.getUsuario()),
                    new ReportUtils.ParameterReport("qtdeCaixas", loteEscolhido.getItensLote().stream().mapToInt(item -> (int) item.getListCaixas().stream().filter(cx -> !cx.isIncompleta() && !cx.isPerdida()).count()).sum()),
                    new ReportUtils.ParameterReport("qtdeItens", loteEscolhido.getItensLote().stream().mapToInt(item -> (int) item.getListCaixas().stream().filter(cx -> !cx.isIncompleta() && !cx.isPerdida()).mapToInt(cx -> cx.getQtde()).sum()).sum()),
                    new ReportUtils.ParameterReport("peso", loteEscolhido.getItensLote().stream().mapToDouble(item -> item.getListCaixas().stream().filter(cx -> !cx.isIncompleta() && !cx.isPerdida()).mapToDouble(cx -> cx.getPeso().doubleValue()).sum()).sum())
            ).view().printWithDialog();
        } catch (JRException | SQLException | IOException e) {
            e.printStackTrace();
        }
    }

    private void imprimirRomaneioGrade() {
        if (loteEscolhido == null) {
            return;
        }
        try {
            new ReportUtils().config().addReport(ReportUtils.ReportFile.ROMANEIO_PRODUTOS_ACABADOS,
                            new ReportUtils.ParameterReport("codEmpresa", "1000"),
                            new ReportUtils.ParameterReport("lote", loteEscolhido.getId().toString()),
                            new ReportUtils.ParameterReport("faccao", loteEscolhido.getCodfor().getCodcli() + " - " + loteEscolhido.getCodfor().getNome()),
                            new ReportUtils.ParameterReport("dtEmissao", StringUtils.toShortDateFormat(loteEscolhido.getDtemissao())),
                            new ReportUtils.ParameterReport("dtEnvio", StringUtils.toShortDateFormat(loteEscolhido.getDtenvio())),
                            new ReportUtils.ParameterReport("usuario", loteEscolhido.getUsuario()),
                            new ReportUtils.ParameterReport("qtdeCaixas", loteEscolhido.getItensLote().stream().mapToInt(item -> (int) item.getListCaixas().stream().filter(cx -> !cx.isIncompleta() && !cx.isPerdida()).count()).sum()),
                            new ReportUtils.ParameterReport("qtdeItens", loteEscolhido.getItensLote().stream().mapToInt(item -> (int) item.getListCaixas().stream().filter(cx -> !cx.isIncompleta() && !cx.isPerdida()).mapToInt(SdCaixaPA::getQtde).sum()).sum()),
                            new ReportUtils.ParameterReport("peso", loteEscolhido.getItensLote().stream().mapToDouble(item -> item.getListCaixas().stream().filter(cx -> !cx.isIncompleta() && !cx.isPerdida()).mapToDouble(cx -> cx.getPeso().doubleValue()).sum()).sum())
                    ).addReport(ReportUtils.ReportFile.ROMANEIO_PRODUTOS_ACABADOS_GRADE,
                            new ReportUtils.ParameterReport("codEmpresa", "1000"),
                            new ReportUtils.ParameterReport("lote", loteEscolhido.getId().toString()),
                            new ReportUtils.ParameterReport("faccao", loteEscolhido.getCodfor().getCodcli() + " - " + loteEscolhido.getCodfor().getNome()),
                            new ReportUtils.ParameterReport("dtEmissao", StringUtils.toShortDateFormat(loteEscolhido.getDtemissao())),
                            new ReportUtils.ParameterReport("dtEnvio", StringUtils.toShortDateFormat(loteEscolhido.getDtenvio())),
                            new ReportUtils.ParameterReport("usuario", loteEscolhido.getUsuario()),
                            new ReportUtils.ParameterReport("qtdeCaixas", loteEscolhido.getItensLote().stream().mapToInt(item -> (int) item.getListCaixas().stream().filter(cx -> !cx.isIncompleta() && !cx.isPerdida()).count()).sum()),
                            new ReportUtils.ParameterReport("qtdeItens", loteEscolhido.getItensLote().stream().mapToInt(item -> (int) item.getListCaixas().stream().filter(cx -> !cx.isIncompleta() && !cx.isPerdida()).mapToInt(SdCaixaPA::getQtde).sum()).sum()),
                            new ReportUtils.ParameterReport("peso", loteEscolhido.getItensLote().stream().mapToDouble(item -> item.getListCaixas().stream().filter(cx -> !cx.isIncompleta() && !cx.isPerdida()).mapToDouble(cx -> cx.getPeso().doubleValue()).sum()).sum())
                    )
                    .view().printWithDialog();
        } catch (JRException | SQLException | IOException e) {
            e.printStackTrace();
        }
    }

    private void imprimirGradeOf(SdItemLotePA itemLote) {

        try {
            new ReportUtils().config().addReport(ReportUtils.ReportFile.GRADE_PRODUTOS_ACABADOS,
                    new ReportUtils.ParameterReport("codEmpresa", "1000"),
                    new ReportUtils.ParameterReport("itemLote", itemLote.getId())
            ).view().printWithDialog();
        } catch (JRException | SQLException | IOException e) {
            e.printStackTrace();
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Janelas">

    private LocalDate janelaDataLote() {
        AtomicReference<LocalDate> data = new AtomicReference<>();
        new Fragment().show(fragment -> {
            fragment.title("Data de Saída do Lote");
            fragment.size(300.0, 170.0);
            FormFieldDate dataField = FormFieldDate.create(field -> {
                field.title.setText("Data");
                field.datePicker.setPrefWidth(600);
                field.value.setValue(LocalDate.now());
                field.addStyle("lg");
            });

            ((Button) fragment.boxFullButtons.getChildren().get(1)).getStyleClass().add("lg");
            fragment.box.getChildren().add(FormBox.create(principal -> {
                principal.vertical();
                principal.expanded();
                principal.add(dataField.build());
            }));
            fragment.buttonsBox.getChildren().add(FormButton.create(btn -> {
                fragment.box.setOnKeyReleased(evt -> {
                    if (evt.getCode() == KeyCode.ENTER) {
                        btn.fire();
                    }
                });
                btn.addStyle("success").addStyle("lg");
                btn.setText("Confirmar");
                btn.setOnAction(event -> {
                    if (dataField.value.getValue().isBefore(LocalDate.now())) {
                        MessageBox.create(message -> {
                            message.message("Data digitada inválida!\n A data do lote tem que ser maior que o dia atual.");
                            message.type(MessageBox.TypeMessageBox.WARNING);
                            message.showAndWait();
                        });
                        return;
                    }
                    if (dataField.value.getValue() != null) {
                        data.set(dataField.value.get());
                        fragment.close();
                    }
                });
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
            }));
        });
        return data.get();
    }

    private String janelaPesoCaixa() {
        AtomicReference<String> peso = new AtomicReference<>("");
        new Fragment().show(fragment -> {
            fragment.title("Peso da Caixa");
            fragment.size(300.0, 170.0);
            FormFieldText pesoField = FormFieldText.create(text -> {
                text.title.setText("Peso");
                text.addStyle("lg");
                text.width(200);
                text.postLabel("KG");
            });

            fragment.box.getChildren().add(FormBox.create(principal -> {
                principal.vertical();
                principal.add(pesoField.build());
            }));
            fragment.buttonsBox.getChildren().add(FormButton.create(btn -> {
                fragment.setOnKeyReleased(evt -> {
                    if (evt.getCode() == KeyCode.ENTER) {
                        btn.fire();
                    }
                });
                btn.addStyle("success");
                btn.setText("Confirmar");
                btn.setOnAction(event -> {
                    if (pesoField.value.isNotEmpty().get()) {
                        peso.set(pesoField.value.get());
                        fragment.close();
                    }
                });
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
            }));
        });
        return peso.get();
    }

    private SdLotePa janelaBuscaLote() {
        AtomicReference<SdLotePa> lotePa = new AtomicReference<>(new SdLotePa());
        new Fragment().show(frag -> {
            frag.modalStage.setMaximized(true);
            ListProperty<SdLotePa> lotesBean = new SimpleListProperty<>(FXCollections.observableArrayList());
            FormFieldText opTextField = FormFieldText.create(field -> {
                field.title("OF");
                field.addStyle("lg");
                field.width(250);
            });

            FormTableView<SdLotePa> tblLotes = FormTableView.create(SdLotePa.class, table -> {
                table.items.bind(lotesBean);
                table.title("Lotes");
                table.expanded();
                table.factoryRow(param -> {
                    return new TableRow<SdLotePa>() {
                        @Override
                        protected void updateItem(SdLotePa item, boolean empty) {
                            super.updateItem(item, empty);
                            clear();

                            if (item != null && !empty) {
                                if (item.getStatus().getCodigo() == 1) getStyleClass().add("table-row-danger");
                                if (item.getStatus().getCodigo() == 2) getStyleClass().add("table-row-primary");
                                if (item.getStatus().getCodigo() == 3) getStyleClass().add("table-row-info");
                                if (item.getStatus().getCodigo() == 4) getStyleClass().add("table-row-warning");
                                if (item.getStatus().getCodigo() == 5) getStyleClass().add("table-row-success");
                                if (item.getStatus().getCodigo() == 6) getStyleClass().add("table-row-success");
                            }
                        }

                        private void clear() {
                            getStyleClass().removeAll("table-row-danger", "table-row-success", "table-row-warning", "table-row-info", "table-row-primary");
                        }
                    };
                });
                table.columns(
                        FormTableColumn.create(cln -> {
                            cln.title("Código");
                            cln.width(100);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdLotePa, SdLotePa>, ObservableValue<SdLotePa>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                            cln.format(param -> new TableCell<SdLotePa, SdLotePa>() {
                                @Override
                                protected void updateItem(SdLotePa item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    if (item != null && !empty) {
                                        setText(item.getId().toString());
                                        getStyleClass().add("lg");
                                    }
                                }
                            });
                        }).build(), /*Código*/
                        FormTableColumn.create(cln -> {
                            cln.title("Data");
                            cln.width(200);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdLotePa, SdLotePa>, ObservableValue<SdLotePa>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                            cln.format(param -> new TableCell<SdLotePa, SdLotePa>() {
                                @Override
                                protected void updateItem(SdLotePa item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    if (item != null && !empty) {
                                        setText(item.getDtenvio() != null ? StringUtils.toShortDateFormat(item.getDtenvio()) : "");
                                        getStyleClass().add("lg");
                                    }
                                }
                            });
                        }).build(), /*Código*/
                        FormTableColumn.create(cln -> {
                            cln.title("Volumes");
                            cln.width(100);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdLotePa, SdLotePa>, ObservableValue<SdLotePa>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                            cln.format(param -> new TableCell<SdLotePa, SdLotePa>() {
                                @Override
                                protected void updateItem(SdLotePa item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    if (item != null && !empty) {
                                        setText(item.getVolumes().toString());
                                        getStyleClass().add("lg");
                                    }
                                }
                            });
                        }).build(), /*Código*/
                        FormTableColumn.create(cln -> {
                            cln.title("Qtde");
                            cln.width(100);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdLotePa, SdLotePa>, ObservableValue<SdLotePa>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                            cln.format(param -> new TableCell<SdLotePa, SdLotePa>() {
                                @Override
                                protected void updateItem(SdLotePa item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    if (item != null && !empty) {
                                        setText(item.getQtde().toString());
                                        getStyleClass().add("lg");
                                    }
                                }
                            });
                        }).build(), /*Código*/
                        FormTableColumn.create(cln -> {
                            cln.title("Status");
                            cln.width(250);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdLotePa, SdLotePa>, ObservableValue<SdLotePa>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                            cln.format(param -> new TableCell<SdLotePa, SdLotePa>() {
                                @Override
                                protected void updateItem(SdLotePa item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    if (item != null && !empty) {
                                        setText(item.getStatus().getStatus());
                                        getStyleClass().add("lg");
                                    }
                                }
                            });
                        }).build(), /*Código*/
                        FormTableColumn.create(cln -> {
                            cln.title("CodFor");
                            cln.width(600);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdLotePa, SdLotePa>, ObservableValue<SdLotePa>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                            cln.format(param -> new TableCell<SdLotePa, SdLotePa>() {
                                @Override
                                protected void updateItem(SdLotePa item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    if (item != null && !empty) {
                                        setText(String.valueOf(item.getCodfor()/*.getCodcli()*/));
                                        getStyleClass().add("lg");
                                    }
                                }
                            });
                        }).build(), /*Código*/
                        FormTableColumn.create(cln -> {
                            cln.title("Usuário");
                            cln.width(200);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdLotePa, SdLotePa>, ObservableValue<SdLotePa>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                            cln.format(param -> new TableCell<SdLotePa, SdLotePa>() {
                                @Override
                                protected void updateItem(SdLotePa item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    if (item != null && !empty) {
                                        setText(item.getUsuario());
                                        getStyleClass().add("lg");
                                    }
                                }
                            });
                        }).build() /*Código*/
                );
            });
            Button buscarOpBtn = FormButton.create(btn -> {
                btn.setText("Buscar");
                btn.addStyle("primary").addStyle("lg");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.SEARCH, ImageUtils.IconSize._16));
                btn.setOnAction(evt -> {
                    String numero = opTextField.value.getValueSafe();
                    List<SdItemLotePA> itens = (List<SdItemLotePA>) new FluentDao()
                            .selectFrom(SdItemLotePA.class)
                            .where(it -> it
                                    .like("numero", numero, TipoExpressao.AND, when -> !numero.equals("")))
                            .resultList();
                    if (itens != null) {
                        List<SdLotePa> itensUnicos = itens.stream().map(it -> it.getLote()).distinct().sorted(Comparator.comparing(SdLotePa::getId).reversed()).collect(Collectors.toList());
                        lotesBean.set(FXCollections.observableArrayList(itensUnicos));
                    }
                });
            });
            Button confirmarLote = FormButton.create(btn -> {
                btn.setText("Confirmar");
                btn.addStyle("success").addStyle("lg");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                btn.setOnAction(evt -> {
                    if (tblLotes.selectedItem() != null) {
                        lotePa.set(tblLotes.selectedItem());
                        frag.close();
                    } else {
                        MessageBox.create(message -> {
                            message.message("Nenhum lote selecionado!");
                            message.type(MessageBox.TypeMessageBox.BLOCKED);
                            message.showAndWait();
                        });
                    }
                });
            });

            frag.setOnKeyReleased(evt -> {
                if (evt.getCode().equals(KeyCode.ENTER)) {
                    buscarOpBtn.fire();
                }
            });
            frag.box.getChildren().add(FormBox.create(principal -> {
                principal.vertical();
                principal.expanded();
                principal.add(FormBox.create(header -> {
                    header.horizontal();
                    header.alignment(Pos.BOTTOM_LEFT);
                    header.add(opTextField.build(), buscarOpBtn);
                }));
                principal.add(FormBox.create(content -> {
                    content.horizontal();
                    content.expanded();
                    content.add(tblLotes.build());
                }));
            }));
            ((Button) frag.boxFullButtons.getChildren().get(1)).getStyleClass().add("lg");
            frag.buttonsBox.getChildren().add(confirmarLote);
        });
        return lotePa.get();
    }

    private void janelaConferirOf(SdItemLotePA of) {
        Map<String, List<SdItemCaixaPA>> listsPorCor = of.getListCaixas()
                .stream()
                .flatMap(caixa -> caixa.getItensCaixa()
                        .stream())
                .collect(Collectors.toList())
                .stream()
                .collect(Collectors.groupingBy(SdItemCaixaPA::getCor));

        new Fragment().show(frag -> {
            frag.modalStage.setMaximized(true);
            frag.title("Revisão da OF por Caixas");
            frag.box.getChildren().add(FormBox.create(principal -> {
                principal.vertical();
                principal.expanded();
                principal.add(FormBox.create(header -> {
                    header.vertical();
                    header.setPadding(new Insets(10, 0, 25, 0));
                    header.add(FormBox.create(l1 -> {
                        l1.horizontal();
                        l1.setPadding(new Insets(0, 0, 7, 0));
                        l1.add(FormBox.create(boxTituloOf -> {
                            boxTituloOf.horizontal();
                            boxTituloOf.width(900);
                            boxTituloOf.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.editable.set(false);
                                field.label("OF");
                                field.width(200);
                                field.value.setValue(of.getNumero());
                                field.addStyle("lg");
                            }).build());
                        }));
                        l1.add(FormBox.create(boxQtdeCaixa -> {
                            boxQtdeCaixa.horizontal();
                            boxQtdeCaixa.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.editable.set(false);
                                field.width(300);
                                field.value.setValue("QTDE CAIXAS: " + of.getListCaixas().stream().filter(it -> !it.isIncompleta() && !it.isPerdida()).count());
                                field.addStyle("lg");
                            }).build());
                        }));
                    }));
                    header.add(FormBox.create(l2 -> {
                        l2.horizontal();
                        l2.add(FormBox.create(boxTotais -> {
                            boxTotais.horizontal();
                            int pecasSeg = of.getListCaixas().stream().filter(SdCaixaPA::isSegunda).mapToInt(it -> it.getItensCaixa().stream().mapToInt(SdItemCaixaPA::getQtde).sum()).sum();
                            int pecasIncompletas = of.getListCaixas().stream().filter(SdCaixaPA::isIncompleta).mapToInt(it -> it.getItensCaixa().stream().mapToInt(SdItemCaixaPA::getQtde).sum()).sum();
                            int pecasPerdidas = of.getListCaixas().stream().filter(SdCaixaPA::isPerdida).mapToInt(it -> it.getItensCaixa().stream().mapToInt(SdItemCaixaPA::getQtde).sum()).sum();
                            int pecasLidas = of.getListCaixas().stream().filter(eb -> !eb.isIncompleta() && !eb.isSegunda() && !eb.isPerdida()).mapToInt(it -> it.getItensCaixa().stream().mapToInt(SdItemCaixaPA::getQtde).sum()).sum();
                            boxTotais.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.editable.set(false);
                                field.width(200);
                                field.label("Total");
                                field.addStyle("lg").addStyle("primary");
                                field.value.setValue(String.valueOf(of.getQtdeTotal()));
                            }).build());
                            boxTotais.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.editable.set(false);
                                field.width(200);
                                field.label("Total Lido");
                                field.addStyle("lg").addStyle("info");
                                field.value.setValue(String.valueOf(pecasLidas + pecasSeg));
                            }).build());
                            boxTotais.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.editable.set(false);
                                field.width(200);
                                field.label("Peças Boas").addStyle("success");
                                field.addStyle("lg");
                                field.value.setValue(String.valueOf(pecasLidas));
                            }).build());
                            boxTotais.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.editable.set(false);
                                field.width(200);
                                field.label("Peças Seg.").addStyle("warning");
                                field.addStyle("lg");
                                field.value.setValue(String.valueOf(pecasSeg));
                            }).build());
                            boxTotais.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.editable.set(false);
                                field.width(200);
                                field.label("Peças Perd.").addStyle("danger");
                                field.addStyle("lg");
                                field.value.setValue(String.valueOf(pecasPerdidas));
                            }).build());
                            boxTotais.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.editable.set(false);
                                field.width(200);
                                field.label("Peças Inc.").addStyle("secundary");
                                field.addStyle("lg");
                                field.value.setValue(String.valueOf(pecasIncompletas));
                            }).build());
                        }));
                    }));
                }));
                principal.add(FormBox.create(content -> {
                    content.horizontal();
                    content.expanded();
                    content.add(FormTabPane.create(tabPane -> {
                        tabPane.expanded();
                        tabPane.addTab(FormTab.create(tab -> {
                            tab.title("Resumo");
                            tab.icon(ImageUtils.getIcon(ImageUtils.Icon.NOTA_FISCAL, ImageUtils.IconSize._24));
                            tab.setContent(FormBox.create(boxPrincipal -> {
                                boxPrincipal.vertical();
                                boxPrincipal.expanded();
                                boxPrincipal.add(FormBox.create(conteudo -> {
                                    conteudo.expanded();
                                    conteudo.flutuante();
                                    conteudo.verticalScroll();
                                    listsPorCor.forEach((cor, list) -> conteudo.add(FormBox.create(boxContent -> {
                                        boxContent.vertical();
                                        boxContent.add(FormBox.create(linha1 -> {
                                            linha1.horizontal();
                                            linha1.add(FormLabel.create(lbl -> {
                                                lbl.setText("Cor: " + cor);
                                                lbl.addStyle("lg");
                                            }));
                                            linha1.add(FormLabel.create(lbl -> {
                                                lbl.setText(list.size() > 0 ? "OF: " + list.get(0).getCaixa().getItemlote().getNumero() : "");
                                                lbl.addStyle("lg");
                                            }));
                                            linha1.add(FormLabel.create(lbl -> {
                                                lbl.setText("Qtde: " + list.stream().mapToInt(SdItemCaixaPA::getQtde).sum());
                                                lbl.addStyle("lg");
                                            }));
                                        }));
                                        boxContent.add(FormBox.create(linha2 -> {
                                            linha2.horizontal();
                                            linha2.add(new Separator(Orientation.VERTICAL));
                                            linha2.add(FormTableView.create(SdItemCaixaPA.class, table -> {
                                                table.withoutHeader();
                                                table.size(440, 180);
                                                table.items.set(FXCollections.observableArrayList(list.stream().filter(distinctByKey(SdItemCaixaPA::getTam)).sorted((o1, o2) -> sortTamanhos(o1.getTam(), o2.getTam())).collect(Collectors.toList())));
                                                table.columns(
                                                        FormTableColumn.create(cln -> {
                                                            cln.title("TAM");
                                                            cln.width(80);
                                                            cln.value((Callback<TableColumn.CellDataFeatures<SdItemCaixaPA, SdItemCaixaPA>, ObservableValue<SdItemCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                                            cln.format(param -> new TableCell<SdItemCaixaPA, SdItemCaixaPA>() {
                                                                @Override
                                                                protected void updateItem(SdItemCaixaPA item, boolean empty) {
                                                                    super.updateItem(item, empty);
                                                                    setText(null);
                                                                    setAlignment(Pos.CENTER);
                                                                    if (item != null && !empty) {
                                                                        setText(item.getTam());
                                                                    }
                                                                }
                                                            });
                                                        }).build(), /*TAM*/
                                                        FormTableColumn.create(cln -> {
                                                            cln.title("QTDE BOA");
                                                            cln.width(80);
                                                            cln.value((Callback<TableColumn.CellDataFeatures<SdItemCaixaPA, SdItemCaixaPA>, ObservableValue<SdItemCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                                            cln.format(param -> new TableCell<SdItemCaixaPA, SdItemCaixaPA>() {
                                                                @Override
                                                                protected void updateItem(SdItemCaixaPA item, boolean empty) {
                                                                    super.updateItem(item, empty);
                                                                    setText(null);
                                                                    setAlignment(Pos.CENTER);
                                                                    if (item != null && !empty) {
                                                                        setText(String.valueOf(list.stream().filter(it -> !it.getCaixa().isSegunda() && !it.getCaixa().isIncompleta() && !it.getCaixa().isPerdida()).filter(it -> it.getTam().equals(item.getTam())).mapToInt(SdItemCaixaPA::getQtde).sum()));
                                                                    }
                                                                }
                                                            });
                                                        }).build(), /*TAM*/
                                                        FormTableColumn.create(cln -> {
                                                            cln.title("QTDE SEG");
                                                            cln.width(80);
                                                            cln.value((Callback<TableColumn.CellDataFeatures<SdItemCaixaPA, SdItemCaixaPA>, ObservableValue<SdItemCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                                            cln.format(param -> new TableCell<SdItemCaixaPA, SdItemCaixaPA>() {
                                                                @Override
                                                                protected void updateItem(SdItemCaixaPA item, boolean empty) {
                                                                    super.updateItem(item, empty);
                                                                    setText(null);
                                                                    setAlignment(Pos.CENTER);
                                                                    if (item != null && !empty) {
                                                                        setText(String.valueOf(list.stream().filter(it -> it.getCaixa().isSegunda()).filter(it -> it.getTam().equals(item.getTam())).mapToInt(SdItemCaixaPA::getQtde).sum()));
                                                                    }
                                                                }
                                                            });
                                                        }).build(), /*TAM*/
                                                        FormTableColumn.create(cln -> {
                                                            cln.title("QTDE INC");
                                                            cln.width(80);
                                                            cln.value((Callback<TableColumn.CellDataFeatures<SdItemCaixaPA, SdItemCaixaPA>, ObservableValue<SdItemCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                                            cln.format(param -> new TableCell<SdItemCaixaPA, SdItemCaixaPA>() {
                                                                @Override
                                                                protected void updateItem(SdItemCaixaPA item, boolean empty) {
                                                                    super.updateItem(item, empty);
                                                                    setText(null);
                                                                    setAlignment(Pos.CENTER);
                                                                    if (item != null && !empty) {
                                                                        setText(String.valueOf(list.stream().filter(it -> it.getCaixa().isIncompleta()).filter(it -> it.getTam().equals(item.getTam())).mapToInt(SdItemCaixaPA::getQtde).sum()));
                                                                    }
                                                                }
                                                            });
                                                        }).build(),/*TAM*/
                                                        FormTableColumn.create(cln -> {
                                                            cln.title("QTDE PERD");
                                                            cln.width(80);
                                                            cln.value((Callback<TableColumn.CellDataFeatures<SdItemCaixaPA, SdItemCaixaPA>, ObservableValue<SdItemCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                                            cln.format(param -> new TableCell<SdItemCaixaPA, SdItemCaixaPA>() {
                                                                @Override
                                                                protected void updateItem(SdItemCaixaPA item, boolean empty) {
                                                                    super.updateItem(item, empty);
                                                                    setText(null);
                                                                    setAlignment(Pos.CENTER);
                                                                    if (item != null && !empty) {
                                                                        setText(String.valueOf(list.stream().filter(it -> it.getCaixa().isPerdida()).filter(it -> it.getTam().equals(item.getTam())).mapToInt(SdItemCaixaPA::getQtde).sum()));
                                                                    }
                                                                }
                                                            });
                                                        }).build(),/*TAM*/
                                                        FormTableColumn.create(cln -> {
                                                            cln.title("QTDE TOT");
                                                            cln.width(80);
                                                            cln.value((Callback<TableColumn.CellDataFeatures<SdItemCaixaPA, SdItemCaixaPA>, ObservableValue<SdItemCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                                            cln.format(param -> new TableCell<SdItemCaixaPA, SdItemCaixaPA>() {
                                                                @Override
                                                                protected void updateItem(SdItemCaixaPA item, boolean empty) {
                                                                    super.updateItem(item, empty);
                                                                    setText(null);
                                                                    setAlignment(Pos.CENTER);
                                                                    if (item != null && !empty) {
                                                                        setText(String.valueOf(list.stream().filter(it -> it.getTam().equals(item.getTam())).mapToInt(SdItemCaixaPA::getQtde).sum()));
                                                                    }
                                                                }
                                                            });
                                                        }).build() /*TAM*/
                                                );
                                            }).build());
                                        }));
                                    })));
                                }));
                            }));
                        }));
                        of.getListCaixas().sort(Comparator.comparing(SdCaixaPA::getId));
                        for (SdCaixaPA caixa : of.getListCaixas()) {
                            tabPane.addTab(FormTab.create(tab -> {
                                tab.setText(String.valueOf(caixa.getId()));
                                tab.icon(ImageUtils.getIcon(getCaixaIcon(caixa), ImageUtils.IconSize._24));
                                tab.setContent(FormBox.create(boxPrincipal -> {
                                    boxPrincipal.vertical();
                                    boxPrincipal.expanded();
                                    boxPrincipal.add(FormBox.create(boxResumo -> {
                                        boxResumo.horizontal();
                                        boxResumo.width(850);
                                        boxResumo.title("Resumo da Caixa");
                                        boxResumo.setPadding(new Insets(10, 0, 0, 0));
                                        boxResumo.add(FormBox.create(boxQtdes -> {
                                            boxQtdes.horizontal();
                                            boxQtdes.add(FormFieldText.create(field -> {
                                                field.withoutTitle();
                                                field.width(250);
                                                field.editable.set(false);
                                                field.value.setValue(String.valueOf(caixa.getId()));
                                                field.addStyle("lg");
                                                field.label("Caixa");
                                            }).build());
                                            boxQtdes.add(FormFieldText.create(field -> {
                                                field.withoutTitle();
                                                field.width(250);
                                                field.editable.set(false);
                                                field.value.setValue(String.valueOf(caixa.getQtde()));
                                                field.addStyle("lg");
                                                field.label("Qtde");
                                            }).build());
                                            boxQtdes.add(FormFieldText.create(field -> {
                                                field.withoutTitle();
                                                field.width(250);
                                                field.editable.set(false);
                                                field.value.setValue(String.valueOf(caixa.getPeso()));
                                                field.addStyle("lg");
                                                field.label("Peso");
                                            }).build());
                                            boxQtdes.add(FormFieldText.create(field -> {
                                                field.withoutTitle();
                                                field.editable.set(false);
                                                field.addStyle("lg");
                                                field.width(400);
                                                field.value.setValue(caixa.isSegunda() ? "CAIXA PEÇAS DE SEGUNDA" : caixa.isPerdida() ? "CAIXA DE PEÇAS PERDIDAS" : caixa.isIncompleta() ? "CAIXA DE PEÇAS INCOMPLETAS" : "CAIXA DE PEÇAS COMPLETAS");
                                                field.addStyle(caixa.isSegunda() ? "info" : caixa.isPerdida() ? "danger" : caixa.isIncompleta() ? "secundary" : "success");
                                            }).build());
                                        }));
                                    }));
                                    boxPrincipal.add(FormBox.create(boxDetalhes -> {
                                        boxDetalhes.horizontal();
                                        boxDetalhes.expanded();
                                        boxDetalhes.title("Detalhes");
                                        boxDetalhes.add(FormTableView.create(SdBarraCaixaPA.class, table -> {
                                            table.title("Barras");
                                            table.items.set(FXCollections.observableArrayList(caixa.getItensCaixa().stream().flatMap(it -> it.getListBarra().stream()).collect(Collectors.toList())));
                                            table.expanded();
                                            table.columns(
                                                    FormTableColumn.create(cln -> {
                                                        cln.title("Barra");
                                                        cln.width(300);
                                                        cln.value((Callback<TableColumn.CellDataFeatures<SdBarraCaixaPA, SdBarraCaixaPA>, ObservableValue<SdBarraCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                                        cln.format(param -> new TableCell<SdBarraCaixaPA, SdBarraCaixaPA>() {
                                                            @Override
                                                            protected void updateItem(SdBarraCaixaPA item, boolean empty) {
                                                                super.updateItem(item, empty);
                                                                setText(null);
                                                                if (item != null && !empty) {
                                                                    setText(item.getBarra());
                                                                    getStyleClass().add("lg");
                                                                }
                                                            }
                                                        });
                                                    }).build(), /*Barra*/
                                                    FormTableColumn.create(cln -> {
                                                        cln.title("Produto");
                                                        cln.width(150);
                                                        cln.value((Callback<TableColumn.CellDataFeatures<SdBarraCaixaPA, SdBarraCaixaPA>, ObservableValue<SdBarraCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                                        cln.format(param -> new TableCell<SdBarraCaixaPA, SdBarraCaixaPA>() {
                                                            @Override
                                                            protected void updateItem(SdBarraCaixaPA item, boolean empty) {
                                                                super.updateItem(item, empty);
                                                                setText(null);
                                                                if (item != null && !empty) {
                                                                    setText(item.getProduto());
                                                                    getStyleClass().add("lg");
                                                                }
                                                            }
                                                        });
                                                    }).build(), /*Barra*/
                                                    FormTableColumn.create(cln -> {
                                                        cln.title("Tam");
                                                        cln.width(150);
                                                        cln.value((Callback<TableColumn.CellDataFeatures<SdBarraCaixaPA, SdBarraCaixaPA>, ObservableValue<SdBarraCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                                        cln.format(param -> new TableCell<SdBarraCaixaPA, SdBarraCaixaPA>() {
                                                            @Override
                                                            protected void updateItem(SdBarraCaixaPA item, boolean empty) {
                                                                super.updateItem(item, empty);
                                                                setText(null);
                                                                if (item != null && !empty) {
                                                                    setText(item.getTam());
                                                                    getStyleClass().add("lg");
                                                                }
                                                            }
                                                        });
                                                    }).build(), /*Barra*/
                                                    FormTableColumn.create(cln -> {
                                                        cln.title("Cor");
                                                        cln.width(150);
                                                        cln.value((Callback<TableColumn.CellDataFeatures<SdBarraCaixaPA, SdBarraCaixaPA>, ObservableValue<SdBarraCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                                        cln.format(param -> new TableCell<SdBarraCaixaPA, SdBarraCaixaPA>() {
                                                            @Override
                                                            protected void updateItem(SdBarraCaixaPA item, boolean empty) {
                                                                super.updateItem(item, empty);
                                                                setText(null);
                                                                if (item != null && !empty) {
                                                                    setText(item.getCor());
                                                                    getStyleClass().add("lg");
                                                                }
                                                            }
                                                        });
                                                    }).build() /*Barra*/

                                            );
                                        }).build());
                                        boxDetalhes.add(FormTableView.create(SdItemCaixaPA.class, table -> {
                                            table.title("Totais");
                                            table.items.set(FXCollections.observableArrayList(generateItensCaixa(caixa)));
                                            table.expanded();
                                            table.columns(
                                                    FormTableColumn.create(cln -> {
                                                        cln.title("Cor");
                                                        cln.width(130);
                                                        cln.value((Callback<TableColumn.CellDataFeatures<SdItemCaixaPA, SdItemCaixaPA>, ObservableValue<SdItemCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                                        cln.format(param -> new TableCell<SdItemCaixaPA, SdItemCaixaPA>() {
                                                            @Override
                                                            protected void updateItem(SdItemCaixaPA item, boolean empty) {
                                                                super.updateItem(item, empty);
                                                                setText(null);
                                                                if (item != null && !empty) {
                                                                    setText(item.getCor());
                                                                    getStyleClass().add("lg");
                                                                }
                                                            }
                                                        });
                                                    }).build(), /*Barra*/
                                                    FormTableColumn.create(cln -> {
                                                        cln.title("Tam");
                                                        cln.width(130);
                                                        cln.value((Callback<TableColumn.CellDataFeatures<SdItemCaixaPA, SdItemCaixaPA>, ObservableValue<SdItemCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                                        cln.format(param -> new TableCell<SdItemCaixaPA, SdItemCaixaPA>() {
                                                            @Override
                                                            protected void updateItem(SdItemCaixaPA item, boolean empty) {
                                                                super.updateItem(item, empty);
                                                                setText(null);
                                                                if (item != null && !empty) {
                                                                    setText(item.getTam());
                                                                    getStyleClass().add("lg");
                                                                }
                                                            }
                                                        });
                                                    }).build(), /*Barra*/
                                                    FormTableColumn.create(cln -> {
                                                        cln.title("Qtde");
                                                        cln.width(130);
                                                        cln.value((Callback<TableColumn.CellDataFeatures<SdItemCaixaPA, SdItemCaixaPA>, ObservableValue<SdItemCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                                        cln.format(param -> new TableCell<SdItemCaixaPA, SdItemCaixaPA>() {
                                                            @Override
                                                            protected void updateItem(SdItemCaixaPA item, boolean empty) {
                                                                super.updateItem(item, empty);
                                                                setText(null);
                                                                if (item != null && !empty) {
                                                                    setText(String.valueOf(item.getQtde()));
                                                                    getStyleClass().add("lg");
                                                                }
                                                            }
                                                        });
                                                    }).build() /*Barra*/
                                            );
                                        }).build());
                                    }));
                                }));
                            }));
                        }
                    }));
                }));
            }));
        });
    }

    private List<SdItemCaixaPA> generateItensCaixa(SdCaixaPA caixa) {
        List<SdItemCaixaPA> itens = new ArrayList<>();
        Map<String, Map<String, Long>> collect = caixa.getItensCaixa().stream().flatMap(it -> it.getListBarra().stream()).collect(Collectors.groupingBy(ol -> ol.getCor(), Collectors.groupingBy(eb -> eb.getTam(), counting())));
        collect.forEach((cor, map) -> {
            map.forEach((tam, qtde) -> {
                itens.add(new SdItemCaixaPA(cor, tam, Math.toIntExact(qtde)));
            });
        });
        itens.sort(Comparator.comparing(SdItemCaixaPA::getCor).thenComparing((o1, o2) -> sortTamanhos(o1.getTam(), o2.getTam())));
        return itens;
    }

    private void janelaConclusaoleituraOf() {
        new Fragment().show(frag -> {
            Button btnConcluirLeitura = FormButton.create(btn -> {
                btn.addStyle("lg").addStyle("success");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                btn.title("Concluir");
                btn.width(400);
                btn.setOnAction(evt -> {
                    ordemEscolhida.setStatus(STATUS_ITEM_CONCLUIDA);
                    removerCaixaIncompleta();
                    finalizarLeituraOp();
                    frag.close();
                });
            });
            Button btnSalvarLeitura = FormButton.create(btn -> {
                btn.addStyle("lg").addStyle("primary");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._24));
                btn.title("Salvar Leitura");
                btn.width(400);
                btn.setOnAction(evt -> {
                    ordemEscolhida.setStatus(STATUS_ITEM_ADICIONADO);
                    finalizarLeituraOp();
                    frag.close();
                });
            });
            Button btnRestantePerdida = FormButton.create(btn -> {
                btn.addStyle("lg").addStyle("warning");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCELAR_PEDIDO, ImageUtils.IconSize._24));
                btn.title("Restante Perdida");
                btn.width(400);
                btn.setOnAction(evt -> {
                    calcularTodosOsTotais();
                    criarCaixaIncompletaPerdidaRestante(true);
                    ordemEscolhida.setStatus(STATUS_ITEM_CONCLUIDA);
                    finalizarLeituraOp();
                    frag.close();
                });
            });
            Button btnRestanteIncompleta = FormButton.create(btn -> {
                btn.addStyle("lg").addStyle("danger");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.PRODUTO_FALTANTE, ImageUtils.IconSize._24));
                btn.title("Restante Incompleta");
                btn.width(400);
                btn.setOnAction(evt -> {
                    calcularTodosOsTotais();
                    criarCaixaIncompletaPerdidaRestante(false);
                    ordemEscolhida.setStatus(STATUS_ITEM_CONCLUIDA);
                    finalizarLeituraOp();
                    frag.close();
                });
            });
            Button btnSelecionarPerdidasIncompletas = FormButton.create(btn -> {
                btn.addStyle("lg").addStyle("dark");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.PRODUTO, ImageUtils.IconSize._24));
                btn.title("Selecionar Perdidas/Incompletas");
                btn.width(400);
                btn.setOnAction(evt -> {
                    if (janelaSelecionarPerdidasIncompletas()) {
                        ordemEscolhida.setStatus(STATUS_ITEM_CONCLUIDA);
                        finalizarLeituraOp();
                        frag.close();
                    }
                });
            });
            Button btnLeituraSegunda = FormButton.create(btn -> {
                btn.addStyle("lg").addStyle("info");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CAIXA_ABERTA2, ImageUtils.IconSize._24));
                btn.title("Leitura Segunda");
                btn.disable.set(isLeituraSegunda.get());
                btn.width(400);
                btn.setOnAction(evt -> {
                    setModeLeituraSegunda();
                    frag.close();
                });
            });
            frag.title("Conclusão da Leitura");
            frag.size(400.0, 230.0);

            frag.getCancelButton().getStyleClass().add("lg");

            frag.box.getChildren().add(FormBox.create(principal -> {
                principal.vertical();
                principal.expanded();
                principal.alignment(Pos.CENTER);
                principal.add(btnConcluirLeitura);
                principal.add(btnSalvarLeitura);
                if (colaborador.isPerdidasAtivas()) principal.add(btnRestantePerdida);
                if (colaborador.isIncompletasAtivas()) principal.add(btnRestanteIncompleta);
                if (colaborador.isIncompletasAtivas() || colaborador.isPerdidasAtivas())
                    principal.add(btnSelecionarPerdidasIncompletas);
                principal.add(btnLeituraSegunda);
            }));
        });
    }

    private boolean janelaSelecionarPerdidasIncompletas() {
        AtomicBoolean result = new AtomicBoolean(false);
        List<SdBarraCaixaPA> barrasCaixa = new ArrayList<>();
        try {
            VSdDadosOfOsPendente of = getOfPendente(ordemEscolhida);
            of.getCores().forEach(itemOf -> {
                List<SdItemLotePA> itensEmTransito = (List<SdItemLotePA>) new FluentDao().selectFrom(SdItemLotePA.class).where(it -> it
                        .equal("numero", of.getNumero())
                        .isIn("lote.status.codigo", new String[]{"4", "5"})
                        .notEqual("lote.id", loteEscolhido.getId())).resultList();
                int qtdeDesconto = 0;

                if (itensEmTransito != null && itensEmTransito.size() > 0) {

                    for (SdItemLotePA itemEmTransito : itensEmTransito) {
                        qtdeDesconto += itemEmTransito.getListCaixas().stream()
                                .filter(it -> !it.isRecebida())
                                .mapToInt(it -> it.getItensCaixa().stream()
                                        .filter(eb ->
                                                eb.getCor().equals(itemOf.getId().getCor()) &&
                                                        eb.getTam().equals(itemOf.getId().getTam()) &&
                                                        eb.getProduto().equals(itemOf.getId().getCodigo())
                                        ).mapToInt(SdItemCaixaPA::getQtde).sum()).sum();
                    }
                }
                int qtdeLida = ordemEscolhida.getListCaixas()
                        .stream()
                        .filter(cx -> cx.getProduto().equals(itemOf.getId().getCodigo()))
                        .flatMap(rb -> rb.getItensCaixa().stream().filter(eb ->
                                eb.getCor().equals(itemOf.getId().getCor()) &&
                                        eb.getTam().equals(itemOf.getId().getTam())
                        )).mapToInt(SdItemCaixaPA::getQtde).sum();
                if (itemOf.getQtde() > (qtdeLida + qtdeDesconto)) {
                    for (int i = 0; i < itemOf.getQtde(); i++) {
                        barrasCaixa.add(new SdBarraCaixaPA(itemOf.getId().getCodigo(), itemOf.getId().getCor(), itemOf.getId().getTam()));
                    }
                }
            });

            new Fragment().show(frag -> {
                frag.size(700.0, 700.0);
                frag.title("Escolha Produtos Incompletos/Perdidos");
                ListProperty barrasIncBean = new SimpleListProperty(FXCollections.observableArrayList(barrasCaixa.stream()
                        .sorted(
                                Comparator.comparing(SdBarraCaixaPA::getProduto)
                                        .thenComparing(SdBarraCaixaPA::getCor)
                                        .thenComparing((o1, o2) -> SortUtils.sortTamanhos(o1.getTam(), o2.getTam()))
                        )
                        .collect(Collectors.toList())));
                final FormTableView<SdBarraCaixaPA> tblBarras = FormTableView.create(SdBarraCaixaPA.class, table -> {
                    table.title("Barras");
                    table.expanded();
                    table.items.bind(barrasIncBean);
                    table.columns(
                            FormTableColumn.create(cln -> {
                                cln.title("Código");
                                cln.width(200);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdBarraCaixaPA, SdBarraCaixaPA>, ObservableValue<SdBarraCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                cln.format(param -> new TableCell<SdBarraCaixaPA, SdBarraCaixaPA>() {
                                    @Override
                                    protected void updateItem(SdBarraCaixaPA item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        if (item != null && !empty) {
                                            setText(item.getProduto());
                                            getStyleClass().add("lg");
                                        }
                                    }
                                });
                            }).build(),
                            FormTableColumn.create(cln -> {
                                cln.title("Cor");
                                cln.width(150);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdBarraCaixaPA, SdBarraCaixaPA>, ObservableValue<SdBarraCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                cln.format(param -> new TableCell<SdBarraCaixaPA, SdBarraCaixaPA>() {
                                    @Override
                                    protected void updateItem(SdBarraCaixaPA item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        if (item != null && !empty) {
                                            setText(item.getCor());
                                            getStyleClass().add("lg");
                                        }
                                    }
                                });
                            }).build(),
                            FormTableColumn.create(cln -> {
                                cln.title("Tam");
                                cln.width(100);
                                cln.alignment(Pos.CENTER);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdBarraCaixaPA, SdBarraCaixaPA>, ObservableValue<SdBarraCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                cln.format(param -> new TableCell<SdBarraCaixaPA, SdBarraCaixaPA>() {
                                    @Override
                                    protected void updateItem(SdBarraCaixaPA item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        if (item != null && !empty) {
                                            setText(item.getTam());
                                            getStyleClass().add("lg");
                                        }
                                    }
                                });
                            }).build(),
                            FormTableColumn.create(cln -> {
                                cln.title("Incompleta");
                                cln.width(70);
                                cln.alignment(Pos.CENTER);
                                cln.setVisible(colaborador.isIncompletasAtivas());
                                cln.value((Callback<TableColumn.CellDataFeatures<SdBarraCaixaPA, SdBarraCaixaPA>, ObservableValue<SdBarraCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                cln.format(param -> new TableCell<SdBarraCaixaPA, SdBarraCaixaPA>() {
                                    @Override
                                    protected void updateItem(SdBarraCaixaPA item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        setGraphic(null);
                                        if (item != null && !empty) {
                                            FormButton btnInc = FormButton.create(btn -> {
                                                btn.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                                btn.addStyle("danger").addStyle("lg");
                                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
                                                btn.setAlignment(Pos.CENTER);
                                                btn.setOnAction(evt -> {
                                                    item.setIncompleta(!item.isIncompleta());
                                                });
                                                item.incompletaProperty().addListener((observable, oldValue, newValue) -> {
                                                    if (newValue) {
                                                        btn.removeStyle("danger");
                                                        btn.addStyle("success");
                                                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._24));
                                                    } else {
                                                        btn.removeStyle("success");
                                                        btn.addStyle("danger");
                                                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
                                                    }
                                                });

                                                item.perdidaProperty().addListener((observable, oldValue, newValue) -> {
                                                    if (newValue) item.setIncompleta(false);
                                                });
                                            });
                                            setGraphic(btnInc);
                                        }
                                    }
                                });
                            }).build(),
                            FormTableColumn.create(cln -> {
                                cln.title("Perdida");
                                cln.width(70);
                                cln.alignment(Pos.CENTER);
                                cln.setVisible(colaborador.isPerdidasAtivas());
                                cln.value((Callback<TableColumn.CellDataFeatures<SdBarraCaixaPA, SdBarraCaixaPA>, ObservableValue<SdBarraCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                cln.format(param -> new TableCell<SdBarraCaixaPA, SdBarraCaixaPA>() {
                                    @Override
                                    protected void updateItem(SdBarraCaixaPA item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        setGraphic(null);
                                        if (item != null && !empty) {
                                            FormButton btnPerd = FormButton.create(btn -> {
                                                btn.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                                btn.addStyle("danger").addStyle("lg");
                                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
                                                btn.setAlignment(Pos.CENTER);
                                                btn.setOnAction(evt -> {
                                                    item.setPerdida(!item.isPerdida());
                                                });
                                                item.perdidaProperty().addListener((observable, oldValue, newValue) -> {
                                                    if (newValue) {
                                                        btn.removeStyle("danger");
                                                        btn.addStyle("success");
                                                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._24));
                                                    } else {
                                                        btn.removeStyle("success");
                                                        btn.addStyle("danger");
                                                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
                                                    }
                                                });

                                                item.incompletaProperty().addListener((observable, oldValue, newValue) -> {
                                                    if (newValue) item.setPerdida(false);
                                                });
                                            });
                                            setGraphic(btnPerd);
                                        }
                                    }
                                });
                            }).build()
                    );
                });

                frag.buttonsBox.getChildren().add(FormButton.create(btn -> {
                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                    btn.addStyle("success").addStyle("lg");
                    btn.title("Confirmar");
                    btn.setOnAction(evt -> {
                        result.set(true);
                        List<SdBarraCaixaPA> itensPerdidos = tblBarras.items.stream().filter(SdBarraCaixaPA::isPerdida).collect(Collectors.toList());
                        List<SdBarraCaixaPA> itensIncompletos = tblBarras.items.stream().filter(SdBarraCaixaPA::isIncompleta).collect(Collectors.toList());

                        calcularTodosOsTotais();
                        criarCaixaIncompletaPerdida(itensPerdidos, true);
                        criarCaixaIncompletaPerdida(itensIncompletos, false);

                        frag.close();
                    });
                }));

                frag.getCancelButton().getStyleClass().add("lg");

                frag.box.getChildren().add(FormBox.create(principal -> {
                    principal.vertical();
                    principal.expanded();
                    principal.add(tblBarras.build());
                }));
            });
        } catch (NullPointerException e) {
            e.printStackTrace();
            MessageBox.create(message -> {
                message.message("Of digitada não está pendente.");
                message.type(MessageBox.TypeMessageBox.BLOCKED);
                message.showAndWait();
            });
        }
        return result.get();
    }

    private boolean janelaEncerramentoLote() {
        AtomicReference<Boolean> retorno = new AtomicReference<>(false);
        new Fragment().show(frag -> {
            Button btnEncerrar = FormButton.create(btn -> {
                btn.addStyle("lg").addStyle("warning");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CAIXA, ImageUtils.IconSize._24));
                btn.title("Encerrar Lote");
                btn.width(300);
                btn.setOnAction(evt -> {
                    loteEscolhido.setStatus(STATUS_ENCERRADO);
                    retorno.set(true);
                    frag.close();
                });
            });
            Button btnEmTransporte = FormButton.create(btn -> {
                btn.addStyle("lg").addStyle("danger");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.TRANSPORTADORA, ImageUtils.IconSize._24));
                btn.title("Em transporte");
                btn.width(300);
                btn.setOnAction(evt -> {
                    loteEscolhido.setStatus(STATUS_EM_TRANSPORTE);
                    loteEscolhido.setDtemissao(LocalDate.now());
                    retorno.set(true);
                    frag.close();
                });
            });
            Button btnImprimirRomaneio = FormButton.create(btn -> {
                btn.addStyle("lg").addStyle("primary");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.IMPRIMIR, ImageUtils.IconSize._24));
                btn.title("Romaneio Simples");
                btn.width(300);
                btn.setOnAction(evt -> {
                    if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                        message.message("Ao imprimir o romaneio você está definindo o lote como \"Em Transporte\". \nDeseja prosseguir?");
                        message.showAndWait();
                    }).value.get())) {
                        loteEscolhido.setStatus(STATUS_EM_TRANSPORTE);
                        loteEscolhido.setDtemissao(LocalDate.now());
                        imprimirRomaneio();
                        retorno.set(true);
                        frag.close();
                    }
                });
            });
            Button btnImprimirGrade = FormButton.create(btn -> {
                btn.addStyle("lg").addStyle("info");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.IMPRIMIR, ImageUtils.IconSize._24));
                btn.title("Romaneio + Grade");
                btn.width(300);
                btn.setOnAction(evt -> {
                    if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                        message.message("Ao imprimir o romaneio você está definindo o lote como \"Em Transporte\". \nDeseja prosseguir?");
                        message.showAndWait();
                    }).value.get())) {
                        loteEscolhido.setStatus(STATUS_EM_TRANSPORTE);
                        loteEscolhido.setDtemissao(LocalDate.now());
                        imprimirRomaneioGrade();
                        retorno.set(true);
                        frag.close();
                    }
                });
            });

            frag.title("Encerramento do lote");
            frag.size(300.0, 230.0);

            frag.box.getChildren().add(FormBox.create(principal -> {
                principal.vertical();
                principal.expanded();
                principal.alignment(Pos.CENTER);
                principal.add(btnEncerrar, btnImprimirRomaneio, btnImprimirGrade, btnEmTransporte);
            }));
        });
        return retorno.get();
    }

    // </editor-fold>

    //<editor-fold desc="Utils">
    private void setModeLeituraSegunda() {
        barrasBean.clear();
        isLeituraSegunda.set(true);
        isExcluirBarra.set(false);
        adicionarNovaCaixa();
        barraLeituraField.requestFocus();
        isConclusaoLeitura.set(true);
        exibirCaixasOp();
    }

    private boolean hasItensPendentes() {
        return listProdutosOrdem.stream().anyMatch(it -> getSomaBarrasLidasTotal(it) < it.getQtde());
    }

    private void handleStatusLeitura() {
        lblStatusLeitura.textField.getStyleClass().removeAll(Arrays.asList("danger", "info", "warning", "success"));
        if (isApenasLeitura.get()) {
            addStyleAllFields("warning");
            lblStatusLeitura.addStyle("warning");
            lblStatusLeitura.value.setValue("VISUALIZAÇÃO");
            return;
        }
        removeStyleAllFields("warning");

        if (isExcluirBarra.not().get()) {
            removeStyleAllFields("danger");
            if (isLeituraSegunda.get()) {
                addStyleAllFields("info");
                lblStatusLeitura.addStyle("info");
                lblStatusLeitura.value.setValue("ADICIONANDO SEGUNDA");
            } else {
                removeStyleAllFields("info");
                lblStatusLeitura.addStyle("success");
                lblStatusLeitura.value.setValue("ADICIONANDO");
            }
        } else {
            addStyleAllFields("danger");
            lblStatusLeitura.addStyle("danger");
            if (isLeituraSegunda.get()) {
                removeStyleAllFields("info");
                lblStatusLeitura.value.setValue("REMOVENDO SEGUNDA");
            } else {
                lblStatusLeitura.value.setValue("REMOVENDO");
            }
        }

        tblListBarrasLidas.refresh();
    }

    private void refreshTblBarras() {
        if (caixaEscolhida != null) {
            barrasBean.set(FXCollections.observableArrayList(caixaEscolhida.getItensCaixa().stream().flatMap(it -> it.getListBarra().stream()).collect(Collectors.toList())));
        } else {
            tblListBarrasLidas.clear();
        }
    }

    private void liberarAdiciaoOp() {
        if (loteEscolhido != null) {
            emAdicaoOps.set(true);
            emLeituraItens.set(false);
            loteEscolhido.getItensLote().forEach(it -> it.setSelecionado(false));
            exibirOpsLote();
        }
    }

    private boolean ofEmLeituraExpedicao(String numeroOf) {
        List<SdItemLotePA> itensComOf = (List<SdItemLotePA>) new FluentDao()
                .selectFrom(SdItemLotePA.class)
                .where(it -> it
                        .equal("numero", numeroOf)
//                        .notEqual("lote.id", itemLote.getLote().getId())
                        .equal("lote.status.codigo", "5"))
                .resultList();
        return itensComOf != null && itensComOf.size() != 0 && !itensComOf.stream().flatMap(it -> it.getListCaixas().stream()).allMatch(SdCaixaPA::isRecebida);
    }

    private void preencherCamposProduto(PaIten paIten) {
        Produto produto = new FluentDao().selectFrom(Produto.class).where(it -> it.equal("codigo", paIten.getId().getCodigo())).singleResult();
        codigoField.value.setValue(paIten.getId().getCodigo() == null ? "" : paIten.getId().getCodigo());
        descProdutoField.value.setValue(produto == null ? "" : produto.getDescricao());
        tamanhoField.value.setValue(paIten.getId().getTam() == null ? "" : paIten.getId().getTam());
        corField.value.setValue(paIten.getId().getCor() == null ? "" : paIten.getId().getCor());
    }

    private void criaTabsCores(SdItemLotePA itemLote) {
        tabPaneCores.getTabs().clear();
        VSdDadosOfOsPendente of = getOfPendente(itemLote);
        if (of == null) {
            MessageBox.create(message -> {
                message.message("A OF digitada não se encontra como pendente.");
                message.type(MessageBox.TypeMessageBox.BLOCKED);
                message.showAndWait();
            });
            return;
        }
        List<String> cores = of.getCores().stream().map(it -> it.getId().getCor()).distinct().sorted(Comparator.comparing(Integer::valueOf)).collect(Collectors.toList());

        listProdutosOrdem.clear();
        for (String cor : cores) {
            List<VSdDadosOfOsPendenteSKU> listGrade = of.getCores().stream().filter(it -> it.getId().getCor().equals(cor)).collect(Collectors.toList());
            SdItemLotePA itemEmTransito = new FluentDao().selectFrom(SdItemLotePA.class).where(it -> it
                    .equal("numero", of.getNumero())
                    .isIn("lote.status.codigo", new String[]{"4", "5"})
                    .notEqual("lote.id", loteEscolhido.getId())).singleResult();

            if (itemEmTransito != null) {
                listGrade.removeIf(it -> it.getQtde() <= itemEmTransito.getListCaixas()
                        .stream().filter(eb -> !eb.isRecebida()).mapToInt(eb -> eb.getItensCaixa().stream().filter(el -> el.getCor().equals(it.getId().getCor()) && el.getTam().equals(it.getId().getTam())).mapToInt(SdItemCaixaPA::getQtde).sum()).sum());
            }

            listProdutosOrdem.addAll(listGrade);
            if (listGrade.size() == 0) {
                continue;
            }
            ListProperty<VSdDadosOfOsPendenteSKU> itens = new SimpleListProperty<>(FXCollections.observableArrayList(sortListaTamanhosGrade(listGrade)));
            if (tabPaneCores.getTabs().stream().noneMatch(it -> it.getText().equals(cor))) {
                FormTab formTab = FormTab.create(tab -> {
                    tab.title(cor);
                    tab.setId(cor);
                    tab.add(FormBox.create(principal -> {
                        principal.horizontal();
                        principal.expanded();
                        principal.add(FormTableView.create(VSdDadosOfOsPendenteSKU.class, table -> {
                            table.tableview().setId(cor);
                            table.items.set(itens);
                            table.withoutHeader();
                            lidoOP.addListener((observable, oldValue, newValue) -> {
                                table.refresh();
                            });
//                            barraLeituraField.value.addListener((observable, oldValue, newValue) -> {
//                                if (newValue == null || newValue.equals("")) {
//                                    table.refresh();
//                                }
//                            });
                            isLeituraSegunda.addListener((observable, oldValue, newValue) -> {
                                table.refresh();
                            });
                            table.factoryRow(param -> new TableRow<VSdDadosOfOsPendenteSKU>() {
                                @Override
                                protected void updateItem(VSdDadosOfOsPendenteSKU item, boolean empty) {
                                    super.updateItem(item, empty);
                                    clear();

                                    if ((item != null) && isLeituraSegunda.get() && (item.getQtde() - getSomaBarrasLidasTotal(item)) <= 0) {
                                        table.items.remove(item);
                                    }
                                    int qtdeDesconto = 0;
                                    if (item != null && !empty) {
                                        if (itemEmTransito != null) {
                                            qtdeDesconto = itemEmTransito.getListCaixas().stream().filter(it -> !it.isRecebida())
                                                    .mapToInt(it -> it.getItensCaixa().stream()
                                                            .filter(eb ->
                                                                    eb.getCor().equals(item.getId().getCor()) &&
                                                                            eb.getTam().equals(item.getId().getTam()) &&
                                                                            eb.getProduto().equals(item.getId().getCodigo())
                                                            ).mapToInt(SdItemCaixaPA::getQtde).sum()).sum();
                                        }

                                        if (isLeituraSegunda.get()) {
                                            if (getSomaBarrasLidasSegunda(item) == (item.getQtde() - getSomaBarrasLidasBoas(item) - qtdeDesconto)) {
                                                getStyleClass().add("table-row-success");
                                            } else {
                                                getStyleClass().add("table-row-info");
                                            }
                                        } else {
                                            if (getSomaBarrasLidasTotal(item) == (item.getQtde() - qtdeDesconto)) {
                                                getStyleClass().add("table-row-success");
                                            } else {
                                                getStyleClass().add("table-row-warning");
                                            }
                                        }
                                        table.refresh();
                                    }
                                }

                                private void clear() {
                                    getStyleClass().removeAll("table-row-danger", "table-row-success", "table-row-warning");
                                }
                            });
                            table.columns(
                                    FormTableColumn.create(cln -> {
                                        cln.title("Tamanho");
                                        cln.width(100);
                                        cln.alignment(Pos.CENTER);
                                        cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfOsPendenteSKU, VSdDadosOfOsPendenteSKU>, ObservableValue<VSdDadosOfOsPendenteSKU>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId()));
                                        cln.format(param -> new TableCell<VSdDadosOfOsPendenteSKUPK, VSdDadosOfOsPendenteSKUPK>() {
                                            @Override
                                            protected void updateItem(VSdDadosOfOsPendenteSKUPK item, boolean empty) {
                                                super.updateItem(item, empty);
                                                setText(null);
                                                if (item != null && !empty) {
                                                    setText(item.getTam());
                                                    getStyleClass().add("lg");
                                                }
                                            }
                                        });
                                    }).build(), /*Tamanho*/
                                    FormTableColumn.create(cln -> {
                                        cln.title("Qtde Lida");
                                        cln.width(100);
                                        cln.alignment(Pos.CENTER);
                                        cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfOsPendenteSKU, VSdDadosOfOsPendenteSKU>, ObservableValue<VSdDadosOfOsPendenteSKU>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                        cln.format(param -> new TableCell<VSdDadosOfOsPendenteSKU, VSdDadosOfOsPendenteSKU>() {
                                            @Override
                                            protected void updateItem(VSdDadosOfOsPendenteSKU item, boolean empty) {
                                                super.updateItem(item, empty);
                                                setText(null);
                                                if (item != null && !empty) {
                                                    setText(String.valueOf(isLeituraSegunda.get() ? getSomaBarrasLidasSegunda(item) : getSomaBarrasLidasTotal(item)));
                                                    getStyleClass().add("lg");
                                                }
                                            }
                                        });
                                    }).build(), /*Qtde*/
                                    FormTableColumn.create(cln -> {
                                        cln.title("Qtde Total");
                                        cln.width(100);
                                        cln.alignment(Pos.CENTER);
                                        cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfOsPendenteSKU, VSdDadosOfOsPendenteSKU>, ObservableValue<VSdDadosOfOsPendenteSKU>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                        cln.format(param -> new TableCell<VSdDadosOfOsPendenteSKU, VSdDadosOfOsPendenteSKU>() {
                                            @Override
                                            protected void updateItem(VSdDadosOfOsPendenteSKU item, boolean empty) {
                                                super.updateItem(item, empty);
                                                setText(null);
                                                int qtdeDesconto = 0;
                                                if (item != null) {
                                                    if (itemEmTransito != null) {
                                                        qtdeDesconto = itemEmTransito.getListCaixas().stream().filter(it -> !it.isRecebida())
                                                                .mapToInt(it -> it.getItensCaixa().stream()
                                                                        .filter(eb ->
                                                                                eb.getCor().equals(item.getId().getCor()) &&
                                                                                        eb.getTam().equals(item.getId().getTam()) &&
                                                                                        eb.getProduto().equals(item.getId().getCodigo())
                                                                        ).mapToInt(SdItemCaixaPA::getQtde).sum()).sum();
                                                    }
                                                    if ((isLeituraSegunda.get() && (item.getQtde() - getSomaBarrasLidasBoas(item) - qtdeDesconto <= 0))) {
                                                        table.items.remove(item);
                                                        table.refresh();
                                                    }
                                                }

                                                if (item != null && !empty) {
                                                    setText(String.valueOf(isLeituraSegunda.get() ? item.getQtde() - getSomaBarrasLidasBoas(item) - qtdeDesconto : item.getQtde() - qtdeDesconto));
                                                    getStyleClass().add("lg");
                                                }
                                            }
                                        });
                                    }).build() /*Qtde*/
                            );
                        }).build());
                    }));
                });
                tabPaneCores.addTab(formTab);
            }
        }
    }
    //</editor-fold>
}
