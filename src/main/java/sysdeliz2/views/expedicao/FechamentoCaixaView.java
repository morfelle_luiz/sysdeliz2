package sysdeliz2.views.expedicao;

import javafx.application.Platform;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import jssc.SerialPortException;
import net.sf.jasperreports.engine.JRException;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import sysdeliz2.controllers.views.expedicao.FechamentoCaixaController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.sysdeliz.*;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.ReportUtils;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.exceptions.FormValidationException;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import javax.print.PrintException;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.concurrent.atomic.AtomicReference;

public class FechamentoCaixaView extends FechamentoCaixaController {

    private static final Logger logger = Logger.getLogger(FechamentoCaixaView.class);
    // <editor-fold defaultstate="collapsed" desc="beanController">
    private Boolean podeExcluirBarra = false;
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="view">
    private final VBox abaFechamento = (VBox) super.tabs.getTabs().get(0).getContent();
    private final VBox abaCaixasRemessa = (VBox) super.tabs.getTabs().get(1).getContent();
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fields">
    // fields aba fechamento
    private final FormFieldText fieldNumeroCaixa = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label(ImageUtils.getIcon(ImageUtils.Icon.LEITURA_BARRA, ImageUtils.IconSize._32));
        field.toUpper();
        field.addStyle("xl");
        field.width(300.0);
        field.alignment(Pos.CENTER);
        field.keyReleased(evt -> fecharCaixa(evt));
    });
    private final FormFieldText fieldNumeroCaixaLida = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("CAIXA");
        field.toUpper();
        field.addStyle("xl").addStyle("amber");
        field.width(280.0);
        field.editable(false);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldNumeroNotaFiscal = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("NF");
        field.addStyle("xl");
        field.width(250.0);
        field.editable(false);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldNumeroPalete = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("PALLET");
        field.addStyle("xl");
        field.width(150.0);
        field.editable(false);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldPesoCaixa = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("PESO");
        field.postLabel("Kg");
        field.addStyle("xl").addStyle("primary");
        field.width(270.0);
        field.editable(false);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldTransportadoraCaixa = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("TRANSPORTADORA");
        field.addStyle("xl");
        field.width(650.0);
        field.editable(false);
        field.alignment(Pos.CENTER_LEFT);
    });
    private final FormFieldText fieldMensagemBraspress = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("RETORNO");
        field.addStyle("xl").addStyle("danger");
        field.width(650.0);
        field.editable(false);
        field.alignment(Pos.CENTER_LEFT);
    });
    private final FormBox boxMensagemBraspress = FormBox.create(box -> {
        box.horizontal();
    });
    // fields aba caixas remessa
    private final FormFieldText fieldRemessa = FormFieldText.create(field -> {
        field.title("Remessa");
        field.addStyle("lg");
    });
    private final FormButton btnCarregarRemessa = FormButton.create(btn -> {
        btn.title("Carregar");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
        btn.addStyle("success");
        btn.setAction(evt -> carregarRemessa());
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="tables">
    private final FormTableView<SdItensCaixaRemessa> tblItensCaixa = FormTableView.create(SdItensCaixaRemessa.class, table -> {
        table.title("Itens da Caixa");
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Codigo");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItensCaixaRemessa, SdItensCaixaRemessa>, ObservableValue<SdItensCaixaRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getCodigo()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Codigo*/,
                FormTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(250.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItensCaixaRemessa, SdItensCaixaRemessa>, ObservableValue<SdItensCaixaRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getDescricao()));
                }).build() /*Descrição*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cor");
                    cln.width(60.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItensCaixaRemessa, SdItensCaixaRemessa>, ObservableValue<SdItensCaixaRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCor()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Cor*/,
                FormTableColumn.create(cln -> {
                    cln.title("Tam");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItensCaixaRemessa, SdItensCaixaRemessa>, ObservableValue<SdItensCaixaRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTam()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Tam*/,
                FormTableColumn.create(cln -> {
                    cln.title("Barra");
                    cln.width(150.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItensCaixaRemessa, SdItensCaixaRemessa>, ObservableValue<SdItensCaixaRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getBarra()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Barra*/,
                FormTableColumn.create(cln -> {
                    cln.title("Ações");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(60.0);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItensCaixaRemessa, SdItensCaixaRemessa>, ObservableValue<SdItensCaixaRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdItensCaixaRemessa, SdItensCaixaRemessa>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnExcluirBarra = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                btn.tooltip("Excluir barra da caixa");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("danger");
                            });

                            @Override
                            protected void updateItem(SdItensCaixaRemessa item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    btnExcluirBarra.setOnAction(evt -> {
                                        excluirBarraCaixa(item);
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    if (podeExcluirBarra)
                                        boxButtonsRow.getChildren().addAll(btnExcluirBarra);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build()
        );
    });
    private final FormTableView<SdCaixaRemessa> tblCaixasRemessa = FormTableView.create(SdCaixaRemessa.class, table -> {
        table.title("Caixas Remessa");
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Caixa");
                    cln.width(90.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCaixaRemessa, SdCaixaRemessa>, ObservableValue<SdCaixaRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumero()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Caixa*/,
                FormTableColumn.create(cln -> {
                    cln.title("Volume");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCaixaRemessa, SdCaixaRemessa>, ObservableValue<SdCaixaRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getVolume()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Volume*/,
                FormTableColumn.create(cln -> {
                    cln.title("Peso");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCaixaRemessa, SdCaixaRemessa>, ObservableValue<SdCaixaRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPeso()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<SdCaixaRemessa, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDecimalFormat(item) + " kg");
                                }
                            }
                        };
                    });
                }).build() /*Peso*/,
                FormTableColumn.create(cln -> {
                    cln.title("Qtde");
                    cln.width(60.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCaixaRemessa, SdCaixaRemessa>, ObservableValue<SdCaixaRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Qtde*/,
                FormTableColumn.create(cln -> {
                    cln.title("Nota");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCaixaRemessa, SdCaixaRemessa>, ObservableValue<SdCaixaRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNota()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Nota*/,
                FormTableColumn.create(cln -> {
                    cln.title("Ações");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(60.0);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCaixaRemessa, SdCaixaRemessa>, ObservableValue<SdCaixaRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdCaixaRemessa, SdCaixaRemessa>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnExcluirCaixa = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                btn.tooltip("Excluir Caixa");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("danger");
                            });

                            @Override
                            protected void updateItem(SdCaixaRemessa item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {

                                    btnExcluirCaixa.setOnAction(evt -> {
                                        excluirCaixaRemessa(item);
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    if (item.getNota() == null)
                                        boxButtonsRow.getChildren().addAll(btnExcluirCaixa);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build()
        );
        table.selectionModelItem((observable, oldValue, newValue) -> {
            if (newValue != null) {
                if (((SdCaixaRemessa) newValue).getNota() != null)
                    podeExcluirBarra = false;
                else
                    podeExcluirBarra = true;

                tblItensCaixa.items.set(FXCollections.observableList(((SdCaixaRemessa) newValue).getItens()));
            }
        });
        table.factoryRow(param -> {
            return new TableRow<SdCaixaRemessa>() {
                @Override
                protected void updateItem(SdCaixaRemessa item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item != null && !empty) {
                        setContextMenu(FormContextMenu.create(menu -> {
                            menu.addItem(menuItem -> {
                                menuItem.setText("Re-Imprimir Etiqueta");
                                menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.IMPRIMIR_TAG, ImageUtils.IconSize._16));
                                menuItem.setOnAction(evt -> {
                                    String zplCodeEtiqueta = "";
                                    try {
                                        _IMPRIME_BRASPRESS.refresh();
                                        if (_IMPRIME_BRASPRESS.getValor().equals("S") && item.getRemessa().getPedidos().get(0).getId().getNumero().getTabTrans().getNome().toUpperCase().contains("BRASPRESS")) {
                                            zplCodeEtiqueta = criarEtiquetaCaixaBrasspress(item, consultaCaixasRemessa);
                                        } else {
                                            zplCodeEtiqueta = criarEtiquetaCaixaDeliz(item, consultaCaixasRemessa);
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                        ExceptionBox.build(message -> {
                                            message.exception(e);
                                            message.showAndWait();
                                        });
                                        zplCodeEtiqueta = criarEtiquetaCaixaDeliz(item, consultaCaixasRemessa);
                                    }
                                    //criarEtiquetaCaixaDeliz(item, consultaCaixasRemessa);
                                    System.out.println(zplCodeEtiqueta);
                                    try {
                                        new ReportUtils().config().view().printZplCode(zplCodeEtiqueta);
                                        if (item.getRemessa().getCodcli().isImprimeConteudoCaixa()) {
                                            new ReportUtils().config()
                                                    .addReport(ReportUtils.ReportFile.ETIQUETA_CONTEUDO_CAIXA_EXPEDICAO, new ReportUtils.ParameterReport("numero", item.getNumero()))
                                                    .view().print();
                                        }
                                    } catch (PrintException e) {
                                        MessageBox.create(message -> {
                                            message.message(e.getMessage());
                                            message.type(MessageBox.TypeMessageBox.ERROR);
                                            message.showFullScreen();
                                        });
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                        ExceptionBox.build(message -> {
                                            message.exception(e);
                                            message.showAndWait();
                                        });
                                    } catch (SQLException e) {
                                        e.printStackTrace();
                                        ExceptionBox.build(message -> {
                                            message.exception(e);
                                            message.showAndWait();
                                        });
                                    } catch (JRException e) {
                                        e.printStackTrace();
                                        ExceptionBox.build(message -> {
                                            message.exception(e);
                                            message.showAndWait();
                                        });
                                    }
                                });
                            });
                        }));
                    }
                }
            };
        });
    });
    // </editor-fold>

    public FechamentoCaixaView() {
        super("Fechamento de Caixa", ImageUtils.getImage(ImageUtils.Icon.FECHAR_CAIXA), new String[]{"Fechamento", "Caixas Remessa"}, Globals.isPortatil());

        JPAUtils.getEntityManager();
        try {
            serialPort.openPort();
            serialPort.setParams(9600, 8, 1, 0);
            byte[] buffer = serialPort.readBytes(12);
            String convertido = new String(buffer);
            serialPort.closePort();
            logger.log(Priority.INFO, "Leitura da SERIAL COM3 iniciada com sucesso.");
        } catch (SerialPortException e) {
            e.printStackTrace();
            logger.log(Priority.ERROR, "Erro na leitura da SERIAL COM3", e);
        }

        if (Globals.isPortatil()) {
            repositor = getFechador(Globals.getNomeUsuario());
            if (repositor == null) {
                MessageBox.create(message -> {
                    message.message("Usuário não habilitado para fechamento de caixas.");
                    message.type(MessageBox.TypeMessageBox.ALERT);
                    message.showAndWait();
                });
                throw new FormValidationException("Fechador não existe!");
            } else {
                SysLogger.addSysDelizLog("Fechamento Caixa", TipoAcao.ENTRAR, String.valueOf(repositor.getCodigo()), "Fechador " + repositor.getNome() + " entrou na tela de fechamento.");
                init();
                Platform.runLater(fieldNumeroCaixa::requestFocus);
            }
        } else {
            init();
        }
    }

    private void init() {
        initAbaFechamento();
        initAbaCaixasRemessa();
    }

    private void initAbaFechamento() {
        abaFechamento.getChildren().add(FormBox.create(principal -> {
            principal.vertical();
            principal.expanded();
            principal.add(FormBox.create(header -> {
                header.horizontal();
                header.add(fieldNumeroCaixa.build());
            }));
            principal.add(new Separator(Orientation.HORIZONTAL));
            principal.add(FormBox.create(container -> {
                container.horizontal();
                container.add(fieldNumeroCaixaLida.build());
            }));
            principal.add(FormBox.create(container -> {
                container.horizontal();
                container.add(fieldNumeroNotaFiscal.build());
                container.add(fieldNumeroPalete.build());
            }));
            principal.add(FormBox.create(container -> {
                container.horizontal();
                container.add(fieldPesoCaixa.build());
            }));
            principal.add(FormBox.create(container -> {
                container.horizontal();
                container.add(fieldTransportadoraCaixa.build());
            }));
            principal.add(boxMensagemBraspress);
        }));
    }

    private void initAbaCaixasRemessa() {
        abaCaixasRemessa.getChildren().add(FormBox.create(boxPrincipal -> {
            boxPrincipal.vertical();
            boxPrincipal.expanded();
            boxPrincipal.add(FormBox.create(boxHeader -> {
                boxHeader.horizontal();
                boxHeader.alignment(Pos.BOTTOM_LEFT);
                boxHeader.add(fieldRemessa.build(), btnCarregarRemessa);
            }));
            boxPrincipal.add(FormBox.create(boxContainer -> {
                boxContainer.horizontal();
                boxContainer.expanded();
                boxContainer.add(tblCaixasRemessa.build(), tblItensCaixa.build());
            }));

        }));
    }

    private void fecharCaixa(KeyEvent evt) {
        if (evt.getCode().equals(KeyCode.ENTER)) {
            if (fieldNumeroCaixa.value.get().startsWith("CX") && fieldNumeroCaixa.value.get().length() == 8) {
                logger.log(Priority.INFO, "Validação do número da caixa realizada com sucesso!");

                // preparando dados para exibição da caixa lida
                limparDados();
                fieldNumeroCaixaLida.value.set(fieldNumeroCaixa.value.get().replace("CX", ""));

                // leitura da balança com o peso da caixa
                BigDecimal pesoCaixaLida = BigDecimal.ZERO;
                try {
                    do {
                        pesoCaixaLida = lerPeso();
                    }
                    while (pesoCaixaLida.compareTo(BigDecimal.ZERO) == 0);
                    logger.log(Priority.INFO, "Leitura do peso da caixa realizada com sucesso!");
                    SysLogger.addSysDelizLog("Fechamento Caixa", TipoAcao.EDITAR, String.valueOf(fieldNumeroCaixaLida.value.get()), "Leitura da balança para a caixa, peso: " + pesoCaixaLida);
                } catch (SerialPortException e) {
                    e.printStackTrace();
                    logger.log(Priority.ERROR, "Leitura do peso da caixa pela balança!", e);
                    SysLogger.addSysDelizLog("Fechamento Caixa", TipoAcao.EDITAR, String.valueOf(fieldNumeroCaixaLida.value.get()), "Não foi possível fazer a leitura da balança, peso da caixa fixido com o peso do produto: " + pesoCaixaLida);

                    MessageBox.create(message -> {
                        message.message("Não foi possível fazer a leitura do peso da balança. Balança retornando o peso zerado para a caixa.");
                        message.type(MessageBox.TypeMessageBox.ERROR);
                        message.showFullScreen();
                    });

                    fieldNumeroCaixa.clear();
                    fieldNumeroCaixa.requestFocus();

                    return;

                }

                if (pesoCaixaLida.compareTo(BigDecimal.ZERO) == 0) {
                    MessageBox.create(message -> {
                        message.message("Não foi possível fazer a leitura do peso da balança. Balança retornando o peso zerado para a caixa.");
                        message.type(MessageBox.TypeMessageBox.ERROR);
                        message.showFullScreen();
                    });

                    fieldNumeroCaixa.clear();
                    fieldNumeroCaixa.requestFocus();
                }

                // carregando dados da caixa lida
                SdCaixaRemessa caixaLida = carregarCaixa(fieldNumeroCaixaLida.value.get());
                if (caixaLida == null) {
                    MessageBox.create(message -> {
                        message.message("Número de caixa não encontrado em uma remessa.");
                        message.type(MessageBox.TypeMessageBox.WARNING);
                        message.showFullScreen();
                    });
                    fieldNumeroCaixa.clear();
                    fieldNumeroCaixa.requestFocus();
                    return;
                }
                logger.log(Priority.INFO, "Validação de caixa existente realizada com sucesso!");

                // verificação se a remessa tem itens faturáveis
                if (remessaCaixa.getPedidos().stream()
                        .filter(pedido -> pedido.getItens().stream().filter(item -> item.getQtde() > 0 && !item.getTipo().equals("MKT"))
                                .mapToInt(SdItensPedidoRemessa::getQtde).sum() > 0)
                        .count() == 0) {
                    MessageBox.create(message -> {
                        message.message("Remessa sem itens faturáveis!");
                        message.type(MessageBox.TypeMessageBox.WARNING);
                        message.showFullScreen();
                    });
                    fieldNumeroCaixa.clear();
                    fieldNumeroCaixa.requestFocus();
                    return;
                }

                // verificando cliente da remessa
                if (!verificaClienteRemessa(caixaLida.getRemessa())) {
                    MessageBox.create(message -> {
                        message.message("A remesssa " + caixaLida.getRemessa().getRemessa() + " teve alteração de cliente, informe o atendimento.");
                        message.type(MessageBox.TypeMessageBox.WARNING);
                        message.showFullScreen();
                    });
                    fieldNumeroCaixa.clear();
                    fieldNumeroCaixa.requestFocus();
                    return;
                }
                logger.log(Priority.INFO, "Validação de dados do cliente realizada com sucesso!");

                // verificando finalização da remessa
                if (!caixaLida.getRemessa().getStatus().getCodigo().equals("T")
                        || remessaCaixa.getPedidos().stream()
                        .mapToInt(pedido -> pedido.getItens().stream()
                                .mapToInt(item -> item.getGrade().stream()
                                        .mapToInt(SdGradeItemPedRem::getQtdep)
                                        .sum())
                                .sum())
                        .sum() > 0) {
                    MessageBox.create(message -> {
                        message.message("A remessa desta caixa ainda não foi toda coletada, aguarde finalizar toda a coleta.");
                        message.type(MessageBox.TypeMessageBox.WARNING);
                        message.showFullScreen();
                    });
                    fieldNumeroCaixa.clear();
                    fieldNumeroCaixa.requestFocus();
                    return;
                }
                logger.log(Priority.INFO, "Validação de finalização de coleta da remessa realizada com sucesso!");

                caixaLida.setPeso(pesoCaixaLida);

                try {
                    // carregando Nota Fiscal
                    if (remessaCaixa.getPedidos().stream().anyMatch(pedido -> pedido.getNota() == null)) {
                        logger.log(Priority.INFO, "Validada necessidade de criar as notas para as caixas da remessa!");
                        criarNfsRemessa(remessaCaixa);
                    }
                    // definindo o volume da caixa, vai ser o count das caixas já lidas da NF +1
                    if (caixaLida.getVolume() == 0) {
                        caixaLida.setVolume(((Long) (caixasRemessa.stream()
                                .filter(caixa -> caixa.getNumero() != caixaLida.getNumero()
                                        && caixa.getNota().equals(caixaLida.getNota())
                                        && caixa.getVolume() != 0)
                                .count() + 1))
                                .intValue());
                    }
                    new FluentDao().merge(caixaLida);

                    // gerando a etiqueta e enviando para impressão
                    JPAUtils.getEntityManager().refresh(caixaLida.getRemessa().getPedidos().get(0).getId().getNumero());
                    if (caixaLida.getRemessa().getPedidos().get(0).getId().getNumero().getTabTrans() == null)
                        throw new FormValidationException("O pedido " + caixaLida.getRemessa().getPedidos().get(0).getId().getNumero().getNumero() + " não está com uma transportadora.");

                    String zplCodeEtiqueta = "";
                    _IMPRIME_BRASPRESS.refresh();
                    if (_IMPRIME_BRASPRESS.getValor().equals("S") && caixaLida.getRemessa().getPedidos().get(0).getId().getNumero().getTabTrans().getNome().toUpperCase().contains("BRASPRESS")) {
                        zplCodeEtiqueta = criarEtiquetaCaixaBrasspress(caixaLida, caixasRemessa);
                    } else {
                        zplCodeEtiqueta = criarEtiquetaCaixaDeliz(caixaLida, caixasRemessa);
                    }
                    System.out.println(zplCodeEtiqueta);
                    try {
                        new ReportUtils().config().view().printZplCode(zplCodeEtiqueta);

                        if (remessaCaixa.getCodcli().isImprimeConteudoCaixa()) {
                            new ReportUtils().config()
                                    .addReport(ReportUtils.ReportFile.ETIQUETA_CONTEUDO_CAIXA_EXPEDICAO, new ReportUtils.ParameterReport("numero", caixaLida.getNumero()))
                                    .view().print();
                        }
                    } catch (PrintException e) {
                        MessageBox.create(message -> {
                            message.message(e.getMessage());
                            message.type(MessageBox.TypeMessageBox.ERROR);
                            message.showFullScreen();
                        });
                        fieldNumeroCaixa.clear();
                        fieldNumeroCaixa.requestFocus();
                        return;
                    } catch (JRException e) {
                        e.printStackTrace();
                        ExceptionBox.build(message -> {
                            message.exception(e);
                            message.showAndWait();
                        });
                    }

                    fieldNumeroNotaFiscal.value.set(caixaLida.getNota());
                    fieldNumeroPalete.value.set(caixaLida.getNota().substring(caixaLida.getNota().length() - 1));
                    fieldPesoCaixa.value.set(StringUtils.toDecimalFormat(caixaLida.getPeso()));
                    fieldTransportadoraCaixa.value.set(remessaCaixa.getPedidos().stream().map(pedido -> pedido.getId().getNumero().getTabTrans().getNome().toUpperCase()).max(Comparator.naturalOrder()).get());
                    if (mensagemBraspress.length() > 0) {
                        fieldMensagemBraspress.value.set(mensagemBraspress);
                        boxMensagemBraspress.add(fieldMensagemBraspress.build());
                    } else {
                        boxMensagemBraspress.remove(fieldMensagemBraspress.build());
                    }

                    if (caixasRemessa.stream().noneMatch(caixa -> caixa.getVolume() == 0)) {
                        SdStatusRemessa status = new FluentDao().selectFrom(SdStatusRemessa.class).where(eb -> eb.equal("codigo", "E")).singleResult();
                        remessaCaixa.setStatus(status);
                        new FluentDao().merge(remessaCaixa);
                        gerarNfRemessa(remessaCaixa);
                    }

                    // add log de fechamento
                    SysLogger.addSysDelizLog("Fechamento Caixa", TipoAcao.EDITAR, String.valueOf(caixaLida.getNumero()), "Caixa fechada");
                } catch (SQLException | IOException e) {
                    MessageBox.create(message -> {
                        message.message("Aconteceu um erro: " + e.getMessage());
                        message.type(MessageBox.TypeMessageBox.ERROR);
                        message.showFullScreen();
                    });
                    logger.log(Priority.ERROR, "Exception na persistencia e impressão dos dados.", e);
                    fieldNumeroCaixa.clear();
                    fieldNumeroCaixa.requestFocus();
                    return;
                } catch (FormValidationException e) {
                    MessageBox.create(message -> {
                        message.message(e.getMessage());
                        message.type(MessageBox.TypeMessageBox.WARNING);
                        message.showFullScreen();
                    });
                    fieldNumeroCaixa.clear();
                    fieldNumeroCaixa.requestFocus();
                    return;
                }
            } else {
                MessageBox.create(message -> {
                    message.message("Código de barras não é válido para o número da caixa.");
                    message.type(MessageBox.TypeMessageBox.ALERT);
                    message.showFullScreen();
                });
            }

            fieldNumeroCaixa.clear();
            fieldNumeroCaixa.requestFocus();
        }
    }

    private void limparDados() {
        fieldNumeroCaixaLida.clear();
        fieldPesoCaixa.clear();
        fieldNumeroNotaFiscal.clear();
        fieldNumeroPalete.clear();
        mensagemBraspress = "";
        boxMensagemBraspress.remove(fieldMensagemBraspress.build());
    }

    private void carregarRemessa() {
        /**
         * task: getCaixasRemessa(fieldRemessa.value.get()); * return: tblCaixasRemessa.items.set(FXCollections.observableList(consultaCaixasRemessa));
         */
        new RunAsyncWithOverlay(this).exec(task -> {
            getCaixasRemessa(fieldRemessa.value.get());
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                tblCaixasRemessa.items.set(FXCollections.observableList(consultaCaixasRemessa));
            }
        });
    }

    private void excluirBarraCaixa(SdItensCaixaRemessa item) {
        SdCaixaRemessa caixaSelecionada = tblCaixasRemessa.selectedItem();
        if (!((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Deseja realmente excluir a barra " + item.getId().getBarra() + " da caixa " + caixaSelecionada.getNumero());
            message.showAndWait();
        }).value.get())) {
            return;
        }

        new Fragment().show(fragment -> {
            fragment.title("Excluir Item da Caixa " + caixaSelecionada.getNumero());
            fragment.size(300.0, 200.0);

            final FormFieldText fieldBarraSelecionada = FormFieldText.create(field -> {
                field.title("Barra em Exclusão");
                field.editable(false);
                field.addStyle("lg");
                field.value.set(item.getId().getBarra());
                field.alignment(Pos.CENTER);
            });
            final FormFieldText fieldLeituraBarra = FormFieldText.create(field -> {
                field.withoutTitle();
                field.label(ImageUtils.getIcon(ImageUtils.Icon.LEITURA_BARRA, ImageUtils.IconSize._32));
                field.alignment(Pos.CENTER);
                field.addStyle("lg");
                field.toUpper();
            });
            fragment.box.getChildren().add(fieldBarraSelecionada.build());
            fragment.box.getChildren().add(fieldLeituraBarra.build());

            fragment.buttonsBox.getChildren().add(FormButton.create(btn -> {
                btn.title("Excluir");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.TRASH, ImageUtils.IconSize._24));
                btn.addStyle("warning");
                btn.defaultButton(true);
                btn.setAction(evt -> {
                    if (!item.getId().getBarra().equals(fieldLeituraBarra.value.get())) {
                        MessageBox.create(message -> {
                            message.message("A barra lida não é igual a barra selecionada para exclusão, verifique a barra digitada.");
                            message.type(MessageBox.TypeMessageBox.ALERT);
                            message.showAndWait();
                        });

                        fieldLeituraBarra.clear();
                        fieldLeituraBarra.requestFocus();
                        return;
                    }

                    /**
                     * task:  * return:
                     */
                    AtomicReference<SQLException> exc = new AtomicReference<>();
                    new RunAsyncWithOverlay(this).exec(task -> {
                        try {
                            excluiBarra(item, caixaSelecionada);
                        } catch (SQLException e) {
                            e.printStackTrace();
                            exc.set(e);
                            return ReturnAsync.EXCEPTION.value;
                        }
                        return ReturnAsync.OK.value;
                    }).addTaskEndNotification(taskReturn -> {
                        if (taskReturn.equals(ReturnAsync.OK.value)) {
                            // add log
                            SysLogger.addSysDelizLog("Fechamento Caixa", TipoAcao.EXCLUIR, String.valueOf(item.getId().getCaixa().getNumero()), "Excluido barra " + item.getId().getBarra() + " da caixa.");

                            MessageBox.create(message -> {
                                message.message("Barra " + item.getId().getBarra() + " removida da caixa " + caixaSelecionada.getNumero() + "!");
                                message.type(MessageBox.TypeMessageBox.CONFIRM);
                                message.position(Pos.TOP_RIGHT);
                                message.notification();
                            });
                            tblItensCaixa.refresh();
                            fragment.close();
                        } else if (taskReturn.equals(ReturnAsync.EXCEPTION.value)) {
                            ExceptionBox.build(message -> {
                                message.exception(exc.get());
                                message.showAndWait();
                            });
                        }
                    });
                });
            }));

            fieldLeituraBarra.requestFocus();
        });
    }

    private void excluirCaixaRemessa(SdCaixaRemessa item) {
        if (item.getRemessa().getStatus().getCodigo().equals("S")) {
            if (!((Boolean) QuestionBox.build(Boolean.class, message -> {
                message.message("Esta caixa está em uma remessa em coleta, pelo coletor " + item.getRemessa().getColetor().getNome() + "\n" +
                        "Deseja realmente excluir a caixa?\n" +
                        "Verifique com o coletor se ele parou de ler as peças.");
                message.showAndWait();
            }).value.get())) {
                return;
            }
        } else {
            if (!((Boolean) QuestionBox.build(Boolean.class, message -> {
                message.message("Deseja realmente excluir a caixa " + item.getNumero());
                message.showAndWait();
            }).value.get())) {
                return;
            }
        }

        /**
         * task:  * return:
         */
        AtomicReference<SQLException> exc = new AtomicReference<>();
        new RunAsyncWithOverlay(this).exec(task -> {
            try {
                excluiCaixa(item);
            } catch (SQLException e) {
                e.printStackTrace();
                exc.set(e);
                return ReturnAsync.EXCEPTION.value;
            }
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                consultaCaixasRemessa.remove(item);
                tblCaixasRemessa.refresh();
                tblItensCaixa.items.set(FXCollections.observableArrayList());

                // add log
                SysLogger.addSysDelizLog("Fechamento Caixa", TipoAcao.EXCLUIR, String.valueOf(item.getRemessa().getRemessa()), "Excluido caixa " + item.getNumero() + " da remessa.");

                MessageBox.create(message -> {
                    message.message("Caixa excluída com sucesso");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            } else if (taskReturn.equals(ReturnAsync.EXCEPTION.value)) {
                ExceptionBox.build(message -> {
                    message.exception(exc.get());
                    message.showAndWait();
                });
            }
        });
    }
}
