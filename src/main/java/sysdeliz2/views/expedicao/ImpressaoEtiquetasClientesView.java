package sysdeliz2.views.expedicao;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.print.PrinterJob;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import sysdeliz2.controllers.views.expedicao.ImpressaoEtiquetasClientesController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.sysdeliz.SdCliPaludo;
import sysdeliz2.models.sysdeliz.SdCliRFStore;
import sysdeliz2.models.ti.Cor;
import sysdeliz2.models.ti.PedIten;
import sysdeliz2.models.ti.Pedido;
import sysdeliz2.models.ti.Produto;
import sysdeliz2.models.view.VSdDadosProdutoBarra;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.dialog.FileChoice;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;

import javax.print.*;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class ImpressaoEtiquetasClientesView extends ImpressaoEtiquetasClientesController {

    private final AtomicReference<String> temperatura = new AtomicReference<>("");


    private final ListProperty<PedIten> itensPedidoBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final BooleanProperty isPaludo = new SimpleBooleanProperty(true);

    private final FormTableView<PedIten> tblEtiquetas = FormTableView.create(PedIten.class, table -> {
        table.items.bind(itensPedidoBean);
        table.expanded();
        table.editable.set(true);
        table.title("Etiquetas");
        table.selectColumn();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<PedIten, PedIten>, ObservableValue<PedIten>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodigo().getCodigo()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(250);
                    cln.value((Callback<TableColumn.CellDataFeatures<PedIten, PedIten>, ObservableValue<PedIten>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodigo().getDescricao()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Cor");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<PedIten, PedIten>, ObservableValue<PedIten>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCorEtq()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Tam");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<PedIten, PedIten>, ObservableValue<PedIten>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getTam()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Marca");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<PedIten, PedIten>, ObservableValue<PedIten>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodigo().getMarca().getDescricao()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Qtde");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<PedIten, PedIten>, ObservableValue<PedIten>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<PedIten, PedIten>() {
                            @Override
                            protected void updateItem(PedIten item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    if (isPaludo.get()) {
                                        setGraphic(FormFieldText.create(field -> {
                                            field.withoutTitle();
                                            field.decimalField(0);
                                            field.value.setValue(String.valueOf(item.getQtdeEtq()));
                                            field.mouseClicked(evt -> {
                                                field.textField.selectAll();
                                            });
                                            field.focusedListener((observable, oldValue, newValue) -> {
                                                if (!newValue)
                                                    item.setQtdeEtq(Integer.parseInt(field.value.getValue()));
                                            });
                                        }).build());
                                    } else {
                                        setText(String.valueOf(item.getQtde()));
                                    }
                                }
                            }
                        };
                    });
                }).build()
        );
    });

    private final FormFieldMultipleFind<Produto> filterProduto = FormFieldMultipleFind.create(Produto.class, field -> {
        field.title("Produto");
        field.width(200);
    });

    private final FormFieldMultipleFind<Cor> filterCor = FormFieldMultipleFind.create(Produto.class, field -> {
        field.title("Cor");
        field.width(200);
    });

    private final FormFieldSingleFind<Pedido> filterPedido = FormFieldSingleFind.create(Pedido.class, field -> {
        field.title("Pedido");
        field.code.setPrefWidth(200);
        field.description.setPrefWidth(0);
        field.width(250);
    });

    private final FormFieldComboBox<String> filterCliente = FormFieldComboBox.create(String.class, field -> {
        field.title("Cliente");
        field.items(FXCollections.observableArrayList(new ArrayList<>(mapClientes.values())));
        field.select("Paludo");
        field.getSelectionModel((observable, oldValue, newValue) -> {
            if (newValue != null) {
                isPaludo.set(newValue.equals("Paludo"));
            }
        });
    });

    private final CheckBox chboxGerarArquivo = new CheckBox("Gerar Arquivo");

    private Button btnImprimir = FormButton.create(btn -> {
        btn.setText("Imprimir");
        btn.addStyle("primary");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.IMPRIMIR_TAG, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            imprimirEtiquetas();
        });
    });

    public ImpressaoEtiquetasClientesView() {
        super("Impressão de Etiquetas dos Clientes", ImageUtils.getImage(ImageUtils.Icon.IMPRIMIR_TAG));
        init();
    }

    private void init() {
        box.getChildren().add(FormBox.create(principal -> {
            principal.vertical();
            principal.expanded();
            principal.add(FormBox.create(boxHeader -> {
                boxHeader.horizontal();
                boxHeader.add(FormBox.create(boxTitledFilter -> {
                    boxTitledFilter.vertical();
                    boxTitledFilter.add(FormTitledPane.create(filter -> {
                        filter.filter();
                        filter.add(FormBox.create(boxFilter -> {
                            boxFilter.vertical();
                            boxFilter.add(FormBox.create(linha1 -> {
                                linha1.horizontal();
                                linha1.add(filterCliente.build(), filterPedido.build());
                            }));
                            boxFilter.add(FormBox.create(linha2 -> {
                                linha2.horizontal();
                                linha2.add(filterProduto.build(), filterCor.build());
                            }));
                        }));
                        filter.find.setOnAction(evt -> {
                            if (filterPedido.value.getValue() == null) {
                                MessageBox.create(message -> {
                                    message.message("Campo pedido não pode estar vazio.");
                                    message.type(MessageBox.TypeMessageBox.WARNING);
                                    message.showAndWait();
                                });
                                return;
                            }
                            new RunAsyncWithOverlay(this).exec(task -> {
                                buscarEtiquetas(
                                        filterCliente.value.getValue(),
                                        filterPedido.value.getValue(),
                                        filterProduto.objectValues.stream().map(it -> it.getCodigo()).toArray(),
                                        filterCor.objectValues.stream().map(it -> it.getCor()).toArray()
                                );
                                return ReturnAsync.OK.value;
                            }).addTaskEndNotification(taskReturn -> {
                                if (taskReturn.equals(ReturnAsync.OK.value)) {
                                    itensPedidoBean.set(FXCollections.observableArrayList(itensPedido));
                                    tblEtiquetas.refresh();
                                }
                            });
                        });
                        filter.clean.setOnAction(evt -> {
                            filterPedido.clear();
                            filterCor.clear();
                            filterProduto.clear();
                        });
                    }));
                }));
            }));
            principal.add(FormBox.create(boxCenter -> {
                boxCenter.vertical();
                boxCenter.expanded();
                boxCenter.add(tblEtiquetas.build());
            }));
            principal.add(FormBox.create(boxFooter -> {
                boxFooter.horizontal();
                boxFooter.add(FormBox.create(boxBtn -> {
                    boxBtn.horizontal();
                    boxBtn.alignment(Pos.CENTER);
                    boxBtn.add(btnImprimir, chboxGerarArquivo);
                }));
            }));
        }));
    }


    private void imprimirEtiquetas() {

        String zplCodeTag = "";
        temperatura.set("");

        new Fragment().show(fg -> {
            fg.size(400.0, 150.0);
            fg.title("Defina a temperatura de impressão: ");
            final FormFieldText temperaturaField = FormFieldText.create(field -> {
                field.title("Temperatura");
                field.value.setValue("5");
                field.decimalField(0);
                field.requestFocus();
            });
            final FormButton btnConfirm = FormButton.create(btn -> {
                btn.addStyle("success");
                btn.title("Confirmar");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                btn.setOnAction(evt -> {
                    if (temperaturaField.value.getValueSafe().equals("")) return;
                    temperatura.set(temperaturaField.value.getValueSafe());
                    fg.close();
                });
            });
            fg.box.getChildren().add(FormBox.create(box -> {
                box.vertical();
                box.add(temperaturaField.build());
            }));
            fg.boxFullButtons.getChildren().add(btnConfirm);
        });
        if (temperatura.get().equals("")) return;

        List<PedIten> selectedItens = tblEtiquetas.items.stream().filter(BasicModel::isSelected).collect(Collectors.toList());

        if (filterCliente.value.getValue().equals("Paludo")) {
            zplCodeTag = criaZPLCodePaludo(selectedItens);
        } else {
            zplCodeTag = criaZPLCodeRfStore(selectedItens);
        }

        if (chboxGerarArquivo.isSelected()) {
            String pathFile = FileChoice.show(box -> {
                box.addExtension(new FileChooser.ExtensionFilter("Arquivo de Texto", "*.txt"));
            }).stringSavePath();

            FileWriter myWriter = null;
            try {
                myWriter = new FileWriter(pathFile);
                myWriter.write(zplCodeTag);
                myWriter.close();
                MessageBox.create(message -> {
                    message.message("Arquivo TXT de etiqueta criado com sucesso.");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            } catch (IOException e) {
                e.printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(e);
                    message.showAndWait();
                });
            }

            selectedItens.forEach(it -> it.setSelected(false));
            return;
        }

        PrinterJob jobPrinterSelect = PrinterJob.createPrinterJob();
        if (jobPrinterSelect == null) {
            MessageBox.create(message -> {
                message.message("Impossível criar o job de impressão com as impressoras instaladas neste usuário, tente reiniciar o computador para atualização da lista de impressoras.");
                message.type(MessageBox.TypeMessageBox.ERROR);
                message.showAndWait();
            });
            return;
        }

        if (!jobPrinterSelect.showPrintDialog(btnImprimir.getScene().getWindow())) {
            return;
        }

        String printerName = jobPrinterSelect.getPrinter().getName();

        PrintService ps = null;
        try {

            byte[] by = zplCodeTag.getBytes();
            DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
            PrintService[] pss = PrintServiceLookup.lookupPrintServices(null, null);

            if (pss.length == 0) throw new RuntimeException("No printer services available.");

            for (PrintService psRead : pss) {
                if (psRead.getName().contains(printerName)) {
                    ps = psRead;
                    break;
                }
            }

            if (ps == null) throw new RuntimeException("No printer mapped in TS.");

            DocPrintJob job = ps.createPrintJob();
            Doc doc = new SimpleDoc(by, flavor, null);

            job.print(doc, null);

            selectedItens.forEach(it -> it.setSelected(false));

            MessageBox.create(message -> {
                message.message("Etiquetas impressas com sucesso!");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
        } catch (PrintException e) {
            GUIUtils.showMessage("Cannot print label on this printer : " + ps.getName(), Alert.AlertType.ERROR);
        }
    }

    private String criaZPLCodeRfStore(List<PedIten> selectedRegisters) {
        StringBuilder zpl = new StringBuilder();
        int contador = 0;
        for (PedIten item : selectedRegisters) {
            List<SdCliRFStore> rfStoreList = (List<SdCliRFStore>) new FluentDao()
                    .selectFrom(SdCliRFStore.class)
                    .where(it -> it
                            .equal("referencia", item.getId().getCodigo().getCodigo())
                            .equal("tam", item.getId().getTam()))
                    .resultList();
            SdCliRFStore rfStore = rfStoreList.stream().filter(it -> it.getCor() != null && it.getCor().equals(item.getId().getCor().getCor())).findFirst().orElse(null);
            if (rfStore == null)
                rfStore = rfStoreList.stream().filter(it -> it.getCor() == null).findFirst().orElse(null);

            if (rfStore != null) {
                for (int i = 0; i < item.getQtde(); i++) {
                    zpl.append(criaParteZplRfStore(rfStore, contador % 3));
                    contador++;
                }
            }
        }
        if (contador % 3 != 0) zpl.append("^PQ1,0,1,Y^XZ");
        return zpl.toString();
    }

    private String criaZPLCodePaludo(List<PedIten> selectedRegisters) {
        StringBuilder zpl = new StringBuilder();
        int contador = 0;
        for (PedIten item : selectedRegisters) {
            SdCliPaludo paludo = new FluentDao()
                    .selectFrom(SdCliPaludo.class)
                    .where(it -> it
                            .equal("id.codigo", item.getId().getCodigo().getCodigo())
                            .equal("id.tamitem", item.getId().getTam())
                            .equal("id.coritem", item.getCorEtq())
                    ).singleResult();

            if (paludo != null) {
                for (int i = 0; i < item.getQtdeEtq(); i++) {
                    zpl.append(criaParteZplPaludo(paludo, contador % 3));
                    contador++;
                }
            }
        }
        if (contador % 3 != 0) zpl.append("^PQ1,0,1,Y^XZ");
        return zpl.toString();
    }

    private String criaParteZplRfStore(SdCliRFStore rfStore, int i) {

        switch (i) {
            case 0:
                return "^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^JUS^PR5,5~SD" + temperatura.get() + "^LRN^CI0^XZ\n" +
                        "^XA\n" +
                        "^MMT\n" +
                        "^PW798\n" +
                        "^LL0448\n" +
                        "^LS0\n" +
                        "^BY2,2,67^FT679,217^BEB,,Y,N\n" +
                        "^FD" + rfStore.getBarra() + "^FS\n" +
                        "^FT 556, 431 ^A0B,25,24^FH\\^FD" + rfStore.getDescricaoproduto().replace(rfStore.getReferencia(), "").replace(rfStore.getDescricao(), "").replace(rfStore.getTam(), "") + "^FS\n" +
                        "^FT 584, 264 ^A0B,28,28^FH\\^FD" + rfStore.getTam() + "^FS\n" +
                        "^FT 584, 375 ^A0B,28,28^FH\\^FD" + rfStore.getCodproduto() + "^FS\n" +
                        "^FT 738, 277 ^A0B,34,33^FH\\^FDR$ " + rfStore.getValorvenda() + "^FS\n" +
                        "^FT 635, 432 ^A0B,23,24^FH\\^FD" + rfStore.getCodinterno() + "^FS\n" +
                        "^FT 608, 432 ^A0B,23,24^FH\\^FD" + rfStore.getDescricao() + "^FS\n" +
                        "^FT 584, 432 ^A0B,28,28^FH\\^FDCod:^FS\n" +
                        "^FT 667, 319 ^A0B,25,24^FH\\^FD" + (rfStore.getCor() == null ? "DIVERSAS" : rfStore.getCor()) + "^FS\n" +
                        "^FT 701, 432 ^A0B,23,24^FH\\^FD" + rfStore.getColecao().replace("ã", "a") + "^FS\n" +
                        "^FT 666, 432 ^A0B,25,24^FH\\^FD" + rfStore.getReferencia() + "^FS\n" +
                        "^FT 740, 433 ^A0B,39,38^FH\\^FDRF Store^FS" +
                        "^LRY^FO 562, 212 ^GB0,221,24^FS^LRN\n" +
                        "^LRY^FO 706, 141 ^GB0,137,41^FS^LRN\n";
            case 1:
                return "^BY2,2,67^FT413,217^BEB,,Y,N\n" +
                        "^FD" + rfStore.getBarra() + "^FS\n" +
                        "^FT 290, 431 ^A0B,25,24^FH\\^FD" + rfStore.getDescricaoproduto().replace(rfStore.getReferencia(), "").replace(rfStore.getDescricao(), "").replace(rfStore.getTam(), "") + "^FS\n" +
                        "^FT 318, 264 ^A0B,28,28^FH\\^FD" + rfStore.getTam() + "^FS\n" +
                        "^FT 318, 375 ^A0B,28,28^FH\\^FD" + rfStore.getCodproduto() + "^FS\n" +
                        "^FT 472, 277 ^A0B,34,33^FH\\^FDR$ " + rfStore.getValorvenda() + "^FS\n" +
                        "^FT 369, 432 ^A0B,23,24^FH\\^FD" + rfStore.getCodinterno() + "^FS\n" +
                        "^FT 342, 432 ^A0B,23,24^FH\\^FD" + rfStore.getDescricao() + "^FS\n" +
                        "^FT 318, 432 ^A0B,28,28^FH\\^FDCod:^FS\n" +
                        "^FT 401, 319 ^A0B,25,24^FH\\^FD" + (rfStore.getCor() == null ? "DIVERSAS" : rfStore.getCor()) + "^FS\n" +
                        "^FT 435, 432 ^A0B,23,24^FH\\^FD" + rfStore.getColecao().replace("ã", "a") + "^FS\n" +
                        "^FT 400, 432 ^A0B,25,24^FH\\^FD" + rfStore.getReferencia() + "^FS\n" +
                        "^FT 474, 433 ^A0B,39,38^FH\\^FDRF Store^FS" +
                        "^LRY^FO 296, 212 ^GB0,221,24^FS^LRN\n" +
                        "^LRY^FO 440, 141 ^GB0,137,41^FS^LRN\n";
            case 2:
                return "^BY2,2,67^FT147,217^BEB,,Y,N\n" +
                        "^FD" + rfStore.getBarra() + "^FS\n" +
                        "^FT 24, 431 ^A0B,25,24^FH\\^FD" + rfStore.getDescricaoproduto().replace(rfStore.getReferencia(), "").replace(rfStore.getDescricao(), "").replace(rfStore.getTam(), "") + "^FS\n" +
                        "^FT 52, 264 ^A0B,28,28^FH\\^FD" + rfStore.getTam() + "^FS\n" +
                        "^FT 52, 375 ^A0B,28,28^FH\\^FD" + rfStore.getCodproduto() + "^FS\n" +
                        "^FT 206, 277 ^A0B,34,33^FH\\^FDR$ " + rfStore.getValorvenda() + "^FS\n" +
                        "^FT 103, 432 ^A0B,23,24^FH\\^FD" + rfStore.getCodinterno() + "^FS\n" +
                        "^FT 76, 432 ^A0B,23,24^FH\\^FD" + rfStore.getDescricao() + "^FS\n" +
                        "^FT 52, 432 ^A0B,28,28^FH\\^FDCod:^FS\n" +
                        "^FT 135, 319 ^A0B,25,24^FH\\^FD" + (rfStore.getCor() == null ? "DIVERSAS" : rfStore.getCor()) + "^FS\n" +
                        "^FT 169, 432 ^A0B,23,24^FH\\^FD" + rfStore.getColecao().replace("ã", "a") + "^FS\n" +
                        "^FT 134, 432 ^A0B,25,24^FH\\^FD" + rfStore.getReferencia() + "^FS\n" +
                        "^FT 208, 433 ^A0B,39,38^FH\\^FDRF Store^FS\n" +
                        "^LRY^FO 30, 212 ^GB0,221,24^FS^LRN\n" +
                        "^LRY^FO 174, 141 ^GB0,137,41^FS^LRN\n" +
                        "^PQ1,0,1,Y^XZ";
            default:
                return "";
        }
    }

    private String criaParteZplPaludo(SdCliPaludo paludo, int i) {

        switch (i) {
            case 0:
                return "^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR5,5~SD" + temperatura.get() + "^JUS^LRN^CI0^XZ \n" +
                        "^XA\n" +
                        "^MMT\n" +
                        "^PW815\n" +
                        "^LL0440\n" +
                        "^LS0\n" +
                        "^FT 753,16^A0R,39,38^FH\\^FDREGGLA^FS\n" +
                        "^FT 734,305^A0R,25,24^FH\\^FD" + paludo.getId().getCoritem() + "^FS\n" +
                        "^FT 767,305^A0R,25,24^FH\\^FD" + paludo.getId().getTamitem() + "^FS\n" +
                        "^FT 556,116^A0R,20,19^FH\\^FD" + paludo.getBarracli() + "^FS\n" +
                        "^FT 720,16^A0R,23,24^FH\\^FD" + paludo.getId().getCodigo() + "^FS\n" +
                        "^FT 694,15^A0R,20,19^FH\\^FD" + paludo.getDescitem() + "^FS\n" +
                        "^FT 665,199^A0R,20,19^FH\\^FDT:^FS\n" +
                        "^FT 667,130^A0R,20,19^FH\\^FD" + paludo.getNp() + "^FS\n" +
                        "^FT 556,400^A0N,20,19^FH\\^FD" + paludo.getLote() + "^FS\n" +
                        "^FT 665,232^A0R,20,19^FH\\^FD" + paludo.getValtotal() + "^FS\n" +
                        "^FT 667,107^A0R,20,19^FH\\^FDX^FS\n" +
                        "^FT 667,50^A0R,20,19^FH\\^FD" + paludo.getValparcela() + "^FS\n" +
                        "^FT 667,16^A0R,20,19^FH\\^FDR$^FS\n" +
                        "^BY4,3,82^FT 576,16^BCR,,N,N\n" +
                        "^FD>;" + paludo.getBarracli() + "^FS\n";
            case 1:
                return "^FT 497,16^A0R,39,38^FH\\^FDREGGLA^FS\n" +
                        "^FT 478,305^A0R,25,24^FH\\^FD" + paludo.getId().getCoritem() + "^FS\n" +
                        "^FT 511,305^A0R,25,24^FH\\^FD" + paludo.getId().getTamitem() + "^FS\n" +
                        "^FT 300,116^A0R,20,19^FH\\^FD" + paludo.getBarracli() + "^FS\n" +
                        "^FT 464,16^A0R,23,24^FH\\^FD" + paludo.getId().getCodigo() + "^FS\n" +
                        "^FT 438,15^A0R,20,19^FH\\^FD" + paludo.getDescitem() + "^FS\n" +
                        "^FT 409,199^A0R,20,19^FH\\^FDT:^FS\n" +
                        "^FT 411,130^A0R,20,19^FH\\^FD" + paludo.getNp() + "^FS\n" +
                        "^FT 300,400^A0N,20,19^FH\\^FD" + paludo.getLote() + "^FS\n" +
                        "^FT 409,232^A0R,20,19^FH\\^FD" + paludo.getValtotal() + "^FS\n" +
                        "^FT 411,107^A0R,20,19^FH\\^FDX^FS\n" +
                        "^FT 411,50^A0R,20,19^FH\\^FD" + paludo.getValparcela() + "^FS\n" +
                        "^FT 411,16^A0R,20,19^FH\\^FDR$^FS\n" +
                        "^BY4,3,82^FT 320,16^BCR,,N,N\n" +
                        "^FD>;" + paludo.getBarracli() + "^FS\n";
            case 2:
                return "^FT 242,16^A0R,39,38^FH\\^FDREGGLA^FS\n" +
                        "^FT 223,305^A0R,25,24^FH\\^FD" + paludo.getId().getCoritem() + "^FS\n" +
                        "^FT 256,305^A0R,25,24^FH\\^FD" + paludo.getId().getTamitem() + "^FS\n" +
                        "^FT 45,116^A0R,20,19^FH\\^FD" + paludo.getBarracli() + "^FS\n" +
                        "^FT 209,16^A0R,23,24^FH\\^FD" + paludo.getId().getCodigo() + "^FS\n" +
                        "^FT 183,15^A0R,20,19^FH\\^FD" + paludo.getDescitem() + "^FS\n" +
                        "^FT 154,199^A0R,20,19^FH\\^FDT:^FS\n" +
                        "^FT 154,130^A0R,20,19^FH\\^FD" + paludo.getNp() + "^FS\n" +
                        "^FT 45,400^A0N,20,19^FH\\^FD" + paludo.getLote() + "^FS\n" +
                        "^FT 154,232^A0R,20,19^FH\\^FD" + paludo.getValtotal() + "^FS\n" +
                        "^FT 156,107^A0R,20,19^FH\\^FDX^FS\n" +
                        "^FT 156,50^A0R,20,19^FH\\^FD" + paludo.getValparcela() + "^FS\n" +
                        "^FT 156,16^A0R,20,19^FH\\^FDR$^FS\n" +
                        "^BY4,3,82^FT 65,16^BCR,,N,N\n" +
                        "^FD>;" + paludo.getBarracli() + "^FS\n" +
                        "^PQ1,0,1,Y^XZ";
            default:
                return "";
        }
    }
}
