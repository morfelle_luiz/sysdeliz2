package sysdeliz2.views.expedicao;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.input.KeyCode;
import javafx.util.Callback;
import sysdeliz2.controllers.views.expedicao.InspecaoCaixasEntradaController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.sysdeliz.EntradaPA.*;
import sysdeliz2.models.ti.PaIten;
import sysdeliz2.models.ti.PaMov;
import sysdeliz2.utils.FuncoesEntradaPA;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.SortUtils;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class InspecaoCaixasEntradaView extends InspecaoCaixasEntradaController {

    // <editor-fold defaultstate="collapsed" desc="Bean">
    private final ListProperty<SdCaixaPA> caixasBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<SdBarraCaixaPA> barrasBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<SdItemCaixaPA> itensCaixaBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final FormFieldText produtoLidoField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Produto");
        field.expanded();
        field.alignment(Pos.CENTER);
        field.editable.set(false);
    });
    // </editor-fold>
    private final FormFieldText corLidaField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Cor");
        field.expanded();
        field.alignment(Pos.CENTER);
        field.editable.set(false);
    });    // <editor-fold defaultstate="collapsed" desc="Fields">
    private final FormFieldText codLoteField = FormFieldText.create(field -> {
        field.title("Código Lote");
        field.decimalField(5);
        field.width(200);
        field.focusedListener((observable, oldValue, newValue) -> {
            if (!newValue) {
                buscarLote();
            }
        });
    });
    private final FormFieldText tamLidoField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Tam");
        field.expanded();
        field.alignment(Pos.CENTER);
        field.editable.set(false);
    });    private final FormFieldText leituraBarraField = FormFieldText.create(field -> {
        field.title("Barra");
        field.addStyle("lg");
        field.expanded();
        field.alignment(Pos.CENTER);
        field.label(ImageUtils.getIcon(ImageUtils.Icon.LEITURA_BARRA, ImageUtils.IconSize._24));
        field.keyReleased(evt -> {
            if (evt.getCode().equals(KeyCode.ENTER) && !field.value.getValueSafe().equals("")) {
                leituraBarra(field.value.getValue());
                field.clear();
            }
        });
    });
    private final FormFieldText codCaixaField = FormFieldText.create(field -> {
        field.title("Código");
        field.expanded();
        field.editable.set(false);
    });
    private final FormFieldText tipoCaixaField = FormFieldText.create(field -> {
        field.title("Tipo");
        field.expanded();
        field.editable.set(false);
    });
    private final FormFieldText prodCaixaField = FormFieldText.create(field -> {
        field.title("Produtos");
        field.expanded();
        field.editable.set(false);
    });
    private final FormFieldText ofCaixaField = FormFieldText.create(field -> {
        field.title("Of");
        field.expanded();
        field.editable.set(false);
    });
    private final FormFieldText pesoCaixaField = FormFieldText.create(field -> {
        field.title("Peso");
        field.expanded();
        field.editable.set(false);
    });
    private final FormFieldText qtdeCaixaField = FormFieldText.create(field -> {
        field.title("Qtde");
        field.expanded();
        field.editable.set(false);
    });
    private final FormTableView<SdBarraCaixaPA> tblBarrasLidas = FormTableView.create(SdBarraCaixaPA.class, table -> {
        table.title("Barras");
        table.items.bind(barrasBean);
        table.expanded();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Produto");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBarraCaixaPA, SdBarraCaixaPA>, ObservableValue<SdBarraCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue().getProduto()));
                }).build(), /*Produto*/
                FormTableColumn.create(cln -> {
                    cln.title("Barra");
                    cln.width(180);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBarraCaixaPA, SdBarraCaixaPA>, ObservableValue<SdBarraCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue().getBarra()));
                }).build(), /*Produto*/
                FormTableColumn.create(cln -> {
                    cln.title("Cor");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBarraCaixaPA, SdBarraCaixaPA>, ObservableValue<SdBarraCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCor()));
                }).build(), /*Produto*/
                FormTableColumn.create(cln -> {
                    cln.title("Tam");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBarraCaixaPA, SdBarraCaixaPA>, ObservableValue<SdBarraCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTam()));
                }).build() /*Produto*/
        );
    });
    private final FormTableView<SdItemCaixaPA> tblResumoCaixa = FormTableView.create(SdItemCaixaPA.class, table -> {
        table.withoutHeader();
        table.expanded();
        table.items.bind(itensCaixaBean);
        table.factoryRow(param -> {
            return new TableRow<SdItemCaixaPA>() {
                protected void updateItem(SdItemCaixaPA item, boolean empty) {
                    super.updateItem(item, empty);
                    clear();

                    if (item != null && !empty) {
                        if (barrasBean.getValue().stream().filter(it -> it.getCor().equals(item.getCor()) && it.getTam().equals(item.getTam())).count() != item.getQtde()) {
                            getStyleClass().add("table-row-warning");
                        } else {
                            getStyleClass().add("table-row-success");
                        }
                    }
                }

                private void clear() {
                    getStyleClass().removeAll("table-row-success", "table-row-warning");
                }
            };
        });
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Cor");
                    cln.width(120);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemCaixaPA, SdItemCaixaPA>, ObservableValue<SdItemCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<SdItemCaixaPA, SdItemCaixaPA>() {
                        @Override
                        protected void updateItem(SdItemCaixaPA item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            if (item != null && !empty) {
                                setText(item.getCor());
                                getStyleClass().add("lg");
                            }
                        }
                    });
                }).build(), /*Produto*/
                FormTableColumn.create(cln -> {
                    cln.title("Tam");
                    cln.width(100);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemCaixaPA, SdItemCaixaPA>, ObservableValue<SdItemCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<SdItemCaixaPA, SdItemCaixaPA>() {
                        @Override
                        protected void updateItem(SdItemCaixaPA item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            if (item != null && !empty) {
                                setText(item.getTam());
                                getStyleClass().add("lg");
                            }
                        }
                    });
                }).build(), /*Produto*/
                FormTableColumn.create(cln -> {
                    cln.title("Qtde Lida");
                    cln.width(100);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemCaixaPA, SdItemCaixaPA>, ObservableValue<SdItemCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<SdItemCaixaPA, SdItemCaixaPA>() {
                        @Override
                        protected void updateItem(SdItemCaixaPA item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            if (item != null && !empty) {
                                setText(String.valueOf(barrasBean.getValue().stream().filter(it -> it.getCor().equals(item.getCor()) && it.getTam().equals(item.getTam())).count()));
                                getStyleClass().add("lg");
                            }
                        }
                    });
                }).build(), /*Produto*/
                FormTableColumn.create(cln -> {
                    cln.title("Qtde Total");
                    cln.width(100);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemCaixaPA, SdItemCaixaPA>, ObservableValue<SdItemCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<SdItemCaixaPA, SdItemCaixaPA>() {
                        @Override
                        protected void updateItem(SdItemCaixaPA item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            if (item != null && !empty) {
                                setText(String.valueOf(item.getQtde()));
                                getStyleClass().add("lg");
                            }
                        }
                    });
                }).build() /*Produto*/
        );
    });
    private SdCaixaPA caixaEscolhida = new SdCaixaPA();

    // </editor-fold>

    public InspecaoCaixasEntradaView() {
        super("Inspeção de Caixas de Entrada", ImageUtils.getImage(ImageUtils.Icon.CAIXA));
        init();
    }

    // <editor-fold defaultstate="collapsed" desc="Table">
    private final FormTableView<SdCaixaPA> tblCaixasInspecao = FormTableView.create(SdCaixaPA.class, table -> {
        table.title("Caixas");
        table.expanded();
        table.items.bind(caixasBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(70);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCaixaPA, SdCaixaPA>, ObservableValue<SdCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId()));

                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Of");
                    cln.width(70);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCaixaPA, SdCaixaPA>, ObservableValue<SdCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue().getItemlote().getNumero()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Referência");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCaixaPA, SdCaixaPA>, ObservableValue<SdCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue().getProduto()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Qtde");
                    cln.width(70);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCaixaPA, SdCaixaPA>, ObservableValue<SdCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Peso");
                    cln.width(70);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdCaixaPA, SdCaixaPA>, ObservableValue<SdCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPeso()));
                }).build() /*Código*/
        );
        table.selectionModelItem((observable, oldValue, newValue) -> {
            caixaEscolhida = (SdCaixaPA) newValue;
            carregaCaixa();
        });
        table.tableview().setOnMouseReleased(event -> {
            leituraBarraField.requestFocus();
        });
    });

    private void init() {
        box.getChildren().add(FormBox.create(principal -> {
            principal.horizontal();
            principal.expanded();
            principal.add(FormBox.create(col1 -> {
                col1.vertical();
                col1.expanded();
                col1.maxWidthSize(400);
                col1.add(FormBox.create(boxBuscaLote -> {
                    boxBuscaLote.horizontal();
                    boxBuscaLote.alignment(Pos.BOTTOM_LEFT);
                    boxBuscaLote.add(codLoteField.build(), btnCarregarLote);
                }));
                col1.add(FormBox.create(boxTblCaixas -> {
                    boxTblCaixas.vertical();
                    boxTblCaixas.expanded();
                    boxTblCaixas.add(tblCaixasInspecao.build());
                }));
                col1.add(FormBox.create(boxFooter -> {
                    boxFooter.horizontal();
                    boxFooter.add(btnConcluirLeituraCaixa);
                }));
            }));
            principal.add(FormBox.create(col2 -> {
                col2.vertical();
                col2.expanded();
                col2.setPadding(new Insets(0, 0, 0, 10));
                col2.maxWidthSize(450);
                col2.add(FormBox.create(boxHeader -> {
                    boxHeader.vertical();
                    boxHeader.add(FormBox.create(l1 -> {
                        l1.horizontal();
                        l1.expanded();
                        l1.add(leituraBarraField.build());
                    }));
                    boxHeader.add(FormBox.create(l2 -> {
                        l2.horizontal();
                        l2.expanded();
                        l2.add(produtoLidoField.build());
                        l2.add(corLidaField.build());
                        l2.add(tamLidoField.build());
                    }));
                }));
                col2.add(FormBox.create(boxContent -> {
                    boxContent.horizontal();
                    boxContent.expanded();
                    boxContent.add(tblBarrasLidas.build());
                }));
            }));
            principal.add(FormBox.create(col3 -> {
                col3.vertical();
                col3.add(FormBox.create(boxDescCaixa -> {
                    boxDescCaixa.vertical();
                    boxDescCaixa.add(FormBox.create(l1 -> {
                        l1.horizontal();
                        l1.add(codCaixaField.build());
                        l1.add(tipoCaixaField.build());
                        l1.add(prodCaixaField.build());
                    }));
                    boxDescCaixa.add(FormBox.create(l2 -> {
                        l2.horizontal();
                        l2.add(qtdeCaixaField.build());
                        l2.add(pesoCaixaField.build());
                        l2.add(ofCaixaField.build());
                    }));
                }));
                col3.add(tblResumoCaixa.build());
            }));
        }));
    }

    private void carregaCaixa() {
        caixaEscolhida = new FluentDao().selectFrom(SdCaixaPA.class).where(it -> it.equal("id", caixaEscolhida.getId())).singleResult();
        if (caixaEscolhida != null) {
            barrasBean.set(FXCollections.observableArrayList(caixaEscolhida.getBarrasInspecao()));

            codCaixaField.value.setValue(String.valueOf(caixaEscolhida.getId()));
            tipoCaixaField.value.setValue(caixaEscolhida.isSegunda() ? "SEGUNDA" : "COMPLETA");
            prodCaixaField.value.setValue(caixaEscolhida.getProduto());
            qtdeCaixaField.value.setValue(String.valueOf(caixaEscolhida.getQtde()));
            pesoCaixaField.value.setValue(String.valueOf(caixaEscolhida.getPeso()));
            ofCaixaField.value.setValue(caixaEscolhida.getItemlote().getNumero());

            itensCaixaBean.set(FXCollections.observableArrayList(caixaEscolhida.getItensCaixa().stream().sorted((o1, o2) -> SortUtils.sortTamanhos(o1.getTam(), o2.getTam())).collect(Collectors.toList())));
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Filter">

    // </editor-fold>

    private void leituraBarra(String barra) {
        SdBarraCaixaPA barraCaixa = new FluentDao().selectFrom(SdBarraCaixaPA.class).where(it -> it.equal("barra", barra)).singleResult();

        if (barraCaixa == null) {
            MessageBox.create(message -> {
                message.message("Barra lida não encontrada.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            return;
        }

        if (!barraCaixa.getItemcaixa().getCaixa().getId().equals(caixaEscolhida.getId())) {
            MessageBox.create(message -> {
                message.message("Barra lida não pertence a caixa selecionada.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            return;
        }

        if (barrasBean.stream().anyMatch(it -> it.getBarra().equals(barra))) {
            MessageBox.create(message -> {
                message.message("Barra já foi lida.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            return;
        }
        SdLotePa loteBarra = barraCaixa.getItemcaixa().getCaixa().getItemlote().getLote();
        SdLotePa loteCaixa = caixaEscolhida.getItemlote().getLote();

        if (!loteCaixa.getId().equals(loteBarra.getId()) && loteBarra.getStatus().getCodigo() != 5) {
            MessageBox.create(message -> {
                message.message("Barra não pode ser adicionada pois já está em outro lote (" + loteBarra.getId() + "), com o status de " + loteBarra.getStatus().getStatus());
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }
        if (!loteCaixa.getId().equals(loteBarra.getId()) && loteBarra.getStatus().getCodigo() == 5) {
            MessageBox.create(message -> {
                message.message("Barra já está no estoque!");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            return;
        }
        produtoLidoField.value.setValue(barraCaixa.getProduto());
        corLidaField.value.setValue(barraCaixa.getCor());
        tamLidoField.value.setValue(barraCaixa.getTam());
        caixaEscolhida.getBarrasInspecao().add(barraCaixa);
        barrasBean.add(barraCaixa);
        tblResumoCaixa.refresh();
        if (barrasBean.size() == caixaEscolhida.getQtde()) {
            concluirLeituraCaixa();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Buttons">
    private final Button btnCarregarLote = FormButton.create(btn -> {
        btn.title("Buscar");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.SEARCH, ImageUtils.IconSize._24));
        btn.addStyle("primary");
        btn.setOnAction(evt -> {
            buscarLote();
        });
    });

    private void buscarLote() {
        if (!codLoteField.value.getValueSafe().equals("")) {
            SdLotePa lote = new FluentDao().selectFrom(SdLotePa.class).where(it -> it.equal("id", codLoteField.value.getValueSafe())).singleResult();
            if (lote != null) {
                caixasBean.set(FXCollections.observableArrayList(lote.getItensLote().stream().flatMap(it -> it.getListCaixas().stream().filter(eb -> eb.isInspecao() && !eb.isRecebida())).collect(Collectors.toList())));
            } else {
                codLoteField.clear();
            }
        }
    }    private final Button btnConcluirLeituraCaixa = FormButton.create(btn -> {
        btn.title("Concluir");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
        btn.addStyle("success");
        btn.setOnAction(evt -> {
            concluirLeituraCaixa();
        });
    });

    // </editor-fold>

    private void concluirLeituraCaixa() {
        if (barrasBean.size() < caixaEscolhida.getQtde()) {
            if (!((Boolean) QuestionBox.build(Boolean.class, message -> {
                message.message("Ainda não foram lidas " + (caixaEscolhida.getQtde() - barrasBean.size()) + " barras.\nDeseja remove-las dessa caixa?");
                message.showAndWait();
            }).value.get())) {
                return;
            } else {
                caixaEscolhida.getItensCaixa().stream().flatMap(it -> it.getListBarra().stream()).collect(Collectors.toList()).stream().filter(eb -> barrasBean.stream().noneMatch(el -> el.getBarra().equals(eb.getBarra()))).forEach(barra -> {
                    barra.setRemovida(true);
                    barra.setDataRemocao(LocalDate.now());
                    new FluentDao().merge(caixaEscolhida);
                });
            }
        }

        if (FuncoesEntradaPA.realizarPresistsBanco(caixaEscolhida)) {
            transferenciaPecasRemovidas();
            caixaEscolhida.setRecebida(true);
            new FluentDao().merge(caixaEscolhida);
            caixasBean.remove(caixaEscolhida);
            SdLotePa lote = new FluentDao().selectFrom(SdLotePa.class).where(it -> it.equal("id", caixaEscolhida.getItemlote().getLote())).singleResult();
            caixasBean.set(FXCollections.observableArrayList(lote.getItensLote().stream().flatMap(it -> it.getListCaixas().stream()).collect(Collectors.toList())));
            if (tblCaixasInspecao.items.size() > 0) {
                tblCaixasInspecao.selectItem(0);
                leituraBarraField.requestFocus();
            } else {
                MessageBox.create(message -> {
                    message.message("Leitura das caixas de inspeção de desse lote foi concluída.");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
                caixaEscolhida.getItemlote().getLote().setStatus(new FluentDao().selectFrom(SdStatusLotePA.class).where(it -> it.equal("codigo", 5)).singleResult());
                new FluentDao().getEntityManager().flush();
                caixaEscolhida = null;
                limparCampos();

            }
        }
    }

    private void transferenciaPecasRemovidas() {
        List<SdItemCaixaPA> itens = generateItensCaixaRemovidas();
        if (itens.size() == 0) {
            return;
        }
        for (SdItemCaixaPA item : itens) {
            PaIten estoque = new FluentDao().selectFrom(PaIten.class)
                    .where(it -> it
                            .equal("id.tam", item.getTam())
                            .equal("id.cor", item.getCor())
                            .equal("id.codigo", item.getProduto())
                            .equal("id.deposito", "0005")
                    ).singleResult();

            PaIten estoquePerdidas = new FluentDao().selectFrom(PaIten.class)
                    .where(it -> it
                            .equal("id.tam", item.getTam())
                            .equal("id.cor", item.getCor())
                            .equal("id.codigo", item.getProduto())
                            .equal("id.deposito", "0026")
                    ).singleResult();

            if (estoque != null) {
                estoque.setQuantidade(estoque.getQuantidade().subtract(new BigDecimal(item.getQtde())));
                if (estoquePerdidas == null) {
                    estoquePerdidas = new PaIten(item, "0026");
                }
                estoquePerdidas.setQuantidade(estoquePerdidas.getQuantidade().add(new BigDecimal(item.getQtde())));
                PaMov paMov = new PaMov(item, Globals.getUsuarioLogado().getUsuario(), "0026", "MN");
                try {
                    new FluentDao().merge(estoque);
                    new FluentDao().merge(estoquePerdidas);
                    new FluentDao().persist(paMov);
                    SysLogger.addSysDelizLog("Inspeção Caixas Entrada", TipoAcao.EDITAR, item.getProduto() + "-" + item.getCor() + "-" + item.getTam(), item.getQtde() + " Barras removidas do estoque, e direcionadas para o estoque 0026!");
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        }
    }

    private List<SdItemCaixaPA> generateItensCaixaRemovidas() {
        List<SdItemCaixaPA> itens = new ArrayList<>();
        try {
            caixaEscolhida = new FluentDao().selectFrom(SdCaixaPA.class).where(it -> it.equal("id", caixaEscolhida.getId())).singleResult();
            if (caixaEscolhida == null) {
                return new ArrayList<>();
            }
            Map<String, Map<String, List<SdBarraCaixaPA>>> grupos = caixaEscolhida.getItensCaixa()
                    .stream().flatMap(it -> it.getListBarra().stream().filter(SdBarraCaixaPA::isRemovida)).collect(Collectors.groupingBy(SdBarraCaixaPA::getCor, Collectors.groupingBy(SdBarraCaixaPA::getTam)));
            grupos.forEach((cor, map) -> map.forEach((tam, list) -> itens.add(new SdItemCaixaPA(cor, tam, list.size()))));
            return itens;
        } catch (Exception e) {
            e.printStackTrace();
            MessageBox.create(message -> {
                message.message("Erro: " + e.getMessage());
                message.type(MessageBox.TypeMessageBox.ERROR);
                message.position(Pos.TOP_RIGHT);
                message.notification();

            });
        }
        return itens;
    }

    private void limparCampos() {
        tblCaixasInspecao.clear();
        tblResumoCaixa.clear();
        tblBarrasLidas.clear();
        barrasBean.clear();
        caixasBean.clear();
        itensCaixaBean.clear();
        codLoteField.clear();
        produtoLidoField.clear();
        corLidaField.clear();
        tamLidoField.clear();
        codCaixaField.clear();
        tipoCaixaField.clear();
        prodCaixaField.clear();
        qtdeCaixaField.clear();
        pesoCaixaField.clear();
        ofCaixaField.clear();
    }










}
