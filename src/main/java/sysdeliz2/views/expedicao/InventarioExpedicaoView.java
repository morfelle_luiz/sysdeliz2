package sysdeliz2.views.expedicao;

import javafx.application.Platform;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.input.KeyCode;
import javafx.util.Callback;
import sysdeliz2.controllers.views.expedicao.InventarioExpedicaoController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.sysdeliz.Inventario.*;
import sysdeliz2.models.ti.PaIten;
import sysdeliz2.models.ti.PaMov;
import sysdeliz2.models.view.VSdDadosProdutoBarra;
import sysdeliz2.models.view.VSdProdutoInventario;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.SortUtils;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.exceptions.FormValidationException;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

public class InventarioExpedicaoView extends InventarioExpedicaoController {

    //    private static final Logger logger = Logger.getLogger(InventarioExpedicaoView.class);

    private final SdStatusInventario STATUS_EM_LEITURA = new FluentDao().selectFrom(SdStatusInventario.class).where(it -> it.equal("codigo", 3)).singleResult();

    private final SdStatusItemInventario STATUS_ITEM_NAO_INVENTARIADO = new FluentDao().selectFrom(SdStatusItemInventario.class).where(it -> it.equal("codigo", 1)).singleResult();
    private final SdStatusItemInventario STATUS_ITEM_EM_LEITURA = new FluentDao().selectFrom(SdStatusItemInventario.class).where(it -> it.equal("codigo", 3)).singleResult();
    private final SdStatusItemInventario STATUS_FINALIZADO = new FluentDao().selectFrom(SdStatusItemInventario.class).where(it -> it.equal("codigo", 5)).singleResult();

    private final FormFieldText codigoField = FormFieldText.create(field -> {
        field.title("Código");
        field.width(150);
        field.editable.set(false);
        field.addStyle("lg");
    });

    private final FormFieldText descricaoField = FormFieldText.create(field -> {
        field.title("Descrição");
        field.width(335);
        field.editable.set(false);
        field.addStyle("lg");
    });

    private final FormFieldText corField = FormFieldText.create(field -> {
        field.title("Cor");
        field.width(150);
        field.editable.set(false);
        field.addStyle("lg");
    });

    private final FormFieldText localField = FormFieldText.create(field -> {
        field.title("Local");
        field.width(150);
        field.editable.set(false);
        field.addStyle("lg");
    });

    private final FormFieldText leituraBarraField = FormFieldText.create(field -> {
        field.label(ImageUtils.getIcon(ImageUtils.Icon.LEITURA_BARRA, ImageUtils.IconSize._32));
        field.addStyle("lg");
        field.alignment(Pos.CENTER);
        field.withoutTitle();
        field.width(800);
        field.keyReleased(evt -> {
            if (evt.getCode().equals(KeyCode.ENTER)) {
                leituraBarra(field);
            }
        });
    });

    private final FormFieldText ultProdutoField = FormFieldText.create(field -> {
        field.title("Ult. Produto");
        field.addStyle("info");
        field.editable.set(false);
    });

    private final FormFieldText ultCorField = FormFieldText.create(field -> {
        field.title("Ult. Cor");
        field.addStyle("info");
        field.editable.set(false);
    });

    private final FormFieldText ultLocalField = FormFieldText.create(field -> {
        field.title("Ult. Local");
        field.addStyle("info");
        field.editable.set(false);
    });

    private final FormTableView<SdGradeItemInventario> gradeItemTbl = FormTableView.create(SdGradeItemInventario.class, table -> {
        table.items.bind(gradeInventarioBean);
        table.withoutHeader();
        table.height(550);
        table.factoryRow(param -> {
            return new TableRow<SdGradeItemInventario>() {
                @Override
                protected void updateItem(SdGradeItemInventario item, boolean empty) {
                    super.updateItem(item, empty);
                    clear();

                    if (item != null && !empty) {
                        getStyleClass().add(item.getQtdelida() == 0 ? "table-row-danger" : "table-row-warning");
                    }
                }

                private void clear() {
                    getStyleClass().removeAll("table-row-danger", "table-row-warning");
                }
            };
        });
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("TAM");
                    cln.width(400);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdGradeItemInventario, SdGradeItemInventario>, ObservableValue<SdGradeItemInventario>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<SdGradeItemInventario, SdGradeItemInventario>() {
                        @Override
                        protected void updateItem(SdGradeItemInventario item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            if (item != null && !empty) {
                                setText(item.getTam());
                                getStyleClass().add("lg");
                            }
                        }
                    });
                }).build(), /*TAM*/
                FormTableColumn.create(cln -> {
                    cln.title("QTDE");
                    cln.width(400);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdGradeItemInventario, SdGradeItemInventario>, ObservableValue<SdGradeItemInventario>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<SdGradeItemInventario, SdGradeItemInventario>() {
                        @Override
                        protected void updateItem(SdGradeItemInventario item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            if (item != null && !empty) {
                                setText(item.getQtdelida().toString());
                                getStyleClass().add("lg");
                            }
                        }
                    });
                }).build() /*TAM*/
        );
    });

    private final Button btnConfirmar = FormButton.create(btn -> {
        btn.addStyle("success").addStyle("lg");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._32));
        btn.title("Confirmar");
        btn.setOnAction(evt -> {
            confirmarLeituraItemInventario();
        });
    });

    private final Button btnCancelar = FormButton.create(btn -> {
        btn.addStyle("danger").addStyle("lg");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._32));
        btn.title("Cancelar");
        btn.setOnAction(evt -> {
            cancelarLeituraInventario();
        });
    });

    public InventarioExpedicaoView() {
        super("Inventário", ImageUtils.getImage(ImageUtils.Icon.DEPOSITO));

//        logger.info("init tela");
        JPAUtils.getEntityManager();

        usuario = getUsuario(Globals.getNomeUsuario());
//        logger.info("init colaborador");
        if (usuario == null) {
            MessageBox.create(message -> {
                message.message("Usuário não habilitado para inventário.");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showAndWait();
            });
            throw new FormValidationException("Coletor não existe!");
        } else {
            SysLogger.addSysDelizLog("Inventario", TipoAcao.ENTRAR, String.valueOf(usuario.getCodigo()), "Coletor " + usuario.getNome() + " iniciou tela de inventário.");
            init();
        }
    }

    private void init() {
        super.box.getChildren().clear();
        limparCampos();
        super.box.getChildren().add(FormBox.create(principal -> {
            principal.horizontal();
            principal.expanded();
            principal.add(FormBox.create(col1 -> {
                col1.vertical();
                col1.expanded();
                col1.add(FormBox.create(boxInfosProd -> {
                    boxInfosProd.horizontal();
                    boxInfosProd.width(800);
                    boxInfosProd.alignment(Pos.CENTER);
                    boxInfosProd.add(codigoField.build(), descricaoField.build(), corField.build(), localField.build());
                }));
                col1.add(FormBox.create(boxLeituraBarra -> {
                    boxLeituraBarra.horizontal();
                    boxLeituraBarra.alignment(Pos.CENTER);
                    boxLeituraBarra.add(leituraBarraField.build());
                    boxLeituraBarra.setPadding(new Insets(10, 5, 20, 5));
                }));
                col1.add(FormBox.create(boxTbl -> {
                    boxTbl.horizontal();
                    boxTbl.alignment(Pos.CENTER);
                    boxTbl.add(gradeItemTbl.build());
                }));
                col1.add(FormBox.create(boxFooter -> {
                    boxFooter.horizontal();
                    boxFooter.alignment(Pos.BOTTOM_RIGHT);
                    boxFooter.add(btnCancelar, btnConfirmar);
                }));
            }));
            principal.add(FormBox.create(col2 -> {
                col2.vertical();
                col2.add(FormBox.create(boxUltLeitura -> {
                    boxUltLeitura.vertical();
                    boxUltLeitura.title("Último Produto Inventariado");
                    boxUltLeitura.add(FormBox.create(boxInfosProd -> {
                        boxInfosProd.horizontal();
                        boxInfosProd.add(ultProdutoField.build(), ultCorField.build());
                    }));
                    boxUltLeitura.add(ultLocalField.build());
                }));
            }));
        }));
        Platform.runLater(leituraBarraField::requestFocus);
    }

    private void limparCampos() {
        codigoField.clear();
        descricaoField.clear();
        corField.clear();
        localField.clear();
        leituraBarraField.clear();
        gradeInventarioBean.clear();
    }

    private void carregaCampos() {
        codigoField.value.setValue(itemInventarioSelecionado.getCodigo().getCodigo());
        descricaoField.value.setValue(itemInventarioSelecionado.getCodigo().getDescricao());
        corField.value.setValue(itemInventarioSelecionado.getCodigo().getCor());
        localField.value.setValue(itemInventarioSelecionado.getCodigo().getLocal() == null ? "" : itemInventarioSelecionado.getCodigo().getLocal());
        refreshTable();
    }

    private void carregaInventario() {

        inventarioSelecionado = new FluentDao()
                .selectFrom(SdInventario.class).where(it -> it.equal("status.codigo", STATUS_EM_LEITURA.getCodigo())).findFirst();
        if (inventarioSelecionado == null) {
            inventarioSelecionado = new SdInventario();
            try {
                inventarioSelecionado = new FluentDao().persist(inventarioSelecionado);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void getProximoProdutoLeitura() {
        itemInventarioSelecionado = null;
        init();
    }

    private void confirmarLeituraItemInventario() {
        if (itemInventarioSelecionado == null) return;
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Tem certeza?");
            message.showFullScreen();
        }).value.get())) {
            int acertos = 0;
            int erros = 0;
            SdInventario inventario = itemInventarioSelecionado.getInventario();
            JPAUtils.getEntityManager().refresh(inventario);
            inventario.getItens().add(itemInventarioSelecionado);
            itemInventarioSelecionado.setInventario(inventario);
            for (SdGradeItemInventario itemGrade : itemInventarioSelecionado.getListGrade()) {
                itemGrade.atualizaQuantidadeAtual();
                if (itemGrade.getQtde().intValue() != itemGrade.getQtdelida().intValue()) erros += 1;
                else acertos += 1;
                try {
                    realizarAlteracoesGrade(itemGrade);
                } catch (Exception e) {
                    MessageBox.create(message -> {
                        message.message("Erro na conclusão!");
                        message.type(MessageBox.TypeMessageBox.ERROR);
                        message.showFullScreen();
                    });
                    e.printStackTrace();
                    return;
                }
            }
            itemInventarioSelecionado.setAcertos(acertos);
            itemInventarioSelecionado.setErros(erros);
            inventario.setAcertos(inventario.getItens().stream().mapToInt(SdItemInventario::getAcertos).sum());
            inventario.setErros(inventario.getItens().stream().mapToInt(SdItemInventario::getErros).sum());
            inventario.setQtdeprodutos(inventario.getItens().size());
            itemInventarioSelecionado.setQtdelida(itemInventarioSelecionado.getListGrade().stream().mapToInt(SdGradeItemInventario::getQtdelida).sum());
            itemInventarioSelecionado.setStatus(STATUS_FINALIZADO);
            itemInventarioSelecionado.setHoraleitura(LocalDateTime.now());

            inventario = new FluentDao().merge(inventario);

            ultProdutoField.value.setValue(itemInventarioSelecionado.getCodigo().getCodigo());
            ultCorField.value.setValue(itemInventarioSelecionado.getCodigo().getCor());
            ultLocalField.value.setValue(itemInventarioSelecionado.getCodigo().getLocal() == null ? "" : itemInventarioSelecionado.getCodigo().getLocal());

            SysLogger.addSysDelizLog("Inventario", TipoAcao.EDITAR, itemInventarioSelecionado.getCodigo().getCodigo() + "-" + itemInventarioSelecionado.getCodigo().getCor(), "Inventário do Produto " +
                    itemInventarioSelecionado.getCodigo().getCodigo() + "-" + itemInventarioSelecionado.getCodigo().getCor() + " realizado, pelo usuário " + usuario.getUsuario() + " - " + usuario.getNome());

            MessageBox.create(message -> {
                message.message("Produto lido com sucesso!");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.showFullScreen();
            });
            getProximoProdutoLeitura();
        }
    }

    private void criaMovimentacao(SdGradeItemInventario itemGrade, PaIten deposito, BigDecimal quantidadeNova) {
        if (!deposito.getQuantidade().equals(quantidadeNova)) {
            BigDecimal qtdeMovimentada = deposito.getQuantidade().subtract(quantidadeNova).abs();
            String operacao = quantidadeNova.intValue() > deposito.getQuantidade().intValue() ? "E" : "S";
            String observacao = qtdeMovimentada + " peças " + (quantidadeNova.intValue() > deposito.getQuantidade().intValue() ? "adicionadas " : "removidas ");
            deposito.setQuantidade(quantidadeNova);
            PaMov paMov = new PaMov(itemGrade, deposito.getId().getDeposito(), qtdeMovimentada, operacao, observacao);
            try {
                deposito = new FluentDao().merge(deposito);
                new FluentDao().persist(paMov);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void realizarAlteracoesGrade(SdGradeItemInventario itemGrade) {
        PaIten estoque = new FluentDao().selectFrom(PaIten.class).where(it -> it
                        .equal("id.tam", itemGrade.getTam())
                        .equal("id.cor", itemGrade.getItem().getCodigo().getCor())
                        .equal("id.codigo", itemGrade.getItem().getCodigo().getCodigo())
                        .equal("id.deposito", "0005")
                )
                .singleResult();

        PaIten b2c = new FluentDao().selectFrom(PaIten.class).where(it -> it
                        .equal("id.tam", itemGrade.getTam())
                        .equal("id.cor", itemGrade.getItem().getCodigo().getCor())
                        .equal("id.codigo", itemGrade.getItem().getCodigo().getCodigo())
                        .equal("id.deposito", "0023")
                )
                .singleResult();

        if (estoque == null) {
            estoque = new PaIten(itemGrade, "0005");
        }

        if (b2c == null) criaMovimentacao(itemGrade, estoque, new BigDecimal(itemGrade.getQtdelida()));
        else if (itemGrade.getQtdelida() < b2c.getQuantidade().intValue()) {
            if (estoque.getQuantidade().equals(BigDecimal.ZERO))
                criaMovimentacao(itemGrade, b2c, new BigDecimal(itemGrade.getQtdelida()));
            else {
                criaMovimentacao(itemGrade, b2c, BigDecimal.ZERO);
                criaMovimentacao(itemGrade, estoque, new BigDecimal(itemGrade.getQtdelida()));
            }
        } else
            criaMovimentacao(itemGrade, estoque, new BigDecimal(itemGrade.getQtdelida() - b2c.getQuantidade().intValue()));
    }

    private void leituraBarra(FormFieldText textField) {

        if (inventarioSelecionado == null) carregaInventario();
        String barraLida = textField.value.getValueSafe();
        textField.clear();
        textField.requestFocus();
        if (barraLida.length() < 6) {
            MessageBox.create(message -> {
                message.message("Tamanho da barra lida é incompátivel!");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showFullScreenMobile();
            });
            return;
        }
        VSdDadosProdutoBarra produtoBarra = new FluentDao().selectFrom(VSdDadosProdutoBarra.class).where(it -> it.equal("barra28", barraLida.substring(0, 6))).singleResult();
        if (produtoBarra == null) {
            MessageBox.create(message -> {
                message.message("Barra lida não é compatível com nenhum produto.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showFullScreenMobile();
            });
            return;
        }
        if (itemInventarioSelecionado == null) {
            JPAUtils.getEntityManager().refresh(inventarioSelecionado);
            SdItemInventario itemInventario = new FluentDao().selectFrom(SdItemInventario.class).where(it -> it.equal("codigo.codigo", produtoBarra.getCodigo()).equal("codigo.cor", produtoBarra.getCor()).equal("inventario.id", inventarioSelecionado.getId())).singleResult();
            if (itemInventario != null) {
                if (itemInventario.getStatus().getCodigo() == STATUS_FINALIZADO.getCodigo()) {
                    MessageBox.create(message -> {
                        message.message("Inventário desse produto já realizado!");
                        message.type(MessageBox.TypeMessageBox.WARNING);
                        message.showFullScreen();
                    });
                    return;
                }
                if (itemInventario.getUsuario() != null && !itemInventario.getUsuario().equals(Globals.getNomeUsuario())) {
                    MessageBox.create(message -> {
                        message.message("Inventário atrelado ao usuário " + itemInventario.getUsuario());
                        message.type(MessageBox.TypeMessageBox.WARNING);
                        message.showFullScreen();
                    });
                    return;
                }
                    inventarioSelecionado = new FluentDao().selectFrom(SdInventario.class).where(it -> it.equal("id", itemInventario.getInventario().getId())).singleResult();
                    itemInventarioSelecionado = itemInventario;

            } else {
                VSdProdutoInventario produtoInventario = new FluentDao().selectFrom(VSdProdutoInventario.class).where(it -> it.equal("codigo", produtoBarra.getCodigo()).equal("cor", produtoBarra.getCor())).singleResult();
                if (produtoInventario == null) {
                    MessageBox.create(message -> {
                        message.message("Produto não encontrado, verifique com o PCP o status dele e de suas cores.");
                        message.type(MessageBox.TypeMessageBox.WARNING);
                        message.showFullScreen();
                    });
                    return;
                }
                inventarioSelecionado = new FluentDao().selectFrom(SdInventario.class).where(it -> it.equal("id", inventarioSelecionado.getId())).singleResult();
                itemInventarioSelecionado = new SdItemInventario(inventarioSelecionado, produtoInventario);
                inventarioSelecionado.getItens().add(itemInventarioSelecionado);
            }
            itemInventarioSelecionado.setStatus(STATUS_ITEM_EM_LEITURA);
            itemInventarioSelecionado.setUsuario(Globals.getNomeUsuario());
            if (itemInventarioSelecionado.getListGrade().size() == 0) generateListGrade(itemInventarioSelecionado);
            SysLogger.addSysDelizLog("Inventario", TipoAcao.EDITAR, itemInventarioSelecionado.getCodigo().getCodigo() + "-" + itemInventarioSelecionado.getCodigo().getCor(), "Inventário do Produto " +
                    itemInventarioSelecionado.getCodigo().getCodigo() + "-" + itemInventarioSelecionado.getCodigo().getCor() + " iniciado, pelo usuário " + usuario.getUsuario() + " - " + usuario.getNome());
            carregaCampos();
        }
        if (!validateBarraJaLida(barraLida)) return;
        if (!produtoBarra.getCodigo().equals(itemInventarioSelecionado.getCodigo().getCodigo()) || !produtoBarra.getCor().equals(itemInventarioSelecionado.getCodigo().getCor())) {
            MessageBox.create(message -> {
                message.message("Barra lida não pertence ao produto inventariado!");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showFullScreen();
            });
            return;
        }
        itemInventarioSelecionado.getListGrade().stream().filter(it -> it.getTam().equals(produtoBarra.getTam())).findFirst().ifPresent(itemGrade -> adicionaBarraLida(barraLida, itemGrade));
        try {
            itemInventarioSelecionado = new FluentDao().persist(itemInventarioSelecionado);
        } catch (SQLException e) {
            MessageBox.create(message -> {
                message.message("Erro ao ler a barra!");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showFullScreen();
            });
            e.printStackTrace();
        }
        refreshTable();
    }

    private void refreshTable() {
        gradeInventarioBean.set(FXCollections.observableArrayList(itemInventarioSelecionado.getListGrade()));
        gradeInventarioBean.sort((o1, o2) -> SortUtils.sortTamanhos(o1.getTam(), o2.getTam()));
    }

    private void generateListGrade(SdItemInventario sdItemInventario) {
        sdItemInventario.getListGrade().clear();
        List<PaIten> itensList = (List<PaIten>) new FluentDao().selectFrom(PaIten.class)
                .where(it -> it
                        .equal("id.codigo", sdItemInventario.getCodigo().getCodigo())
                        .equal("id.cor", sdItemInventario.getCodigo().getCor())
                        .isIn("id.deposito", new String[]{"0005", "0023"})
                ).resultList();

        itensList.stream().collect(Collectors.groupingBy(it -> it.getId().getTam())).forEach((tam, list) -> sdItemInventario.getListGrade().add(new SdGradeItemInventario(sdItemInventario, list.stream().mapToInt(eb -> eb.getQuantidade().intValue()).sum(), tam)));
    }

    private void adicionaBarraLida(String barraLida, SdGradeItemInventario itemGrade) {
        SdBarraItemInventario sdBarra = new SdBarraItemInventario(itemGrade, barraLida);
        itemGrade.getListBarra().add(sdBarra);
        itemGrade.setQtdelida(itemGrade.getListBarra().size());
    }

    private boolean validateBarraJaLida(String barraLida) {
        SdBarraItemInventario barraItemInventario = new FluentDao().selectFrom(SdBarraItemInventario.class).where(it -> it.equal("barra", barraLida)).singleResult();
        if (barraItemInventario != null && barraItemInventario.getItemgrade().getItem().getInventario().getId().equals(inventarioSelecionado.getId())) {
            MessageBox.create(message -> {
                message.message("Barra já lida!");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showFullScreen();
            });
            return false;
        }
        return true;
    }

    private void cancelarLeituraInventario() {
        if (itemInventarioSelecionado == null) return;
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Tem certeza que deseja cancelar?");
            message.showAndWait();
        }).value.get())) {
            itemInventarioSelecionado.setUsuario(null);
            itemInventarioSelecionado.setStatus(STATUS_ITEM_NAO_INVENTARIADO);
            itemInventarioSelecionado.getListGrade().clear();

            itemInventarioSelecionado = new FluentDao().merge(itemInventarioSelecionado);
            getProximoProdutoLeitura();
        }
    }
}
