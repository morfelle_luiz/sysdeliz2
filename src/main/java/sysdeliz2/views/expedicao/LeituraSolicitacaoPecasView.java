package sysdeliz2.views.expedicao;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import sysdeliz2.controllers.views.expedicao.LeituraSolicitacaoPecasController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.models.sysdeliz.solicitacaoExp.*;
import sysdeliz2.models.ti.*;
import sysdeliz2.models.view.VSdDadosProdutoBarra;
import sysdeliz2.utils.ReportUtils;
import sysdeliz2.utils.SortUtils;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class LeituraSolicitacaoPecasView extends LeituraSolicitacaoPecasController {

    // <editor-fold defaultstate="collapsed" desc="List Bean">
    private final ListProperty<SdSolicitacaoExp> solicitacoesBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<SdItemSolicitacaoExp> itensSolicitacaoLeituraBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<SdItemSolicitacaoExp> itemGradeLeituraBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<SdBarraItemSolicExp> barrasItemSolicitacaoBean = new SimpleListProperty<>(FXCollections.observableArrayList());

    private final List<SdStatusSolicitacaoExp> listStatus = (List<SdStatusSolicitacaoExp>) new FluentDao().selectFrom(SdStatusSolicitacaoExp.class).get().orderBy("codigo", OrderType.ASC).resultList();
    // </editor-fold>

    private final BooleanProperty emLeitura = new SimpleBooleanProperty(false);
    private final BooleanProperty removendo = new SimpleBooleanProperty(false);
    private final BooleanProperty recebendo = new SimpleBooleanProperty(false);

    private SdSolicitacaoExp solicitacaoEscolhida = null;
    private SdItemSolicitacaoExp itemSolicitacaoEscolhida = null;


    // <editor-fold defaultstate="collapsed" desc="Table">

    private final FormTableView<SdItemSolicitacaoExp> tblProdutos = FormTableView.create(SdItemSolicitacaoExp.class, table -> {
        table.title("Produtos");
        table.items.bind(itensSolicitacaoLeituraBean);
        table.selectionModelItem((observable, oldValue, newValue) -> {
            if (newValue != null) carregaGradeItemSolicitacao((SdItemSolicitacaoExp) newValue);
        });
        table.expanded();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemSolicitacaoExp, SdItemSolicitacaoExp>, ObservableValue<SdItemSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdItemSolicitacaoExp, SdItemSolicitacaoExp>() {
                            @Override
                            protected void updateItem(SdItemSolicitacaoExp item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);

                                if (item != null && !empty) {
                                    setText(item.getProduto().getCodigo());
                                }
                            }
                        };
                    });
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Cor");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemSolicitacaoExp, SdItemSolicitacaoExp>, ObservableValue<SdItemSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdItemSolicitacaoExp, SdItemSolicitacaoExp>() {
                            @Override
                            protected void updateItem(SdItemSolicitacaoExp item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);

                                if (item != null && !empty) {
                                    setText(item.getProduto().getCor());
                                }
                            }
                        };
                    });
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Qtde Lida");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemSolicitacaoExp, SdItemSolicitacaoExp>, ObservableValue<SdItemSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdItemSolicitacaoExp, SdItemSolicitacaoExp>() {
                            @Override
                            protected void updateItem(SdItemSolicitacaoExp item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);

                                if (item != null && !empty) {
                                    setText(String.valueOf(getCountBarrasLidas(item)));
                                }
                            }
                        };
                    });
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Qtde");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemSolicitacaoExp, SdItemSolicitacaoExp>, ObservableValue<SdItemSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdItemSolicitacaoExp, SdItemSolicitacaoExp>() {
                            @Override
                            protected void updateItem(SdItemSolicitacaoExp item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);

                                if (item != null && !empty) {
                                    setText(String.valueOf(item.getQtde()));
                                }
                            }
                        };
                    });
                }).build() /*Código*/
        );
    });

    private final FormTableView<SdBarraItemSolicExp> tblBarraProdutos = FormTableView.create(SdBarraItemSolicExp.class, table -> {
        table.title("Barra");
        table.items.bind(barrasItemSolicitacaoBean);
        table.expanded();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Barra");
                    cln.width(200);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBarraItemSolicExp, SdBarraItemSolicExp>, ObservableValue<SdBarraItemSolicExp>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdBarraItemSolicExp, SdBarraItemSolicExp>() {
                            @Override
                            protected void updateItem(SdBarraItemSolicExp item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);

                                if (item != null && !empty) {
                                    getStyleClass().add("md");
                                    setText(item.getBarra());
                                }
                            }
                        };
                    });
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBarraItemSolicExp, SdBarraItemSolicExp>, ObservableValue<SdBarraItemSolicExp>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdBarraItemSolicExp, SdBarraItemSolicExp>() {
                            @Override
                            protected void updateItem(SdBarraItemSolicExp item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);

                                if (item != null && !empty) {
                                    getStyleClass().add("md");
                                    setText(item.getItemSolicitacao().getProduto().getCodigo());
                                }
                            }
                        };
                    });
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Cor");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBarraItemSolicExp, SdBarraItemSolicExp>, ObservableValue<SdBarraItemSolicExp>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdBarraItemSolicExp, SdBarraItemSolicExp>() {
                            @Override
                            protected void updateItem(SdBarraItemSolicExp item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);

                                if (item != null && !empty) {
                                    getStyleClass().add("md");
                                    setText(item.getItemSolicitacao().getProduto().getCor());
                                }
                            }
                        };
                    });
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Tam");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdBarraItemSolicExp, SdBarraItemSolicExp>, ObservableValue<SdBarraItemSolicExp>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdBarraItemSolicExp, SdBarraItemSolicExp>() {
                            @Override
                            protected void updateItem(SdBarraItemSolicExp item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);

                                if (item != null && !empty) {
                                    getStyleClass().add("md");
                                    setText(item.getItemSolicitacao().getProduto().getTam());
                                }
                            }
                        };
                    });
                }).build() /*Código*/
        );
    });

    private final FormTableView<SdItemSolicitacaoExp> tblGradeLeitura = FormTableView.create(SdItemSolicitacaoExp.class, table -> {
        table.title("Barra");
        table.items.bind(itemGradeLeituraBean);
        table.factoryRow(param -> new TableRow<SdItemSolicitacaoExp>() {
            @Override
            protected void updateItem(SdItemSolicitacaoExp item, boolean empty) {
                super.updateItem(item, empty);
                clear();

                if (item != null && !empty) {
                    if (getCountBarrasLidasTam(item) == item.getQtde()) {
                        getStyleClass().add("table-row-success");
                    } else if (getCountBarrasLidasTam(item) == 0) {
                        getStyleClass().add("table-row-danger");
                    } else {
                        getStyleClass().add("table-row-warning");
                    }
                }
            }

            private void clear() {
                getStyleClass().removeIf(it -> it.contains("table-row-") && !it.equals("table-row-cell"));
            }
        });
        table.expanded();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Tam");
                    cln.width(100);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemSolicitacaoExp, SdItemSolicitacaoExp>, ObservableValue<SdItemSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdItemSolicitacaoExp, SdItemSolicitacaoExp>() {
                            @Override
                            protected void updateItem(SdItemSolicitacaoExp item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);

                                if (item != null && !empty) {
                                    getStyleClass().add("lg");
                                    setText(item.getProduto().getTam());
                                }
                            }
                        };
                    });
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Qtde Lida");
                    cln.width(100);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemSolicitacaoExp, SdItemSolicitacaoExp>, ObservableValue<SdItemSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdItemSolicitacaoExp, SdItemSolicitacaoExp>() {
                            @Override
                            protected void updateItem(SdItemSolicitacaoExp item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);

                                if (item != null && !empty) {
                                    getStyleClass().add("lg");
                                    setText(String.valueOf(getCountBarrasLidasTam(item)));
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Qtde");
                    cln.width(100);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemSolicitacaoExp, SdItemSolicitacaoExp>, ObservableValue<SdItemSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdItemSolicitacaoExp, SdItemSolicitacaoExp>() {
                            @Override
                            protected void updateItem(SdItemSolicitacaoExp item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    getStyleClass().add("lg");
                                    setText(String.valueOf(item.getQtde()));
                                }
                            }
                        };
                    });
                }).build()
        );
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Filter">

    private final FormFieldDatePeriod filterDataSolicitacao = FormFieldDatePeriod.create(field -> {
        field.title("Data");
        field.setPadding(new Insets(-5, 5, 5, 5));
        field.withLabels();
        field.valueBegin.setValue(LocalDate.now().minusMonths(1));
        field.valueEnd.setValue(LocalDate.now().plusMonths(1));
    });

    private final FormFieldSingleFind<SdTipoSolicitacaoExp> filterTipoSolicitacao = FormFieldSingleFind.create(SdTipoSolicitacaoExp.class, field -> {
        field.title("Tipo");
        field.dividedWidth(520);
    });

    private final FormFieldMultipleFind<Produto> filterProduto = FormFieldMultipleFind.create(Produto.class, field -> {
        field.title("Produto");
        field.width(200);
    });

    private final FormFieldMultipleFind<SdSolicitacaoExp> filterSolicitacaoExp = FormFieldMultipleFind.create(SdSolicitacaoExp.class, field -> {
        field.title("Código");
        field.width(200);
    });

    private final FormFieldMultipleFind<Colecao> filterColecao = FormFieldMultipleFind.create(Colecao.class, field -> {
        field.title("Coleção");
        field.width(200);
    });

    private final FormFieldMultipleFind<Marca> filterMarca = FormFieldMultipleFind.create(Marca.class, field -> {
        field.title("Marca");
        field.width(200);
    });

    private final FormFieldText filterUsuario = FormFieldText.create(field -> {
        field.title("Usuario");
        field.width(200);
    });

    private final FormFieldText fieldBarraLeitura = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label(ImageUtils.getIcon(ImageUtils.Icon.LEITURA_BARRA, ImageUtils.IconSize._24));
        field.width(580);
        field.alignment(Pos.CENTER);
        field.addStyle("lg");
        removendo.addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                field.addStyle("danger");
            } else {
                field.removeStyle("danger");
            }
        });
        field.keyReleased(evt -> {
            String barraLida = field.value.getValueSafe();
            if (evt.getCode().equals(KeyCode.ENTER) && !barraLida.equals("")) {
                if (removendo.not().get()) adicionandoBarra(barraLida);
                else removendoBarra(barraLida);
                field.clear();
            }

        });
    });

    private final FormFieldSingleFind<SdSolicitacaoExp> filterSolicitacao = FormFieldSingleFind.create(SdSolicitacaoExp.class, field -> {
        field.title("Código");
        field.dividedWidth(500);
        field.editable.bind(emLeitura.not());
        field.value.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                carregaSolicitacaoLeitura((SdSolicitacaoExp) newValue);
            }
        });
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="TextField">
    private final FormFieldText fieldNumeroSolicitacaoLeitura = FormFieldText.create(field -> {
        field.title("Código");
        field.editable.set(false);
        field.width(120);
    });

    private final FormFieldText fieldTipoSolicitacaoleitura = FormFieldText.create(field -> {
        field.title("Tipo Solicitação");
        field.editable.set(false);
        field.width(340);
    });

    private final FormFieldText fieldUsuarioLeitura = FormFieldText.create(field -> {
        field.title("Usuário");
        field.editable.set(false);
        field.width(170);
    });

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Button">
    private final FormButton btnStatusLeitura = FormButton.create(btn -> {
        btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
        btn.addStyle("success");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._32));
        btn.setOnAction(evt -> {
            handleStatusLeitura();
        });
    });

    private final Button btnConfirmarLeitura = FormButton.create(btn -> {
        btn.setText("Confirmar");
        btn.addStyle("success");
        btn.width(150);
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            confirmarLeitura();
        });
    });

    private final Button btnCancelarLeitura = FormButton.create(btn -> {
        btn.setText("Cancelar");
        btn.addStyle("danger");
        btn.width(150);
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            cancelarLeitura();
        });
    });

    private final Button btnSalvarSolicitacao = FormButton.create(btn -> {
        btn.title("Salvar");
        btn.addStyle("primary");
        btn.width(150);
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.SALVAR, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            salvarSolicitacao();
        });
    });

    private final Button btnImprimirSolicitacao = FormButton.create(btn -> {
        btn.title("Imprimir");
        btn.addStyle("primary");
        btn.width(120);
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.IMPRIMIR, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            imprimirSolicitacao();
        });
    });

    private final Button btnCarregarSolicitacao = FormButton.create(btn -> {
        btn.title("Leitura");
        btn.addStyle("success");
        btn.width(120);
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            if (solicitacaoEscolhida != null)
                filterSolicitacao.value.setValue(solicitacaoEscolhida);
        });
    });

    // </editor-fold>

    private final FormTableView<SdSolicitacaoExp> tblSolicitacao = FormTableView.create(SdSolicitacaoExp.class, table -> {
        table.title("Solicitações");
        table.expanded();
        table.items.bind(solicitacoesBean);
        table.factoryRow(param -> new TableRow<SdSolicitacaoExp>() {
            @Override
            protected void updateItem(SdSolicitacaoExp item, boolean empty) {
                super.updateItem(item, empty);
                clear();

                if (item != null && !empty) {
                    getStyleClass().add("table-row-" + item.getStatus().getStyle());
                }
            }

            private void clear() {
                getStyleClass().removeIf(it -> it.contains("table-row-") && !it.equals("table-row-cell"));
            }
        });
        listStatus.forEach(it -> table.addIndice(table.indice(it.getStatus(), it.getStyle())));
        table.selectionModelItem((observable, oldValue, newValue) -> {
            if (newValue != null) solicitacaoEscolhida = (SdSolicitacaoExp) newValue;
        });
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(120);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdSolicitacaoExp, SdSolicitacaoExp>, ObservableValue<SdSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId()));
                }).build(), /**/
                FormTableColumn.create(cln -> {
                    cln.title("Tipo");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdSolicitacaoExp, SdSolicitacaoExp>, ObservableValue<SdSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTipo().getTipo()));
                }).build(), /**/
                FormTableColumn.create(cln -> {
                    cln.title("Descrição Tipo");
                    cln.width(400);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdSolicitacaoExp, SdSolicitacaoExp>, ObservableValue<SdSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTipo().getDescricao()));
                }).build(), /**/
                FormTableColumn.create(cln -> {
                    cln.title("Dt Solicitação");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdSolicitacaoExp, SdSolicitacaoExp>, ObservableValue<SdSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(StringUtils.toShortDateFormat(param.getValue().getDtSolicitacao())));
                }).build(), /**/
                FormTableColumn.create(cln -> {
                    cln.title("Dt Envio Produto");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdSolicitacaoExp, SdSolicitacaoExp>, ObservableValue<SdSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(StringUtils.toShortDateFormat(param.getValue().getDtEnvioProduto())));
                }).build(), /**/
                FormTableColumn.create(cln -> {
                    cln.title("Dt Devolução");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdSolicitacaoExp, SdSolicitacaoExp>, ObservableValue<SdSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(StringUtils.toShortDateFormat(param.getValue().getDtDevolucao())));
                }).build(), /**/
                FormTableColumn.create(cln -> {
                    cln.title("Status");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdSolicitacaoExp, SdSolicitacaoExp>, ObservableValue<SdSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(param.getValue().getStatus().getStatus()));
                }).build(), /**/
                FormTableColumn.create(cln -> {
                    cln.title("Usuário");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdSolicitacaoExp, SdSolicitacaoExp>, ObservableValue<SdSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(param.getValue().getUsuario()));
                }).build() /**/
        );
    });

    // <editor-fold defaultstate="collapsed" desc="Box">
    private final VBox tabListagem = (VBox) super.tabs.getTabs().get(0).getContent();
    private final VBox tabLeitura = (VBox) super.tabs.getTabs().get(1).getContent();
    // </editor-fold>

    public LeituraSolicitacaoPecasView() {
        super("Leitura de Solicitações", ImageUtils.getImage(ImageUtils.Icon.LEITURA_BARRA), new String[]{"Listagem", "Leitura"});
        initListagem();
        initLeitura();
        solicitacoesBean.set(FXCollections.observableArrayList((List<SdSolicitacaoExp>) new FluentDao().selectFrom(SdSolicitacaoExp.class).where(it -> it.equal("status.codigo", STATUS_LIBERADO_LEITURA.getCodigo())).resultList()));
        tblSolicitacao.refresh();
    }

    private void initListagem() {
        tabListagem.getChildren().add(FormBox.create(principal -> {
            principal.vertical();
            principal.expanded();
            principal.add(FormBox.create(boxHeader -> {
                boxHeader.horizontal();
                boxHeader.add(FormBox.create(boxTitledFilter -> {
                    boxTitledFilter.vertical();
                    boxTitledFilter.add(FormTitledPane.create(filter -> {
                        filter.filter();
                        filter.width(1200);
                        filter.add(FormBox.create(boxFilter -> {
                            boxFilter.vertical();
                            boxFilter.add(FormBox.create(linha1 -> {
                                linha1.horizontal();
                                linha1.add(filterSolicitacaoExp.build(), filterTipoSolicitacao.build(), filterUsuario.build());
                            }));
                            boxFilter.add(FormBox.create(linha2 -> {
                                linha2.horizontal();
                                linha2.add(filterProduto.build(), filterColecao.build(), filterMarca.build(), filterDataSolicitacao.build());
                            }));
                        }));
                        filter.find.setOnAction(evt -> {
                            new RunAsyncWithOverlay(this).exec(task -> {
                                buscarSolicitacoes(
                                        filterSolicitacaoExp.objectValues.stream().map(SdSolicitacaoExp::getId).toArray(),
                                        filterTipoSolicitacao.value.get() == null ? new SdTipoSolicitacaoExp() : filterTipoSolicitacao.value.get(),
                                        filterDataSolicitacao.valueBegin.getValue(),
                                        filterDataSolicitacao.valueEnd.getValue(),
                                        filterProduto.objectValues.stream().map(Produto::getCodigo).toArray(),
                                        filterColecao.objectValues.stream().map(Colecao::getCodigo).toArray(),
                                        filterMarca.objectValues.stream().map(Marca::getCodigo).toArray(),
                                        filterUsuario.value.getValueSafe()
                                );
                                return ReturnAsync.OK.value;
                            }).addTaskEndNotification(taskReturn -> {
                                if (taskReturn.equals(ReturnAsync.OK.value)) {
                                    solicitacoesBean.set(FXCollections.observableArrayList(solicitacoesExp));
                                }
                            });
                        });
                        filter.clean.setOnAction(evt -> {
                            filterSolicitacaoExp.clear();
                            filterTipoSolicitacao.clear();
                            filterDataSolicitacao.clear();
                            filterProduto.clear();
                            filterColecao.clear();
                            filterMarca.clear();
                            filterUsuario.clear();
                        });
                    }));
                }));
            }));
            principal.add(FormBox.create(boxCenter -> {
                boxCenter.horizontal();
                boxCenter.expanded();
                boxCenter.add(tblSolicitacao.build());
            }));
            principal.add(FormBox.create(boxFooter -> {
                boxFooter.horizontal();
                boxFooter.add(btnImprimirSolicitacao, btnCarregarSolicitacao);
            }));
        }));
    }

    private void initLeitura() {
        tabLeitura.getChildren().add(FormBox.create(root -> {
            root.horizontal();
            root.expanded();
            root.add(FormBox.create(col1 -> {
                col1.vertical();
                col1.title("Solicitação");
                col1.expanded();
                col1.add(FormBox.create(boxItensSoli -> {
                    boxItensSoli.vertical();
                    boxItensSoli.expanded();
                    boxItensSoli.add(FormBox.create(boxBuscaSolici -> {
                        boxBuscaSolici.horizontal();
                        boxBuscaSolici.add(filterSolicitacao.build());
                    }));
                    boxItensSoli.add(tblProdutos.build());
                }));
            }));
            root.add(FormBox.create(col2 -> {
                col2.vertical();
                col2.expanded();
                col2.add(FormBox.create(boxFiltroBarra -> {
                    boxFiltroBarra.vertical();
                    boxFiltroBarra.title("Infos Solicitação");
                    boxFiltroBarra.alignment(Pos.TOP_CENTER);
                    boxFiltroBarra.add(FormBox.create(linha -> {
                        linha.horizontal();
                        linha.add(fieldNumeroSolicitacaoLeitura.build(), fieldTipoSolicitacaoleitura.build(), fieldUsuarioLeitura.build());
                    }));
                    boxFiltroBarra.add(FormBox.create(linha -> {
                        linha.horizontal();
                        linha.add(fieldBarraLeitura.build(), btnStatusLeitura);
                    }));
                }));
                col2.add(FormBox.create(boxLinha -> {
                    boxLinha.horizontal();
                    boxLinha.expanded();
                    boxLinha.add(FormBox.create(boxTblBarra -> {
                        boxTblBarra.vertical();
                        boxTblBarra.add(tblBarraProdutos.build());
                    }));
                    boxLinha.add(FormBox.create(boxGrade -> {
                        boxGrade.vertical();
                        boxGrade.add(tblGradeLeitura.build());
                        boxGrade.add(FormBox.create(boxBtns -> {
                            boxBtns.horizontal();
                            boxBtns.add(btnCancelarLeitura, btnSalvarSolicitacao, btnConfirmarLeitura);
                        }));
                    }));
                }));
            }));
        }));
    }

    private void handleStatusLeitura() {
        if (recebendo.get()) return;
        removendo.set(removendo.not().getValue());
        if (removendo.get()) {
            btnStatusLeitura.getStyleClass().remove("success");
            btnStatusLeitura.addStyle("danger");
            btnStatusLeitura.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._32));
        } else {
            btnStatusLeitura.getStyleClass().remove("danger");
            btnStatusLeitura.addStyle("success");
            btnStatusLeitura.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._32));
        }

    }

    private void cancelarLeitura() {
        if (solicitacaoEscolhida == null) return;
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Tem certeza que deseja cancelar?");
            message.showAndWait();
        }).value.get())) {
            emLeitura.set(false);
            limparCampos();
        }
    }

    private void confirmarLeitura() {
        if (solicitacaoEscolhida == null) return;
//        if (solicitacaoEscolhida.getItens().stream().mapToInt(SdItemSolicitacaoExp::getQtde).sum() != solicitacaoEscolhida.getItens().stream().mapToInt(it -> it.getBarras().size()).sum()) {
//            MessageBox.create(message -> {
//                message.message("Você ainda não leu todos produtos dessa solicitação, faça a leitura de todos antes de concluir");
//                message.type(MessageBox.TypeMessageBox.WARNING);
//                message.showAndWait();
//            });
//            return;
//        }
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Tem certeza que deseja concluir a leitura?");
            message.showAndWait();
        }).value.get())) {
            emLeitura.set(false);
            if (recebendo.get()) {
                solicitacaoEscolhida.setStatus(STATUS_FINALIZADA);
                solicitacaoEscolhida.setDtDevolucao(LocalDate.now());
                transferirDeposito(solicitacaoEscolhida, "0001", "0005");
                SysLogger.addSysDelizLog("Leitura Solicitação de Peças", TipoAcao.EDITAR, String.valueOf(solicitacaoEscolhida.getId()), "Solicitação " + solicitacaoEscolhida.getId() + " teve as peças devolvidas");
            } else {
                solicitacaoEscolhida.setStatus(STATUS_PCS_ENTREGUES);
                solicitacaoEscolhida.setDtEnvioProduto(LocalDate.now());
                transferirDeposito(solicitacaoEscolhida, "0005", "0001");
                SysLogger.addSysDelizLog("Leitura Solicitação de Peças", TipoAcao.EDITAR, String.valueOf(solicitacaoEscolhida.getId()), "Solicitação " + solicitacaoEscolhida.getId() + " teve a leitura finalizada e as peças entregues");
            }
            solicitacaoEscolhida = new FluentDao().merge(solicitacaoEscolhida);

            limparCampos();
            MessageBox.create(message -> {
                message.message("Leitura concluída com sucesso");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
        }
    }

    private void transferirDeposito(SdSolicitacaoExp solicitacaoEscolhida, String depositoOrigem, String depositoDestino) {

        for (SdItemSolicitacaoExp item : solicitacaoEscolhida.getItens().stream().filter(it -> it.getBarras().size() > 0).collect(Collectors.toList())) {
            List<PaIten> itens = (List<PaIten>) new FluentDao().selectFrom(PaIten.class).where(it -> it
                    .equal("id.codigo", item.getProduto().getCodigo())
                    .equal("id.cor", item.getProduto().getCor())
                    .equal("id.tam", item.getProduto().getTam())
            ).resultList();

            if (itens == null || itens.size() == 0) {
                MessageBox.create(message -> {
                    message.message("Houve um erro na transferência para o deposito 0001, consulte o cadastro desses produtos");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showAndWait();
                });
                continue;
            }

            PaIten origem = itens.stream().filter(it -> it.getId().getDeposito().equals(depositoOrigem)).findFirst().orElse(null);
            PaIten destino = itens.stream().filter(it -> it.getId().getDeposito().equals(depositoDestino)).findFirst().orElse(null);

            if (origem == null || destino == null) {
                MessageBox.create(message -> {
                    message.message("Houve um erro na transferência entre os depositos, consulte o cadastro desses produtos");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showAndWait();
                });
                continue;
            }

            try {
                new FluentDao().persist(new PaMov(item, depositoOrigem, "S", "Solicitação " + solicitacaoEscolhida.getId() + " - Saída do depósito " + depositoOrigem + " - " + item.getQtde() + " pçs"));
                new FluentDao().persist(new PaMov(item, depositoDestino, "E", "Solicitação " + solicitacaoEscolhida.getId() + " - Entrada no depósito " + depositoDestino + " - " + item.getQtde() + " pçs"));
            } catch (Exception e) {
                e.printStackTrace();
            }

            SysLogger.addSysDelizLog(
                    "Leitura Solicitação de Peças",
                    TipoAcao.MOVIMENTAR,
                    solicitacaoEscolhida.getId() + "-" + item.getProduto().getCodigo() + "-" + item.getProduto().getCor() + "-" + item.getProduto().getTam(),
                    "Solicitação " + solicitacaoEscolhida.getId() + " - Saída do depósito " + depositoOrigem + " - " + item.getQtde() + " pçs"
            );

            SysLogger.addSysDelizLog(
                    "Leitura Solicitação de Peças",
                    TipoAcao.MOVIMENTAR,
                    solicitacaoEscolhida.getId() + "-" + item.getProduto().getCodigo() + "-" + item.getProduto().getCor() + "-" + item.getProduto().getTam(),
                    "Solicitação " + solicitacaoEscolhida.getId() + " - Entrada no depósito " + depositoDestino + " - " + item.getQtde() + " pçs"
            );

            origem.setQuantidade(BigDecimal.valueOf(origem.getQuantidade().intValue() - item.getQtde()));
            destino.setQuantidade(BigDecimal.valueOf(destino.getQuantidade().intValue() + item.getQtde()));

            new FluentDao().merge(origem);
            new FluentDao().merge(destino);
        }

        MessageBox.create(message -> {
            message.message("Produtos transferidos para o depósito " + depositoDestino);
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();

        });
    }

    private void salvarSolicitacao() {
        if (solicitacaoEscolhida == null) return;
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Tem certeza que deseja salvar a leitura feita até o momento?");
            message.showAndWait();
        }).value.get())) {
            emLeitura.set(false);
            solicitacaoEscolhida = new FluentDao().merge(solicitacaoEscolhida);
            limparCampos();
            MessageBox.create(message -> {
                message.message("Leitura salva!");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
        }
    }

    private void limparCampos() {
        tblBarraProdutos.clear();
        tblGradeLeitura.clear();
        tblProdutos.clear();

        fieldUsuarioLeitura.clear();
        fieldTipoSolicitacaoleitura.clear();
        fieldNumeroSolicitacaoLeitura.clear();
        filterSolicitacao.clear();

        solicitacaoEscolhida = null;
        itemSolicitacaoEscolhida = null;

        tblSolicitacao.tableview().getSelectionModel().clearSelection();
    }

    private void carregaSolicitacaoLeitura(SdSolicitacaoExp solicitacao) {

        if (solicitacao == null) return;

        if (solicitacao.getStatus().equals(STATUS_FINALIZADA)) {
            MessageBox.create(message -> {
                message.message("Solicitação já foi finalizada");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            filterSolicitacao.clear();
            return;
        }

        if (!solicitacao.getStatus().equals(STATUS_LIBERADO_LEITURA) && !solicitacao.getStatus().equals(STATUS_PCS_ENTREGUES)) {
            MessageBox.create(message -> {
                message.message("Solicitação não está liberada para leitura");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            filterSolicitacao.clear();
            return;
        }

        recebendo.set(false);

        if (solicitacao.getStatus().equals(STATUS_PCS_ENTREGUES)) {
            recebendo.set(true);
        }

        tabs.getSelectionModel().select(1);

        itensSolicitacaoLeituraBean.clear();
        barrasItemSolicitacaoBean.clear();
        solicitacaoEscolhida = solicitacao;
        fieldNumeroSolicitacaoLeitura.setValue(String.valueOf(solicitacaoEscolhida.getId()));
        fieldTipoSolicitacaoleitura.setValue(solicitacaoEscolhida.getTipo().getTipo() + " - " + solicitacaoEscolhida.getTipo().getDescricao());
        fieldUsuarioLeitura.setValue(solicitacaoEscolhida.getUsuario());

        Map<String, Map<String, List<SdItemSolicitacaoExp>>> collect = solicitacaoEscolhida.getItens().stream().collect(Collectors.groupingBy((SdItemSolicitacaoExp item) ->
                        item.getProduto().getCodigo(), Collectors.groupingBy(item ->
                        item.getProduto().getCor()
                )
        ));
        collect.forEach((codigo, mapCores) ->
                mapCores.forEach((cor, list) ->
                        itensSolicitacaoLeituraBean.add(
                                new SdItemSolicitacaoExp(list.get(0).getProduto(), list.stream().mapToInt(SdItemSolicitacaoExp::getQtde).sum(), solicitacaoEscolhida)
                        )));

        if (!recebendo.get())
            barrasItemSolicitacaoBean.addAll(solicitacaoEscolhida.getItens().stream().flatMap(it -> it.getBarras().stream()).collect(Collectors.toList()));

        itensSolicitacaoLeituraBean.sort(Comparator.comparing((SdItemSolicitacaoExp it) ->
                it.getProduto().getCodigo()).thenComparing(it ->
                it.getProduto().getCor()).thenComparing((o1, o2) ->
                SortUtils.sortTamanhos(o1.getProduto().getTam(), o2.getProduto().getTam())));

        barrasItemSolicitacaoBean.sort(Comparator.comparing((SdBarraItemSolicExp it) ->
                it.getItemSolicitacao().getProduto().getCodigo()).thenComparing(it ->
                it.getItemSolicitacao().getProduto().getCor()).thenComparing((o1, o2) ->
                SortUtils.sortTamanhos(o1.getItemSolicitacao().getProduto().getTam(), o2.getItemSolicitacao().getProduto().getTam())));

        fieldBarraLeitura.requestFocus();
    }

    private void adicionandoBarra(String barraLida) {
        VSdDadosProdutoBarra barra28 = new FluentDao().selectFrom(VSdDadosProdutoBarra.class).where(it -> it.equal("barra28", barraLida.substring(0, 6))).singleResult();
        if (barra28 == null) {
            MessageBox.create(message -> {
                message.message("Barra lida não associada a nenhum produto");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }
        SdBarraItemSolicExp barraDuplicada = barrasItemSolicitacaoBean.stream().filter(it -> it.getBarra().equals(barraLida)).findFirst().orElse(null);

        SdBarraItemSolicExp itemBanco = new FluentDao().selectFrom(SdBarraItemSolicExp.class)
                .where(it -> it.equal("barra", barraLida).equal("itemSolicitacao.solicitacao.id", solicitacaoEscolhida.getId())).singleResult();

        if (recebendo.get() && itemBanco == null) {
            MessageBox.create(message -> {
                message.message("Barra lida não pertence a nenhum produto enviado anteriormente");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }
        if (recebendo.not().get() && barraDuplicada != null) {
            MessageBox.create(message -> {
                message.message("Barra já lida");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }
        SdItemSolicitacaoExp itemSolic = solicitacaoEscolhida.getItens().stream().filter(it -> it
                        .getProduto().getCodigo().equals(barra28.getCodigo()) &&
                        it.getProduto().getCor().equals(barra28.getCor()) &&
                        it.getProduto().getTam().equals(barra28.getTam()))
                .findFirst().orElse(null);

        if (itemSolic == null) {
            MessageBox.create(message -> {
                message.message("Barra lida não pertence a nenhum produto dessa solicitação");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }
        if (recebendo.not().get()) {
            SdBarraItemSolicExp barraItemSolic = new SdBarraItemSolicExp(itemSolic, barraLida);
            itemSolic.getBarras().add(barraItemSolic);
            barrasItemSolicitacaoBean.add(barraItemSolic);
        } else {
            barrasItemSolicitacaoBean.add(itemBanco);
        }

        carregaGradeItemSolicitacao(itemSolic);
        MessageBox.create(message -> {
            message.message("Barra lida com sucesso");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
        tblGradeLeitura.refresh();
    }

    private void carregaGradeItemSolicitacao(SdItemSolicitacaoExp itemSolic) {
        if (itemSolicitacaoEscolhida == null || !itemSolicitacaoEscolhida.equals(itemSolic)) {
            itemSolicitacaoEscolhida = itemSolic;
            List<SdItemSolicitacaoExp> listGrade = solicitacaoEscolhida.getItens().stream().filter(it ->
                            it.getProduto().getCodigo().equals(itemSolic.getProduto().getCodigo()) && it.getProduto().getCor().equals(itemSolic.getProduto().getCor()))
                    .collect(Collectors.toList());

            itemGradeLeituraBean.set(FXCollections.observableArrayList(listGrade));
            itemGradeLeituraBean.sort((o1, o2) -> SortUtils.sortTamanhos(o1.getProduto().getTam(), o2.getProduto().getTam()));
        }
    }

    private void removendoBarra(String barraLida) {
        SdBarraItemSolicExp barraItem = solicitacaoEscolhida.getItens().stream().flatMap(it -> it.getBarras().stream()).collect(Collectors.toList()).stream().filter(it -> it.getBarra().equals(barraLida)).findFirst().orElse(null);
        if (barraItem == null) {
            MessageBox.create(message -> {
                message.message("Barra lida não pertence a nenhum produto já lido");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            return;
        }

        SdItemSolicitacaoExp itemSolicitacao = barraItem.getItemSolicitacao();
        itemSolicitacao.getBarras().remove(barraItem);

        barrasItemSolicitacaoBean.removeIf(it -> it.getBarra().equals(barraItem.getBarra()));

        SdBarraItemSolicExp barra = new FluentDao().selectFrom(SdBarraItemSolicExp.class).where(it -> it.equal("barra", barraItem.getBarra())).singleResult();

        if (barra != null) {
            new FluentDao().delete(barra);
        }

        MessageBox.create(message -> {
            message.message("Barra removida com sucesso");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }

    private long getCountBarrasLidasTam(SdItemSolicitacaoExp item) {
        return barrasItemSolicitacaoBean.stream().filter(it -> it
                .getItemSolicitacao().getProduto().getCodigo().equals(item.getProduto().getCodigo()) &&
                it.getItemSolicitacao().getProduto().getCor().equals(item.getProduto().getCor()) &&
                it.getItemSolicitacao().getProduto().getTam().equals(item.getProduto().getTam())
        ).count();
    }

    private long getCountBarrasLidas(SdItemSolicitacaoExp item) {
        return barrasItemSolicitacaoBean.stream().filter(it -> it
                .getItemSolicitacao().getProduto().getCodigo().equals(item.getProduto().getCodigo()) &&
                it.getItemSolicitacao().getProduto().getCor().equals(item.getProduto().getCor())
        ).count();
    }

    private void imprimirSolicitacao() {

        if (solicitacaoEscolhida == null) return;

        SysLogger.addSysDelizLog("Analise Solicitação de Peças", TipoAcao.CADASTRAR, String.valueOf(solicitacaoEscolhida.getId()), "Solicitação " + solicitacaoEscolhida.getId() + " impressa");

        try {
            new ReportUtils().config().addReport(ReportUtils.ReportFile.SOLICITACAO_EXP,
                    new ReportUtils.ParameterReport("solicitacao", String.valueOf(solicitacaoEscolhida.getId()))).view().printWithDialog();
        } catch (JRException | SQLException | IOException e) {
            e.printStackTrace();
        }
        MessageBox.create(message -> {
            message.message("Etiqueta impressa com sucesso");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }

}
