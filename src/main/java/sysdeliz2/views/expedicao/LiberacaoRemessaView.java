package sysdeliz2.views.expedicao;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import sysdeliz2.controllers.views.expedicao.LiberacaoRemessaController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.sysdeliz.*;
import sysdeliz2.models.sysdeliz.sistema.SdParametros;
import sysdeliz2.models.ti.*;
import sysdeliz2.models.view.expedicao.VSdReservasAlocadas;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.exceptions.BreakException;
import sysdeliz2.utils.gui.PDFView;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.mails.SimpleMail;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class LiberacaoRemessaView extends LiberacaoRemessaController {

    // <editor-fold defaultstate="collapsed" desc="bean">
    private final ListProperty<SdRemessaCliente> beanRemessas = new SimpleListProperty<>(FXCollections.observableArrayList());
    private List<SdStatusRemessa> statusLiberacao = (List<SdStatusRemessa>) new FluentDao().selectFrom(SdStatusRemessa.class).where(eb -> eb.equal("exibeLiberacao", true)).resultList();
    private final SdParametros _STATUS_IMPRESSO_NF = new FluentDao().selectFrom(SdParametros.class).where(eb -> eb.equal("codigo", 7)).singleResult();
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fields">
    private final FormFieldMultipleFind<Entidade> fieldFilterEntidade = FormFieldMultipleFind.create(Entidade.class, field -> {
        field.title("Cliente");
        field.width(200.0);
    });
    private final FormFieldSegmentedButton<String> fieldFilterStatus = FormFieldSegmentedButton.create(field -> {
        field.title("Status");

        List<FormFieldSegmentedButton.OptionButton> opcoes = new ArrayList<>();
        opcoes.add(field.option("Todos", "0", FormFieldSegmentedButton.Style.SECUNDARY));
        statusLiberacao.forEach(status -> opcoes.add(field.option(status.getDescricao(), status.getCodigo(), status.getCor())));
        field.options(opcoes);
        field.select(0);
    });
    private final FormFieldDatePeriod fieldFilterPeriodoEmissaoNf = FormFieldDatePeriod.create(field -> {
        field.title("Emissão NF");
        field.valueBegin.set(LocalDate.of(1900, 1, 1));
        field.valueEnd.set(LocalDate.of(2050, 12, 31));
    });
    private final FormFieldSegmentedButton<String> fieldFilterClientesBoletoImpresso = FormFieldSegmentedButton.create(field -> {
        field.title("Boleto Impresso");
        field.options(
                field.option("Ambos", "A", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("Sim", "S", FormFieldSegmentedButton.Style.SUCCESS),
                field.option("Não", "N", FormFieldSegmentedButton.Style.DANGER)
        );
        field.select(0);
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="buttons">
    FormButton btnLiberarRemessa = FormButton.create(btn -> {
        btn.title("Liberar Transmissão");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._32));
        btn.addStyle("lg").addStyle("success");
        btn.disable.bind(beanRemessas.emptyProperty());
        btn.setAction(evt -> enviarRemessasSelecionadas());
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="tables">
    private final FormTableView<SdRemessaCliente> tblRemessas = FormTableView.create(SdRemessaCliente.class, table -> {
        table.title("Remessas");
        table.expanded();
        table.editable.bind(beanRemessas.emptyProperty().not());
        table.items.bind(beanRemessas);
//        table.selectColumn();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Status");
                    cln.width(40.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdRemessaCliente, SdRemessaCliente>, ObservableValue<SdRemessaCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getStatus().getCodigo()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Status Reserva*/,
                FormTableColumn.create(cln -> {
                    cln.title("Remessa");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdRemessaCliente, SdRemessaCliente>, ObservableValue<SdRemessaCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getRemessa()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Remessa*/,
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(60.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdRemessaCliente, SdRemessaCliente>, ObservableValue<SdRemessaCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodcli().getCodcli()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Código*/,
                FormTableColumn.create(cln -> {
                    cln.title("Nome");
                    cln.width(320.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdRemessaCliente, SdRemessaCliente>, ObservableValue<SdRemessaCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodcli().getRazaosocial()));
                }).build() /*Nome*/,
                FormTableColumn.create(cln -> {
                    cln.title("Dt. Entrega");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdRemessaCliente, SdRemessaCliente>, ObservableValue<SdRemessaCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDtentrega()));
                    cln.format(param -> {
                        return new TableCell<VSdReservasAlocadas, LocalDate>() {
                            @Override
                            protected void updateItem(LocalDate item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDateFormat(item));
                                }
                            }
                        };
                    });
                    cln.alignment(Pos.CENTER);
                }).build() /*Dt. Entrega*/,
                FormTableColumn.create(cln -> {
                    cln.title("Depósito");
                    cln.width(60.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdRemessaCliente, SdRemessaCliente>, ObservableValue<SdRemessaCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDeposito()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Dt. Entrega*/,
                FormTableColumn.create(cln -> {
                    cln.title("Peças");
                    cln.width(60.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdRemessaCliente, SdRemessaCliente>, ObservableValue<SdRemessaCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Peças*/,
                FormTableColumn.create(cln -> {
                    cln.title("Valor");
                    cln.width(100.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdRemessaCliente, SdRemessaCliente>, ObservableValue<SdRemessaCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValor()));
                    cln.format(param -> {
                        return new TableCell<VSdReservasAlocadas, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toMonetaryFormat(item, 2));
                                }
                            }
                        };
                    });
                    cln.alignment(Pos.CENTER_RIGHT);
                }).build() /*Valor*/,
                FormTableColumn.create(cln -> {
                    cln.title("");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(40.0);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdRemessaCliente, SdRemessaCliente>, ObservableValue<SdRemessaCliente>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdRemessaCliente, SdRemessaCliente>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnCancelarExpedicao = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                btn.tooltip("Cancelar Expedição de Remessa");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("danger");
                            });

                            @Override
                            protected void updateItem(SdRemessaCliente item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {

                                    btnCancelarExpedicao.setOnAction(evt -> {
                                        cancelarExpedicaoRemessa(item);
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    if (item.getStatus().getCodigo().equals("E") && item.getPedidos().stream().allMatch(pedido -> pedido.getNota() == null || pedido.getNota().getNatureza() == null))
                                        boxButtonsRow.getChildren().addAll(btnCancelarExpedicao);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build()
        );
        table.factoryRow(param -> {
            return new TableRow<SdRemessaCliente>() {
                @Override
                protected void updateItem(SdRemessaCliente item, boolean empty) {
                    super.updateItem(item, empty);
                    clearStyle();

                    if (item != null && !empty) {
                        if (item.getStatus().getCor() != null)
                            getStyleClass().add("table-row-" + item.getStatus().getCor());
                    }
                }

                void clearStyle() {
                    getStyleClass().removeAll("table-row-danger", "table-row-success", "table-row-warning", "table-row-info", "table-row-primary", "table-row-secundary", "table-row-dark", "table-row-amber");
                }

            };
        });
        List<FormTableView<SdRemessaCliente>.ItemIndice> indices = new ArrayList<>();
        statusLiberacao.forEach(status -> {
            indices.add(table.indice(status.getCodigo(), status.getCor(), status.getDescricao()));
        });
        table.indices(indices);
        table.selectionModelItem((observable, oldValue, newValue) -> {
            if (newValue != null)
                selectRemessaTransmissao((SdRemessaCliente) newValue);
        });
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="box">
    private final FormBox boxNfs = FormBox.create(box -> {
        box.vertical();
        box.expanded();
        box.bothScroll();
    });
    // </editor-fold>

    public LiberacaoRemessaView() {
        super("Liberação de Remessas para Transmissão", ImageUtils.getImage(ImageUtils.Icon.BID));
        init();
        consultaRemessas(new Object[]{}, "E", LocalDate.of(1900, 1, 1), LocalDate.of(2050, 12, 31), "A");
    }

    private void init() {
        box.getChildren().add(FormBox.create(principal -> {
            principal.horizontal();
            principal.expanded();
            principal.add(FormBox.create(boxColL -> {
                boxColL.vertical();
                boxColL.add(FormBox.create(header -> {
                    header.horizontal();
                    header.add(FormTitledPane.create(filter -> {
                        filter.filter();
                        filter.add(FormBox.create(fields -> {
                            fields.vertical();
                            fields.add(FormBox.create(boxRow1 -> {
                                boxRow1.horizontal();
                                boxRow1.add(fieldFilterEntidade.build());
                                boxRow1.add(fieldFilterPeriodoEmissaoNf.build());
                            }));
                            fields.add(FormBox.create(boxRow1 -> {
                                boxRow1.horizontal();
                                boxRow1.add(fieldFilterStatus.build());
                                boxRow1.add(fieldFilterClientesBoletoImpresso.build());
                            }));
                        }));
                        filter.find.setOnAction(evt -> {
                            beanRemessas.clear();
                            consultaRemessas(fieldFilterEntidade.objectValues.stream().map(Entidade::getCodcli).toArray(),
                                    fieldFilterStatus.value.get(),
                                    fieldFilterPeriodoEmissaoNf.valueBegin.get(),
                                    fieldFilterPeriodoEmissaoNf.valueEnd.get(),
                                    fieldFilterClientesBoletoImpresso.value.get());
                        });
                        filter.clean.setOnAction(evt -> {
                            fieldFilterEntidade.clear();
                            fieldFilterStatus.select(0);
                            fieldFilterPeriodoEmissaoNf.valueBegin.set(LocalDate.of(1900, 1, 1));
                            fieldFilterPeriodoEmissaoNf.valueEnd.set(LocalDate.of(2050, 12, 31));
                            fieldFilterClientesBoletoImpresso.select(0);
                            beanRemessas.clear();
                        });
                    }));
                }));
                boxColL.add(FormBox.create(container -> {
                    container.vertical();
                    container.expanded();
                    container.add(tblRemessas.build());
                }));
                boxColL.add(FormBox.create(footer -> {
                    footer.horizontal();
//                footer.add(btnLiberarRemessa);
                }));
            }));
            principal.add(FormBox.create(boxColR -> {
                boxColR.vertical();
                boxColR.expanded();
                boxColR.add(boxNfs);
            }));
        }));
    }

    private void consultaRemessas(Object[] clientes, String status, LocalDate inicioEmissao, LocalDate fimEmissao, String boletoImpresso) {
        /**
         * task: getRemessas(clientes, status) * return: beanRemessas.set(FXCollections.observableList(remessas))
         */
        new RunAsyncWithOverlay(this).exec(task -> {
            JPAUtils.clearEntitys(beanRemessas);
            getRemessas(clientes, status, inicioEmissao, fimEmissao, boletoImpresso);
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                beanRemessas.set(FXCollections.observableList(remessas));
            }
        });
    }

    private void enviarRemessasSelecionadas() {

    }

    private void cancelarExpedicaoRemessa(SdRemessaCliente item) {
        try {
            deleteNotaRemessa(item);
            item.getPedidos().forEach(pedido -> pedido.setNota(null));
            item.setStatus(statusRemessaColetaFinalizada);
            new FluentDao().merge(item);
            JPAUtils.getEntityManager().refresh(item);
            tblRemessas.refresh();

            // add log da acao
            SysLogger.addSysDelizLog("Liberacao NF", TipoAcao.EDITAR, String.valueOf(item.getRemessa()), "Retornado remessa para fechamento de caixa e excluído NFs da remessa.");

            MessageBox.create(message -> {
                message.message("Remessa enviada para coleta finalizada.");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
        } catch (SQLException e) {
            e.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(e);
                message.showAndWait();
            });
        }
    }

    private Map<String, String> getDescStyleStatusNf(String status) {
        Map<String, String> mapDescStyleStatus = new HashMap<>();
        mapDescStyleStatus.put("descricao", status == null ? "NF não criada" :
                status.equals("S") ? "Transmitida" :
                        status.equals("N") ? "Não Transmitida" :
                                status.equals("C") ? "Cancelada" :
                                        status.equals("D") ? "Denegada" :
                                                "Aguardando Liberação");
        mapDescStyleStatus.put("style", status == null ? "dark" :
                status.equals("S") ? "success" :
                        status.equals("N") ? "primary" :
                                status.equals("C") ? "danger" :
                                        status.equals("D") ? "danger" :
                                                "warning");

        return mapDescStyleStatus;
    }

    private String getFile(String fatura) {
        String filePath = null;
        String basePath = "L:/Expedição/6.BOLETOS EXPEDIÇÃO/";
        File folder = new File(basePath);
        String[] files = folder.list();
        for (String fileName : files) {
            String fileReadPath = basePath + fileName;
            File file = new File(fileReadPath);
            if (!file.isDirectory()) {
                if (fileName.toUpperCase().endsWith(".PDF") && fileName.toUpperCase().startsWith(fatura)) {
                    filePath = file.getPath();
                    break;
                }
            }
        }

        return filePath;
    }

    private void selectRemessaTransmissao(SdRemessaCliente remessa) {
        boxNfs.clear();

        boxNfs.add(FormBox.create(box -> {
            box.horizontal();
            box.add(FormFieldTextArea.create(field -> {
                field.title("Observação da Remessa");
                field.editable(false);
                field.expanded();
                field.width(400.0);
                field.value.set(remessa.getObservacao());
            }).build());
        }));
        Map<Nota, List<SdPedidosRemessa>> notasRemessa = remessa.getPedidos().stream()
                .filter(pedido -> pedido.getItens().stream()
                        .filter(item -> item.getQtde() > 0 && !item.getTipo().equals("MKT"))
                        .mapToInt(SdItensPedidoRemessa::getQtde).sum() > 0 && pedido.getNota() != null)
                .collect(Collectors.groupingBy(pedido -> pedido.getNota() != null ? pedido.getNota() : new Nota()));
        for (Map.Entry<Nota, List<SdPedidosRemessa>> notaListEntry : notasRemessa.entrySet()) {
            Nota nfPedido = notaListEntry.getKey();
            List<SdPedidosRemessa> pedidosNf = notaListEntry.getValue();
            AtomicReference<String> sitDupPedido = new AtomicReference<>("0");
            pedidosNf.stream().filter(pedido -> pedido.getId().getNumero().getSitDup() != null && pedido.getId().getNumero().getSitDup().equals("26")).findFirst().ifPresent(pedido -> sitDupPedido.set(pedido.getId().getNumero().getSitDup()));
            pedidosNf.sort(Comparator.comparing(pedido -> pedido.getId().getNumero().getNumero()));
            List<SdCaixaRemessa> caixasNf = (List<SdCaixaRemessa>) new FluentDao().selectFrom(SdCaixaRemessa.class).where(eb -> eb.equal("nota", nfPedido.getFatura())).resultList();
            Nota nfDevolucao = new FluentDao().selectFrom(Nota.class).where(eb -> eb.equal("notadev", nfPedido.getFatura())).singleResult();
            AtomicReference<Map<String, String>> mapDescStyleStatus = new AtomicReference<>(getDescStyleStatusNf(nfPedido.getImpresso()));
            final StringProperty beanMensagemNf = new SimpleStringProperty(nfPedido.getMensagem());
            final FormFieldText fieldStatusNf = FormFieldText.create(field -> {
                field.title("Status");
                field.editable(false);
                field.width(200.0);
                field.addStyle(mapDescStyleStatus.get().get("style"));
                field.alignment(Pos.CENTER_LEFT);
                field.value.set(mapDescStyleStatus.get().get("descricao"));
            });
            final FormFieldText fieldValorNF = FormFieldText.create(field -> {
                field.title("Valor NF");
                field.editable(false);
                field.label("R$");
                field.width(150.0);
                field.alignment(Pos.CENTER_RIGHT);
                field.addStyle("lg").addStyle("info");
                field.value.set(StringUtils.toDecimalFormat(pedidosNf.stream()
                        .mapToDouble(pedido -> pedido.getItens().stream()
                                .filter(item -> item.getStatusitem().equals("C"))
                                .mapToDouble(item -> item.getValor().doubleValue())
                                .sum())
                        .sum(), 2));
            });
            parcelasFaturamento.clear();
            contabilFaturamento.clear();
            dupsRepFaturamento.clear();

            boxNfs.add(FormBox.create(boxPrincipal -> {
                boxPrincipal.vertical();
                boxPrincipal.add(FormBox.create(boxHeader -> {
                    boxHeader.horizontal();
                    boxHeader.alignment(Pos.BOTTOM_LEFT);
                    boxHeader.add(FormFieldText.create(field -> {
                        field.title("Nota Fiscal");
                        field.editable(false);
                        field.alignment(Pos.CENTER);
                        field.addStyle("lg").addStyle("amber");
                        field.value.set(nfPedido.getFatura());
                    }).build());
                    boxHeader.add(fieldStatusNf.build());
                    boxHeader.add(FormBox.create(boxValor -> {
                        boxValor.horizontal();
                        boxValor.expanded();
                        boxValor.alignment(Pos.CENTER_RIGHT);
                        boxValor.add(fieldValorNF.build());
                    }));
                }));
                boxPrincipal.add(FormBox.create(boxContainer -> {
                    boxContainer.vertical();
                    boxContainer.add(FormBox.create(boxRow -> {
                        boxRow.horizontal();
                        boxRow.add(FormBox.create(boxcol1 -> {
                            boxcol1.vertical();
                            boxcol1.expanded();
                            boxcol1.add(FormTableView.create(SdPedidosRemessa.class, table -> {
                                table.title("Pedidos NF");
                                table.expanded();
                                table.height(120.0);
                                table.items.set(FXCollections.observableList(pedidosNf));
                                table.columns(
                                        FormTableColumn.create(cln -> {
                                            cln.title("Pedido");
                                            cln.width(75.0);
                                            cln.value((Callback<TableColumn.CellDataFeatures<SdPedidosRemessa, SdPedidosRemessa>, ObservableValue<SdPedidosRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getNumero().getNumero()));
                                            cln.alignment(Pos.CENTER);
                                        }).build() /*Pedido*/,
                                        FormTableColumn.create(cln -> {
                                            cln.title("Dt. Base");
                                            cln.width(70.0);
                                            cln.value((Callback<TableColumn.CellDataFeatures<SdPedidosRemessa, SdPedidosRemessa>, ObservableValue<SdPedidosRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDtBase()));
                                            cln.format(param -> {
                                                return new TableCell<SdPedidosRemessa, LocalDate>() {
                                                    @Override
                                                    protected void updateItem(LocalDate item, boolean empty) {
                                                        super.updateItem(item, empty);
                                                        setText(null);
                                                        if (item != null && !empty) {
                                                            setText(StringUtils.toDateFormat(item));
                                                        }
                                                    }
                                                };
                                            });
                                        }).build() /*Dt. Base*/,
                                        FormTableColumn.create(cln -> {
                                            cln.title("Rep.");
                                            cln.width(190.0);
                                            cln.value((Callback<TableColumn.CellDataFeatures<SdPedidosRemessa, SdPedidosRemessa>, ObservableValue<SdPedidosRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getNumero().getCodrep().toString()));
                                        }).build() /*Rep.*/,
                                        FormTableColumn.create(cln -> {
                                            cln.title("% Desc.");
                                            cln.width(50.0);
                                            cln.value((Callback<TableColumn.CellDataFeatures<SdPedidosRemessa, SdPedidosRemessa>, ObservableValue<SdPedidosRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getNumero().getPercDesc()));
                                            cln.alignment(Pos.CENTER);
                                        }).build() /*Desc.*/,
                                        FormTableColumn.create(cln -> {
                                            cln.title("Cond.");
                                            cln.width(110.0);
                                            cln.value((Callback<TableColumn.CellDataFeatures<SdPedidosRemessa, SdPedidosRemessa>, ObservableValue<SdPedidosRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getNumero().getPagto().trim()));
                                        }).build() /*Cond.*/,
                                        FormTableColumn.create(cln -> {
                                            cln.title("Marca");
                                            cln.width(35.0);
                                            cln.value((Callback<TableColumn.CellDataFeatures<SdPedidosRemessa, SdPedidosRemessa>, ObservableValue<SdPedidosRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getMarca()));
                                            cln.alignment(Pos.CENTER);
                                        }).build() /*Marca*/,
                                        FormTableColumn.create(cln -> {
                                            cln.title("Transp.");
                                            cln.width(160.0);
                                            cln.value((Callback<TableColumn.CellDataFeatures<SdPedidosRemessa, SdPedidosRemessa>, ObservableValue<SdPedidosRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getNumero().getTabTrans().toString()));
                                        }).build() /*Transp.*/,
                                        FormTableColumn.create(cln -> {
                                            cln.title("Qtde");
                                            cln.width(50.0);
                                            cln.value((Callback<TableColumn.CellDataFeatures<SdPedidosRemessa, SdPedidosRemessa>, ObservableValue<SdPedidosRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                                            cln.alignment(Pos.CENTER);
                                        }).build() /*Qtde*/,
                                        FormTableColumn.create(cln -> {
                                            cln.title("Valor");
                                            cln.width(70.0);
                                            cln.value((Callback<TableColumn.CellDataFeatures<SdPedidosRemessa, SdPedidosRemessa>, ObservableValue<SdPedidosRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValor()));
                                            cln.alignment(Pos.CENTER_RIGHT);
                                            cln.format(param -> {
                                                return new TableCell<SdPedidosRemessa, BigDecimal>() {
                                                    @Override
                                                    protected void updateItem(BigDecimal item, boolean empty) {
                                                        super.updateItem(item, empty);
                                                        setText(null);
                                                        if (item != null && !empty) {
                                                            setText(StringUtils.toMonetaryFormat(item, 2));
                                                        }
                                                    }
                                                };
                                            });
                                        }).build() /*Valor*/
                                );
                            }).build());
                            boxcol1.add(FormBox.create(boxAcoes -> {
                                boxAcoes.horizontal();
                                boxAcoes.alignment(Pos.TOP_LEFT);
                                boxAcoes.add(FormButton.create(btnLiberarTransmissao -> {
                                    btnLiberarTransmissao.title("Liberar Transmissão");
                                    btnLiberarTransmissao.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._32));
                                    btnLiberarTransmissao.addStyle("lg").addStyle("success");
                                    btnLiberarTransmissao.disable.bind(nfPedido.naturezaProperty().isNotNull().and(nfPedido.impressoProperty().isNotEqualTo("X")).or(nfPedido.faturaProperty().isNull()));
                                    btnLiberarTransmissao.setAction(evt -> {
                                        pedidosNf.forEach(pedido -> JPAUtils.getEntityManager().refresh(pedido.getId().getNumero()));
                                        if (pedidosNf.stream().anyMatch(pedido -> pedido.getId().getNumero().getFinanceiro().equals("0") || pedido.getId().getNumero().getBloqueio().equals("0"))) {
                                            MessageBox.create(message -> {
                                                message.message("Cliente com bloqueio financeiro/comercial.");
                                                message.type(MessageBox.TypeMessageBox.ALERT);
                                                message.showAndWait();
                                            });
                                            return;
                                        }

                                        parcelasFaturamento.clear();
                                        contabilFaturamento.clear();
                                        dupsRepFaturamento.clear();
                                        new Fragment().show(fragment -> {
                                            fragment.title("Liberação da NF: " + nfPedido.getFatura());
                                            fragment.size(500.0, 500.0);

                                            try {
                                                // completando dados do cabeçalho da NF
                                                Nota notaAtualizada = atualizaNotaFiscal(nfPedido, pedidosNf, caixasNf, remessa);
                                                final FormFieldText fieldNaturezaNf = FormFieldText.create(field -> {
                                                    field.title("Natureza");
                                                    field.editable(false);
                                                    field.width(70.0);
                                                    field.value.set(notaAtualizada.getNatureza().getNatureza());
                                                    field.alignment(Pos.CENTER);
                                                });
                                                final FormFieldText fieldUfEntrega = FormFieldText.create(field -> {
                                                    field.title("UF");
                                                    field.editable(false);
                                                    field.width(50.0);
                                                    field.value.set(remessa.getCodcli().getCepentrega().getCidade().getCodEst().getId().getSiglaEst());
                                                    field.alignment(Pos.CENTER);
                                                });
                                                final FormFieldText fieldSuframaCliente = FormFieldText.create(field -> {
                                                    field.title("Suframa");
                                                    field.editable(false);
                                                    field.width(80.0);
                                                    field.value.set(remessa.getCodcli().getSuframa());
                                                    field.alignment(Pos.CENTER);
                                                });
                                                final FormFieldText fieldInscricaoCliente = FormFieldText.create(field -> {
                                                    field.title("Inscrição");
                                                    field.editable(false);
                                                    field.width(90.0);
                                                    field.value.set(remessa.getCodcli().getInscricao());
                                                    field.alignment(Pos.CENTER);
                                                });
                                                final FormFieldText fieldTipoPedido = FormFieldText.create(field -> {
                                                    field.title("Tipo Ped.");
                                                    field.editable(false);
                                                    field.width(70.0);
                                                    field.value.set(pedidosNf.stream().anyMatch(pedido -> pedido.getId().getNumero().getPeriodo().getTipoFaturamento().equals("B")) ? "Bonificado" : "Venda");
                                                    field.alignment(Pos.CENTER);
                                                });
                                                beanMensagemNf.set(notaAtualizada.getMensagem());

                                                // transportadora
                                                AtomicReference<TabTran> transportadora = new AtomicReference<>();
                                                pedidosNf.stream().findFirst().ifPresent(pedido -> transportadora.set(pedido.getId().getNumero().getTabTrans()));
                                                final FormFieldSingleFind<TabTran> fieldTransportadora = FormFieldSingleFind.create(TabTran.class, field -> {
                                                    field.title("Transportadora");
                                                    field.setDefaultCode(transportadora.get().getCodigo());
                                                });

                                                // condição e parcelamento
                                                String condicaoPedido = pedidosNf.get(0).getId().getNumero().getPagto();
                                                String condicaoFaturamento = getCondicaoFaturamento(remessa, pedidosNf, notaAtualizada);
                                                final FormFieldText fieldValorTotalNF = FormFieldText.create(field -> {
                                                    field.title("Total NF");
                                                    field.editable(false);
                                                    field.alignment(Pos.CENTER_RIGHT);
                                                    field.value.set(StringUtils.toMonetaryFormat(notaAtualizada.getValor(), 2));
                                                });
                                                final FormFieldText fieldDataBase = FormFieldText.create(field -> {
                                                    field.title("Data Base");
                                                    field.editable(false);
                                                    field.alignment(Pos.CENTER);
                                                });
                                                final FormFieldText fieldCondicaoOriginal = FormFieldText.create(field -> {
                                                    field.title("Condição Pedido");
                                                    field.editable(false);
                                                    field.value.set(condicaoPedido.trim());
                                                });
                                                final FormFieldText fieldCondicaoFaturamento = FormFieldText.create(field -> {
                                                    field.title("Condição Faturamento");
                                                    field.editable(false);
                                                    field.value.set(condicaoFaturamento);
                                                });
                                                final FormTableView<Receber> tblParcelas = FormTableView.create(Receber.class, table -> {
                                                    table.title("Parcelamento");
                                                    table.expanded();
                                                    table.items.set(FXCollections.observableList(parcelasFaturamento));
                                                    table.columns(
                                                            FormTableColumn.create(cln -> {
                                                                cln.title("Duplicata");
                                                                cln.width(80.0);
                                                                cln.value((Callback<TableColumn.CellDataFeatures<Receber, Receber>, ObservableValue<Receber>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumero()));
                                                                cln.alignment(Pos.CENTER);
                                                            }).build() /*Duplicata*/,
                                                            FormTableColumn.create(cln -> {
                                                                cln.title("Vencimento");
                                                                cln.width(90.0);
                                                                cln.value((Callback<TableColumn.CellDataFeatures<Receber, Receber>, ObservableValue<Receber>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDtvencto()));
                                                                cln.alignment(Pos.CENTER);
                                                                cln.format(param -> {
                                                                    return new TableCell<Receber, LocalDate>() {
                                                                        @Override
                                                                        protected void updateItem(LocalDate item, boolean empty) {
                                                                            super.updateItem(item, empty);
                                                                            setText(null);
                                                                            if (item != null && !empty) {
                                                                                setText(StringUtils.toDateFormat(item));
                                                                            }
                                                                        }
                                                                    };
                                                                });
                                                            }).build() /*Data*/,
                                                            FormTableColumn.create(cln -> {
                                                                cln.title("Valor");
                                                                cln.width(90.0);
                                                                cln.value((Callback<TableColumn.CellDataFeatures<Receber, Receber>, ObservableValue<Receber>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValor2()));
                                                                cln.alignment(Pos.CENTER_RIGHT);
                                                                cln.format(param -> {
                                                                    return new TableCell<Receber, BigDecimal>() {
                                                                        @Override
                                                                        protected void updateItem(BigDecimal item, boolean empty) {
                                                                            super.updateItem(item, empty);
                                                                            setText(null);
                                                                            if (item != null && !empty) {
                                                                                setText(StringUtils.toMonetaryFormat(item, 2));
                                                                            }
                                                                        }
                                                                    };
                                                                });
                                                            }).build() /*Valor*/
                                                    );
                                                });
                                                final FormFieldSingleFind<Condicao> fieldSelecionarCondicao = FormFieldSingleFind.create(Condicao.class, field -> {
                                                    field.title("Condição Faturamento");
                                                    field.postSelected((observable, oldValue, newValue) -> {
                                                        if (getParcelamento((Condicao) field.value.get(), remessa, pedidosNf, notaAtualizada)) {
                                                            if (parcelasFaturamento.size() > 0) {
                                                                tblParcelas.setItems(FXCollections.observableList(parcelasFaturamento));
                                                            } else {
                                                                field.clear();
                                                            }
                                                        } else {
                                                            field.clear();
                                                        }
                                                    });
                                                });

                                                fragment.box.getChildren().add(FormBox.create(boxDadosFaturamento -> {
                                                    boxDadosFaturamento.vertical();
                                                    boxDadosFaturamento.add(FormBox.create(boxDadosNf -> {
                                                        boxDadosNf.horizontal();
                                                        boxDadosNf.add(fieldUfEntrega.build());
                                                        boxDadosNf.add(fieldNaturezaNf.build());
                                                        boxDadosNf.add(fieldTipoPedido.build());
                                                        boxDadosNf.add(fieldInscricaoCliente.build());
                                                        boxDadosNf.add(fieldSuframaCliente.build());
                                                    }));
                                                    boxDadosFaturamento.add(fieldTransportadora.build());
                                                    boxDadosFaturamento.add(FormBox.create(boxPagto -> {
                                                        boxPagto.vertical();
                                                        boxPagto.title("Pagamento");
                                                        boxPagto.add(FormBox.create(boxValorNF -> {
                                                            boxValorNF.horizontal();
                                                            boxValorNF.add(fieldValorTotalNF.build());
                                                            if (pedidosNf.stream().findFirst().get().getDtBase() != null) {
                                                                fieldDataBase.value.set(StringUtils.toDateFormat(pedidosNf.stream().findFirst().get().getDtBase()));
                                                                boxValorNF.add(fieldDataBase.build());
                                                            }
                                                        }));
                                                        boxPagto.add(FormBox.create(boxCondPagto -> {
                                                            boxCondPagto.horizontal();
                                                            boxCondPagto.add(FormBox.create(boxDadosPagto -> {
                                                                boxDadosPagto.vertical();
                                                                boxDadosPagto.add(fieldCondicaoOriginal.build());
                                                                boxDadosPagto.add(condicaoFaturamento != null ? fieldCondicaoFaturamento.build() : fieldSelecionarCondicao.build());
                                                            }));
                                                            boxCondPagto.add(tblParcelas.build());
                                                        }));
                                                    }));
                                                }));
                                                fragment.buttonsBox.getChildren().add(FormButton.create(btn -> {
                                                    btn.title("Enviar");
                                                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._24));
                                                    btn.addStyle("success");
                                                    btn.setAction(evtEnviar -> {
                                                        if (parcelasFaturamento.size() == 0 && notaAtualizada.getNatureza().isDupli()) {
                                                            MessageBox.create(message -> {
                                                                message.message("Você precisa definir as duplicatas para o faturamento.");
                                                                message.type(MessageBox.TypeMessageBox.IMPORTANT);
                                                                message.showAndWait();
                                                            });
                                                            return;
                                                        }

                                                        try {
                                                            // persistindo a NF
                                                            new FluentDao().merge(notaAtualizada);
                                                            // persistindo as duplicatas
                                                            new FluentDao().persistAll(parcelasFaturamento);
                                                            new FluentDao().persistAll(dupsRepFaturamento);
                                                            // persistindo o contábil
                                                            new FluentDao().persistAll(contabilFaturamento.stream()
                                                                    .filter(contabil -> contabil.getValor() != null && contabil.getValor().compareTo(BigDecimal.ZERO) > 0)
                                                                    .collect(Collectors.toList()));
                                                            // atualizando os dados do TI (PEDIDO3, PED_ITEN e PED_RESERVA)
                                                            new NativeDAO().runNativeQueryProcedure(String.format("p_sd_atualiza_faturamento('%s')", nfPedido.getFatura()));

                                                            // liberando NF para transmissão e impressão
                                                            _STATUS_IMPRESSO_NF.refresh();
                                                            notaAtualizada.setTransport(fieldTransportadora.value.get().getCodigo());
                                                            notaAtualizada.setImpresso(_STATUS_IMPRESSO_NF.getValor());
                                                            new FluentDao().merge(notaAtualizada);

                                                            // verificação de nfs pendentes na remessa para atualizar o status da remessa
                                                            if (remessa.getPedidos().stream().filter(pedido -> pedido.getQtde() > 0).noneMatch(pedido -> pedido.getNota().getImpresso().equals("X"))) {
                                                                remessa.setStatus(statusRemessaFaturada);
                                                                remessa.setDtFatura(LocalDate.now());
                                                                new FluentDao().merge(remessa);
                                                            }

                                                            // log da ação
                                                            SysLogger.addSysDelizLog("Liberacao NF", TipoAcao.ENVIAR, notaAtualizada.getFatura(), "Liberado NF para transmissão ao SEFAZ.");

                                                            // atualizações de tela
                                                            fragment.close();
                                                            MessageBox.create(message -> {
                                                                message.message("NF " + notaAtualizada.getFatura() + " enviada para transmissão com sucesso.");
                                                                message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                                message.position(Pos.TOP_RIGHT);
                                                                message.notification();
                                                            });
                                                            mapDescStyleStatus.set(getDescStyleStatusNf(notaAtualizada.getImpresso()));
                                                            fieldStatusNf.clearDefaultStyle();
                                                            fieldStatusNf.addStyle(mapDescStyleStatus.get().get("style"));
                                                            fieldStatusNf.value.set(mapDescStyleStatus.get().get("descricao"));
                                                            tblRemessas.refresh();
                                                        } catch (SQLException e) {
                                                            e.printStackTrace();
                                                            StringWriter sw = new StringWriter();
                                                            PrintWriter pw = new PrintWriter(sw);
                                                            e.printStackTrace(pw);
                                                            String exceptionText = sw.toString();
                                                            SimpleMail.INSTANCE.addDestinatario("diego@deliz.com.br")
                                                                    .comAssunto("Exception report SysDeliz2 [LIBERAÇÃO DE REMESSA]")
                                                                    .comCorpo(e.getLocalizedMessage() + "\n\n" + exceptionText)
                                                                    .send();
                                                            ExceptionBox.build(message -> {
                                                                message.exception(e);
                                                                message.showAndWait();
                                                            });
                                                        }

                                                    });
                                                }));
                                            } catch (BreakException e) {
                                                e.printStackTrace();
                                                MessageBox.create(message -> {
                                                    message.message(e.getMessage());
                                                    message.type(MessageBox.TypeMessageBox.ERROR);
                                                    message.showAndWait();
                                                });
                                                fragment.close();
                                            }  catch (Exception e) {
                                                e.printStackTrace();
                                                ExceptionBox.build(message -> {
                                                    message.exception(e);
                                                    message.showAndWait();
                                                });
                                                fragment.close();
                                            }
                                        });
                                    });
                                }));
                                if (nfDevolucao != null && caixasNf.stream().mapToLong(caixa -> caixa.getItens().stream().allMatch(item -> item.getStatus().equals("D")) ? 0 : 1).sum() > 0)
                                    boxAcoes.add(FormButton.create(btnDevolverNf -> {
                                        btnDevolverNf.title("Devolver NF");
                                        btnDevolverNf.icon(ImageUtils.getIcon(ImageUtils.Icon.CAIXA_ABERTA, ImageUtils.IconSize._24));
                                        btnDevolverNf.addStyle("danger");
                                        btnDevolverNf.setAction(evt -> {
                                            devolverNf(caixasNf);
                                            MessageBox.create(message -> {
                                                message.message("Devolvido itens (barra) da NF.");
                                                message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                message.position(Pos.TOP_RIGHT);
                                                message.notification();
                                            });
                                        });
                                    }));
                                if (nfPedido.getSdNota() != null && sitDupPedido.get().equals("26") && !nfPedido.getSdNota().isImpressoBoleto())
                                    boxAcoes.add(FormButton.create(btnImprimirBoleto -> {
                                        btnImprimirBoleto.title("Imprimir Boletos");
                                        btnImprimirBoleto.icon(ImageUtils.getIcon(ImageUtils.Icon.BOLETO, ImageUtils.IconSize._24));
                                        btnImprimirBoleto.addStyle("info");
                                        btnImprimirBoleto.setAction(evt -> {
                                            String pathBoleto = getFile(nfPedido.getFatura());
                                            if (pathBoleto != null) {
//                                            PrinterJob job = PrinterJob.getPrinterJob();
//                                            if (job.printDialog()) {
                                                try {
                                                    File file = new File(pathBoleto);
//                                                    FileInputStream fis = new FileInputStream(pathBoleto);
//                                                    //DocFlavor flavor = DocFlavor.INPUT_STREAM.PDF;
//                                                    DocFlavor flavor = DocFlavor.BYTE_ARRAY.PDF;
//                                                    //DocFlavor flavor = DocFlavor.SERVICE_FORMATTED.PRINTABLE;
//                                                    Doc pdfDoc = new SimpleDoc(fis, flavor, null);
//                                                    DocPrintJob printJob = job.getPrintService().createPrintJob();
//                                                    PrintJobWatcher pjw = new PrintJobWatcher(printJob);
//                                                    printJob.print(pdfDoc, null);
//                                                    pjw.waitForDone();
//                                                    fis.close();

                                                    Node viewPdf = PDFView.showPDF(file);
                                                    new Fragment().show(fragment -> {
                                                        fragment.title("Boletos fatura: " + nfPedido.getFatura());
                                                        fragment.size(950.0, 600.0);
                                                        fragment.box.getChildren().add(FormBox.create(boxPrinter -> {
                                                            boxPrinter.vertical();
                                                            boxPrinter.expanded();
                                                            boxPrinter.bothScroll();
                                                            boxPrinter.add(viewPdf);
                                                        }));
                                                    });

                                                    nfPedido.getSdNota().setImpressoBoleto(true);
                                                    new FluentDao().merge(nfPedido.getSdNota());

                                                    String pathTarget = "L:\\Expedição\\6.BOLETOS EXPEDIÇÃO\\"
                                                            + nfPedido.getDtemissao().getYear() + "\\"
                                                            + StringUtils.lpad(nfPedido.getDtemissao().getMonthValue(), 2, "0") + "\\"
                                                            + StringUtils.lpad(nfPedido.getDtemissao().getDayOfMonth(), 2, "0") + "\\";
                                                    if (!Files.exists(Paths.get(pathTarget)))
                                                        Files.createDirectories(Paths.get(pathTarget));
                                                    Files.move(file.toPath(), new File(pathTarget + file.getName()).toPath(), StandardCopyOption.REPLACE_EXISTING);

                                                    // log acao
                                                    SysLogger.addSysDelizLog("Liberacao NF", TipoAcao.IMPRIMIR, nfPedido.getFatura(), "Impresso boletos da fatura. Movimento boletos para " + pathTarget + " arquivo: " + file.getName());

                                                    MessageBox.create(message -> {
                                                        message.message("Boleto enviado para impressão e movido para pasta de impressos.");
                                                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                        message.position(Pos.TOP_RIGHT);
                                                        message.notification();
                                                    });
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                    ExceptionBox.build(message -> {
                                                        message.exception(e);
                                                        message.showAndWait();
                                                    });
                                                }
//                                            }
                                            } else {
                                                MessageBox.create(message -> {
                                                    message.message("Boletos não encontrados para impressão, verifique com o financeiro");
                                                    message.type(MessageBox.TypeMessageBox.ALERT);
                                                    message.showAndWait();
                                                });
                                            }
                                        });
                                    }));
                                if (nfPedido.getImpresso() != null && nfPedido.getImpresso().equals("C"))
                                    boxAcoes.add(FormButton.create(btnRecriarNF -> {
                                        btnRecriarNF.title("Recriar Itens NF");
                                        btnRecriarNF.icon(ImageUtils.getIcon(ImageUtils.Icon.BID, ImageUtils.IconSize._24));
                                        btnRecriarNF.addStyle("warning");
                                        btnRecriarNF.setAction(evt -> {
                                            geradorDeItens(nfPedido, pedidosNf, caixasNf, remessa);
                                        });
                                    }));
                            }));
                        }));
                        boxRow.add(FormTableView.create(SdCaixaRemessa.class, table -> {
                            table.title("Pedidos NF");
                            table.height(150.0);
                            table.width(200.0);
                            table.items.set(FXCollections.observableList(caixasNf));
                            table.columns(
                                    FormTableColumn.create(cln -> {
                                        cln.title("Numero");
                                        cln.width(55.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<SdCaixaRemessa, SdCaixaRemessa>, ObservableValue<SdCaixaRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumero()));
                                        cln.alignment(Pos.CENTER);
                                    }).build() /*Numero*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Vol.");
                                        cln.width(25.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<SdCaixaRemessa, SdCaixaRemessa>, ObservableValue<SdCaixaRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getVolume()));
                                        cln.alignment(Pos.CENTER);
                                    }).build() /*Volume*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Qtde");
                                        cln.width(50.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<SdCaixaRemessa, SdCaixaRemessa>, ObservableValue<SdCaixaRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                                        cln.alignment(Pos.CENTER);
                                    }).build() /*Qtde*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Peso");
                                        cln.width(55.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<SdCaixaRemessa, SdCaixaRemessa>, ObservableValue<SdCaixaRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPeso()));
                                        cln.alignment(Pos.CENTER_RIGHT);
                                        cln.format(param -> {
                                            return new TableCell<SdPedidosRemessa, BigDecimal>() {
                                                @Override
                                                protected void updateItem(BigDecimal item, boolean empty) {
                                                    super.updateItem(item, empty);
                                                    setText(null);
                                                    if (item != null && !empty) {
                                                        setText(StringUtils.toDecimalFormat(item) + " Kg");
                                                    }
                                                }
                                            };
                                        });
                                    }).build() /*Peso*/
                            );
                        }).build());
                    }));
                }));
                boxPrincipal.add(new Separator(Orientation.HORIZONTAL));
            }));
        }
    }
}
