package sysdeliz2.views.expedicao;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Callback;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.jetbrains.annotations.Nullable;
import sysdeliz2.controllers.views.expedicao.ManutencaoLocaisController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.dao.fluentDao.Where;
import sysdeliz2.models.sysdeliz.SdLocalExpedicao;
import sysdeliz2.models.sysdeliz.SdRuaExpedicao;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Linha;
import sysdeliz2.models.ti.PaIten;
import sysdeliz2.models.view.VSdDadosProduto;
import sysdeliz2.models.view.VSdDadosProdutoCorTam;
import sysdeliz2.utils.ParametroConsulta;
import sysdeliz2.utils.SortUtils;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.sys.ImageUtils;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class ManutencaoLocaisView extends ManutencaoLocaisController {

    private SdRuaExpedicao ruaSelecionada = null;
    private ListProperty<VSdDadosProduto> produtos = new SimpleListProperty<>(FXCollections.observableArrayList(new ArrayList<>()));
    private ListProperty<SdLocalExpedicao> locaisBean = new SimpleListProperty<>(FXCollections.observableArrayList(new ArrayList<>()));
    private List<VSdDadosProdutoCorTam> produtosCorTam = new ArrayList<>();
    private List<VSdDadosProdutoCorTam> produtosCorTamAlocados = new ArrayList<>();
    private List<SdRuaExpedicao> ruas = (List<SdRuaExpedicao>) new FluentDao().selectFrom(SdRuaExpedicao.class).get().orderBy("indice", OrderType.ASC).resultList();
    private List<FormControlButton> btnsRuas = new ArrayList<>();
    private BidiMap<String, String> fieldsToOrder = new DualHashBidiMap<>();

    {
        fieldsToOrder.put("codigo", "Código");
        fieldsToOrder.put("codmarca", "Marca");
        fieldsToOrder.put("codcol", "Coleção");
        fieldsToOrder.put("linha", "Linha");
        fieldsToOrder.put("codigo.dtEntrega", "Entrega");

    }


    private ListProperty<ParametroConsulta> parametrosBean = new SimpleListProperty<>(FXCollections.observableArrayList());

    private FormBox boxParametros = FormBox.create(box -> {
        box.vertical();
        box.title("Parâmetros");
        box.width(350);
    });

    private Comparator<SdLocalExpedicao> comparatorSdLocal = new Comparator<SdLocalExpedicao>() {
        @Override
        public int compare(SdLocalExpedicao o1, SdLocalExpedicao o2) {
            return Comparator
                    .comparing(it -> ((SdLocalExpedicao) it).getRua().getRua())
                    .thenComparing(it -> ((SdLocalExpedicao) it).getLado())
                    .thenComparing(it -> ((SdLocalExpedicao) it).getPosicao())
                    .thenComparing(it -> ((SdLocalExpedicao) it).getAndar(), Comparator.reverseOrder())
                    .compare(o1, o2);
        }
    };

    private Comparator<Object> comparator = Comparator
            .comparing(it -> ((VSdDadosProdutoCorTam) it).getCodigo())
            .thenComparing(it -> ((VSdDadosProdutoCorTam) it).getCor())
            .thenComparing((o11, o21) -> SortUtils.sortTamanhos(((VSdDadosProdutoCorTam) o11).getTam(), ((VSdDadosProdutoCorTam) o21).getTam()));


    // <editor-fold defaultstate="collapsed" desc="Components">
    private final FormFieldToggleSingle toggleIncluirContinuas = FormFieldToggleSingle.create(field -> {
        field.title("Incluir Contínuas");
        field.value.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                consultarProdutos();
            }
        });
    });

    private final FormFieldSingleFind<Colecao> singleFindColecao = FormFieldSingleFind.create(Colecao.class, field -> {
        field.title("Coleção");
        field.dividedWidth(400);
        field.value.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                consultarProdutos();
            }
        });
    });

    private final FormFieldMultipleFind<Colecao> multiColecaoManterLocal = FormFieldMultipleFind.create(Colecao.class, field -> {
        field.title("Coleções Locais Fixos");
        field.toUpper();
    });

    private final FormFieldComboBox<String> comboBoxAndaresVazios = FormFieldComboBox.create(String.class, field -> {
        field.title("Locais Vazios");
        field.items(FXCollections.observableArrayList(Arrays.asList("0", "1", "2", "3")));
//        field.width(130);
        field.select(0);
        field.promptText("Selecione...");
    });

    private final FormFieldComboBox<String> comboBoxPiso = FormFieldComboBox.create(String.class, field -> {
        field.title("Piso");
        field.items(FXCollections.observableArrayList(Arrays.asList("0", "1")));
        field.select(0);
        field.width(75);
        field.promptText("Selecione...");
    });

    private final FormFieldComboBox<String> comboBoxRuaInicio = FormFieldComboBox.create(String.class, field -> {
        field.title("Rua Inicio");
        field.items(FXCollections.observableArrayList(ruas.stream().map(it -> it.getRua()).distinct().sorted().collect(Collectors.toList())));
        field.select(0);
        field.width(75);
    });

    private final FormFieldComboBox<String> comboBoxDirecao = FormFieldComboBox.create(String.class, field -> {
        field.title("Direção Rua");
        field.items(FXCollections.observableArrayList(Arrays.asList("Normal", "Invertida")));
        field.select(0);
        field.width(125);
    });

    private final FormListView<Linha> listLinhasOrdenacao = FormListView.create(field -> {
        field.title("Linhas");
        field.width(320);
        field.listProperties().setCellFactory(new Callback<ListView<Linha>, ListCell<Linha>>() {
            @Override
            public ListCell<Linha> call(ListView<Linha> param) {
                return new ListCell<Linha>() {
                    @Override
                    protected void updateItem(Linha item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(null);
                        setGraphic(null);
                        if (item != null && !empty) {
                            setText("[" + item.getSdLinha() + "] " + item.getSdLinhaDesc());
//                            setWidth(300);
                            setGraphic(FormBox.create(box -> {
                                box.vertical();
                                box.add(FormButton.create(btn -> {
                                    btn.addStyle("secundary");
                                    btn.addStyle("es");
                                    btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CIMA, 12.0));
                                    btn.setOnAction(evt -> {
                                        int index = field.items.indexOf(item);
                                        field.items.remove(item);
                                        field.items.add(index - 1, item);
                                        param.refresh();
                                    });
                                    btn.disable.set(field.items.indexOf(item) == 0);
                                }));
                                box.add(FormButton.create(btn -> {
                                    btn.addStyle("secundary");
                                    btn.addStyle("es");
                                    btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.BAIXO, 12.0));
                                    btn.setOnAction(evt -> {
                                        int index = field.items.indexOf(item);
                                        field.items.remove(item);
                                        field.items.add(index + 1, item);
                                        param.refresh();
                                    });
                                    btn.disable.set(field.items.indexOf(item) == field.items.size() - 1);
                                }));
                            }));
                        }
                    }

                    ;
                };
            }
        });

        produtos.sizeProperty().addListener((observable, oldValue, newValue) -> {
            field.items.clear();
            if (newValue.intValue() != 0) {
                List<Linha> listaLinhas = produtos.stream().map(it -> it.getCodlin()).filter(it -> it != null).distinct().map(linha -> ((Linha) new FluentDao().selectFrom(Linha.class).where(it -> it.equal("codigo", linha)).singleResult())).collect(Collectors.toList());
                for (int i = 0; i < listaLinhas.size(); i++) {
                    listaLinhas.get(i).setIndice(i);
                }
                field.items.addAll(listaLinhas);
            }
        });
    });

    private FormBox boxRuasSelecionadas = FormBox.create(box -> {
        box.flutuante();
        box.title("Ruas Selecionadas");
        box.border();

        comboBoxPiso.getSelectionModel((observable, oldValue, newValue) -> {
            box.clear();
            btnsRuas.clear();
            for (SdRuaExpedicao rua : ruas.stream().filter(it -> it.getPiso().equals(newValue)).collect(Collectors.toList())) {
                desenhaBotaoRua(box, rua);
            }
        });

        for (SdRuaExpedicao rua : ruas.stream().filter(it -> it.getPiso().equals(comboBoxPiso.value.getValue())).collect(Collectors.toList())) {
            desenhaBotaoRua(box, rua);
        }

    });

    private final FormTableView<VSdDadosProduto> tblProdutos = FormTableView.create(VSdDadosProduto.class, table -> {
        table.title("Produtos");
        table.items.bind(produtos);
        table.expanded();
        table.editable.set(true);
        table.selectColumn();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(70);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosProduto, VSdDadosProduto>, ObservableValue<VSdDadosProduto>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo()));
                }).build(),
//                FormTableColumn.create(cln -> {
//                    cln.title("Descrição");
//                    cln.width(250);
//                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosProduto, VSdDadosProduto>, ObservableValue<VSdDadosProduto>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDescricao()));
//                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Linha");
                    cln.width(180);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosProduto, VSdDadosProduto>, ObservableValue<VSdDadosProduto>>) param -> new ReadOnlyObjectWrapper("[" + param.getValue().getCodlin() + "] " + param.getValue().getLinha()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Coleção");
                    cln.width(200);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosProduto, VSdDadosProduto>, ObservableValue<VSdDadosProduto>>) param -> new ReadOnlyObjectWrapper("[" + param.getValue().getCodCol() + "] " + param.getValue().getColecao()));
                }).build()
        );
    });

    private final FormTableView<SdLocalExpedicao> tblLocais = FormTableView.create(SdLocalExpedicao.class, table -> {
        table.items.bind(locaisBean);
        table.title("Locais");
        table.expanded();

        table.factoryRow(param -> new TableRow<SdLocalExpedicao>() {
            protected void updateItem(SdLocalExpedicao item, boolean empty) {
                super.updateItem(item, empty);
                clear();
                if (item != null && !empty && item.getProduto() != null) {
                    if (produtosCorTamAlocados.stream().anyMatch(it -> it.getCodigo().equals(item.getProduto().getCodigo()) && it.getCor().equals(item.getCor()) && it.getTam().equals(item.getTam())))
                        getStyleClass().add("table-row-warning");
                }
            }

            protected void clear() {
                getStyleClass().removeAll("table-row-warning");
            }
        });
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Local");
                    cln.width(90);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdLocalExpedicao, SdLocalExpedicao>, ObservableValue<SdLocalExpedicao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLocal()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdLocalExpedicao, SdLocalExpedicao>, ObservableValue<SdLocalExpedicao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getProduto() == null ? "" : param.getValue().getProduto().getCodigo()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Cor");
                    cln.width(60);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdLocalExpedicao, SdLocalExpedicao>, ObservableValue<SdLocalExpedicao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCor()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Tam");
                    cln.alignment(Pos.CENTER);
                    cln.width(60);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdLocalExpedicao, SdLocalExpedicao>, ObservableValue<SdLocalExpedicao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTam()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Linha");
                    cln.width(180);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdLocalExpedicao, SdLocalExpedicao>, ObservableValue<SdLocalExpedicao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getProduto() == null ? "" : "[" + param.getValue().getProduto().getCodlin() + "] " + param.getValue().getProduto().getLinha()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Coleção");
                    cln.width(180);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdLocalExpedicao, SdLocalExpedicao>, ObservableValue<SdLocalExpedicao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getProduto() == null ? "" : "[" + param.getValue().getProduto().getCodCol() + "] " + param.getValue().getProduto().getColecao()));
                }).build()
        );
    });

    private final FormFieldText textTodosProdutos = FormFieldText.create(field -> {
        field.title("Todos Produtos");
        field.editable.set(false);
        field.value.setValue("0");
    });

    private final FormFieldText textProdutosAlocados = FormFieldText.create(field -> {
        field.title("Produtos Alocados");
        field.editable.set(false);
        field.value.setValue("0");
    });

    private final FormFieldText textProdutosPendentes = FormFieldText.create(field -> {
        field.title("Produtos Pendentes");
        field.editable.set(false);
        field.value.setValue("0");
    });

    private final FormFieldText textTodosLocais = FormFieldText.create(field -> {
        field.title("Todos Locais");
        field.editable.set(false);
        field.value.setValue("0");
    });

    private final FormFieldText textLocaisOcupados = FormFieldText.create(field -> {
        field.title("Locais Ocupados");
        field.editable.set(false);
        field.value.setValue("0");
    });

    private final FormFieldText textLocaisLivres = FormFieldText.create(field -> {
        field.title("Locais Livres");
        field.editable.set(false);
        field.value.setValue("0");
    });

    private final FormFieldText textGradesOcupadas = FormFieldText.create(field -> {
        field.title("Grades Ocupados");
        field.editable.set(false);
        field.value.setValue("0");
    });

    private final FormFieldText textGradesLivres = FormFieldText.create(field -> {
        field.title("Grade Livres");
        field.editable.set(false);
        field.value.setValue("0");
    });

    // </editor-fold>

    private VBox tabListagem = (VBox) super.tabs.getTabs().get(0).getContent();

    private VBox tabManutencao = (VBox) super.tabs.getTabs().get(1).getContent();

    public ManutencaoLocaisView() {
        super("Manutenção de Locais", ImageUtils.getImage(ImageUtils.Icon.DEPOSITO), new String[]{"Listagem", "Manutenção", "Relatórios"});
        initListagem();
        initManutencao();
        desenhaBoxParametros();
    }

    public void initManutencao() {
        List<String> pisos = ruas.stream().map(SdRuaExpedicao::getPiso).distinct().collect(Collectors.toList());
        tabManutencao.getChildren().add(FormBox.create(root -> {
            root.horizontal();
            root.add(boxRootRuas -> {
                boxRootRuas.vertical();
                for (String piso : pisos) {
                    List<SdRuaExpedicao> ruasAndar = ruas.stream().filter(it -> it.getPiso().equals(piso)).collect(Collectors.toList());
                    boxRootRuas.add(boxRuas -> {
                        boxRuas.vertical();
                        boxRuas.title("Piso " + piso);
                        boxRuas.add(boxInfos -> {
                            boxInfos.horizontal();
                            boxInfos.title("Informações");
                            boxInfos.add(linha -> {
                                linha.horizontal();
                                linha.add(FormFieldText.create(field -> {
                                    field.title("Qtde Locais");
                                    field.editable.set(false);
                                    field.value.setValue(ruasAndar.stream().reduce(0, (acc, val) -> acc + (int) val.getLocais().stream().map(it -> it.getPosicao() + it.getLado()).distinct().count(), Integer::sum).toString());
                                }).build());
                                linha.add(FormFieldText.create(field -> {
                                    field.title("Locais Ocupados");
                                    field.editable.set(false);
                                    field.addStyle("warning");
                                    field.value.setValue(ruasAndar.stream().reduce(0, (acc, val) -> acc + (int) val.getLocais().stream().filter(it -> it.getProduto() != null).map(it -> it.getPosicao() + it.getLado()).distinct().count(), Integer::sum).toString());
                                }).build());
                                linha.add(FormFieldText.create(field -> {
                                    field.title("Locais Livres");
                                    field.editable.set(false);
                                    field.addStyle("success");
                                    field.value.setValue(String.valueOf(ruasAndar.stream().reduce(0, (acc, val) -> acc + (int) val.getLocais().stream().map(it -> it.getPosicao() + it.getLado()).distinct().count(), Integer::sum) - ruasAndar.stream().reduce(0, (acc, val) -> acc + (int) val.getLocais().stream().filter(it -> it.getProduto() != null).map(it -> it.getPosicao() + it.getLado()).distinct().count(), Integer::sum)));
                                }).build());
                            });
                            boxInfos.add(new Separator(Orientation.VERTICAL));
                            boxInfos.add(linha -> {
                                linha.horizontal();
                                linha.add(FormFieldText.create(field -> {
                                    field.title("Qtde Grades");
                                    field.editable.set(false);
                                    field.value.setValue(ruasAndar.stream().reduce(0, (acc, val) -> acc + val.getLocais().size(), Integer::sum).toString());
                                }).build());
                                linha.add(FormFieldText.create(field -> {
                                    field.title("Grades Ocupadas");
                                    field.editable.set(false);
                                    field.addStyle("warning");
                                    field.value.setValue(ruasAndar.stream().reduce(0, (acc, val) -> acc + (int) val.getLocais().stream().filter(it -> it.getProduto() != null).count(), Integer::sum).toString());
                                }).build());
                                linha.add(FormFieldText.create(field -> {
                                    field.title("Grades Livres");
                                    field.editable.set(false);
                                    field.addStyle("success");
                                    field.value.setValue(ruasAndar.stream().reduce(0, (acc, val) -> acc + (int) val.getLocais().stream().filter(it -> it.getProduto() == null).count(), Integer::sum).toString());
                                }).build());
                            });
                        });
                        boxRuas.add(linha -> {
                            linha.horizontal();
                            for (SdRuaExpedicao rua : ruasAndar) {
                                linha.add(boxRua -> {
                                    boxRua.vertical();
                                    boxRua.size(70, 70);
                                    boxRua.alignment(Pos.CENTER);
                                    boxRua.alignment(VPos.CENTER);
                                    boxRua.setBackground(new Background(new BackgroundFill(Color.web("#802046"), CornerRadii.EMPTY, Insets.EMPTY)));
                                    boxRua.border();
//                                    boxRua.setBackground(new Background(new BackgroundFill(Color.LIGHTGRAY, CornerRadii.EMPTY, Insets.EMPTY)));
                                    boxRua.setOnMouseClicked(event -> {
                                        if (event.getButton().equals(MouseButton.PRIMARY))
                                            if (event.getClickCount() == 1) ruaSelecionada = rua;
                                            else if (event.getClickCount() == 2) janelaVizualizacaoRua(rua);

                                    });
                                    boxRua.hoverProperty().addListener((observable, oldValue, newValue) -> {
                                        boxRua.setBackground(new Background(new BackgroundFill(newValue ? Color.LIGHTGRAY : Color.web("#802046"), CornerRadii.EMPTY, Insets.EMPTY)));
                                    });
                                    boxRua.add(header -> {
                                        header.vertical();
                                        header.alignment(Pos.CENTER);

                                        header.add(FormLabel.create(lbl -> {
                                            lbl.setText(rua.getRua());
                                            lbl.setFont(Font.font(15));
                                            lbl.alignment(Pos.CENTER);
                                            lbl.setTextFill(Color.WHITE);
                                        }));
                                    });
                                });
                            }
                        });
                    });
                }
            });
        }));
    }

    public void initListagem() {
        tabListagem.getChildren().add(FormBox.create(root -> {
            root.horizontal();
            root.expanded();
            root.add(col1 -> {
                col1.vertical();
                col1.add(boxHeader -> {
                    boxHeader.horizontal();
                    boxHeader.add(boxFiltros -> {
                        boxFiltros.vertical();
                        boxFiltros.add(linha -> {
                            linha.horizontal();
                            linha.add(singleFindColecao.build());
                        });
                        boxFiltros.add(linha -> {
                            linha.horizontal();
                            linha.add(comboBoxPiso.build(), comboBoxRuaInicio.build(), comboBoxDirecao.build());
                        });
                        boxFiltros.add(linha -> {
                            linha.horizontal();
                            linha.add(toggleIncluirContinuas.build(), multiColecaoManterLocal.build());
                        });
                        boxFiltros.add(boxRuasSelecionadas);
                    });
                });
                col1.add(boxSelectLinha -> {
                    boxSelectLinha.horizontal();
                    boxSelectLinha.add(boxParametros.build());
                    boxSelectLinha.add(listLinhasOrdenacao.build());
                });
                col1.add(footer -> {
                    footer.horizontal();
                    footer.add(FormButton.create(btn -> {
                        btn.title("Gerar Locais");
                        btn.addStyle("primary");
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._24));
                        btn.setOnAction(evt -> {
                            gerarLocais();
                        });
                    }));
                });
            });
            root.add(col2 -> {
                col2.vertical();
                col2.expanded();
                col2.add(tblProdutos.build());
            });
            root.add(col3 -> {
                col3.vertical();
                col3.expanded();
                col3.add(boxInformacoes -> {
                    boxInformacoes.vertical();
                    boxInformacoes.title("Informações");
                    boxInformacoes.add(l1 -> {
                        l1.horizontal();
                        l1.add(textTodosProdutos.build(), textProdutosPendentes.build(), textProdutosAlocados.build());
                    });
                    boxInformacoes.add(l2 -> {
                        l2.horizontal();
                        l2.add(textTodosLocais.build(), textLocaisOcupados.build(), textLocaisLivres.build(), textGradesLivres.build(), textGradesOcupadas.build());
                    });
                });
                col3.add(boxTblLocais -> {
                    boxTblLocais.vertical();
                    boxTblLocais.expanded();
                    boxTblLocais.add(tblLocais.build());
                });
            });
        }));
    }

    private void gerarLocais() {

        carregaLocaisAtuais();

        List<VSdDadosProdutoCorTam> produtosToDefineLocal = new ArrayList<>();

        for (SdLocalExpedicao local : locaisBean.stream().filter(it -> it.getProduto() != null).collect(Collectors.toList())) {
            if (multiColecaoManterLocal.objectValues.stream().noneMatch(it -> it.getCodigo().equals(local.getProduto().getCodCol()))) {
                local.setProduto(null);
                local.setCor(null);
                local.setTam(null);
            }
        }

        List<SdLocalExpedicao> locaisRuasSelecionadas = locaisBean.stream().filter(it -> {
            FormControlButton btnRua = btnsRuas.stream().filter(eb -> eb.codeFilter.getValueSafe().equals(it.getRua().getRua())).findFirst().get();
            return btnRua.isSelected.not().getValue() && btnRua.disable.not().get();
        }).collect(Collectors.toList());

        produtosToDefineLocal.addAll(produtosCorTam.stream()
                .filter(it -> it.getProdutoObj().isSelected())
                .collect(Collectors.toList())
        );

        produtosToDefineLocal.removeAll(produtosCorTamAlocados.stream().filter(it -> multiColecaoManterLocal.objectValues.stream().anyMatch(eb -> eb.getCodigo().equals(it.getCodcol()))).collect(Collectors.toList()));

        Object[] colecoesParaManter = multiColecaoManterLocal.objectValues.stream().map(eb -> eb.getCodigo()).toArray();

        Where.ResultBuilder where = new FluentDao().selectFrom(VSdDadosProdutoCorTam.class).where(it -> it
                        .isIn("codigo", produtosCorTam.stream().filter(eb -> eb.getProdutoObj().isSelected()).map(eb -> eb.getCodigo()).distinct().toArray())
                        .notIn("codcol", colecoesParaManter, TipoExpressao.AND, when -> colecoesParaManter.length > 0)
//                .equal("possuiPedido", true)
                        .equal("local", "AINSP")
        );

//        List<Ordenacao> ordenacoes = new ArrayList<>();
//
//        for (ParametroConsulta parametro : parametrosBean) {
//            where = where.orderBy(parametro.getField(), parametro.getTipoOrder().equals("ASC") ? OrderType.ASC : OrderType.DESC);
//        }
//        where.orderBy(Arrays.asList(new Ordenacao("codigo"), new Ordenacao("cor"), new Ordenacao("posicao")));

        produtosToDefineLocal = (List<VSdDadosProdutoCorTam>) where.resultList();



        produtosToDefineLocal.sort(Comparator.comparing(o -> 0).thenComparing((o1, o2) -> {

            int resultado = 0;

            for (ParametroConsulta parametroConsulta : parametrosBean) {
                try {

                    if (parametroConsulta.getField().equals("linha")) {
                        resultado = Integer.compare(listLinhasOrdenacao.items.getValue().indexOf(((VSdDadosProdutoCorTam) o1).getLinhaObj()), listLinhasOrdenacao.items.getValue().indexOf(((VSdDadosProdutoCorTam) o2).getLinhaObj()));
                        if (resultado != 0) return resultado;
                    } else {
                        Field field = VSdDadosProdutoCorTam.class.getDeclaredField(parametroConsulta.getField());
                        field.setAccessible(true);
                        resultado = field.get(o1).toString().compareTo(field.get(o2).toString());
                        if (resultado != 0) return resultado;
                    }
                } catch (NoSuchFieldException e) {
                    throw new RuntimeException(e);
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            }

            return resultado;
        }).thenComparing(it -> ((VSdDadosProdutoCorTam) it).getCodigo()).thenComparing(it -> ((VSdDadosProdutoCorTam) it).getCor()).thenComparing(it -> ((VSdDadosProdutoCorTam) it).getPosicao()));

        locaisRuasSelecionadas.sort(comparatorSdLocal);
        List<SdLocalExpedicao> locaisLivres = locaisRuasSelecionadas.stream().filter(it -> it.getProduto() == null).collect(Collectors.toList());

        Map<VSdDadosProduto, Map<String, List<VSdDadosProdutoCorTam>>> collectProdutoCor = produtosToDefineLocal.stream().collect(Collectors.groupingBy(VSdDadosProdutoCorTam::getProdutoObj, Collectors.groupingBy(VSdDadosProdutoCorTam::getCor)));

        AtomicInteger indiceUltimoLocal = new AtomicInteger(0);

        collectProdutoCor.forEach((produto, mapCor) -> {
            mapCor.forEach((cor, listProdutos) -> {
                int locaisNecessarios = listProdutos.size();

                List<SdLocalExpedicao> locaisDisponiveis = new ArrayList<>();

                Set<SdLocalExpedicao> locaisIndisponiveis = new HashSet<>();

                while (locaisDisponiveis.size() < locaisNecessarios) {
                    SdLocalExpedicao posicaoNova = locaisLivres.stream()
                            .filter(it -> locaisDisponiveis.stream().noneMatch(eb -> eb.getLocal().equals(it.getLocal())))
                            .filter(it -> locaisIndisponiveis.stream().noneMatch(eb -> eb.getLocal().equals(it.getLocal())))
                            .filter(it -> locaisLivres.indexOf(it) >= indiceUltimoLocal.get())
                            .findFirst().orElse(null);

                    if (posicaoNova == null) break;

                    System.out.println("--- " + posicaoNova.getLocal());

                    if (
                            locaisRuasSelecionadas.indexOf(posicaoNova) != locaisRuasSelecionadas.size() - 1 &&
                                    locaisRuasSelecionadas.get(locaisRuasSelecionadas.indexOf(posicaoNova) + 1).getProduto() == null &&
                                    locaisLivres.indexOf(posicaoNova) >= indiceUltimoLocal.get()
                    ) {
                        locaisDisponiveis.add(posicaoNova);
                        System.out.println("Posição Adicionada" + posicaoNova.getLocal());
                    } else {
                        locaisIndisponiveis.add(posicaoNova);
                        locaisIndisponiveis.addAll(locaisDisponiveis);
                        locaisDisponiveis.clear();
                        System.out.println("Limpando");
                    }
                }
                System.out.println("Locais Achados");
                if (locaisDisponiveis.size() == locaisNecessarios) {
                    listProdutos.forEach(prod -> {
                        SdLocalExpedicao posicaoNova = locaisDisponiveis.stream().filter(it -> it.getProduto() == null).findFirst().orElse(null);
                        System.out.println("Posição " + posicaoNova.getLocal());
                        posicaoNova.setProduto(prod.getProdutoObj());
                        posicaoNova.setCor(prod.getCor());
                        posicaoNova.setTam(prod.getTam());
                        indiceUltimoLocal.set(locaisLivres.indexOf(posicaoNova));
                        locaisLivres.remove(posicaoNova);
                    });
                }

            });
        });

        locaisBean.sort(comparatorSdLocal);
        carregaCampos();
    }

    private void carregaCampos() {

        long gradesVazias = tblLocais.items.stream().filter(it -> it.getProduto() == null).count();
        Map<VSdDadosProduto, List<VSdDadosProdutoCorTam>> collectProdutos = produtosCorTam.stream().collect(Collectors.groupingBy(produto -> produto.getProdutoObj()));

        Map<String, List<SdLocalExpedicao>> collectPosicoes = locaisBean.stream().collect(Collectors.groupingBy(it -> it.getRua().getRua() + "-" + it.getPosicao() + "-" + it.getLado()));

        textGradesOcupadas.value.setValue(String.valueOf(tblLocais.items.size() - gradesVazias));
        textGradesLivres.value.setValue(String.valueOf(gradesVazias));

        textTodosProdutos.value.setValue(String.valueOf(collectProdutos.keySet().size()));

        AtomicInteger contProdutosAlocados = new AtomicInteger();

        collectProdutos.forEach((produto, listCores) -> {
            if (listCores.stream().allMatch(it ->
                    locaisBean.stream().filter(eb -> eb.getProduto() != null).anyMatch(eb -> eb
                            .getProduto().getCodigo().equals(it.getCodigo()) &&
                            eb.getCor().equals(it.getCor()) &&
                            eb.getTam().equals(it.getTam())
                    )
            )
            )
                contProdutosAlocados.getAndIncrement();
        });

        textProdutosAlocados.value.setValue(String.valueOf(contProdutosAlocados.get()));
        textProdutosPendentes.value.setValue(String.valueOf(collectProdutos.keySet().size() - contProdutosAlocados.get()));


        textTodosLocais.value.setValue(String.valueOf(collectPosicoes.keySet().size()));

        AtomicInteger contLocaisOcupados = new AtomicInteger();

        collectPosicoes.forEach((posicao, listPosicoes) -> {
            if (listPosicoes.stream().allMatch(it -> it.getProduto() != null)) contLocaisOcupados.getAndIncrement();
        });

        textLocaisOcupados.value.setValue(String.valueOf(contLocaisOcupados.get()));
        textLocaisLivres.value.setValue(String.valueOf(collectPosicoes.keySet().size() - contLocaisOcupados.get()));

    }

    private void janelaVizualizacaoRua(SdRuaExpedicao rua) {
        new Fragment().showAndContinue(fg -> {
            fg.size(1600.0, 900.0);
            fg.title("Rua " + rua.getRua());
            FormBox boxInformacaoPosicao = FormBox.create(boxPos -> {
                boxPos.vertical();
                boxPos.expanded();
            });
            fg.box.getChildren().add(FormBox.create(boxRoot -> {
                boxRoot.horizontal();
                boxRoot.add(boxScrollRua -> {
                    boxScrollRua.vertical();
                    boxScrollRua.width(515);
                    boxScrollRua.verticalScroll();
                    Map<String, List<SdLocalExpedicao>> collectPosicoes = rua.getLocais().stream().collect(Collectors.groupingBy(SdLocalExpedicao::getPosicao));
                    for (String posicao : collectPosicoes.keySet().stream().sorted(Comparator.comparingInt(Integer::parseInt)).collect(Collectors.toList())) {
                        boxScrollRua.add(boxPosicao -> {
                            boxPosicao.horizontal();
                            boxPosicao.border();
                            Map<String, List<SdLocalExpedicao>> collectLados = collectPosicoes.get(posicao).stream().collect(Collectors.groupingBy(SdLocalExpedicao::getLado));
                            for (String lado : collectLados.keySet().stream().sorted().collect(Collectors.toList())) {
                                boxPosicao.add(boxLado -> {
                                    boxLado.vertical();
                                    boxLado.setPrefWidth(240);
                                    boxLado.border();
//                                                                    boxLado.setBackground(new Background(new BackgroundFill(Color.LIGHTGRAY, CornerRadii.EMPTY, Insets.EMPTY)));
//                                                                    boxLado.hoverProperty().addListener((observable, oldValue, newValue) -> {
//                                                                        boxLado.setBackground(new Background(new BackgroundFill(newValue ? Color.DARKGRAY : Color.LIGHTGRAY, CornerRadii.EMPTY, Insets.EMPTY)));
//                                                                    });
                                    Map<Integer, List<SdLocalExpedicao>> collectAndar = collectLados.get(lado).stream().collect(Collectors.groupingBy(SdLocalExpedicao::getAndar));
                                    boxLado.setOnMouseClicked(evt -> {
                                        boxInformacaoPosicao.clear();
                                        for (Integer andar : collectAndar.keySet().stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList())) {
                                            boxInformacaoPosicao.add(boxAndar -> {
                                                boxAndar.horizontal();
                                                boxAndar.setBorder(new Border(new BorderStroke(Color.TRANSPARENT, BorderStrokeStyle.SOLID, new CornerRadii(20), BorderWidths.FULL)));
                                                for (SdLocalExpedicao sdLocalExpedicao : collectAndar.get(andar)) {

                                                    FormFieldText textQtde = FormFieldText.create(text -> {
                                                        text.title("Qtde");
                                                        text.editable.set(false);
                                                        text.width(50);
                                                        text.value.setValue(sdLocalExpedicao.getProduto() == null ? "0" : getEstoqueProduto(sdLocalExpedicao).toString());
                                                    });

                                                    FormFieldSingleFind singleFiendProduto = FormFieldSingleFind.create(VSdDadosProdutoCorTam.class, field -> {
                                                        field.title("Produto");
                                                        field.code.editableProperty().unbind();
                                                        field.code.disableProperty().unbind();
                                                        field.code.setDisable(true);
                                                        if (sdLocalExpedicao.getProduto() != null)
                                                            field.value.setValue(getVsdDadosProdutoCorTam(sdLocalExpedicao));
                                                        field.dividedWidth(450);

                                                        field.value.addListener((observable, oldValue, newValue) -> {
                                                            if (newValue != null) {
                                                                textQtde.value.setValue(getEstoqueProduto(sdLocalExpedicao).toString());
                                                            }
                                                        });
                                                    });

                                                    boxAndar.add(boxProduto -> {
                                                        boxProduto.vertical();
                                                        boxProduto.border();
                                                        boxProduto.add(header -> {
                                                            header.vertical();
                                                            header.alignment(Pos.CENTER);
                                                            header.setBackground(new Background(new BackgroundFill(Color.web("#802046"), CornerRadii.EMPTY, Insets.EMPTY)));
                                                            header.add(FormLabel.create(lbl -> {
                                                                lbl.setText(sdLocalExpedicao.getLocal());
                                                                lbl.setFont(Font.font(15));
                                                                lbl.alignment(Pos.CENTER);
                                                                lbl.setTextFill(Color.WHITE);
                                                            }));
                                                        });
                                                        boxProduto.add(main -> {
                                                            main.horizontal();
                                                            main.add(singleFiendProduto.build());
                                                            main.add(textQtde.build());
                                                        });
                                                        boxProduto.add(new Separator(Orientation.HORIZONTAL));
                                                        boxProduto.add(footer -> {
                                                            footer.horizontal();
                                                            footer.alignment(Pos.BOTTOM_RIGHT);
                                                            footer.add(FormButton.create(btn -> {
                                                                btn.addStyle("danger");
                                                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
                                                                btn.title("Cancelar");
                                                                btn.setOnAction(evtBtn -> {
                                                                    if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                                                                        message.message("Tem certeza que deseja cancelar?");
                                                                        message.showAndWait();
                                                                    }).value.get())) {

                                                                    }
                                                                });
                                                            }));
                                                            footer.add(FormButton.create(btn -> {
                                                                btn.addStyle("success");
                                                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                                                                btn.title("Confirmar");
                                                                btn.setOnAction(evtBtn -> {
                                                                });
                                                            }));
                                                        });
                                                    });
                                                }
                                            });
                                        }
                                    });
                                    boxLado.add(header -> {
                                        header.vertical();
                                        header.alignment(Pos.CENTER);
                                        header.setBackground(new Background(new BackgroundFill(Color.web("#802046"), CornerRadii.EMPTY, Insets.EMPTY)));
                                        header.add(FormLabel.create(lbl -> {
                                            lbl.setText(posicao + " - " + lado);
                                            lbl.setFont(Font.font(15));
                                            lbl.alignment(Pos.CENTER);
                                            lbl.setTextFill(Color.WHITE);
                                        }));
                                    });
                                    boxLado.add(content -> {
                                        content.vertical();
                                        for (SdLocalExpedicao sdLocalExpedicao : collectLados.get(lado).stream().sorted((o1, o2) -> Integer.compare(o2.getAndar(), o1.getAndar())).collect(Collectors.toList())) {
                                            content.add(boxProdutoLocal -> {
                                                boxProdutoLocal.horizontal();
                                                if (sdLocalExpedicao.getProduto() != null) {
                                                    boxProdutoLocal.add(FormLabel.create(lbl -> {
                                                        lbl.setPrefWidth(150);
                                                        lbl.setText(sdLocalExpedicao.getAndar() + ": " + sdLocalExpedicao.getProduto().getCodigo() + "-" + sdLocalExpedicao.getCor() + "-" + sdLocalExpedicao.getTam());
                                                    }));
                                                    boxProdutoLocal.add(FormLabel.create(lbl -> {
                                                        lbl.setText("Qtde: " + getEstoqueProduto(sdLocalExpedicao));
                                                    }));
                                                } else {
                                                    boxProdutoLocal.add(FormLabel.create(lbl -> {
                                                        lbl.setText("Sem Produto");
                                                        lbl.boldText();
                                                        lbl.setPrefWidth(100);
                                                    }));
                                                }
                                            });
                                        }
                                    });
                                });
                            }
                        });
                    }
                });
                boxRoot.add(boxInformacaoPosicao);
            }));
        });
    }

    @Nullable
    private static Object getVsdDadosProdutoCorTam(SdLocalExpedicao sdLocalExpedicao) {
        return new FluentDao().selectFrom(VSdDadosProdutoCorTam.class).where(it -> it.equal("codigo", sdLocalExpedicao.getProduto().getCodigo()).equal("cor", sdLocalExpedicao.getCor()).equal("tam", sdLocalExpedicao.getTam())).singleResult();
    }

    @Nullable
    private BigDecimal getEstoqueProduto(SdLocalExpedicao sdLocalExpedicao) {
        List<PaIten> estoque = (List<PaIten>) new FluentDao().selectFrom(PaIten.class).where(it -> it.equal("id.codigo", sdLocalExpedicao.getProduto().getCodigo()).equal("id.cor", sdLocalExpedicao.getCor()).equal("id.tam", sdLocalExpedicao.getTam()).isIn("id.deposito", new String[]{"0005", "0023"})).resultList();
        return estoque.stream().map(PaIten::getQuantidade).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private void desenhaBotaoRua(FormBox box, SdRuaExpedicao rua) {
        FormControlButton btnRua = FormControlButton.create(btn -> {
            btn.title(rua.getRua());
            btn.addStyle("sm");
            btn.codeFilter.set(rua.getRua());
            btn.bgColorStandard.set("#5cb85c");
            btn.bgColorSelected.set("#d9534f");
            comboBoxRuaInicio.getSelectionModel((observable, oldValue, newValue) -> {
                if (rua.getRua().compareTo(newValue) < 0) {
                    btn.disable.set(true);
                } else {
                    btn.disable.set(false);
                }
            });
        });

        box.add(btnRua);
        btnsRuas.add(btnRua);
    }

    private void consultarProdutos() {
        produtos.clear();
        produtosCorTamAlocados.clear();
        produtosCorTam.clear();

        Colecao colecao = singleFindColecao.value.getValue();
        if (colecao == null) return;
        List<VSdDadosProdutoCorTam> produtosColecao = (List<VSdDadosProdutoCorTam>) new FluentDao().selectFrom(VSdDadosProdutoCorTam.class)
                .where(it -> it
                        .equal("colAtual.codigo", colecao.getCodigo())
                        .notEqual("codcol", "17CO", TipoExpressao.AND, when -> !toggleIncluirContinuas.value.getValue()))
                .resultList();
        produtosCorTam.addAll(produtosColecao);
        produtos.addAll(produtosColecao.stream().map(it -> it.getProdutoObj()).distinct().collect(Collectors.toList()));
        produtos.forEach(prod -> prod.setSelected(true));

        carregaLocaisAtuais();
        carregaCampos();
    }

    private void carregaLocaisAtuais() {
        locaisBean.clear();
        locaisBean.addAll((List<SdLocalExpedicao>) new FluentDao().selectFrom(SdLocalExpedicao.class).where(it -> it.equal("rua.piso", comboBoxPiso.value.getValue())).resultList());

        for (SdLocalExpedicao local : locaisBean) {
            local.setProduto(null);
            local.setCor(null);
            local.setTam(null);
        }

        for (SdLocalExpedicao local : locaisBean) {
            produtosCorTam.stream().filter(it -> it.isPossuiPedido()).filter(it -> it.getLocal().equals(local.getLocal())).findFirst().ifPresent(prod -> {
                local.setProduto(prod.getProdutoObj());
                local.setCor(prod.getCor());
                local.setTam(prod.getTam());
                produtosCorTamAlocados.add(prod);
            });
        }
        locaisBean.sort(comparatorSdLocal);

    }

    private void desenhaBoxParametros() {
        boxParametros.clear();
        boxParametros.add(header -> {
            header.horizontal();
            header.border();
            header.add(FormButton.create(btn -> {
                btn.addStyle("success");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                btn.title("Adicionar Ordenação");
                btn.width(170);
                btn.setOnAction(evt -> {
                    parametrosBean.add(new ParametroConsulta(parametrosBean.size(), "", "ASC"));
                    desenhaBoxParametros();
                });
            }));
            header.add(FormButton.create(btn -> {
                btn.addStyle("danger");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
                btn.title("Limpar Itens");
                btn.width(170);
                btn.setOnAction(evt -> {
                    parametrosBean.clear();
                    desenhaBoxParametros();
                });
            }));
        });

        if (parametrosBean.size() == 0) {
            return;
        }
        for (ParametroConsulta parametro : parametrosBean.sorted(Comparator.comparing(ParametroConsulta::getIndice))) {
            boxParametros.add(box -> {
                box.horizontal();
                box.border();
                box.alignment(Pos.CENTER);
                box.add(FormButton.create(btn -> {
                    btn.addStyle("danger");
                    btn.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                    btn.tooltip("Remover Parâmetro");
                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
                    btn.setOnAction(evt -> {
                        try {
                            parametrosBean.remove(parametro);
                            parametrosBean.forEach(it -> it.setIndice(parametrosBean.indexOf(it)));
                            desenhaBoxParametros();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    });
                }));
                box.add(boxBtns -> {
                    boxBtns.vertical();
                    boxBtns.add(FormButton.create(btn -> {
                        btn.addStyle("warning").addStyle("xs");
                        btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CIMA, ImageUtils.IconSize._16));
                        if (parametro.getIndice() == 0) btn.disable.set(true);
                        btn.setOnAction(evt -> {
                            ParametroConsulta parametroConsulta = parametrosBean.stream().filter(it -> it.getIndice() == parametro.getIndice() - 1).findFirst().orElse(null);
                            if (parametroConsulta != null) {
                                parametro.setIndice(parametro.getIndice() - 1);
                                parametroConsulta.setIndice(parametroConsulta.getIndice() + 1);
                                desenhaBoxParametros();
                            }
                        });
                    }));
                    boxBtns.add(FormButton.create(btn -> {
                        btn.addStyle("warning").addStyle("xs");
                        btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.BAIXO, ImageUtils.IconSize._16));
                        if (parametro.getIndice() == parametrosBean.size() - 1) btn.disable.set(true);
                        btn.setOnAction(evt -> {
                            ParametroConsulta parametroConsulta = parametrosBean.stream().filter(it -> it.getIndice() == parametro.getIndice() + 1).findFirst().orElse(null);
                            if (parametroConsulta != null) {
                                parametro.setIndice(parametro.getIndice() + 1);
                                parametroConsulta.setIndice(parametroConsulta.getIndice() - 1);
                                desenhaBoxParametros();
                            }
                        });
                    }));
                });
                box.add(FormLabel.create(lbl -> {
                    lbl.setText(String.valueOf(parametro.getIndice()));
                }));
                box.add(FormFieldComboBox.create(String.class, combo -> {
                    combo.title("Atributo");
                    combo.width(130);
                    combo.items(FXCollections.observableArrayList(fieldsToOrder.values()));
                    combo.select(fieldsToOrder.get(parametro.getField()));
                    combo.getSelectionModel((observable, oldValue, newValue) -> {
                        if (newValue != null) {
                            parametro.setField(fieldsToOrder.getKey(newValue));
                        }
                    });

                }).build());
                box.add(FormFieldComboBox.create(String.class, combo -> {
                    combo.title("Ordenar Por");
                    combo.width(90);
                    combo.items(FXCollections.observableList(Arrays.asList("Maior", "Menor")));
                    combo.select(parametro.getTipoOrder().equals("DESC") ? "Maior" : "Menor");
                    combo.getSelectionModel((observable, oldValue, newValue) -> {
                        if (newValue != null) {
                            parametro.setTipoOrder(newValue.equals("Maior") ? "DESC" : "ASC");
                        }
                    });
                }).build());
            });
        }
    }

}
