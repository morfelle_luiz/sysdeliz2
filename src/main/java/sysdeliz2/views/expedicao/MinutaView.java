package sysdeliz2.views.expedicao;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ContentDisplay;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import org.apache.log4j.Logger;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.models.sysdeliz.SdTela;
import sysdeliz2.utils.StaticVersion;
import sysdeliz2.utils.gui.components.FormBadges;
import sysdeliz2.utils.gui.components.FormBox;
import sysdeliz2.utils.gui.components.FormButton;
import sysdeliz2.utils.gui.components.FormLabel;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

public class MinutaView extends VBox {

    private static final Logger logger = Logger.getLogger(MinutaView.class);

    public MinutaView() {
        JPAUtils.getEntityManager();
        init();
    }

    public void init() {
        super.getChildren().clear();
        super.getChildren().add(FormBadges.create(badge -> {
            badge.position(Pos.TOP_RIGHT, 0.0);
            badge.node(FormButton.create(btn -> {
                btn.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.DESLIGAR, ImageUtils.IconSize._48));
                btn.setStyle("-fx-background-color: transparent; -fx-border-color: transparent");
                btn.setPadding(new Insets(0));
                btn.setOnAction(evt -> {
                    System.exit(0);
                });
            }));
            badge.badgedNode(FormBox.create(principal -> {
                principal.vertical();
                principal.expanded();
                principal.alignment(Pos.CENTER);
                principal.add(FormBox.create(head -> {
                    head.horizontal();
                    head.add(FormBox.create(header -> {
                        header.vertical();
                        header.alignment(Pos.BOTTOM_CENTER);
                        header.setPadding(new Insets(8, 0, 0, 0));
                        ImageView icon = ImageUtils.getIcon(ImageUtils.Icon.LOGO_DLZ, ImageUtils.IconSize._24);
                        icon.setPickOnBounds(true);
                        icon.setPreserveRatio(true);
                        header.add(icon);
                    }));
                    head.add(FormLabel.create(label -> {
                        label.value.set(StaticVersion.versaoSistema);
                    }).build());
                }));
                principal.add(FormBox.create(boxMain -> {
                    boxMain.flutuante();
                    boxMain.expanded();
                    boxMain.alignment(Pos.CENTER);
                    boxMain.setPadding(new Insets(0, 8, 0, 8));

                    List<SdTela> telasMobile = (List<SdTela>) new FluentDao().selectFrom(SdTela.class)
                            .where(it -> it.equal("mobile", true))
                            .orderBy("codTela", OrderType.ASC).resultList();
                    for (SdTela tela : telasMobile) {
                        createButton(boxMain, tela);
                    }
                }));

            }));
        }));
    }

    private void createButton(FormBox boxMain, SdTela tela) {
        boxMain.add(FormButton.create(btn -> {
            btn.contentDisplay(ContentDisplay.TOP);
            btn.title(tela.getNomeTela());
            btn.width(105);
            btn.height(105);
            ImageView imageView = new ImageView(new Image(getClass().getResourceAsStream(tela.getIcone())));
            imageView.setFitHeight(48);
            imageView.setFitWidth(48);
            imageView.setPickOnBounds(true);
            imageView.setPreserveRatio(true);
            btn.icon(imageView);
            btn.addStyle(tela.getStyle());
            btn.setAction(evt -> {
                try {
                     Class.forName(tela.getPath()).getConstructor(MinutaView.class).newInstance(this);
                } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            });
        }));
    }
}
