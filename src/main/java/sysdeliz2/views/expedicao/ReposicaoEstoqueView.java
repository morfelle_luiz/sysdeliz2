package sysdeliz2.views.expedicao;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import sysdeliz2.controllers.views.expedicao.ReposicaoEstoqueController;
import sysdeliz2.controllers.views.expedicao.ReposicaoEstoqueJobController;
import sysdeliz2.models.view.VSdReposicaoEstoque;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.exceptions.FormValidationException;
import sysdeliz2.utils.gui.components.FormBox;
import sysdeliz2.utils.gui.components.FormButton;
import sysdeliz2.utils.gui.components.FormTableColumn;
import sysdeliz2.utils.gui.components.FormTableView;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.sql.SQLException;

public class ReposicaoEstoqueView extends ReposicaoEstoqueController {
    
    private final FormTableView<VSdReposicaoEstoque> tblReposicoes = FormTableView.create(VSdReposicaoEstoque.class, table -> {
        table.title("Produtos para Reposição");
        table.expanded();
        table.items.bind(beanReposicoes);
        table.tableProperties().setStyle("-fx-font-size: 14pt;");
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("");
                    cln.width(50.0);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdReposicaoEstoque, VSdReposicaoEstoque>, ObservableValue<VSdReposicaoEstoque>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<VSdReposicaoEstoque, VSdReposicaoEstoque>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnResolver = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                                btn.tooltip("Confirmar Reposição");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("success");
                            });
                            
                            @Override
                            protected void updateItem(VSdReposicaoEstoque item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    btnResolver.setOnAction(evt -> {
                                        resolverReposicao(item);
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    if (!item.getCodigo().getStatus().equals("R"))
                                        boxButtonsRow.getChildren().addAll(btnResolver);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Remessa");
                    cln.width(100.0);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdReposicaoEstoque, VSdReposicaoEstoque>, ObservableValue<VSdReposicaoEstoque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getRemessa()));
                }).build() /*Remessa*/,
                FormTableColumn.create(cln -> {
                    cln.title("Produto");
                    cln.width(90.0);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdReposicaoEstoque, VSdReposicaoEstoque>, ObservableValue<VSdReposicaoEstoque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getProduto().getCodigo()));
                }).build() /*Produto*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cor");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdReposicaoEstoque, VSdReposicaoEstoque>, ObservableValue<VSdReposicaoEstoque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getCor()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Cor*/,
                FormTableColumn.create(cln -> {
                    cln.title("Local");
                    cln.width(100.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdReposicaoEstoque, VSdReposicaoEstoque>, ObservableValue<VSdReposicaoEstoque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getLocal()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Local*/,
//                FormTableColumn.create(cln -> {
//                    cln.title("Data");
//                    cln.width(140.0);
//                    cln.value((Callback<TableColumn.CellDataFeatures<VSdReposicaoEstoque, VSdReposicaoEstoque>, ObservableValue<VSdReposicaoEstoque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getDtemissao()));
//                    cln.alignment(Pos.CENTER);
//                    cln.format(param -> {
//                        return new TableCell<VSdReposicaoEstoque, LocalDateTime>() {
//                            @Override
//                            protected void updateItem(LocalDateTime item, boolean empty) {
//                                super.updateItem(item, empty);
//                                setText(null);
//                                if (item != null && !empty) {
//                                    setText(StringUtils.toDateTimeFormat(item));
//                                }
//                            }
//                        };
//                    });
//                }).build() /*Data*/,
//                FormTableColumn.create(cln -> {
//                    cln.title("Coletor");
//                    cln.width(160.0);
//                    cln.value((Callback<TableColumn.CellDataFeatures<VSdReposicaoEstoque, VSdReposicaoEstoque>, ObservableValue<VSdReposicaoEstoque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getColetor().toString()));
//                }).build() /*Coletor*/,
                FormTableColumn.create(cln -> {
                    cln.title("Estoque");
                    cln.width(800.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdReposicaoEstoque, VSdReposicaoEstoque>, ObservableValue<VSdReposicaoEstoque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDep0005()));
                }).build() /*Estoque*/,
                FormTableColumn.create(cln -> {
                    cln.title("");
                    cln.width(50.0);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdReposicaoEstoque, VSdReposicaoEstoque>, ObservableValue<VSdReposicaoEstoque>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<VSdReposicaoEstoque, VSdReposicaoEstoque>(){
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnAvisarFuro = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CAIXA_VAZIA, ImageUtils.IconSize._24));
                                btn.tooltip("Informar furo de estoque");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("danger");
                            });
                            @Override
                            protected void updateItem(VSdReposicaoEstoque item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    
                                    btnAvisarFuro.setOnAction(evt -> {
                                        avisarFuroEstoque(item);
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnAvisarFuro);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build()
//                FormTableColumn.create(cln -> {
//                    cln.title("Estoque 0001");
//                    cln.width(800.0);
//                    cln.value((Callback<TableColumn.CellDataFeatures<VSdReposicaoEstoque, VSdReposicaoEstoque>, ObservableValue<VSdReposicaoEstoque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDep0001()));
//                }).build() /*Estoque 0001*/
        );
        table.factoryRow(param -> {
            return new TableRow<VSdReposicaoEstoque>() {
                @Override
                protected void updateItem(VSdReposicaoEstoque item, boolean empty) {
                    super.updateItem(item, empty);
                    clearStyle();
                    if (item != null && !empty) {
                        if (item.getCodigo().getStatus().equals("F"))
                            getStyleClass().add("table-row-danger");
                        else if (item.getCodigo().getStatus().equals("A"))
                            getStyleClass().add("table-row-warning");
                    }
                }
                
                private void clearStyle() {
                    getStyleClass().removeAll("table-row-danger", "table-row-warning");
                }
            };
        });
        table.indices(
                table.indice(Color.LIGHTPINK, "Apontado Falta", false),
                table.indice(Color.LIGHTYELLOW, "Aviso de Reposição", false)
        );
    });
    private final FormTableView<VSdReposicaoEstoque> tblFurosEstoque = FormTableView.create(VSdReposicaoEstoque.class, table -> {
        table.title("Produtos para Reposição");
        table.expanded();
        table.items.bind(beanFurosDeEstoque);
        table.tableProperties().setStyle("-fx-font-size: 14pt;");
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("");
                    cln.width(50.0);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdReposicaoEstoque, VSdReposicaoEstoque>, ObservableValue<VSdReposicaoEstoque>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<VSdReposicaoEstoque, VSdReposicaoEstoque>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnResolver = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                                btn.tooltip("Confirmar Reposição");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("success");
                            });
                            
                            @Override
                            protected void updateItem(VSdReposicaoEstoque item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    btnResolver.setOnAction(evt -> {
                                        resolverReposicao(item);
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    if (!item.getCodigo().getStatus().equals("R"))
                                        boxButtonsRow.getChildren().addAll(btnResolver);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Remessa");
                    cln.width(100.0);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdReposicaoEstoque, VSdReposicaoEstoque>, ObservableValue<VSdReposicaoEstoque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getRemessa()));
                }).build() /*Remessa*/,
                FormTableColumn.create(cln -> {
                    cln.title("Produto");
                    cln.width(90.0);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdReposicaoEstoque, VSdReposicaoEstoque>, ObservableValue<VSdReposicaoEstoque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getProduto().getCodigo()));
                }).build() /*Produto*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cor");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdReposicaoEstoque, VSdReposicaoEstoque>, ObservableValue<VSdReposicaoEstoque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getCor()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Cor*/,
                FormTableColumn.create(cln -> {
                    cln.title("Local");
                    cln.width(100.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdReposicaoEstoque, VSdReposicaoEstoque>, ObservableValue<VSdReposicaoEstoque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getLocal()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Local*/,
//                FormTableColumn.create(cln -> {
//                    cln.title("Data");
//                    cln.width(140.0);
//                    cln.value((Callback<TableColumn.CellDataFeatures<VSdReposicaoEstoque, VSdReposicaoEstoque>, ObservableValue<VSdReposicaoEstoque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getDtemissao()));
//                    cln.alignment(Pos.CENTER);
//                    cln.format(param -> {
//                        return new TableCell<VSdReposicaoEstoque, LocalDateTime>() {
//                            @Override
//                            protected void updateItem(LocalDateTime item, boolean empty) {
//                                super.updateItem(item, empty);
//                                setText(null);
//                                if (item != null && !empty) {
//                                    setText(StringUtils.toDateTimeFormat(item));
//                                }
//                            }
//                        };
//                    });
//                }).build() /*Data*/,
//                FormTableColumn.create(cln -> {
//                    cln.title("Coletor");
//                    cln.width(160.0);
//                    cln.value((Callback<TableColumn.CellDataFeatures<VSdReposicaoEstoque, VSdReposicaoEstoque>, ObservableValue<VSdReposicaoEstoque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getColetor().toString()));
//                }).build() /*Coletor*/,
                FormTableColumn.create(cln -> {
                    cln.title("Estoque");
                    cln.width(800.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdReposicaoEstoque, VSdReposicaoEstoque>, ObservableValue<VSdReposicaoEstoque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDep0005()));
                }).build() /*Estoque*/
//                FormTableColumn.create(cln -> {
//                    cln.title("Estoque 0001");
//                    cln.width(800.0);
//                    cln.value((Callback<TableColumn.CellDataFeatures<VSdReposicaoEstoque, VSdReposicaoEstoque>, ObservableValue<VSdReposicaoEstoque>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDep0001()));
//                }).build() /*Estoque 0001*/
        );
        table.factoryRow(param -> {
            return new TableRow<VSdReposicaoEstoque>() {
                @Override
                protected void updateItem(VSdReposicaoEstoque item, boolean empty) {
                    super.updateItem(item, empty);
                    clearStyle();
                    if (item != null && !empty) {
                        if (item.getCodigo().getStatus().equals("F"))
                            getStyleClass().add("table-row-danger");
                        else if (item.getCodigo().getStatus().equals("A"))
                            getStyleClass().add("table-row-warning");
                    }
                }
                
                private void clearStyle() {
                    getStyleClass().removeAll("table-row-danger", "table-row-warning");
                }
            };
        });
        table.indices(
                table.indice(Color.LIGHTPINK, "Apontado Falta", false),
                table.indice(Color.LIGHTYELLOW, "Aviso de Reposição", false)
        );
    });
    
    private Scheduler scheduler = new StdSchedulerFactory().getScheduler();
    private final VBox abaApontamentos = (VBox) super.tabs.getTabs().get(0).getContent();
    private final VBox abaFurosDeEstoque = (VBox) super.tabs.getTabs().get(1).getContent();
    
    public ReposicaoEstoqueView() throws SchedulerException {
        super("Resposição de Peças para Estoque", ImageUtils.getImage(ImageUtils.Icon.DEPOSITO), Globals.isPortatil());
        JPAUtils.getEntityManager();
        
        if (Globals.isPortatil()) {
            repositor = getColetor(Globals.getNomeUsuario());
            if (repositor == null) {
                MessageBox.create(message -> {
                    message.message("Usuário não habilitado para reposição de produtos.");
                    message.type(MessageBox.TypeMessageBox.ALERT);
                    message.showAndWait();
                });
                throw new FormValidationException("Coletor não existe!");
            } else {
                SysLogger.addSysDelizLog("Reposicao Estoque", TipoAcao.ENTRAR, String.valueOf(repositor.getCodigo()), "Repositor " + repositor.getNome() + " entrou na tela de reposição.");
                init();
                onLoad();
                try {
                    initJob();
                } catch (SchedulerException e) {
                    e.printStackTrace();
                    ExceptionBox.build(message -> {
                        message.exception(e);
                        message.showAndWait();
                    });
                }
            }
        } else {
            init();
            onLoad();
            try {
                initJob();
            } catch (SchedulerException e) {
                e.printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(e);
                    message.showAndWait();
                });
            }
        }
    }
    
    private void init() {
        abaApontamentos.getChildren().add(FormBox.create(mainContainer -> {
            mainContainer.vertical();
            mainContainer.expanded();
            mainContainer.add(tblReposicoes.build());
        }));
        abaFurosDeEstoque.getChildren().add(FormBox.create(mainContainer -> {
            mainContainer.vertical();
            mainContainer.expanded();
            mainContainer.add(tblFurosEstoque.build());
        }));
    }
    
    private void onLoad() {
        getReposicoes();
        getFurosEstoque();
    }
    
    private void resolverReposicao(VSdReposicaoEstoque item) {
        try {
            resolveReposicao(item);
            beanReposicoes.remove(item);
            beanFurosDeEstoque.remove(item);
            MessageBox.create(message -> {
                message.message("Reposição marcada como resolvida!");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            tblReposicoes.refresh();
            tblFurosEstoque.refresh();
            SysLogger.addSysDelizLog("Resposicao Estoque", TipoAcao.EDITAR, item.getCodigo().getProduto().getCodigo(),
                    "Repositor " + repositor.getNome() + " marcou produto na cor " + item.getCodigo().getCor() + " como resolvido para o lançamento " + item.getCodigo().getCodigo());
        } catch (SQLException e) {
            e.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(e);
                message.showAndWait();
            });
        }
    }
    
    private void avisarFuroEstoque(VSdReposicaoEstoque item) {
        try {
            informarFuroEstoque(item);
            beanReposicoes.remove(item);
            beanFurosDeEstoque.remove(item);
            MessageBox.create(message -> {
                message.message("Reposição marcada como furo de estoque!");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            tblReposicoes.refresh();
            tblFurosEstoque.refresh();
            SysLogger.addSysDelizLog("Resposicao Estoque", TipoAcao.EDITAR, item.getCodigo().getProduto().getCodigo(),
                    "Repositor " + repositor.getNome() + " marcou produto na cor " + item.getCodigo().getCor() + " como furo de estoque para o lançamento " + item.getCodigo().getCodigo());
        } catch (SQLException e) {
            e.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(e);
                message.showAndWait();
            });
        }
    }
    
    private void initJob() throws SchedulerException {
        JobDetail job = JobBuilder.newJob(ReposicaoEstoqueJobController.class).build();
        Trigger trigger = TriggerBuilder.newTrigger().withIdentity("AtualizaReposicoes", "schedule").withSchedule(CronScheduleBuilder.cronSchedule("0 0/1 * * * ?")).build();
        trigger.getJobDataMap().put("list", beanReposicoes);
        trigger.getJobDataMap().put("furos", beanFurosDeEstoque);
        
        scheduler.scheduleJob(job, trigger);
        scheduler.start();
    }
    
    @Override
    public void closeWindow() {
        try {
            scheduler.shutdown();
        } catch (SchedulerException e) {
            e.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(e);
                message.showAndWait();
            });
        }
        super.closeWindow();
    }
}
