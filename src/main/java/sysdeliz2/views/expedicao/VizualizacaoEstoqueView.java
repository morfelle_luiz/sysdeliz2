package sysdeliz2.views.expedicao;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.stage.Popup;
import javafx.util.Callback;
import sysdeliz2.controllers.views.expedicao.VizualizacaoEstoqueController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.ti.Cor;
import sysdeliz2.models.view.VSdDadosProduto;
import sysdeliz2.models.view.VSdLocalExpedicao;
import sysdeliz2.models.view.VSdLocalExpedicaoQuant;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;

import java.awt.*;
import java.util.List;
import java.util.*;
import java.util.stream.Collectors;

public class VizualizacaoEstoqueView extends VizualizacaoEstoqueController {

    //<editor-fold desc="Bean">
    private final ListProperty<VSdLocalExpedicaoQuant> locaisBean = new SimpleListProperty<>();
    private List<VSdLocalExpedicao> todosLocais = new ArrayList<>();

    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Table">
    private final FormTableView<VSdLocalExpedicaoQuant> tblLocalExpedicao = FormTableView.create(VSdLocalExpedicaoQuant.class, table -> {
        table.title("Locais");
        table.expanded();
        table.items.bind(locaisBean);
        table.factoryRow(param -> {
            return new TableRow<VSdLocalExpedicaoQuant>() {
                @Override
                protected void updateItem(VSdLocalExpedicaoQuant item, boolean empty) {
                    super.updateItem(item, empty);
                    clear();

                    if (item != null && !empty) {
                        if (item.getId().getProduto().equals("S PROD")) getStyleClass().add("table-row-danger");
                        else if (item.getEstoque() > 0) getStyleClass().add("table-row-success");
                        else getStyleClass().add("table-row-warning");
                    }
                }

                private void clear() {
                    getStyleClass().removeAll("table-row-danger", "table-row-success", "table-row-warning");
                }
            };
        });
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Ações");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(115.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdLocalExpedicaoQuant, VSdLocalExpedicaoQuant>, ObservableValue<VSdLocalExpedicaoQuant>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<VSdLocalExpedicaoQuant, VSdLocalExpedicaoQuant>() {
                        final HBox boxButtonsRow = new HBox(3);
                        final Button btnVisualizar = FormButton.create(btn -> {
                            btn.icon(ImageUtils.getIconButton(ImageUtils.ButtonIcon.FIND, ImageUtils.IconSize._16));
                            btn.tooltip("Visualizar Cadastro");
                            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            btn.addStyle("xs").addStyle("info");
                        });
                        final Button btnEditar = FormButton.create(btn -> {
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EDIT, ImageUtils.IconSize._16));
                            btn.tooltip("Editar Cadastro");
                            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            btn.addStyle("xs").addStyle("warning");
                        });
                        final Button btnExcluir = FormButton.create(btn -> {
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.TRASH, ImageUtils.IconSize._16));
                            btn.tooltip("Excluir Cadastro");
                            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            btn.addStyle("xs").addStyle("danger");
                        });

                        @Override
                        protected void updateItem(VSdLocalExpedicaoQuant item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {

                                btnVisualizar.setOnAction(evt -> {
                                    abrirCadastro(item);
                                });
                                btnEditar.setOnAction(evt -> {
                                    editarCadastro(item);
                                });
                                btnExcluir.setOnAction(evt -> {
                                    excluirCadastro(item);
                                });
                                boxButtonsRow.getChildren().clear();
                                boxButtonsRow.getChildren().addAll(btnVisualizar, btnEditar, btnExcluir);
                                setGraphic(boxButtonsRow);
                            }
                        }
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Andar");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdLocalExpedicaoQuant, VSdLocalExpedicaoQuant>, ObservableValue<VSdLocalExpedicaoQuant>>) param -> new ReadOnlyObjectWrapper(param.getValue().getAndar()));
                    cln.alignment(Pos.CENTER);
                }).build(), /*Andar*/
                FormTableColumn.create(cln -> {
                    cln.title("Rua");
                    cln.width(100);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdLocalExpedicaoQuant, VSdLocalExpedicaoQuant>, ObservableValue<VSdLocalExpedicaoQuant>>) param -> new ReadOnlyObjectWrapper(param.getValue().getRua()));
                }).build(), /*Andar*/
                FormTableColumn.create(cln -> {
                    cln.title("Posição");
                    cln.width(120);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdLocalExpedicaoQuant, VSdLocalExpedicaoQuant>, ObservableValue<VSdLocalExpedicaoQuant>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPosicao()));
                }).build(), /*Andar*/
                FormTableColumn.create(cln -> {
                    cln.title("Lado");
                    cln.width(100);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdLocalExpedicaoQuant, VSdLocalExpedicaoQuant>, ObservableValue<VSdLocalExpedicaoQuant>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLado()));
                }).build(), /*Andar*/
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(170);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdLocalExpedicaoQuant, VSdLocalExpedicaoQuant>, ObservableValue<VSdLocalExpedicaoQuant>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getProduto()));
                }).build(), /*Andar*/
                FormTableColumn.create(cln -> {
                    cln.title("Cor");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdLocalExpedicaoQuant, VSdLocalExpedicaoQuant>, ObservableValue<VSdLocalExpedicaoQuant>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCor()));
                }).build(), /*Andar*/
                FormTableColumn.create(cln -> {
                    cln.title("Estoque");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdLocalExpedicaoQuant, VSdLocalExpedicaoQuant>, ObservableValue<VSdLocalExpedicaoQuant>>) param -> new ReadOnlyObjectWrapper(param.getValue().getEstoque()));
                }).build(), /*Andar*/
                FormTableColumn.create(cln -> {
                    cln.title("Producao");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdLocalExpedicaoQuant, VSdLocalExpedicaoQuant>, ObservableValue<VSdLocalExpedicaoQuant>>) param -> new ReadOnlyObjectWrapper(param.getValue().getProducao()));
                }).build(), /*Andar*/
                FormTableColumn.create(cln -> {
                    cln.title("Acabamento");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdLocalExpedicaoQuant, VSdLocalExpedicaoQuant>, ObservableValue<VSdLocalExpedicaoQuant>>) param -> new ReadOnlyObjectWrapper(param.getValue().getAcabamento()));
                }).build(), /*Andar*/
                FormTableColumn.create(cln -> {
                    cln.title("Qtde Total");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdLocalExpedicaoQuant, VSdLocalExpedicaoQuant>, ObservableValue<VSdLocalExpedicaoQuant>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtdeTotal()));
                }).build() /*Andar*/
        );
    });
    // </editor-fold>

    //<editor-fold desc="Box">
    private final VBox listagemTab = (VBox) this.tabs.getTabs().get(0).getContent();
    private final VBox manutencaoTab = (VBox) this.tabs.getTabs().get(1).getContent();
    private final VBox vizualizacaoTab = (VBox) this.tabs.getTabs().get(2).getContent();
    private final FormNavegation<VSdLocalExpedicaoQuant> navegation = FormNavegation.create(nav -> {
        nav.withNavegation(tblLocalExpedicao);
        nav.withActions(true, true, true);
        nav.withReturn(evt -> super.tabs.getSelectionModel().select(0));
        nav.btnAddRegister(evt -> novoCadastro());
        nav.btnDeleteRegister(evt -> excluirCadastro((VSdLocalExpedicaoQuant) nav.selectedItem.get()));
        nav.btnEditRegister(evt -> {
            editarCadastro((VSdLocalExpedicaoQuant) nav.selectedItem.get());
        });
        nav.btnSave(evt -> {
            salvarCadastro();
        });
        nav.btnCancel(evt -> cancelarCadastro());
        nav.selectedItem.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                carregaDados((VSdLocalExpedicaoQuant) newValue);
            }
        });
    });
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Filed">

    private final FormFieldComboBox<String> ladoField = FormFieldComboBox.create(String.class, field -> {
        field.title("Lado");
        field.items(FXCollections.observableArrayList(Arrays.asList("D", "E")));
    });

    private final FormFieldComboBox<String> posicaoField = FormFieldComboBox.create(String.class, field -> {
        field.title("Posição");
    });

    private final FormFieldComboBox<String> ruaField = FormFieldComboBox.create(String.class, field -> {
        field.items(FXCollections.observableArrayList(todosLocais.stream().map(VSdLocalExpedicao::getRua).collect(Collectors.toList())));
        field.select(0);
        field.title("Rua");
        field.getSelectionModel((observable, oldValue, newValue) -> {
            posicaoField.items(FXCollections.observableArrayList(todosLocais.stream().filter(it -> it.getRua().equals(newValue)).map(VSdLocalExpedicao::getPosicao).distinct().collect(Collectors.toList())));
        });
    });

    private final FormFieldComboBox<String> andarField = FormFieldComboBox.create(String.class, field -> {
        field.title("Andar");
        field.items(FXCollections.observableArrayList(Arrays.asList("0", "1")));
        field.getSelectionModel((observable, oldValue, newValue) -> {
            ruaField.items(FXCollections.observableArrayList(todosLocais.stream().filter(it -> it.getAndar().equals(newValue)).map(VSdLocalExpedicao::getRua).distinct().collect(Collectors.toList())));
        });
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Filter">
    private final FormFieldComboBox<String> andarComboFilter = FormFieldComboBox.create(String.class, field -> {
        field.items(FXCollections.observableArrayList(Arrays.asList("Todos", "0", "1")));
        field.select(0);
        field.title("Andar");
    });

    private final FormFieldComboBox<String> ruaFilter = FormFieldComboBox.create(String.class, field -> {
        field.title("Rua");
        List<String> ruas = ((List<VSdLocalExpedicao>) new FluentDao().selectFrom(VSdLocalExpedicao.class).get().resultList()).stream().map(VSdLocalExpedicao::getRua).distinct().sorted().collect(Collectors.toList());
        ruas.add(0, "Todas");
        field.items(FXCollections.observableArrayList(ruas));
        field.select(0);
    });

    private final FormFieldComboBox<String> posicaoFilter = FormFieldComboBox.create(String.class, field -> {
        List<String> posicoes = ((List<VSdLocalExpedicao>) new FluentDao().selectFrom(VSdLocalExpedicao.class).get().resultList()).stream().map(VSdLocalExpedicao::getPosicao).distinct().sorted().collect(Collectors.toList());
        posicoes.add(0, "Todas");
        field.items(FXCollections.observableArrayList(posicoes));
        field.title("Posição");
        field.select(0);
    });

    private final FormFieldComboBox<String> ladoComboFilter = FormFieldComboBox.create(String.class, field -> {
        field.items(FXCollections.observableArrayList(Arrays.asList("Ambos", "E", "D")));
        field.select(0);
        field.title("Lado");
    });

    private final FormFieldComboBox<String> statusLocal = FormFieldComboBox.create(String.class, field -> {
        field.items(FXCollections.observableArrayList(Arrays.asList("Todos", "S/Estoque", "C/Estoque", "S/Prod")));
        field.select(0);
        field.title("Status");
    });

    private final FormFieldMultipleFind<VSdDadosProduto> produtoFilter = FormFieldMultipleFind.create(VSdDadosProduto.class, field -> {
        field.width(150);
        field.title("Produto");
        field.toUpper();
    });

    private final FormFieldMultipleFind<Cor> corFilter = FormFieldMultipleFind.create(Cor.class, field -> {
        field.width(150);
        field.title("Cor");
    });
    // </editor-fold>

    public VizualizacaoEstoqueView() {
        super("Vizualização Estoque", ImageUtils.getImage(ImageUtils.Icon.DEPOSITO), new String[]{"Listagem", "Manutenção", "Vizualização"});
        initListagem();
        initManutencao();
        super.tabs.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (super.tabs.getTabs().indexOf(newValue) == 2 && vizualizacaoTab.getChildren().size() == 0) {
                initVizualizacao();
            }
        });
    }

    private void initListagem() {
        listagemTab.getChildren().add(FormBox.create(principal -> {
            principal.vertical();
            principal.expanded();
            principal.add(FormBox.create(boxHeader -> {
                boxHeader.horizontal();
                boxHeader.add(FormBox.create(boxTitledFilter -> {
                    boxTitledFilter.vertical();
                    boxTitledFilter.add(FormTitledPane.create(filter -> {
                        filter.filter();
                        filter.add(FormBox.create(boxFilter -> {
                            boxFilter.vertical();
                            boxFilter.add(FormBox.create(linha1 -> {
                                linha1.horizontal();
                                linha1.add(andarComboFilter.build(), ruaFilter.build(), posicaoFilter.build(), ladoComboFilter.build());
                            }));
                            boxFilter.add(FormBox.create(linha2 -> {
                                linha2.horizontal();
                                linha2.add(produtoFilter.build(), corFilter.build(), statusLocal.build());
                            }));
                        }));
                        filter.find.setOnAction(evt -> {
                            new RunAsyncWithOverlay(this).exec(task -> {
                                buscarLocais(andarComboFilter.value.getValue(),
                                        ruaFilter.value.getValue(),
                                        posicaoFilter.value.getValue(),
                                        ladoComboFilter.value.getValue(),
                                        statusLocal.value.getValue(),
                                        produtoFilter.objectValues.stream().map(it -> it.getCodigo()).toArray(),
                                        corFilter.objectValues.stream().map(it -> it.getCor()).toArray()
                                );
                                return ReturnAsync.OK.value;
                            }).addTaskEndNotification(taskReturn -> {
                                if (taskReturn.equals(ReturnAsync.OK.value)) {
                                    locaisBean.set(FXCollections.observableArrayList(locais));
                                }
                            });
                        });
                        filter.clean.setOnAction(evt -> {
                            andarComboFilter.select(0);
                            ruaFilter.clear();
                            posicaoFilter.clear();
                            ladoComboFilter.select(0);
                            produtoFilter.clear();
                            corFilter.clear();
                        });
                    }));
                }));
                boxHeader.add(FormBox.create(boxLegenda -> {
                    boxLegenda.horizontal();
                    boxLegenda.title("Legenda");
                    boxLegenda.add(FormBox.create(col1 -> {
                        col1.vertical();
                        col1.alignment(Pos.CENTER);
                        col1.add(FormBox.create(l1 -> {
                            l1.horizontal();
                            l1.alignment(Pos.CENTER);
                            l1.height(27);
                            l1.add(FormBox.create(boxCor -> {
                                boxCor.horizontal();
                                boxCor.alignment(Pos.CENTER);
                                boxCor.add(new Circle(8, Color.LIGHTGREEN));
                            }));
                        }));
                        col1.add(FormBox.create(l2 -> {
                            l2.horizontal();
                            l2.alignment(Pos.CENTER);
                            l2.height(27);
                            l2.add(FormBox.create(boxCor -> {
                                boxCor.horizontal();
                                boxCor.alignment(Pos.CENTER);
                                boxCor.add(new Circle(8, Color.LIGHTGOLDENRODYELLOW));
                            }));
                        }));
                        col1.add(FormBox.create(l3 -> {
                            l3.horizontal();
                            l3.alignment(Pos.CENTER);
                            l3.height(27);
                            l3.add(FormBox.create(boxCor -> {
                                boxCor.horizontal();
                                boxCor.alignment(Pos.CENTER);
                                boxCor.add(new Circle(8, Color.INDIANRED));
                            }));
                        }));
                    }));
                    boxLegenda.add(FormBox.create(col2 -> {
                        col2.vertical();
                        col2.alignment(Pos.CENTER);
                        col2.add(FormBox.create(l1 -> {
                            l1.horizontal();
                            l1.alignment(Pos.CENTER);
                            l1.add(FormBox.create(boxText -> {
                                boxText.horizontal();
                                boxText.alignment(Pos.CENTER);
                                boxText.add(FormLabel.create(lbl -> lbl.setText("Produtos com Estoque")));
                            }));
                        }));
                        col2.add(FormBox.create(l2 -> {
                            l2.horizontal();
                            l2.alignment(Pos.CENTER);
                            l2.add(FormBox.create(boxText -> {
                                boxText.horizontal();
                                boxText.alignment(Pos.CENTER);
                                boxText.add(FormLabel.create(lbl -> lbl.setText("Produtos sem Estoque")));
                            }));
                        }));
                        col2.add(FormBox.create(l3 -> {
                            l3.horizontal();
                            l3.alignment(Pos.CENTER);
                            l3.add(FormBox.create(boxText -> {
                                boxText.horizontal();
                                boxText.alignment(Pos.CENTER);
                                boxText.add(FormLabel.create(lbl -> lbl.setText("Sem Produto")));
                            }));
                        }));
                    }));
                }));
            }));
            principal.add(FormBox.create(boxCenter -> {
                boxCenter.horizontal();
                boxCenter.expanded();
                boxCenter.add(tblLocalExpedicao.build());
            }));
        }));
    }

    private void initManutencao() {
        todosLocais = (List<VSdLocalExpedicao>) new FluentDao().selectFrom(VSdLocalExpedicao.class).get().resultList();
        manutencaoTab.getChildren().add(navegation);
        manutencaoTab.getChildren().add(FormBox.create(principal -> {
            principal.vertical();
            principal.expanded();
            principal.add(andarField.build(), ruaField.build(), posicaoField.build(), ladoField.build());
        }));
    }

    private void initVizualizacao() {
        new RunAsyncWithOverlay(this).exec(task -> ReturnAsync.OK.value).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                Map<String, Map<String, Map<String, List<VSdLocalExpedicao>>>> collect = ((List<VSdLocalExpedicao>) new FluentDao().selectFrom(VSdLocalExpedicao.class).get().resultList())
                        .stream().collect(Collectors.groupingBy(VSdLocalExpedicao::getRua, Collectors.groupingBy(VSdLocalExpedicao::getPosicao, Collectors.groupingBy(VSdLocalExpedicao::getLado))));
                vizualizacaoTab.getChildren().add(
                        FormBox.create(principal -> {
                            principal.expanded();
                            principal.horizontal();
                            principal.add(FormBadges.create(bg -> {
                                bg.node(FormTitledPane.create(boxLegenda -> {
                                    boxLegenda.title("Legenda");
                                    boxLegenda.add(FormBox.create(linha -> {
                                        linha.horizontal();
                                        linha.add(FormBox.create(boxE -> {
                                            boxE.horizontal();
                                            boxE.width(15);
                                            boxE.alignment(Pos.CENTER);
                                            boxE.add(new Circle(7,Color.AQUA));
                                        }));
                                        linha.add(FormBox.create(boxD -> {
                                            boxD.horizontal();
                                            boxD.add(FormLabel.create(txt -> txt.setText("Jeans Masculino")));
                                        }));
                                    }));
                                    boxLegenda.add(FormBox.create(linha -> {
                                        linha.horizontal();
                                        linha.add(FormBox.create(boxE -> {
                                            boxE.horizontal();
                                            boxE.width(15);
                                            boxE.alignment(Pos.CENTER);
                                            boxE.add(new Circle(7,Color.LEMONCHIFFON));
                                        }));
                                        linha.add(FormBox.create(boxD -> {
                                            boxD.horizontal();
                                            boxD.add(FormLabel.create(txt -> txt.setText("Jeans Feminino")));
                                        }));
                                    }));
                                    boxLegenda.add(FormBox.create(linha -> {
                                        linha.horizontal();
                                        linha.add(FormBox.create(boxE -> {
                                            boxE.horizontal();
                                            boxE.width(15);
                                            boxE.alignment(Pos.CENTER);
                                            boxE.add(new Circle(7,Color.DARKSEAGREEN));
                                        }));
                                        linha.add(FormBox.create(boxD -> {
                                            boxD.horizontal();
                                            boxD.add(FormLabel.create(txt -> txt.setText("Malha Masculina")));
                                        }));
                                    }));
                                    boxLegenda.add(FormBox.create(linha -> {
                                        linha.horizontal();
                                        linha.add(FormBox.create(boxE -> {
                                            boxE.horizontal();
                                            boxE.width(15);
                                            boxE.alignment(Pos.CENTER);
                                            boxE.add(new Circle(7,Color.MEDIUMPURPLE));
                                        }));
                                        linha.add(FormBox.create(boxD -> {
                                            boxD.horizontal();
                                            boxD.add(FormLabel.create(txt -> txt.setText("Malha Feminina")));
                                        }));
                                    }));
                                    boxLegenda.add(FormBox.create(linha -> {
                                        linha.horizontal();
                                        linha.add(FormBox.create(boxE -> {
                                            boxE.horizontal();
                                            boxE.width(15);
                                            boxE.alignment(Pos.CENTER);
                                            boxE.add(new Circle(7,Color.DIMGRAY));
                                        }));
                                        linha.add(FormBox.create(boxD -> {
                                            boxD.horizontal();
                                            boxD.add(FormLabel.create(txt -> txt.setText("Marketing")));
                                        }));
                                    }));
                                    boxLegenda.add(FormBox.create(linha -> {
                                        linha.horizontal();
                                        linha.add(FormBox.create(boxE -> {
                                            boxE.horizontal();
                                            boxE.width(15);
                                            boxE.alignment(Pos.CENTER);
                                            boxE.add(new Circle(7,Color.INDIANRED));
                                        }));
                                        linha.add(FormBox.create(boxD -> {
                                            boxD.horizontal();
                                            boxD.add(FormLabel.create(txt -> txt.setText("Sem Produtos")));
                                        }));
                                    }));
                                }));
                                bg.position(Pos.BOTTOM_RIGHT, 10.0);
                                bg.badgedNode(FormBox.create(boxPrinci -> {
                                    boxPrinci.expanded();
                                    boxPrinci.horizontal();
                                    boxPrinci.horizontalScroll();
                                    collect.forEach((rua, mapPosicao) -> boxPrinci.add(FormBox.create(ruaBox -> {
                                        ruaBox.vertical();
                                        ruaBox.title(rua);
                                        ruaBox.setPadding(new Insets(-2));
                                        boolean invertido = mapPosicao.entrySet().stream().findFirst().get().getValue().entrySet().stream().findFirst().get().getValue().get(0).getIndice() % 2 != 0;

                                        Map<String, Map<String, List<VSdLocalExpedicao>>> sortedPosicaoMap;
                                        if (invertido) {
                                            sortedPosicaoMap = mapPosicao.entrySet().stream().sorted(Map.Entry.comparingByKey(Comparator.reverseOrder())).collect(Collectors.toMap(
                                                    Map.Entry::getKey,
                                                    Map.Entry::getValue,
                                                    (oldValue, newValue) -> oldValue, LinkedHashMap::new
                                            ));
                                        } else {
                                            sortedPosicaoMap = mapPosicao.entrySet().stream().sorted(Map.Entry.comparingByKey()).collect(Collectors.toMap(
                                                    Map.Entry::getKey,
                                                    Map.Entry::getValue,
                                                    (oldValue, newValue) -> oldValue, LinkedHashMap::new
                                            ));
                                        }
                                        sortedPosicaoMap.forEach((posicao, mapLado) -> ruaBox.add(FormBox.create(boxPosicao -> {
                                            boxPosicao.horizontal();
                                            boxPosicao.setMaxHeight(37);
                                            boxPosicao.setPadding(new Insets(-2));
                                            if (!mapLado.containsKey("E")) {
                                                mapLado.put("E", new ArrayList<>(Collections.singletonList(new VSdLocalExpedicao("E", "S", "S COR"))));
                                            }
                                            if (!mapLado.containsKey("D")) {
                                                mapLado.put("D", new ArrayList<>(Collections.singletonList(new VSdLocalExpedicao("D", "S", "S COR"))));
                                            }

                                            Map<String, List<VSdLocalExpedicao>> sortedLadoMap;
                                            if (invertido) {
                                                sortedLadoMap = mapLado.entrySet().stream().sorted(Map.Entry.comparingByKey(Comparator.reverseOrder())).collect(Collectors.toMap(
                                                        Map.Entry::getKey,
                                                        Map.Entry::getValue,
                                                        (oldValue, newValue) -> oldValue, LinkedHashMap::new
                                                ));
                                            } else {
                                                sortedLadoMap = mapLado.entrySet().stream().sorted(Map.Entry.comparingByKey()).collect(Collectors.toMap(
                                                        Map.Entry::getKey,
                                                        Map.Entry::getValue,
                                                        (oldValue, newValue) -> oldValue, LinkedHashMap::new
                                                ));
                                            }

                                            sortedLadoMap.forEach((lado, list) -> boxPosicao.add(FormBox.create(boxLado -> {
                                                boxLado.width(35);
                                                boxLado.alignment(Pos.CENTER);
                                                boxLado.vertical();
                                                boxLado.border();
                                                boxLado.setPadding(new Insets(-2));

                                                Popup popup = new Popup();
                                                popup.getContent().add(FormBox.create(popupPrincipal -> {
                                                    popupPrincipal.vertical();
                                                    popupPrincipal.expanded();

                                                    for (VSdLocalExpedicao produto : list) {
                                                        popupPrincipal.add(FormBox.create(boxPane -> {
                                                            boxPane.vertical();
                                                            boxPane.expanded();
                                                            boxPane.border();
                                                            boxPane.setBackground(new Background(new BackgroundFill(getColorField(produto), CornerRadii.EMPTY, Insets.EMPTY)));
                                                            boxPane.alignment(Pos.CENTER);
                                                            boxPane.add(FormLabel.create(lbl -> {
                                                                lbl.alignment(Pos.CENTER);
                                                                lbl.setFont(javafx.scene.text.Font.font("Verdana", 10));
                                                                lbl.setPadding(new Insets(-5));
                                                                lbl.value.setValue(rua + " - " + posicao + " - " + lado);
                                                            }));
                                                            boxPane.add(FormLabel.create(lbl -> {
                                                                lbl.alignment(Pos.CENTER);
                                                                lbl.setFont(javafx.scene.text.Font.font("Verdana", FontWeight.BOLD, 20));
                                                                lbl.setPadding(new Insets(-5, 0, -5, 0));
                                                                lbl.value.setValue(produto.getProduto());
                                                            }));
                                                            boxPane.add(FormLabel.create(lbl -> {
                                                                lbl.alignment(Pos.CENTER);
                                                                lbl.setFont(javafx.scene.text.Font.font("Verdana", FontPosture.ITALIC, 10));
                                                                lbl.setPadding(new Insets(-3));
                                                                lbl.value.setValue(produto.getCor());
                                                            }));
                                                            boxPane.add(FormLabel.create(lbl -> {
                                                                lbl.alignment(Pos.CENTER);
                                                                lbl.setFont(javafx.scene.text.Font.font("Verdana", FontWeight.BOLD, FontPosture.ITALIC, 10));
                                                                lbl.setPadding(new Insets(-1));
                                                                lbl.value.setValue("Qtde: " + produto.getEstoque().toString());
                                                            }));
                                                        }));
                                                    }
                                                }));

                                                boxLado.hoverProperty().addListener((observable, oldValue, newValue) -> {
                                                    Point mouseLocation = MouseInfo.getPointerInfo().getLocation();
                                                    popup.setX(mouseLocation.getX());
                                                    popup.setY(mouseLocation.getY());
                                                    if (newValue && !list.get(0).getProduto().equals("S"))
                                                        popup.show(boxLado, mouseLocation.getX(), mouseLocation.getY());
                                                    else popup.hide();
                                                });

                                                if (list.size() > 0) {
                                                    if (!list.get(0).getProduto().equals("S") && list.get(0).isAtivo()) {
                                                        boxLado.add(FormLabel.create(lbl -> {
                                                            lbl.alignment(Pos.CENTER);
                                                            lbl.setFont(javafx.scene.text.Font.font("Verdana", FontWeight.BOLD, 8.3));
                                                            lbl.setPadding(new Insets(0, -5, 0, -5));
                                                            lbl.value.setValue(posicao + "-" + lado);
                                                        }));
                                                        boxLado.setBackground(new Background(new BackgroundFill(getColorField(list.get(0)), CornerRadii.EMPTY, Insets.EMPTY)));
                                                    }
                                                }
                                            })));
                                        })));
                                    })));
                                }));
                            }));
                        })
                );
            }
        });

    }

    private Color getColorField(VSdLocalExpedicao local) {

        if (local.getProduto().equals("S")) return Color.TRANSPARENT;
        if (local.getProduto().equals("S PROD")) return Color.INDIANRED;
        if (local.getTipoLinha().equals("J")) {
            return local.getGenero().equals("M") ? Color.AQUA: Color.LEMONCHIFFON;
        } else if (local.getTipoLinha().equals("M")){
         return local.getGenero().equals("M") ? Color.DARKSEAGREEN : Color.MEDIUMPURPLE;
        } else return Color.DIMGRAY;
    }

    private void novoCadastro() {
        navegation.selectedItem.setValue(new VSdLocalExpedicaoQuant());
        limparCampos();
        navegation.inEdition.set(true);
    }

    private void limparCampos() {

    }

    private void editarCadastro(VSdLocalExpedicaoQuant local) {
        if (local != null) {
            navegation.inEdition.set(true);
            abrirCadastro(local);
            MessageBox.create(message -> {
                message.message("Ativado modo edição");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
        }
    }

    private void abrirCadastro(VSdLocalExpedicaoQuant local) {
        navegation.selectedItem.set(local);
        tabs.getSelectionModel().select(1);
        carregaDados(local);
    }

    private void excluirCadastro(VSdLocalExpedicaoQuant local) {
    }

    private void salvarCadastro() {
    }

    private void carregaDados(VSdLocalExpedicaoQuant local) {
    }

    private void cancelarCadastro() {
    }

}
