package sysdeliz2.views.expedicao.minuta;

import javafx.application.Platform;
import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.util.Callback;
import sysdeliz2.controllers.views.expedicao.MinutaController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.sysdeliz.SdGradeItemPedRem;
import sysdeliz2.models.ti.PaIten;
import sysdeliz2.models.view.*;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.SysLogger;
import sysdeliz2.views.expedicao.MinutaView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CadastroLocal extends MinutaController {

    private final MinutaView containerView;

    private VSdDadosProdutoBarra produtoSelecionado;

    private final BooleanProperty lendoLocal = new SimpleBooleanProperty(false);

    private List<VsdDadosProdutoLocal> produtos = new ArrayList<>();

    private final ListProperty<VSdLocalProduto> locaisLivresBean = new SimpleListProperty<>();

    private final FormTableView<VSdLocalProduto> tblLocaisLivres = FormTableView.create(VSdLocalProduto.class, table -> {
        table.withoutHeader();
        table.items.bind(locaisLivresBean);
        table.addStyle("large-scrollbar-tableview");
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Rua");
                    cln.width(90);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdLocalProduto, VSdLocalProduto>, ObservableValue<VSdLocalProduto>>) param -> new ReadOnlyObjectWrapper(param.getValue().getRua()));
                }).build(), /*Rua*/
                FormTableColumn.create(cln -> {
                    cln.title("Local");
                    cln.width(120);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdLocalProduto, VSdLocalProduto>, ObservableValue<VSdLocalProduto>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLocal()));
                }).build() /*Rua*/
        );
    });

    //<editor-fold desc="Components">
    private final FormFieldText barraField = FormFieldText.create(field -> {
        field.title("Barra");
        field.editable.bind(lendoLocal.not());
        field.keyReleased(event -> {
            if (event.getCode().equals(KeyCode.ENTER) && !field.value.getValueSafe().equals("")) {
                buscarBarraProduto(field);
            }
        });
    });

    private final FormFieldText localLeituraField = FormFieldText.create(field -> {
        field.title("Local Novo");
        field.editable.bind(lendoLocal);
        field.toUpper();
        field.keyReleased(evt -> {
            if (evt.getCode().equals(KeyCode.ENTER) && !field.value.getValueSafe().equals("")) {
                confirmarAlteracaoLocal();
            }
        });
    });

    private final FormFieldText produtoField = FormFieldText.create(field -> {
        field.title("Código");
        field.editable.set(false);
    });

    private final FormFieldText corField = FormFieldText.create(field -> {
        field.title("Cor");
        field.editable.set(false);
        field.width(70);
    });

    private final FormFieldText marcaField = FormFieldText.create(field -> {
        field.title("Marca");
        field.editable.set(false);
        field.width(75);
    });

    private final FormFieldText tamField = FormFieldText.create(field -> {
        field.title("Tam");
        field.alignment(Pos.CENTER);
        field.editable.set(false);
        field.setVisible(false);
        field.width(75);
    });

    private final FormFieldText entregaField = FormFieldText.create(field -> {
        field.title("Entrega");
        field.editable.set(false);
        field.width(100);
    });

    private final FormFieldText colecaoField = FormFieldText.create(field -> {
        field.title("Coleção");
        field.editable.set(false);
        field.width(50);
    });

    private final FormFieldText descricaoField = FormFieldText.create(field -> {
        field.title("Descrição");
        field.editable.set(false);
        field.width(170);
    });

    private final FormFieldText gradeField = FormFieldText.create(field -> {
        field.title("Grade");
        field.editable.set(false);
        field.alignment(Pos.CENTER);
    });

    private final FormFieldText localAtualField = FormFieldText.create(field -> {
        field.title("Local Atual");
        field.editable.set(false);
        field.alignment(Pos.CENTER);
    });

    private final FormFieldText andarField = FormFieldText.create(field -> {
        field.title("Andar");
        field.editable.set(false);
        field.setVisible(false);
        field.width(70);
    });

    private final FormFieldComboBox<String> comboBoxAndar = FormFieldComboBox.create(String.class, field -> {
        field.title("Andar");
        field.width(90);
        field.getSelectionModel((observable, oldValue, newValue) -> {
            carregarLocaisLivresAndar(newValue);
        });
    });

    private final FormFieldComboBox<String> comboBoxRua = FormFieldComboBox.create(String.class, field -> {
        field.title("Rua");
        field.width(90);
        field.getSelectionModel((observable, oldValue, newValue) -> {
            carregaLocaisLivresRua(newValue);
        });
    });

    private final Pagination pagination = new Pagination();
    //</editor-fold>

    //<editor-fold desc="Buttons">
    private final Button btnConfirmar = FormButton.create(btn -> {
        btn.addStyle("success");
        btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._32));
        btn.setOnAction(evt -> {
            confirmarAlteracaoLocal();
        });
    });

    private final Button btnCancelar = FormButton.create(btn -> {
        btn.addStyle("danger");
        btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._32));
        btn.setOnAction(evt -> {
            cancelarLocal();
        });
    });

    private final Button btnVoltar = FormButton.create(btnVoltar -> {
        btnVoltar.title("Retornar");
        btnVoltar.icon(ImageUtils.getIcon(ImageUtils.Icon.RETORNAR, ImageUtils.IconSize._24));
        btnVoltar.addStyle("warning");
        btnVoltar.setAction(evt -> init());
    });
    //</editor-fold>

    private FormBox boxLeituraBarra = FormBox.create(boxBarraLocal -> {
        boxBarraLocal.vertical();
        boxBarraLocal.add(FormFieldText.create(field -> {
            field.withoutTitle();
            field.label(ImageUtils.getIcon(ImageUtils.Icon.LEITURA_BARRA, ImageUtils.IconSize._24));
            field.keyReleased(evt -> {
                if (evt.getCode().equals(KeyCode.ENTER) && !field.value.getValueSafe().equals("")) {
                    carregaLocal(field.value.getValueSafe());
                    field.clear();
                }
            });
        }).build());
    });

    public CadastroLocal(MinutaView containerView) {
        this.containerView = containerView;
        init();
    }

    private void init() {
        containerView.getChildren().clear();
        containerView.getChildren().add(FormBox.create(content -> {
            content.vertical();
            content.expanded();
            content.alignment(Pos.CENTER);
            content.setPadding(new Insets(7));
            content.add(FormButton.create(btn -> {
                btn.title("Cadastrar Local");
                btn.addStyle("success").addStyle("lg");
                btn.width(230);
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._32));
                btn.setOnAction(evt -> initCadastroLocal());
            }));
            content.add(FormButton.create(btn -> {
                btn.title("Ler Local");
                btn.addStyle("primary").addStyle("lg");
                btn.width(230);
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.LEITURA_BARRA, ImageUtils.IconSize._32));
                btn.setOnAction(evt -> initLeituraLocal());
            }));
            content.add(FormButton.create(btn -> {
                btn.title("Locais Livres");
                btn.addStyle("dark").addStyle("lg");
                btn.width(230);
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.LOCAL, ImageUtils.IconSize._32));
                btn.setOnAction(evt -> initLocaisLivres());
            }));
            content.add(FormButton.create(btn -> {
                btn.title("Limpar Local");
                btn.addStyle("danger").addStyle("lg");
                btn.width(230);
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._32));
                btn.setOnAction(evt -> initDeleteLocal());
            }));
            content.add(FormButton.create(btn -> {
                btn.title("Retornar");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.RETORNAR, ImageUtils.IconSize._24));
                btn.addStyle("warning");
                btn.setOnAction(evt -> containerView.init());
            }));
        }));
    }

    private void initLocaisLivres() {
        containerView.getChildren().clear();
        containerView.getChildren().add(FormBox.create(container -> {
            container.vertical();
            container.expanded();
            container.setPadding(new Insets(7));
            container.add(FormBox.create(header -> {
                header.horizontal();
                List<VSdLocalProduto> locais = (List<VSdLocalProduto>) new FluentDao().selectFrom(VSdLocalProduto.class).get().resultList();
                List<String> ruas = locais.stream().map(VSdLocalProduto::getRua).distinct().sorted().collect(Collectors.toList());
                List<String> andares = locais.stream().map(VSdLocalProduto::getPiso).distinct().sorted().collect(Collectors.toList());

                ruas.add(0, "Todas");
                andares.add(0, "Todos");
                comboBoxRua.items(FXCollections.observableArrayList(ruas));
                comboBoxAndar.items(FXCollections.observableArrayList(andares));
                comboBoxRua.select("Todas");
                comboBoxAndar.select("Todos");
                header.add(comboBoxAndar.build());
                header.add(comboBoxRua.build());
            }));
            container.add(FormBox.create(content -> {
                content.vertical();
                content.expanded();
                carregaLocaisLivresRua("Todas");
                content.add(tblLocaisLivres.build());
            }));
            container.add(btnVoltar);
        }));
    }

    private void initDeleteLocal() {
        containerView.getChildren().clear();
        containerView.getChildren().add(FormBox.create(principal -> {
            principal.vertical();
            principal.expanded();
            principal.setPadding(new Insets(7));
            principal.add(boxLeituraBarra);
            principal.add(pagination);
            principal.add(FormBox.create(boxFooter -> {
                boxFooter.vertical();
                boxFooter.alignment(Pos.BOTTOM_CENTER);
                boxFooter.add(FormBox.create(buttonsBox -> {
                    buttonsBox.horizontal();
                    buttonsBox.add(btnVoltar);
                    buttonsBox.add(FormButton.create(btn -> {
                        btn.addStyle("danger");
                        btn.title("Limpar Local");
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
                        btn.setOnAction(evt -> {
                            if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                                message.message("Tem certeza?");
                                message.showFullScreenMobile();
                            }).value.get())) {
                                limparLocal();
                            }
                        });
                    }));
                }));
            }));
            loadPagination();
        }));
    }

    private void initLeituraLocal() {
        containerView.getChildren().clear();
        containerView.getChildren().add(FormBox.create(principal -> {
            principal.vertical();
            principal.expanded();
            principal.alignment(Pos.CENTER);
            principal.setPadding(new Insets(7));
            principal.add(boxLeituraBarra);
            principal.add(pagination);
            principal.add(FormBox.create(boxFooter -> {
                boxFooter.vertical();
                boxFooter.alignment(Pos.BOTTOM_CENTER);
                boxFooter.add(FormBox.create(buttonsBox -> {
                    buttonsBox.horizontal();
                    buttonsBox.add(btnVoltar);
                }));
            }));
            loadPagination();
        }));
    }

    private void initCadastroLocal() {
        containerView.getChildren().clear();
        containerView.getChildren().add(FormBox.create(principal -> {
            principal.vertical();
            principal.expanded();
            principal.setPadding(new Insets(7));
            principal.add(FormBox.create(boxLeitura -> {
                boxLeitura.horizontal();
                boxLeitura.add(barraField.build());
                boxLeitura.add(localLeituraField.build());
            }));
            principal.add(new Separator(Orientation.HORIZONTAL));
            principal.add(FormBox.create(produtoLidoBox -> {
                produtoLidoBox.horizontal();
                produtoLidoBox.add(produtoField.build(), corField.build(), tamField.build());
            }));
            principal.add(FormBox.create(boxGrade -> {
                boxGrade.vertical();
                boxGrade.add(gradeField.build());
            }));
            principal.add(FormBox.create(boxLocalAtual -> {
                boxLocalAtual.vertical();
                boxLocalAtual.add(localAtualField.build());
            }));
            principal.add(FormBox.create(boxBtns -> {
                boxBtns.horizontal();
                boxBtns.alignment(Pos.BOTTOM_CENTER);
                boxBtns.add(FormButton.create(btn -> {
                    btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                    btn.addStyle("warning");
                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.RETORNAR, ImageUtils.IconSize._32));
                    btn.setOnAction(evt -> {
                        init();
                    });
                }));
                boxBtns.add(btnCancelar, btnConfirmar);
            }));
        }));
        Platform.runLater(barraField::requestFocus);
    }

    private void limparLocal() {
        for (VsdDadosProdutoLocal produto : produtos) {
            List<PaIten> itens = (List<PaIten>) new FluentDao().selectFrom(PaIten.class).where(it -> it.equal("id.codigo", produto.getCodigo()).equal("id.cor", produto.getCor())).resultList();
            if (itens != null && itens.size() == 0) continue;
            if (produto.getTam() != null && !produto.getTam().equals(""))
                itens = itens.stream().filter(it -> it.getId().getTam().equals(produto.getTam())).collect(Collectors.toList());
            for (PaIten iten : itens) {
                iten.setLocal(null);
                new FluentDao().merge(iten);
                SysLogger.addSysDelizLog("Cadastro Local", TipoAcao.EXCLUIR, iten.getId().getCodigo() + "-" + iten.getId().getCor() + "-" + iten.getId().getTam(),
                        "Local do produto " + iten.getId().getCodigo() + "-" + iten.getId().getCor() + "-" + iten.getId().getTam() + " excluído!");
            }
        }

        MessageBox.create(message -> {
            message.message("Local limpo!");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.showFullScreenMobile();
        });
        localAtualField.clear();
        andarField.clear();
    }

    private void loadPagination() {
        pagination.setPageCount(produtos.size() == 0 ? 1 : produtos.size());
        pagination.setMaxPageIndicatorCount(4);

        pagination.currentPageIndexProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                carregaCampos(produtos.get(newValue.intValue()));
            }
        });

        pagination.setPageFactory((pageIndex) -> FormBox.create(principal -> {
            principal.vertical();
            principal.expanded();
            principal.add(FormBox.create(boxLocal -> {
                boxLocal.horizontal();
                boxLocal.add(localAtualField.build(), andarField.build(), entregaField.build());
            }));
            principal.add(FormBox.create(boxProd -> {
                boxProd.horizontal();
                boxProd.add(produtoField.build(), corField.build(), tamField.build(), marcaField.build());
            }));
            principal.add(FormBox.create(boxDesc -> {
                boxDesc.horizontal();
                boxDesc.add(colecaoField.build(), descricaoField.build());
            }));
        }));

    }

    private void carregaLocal(String valor) {
        produtos.clear();
        limparCampos();
        if (valor.length() > 11) {
            VsdDadosProdutoLocal produto = new FluentDao().selectFrom(VsdDadosProdutoLocal.class).where(it -> it.equal("barra28", valor.substring(0, 6))).singleResult();
            if (produto != null) produtos.add(produto);
        } else {
            List<VsdDadosProdutoLocal> produtoList = (List<VsdDadosProdutoLocal>) new FluentDao().selectFrom(VsdDadosProdutoLocal.class).where(it -> it.equal("local", valor)).resultList();
            if (produtoList != null && produtoList.size() > 0) produtos.addAll(produtoList);
        }

        if (produtos.size() == 0) return;

        if (valor.length() < 11 && Character.isDigit(valor.charAt(valor.length() - 1))) {
            tamField.setVisible(true);
            andarField.setVisible(true);
        }

        pagination.setPageCount(produtos.size());
        pagination.setCurrentPageIndex(0);
        carregaCampos(produtos.get(0));
    }

    private void carregaCampos(VsdDadosProdutoLocal produto) {
        produtoField.value.setValue(produto.getCodigo());
        corField.value.setValue(produto.getCor());
        colecaoField.value.setValue(produto.getColecao());
        andarField.value.setValue(String.valueOf(produto.getLocal().charAt(produto.getLocal().length() - 1)));
        tamField.value.setValue(produto.getTam() == null ? "" : produto.getTam());
        localAtualField.value.setValue(produto.getLocal());
        entregaField.value.setValue(produto.getEntrega());
        marcaField.value.setValue(produto.getMarca());
        descricaoField.value.setValue(produto.getDescricao());
    }

    private void confirmarAlteracaoLocal() {
        if (produtoSelecionado == null) {
            return;
        }
        String novoLocal = localLeituraField.value.getValueSafe();
        localLeituraField.clear();

        if (novoLocal.equals("")) {
            return;
        }
        if (novoLocal.length() > 10) {
            MessageBox.create(message -> {
                message.message("Local lido invalido!");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showFullScreenMobile();
            });
            return;
        }

        if (!localAtualField.value.getValueSafe().equals("") && !localAtualField.value.getValueSafe().equals("AINSP") && novoLocal.charAt(0) != localAtualField.value.getValue().charAt(0)) {
            List<SdGradeItemPedRem> gradeItens = (List<SdGradeItemPedRem>) new FluentDao().selectFrom(SdGradeItemPedRem.class)
                    .where(it -> it
                            .equal("id.codigo", produtoSelecionado.getCodigo())
                            .notIn("statusitem", new String[] {"X","C"})
                            .equal("id.cor", produtoSelecionado.getCor()))
                    .resultList();

            if(gradeItens != null && gradeItens.size() > 0) {
                MessageBox.create(message -> {
                    message.message("Produto em remessa pendente.");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showFullScreen();
                });
                return;
            }
        }

        if (!localAtualField.value.getValueSafe().equals("-")) {
            if (!((Boolean) QuestionBox.build(Boolean.class, message -> {
                message.message("Alterar Local?");
                message.showFullScreenMobile();
            }).value.get())) {
                return;
            }
        }

        List<PaIten> skus = (List<PaIten>) new FluentDao().selectFrom(PaIten.class)
                .where(it -> it
                        .equal("id.codigo", produtoSelecionado.getCodigo())
                        .equal("id.cor", produtoSelecionado.getCor())
                ).resultList();

        if (skus == null || skus.size() == 0) {
            MessageBox.create(message -> {
                message.message("Cadastro não encontrado!");
                message.type(MessageBox.TypeMessageBox.ERROR);
                message.showFullScreen();
            });
        } else {
            if (novoLocal.equals(localAtualField.value.getValue()) && skus.stream().noneMatch(it -> it.getLocal() == null || it.getLocal().equals(""))) {
                MessageBox.create(message -> {
                    message.message("Local igual.");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showFullScreen();
                });
                return;
            }
            if (Character.isDigit(novoLocal.charAt(novoLocal.length() - 1))) {
                skus = skus.stream().filter(it -> it.getId().getTam().equals(produtoSelecionado.getTam())).collect(Collectors.toList());
            }
            for (PaIten iten : skus) {
                iten.setLocal(novoLocal);
                new FluentDao().merge(iten);
                SysLogger.addSysDelizLog("Cadastro Local", TipoAcao.CADASTRAR, iten.getId().getCodigo() + "-" + iten.getId().getCor() + "-" + iten.getId().getTam(),
                        "Produto " + iten.getId().getCodigo() + "-" + iten.getId().getCor() + "-" + iten.getId().getTam() + " cadastrado no local " + novoLocal);
            }
            localAtualField.value.setValue(novoLocal);

            MessageBox.create(message -> {
                message.message("Cadastrado com sucesso!");
                message.type(MessageBox.TypeMessageBox.SUCCESS);
                message.position(Pos.TOP_RIGHT);
                message.showFullScreen();
            });

        }
        JPAUtils.clearEntity(produtoSelecionado);
        barraField.clear();
        lendoLocal.set(false);
        barraField.requestFocus();
    }

    private void cancelarLocal() {
        limparCampos();
        produtoSelecionado = null;
        lendoLocal.set(false);
        barraField.requestFocus();
    }

    private void limparCampos() {
        barraField.clear();
        localLeituraField.clear();
        produtoField.clear();
        corField.clear();
        gradeField.clear();
        tamField.clear();
        localAtualField.clear();
        colecaoField.clear();
        andarField.clear();
        entregaField.clear();
        marcaField.clear();
        descricaoField.clear();
    }

    private void buscarBarraProduto(FormFieldText field) {
        if (field.value.getValue().length() < 6) {
            return;
        }
        VSdDadosProdutoBarra produto = getDadosProdutoBarra(field.value.getValue());
        if (produto == null) {
            field.clear();
            return;
        }

        produtoSelecionado = produto;
        limparCampos();
        produtoField.value.setValue(produto.getCodigo());
        corField.value.setValue(produto.getCor());
        tamField.value.setValue(produto.getTam());

//        VSdLocalExpedicao local = new FluentDao().selectFrom(VSdLocalExpedicao.class).where(it -> it.equal("produto", produto.getCodigo()).equal("cor", produto.getCor())).singleResult();
        if (produto.getLocal() == null || produto.getLocal().equals("")) {
            localAtualField.value.setValue("-");
        } else {
            localAtualField.value.setValue(produto.getLocal());
        }

        try {
            List<Object> objects = new NativeDAO().runNativeQuery("" +
                    "select listagg(cort.tam, ' ') within group(order by fx.posicao) grade\n" +
                    "  from vproduto_cortam cort\n" +
                    "  join produto_001 prd\n" +
                    "    on prd.codigo = cort.codigo\n" +
                    "  join faixa_iten_001 fx\n" +
                    "    on fx.faixa = prd.faixa\n" +
                    "   and fx.tamanho = cort.tam\n" +
                    " where cort.codigo = '%s'\n" +
                    "   and cort.cor = '%s'", produto.getCodigo(), produto.getCor());

            gradeField.value.setValue(((Map<String, Object>) objects.get(0)).get("GRADE").toString());

        } catch (SQLException | NullPointerException e) {
            e.printStackTrace();
        }

        lendoLocal.set(true);
        localLeituraField.requestFocus();
    }

    private void carregaLocaisLivresRua(Object rua) {
        List<VSdLocalProduto> locais = (List<VSdLocalProduto>) new FluentDao().selectFrom(VSdLocalProduto.class).where(it -> it
                .equal("rua", rua, TipoExpressao.AND, when -> !rua.equals("Todas"))
                .isNull("produto")
        ).orderBy("local", OrderType.ASC).resultList();
        locaisLivresBean.set(FXCollections.observableArrayList(locais));
        tblLocaisLivres.refresh();
    }

    private void carregarLocaisLivresAndar(Object piso) {
        List<VSdLocalProduto> locais = (List<VSdLocalProduto>) new FluentDao().selectFrom(VSdLocalProduto.class).where(it -> it
                .equal("piso", piso, TipoExpressao.AND, when -> !piso.equals("Todos"))
                .isNull("produto")
        ).orderBy("local", OrderType.ASC).resultList();

        List<String> ruas = locais.stream().map(VSdLocalProduto::getRua).distinct().sorted().collect(Collectors.toList());
        ruas.add(0, "Todas");

        comboBoxRua.items(FXCollections.observableArrayList(ruas));
        comboBoxRua.select("Todas");
        locaisLivresBean.set(FXCollections.observableArrayList(locais));
        tblLocaisLivres.refresh();
    }

}
