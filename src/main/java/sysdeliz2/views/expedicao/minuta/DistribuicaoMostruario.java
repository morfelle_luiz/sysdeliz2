package sysdeliz2.views.expedicao.minuta;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Separator;
import javafx.scene.input.KeyCode;
import org.apache.log4j.Logger;
import sysdeliz2.controllers.views.expedicao.MinutaController;
import sysdeliz2.models.view.expedicao.VSdDistribuicaoMostruario;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.FormBox;
import sysdeliz2.utils.gui.components.FormButton;
import sysdeliz2.utils.gui.components.FormFieldText;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsync;
import sysdeliz2.utils.sys.SysLogger;
import sysdeliz2.views.expedicao.MinutaView;

import java.util.Arrays;
import java.util.List;

public class DistribuicaoMostruario extends MinutaController {

    private static final Logger logger = Logger.getLogger(DistribuicaoMostruario.class);
    private MinutaView containerView;

    private final IntegerProperty countCaixasLidas = new SimpleIntegerProperty(0);
    private final IntegerProperty caixasLidas = new SimpleIntegerProperty(0);
    private final ObjectProperty<VSdDistribuicaoMostruario> produtoDistribuicao = new SimpleObjectProperty<>(null);

    private final FormFieldText fieldMostruarioBarcodeProduto = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label(ImageUtils.getIcon(ImageUtils.Icon.QRCODE, ImageUtils.IconSize._32));
        field.addStyle("lg");
        field.expanded();
        field.editable.bind(produtoDistribuicao.isNull());
        field.keyReleased(kvt -> {
            if (kvt.getCode().equals(KeyCode.ENTER)) {
                loadProdutoMostruario(field.value.get());
            }
        });
    });
    private final FormFieldText fieldMostruarioProduto = FormFieldText.create(field -> {
        field.withoutTitle();
        field.width(75.0);
        field.editable.set(false);
    });
    private final FormFieldText fieldMostruarioTipo = FormFieldText.create(field -> {
        field.withoutTitle();
        field.width(60.0);
        field.editable.set(false);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldMostruarioMarca = FormFieldText.create(field -> {
        field.withoutTitle();
        field.expanded();
//        field.width(110.0);
        field.editable.set(false);
    });
    private final FormFieldText fieldMostruarioCaixas = FormFieldText.create(field -> {
        field.withoutTitle();
//        field.expanded();
//        field.alignment(Pos.CENTER);
        field.width(70.0);
        field.editable.set(false);
        field.addStyle("lg");
    });
    private final FormFieldText fieldMostruarioCaixasExcluidas = FormFieldText.create(field -> {
        field.withoutTitle();
        field.expanded();
        field.alignment(Pos.CENTER);
        field.label(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
//        field.width(60.0);
        field.editable.set(false);
    });
    private final FormFieldText fieldMostruarioBarcodeCaixa = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label(ImageUtils.getIcon(ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._32));
        field.addStyle("lg");
//        field.width(140.0);
        field.expanded();
        field.editable.bind(produtoDistribuicao.isNotNull());
        field.keyReleased(kvt -> {
            if (kvt.getCode().equals(KeyCode.ENTER)) {
                apontarCaixaMostruario(field.value.get());
            }
        });
    });
    private final FormFieldText fieldMostruarioCaixasLidas = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label(ImageUtils.getIcon(ImageUtils.Icon.CAIXA, ImageUtils.IconSize._32));
        field.addStyle("lg");
        field.width(90.0);
        field.alignment(Pos.CENTER);
        field.value.bind(countCaixasLidas.asString());
        field.editable.set(false);
    });

    public DistribuicaoMostruario(MinutaView container) {
        this.containerView = container;
        init();
    }

    public void init() {
        containerView.getChildren().clear();
        containerView.getChildren().add(FormBox.create(children -> {
            children.vertical();
            children.expanded();
            children.add(FormBox.create(container -> {
                container.vertical();
                container.expanded();
                container.add(FormBox.create(content -> {
                    content.vertical();
                    content.expanded();
                    content.add(FormBox.create(header -> {
                        header.horizontal();
                        header.add(fieldMostruarioBarcodeProduto.build());
                        header.add(FormButton.create(btnFinalizar -> {
                            btnFinalizar.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._32));
                            btnFinalizar.addStyle("success").addStyle("lg");
                            btnFinalizar.setAction(evt -> finalizarDistribuicaoProduto());
                        }));
                    }));
                    content.add(new Separator(Orientation.HORIZONTAL));
                    content.add(FormBox.create(boxDadosProduto -> {
                        boxDadosProduto.vertical();
                        boxDadosProduto.expanded();
                        boxDadosProduto.add(FormBox.create(boxFields -> {
                            boxFields.horizontal();
                            boxFields.add(fieldMostruarioProduto.build());
                            boxFields.add(fieldMostruarioTipo.build());
                            boxFields.add(fieldMostruarioMarca.build());
                        }));
                        boxDadosProduto.add(FormBox.create(boxFields -> {
                            boxFields.horizontal();
                            boxFields.alignment(Pos.TOP_LEFT);
                            boxFields.add(fieldMostruarioCaixas.build());
                            boxFields.add(fieldMostruarioCaixasExcluidas.build());
                        }));
                        boxDadosProduto.add(FormBox.create(boxFields -> {
                            boxFields.horizontal();
                            boxFields.add(fieldMostruarioBarcodeCaixa.build());
                            boxFields.add(fieldMostruarioCaixasLidas.build());
                        }));
                    }));
                }));
                container.add(new Separator(Orientation.HORIZONTAL));
                container.add(FormButton.create(btnVoltar -> {
                    btnVoltar.title("Retornar");
                    btnVoltar.icon(ImageUtils.getIcon(ImageUtils.Icon.RETORNAR, ImageUtils.IconSize._32));
                    btnVoltar.addStyle("warning");
                    btnVoltar.setAction(evt -> containerView.init());
                }));
            }));
        }));
        fieldMostruarioBarcodeProduto.requestFocus();
    }

    private void loadProdutoMostruario(String barcode) {
        fieldMostruarioCaixas.clear();
        fieldMostruarioCaixasExcluidas.clear();
        fieldMostruarioTipo.clear();
        fieldMostruarioMarca.clear();
        fieldMostruarioBarcodeCaixa.clear();
        fieldMostruarioProduto.clear();

        countCaixasLidas.set(0);
        caixasLidas.set(0);
        produtoDistribuicao.set(null);

        if (barcode.length() < 16) {
            MessageBox.create(message -> {
                message.message("Código de barras: " + barcode + " inválido para produto de mostruário!");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showFullScreen();
            });
            fieldMostruarioBarcodeProduto.clear();
            fieldMostruarioBarcodeProduto.requestFocus();
            return;
        }

        VSdDistribuicaoMostruario produto = getProdutoBarra(barcode);
        if (produto != null) {
            if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                    message.message("Deseja distribuir o produto: "+produto.getCodigo());
                    message.showFullScreenMobile();
                }).value.get())) {
                produtoDistribuicao.set(produto);

                countCaixasLidas.set(Arrays.asList(produtoDistribuicao.get().getDistribuicao().split("\\|")).size());
                fieldMostruarioProduto.setValue(produto.getCodigo());
                fieldMostruarioMarca.setValue(produto.getDescmarca());
                fieldMostruarioTipo.setValue(produto.getDesctipo());
                fieldMostruarioCaixas.setValue(produto.getCaixas());
                fieldMostruarioCaixasExcluidas.setValue(produto.getMenos());
                //fieldMostruarioBarcodeCaixa.requestFocus();

                new RunAsync().exec(task -> {
                    for (String mostruario : produto.getDistribuicao().split("\\|")) {
                        separarProdutoCaixa(Integer.parseInt(mostruario), produto.getColvenda(), produto.getCodigo());
                    }
                    return ReturnAsync.OK.value;
                }).addTaskEndNotification(taskReturn -> {
                    if (taskReturn.equals(ReturnAsync.OK.value)) {

                    }
                });
            }
        } else {
            MessageBox.create(message -> {
                message.message("Produto não encontrado ou já concluído no mostruário!\n" +
                        "Barra: " + barcode);
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showFullScreenMobile();
            });
        }

        produtoDistribuicao.set(null);
        fieldMostruarioBarcodeProduto.clear();
        fieldMostruarioBarcodeProduto.requestFocus();
    }

    private void apontarCaixaMostruario(String barcode) {
        List<String> caixas = Arrays.asList(produtoDistribuicao.get().getDistribuicao().split("\\|"));
        if (!caixas.contains(barcode)) {
            MessageBox.create(message -> {
                message.message("O produto selecionado não se encontra nesta caixa.");
                message.type(MessageBox.TypeMessageBox.BLOCKED);
                message.showFullScreen();
            });
            fieldMostruarioBarcodeCaixa.clear();
            fieldMostruarioBarcodeCaixa.requestFocus();
            return;
        }
        if (countCaixasLidas.get() == 0) {
            MessageBox.create(message -> {
                message.message("Já foram lidas todas as caixas para esse produto");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showFullScreen();
            });
            fieldMostruarioBarcodeCaixa.clear();
            fieldMostruarioBarcodeCaixa.requestFocus();
            return;
        }

        Integer mostruario = Integer.parseInt(barcode);
        if (separarProdutoCaixa(mostruario, produtoDistribuicao.get().getColvenda(),
                produtoDistribuicao.get().getCodigo())) {
            countCaixasLidas.set(countCaixasLidas.subtract(1).get());
            caixasLidas.set(caixasLidas.add(1).get());
        }
        fieldMostruarioBarcodeCaixa.clear();
        fieldMostruarioBarcodeCaixa.requestFocus();
    }

    private void finalizarDistribuicaoProduto() {

        SysLogger.addSysDelizLog("Distribuição Mostruário", TipoAcao.MOVIMENTAR, produtoDistribuicao.get().getCodigo(),
                "Concluindo distribuição de mostruário do produto para " + caixasLidas.get() + " caixas.");

        fieldMostruarioCaixas.clear();
        fieldMostruarioTipo.clear();
        fieldMostruarioMarca.clear();
        fieldMostruarioBarcodeCaixa.clear();
        fieldMostruarioProduto.clear();
        fieldMostruarioCaixasExcluidas.clear();

        countCaixasLidas.set(0);
        caixasLidas.set(0);
        produtoDistribuicao.set(null);
        fieldMostruarioBarcodeProduto.clear();
        fieldMostruarioBarcodeProduto.requestFocus();
    }
}
