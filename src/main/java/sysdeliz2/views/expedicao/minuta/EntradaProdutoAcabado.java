package sysdeliz2.views.expedicao.minuta;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.util.Callback;
import org.apache.log4j.Logger;
import sysdeliz2.controllers.views.expedicao.MinutaController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.sysdeliz.EntradaPA.SdCaixaPA;
import sysdeliz2.models.sysdeliz.EntradaPA.SdItemCaixaPA;
import sysdeliz2.models.sysdeliz.EntradaPA.SdLotePa;
import sysdeliz2.models.sysdeliz.EntradaPA.SdStatusLotePA;
import sysdeliz2.models.ti.PaIten;
import sysdeliz2.utils.FuncoesEntradaPA;
import sysdeliz2.utils.SortUtils;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.SysLogger;
import sysdeliz2.views.expedicao.MinutaView;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

public class EntradaProdutoAcabado extends MinutaController {

    private static final Logger logger = Logger.getLogger(EntradaProdutoAcabado.class);
    private final MinutaView containerView;

    public EntradaProdutoAcabado(MinutaView container) {
        this.containerView = container;
        this.usuario = System.getProperty("user.name");
        init();
    }

    public void init() {
        containerView.getChildren().clear();
        containerView.getChildren().add(FormBox.create(principal -> {
            principal.vertical();
            principal.expanded();
            principal.setPadding(new Insets(7));
            principal.add(FormBox.create(boxMain -> {
                boxMain.vertical();
                boxMain.expanded();
                boxMain.alignment(Pos.CENTER);
                boxMain.add(FormButton.create(btnCriarNova -> {
                    btnCriarNova.title("Recebimento");
                    btnCriarNova.width(250.0);
                    btnCriarNova.icon(ImageUtils.getIcon(ImageUtils.Icon.DEPOSITO, ImageUtils.IconSize._32));
                    btnCriarNova.addStyle("lg").addStyle("primary");
                    btnCriarNova.setAction(evt -> leituraLote());
                }));
                boxMain.add(FormButton.create(btnVoltar -> {
                    btnVoltar.title("Retornar");
                    btnVoltar.icon(ImageUtils.getIcon(ImageUtils.Icon.RETORNAR, ImageUtils.IconSize._32));
                    btnVoltar.addStyle("lg").addStyle("warning");
                    btnVoltar.setAction(evt -> containerView.init());
                }));
            }));
        }));
    }

    //<editor-fold desc="Ações Entrada PA">
    private void leituraLote() {
        containerView.getChildren().clear();

        final FormFieldText fieldCodigoLote = FormFieldText.create(field -> {
            field.title("Lote");
            field.label(ImageUtils.getIcon(ImageUtils.Icon.LEITURA_BARRA, ImageUtils.IconSize._32));
            field.alignment(Pos.CENTER);
            field.toUpper();
            field.addStyle("lg");
        });
        if (new Fragment().show(fragment -> {
            fragment.title("Leitura Lote");
            fragment.size(200.0, 100.0);
            fragment.box.getChildren().add(fieldCodigoLote.build());
            fragment.buttonsBox.getChildren().add(FormButton.create(btnAbrirLote -> {
                fragment.setOnKeyReleased(evt -> {
                    if (evt.getCode().equals(KeyCode.ENTER)) {
                        btnAbrirLote.fire();
                    }
                });
                btnAbrirLote.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                btnAbrirLote.addStyle("success");
                btnAbrirLote.setAction(evt -> {
                    carregarLote(fieldCodigoLote, fragment);
                });
            }));
            fieldCodigoLote.requestFocus();
        }).compareTo(Fragment.ReturnType.CANCEL) == 0) {
            init();
        }
    }

    private void carregarLote(FormFieldText fieldCodigoLote, Fragment fragment) {
        loteEscolhido = new FluentDao().selectFrom(SdLotePa.class).where(eb -> eb.equal("id", fieldCodigoLote.value.get().trim())).singleResult();
        if (loteEscolhido == null) {
            fieldCodigoLote.clear();
            fieldCodigoLote.requestFocus();
            return;
        }

        try {
            JPAUtils.refresh(loteEscolhido);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (loteEscolhido.getStatus().getCodigo() == 1 || loteEscolhido.getStatus().getCodigo() == 2) {
            MessageBox.create(message -> {
                message.message("Lote não está disponível para recebimento ainda.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showFullScreenMobile();
            });
            return;
        }

        if (loteEscolhido.getStatus().getCodigo() == 6) {
            MessageBox.create(message -> {
                message.message("Lote já recebido");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.showFullScreenMobile();
            });
            return;
        }
        loteEscolhido.setStatus(new FluentDao().selectFrom(SdStatusLotePA.class).where(it -> it.equal("codigo", 5)).singleResult());
        loteEscolhido = new FluentDao().merge(loteEscolhido);
        fragment.close();
        recebePerdidasIncompletas();
        carregaCaixas();
    }

    private void carregaCaixas() {
        containerView.getChildren().clear();
        JPAUtils.clearEntity(loteEscolhido);
        loteEscolhido = new FluentDao().selectFrom(SdLotePa.class).where(it -> it.equal("id", loteEscolhido.getId())).singleResult();
        refreshBeansCaixasPa();
        ListProperty<SdCaixaPA> caixasParaLeitura = new SimpleListProperty<>(FXCollections.observableArrayList(
                loteEscolhido.getItensLote()
                        .stream()
                        .flatMap(it -> it.getListCaixas()
                                .stream()
                                .filter(eb -> !eb.isIncompleta() && !eb.isInspecao() && !eb.isRecebida() && !eb.isPerdida()))
                        .collect(Collectors.toList())
        ));
        if (caixasParaLeitura.size() == 0) {
            encerrarLeituraLote();
            return;
        }
        final FormTableView<SdCaixaPA> tblCaixas = FormTableView.create(SdCaixaPA.class, table -> {
            table.expanded();
            table.withoutHeader();
            table.items.bind(caixasParaLeitura);
            table.factoryRow(param -> {
                return new TableRow<SdCaixaPA>() {
                    @Override
                    protected void updateItem(SdCaixaPA item, boolean empty) {
                        super.updateItem(item, empty);
                        clear();
                        if (item != null && !empty && item.isSegunda()) {
                            getStyleClass().add("table-row-info");
                        }
                    }

                    private void clear() {
                        getStyleClass().removeAll("table-row-info");
                    }
                };
            });
            table.columns(
                    FormTableColumn.create(cln -> {
                        cln.title("OF");
                        cln.width(55);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdCaixaPA, SdCaixaPA>, ObservableValue<SdCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue().getItemlote().getNumero()));
                    }).build(), /*OF*/
                    FormTableColumn.create(cln -> {
                        cln.title("Caixa");
                        cln.width(55);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdCaixaPA, SdCaixaPA>, ObservableValue<SdCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId()));
                    }).build(), /*OF*/
                    FormTableColumn.create(cln -> {
                        cln.title("Peso");
                        cln.width(55);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdCaixaPA, SdCaixaPA>, ObservableValue<SdCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPeso()));
                    }).build(), /*OF*/
                    FormTableColumn.create(cln -> {
                        cln.title("Qtde");
                        cln.width(55);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdCaixaPA, SdCaixaPA>, ObservableValue<SdCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                    }).build() /*OF*/

            );
        });
        containerView.getChildren().add(FormBox.create(principal -> {
            principal.vertical();
            principal.expanded();
            principal.add(FormBox.create(boxMain -> {
                boxMain.vertical();
                boxMain.expanded();
                boxMain.alignment(Pos.CENTER);
                boxMain.add(FormBox.create(top -> {
                    top.horizontal();
                    top.setPadding(new Insets(2, 2, 0, 0));
                    top.add(FormLabel.create(lb -> lb.setText("Lote: " + loteEscolhido.getId())));
                    top.add(FormFieldText.create(text -> {
                        text.withoutTitle();
                        text.expanded();
                        text.label(ImageUtils.getIcon(ImageUtils.Icon.LEITURA_BARRA, ImageUtils.IconSize._24));
                        text.textField.setOnKeyPressed(event -> {
                            if (event.getCode().equals(KeyCode.ENTER) && !text.value.getValueSafe().equals("")) {
                                if (caixasParaLeitura.stream().noneMatch(it -> it.getId() == Integer.parseInt(text.value.getValue()))) {
                                    text.clear();
                                    text.requestFocus();
                                    return;
                                }
                                recebimentoCaixa(caixasParaLeitura, tblCaixas, text);
                            }
                        });
                    }).build());
                }));
                boxMain.add(FormBox.create(header -> {
                    header.horizontal();
                    header.setMaxHeight(5);
                    header.add(FormFieldText.create(text -> {
                        text.withoutTitle();
                        text.value.setValue(String.valueOf(totalCaixaPa.getValue()));
                        text.label("T");
                        text.expanded();
                        text.addStyle("success");
                        text.editable.set(false);
                    }).build());
                    header.add(FormFieldText.create(text -> {
                        text.withoutTitle();
                        text.value.setValue(String.valueOf(qtdeLidaCaixaPa.getValue()));
                        text.label("L");
                        text.expanded();
                        text.addStyle("primary");
                        text.editable.set(false);
                        qtdeLidaCaixaPa.addListener((observable, oldValue, newValue) -> {
                            text.value.setValue(String.valueOf(newValue));
                        });
                    }).build());
                    header.add(FormFieldText.create(text -> {
                        text.withoutTitle();
                        text.value.setValue(String.valueOf(saldoCaixaPa.getValue()));
                        text.label("S");
                        text.expanded();
                        text.addStyle("warning");
                        text.editable.set(false);
                        saldoCaixaPa.addListener((observable, oldValue, newValue) -> {
                            text.value.setValue(String.valueOf(newValue));
                        });
                    }).build());
                    header.add(FormFieldText.create(text -> {
                        text.withoutTitle();
                        text.value.setValue(String.valueOf(inspecaoCaixaPa.getValue()));
                        text.label("I");
                        text.expanded();
                        text.addStyle("danger");
                        text.editable.set(false);
                        inspecaoCaixaPa.addListener((observable, oldValue, newValue) -> {
                            text.value.setValue(String.valueOf(newValue));
                        });
                    }).build());
                }));
                boxMain.add(FormBox.create(boxTable -> {
                    boxTable.vertical();
                    boxTable.expanded();
                    boxTable.add(tblCaixas.build());
                }));
                boxMain.add(FormBox.create(boxFooter -> {
                    boxFooter.horizontal();
                    boxFooter.add(FormButton.create(btnVoltar -> {
                        btnVoltar.title("Voltar");
                        btnVoltar.icon(ImageUtils.getIcon(ImageUtils.Icon.ANTERIOR, ImageUtils.IconSize._24));
                        btnVoltar.addStyle("warning");
                        btnVoltar.setAction(event -> init());
                    }));
                }));
            }));
        }));
    }

    private void encerrarLeituraLote() {
        loteEscolhido.setStatus(new FluentDao().selectFrom(SdStatusLotePA.class).where(it -> it.equal("codigo", 6)).singleResult());
        loteEscolhido = new FluentDao().merge(loteEscolhido);
        MessageBox.create(message -> {
            message.message("Lote lido com sucesso!");
            message.type(MessageBox.TypeMessageBox.SUCCESS);
            message.showFullScreen();
        });
        init();
    }

    private void recebePerdidasIncompletas() {
        List<SdCaixaPA> listCaixas = loteEscolhido.getItensLote().stream().flatMap(it -> it.getListCaixas().stream().filter(eb -> (eb.isIncompleta() || eb.isPerdida()) && !eb.isRecebida())).collect(Collectors.toList());
        if (listCaixas.size() > 0) {
            for (SdCaixaPA caixaPa : listCaixas) {
                if (FuncoesEntradaPA.realizarPresistsBanco(caixaPa)) {
                    caixaPa.setRecebida(true);
                    new FluentDao().merge(caixaPa);
                }
            }
            MessageBox.create(message -> {
                message.message("Incompletas/Perdidas Recebidas!");
                message.type(MessageBox.TypeMessageBox.SUCCESS);
                message.showFullScreen();
            });
        }
    }

    private void recebimentoCaixa(ListProperty<SdCaixaPA> caixasParaLeitura, FormTableView<SdCaixaPA> tblCaixas, FormFieldText text) {
        SdCaixaPA sdCaixaPA = caixasParaLeitura.stream().filter(it -> it.getId() == Integer.parseInt(text.value.getValue())).findFirst().get();
        if (telaLeituraCaixa(sdCaixaPA)) {
            sdCaixaPA.setRecebida(true);
            new FluentDao().merge(sdCaixaPA);
            text.clear();
            caixasParaLeitura.remove(sdCaixaPA);
            tblCaixas.items.remove(sdCaixaPA);
            SysLogger.addSysDelizLog("Tela Minuta", TipoAcao.ENTRAR, sdCaixaPA.getId().toString(), "Caixa " + sdCaixaPA.getId().toString() + " lida com sucesso!");
            carregaCaixas();
        } else {
            text.clear();
            text.requestFocus();
        }
    }

    private boolean telaLeituraCaixa(SdCaixaPA sdCaixaPA) {
        containerView.getChildren().clear();
        AtomicBoolean retorno = new AtomicBoolean(false);

        final FormFieldText pesoCaixa = FormFieldText.create(field -> {
            field.title("Peso");
            field.value.setValue(sdCaixaPA.getPeso() + " KG");
            field.editable.set(false);
            field.focusTraversable.set(false);
            field.textField.deselect();
        });
        final FormFieldText codCaixa = FormFieldText.create(field -> {
            field.title("Caixa");
            field.expanded();
            field.value.setValue(String.valueOf(sdCaixaPA.getId()));
            field.editable.set(false);
            field.focusTraversable.set(false);
            field.textField.deselect();
        });
        final FormFieldText produtoField = FormFieldText.create(field -> {
            field.title("Produto");
            field.expanded();
            field.value.setValue(String.valueOf(sdCaixaPA.getProduto()));
            field.editable.set(false);
            field.focusTraversable.set(false);
            field.textField.deselect();
        });
        final FormFieldText entregaField = FormFieldText.create(field -> {
            field.title("Entrega");
            field.expanded();
            field.setValue(new SimpleDateFormat("MMM").format(Date.from(getDadosProduto(sdCaixaPA.getProduto()).getDtEntrega().atStartOfDay(ZoneId.systemDefault()).toInstant())).toUpperCase());
            field.editable.set(false);
            field.focusTraversable.set(false);
            field.textField.deselect();
        });
        FormButton btnInspecionarCaixas = FormButton.create(btn -> {
            btn.addStyle("warning");
            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.SEARCH, ImageUtils.IconSize._32));
            btn.setOnAction(evt -> {
                if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                    message.message("Inspecionar Caixa?");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showFullScreen();
                }).value.get())) {
                    sdCaixaPA.setInspecao(true);
                    new FluentDao().merge(sdCaixaPA);
                    SysLogger.addSysDelizLog("Tela Minuta", TipoAcao.EDITAR, sdCaixaPA.getId().toString(), "Caixa " + sdCaixaPA.getId().toString() + " enviada para inspeção!");
                    retorno.set(true);
                    carregaCaixas();
                }
            });
        });
        FormButton btnConfirmarCaixa = FormButton.create(btn -> {
            btn.addStyle("success");
            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._32));
            btn.setOnAction(evt -> {
                JPAUtils.getEntityManager().refresh(sdCaixaPA);
                if (FuncoesEntradaPA.realizarPresistsBanco(sdCaixaPA)) {
                    retorno.set(true);
                    sdCaixaPA.setRecebida(true);
                    new FluentDao().merge(sdCaixaPA);
                }
                carregaCaixas();
            });
        });
        FormButton btnCancelar = FormButton.create(btn -> {
            btn.addStyle("danger");
            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._32));
            btn.setOnAction(evt -> {
                carregaCaixas();
            });
        });
        ListProperty<SdItemCaixaPA> itensBean = new SimpleListProperty<>(FXCollections.observableArrayList());

        final FormTableView<SdItemCaixaPA> tblItensCaixa = FormTableView.create(SdItemCaixaPA.class, table -> {
            table.withoutHeader();
            table.items.bind(itensBean);
            table.expanded();
            table.columns(
                    FormTableColumn.create(cln -> {
                        cln.title("Cor");
                        cln.width(80);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdItemCaixaPA, SdItemCaixaPA>, ObservableValue<SdItemCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCor()));
                    }).build() /*Caixa*/

            );
        });

        List<String> tamanhos = sdCaixaPA.getItensCaixa().stream().map(SdItemCaixaPA::getTam).sorted(SortUtils::sortTamanhos).collect(Collectors.toList());

        for (String tamanho : tamanhos) {
            tblItensCaixa.addColumn(FormTableColumn.create(cln -> {
                        cln.title(tamanho);
                        cln.width(80);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdItemCaixaPA, SdItemCaixaPA>, ObservableValue<SdItemCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                        cln.format(param -> {
                            return new TableCell<SdItemCaixaPA, SdItemCaixaPA>() {
                                @Override
                                protected void updateItem(SdItemCaixaPA item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    if (item != null && !empty) {

                                        List<PaIten> itensList = (List<PaIten>) new FluentDao().selectFrom(PaIten.class)
                                                .where(it -> it
                                                        .equal("id.codigo", item.getProduto())
                                                        .equal("id.cor", item.getCor())
                                                        .equal("id.tam", tamanho)
                                                        .isIn("id.deposito", new String[]{"0005", "0023"})
                                                ).resultList();
                                        if (itensList.size() > 0) {
                                            setText(String.valueOf(itensList.stream().mapToInt(it -> it.getQuantidade().intValue()).sum()));
                                        } else {
                                            setText("0");
                                        }
                                    }
                                }
                            };
                        });
                    }).build()
            );
        }
        for (String cor : sdCaixaPA.getItensCaixa().stream().map(SdItemCaixaPA::getCor).distinct().collect(Collectors.toList())) {
            itensBean.add(new SdItemCaixaPA(sdCaixaPA.getProduto(), cor));
        }

        containerView.getChildren().add(FormBox.create(root -> {
            root.vertical();
            root.expanded();
            root.add(FormBox.create(header -> {
                header.horizontal();
                header.setBackground(new Background(new BackgroundFill(Paint.valueOf("#802046"), null, null)));
                header.setPadding(new Insets(0));
                header.add(FormLabel.create(lbl -> {
                    lbl.value.setValue("Confirmar Peso da Caixa");
                    lbl.setFont(new Font(18));
                    lbl.setTextFill(Color.WHITESMOKE);
                }));
            }));
            root.add(FormBox.create(principal -> {
                principal.vertical();
                principal.expanded();
                principal.setPadding(new Insets(0, 8, 8, 8));
                principal.alignment(Pos.TOP_CENTER);

                principal.add(FormBox.create(content -> {
                    content.vertical();
                    content.add(FormBox.create(line1 -> {
                        line1.horizontal();
                        line1.add(codCaixa.build(), produtoField.build());
                    }));

                    content.add(FormBox.create(lin2 -> {
                        lin2.horizontal();
                        lin2.add(pesoCaixa.build(), entregaField.build());
                    }));
                }));

                principal.add(FormBox.create(buttonBox -> {
                    buttonBox.horizontal();
                    buttonBox.alignment(Pos.CENTER);
                    buttonBox.add(btnConfirmarCaixa, btnInspecionarCaixas, btnCancelar);
                }));
                principal.add(FormBox.create(boxTbl -> {
                    boxTbl.vertical();
                    boxTbl.expanded();
                    boxTbl.add(tblItensCaixa.build());
                }));
            }));
        }));
        return retorno.get();
    }
    //</editor-fold>

}
