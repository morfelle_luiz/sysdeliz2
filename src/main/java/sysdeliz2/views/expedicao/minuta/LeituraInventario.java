package sysdeliz2.views.expedicao.minuta;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.input.KeyCode;
import javafx.scene.text.Font;
import javafx.util.Callback;
import sysdeliz2.controllers.views.expedicao.MinutaController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.models.sysdeliz.Inventario.*;
import sysdeliz2.models.ti.PaIten;
import sysdeliz2.models.ti.PaMov;
import sysdeliz2.models.view.VSdDadosProdutoBarra;
import sysdeliz2.models.view.VSdProdutoInventario;
import sysdeliz2.utils.SortUtils;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.views.expedicao.MinutaView;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class LeituraInventario extends MinutaController {

    private MinutaView containerView;
    private SdInventario inventarioSelecionado;
    private SdItemInventario itemInventarioSelecionado;
    private List<SdItemInventario> listItens = new ArrayList<>();
    private final BooleanProperty hasSkipped = new SimpleBooleanProperty(false);
    private final ListProperty<SdGradeItemInventario> gradeInventarioBean = new SimpleListProperty<>(FXCollections.observableArrayList());

    //<editor-fold desc="Field">
    private final FormFieldText referenciaField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("REF.");
        field.expanded();
        field.addStyle("xs");
        field.editable.set(false);
    });

    private final FormFieldText corField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("COR");
        field.expanded();
        field.addStyle("xs");
        field.editable.set(false);
    });

    private final FormFieldText leituraBarraField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label(ImageUtils.getIcon(ImageUtils.Icon.LEITURA_BARRA, ImageUtils.IconSize._16));
        field.expanded();
        field.addStyle("xs");
        field.textField.setOnKeyReleased(event -> {
            if (event.getCode().equals(KeyCode.ENTER)) {
                if (field.value.isEmpty().get()) {
                    field.clear();
                    field.requestFocus();
                } else {
                    leituraBarra(field);
                }
            }
        });
    });

    //</editor-fold>

    //<editor-fold desc="Buttons">
    private final Button btnConfirmar = FormButton.create(btn -> {
        btn.addStyle("success");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            confirmarLeituraItemInventario();
        });
    });

    private final Button btnVoltar = FormButton.create(btnVoltar -> {
        btnVoltar.icon(ImageUtils.getIcon(ImageUtils.Icon.RETORNAR, ImageUtils.IconSize._24));
        btnVoltar.addStyle("warning");
        btnVoltar.setOnAction(evt -> containerView.init());
    });

    private final Button btnPularProduto = FormButton.create(btnPUlar -> {
        btnPUlar.icon(ImageUtils.getIcon(ImageUtils.Icon.PROXIMO, ImageUtils.IconSize._24));
        btnPUlar.setText("Pular Produto");
        btnPUlar.addStyle("dark");
        btnPUlar.disable.bind(hasSkipped);
        btnPUlar.setAction(evt -> pularLeituraProduto());
    });

    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Table">
    private final FormTableView<SdGradeItemInventario> gradeItemTbl = FormTableView.create(SdGradeItemInventario.class, table -> {
        table.expanded();
        table.items.bind(gradeInventarioBean);
        table.withoutHeader();
        table.tableview().setPadding(new Insets(-1));
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("TAM");
                    cln.width(110);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdGradeItemInventario, SdGradeItemInventario>, ObservableValue<SdGradeItemInventario>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTam()));
                }).build(), /*TAM*/
                FormTableColumn.create(cln -> {
                    cln.title("QTDE");
                    cln.width(110);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdGradeItemInventario, SdGradeItemInventario>, ObservableValue<SdGradeItemInventario>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtdelida()));
                }).build() /*TAM*/
        );
    });

    // </editor-fold>

    public LeituraInventario(MinutaView container) {
        if (inventarioSelecionado != null) JPAUtils.clearEntity(inventarioSelecionado);
        containerView = container;
        carregaInventario();
    }

    public void init() {
        containerView.getChildren().clear();
        gradeItemTbl.clear();
        gradeInventarioBean.bindContent(FXCollections.observableArrayList(itemInventarioSelecionado.getListGrade().stream().sorted((o1, o2) -> SortUtils.sortTamanhos(o1.getTam(), o2.getTam())).collect(Collectors.toList())));
        containerView.getChildren().add(FormBox.create(principal -> {
            principal.vertical();
            principal.add(FormBox.create(header -> {
                header.vertical();
                header.setPadding(new Insets(5, 5, 0, 5));
                FormLabel lblLocal = FormLabel.create(lbl -> {
                    lbl.setFont(Font.font(10));
                    lbl.value.setValue("Loc: " + (itemInventarioSelecionado.getCodigo().getLocal() == null ? "" : itemInventarioSelecionado.getCodigo().getLocal()));
                });
                referenciaField.value.bind(itemInventarioSelecionado.getCodigo().codigoProperty());
                corField.value.bind(itemInventarioSelecionado.getCodigo().corProperty());
                header.add(FormBox.create(l1 -> {
                    l1.horizontal();
                    l1.add(referenciaField.build(), corField.build());
                }));
                header.add(FormBox.create(l2 -> {
                    l2.horizontal();
                    l2.add(lblLocal, leituraBarraField.build());
                }));
            }));
            principal.add(FormBox.create(content -> {
                content.vertical();
                content.expanded();
                content.add(gradeItemTbl.build());
            }));
            principal.add(FormBox.create(bottom -> {
                bottom.horizontal();
                bottom.alignment(Pos.CENTER);
                bottom.expanded();
                bottom.setPadding(new Insets(0, 5, 0, 5));
                bottom.add(FormBox.create(l1 -> {
                    l1.horizontal();
                    l1.alignment(Pos.CENTER_LEFT);
                    l1.add(btnVoltar);
                }));
                bottom.add(FormBox.create(l2 -> {
                    l2.horizontal();
                    l2.alignment(Pos.CENTER);
                    l2.add(btnPularProduto);
                }));
                bottom.add(FormBox.create(l3 -> {
                    l3.horizontal();
                    l3.alignment(Pos.CENTER_RIGHT);
                    l3.add(btnConfirmar);
                }));
            }));
        }));
        leituraBarraField.requestFocus();
    }

    private void carregaInventario() {

        inventarioSelecionado = new FluentDao()
                .selectFrom(SdInventario.class)
                .where(it -> it
                        .equal("mobile", true)
                        .lessThanOrEqualTo("dtleitura", LocalDate.now())
                        .isIn("status.codigo", new String[]{"2", "3"})
                ).orderBy("dtleitura", OrderType.ASC).findFirst();
        if (inventarioSelecionado == null) {
            MessageBox.create(message -> {
                message.message("Nenhum inventário encontrado!");
                message.type(MessageBox.TypeMessageBox.SUCCESS);
                message.showFullScreen();
            });
            containerView.init();
            return;
        }
        inventarioSelecionado.setStatus(new FluentDao().selectFrom(SdStatusInventario.class).where(it -> it.equal("codigo", 3)).singleResult());
        inventarioSelecionado = new FluentDao().merge(inventarioSelecionado);
        SdStatusItemInventario statusInventarioAgendado = new FluentDao().selectFrom(SdStatusItemInventario.class).where(it -> it.equal("codigo", 3)).singleResult();

        inventarioSelecionado.getItens().forEach(item -> {
            if (item.getStatus().getCodigo() == 2 || item.getStatus().getCodigo() == 4) {
                item.getCodigo().setLocal(item.getCodigo().getLocal() == null ? "0-0-N/E" : item.getCodigo().getLocal());
                item.setStatus(statusInventarioAgendado);
            }
        });
        listItens = inventarioSelecionado.getItens().stream().filter(it -> it.getStatus().getCodigo() == 3).collect(Collectors.toList());
        listItens.sort(Comparator.comparing((SdItemInventario it) -> it.getCodigo().getLocal()));
        getProximoProdutoLeitura();
    }

    private void getProximoProdutoLeitura() {
        if (listItens.size() > 0) {
            itemInventarioSelecionado = listItens.get(0);
            itemInventarioSelecionado.setStatus(new FluentDao().selectFrom(SdStatusItemInventario.class).where(it -> it.equal("codigo", 4)).singleResult());
            itemInventarioSelecionado = new FluentDao().merge(itemInventarioSelecionado);
            init();
        } else {
            MessageBox.create(message -> {
                message.message("Todos os produtos do inventário de hoje lidos!");
                message.type(MessageBox.TypeMessageBox.SUCCESS);
                message.showFullScreen();
            });
            inventarioSelecionado.setStatus(new FluentDao().selectFrom(SdStatusInventario.class).where(it -> it.equal("codigo", 4)).singleResult());
            inventarioSelecionado = new FluentDao().merge(inventarioSelecionado);
            containerView.init();
        }
    }

    private void confirmarLeituraItemInventario() {
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Tem certeza?");
            message.showFullScreen();
        }).value.get())) {
            int acertos = 0;
            int erros = 0;
            SdInventario inventario = itemInventarioSelecionado.getInventario();

            for (SdGradeItemInventario itemGrade : itemInventarioSelecionado.getListGrade()) {
                if (itemGrade.getQtde().intValue() != itemGrade.getQtdelida().intValue()) erros += 1;
                else acertos += 1;
                try {
                    realizarAlteracoesGrade(itemGrade);
                } catch (Exception e) {
                    MessageBox.create(message -> {
                        message.message("Erro na conclusão!");
                        message.type(MessageBox.TypeMessageBox.ERROR);
                        message.showFullScreen();
                    });
                    e.printStackTrace();
                    return;
                }
            }
            itemInventarioSelecionado.setAcertos(acertos);
            itemInventarioSelecionado.setErros(erros);
            inventario.setAcertos(inventario.getAcertos() + acertos);
            inventario.setErros(inventario.getErros() + erros);
            inventario = new FluentDao().merge(inventario);
            itemInventarioSelecionado.setQtdelida(itemInventarioSelecionado.getListGrade().stream().mapToInt(SdGradeItemInventario::getQtdelida).sum());
            itemInventarioSelecionado.setStatus(new FluentDao().selectFrom(SdStatusItemInventario.class).where(it -> it.equal("codigo", 5)).singleResult());
            itemInventarioSelecionado.setHoraleitura(LocalDateTime.now());
            itemInventarioSelecionado = new FluentDao().merge(itemInventarioSelecionado);
            listItens.remove(itemInventarioSelecionado);
            inventario = new FluentDao().merge(inventario);
            getProximoProdutoLeitura();
        }
    }

    private void criaMovimentacao(SdGradeItemInventario itemGrade, PaIten deposito, BigDecimal quantidadeNova) {
        if (!deposito.getQuantidade().equals(quantidadeNova)) {
            BigDecimal qtdeMovimentada = deposito.getQuantidade().subtract(quantidadeNova).abs();
            String operacao = quantidadeNova.intValue() > deposito.getQuantidade().intValue() ? "E" : "S";
            String observacao = qtdeMovimentada + " peças " + (quantidadeNova.intValue() > deposito.getQuantidade().intValue() ? "adicionadas " : "removidas ");
            deposito.setQuantidade(quantidadeNova);
            PaMov paMov = new PaMov(itemGrade, deposito.getId().getDeposito(), qtdeMovimentada, operacao, observacao);
            try {
                new FluentDao().persist(paMov);
                new FluentDao().merge(deposito);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void realizarAlteracoesGrade(SdGradeItemInventario itemGrade) {
        PaIten estoque = new FluentDao().selectFrom(PaIten.class).where(it -> it
                        .equal("id.tam", itemGrade.getTam())
                        .equal("id.cor", itemGrade.getItem().getCodigo().getCor())
                        .equal("id.codigo", itemGrade.getItem().getCodigo().getCodigo())
                        .equal("id.deposito", "0005")
                )
                .singleResult();

        PaIten b2c = new FluentDao().selectFrom(PaIten.class).where(it -> it
                        .equal("id.tam", itemGrade.getTam())
                        .equal("id.cor", itemGrade.getItem().getCodigo().getCor())
                        .equal("id.codigo", itemGrade.getItem().getCodigo().getCodigo())
                        .equal("id.deposito", "0023")
                )
                .singleResult();

        if (estoque == null) {
            estoque = new PaIten(itemGrade, "0005");
        }

        if (b2c == null) criaMovimentacao(itemGrade, estoque, new BigDecimal(itemGrade.getQtdelida()));
        else if (itemGrade.getQtdelida() < b2c.getQuantidade().intValue()) {
            if (estoque.getQuantidade().equals(BigDecimal.ZERO))
                criaMovimentacao(itemGrade, b2c, new BigDecimal(itemGrade.getQtdelida()));
            else {
                criaMovimentacao(itemGrade, b2c, BigDecimal.ZERO);
                criaMovimentacao(itemGrade, estoque, new BigDecimal(itemGrade.getQtdelida()));
            }
        } else
            criaMovimentacao(itemGrade, estoque, new BigDecimal(itemGrade.getQtdelida() - b2c.getQuantidade().intValue()));
    }

    private void leituraBarra(FormFieldText textField) {
        try {
            String barraLida = textField.value.getValue();
            textField.clear();
            textField.requestFocus();
            VSdDadosProdutoBarra produtoBarra = new FluentDao().selectFrom(VSdDadosProdutoBarra.class).where(it -> it.equal("barra28", barraLida.substring(0, 6))).singleResult();

            if (produtoBarra == null) {
                MessageBox.create(message -> {
                    message.message("Produto não encontrado!");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showFullScreenMobile();
                });
                return;
            }
            if (!validateBarraJaLida(barraLida)) return;
            if (produtoBarra.getCodigo().equals(itemInventarioSelecionado.getCodigo().getCodigo()) && produtoBarra.getCor().equals(itemInventarioSelecionado.getCodigo().getCor())) {
                itemInventarioSelecionado.getListGrade().stream().filter(it -> it.getTam().equals(produtoBarra.getTam())).findFirst().ifPresent(itemGrade -> adicionaBarraLida(barraLida, itemGrade));
            } else {
                SdItemInventario sdItemInventario = new SdItemInventario(inventarioSelecionado, new FluentDao().selectFrom(VSdProdutoInventario.class).where(it -> it.equal("codigo", produtoBarra.getCodigo()).equal("cor", produtoBarra.getCor())).singleResult());
                inventarioSelecionado.getItens().add(sdItemInventario);
                sdItemInventario.setStatus(new FluentDao().selectFrom(SdStatusItemInventario.class).where(it -> it.equal("codigo", 2)).singleResult());
                sdItemInventario = new FluentDao().merge(sdItemInventario);
                listItens.add(sdItemInventario);
                listItens.sort(Comparator.comparing((SdItemInventario it) -> it.getCodigo().getLocal()));
                MessageBox.create(message -> {
                    message.message("Produto no local errado!\nProduto adicionado ao inventário.");
                    message.getMessage().setFont(new Font(12));
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showFullScreen();
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
            MessageBox.create(message -> {
                message.message("Erro durante leitura inventário");
                message.type(MessageBox.TypeMessageBox.ERROR);
                message.showFullScreenMobile();
            });
        }
    }

    private void adicionaBarraLida(String barraLida, SdGradeItemInventario itemGrade) {
        SdBarraItemInventario sdBarra = new SdBarraItemInventario(itemGrade, barraLida);
        itemGrade.getListBarra().add(sdBarra);
        itemGrade.setQtdelida(itemGrade.getListBarra().size());
        itemGrade = new FluentDao().merge(itemGrade);
        gradeItemTbl.refresh();
    }

    private boolean validateBarraJaLida(String barraLida) {
        SdBarraItemInventario barraItemInventario = new FluentDao().selectFrom(SdBarraItemInventario.class)
                .where(it -> it
                        .equal("barra", barraLida)

                ).singleResult();
        if (barraItemInventario != null && barraItemInventario.getItemgrade().getItem().getInventario().getId().equals(itemInventarioSelecionado.getInventario().getId())) {
            MessageBox.create(message -> {
                message.message("Barra já lida nesse inventário!");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showFullScreen();
            });
            return false;
        }
        return true;
    }

    private void pularLeituraProduto() {
        if (listItens.size() == 1) {
            MessageBox.create(message -> {
                message.message("Não há próximo produto.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showFullScreen();
            });
            leituraBarraField.requestFocus();
            return;
        }
        hasSkipped.set(true);
        SdItemInventario primeiro = listItens.get(0);
        listItens.remove(primeiro);
        listItens.add(primeiro);
        getProximoProdutoLeitura();
        leituraBarraField.requestFocus();
    }
}
