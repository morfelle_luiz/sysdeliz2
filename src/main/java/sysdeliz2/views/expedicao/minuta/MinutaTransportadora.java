package sysdeliz2.views.expedicao.minuta;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import org.apache.log4j.Logger;
import sysdeliz2.controllers.views.expedicao.MinutaController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.sysdeliz.SdMinutaExpedicao;
import sysdeliz2.models.ti.Nota;
import sysdeliz2.models.ti.TabTran;
import sysdeliz2.models.view.VSdNfsMinuta;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.ReportUtils;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.mails.SimpleMail;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.views.expedicao.MinutaView;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class MinutaTransportadora extends MinutaController {

    private static final Logger logger = Logger.getLogger(MinutaTransportadora.class);
    private MinutaView containerView;
    // <editor-fold defaultstate="collapsed" desc="fields minuta">
    private final FormFieldText fieldNumeroCaixa = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label(ImageUtils.getIcon(ImageUtils.Icon.QRCODE, ImageUtils.IconSize._32));
        field.alignment(Pos.CENTER);
        field.addStyle("lg");
        field.toUpper();
        field.keyReleased(evt -> {
            if (evt.getCode().equals(KeyCode.ENTER))
                confirmaCaixa();
        });
    });
    private final FormTableView<VSdNfsMinuta> tblNotasMinuta = FormTableView.create(VSdNfsMinuta.class, table -> {
        table.title("NFs Minuta");
        table.expanded();
        table.items.bind(beanNfsParaLeitura);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Nota");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdNfsMinuta, VSdNfsMinuta>, ObservableValue<VSdNfsMinuta>>) param -> new ReadOnlyObjectWrapper(param.getValue().getFatura()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Nota*/,
                FormTableColumn.create(cln -> {
                    cln.title("Volumes");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdNfsMinuta, VSdNfsMinuta>, ObservableValue<VSdNfsMinuta>>) param -> new ReadOnlyObjectWrapper(param.getValue().getVolumes()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Volumes*/,
                FormTableColumn.create(cln -> {
                    cln.title("Lidos");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdNfsMinuta, VSdNfsMinuta>, ObservableValue<VSdNfsMinuta>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLidos()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Lidos*/
        );
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="conferência">
    private final IntegerProperty beanNfsLidas = new SimpleIntegerProperty(0);
    protected final ListProperty<SdMinutaExpedicao> beanNfsMinuta = new SimpleListProperty<>();
    private final FormFieldText fieldChaveNf = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label(ImageUtils.getIcon(ImageUtils.Icon.LEITURA_BARRA, ImageUtils.IconSize._32));
        field.alignment(Pos.CENTER);
        field.addStyle("lg");
        field.toUpper();
        field.keyReleased(evt -> {
            if (evt.getCode().equals(KeyCode.ENTER))
                confirmaCaixa();
        });
    });
    private final FormTableView<SdMinutaExpedicao> tblNotasConferencia = FormTableView.create(SdMinutaExpedicao.class, table -> {
        table.title("NFs Minuta");
        table.expanded();
        table.items.bind(beanNfsMinuta);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Nota");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMinutaExpedicao, SdMinutaExpedicao>, ObservableValue<SdMinutaExpedicao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getNota().getFatura()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Nota*/,
                FormTableColumn.create(cln -> {
                    cln.title("Caixas");
                    cln.width(140.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMinutaExpedicao, SdMinutaExpedicao>, ObservableValue<SdMinutaExpedicao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCaixas()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Volumes*/
        );
    });
    // </editor-fold>

    public MinutaTransportadora(MinutaView container) {
        containerView = container;
        init();
    }

    public void init() {
        containerView.getChildren().clear();
        containerView.getChildren().add(FormBox.create(principal -> {
            principal.vertical();
            principal.expanded();
            principal.setPadding(new Insets(0,7,0,7));
            principal.add(FormBox.create(boxMain -> {
                boxMain.vertical();
                boxMain.expanded();
                boxMain.alignment(Pos.CENTER);
                boxMain.add(FormButton.create(btnCriarNova -> {
                    btnCriarNova.title("Criar Minuta");
                    btnCriarNova.width(250.0);
                    btnCriarNova.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._32));
                    btnCriarNova.addStyle("lg").addStyle("primary");
                    btnCriarNova.setAction(evt -> criarMinuta());
                }));
                boxMain.add(FormButton.create(btnConsultarCaixa -> {
                    btnConsultarCaixa.title("Consulta Caixa");
                    btnConsultarCaixa.width(250.0);
                    btnConsultarCaixa.icon(ImageUtils.getIcon(ImageUtils.Icon.CAIXA_VAZIA, ImageUtils.IconSize._32));
                    btnConsultarCaixa.addStyle("lg").addStyle("info");
                    btnConsultarCaixa.setAction(evt -> consultarCaixa());
                }));
                boxMain.add(FormButton.create(btnMinutaManual -> {
                    btnMinutaManual.title("Minuta Manual");
                    btnMinutaManual.width(250.0);
                    btnMinutaManual.icon(ImageUtils.getIcon(ImageUtils.Icon.ABRIR, ImageUtils.IconSize._32));
                    btnMinutaManual.addStyle("lg").addStyle("warning");
                    btnMinutaManual.setAction(evt -> minutaManual());
                }));
                boxMain.add(FormButton.create(btnFecharMinuta -> {
                    btnFecharMinuta.title("Fechar Minuta");
                    btnFecharMinuta.width(250.0);
                    btnFecharMinuta.icon(ImageUtils.getIcon(ImageUtils.Icon.ANEXO, ImageUtils.IconSize._32));
                    btnFecharMinuta.addStyle("lg").addStyle("success");
                    btnFecharMinuta.setAction(evt -> fecharMinuta());
                }));
                boxMain.add(FormBox.create(toolbar -> {
                    toolbar.horizontal();
                    toolbar.alignment(Pos.CENTER);
                    toolbar.add(FormButton.create(btnConferenciaNf -> {
                        btnConferenciaNf.title("Conf. NFs");
                        btnConferenciaNf.width(185.0);
                        btnConferenciaNf.icon(ImageUtils.getIcon(ImageUtils.Icon.LEITURA_BARRA, ImageUtils.IconSize._32));
                        btnConferenciaNf.addStyle("lg").addStyle("dark");
                        btnConferenciaNf.setAction(evt -> conferirNfMinuta());
                    }));
                    toolbar.add(FormButton.create(btnImprimirMinuta -> {
                        btnImprimirMinuta.title("");
                        btnImprimirMinuta.icon(ImageUtils.getIcon(ImageUtils.Icon.IMPRIMIR, ImageUtils.IconSize._32));
                        btnImprimirMinuta.addStyle("lg").addStyle("primary");
                        btnImprimirMinuta.setAction(evt -> imprimirMinuta());
                    }));
                }));
                boxMain.add(FormButton.create(btnVoltar -> {
                    btnVoltar.title("Retornar");
                    btnVoltar.icon(ImageUtils.getIcon(ImageUtils.Icon.RETORNAR, ImageUtils.IconSize._32));
                    btnVoltar.addStyle("lg").addStyle("warning");
                    btnVoltar.setAction(evt -> containerView.init());
                }));
            }));
        }));
    }

    //<editor-fold desc="Ações Minuta">
    private void criarMinuta() {
        containerView.getChildren().clear();
        beanVolumesLidos.set(0);
        beanNfsParaLeitura.clear();
        beanNotasMinuta.clear();

        final FormFieldText fieldCodigoTransportadora = FormFieldText.create(field -> {
            field.title("Transp");
            field.label(ImageUtils.getIcon(ImageUtils.Icon.QRCODE, ImageUtils.IconSize._32));
            field.alignment(Pos.CENTER);
            field.toUpper();
            field.addStyle("lg");
        });
        if (new Fragment().show(fragment -> {
            fragment.title("Transportadora");
            fragment.size(200.0, 100.0);
            fragment.box.getChildren().add(fieldCodigoTransportadora.build());
            fragment.buttonsBox.getChildren().add(FormButton.create(btnAbrirTransp -> {
                btnAbrirTransp.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                btnAbrirTransp.addStyle("success");
                btnAbrirTransp.setAction(evt -> {
                    TabTran transpMinuta = new FluentDao().selectFrom(TabTran.class)
                            .where(eb -> eb.equal("codigo", fieldCodigoTransportadora.value.get()))
                            .singleResult();
                    if (transpMinuta == null) {
                        fieldCodigoTransportadora.clear();
                        fieldCodigoTransportadora.requestFocus();
                        return;
                    }

                    MessageBox.create(message -> {
                        message.message("Transp: " + transpMinuta.getNome());
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.showFullScreenMobile();
                    });
                    containerView.getChildren().add(FormBox.create(principal -> {
                        principal.vertical();
                        principal.expanded();
                        principal.add(FormBox.create(boxMain -> {
                            boxMain.vertical();
                            boxMain.expanded();
                            boxMain.alignment(Pos.CENTER);
                            boxMain.add(fieldNumeroCaixa.build());
                            boxMain.add(FormBox.create(boxTable -> {
                                boxTable.vertical();
                                boxTable.expanded();
                                boxTable.add(tblNotasMinuta.build());
                            }));
                            boxMain.add(FormBox.create(boxFooter -> {
                                boxFooter.horizontal();
                                boxFooter.add(FormButton.create(btnVoltar -> {
                                    btnVoltar.title("Voltar");
                                    btnVoltar.icon(ImageUtils.getIcon(ImageUtils.Icon.ANTERIOR, ImageUtils.IconSize._24));
                                    btnVoltar.addStyle("warning");
                                    btnVoltar.setAction(event -> init());
                                }));
                                boxFooter.add(FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.width(100.0);
                                    field.alignment(Pos.CENTER);
                                    field.label("Lidos");
                                    field.value.bind(beanVolumesLidos.asString());
                                }).build());
                            }));
                            fieldNumeroCaixa.requestFocus();
                        }));
                    }));
                    JPAUtils.clearEntitys(beanNfsParaLeitura);
                    beanNfsParaLeitura.set(FXCollections.observableList(carregaNfsParaLeituraMinuta(transpMinuta.getCodigo())));
                    beanNotasMinuta.set(FXCollections.observableList(carregaMinutaExistente()));
                    beanVolumesLidos.set(beanNotasMinuta.get().stream().mapToInt(SdMinutaExpedicao::getVolumesLidos).sum());

                    fragment.close();
                    logger.info("Carregado " + beanNotasMinuta.size() + " itens!!!");
                });
            }));
            fieldCodigoTransportadora.requestFocus();
        }).compareTo(Fragment.ReturnType.CANCEL) == 0) {
            this.init();
        }
    }

    private void consultarCaixa() {
        containerView.getChildren().clear();
        final FormFieldText fieldNumeroCaixaConsulta = FormFieldText.create(field -> {
            field.withoutTitle();
            field.label(ImageUtils.getIcon(ImageUtils.Icon.QRCODE, ImageUtils.IconSize._24));
            field.toUpper();
            field.alignment(Pos.CENTER);
            field.keyReleased(kevt -> {
                if (kevt.getCode().equals(KeyCode.ENTER)) {
                    consultaCaixa(field.value.get().replace("CX", ""));
                    field.clear();
                    field.requestFocus();
                }
            });
        });
        final FormFieldText fieldNotaFiscal = FormFieldText.create(field -> {
            field.withoutTitle();
            field.label("NF");
            field.editable(false);
            field.value.bind(beanNfConsultaCaixa);
            field.alignment(Pos.CENTER);
        });
        final FormFieldText fieldRemessa = FormFieldText.create(field -> {
            field.withoutTitle();
            field.label("Remessa");
            field.editable(false);
            field.value.bind(beanRemessaConsultaCaixa);
            field.alignment(Pos.CENTER);
        });
        final FormFieldText fieldCliente = FormFieldText.create(field -> {
            field.withoutTitle();
            field.label("Cliente");
            field.editable(false);
            field.value.bind(beanClienteConsultaCaixa);
        });
        final FormFieldText fieldVolume = FormFieldText.create(field -> {
            field.withoutTitle();
            field.label("Volume");
            field.editable(false);
            field.value.bind(beanVolumeConsultaCaixa);
            field.alignment(Pos.CENTER);
        });
        final FormFieldText fieldTransportadoraMinuta = FormFieldText.create(field -> {
            field.withoutTitle();
            field.label("Transportadora");
            field.editable(false);
            field.value.bind(beanTranspConsultaCaixa);
            field.alignment(Pos.CENTER);
        });
        final FormFieldText fieldDtMinuta = FormFieldText.create(field -> {
            field.withoutTitle();
            field.label("Data Minuta");
            field.editable(false);
            field.value.bind(beanDtMinutaConsultaCaixa);
            field.alignment(Pos.CENTER);
        });
        containerView.getChildren().add(FormBox.create(principal -> {
            principal.vertical();
            principal.expanded();
            principal.add(FormBox.create(boxMain -> {
                boxMain.vertical();
                boxMain.expanded();
                boxMain.alignment(Pos.CENTER);
                boxMain.add(fieldNumeroCaixaConsulta.build());
                boxMain.add(FormBox.create(boxDadosCaixa -> {
                    boxDadosCaixa.vertical();
                    boxDadosCaixa.expanded();
                    boxDadosCaixa.add(fieldNotaFiscal.build());
                    boxDadosCaixa.add(fieldRemessa.build());
                    boxDadosCaixa.add(fieldCliente.build());
                    boxDadosCaixa.add(fieldVolume.build());
                    boxDadosCaixa.add(fieldTransportadoraMinuta.build());
                    boxDadosCaixa.add(fieldDtMinuta.build());
                }));
                boxMain.add(FormBox.create(boxFooter -> {
                    boxFooter.horizontal();
                    boxFooter.add(FormButton.create(btnVoltar -> {
                        btnVoltar.title("Voltar");
                        btnVoltar.icon(ImageUtils.getIcon(ImageUtils.Icon.ANTERIOR, ImageUtils.IconSize._24));
                        btnVoltar.addStyle("warning");
                        btnVoltar.setAction(event -> init());
                    }));
                }));
            }));
        }));
        fieldNumeroCaixaConsulta.requestFocus();
    }

    private void minutaManual() {
        containerView.getChildren().clear();
        //nfsMinutaManual();
        final FormFieldText fieldCodigoTransportadora = FormFieldText.create(field -> {
            field.title("Transp");
            field.label(ImageUtils.getIcon(ImageUtils.Icon.QRCODE, ImageUtils.IconSize._32));
            field.alignment(Pos.CENTER);
            field.toUpper();
            field.addStyle("lg");
        });
        if (new Fragment().show(fragment -> {
            fragment.title("Transportadora");
            fragment.size(200.0, 100.0);
            fragment.box.getChildren().add(fieldCodigoTransportadora.build());
            fragment.buttonsBox.getChildren().add(FormButton.create(btnAbrirTransp -> {
                btnAbrirTransp.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                btnAbrirTransp.addStyle("success");
                btnAbrirTransp.setAction(evt -> {
                    TabTran transpMinuta = new FluentDao().selectFrom(TabTran.class).where(eb -> eb.equal("codigo", fieldCodigoTransportadora.value.get())).singleResult();
                    if (transpMinuta == null) {
                        fieldCodigoTransportadora.clear();
                        fieldCodigoTransportadora.requestFocus();
                        return;
                    }

                    MessageBox.create(message -> {
                        message.message("Transp: " + transpMinuta.getNome());
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.showFullScreenMobile();
                    });

                    nfsMinutaManual(transpMinuta.getCodigo());
                    containerView.getChildren().add(FormBox.create(principal -> {
                        principal.vertical();
                        principal.expanded();
                        principal.add(FormBox.create(boxMain -> {
                            boxMain.vertical();
                            boxMain.expanded();
                            boxMain.alignment(Pos.CENTER);
                            boxMain.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.label("Chave");
                                field.addStyle("lg");
                                field.keyReleased(evtKey -> {
                                    adicionarNfManualPorChave(field.value.get(), transpMinuta.getCodigo());
                                    field.clear();
                                    field.requestFocus();
                                });
                            }).build());
                            boxMain.add(FormBox.create(boxDadosCaixa -> {
                                boxDadosCaixa.vertical();
                                boxDadosCaixa.expanded();
                                boxDadosCaixa.add(FormTableView.create(VSdNfsMinuta.class, table -> {
                                    table.title("NFs Manuais");
                                    table.items.bind(beanNfsMinutaManual);
                                    table.expanded();
                                    table.columns(
                                            FormTableColumn.create(cln -> {
                                                cln.title("Nota Fiscal");
                                                cln.width(80.0);
                                                cln.value((Callback<TableColumn.CellDataFeatures<VSdNfsMinuta, VSdNfsMinuta>, ObservableValue<VSdNfsMinuta>>) param -> new ReadOnlyObjectWrapper(param.getValue().getFatura()));
                                                cln.alignment(Pos.CENTER);
                                            }).build() /*Nota Fiscal*/,
                                            FormTableColumn.create(cln -> {
                                                cln.title("Volumes");
                                                cln.width(55.0);
                                                cln.value((Callback<TableColumn.CellDataFeatures<VSdNfsMinuta, VSdNfsMinuta>, ObservableValue<VSdNfsMinuta>>) param -> new ReadOnlyObjectWrapper(param.getValue().getVolumes()));
                                                cln.alignment(Pos.CENTER);
                                            }).build() /*Volumes*/,
                                            FormTableColumn.create(cln -> {
                                                cln.title("");
                                                cln.width(50.0);
                                                cln.alignment(FormTableColumn.Alignment.CENTER);
                                                cln.value((Callback<TableColumn.CellDataFeatures<VSdNfsMinuta, VSdNfsMinuta>, ObservableValue<VSdNfsMinuta>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                                cln.format(param -> {
                                                    return new TableCell<VSdNfsMinuta, VSdNfsMinuta>() {
                                                        final HBox boxButtonsRow = new HBox(3);
                                                        final Button btnIncluirMinuta = FormButton.create(btn -> {
                                                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._24));
                                                            btn.tooltip("Incluir NF na minuta");
                                                            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                                            btn.addStyle("lg").addStyle("success");
                                                        });

                                                        @Override
                                                        protected void updateItem(VSdNfsMinuta item, boolean empty) {
                                                            super.updateItem(item, empty);
                                                            setText(null);
                                                            setGraphic(null);
                                                            if (item != null && !empty) {
                                                                btnIncluirMinuta.setOnAction(evt -> {
                                                                    incluirNfMinuta(item, transpMinuta.getCodigo());
                                                                    table.refresh();
                                                                });
                                                                boxButtonsRow.getChildren().clear();
                                                                boxButtonsRow.getChildren().addAll(btnIncluirMinuta);
                                                                setGraphic(boxButtonsRow);
                                                            }
                                                        }
                                                    };
                                                });
                                            }).build()
                                    );
                                    table.factoryRow(param -> {
                                        return new TableRow<VSdNfsMinuta>() {
                                            @Override
                                            protected void updateItem(VSdNfsMinuta item, boolean empty) {
                                                super.updateItem(item, empty);
                                                setStyle(getStyle() + "; -fx-font-size: 16px;");
                                            }
                                        };
                                    });
                                }).build());
                            }));
                            boxMain.add(FormBox.create(boxFooter -> {
                                boxFooter.horizontal();
                                boxFooter.add(FormButton.create(btnVoltar -> {
                                    btnVoltar.title("Voltar");
                                    btnVoltar.icon(ImageUtils.getIcon(ImageUtils.Icon.ANTERIOR, ImageUtils.IconSize._24));
                                    btnVoltar.addStyle("warning");
                                    btnVoltar.setAction(event -> init());
                                }));
                            }));
                        }));
                    }));
                    fragment.close();
                });
            }));
        }).compareTo(Fragment.ReturnType.CANCEL) == 0) {
            this.init();
        }
    }

    private void adicionarNfManualPorChave(String chaveNf, String transp) {
        Nota notaChave = new FluentDao().selectFrom(Nota.class)
                        .where(eb-> eb.equal("chavenfe", chaveNf))
                                .singleResult();
        if (notaChave == null) {
            MessageBox.create(message -> {
                message.message("Não foi encontrado uma NF com esta chave. Verifique a leitura da chave da NF ou selecione na lista de NFs.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showFullScreenMobile();
            });
            return;
        }
        VSdNfsMinuta nfMinuta = new FluentDao().selectFrom(VSdNfsMinuta.class)
                .where(eb->eb.equal("fatura", notaChave.getFatura()))
                .singleResult();
        if (nfMinuta == null) {
            MessageBox.create(message -> {
                message.message("Não foi possível encontrar essa NF liberada para minuta, ela já pode estar em uma outra minuta, verifique os dados da NF.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showFullScreenMobile();
            });
            return;
        }
        incluirNfMinuta(nfMinuta, transp);
    }

    private void fecharMinuta() {
        final FormFieldText fieldCodigoTransportadora = FormFieldText.create(field -> {
            field.title("Transp");
            field.label(ImageUtils.getIcon(ImageUtils.Icon.QRCODE, ImageUtils.IconSize._32));
            field.alignment(Pos.CENTER);
            field.toUpper();
            field.addStyle("lg");
        });
        new Fragment().show(fragment -> {
            fragment.title("Transportadora");
            fragment.size(200.0, 100.0);
            fragment.box.getChildren().add(fieldCodigoTransportadora.build());
            fragment.buttonsBox.getChildren().add(FormButton.create(btnAbrirTransp -> {
                btnAbrirTransp.icon(ImageUtils.getIcon(ImageUtils.Icon.ANEXO, ImageUtils.IconSize._24));
                btnAbrirTransp.addStyle("primary");
                btnAbrirTransp.setAction(evt -> {
                    TabTran transpMinuta = new FluentDao().selectFrom(TabTran.class)
                            .where(eb -> eb
                                    .equal("codigo", fieldCodigoTransportadora.value.get()))
                            .singleResult();
                    if (transpMinuta == null) {
                        fieldCodigoTransportadora.clear();
                        fieldCodigoTransportadora.requestFocus();
                        return;
                    }

                    try {
                        fragment.buttonsBox.setDisable(true);
                        incluirNfsVolumesZerado(transpMinuta.getCodigo());

                        MessageBox.create(message -> {
                            message.message("Fechado minuta da transp: " + transpMinuta.getNome() + " data: " + StringUtils.toDateFormat(LocalDate.now()) + " para conferência.");
                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                            message.showFullScreenMobile();
                        });

                        fragment.buttonsBox.setDisable(false);
                        fragment.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                        MessageBox.create(message -> {
                            message.message("Ocorreu um erro na impressão da minuta. Erro: " + e.getMessage());
                            message.type(MessageBox.TypeMessageBox.ERROR);
                            message.showFullScreenMobile();
                        });
                    }
                });
            }));
        });
    }

    private void imprimirMinuta() {
        final FormFieldText fieldCodigoTransportadora = FormFieldText.create(field -> {
            field.title("Transp");
            field.label(ImageUtils.getIcon(ImageUtils.Icon.QRCODE, ImageUtils.IconSize._32));
            field.alignment(Pos.CENTER);
            field.toUpper();
            field.addStyle("lg");
        });
        new Fragment().show(fragment -> {
            fragment.title("Transportadora");
            fragment.size(200.0, 100.0);
            fragment.box.getChildren().add(fieldCodigoTransportadora.build());
            fragment.buttonsBox.getChildren().add(FormButton.create(btnAbrirTransp -> {
                btnAbrirTransp.icon(ImageUtils.getIcon(ImageUtils.Icon.IMPRIMIR, ImageUtils.IconSize._24));
                btnAbrirTransp.addStyle("primary");
                btnAbrirTransp.setAction(evt -> {
                    TabTran transpMinuta = new FluentDao().selectFrom(TabTran.class)
                            .where(eb -> eb
                                    .equal("codigo", fieldCodigoTransportadora.value.get()))
                            .singleResult();
                    if (transpMinuta == null) {
                        fieldCodigoTransportadora.clear();
                        fieldCodigoTransportadora.requestFocus();
                        return;
                    }

                    try {
                        fragment.buttonsBox.setDisable(true);
                        new ReportUtils().config().addReport(ReportUtils.ReportFile.MINUTA_EXPEDICAO,
                                        new ReportUtils.ParameterReport("dt_minuta", StringUtils.toDateFormat(LocalDate.now())),
                                        new ReportUtils.ParameterReport("cod_transp", transpMinuta.getCodigo()),
                                        new ReportUtils.ParameterReport("codEmpresa", Globals.getEmpresaLogada().getCodigo()))
                                .view()
                                .print();

                        new ReportUtils().config().addReport(ReportUtils.ReportFile.MINUTA_EXPEDICAO,
                                        new ReportUtils.ParameterReport("dt_minuta", StringUtils.toDateFormat(LocalDate.now())),
                                        new ReportUtils.ParameterReport("cod_transp", transpMinuta.getCodigo()),
                                        new ReportUtils.ParameterReport("codEmpresa", Globals.getEmpresaLogada().getCodigo()))
                                .view()
                                .toPdf("C:\\SysDelizLocal\\local_files\\minuta.pdf");
                        SimpleMail.INSTANCE
                                .addDestinatario("minuta@deliz.com.br")
                                .addCCO("diego@deliz.com.br")
                                .comAssunto("Minuta Expedição "+StringUtils.toDateFormat(LocalDate.now()) + " transportadora " + transpMinuta.getCodigo())
                                .comCorpo("Segue anexo minuta gerada pelo leitor.")
                                .addAttachment("C:\\SysDelizLocal\\local_files\\minuta.pdf")
                                .send();

                        MessageBox.create(message -> {
                            message.message("Impresso minuta para a transp: " + transpMinuta.getNome() + " data: " + StringUtils.toDateFormat(LocalDate.now()));
                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                            message.showFullScreenMobile();
                        });

                        fragment.buttonsBox.setDisable(false);
                        fragment.close();
                    } catch (SQLException | JRException | IOException e) {
                        e.printStackTrace();
                        MessageBox.create(message -> {
                            message.message("Ocorreu um erro na impressão da minuta. Erro: " + e.getMessage());
                            message.type(MessageBox.TypeMessageBox.ERROR);
                            message.showFullScreenMobile();
                        });
                    }
                });
            }));
        });
    }

    private void conferirNfMinuta() {
        containerView.getChildren().clear();
        beanNfsLidas.set(0);
        beanNfsMinuta.clear();
        fieldChaveNf.clear();

        final FormFieldText fieldCodigoTransportadora = FormFieldText.create(field -> {
            field.title("Transp");
            field.label(ImageUtils.getIcon(ImageUtils.Icon.QRCODE, ImageUtils.IconSize._32));
            field.alignment(Pos.CENTER);
            field.toUpper();
            field.addStyle("lg");
        });
        if (new Fragment().show(fragment -> {
            fragment.title("Transportadora");
            fragment.size(200.0, 100.0);
            fragment.box.getChildren().add(fieldCodigoTransportadora.build());
            fragment.buttonsBox.getChildren().add(FormButton.create(btnAbrirTransp -> {
                btnAbrirTransp.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                btnAbrirTransp.addStyle("success");
                btnAbrirTransp.setAction(evt -> {
                    TabTran transpMinuta = new FluentDao().selectFrom(TabTran.class)
                            .where(eb -> eb.equal("codigo", fieldCodigoTransportadora.value.get()))
                            .singleResult();
                    if (transpMinuta == null) {
                        fieldCodigoTransportadora.clear();
                        fieldCodigoTransportadora.requestFocus();
                        return;
                    }

                    MessageBox.create(message -> {
                        message.message("Transp: " + transpMinuta.getNome());
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.showFullScreenMobile();
                    });
                    containerView.getChildren().add(FormBox.create(principal -> {
                        principal.vertical();
                        principal.expanded();
                        principal.add(FormBox.create(boxMain -> {
                            boxMain.vertical();
                            boxMain.expanded();
                            boxMain.alignment(Pos.CENTER);
                            boxMain.add(FormBox.create(boxHeader -> {
                                boxHeader.horizontal();
                                boxHeader.add(FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.expanded();
                                    field.label(ImageUtils.getIcon(ImageUtils.Icon.LEITURA_BARRA, ImageUtils.IconSize._32));
                                    field.alignment(Pos.CENTER);
                                    field.addStyle("lg");
                                    field.toUpper();
                                    field.keyReleased(evtKey -> {
                                        if (evtKey.getCode().equals(KeyCode.ENTER)) {
                                            confirmarNfMinuta(field.value.get(), transpMinuta);
                                            field.clear();
                                            field.requestFocus();
                                        }
                                    });
                                }).build());
                                boxHeader.add(FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.width(100.0);
                                    field.alignment(Pos.CENTER);
                                    field.label("Conf.");
                                    field.value.bind(beanNfsLidas.asString());
                                }).build());
                            }));
                            boxMain.add(FormBox.create(boxTable -> {
                                boxTable.vertical();
                                boxTable.expanded();
                                boxTable.add(tblNotasConferencia.build());
                            }));
                            boxMain.add(FormBox.create(boxFooter -> {
                                boxFooter.horizontal();
                                boxFooter.add(FormButton.create(btnVoltar -> {
                                    btnVoltar.title("Voltar");
                                    btnVoltar.icon(ImageUtils.getIcon(ImageUtils.Icon.ANTERIOR, ImageUtils.IconSize._24));
                                    btnVoltar.addStyle("warning");
                                    btnVoltar.setAction(event -> init());
                                }));
                            }));
                            fieldChaveNf.requestFocus();
                        }));
                    }));

                    List<SdMinutaExpedicao> notasMinuta = carregaNfsConferenciaMinuta(transpMinuta.getCodigo());
                    beanNfsLidas.set((int) notasMinuta.stream().filter(SdMinutaExpedicao::isConferido).count());
                    beanNfsMinuta.set(FXCollections.observableList(notasMinuta.stream().filter(minuta -> !minuta.isConferido()).collect(Collectors.toList())));

                    fragment.close();
                    logger.info("Carregado " + beanNotasMinuta.size() + " itens!!!");
                });
            }));
            fieldCodigoTransportadora.requestFocus();
        }).compareTo(Fragment.ReturnType.CANCEL) == 0) {
            this.init();
        }
    }

    private void confirmarNfMinuta(String chave, TabTran transpMinuta) {
        Nota notaChave = new FluentDao().selectFrom(Nota.class)
                .where(eb-> eb.equal("chavenfe", chave))
                .singleResult();
        if (notaChave == null) {
            MessageBox.create(message -> {
                message.message("Não foi encontrado uma NF com esta chave. Verifique a leitura da chave da NF ou selecione na lista de NFs.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showFullScreenMobile();
            });
            return;
        }
        SdMinutaExpedicao nfMinuta = new FluentDao().selectFrom(SdMinutaExpedicao.class)
                .where(eb->eb.equal("id.nota.fatura", notaChave.getFatura()))
                .singleResult();
        if (nfMinuta == null) {
            MessageBox.create(message -> {
                message.message("A NF " + notaChave.getFatura() + " não se encontra em uma minuta.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showFullScreenMobile();
            });
            return;
        }
        if (nfMinuta.isConferido()) {
            MessageBox.create(message -> {
                message.message("A NF " + notaChave.getFatura() + " já foi conferida na minuta: " +
                        nfMinuta.getId().getTransportadora() + " - " + StringUtils.toDateFormat(nfMinuta.getId().getDtMinuta()));
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showFullScreenMobile();
            });
            return;
        }
        if (!nfMinuta.getId().getTransportadora().equals(transpMinuta.getCodigo())) {
            MessageBox.create(message -> {
                message.message("A NF " + notaChave.getFatura() + " não se encontra na minuta da transportadora " + transpMinuta.getNome());
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showFullScreenMobile();
            });
            return;
        }
        if (!nfMinuta.getId().getDtMinuta().equals(LocalDate.now())) {
            MessageBox.create(message -> {
                message.message("A NF " + notaChave.getFatura() + " não se encontra na minuta do dia de hoje");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showFullScreenMobile();
            });
            return;
        }

        nfMinuta.setConferido(true);
        new FluentDao().merge(nfMinuta);
        beanNfsLidas.set(beanNfsLidas.add(1).get());
        beanNfsMinuta.remove(nfMinuta);
    }

    private void confirmaCaixa() {
        String caixa = fieldNumeroCaixa.value.get();
        if (caixa.startsWith("CX") && caixa.length() == 8) {
            final FormFieldText fieldConfirmaNumeroCaixa = FormFieldText.create(field -> {
                field.withoutTitle();
                field.label(ImageUtils.getIcon(ImageUtils.Icon.QRCODE, ImageUtils.IconSize._32));
                field.alignment(Pos.CENTER);
                field.toUpper();
                field.addStyle("lg");
            });
            new Fragment().show(fragment -> {
                fragment.title("Confirmar Caixa");
                fragment.size(200.0, 100.0);
                fragment.box.getChildren().add(fieldConfirmaNumeroCaixa.build());
                fragment.buttonsBox.getChildren().add(FormButton.create(btn -> {
                    btn.title("");
                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                    btn.addStyle("success");
                    btn.defaultButton(true);
                    btn.setAction(evt -> {
                        actionConfirmarCaixa(caixa.replace("CX", ""), fieldConfirmaNumeroCaixa, fragment);
                    });
                }));
            });
            fieldConfirmaNumeroCaixa.requestFocus();
        } else {
            MessageBox.create(message -> {
                message.message("Código de barras não é válido para o número da caixa.");
                message.type(MessageBox.TypeMessageBox.ERROR);
                message.showFullScreenMobile();
            });
            fieldNumeroCaixa.clear();
            fieldNumeroCaixa.requestFocus();
        }
    }

    private void actionConfirmarCaixa(String caixa, FormFieldText fieldConfirmaNumeroCaixa, Fragment fragment) {
        // verificação da barra lida
        if (fieldConfirmaNumeroCaixa.value.get().startsWith("CX")) {
            MessageBox.create(message -> {
                message.message("Barra lida para confirmação inválida, barra: " + fieldConfirmaNumeroCaixa.value.get());
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showFullScreenMobile();
            });
            fieldConfirmaNumeroCaixa.clear();
            fieldConfirmaNumeroCaixa.requestFocus();
            return;
        }

        // verificação se os números são iguais
        if (fieldConfirmaNumeroCaixa.value.isEqualTo(caixa).get()) {
            fragment.close();
            incluirVolumeMinuta(caixa);
            tblNotasMinuta.refresh();
            fieldNumeroCaixa.clear();
            fieldNumeroCaixa.requestFocus();
        } else {
            fragment.close();
            MessageBox.create(message -> {
                message.message("Os números das barras não são iguais, caixa: " + fieldConfirmaNumeroCaixa.value.get());
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showFullScreenMobile();
            });
            fieldNumeroCaixa.clear();
            fieldNumeroCaixa.requestFocus();
        }
    }
    //</editor-fold>

}
