package sysdeliz2.views.expedicao.minuta;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.Border;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.util.Callback;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import sysdeliz2.controllers.views.expedicao.MinutaController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.view.VSdDadosProduto;
import sysdeliz2.models.view.VSdDadosProdutoBarra;
import sysdeliz2.models.view.VSdLocalExpedicao;
import sysdeliz2.models.view.expedicao.VSdEstoqueProdutoCor;
import sysdeliz2.models.view.expedicao.VSdProdutoReposicao;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;
import sysdeliz2.views.expedicao.MinutaView;

import javax.swing.plaf.synth.Region;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.TextStyle;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class ReposicaoEstoque extends MinutaController {

    private static final Logger logger = Logger.getLogger(ReposicaoEstoque.class);
    private MinutaView containerView;

    private final ListProperty<VSdProdutoReposicao> produtosReposicao = new SimpleListProperty<>();
    private final ObjectProperty<VSdDadosProdutoBarra> produtoBarra = new SimpleObjectProperty<>();
    private final ObjectProperty<VSdDadosProduto> produtoDados = new SimpleObjectProperty<>();
    private final BooleanProperty tipoEntradaManual = new SimpleBooleanProperty(false);


    private final FormFieldText fieldBarcodeProduto = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label(ImageUtils.getIcon(ImageUtils.Icon.QRCODE, ImageUtils.IconSize._32));
        field.addStyle("lg");
        field.expanded();
        field.toUpper();
        field.editable.bind(produtoBarra.isNull());
        field.keyReleased(kvt -> {
//            if (kvt.getCode().equals(KeyCode.ENTER)) {
//                loadProduto(field.value.get());
//            }
        });
    });
    private final FormFieldText fieldProduto = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label(ImageUtils.getIcon(ImageUtils.Icon.PRODUTO, ImageUtils.IconSize._32));
        field.expanded();
        field.addStyle("lg");
        field.editable.set(false);
    });
    private final FormFieldText fieldTipo = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Tip");
        field.editable.set(false);
        field.width(90.0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldMarca = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Marca");
        field.editable.set(false);
        field.expanded();
    });
    private final FormFieldText fieldCor = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Cor");
        field.alignment(Pos.CENTER);
        field.width(80.0);
        field.editable.set(false);
    });
    private final FormFieldText fieldLocal = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Loc");
        field.expanded();
        field.editable.set(false);
    });

    private final FormFieldText fieldEntrega = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Entrega");
        field.alignment(Pos.CENTER);
        field.textField.setMinWidth(50);
        field.editable.set(false);
    });

    private final VBox boxGradeEstoque = FormBox.create(boxGradeEstoque -> {
        boxGradeEstoque.vertical();
        boxGradeEstoque.expanded();
    });

    private final FormTableView<VSdProdutoReposicao> tblProdutos = FormTableView.create(VSdProdutoReposicao.class, table -> {
        table.title("Produto");
        table.expanded();
        table.items.bind(produtosReposicao);
        table.fontSize(16);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("R");
                    cln.width(20.0);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdProdutoReposicao, VSdProdutoReposicao>, ObservableValue<VSdProdutoReposicao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getOrdem()));
                }).build() /*RUA*/,
                FormTableColumn.create(cln -> {
                    cln.title("PROD");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdProdutoReposicao, VSdProdutoReposicao>, ObservableValue<VSdProdutoReposicao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getProduto()));
                }).build() /*PROD*/,
                FormTableColumn.create(cln -> {
                    cln.title("COR");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdProdutoReposicao, VSdProdutoReposicao>, ObservableValue<VSdProdutoReposicao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCor()));
                    cln.alignment(Pos.CENTER);
                }).build() /*COR*/,
                FormTableColumn.create(cln -> {
                    cln.title("LOCAL");
                    cln.width(110.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdProdutoReposicao, VSdProdutoReposicao>, ObservableValue<VSdProdutoReposicao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLocal()));
                }).build() /*LOCAL*/
        );
        table.factoryRow(param -> {
            return new TableRow<VSdProdutoReposicao>() {
                @Override
                protected void updateItem(VSdProdutoReposicao item, boolean empty) {
                    super.updateItem(item, empty);
                    getStyleClass().removeAll("table-row-danger", "table-row-warning");
                    if (item != null && !empty)
                        getStyleClass().add(item.getStatus().equals("F") ? "table-row-danger" : "table-row-warning");
                }
            };
        });
    });

    public ReposicaoEstoque(MinutaView container) {
        this.containerView = container;
        init();
    }

    public void init() {
        containerView.getChildren().clear();
        containerView.getChildren().add(FormBox.create(container -> {
            container.vertical();
            container.expanded();
            container.add(FormBox.create(content -> {
                content.vertical();
                content.expanded();
                content.alignment(Pos.CENTER);
                content.add(FormBox.create(toolbar -> {
                    toolbar.horizontal();
                    toolbar.alignment(Pos.CENTER);
                    toolbar.add(FormButton.create(btnAbrirJeans -> {
                        btnAbrirJeans.title("JEANS");
                        btnAbrirJeans.icon(ImageUtils.getIcon(ImageUtils.Icon.PRODUTO, ImageUtils.IconSize._48));
                        btnAbrirJeans.addStyle("success");
                        btnAbrirJeans.contentDisplay(ContentDisplay.BOTTOM);
                        btnAbrirJeans.setAction(evt -> abrirJeans());
                    }));
                    toolbar.add(FormButton.create(btnAbrirModa -> {
                        btnAbrirModa.title("MODA");
                        btnAbrirModa.icon(ImageUtils.getIcon(ImageUtils.Icon.MODA, ImageUtils.IconSize._48));
                        btnAbrirModa.contentDisplay(ContentDisplay.BOTTOM);
                        btnAbrirModa.addStyle("dark");
                        btnAbrirModa.setAction(evt -> abrirModa());
                    }));
                }));
                content.add(FormButton.create(btnInspecionar -> {
                    btnInspecionar.title("Inspeção");
                    btnInspecionar.icon(ImageUtils.getIcon(ImageUtils.Icon.ETIQUETA_PRODUTO, ImageUtils.IconSize._32));
                    btnInspecionar.addStyle("primary").addStyle("lg");
                    btnInspecionar.setAction(evt -> initInspecao());
                }));
                content.add(FormButton.create(btnVerEstoque -> {
                    btnVerEstoque.title("Ver Estoque");
                    btnVerEstoque.icon(ImageUtils.getIcon(ImageUtils.Icon.CAIXA_ABERTA, ImageUtils.IconSize._32));
                    btnVerEstoque.addStyle("info").addStyle("lg");
                    btnVerEstoque.setAction(evt -> initVerEstoque());
                }));
            }));
            container.add(new Separator(Orientation.HORIZONTAL));
            container.add(FormButton.create(btnVoltar -> {
                btnVoltar.title("Retornar");
                btnVoltar.icon(ImageUtils.getIcon(ImageUtils.Icon.RETORNAR, ImageUtils.IconSize._32));
                btnVoltar.addStyle("warning");
                btnVoltar.setAction(evt -> containerView.init());
            }));
        }));
    }

    private void abrirJeans() {
        JPAUtils.getEntityManager().clear();
        produtosReposicao.set(FXCollections.observableList(getProdutosReposicao("J")));
        initListagem();
    }

    private void abrirModa() {
        JPAUtils.getEntityManager().clear();
        produtosReposicao.set(FXCollections.observableList(getProdutosReposicao("M")));
        initListagem();
    }

    private void initListagem() {
        containerView.getChildren().clear();
        containerView.getChildren().add(FormBox.create(container -> {
            container.vertical();
            container.expanded();
            container.add(FormBox.create(header -> {
                header.horizontal();
                header.add(FormButton.create(btnVoltar -> {
                    //btnVoltar.title("Menu");
                    btnVoltar.icon(ImageUtils.getIcon(ImageUtils.Icon.RETORNAR, ImageUtils.IconSize._24));
                    btnVoltar.addStyle("warning");
                    btnVoltar.setAction(evt -> init());
                }));
                header.add(FormBox.create(toolbar -> {
                    toolbar.horizontal();
                    toolbar.expanded();
                    toolbar.alignment(Pos.CENTER_RIGHT);
                    toolbar.add(FormButton.create(btn -> {
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CAIXA_VAZIA, ImageUtils.IconSize._32));
                        btn.addStyle("danger");
                        btn.setAction(evt -> apontarFuro(tblProdutos.selectedItem()));
                    })); // informa furo de estoque
                    toolbar.add(FormButton.create(btn -> {
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ALERT_INFO, ImageUtils.IconSize._32));
                        btn.addStyle("primary");
                        btn.setAction(evt -> {
                            VSdProdutoReposicao produto = tblProdutos.selectedItem();
                            new Fragment().show(fragment -> {
                                fragment.title("Estoque " + produto.getProduto());
                                fragment.size(235.0, 290.0);
                                fragment.box.getChildren().add(FormBox.create(boxGrade -> {
                                    boxGrade.flutuante();
                                    boxGrade.expanded();
                                    for (String grade : produto.getEstoque().split("\\|")) {
                                        boxGrade.add(FormFieldText.create(field -> {
                                            field.withoutTitle();
                                            String[] estoqueGrade = grade.split(":");
                                            field.label(estoqueGrade[0].trim());
                                            field.value.set(estoqueGrade[1].trim());
                                            field.editable.set(false);
                                            field.addStyle(estoqueGrade[1].trim().equals("0") ? "danger" : "success");
                                            field.alignment(Pos.CENTER);
                                            field.width(60.0);
                                        }).build());
                                    }
                                }));
                            });
                        });
                    })); // informações de estoque do produto
                    toolbar.add(new Separator(Orientation.VERTICAL));
                    toolbar.add(FormButton.create(btn -> {
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._32));
                        btn.addStyle("success");
                        btn.setAction(evt -> confirmarReposicao(tblProdutos.selectedItem()));
                    })); // confirmar reposição
                }));
            }));
            container.add(tblProdutos.build());
        }));
    }

    private void initInspecao() {
        containerView.getChildren().clear();
        containerView.getChildren().add(FormBox.create(container -> {
            container.vertical();
            container.expanded();
            container.add(FormBox.create(content -> {
                content.vertical();
                content.expanded();
                content.add(FormBox.create(header -> {
                    header.horizontal();
                    header.add(fieldBarcodeProduto.build());
                    header.add(FormButton.create(btn -> {
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.SEARCH, ImageUtils.IconSize._32));
                        btn.addStyle("primary").addStyle("lg");
                        btn.defaultButton(true);
                        btn.setAction(evt -> {
                            loadProduto(fieldBarcodeProduto.value.get());
                        });
                    }));
                }));
                content.add(new Separator(Orientation.HORIZONTAL));
                content.add(FormBox.create(boxDadosProduto -> {
                    boxDadosProduto.vertical();
                    boxDadosProduto.expanded();
                    boxDadosProduto.add(FormBox.create(boxFields -> {
                        boxFields.horizontal();
                        boxFields.add(fieldProduto.build());
                        boxFields.add(fieldTipo.build());
                    }));
                    boxDadosProduto.add(FormBox.create(boxFields -> {
                        boxFields.horizontal();
                        boxFields.add(fieldMarca.build());
                    }));
                    boxDadosProduto.add(FormBox.create(boxFields -> {
                        boxFields.horizontal();
                        boxFields.alignment(Pos.TOP_LEFT);
                        boxFields.add(fieldCor.build());
                        boxFields.add(fieldLocal.build());
                    }));
                    boxDadosProduto.add(FormBox.create(toolbar -> {
                        toolbar.horizontal();
                        toolbar.add(FormButton.create(btnFinalizar -> {
                            btnFinalizar.title("Confirma");
                            btnFinalizar.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._32));
                            btnFinalizar.addStyle("success").addStyle("lg");
                            btnFinalizar.setAction(evt -> informarReposicao());
                        }));
                        toolbar.add(FormButton.create(btnCancelar -> {
                            btnCancelar.title("Cancela");
                            btnCancelar.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
                            btnCancelar.addStyle("danger");
                            btnCancelar.setAction(evt -> {
                                clear();
                                fieldBarcodeProduto.clear();
                                fieldBarcodeProduto.requestFocus();
                            });
                        }));
                    }));
                }));
            }));
            container.add(new Separator(Orientation.HORIZONTAL));
            container.add(FormButton.create(btnVoltar -> {
                btnVoltar.title("Retornar");
                btnVoltar.icon(ImageUtils.getIcon(ImageUtils.Icon.RETORNAR, ImageUtils.IconSize._32));
                btnVoltar.addStyle("warning");
                btnVoltar.setAction(evt -> init());
            }));
        }));
        fieldBarcodeProduto.requestFocus();
    }

    private void initVerEstoque() {
        containerView.getChildren().clear();
        containerView.getChildren().add(FormBox.create(container -> {
            container.vertical();
            container.expanded();
            container.spacing(1);
            container.add(FormBox.create(content -> {
                content.vertical();
                content.expanded();
                content.add(FormBox.create(header -> {
                    header.horizontal();
                    header.add(fieldBarcodeProduto.build());
                    header.add(FormButton.create(btn -> {
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.SEARCH, ImageUtils.IconSize._32));
                        btn.addStyle("primary").addStyle("lg");
                        btn.defaultButton(true);
                        btn.setAction(evt -> {
                            loadEstoque(fieldBarcodeProduto.value.get());
                        });
                    }));
                }));
                content.add(new Separator(Orientation.HORIZONTAL));
                content.add(FormBox.create(boxDadosProduto -> {
                    boxDadosProduto.vertical();
                    boxDadosProduto.expanded();
                    boxDadosProduto.add(FormBox.create(boxFields -> {
                        boxFields.horizontal();
                        boxFields.add(fieldLocal.build());
                        boxFields.add(fieldEntrega.build());
                    }));
                    boxDadosProduto.add(FormBox.create(boxFields -> {
                        boxFields.horizontal();
                        boxFields.add(fieldProduto.build(), fieldCor.build());
                    }));
                    boxDadosProduto.add(FormBox.create(boxFields -> {
                        boxFields.horizontal();
                        boxFields.expanded();
                        boxFields.add(boxGradeEstoque);
                    }));
                }));
            }));
            container.add(new Separator(Orientation.HORIZONTAL));
            container.add(FormButton.create(btnVoltar -> {
                btnVoltar.title("Retornar");
                btnVoltar.icon(ImageUtils.getIcon(ImageUtils.Icon.RETORNAR, ImageUtils.IconSize._24));
                btnVoltar.addStyle("warning");
                btnVoltar.setAction(evt -> init());
            }));
        }));
        fieldBarcodeProduto.requestFocus();
    }

    private void clear() {
        fieldProduto.clear();
        fieldTipo.clear();
        fieldMarca.clear();
        fieldCor.clear();
        fieldLocal.clear();
        produtoBarra.set(null);
        tipoEntradaManual.set(false);
    }

    private void loadProduto(String barcode) {
        clear();
        String[] codigoManual = barcode.split("-");
        if (barcode.length() < 16 && codigoManual.length < 2) {
            MessageBox.create(message -> {
                message.message("Código de barras: " + barcode + " inválido para produto!");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showFullScreen();
            });
            fieldBarcodeProduto.clear();
            fieldBarcodeProduto.requestFocus();
            return;
        }

        if (produtoBarra != null)
            JPAUtils.clearEntity(produtoBarra);
        if (codigoManual.length < 2) {
            VSdDadosProdutoBarra produto = getDadosProdutoBarra(barcode);
            if (produto != null) {
                produtoBarra.set(produto);
                produtoDados.set(getDadosProduto(produto.getCodigo()));

                fieldProduto.setValue(produto.getCodigo());
                fieldTipo.setValue(produtoDados.get().getTipo().equals("J") ? "JEANS" : "MODA");
                fieldMarca.setValue(produtoDados.get().getMarca());
                fieldCor.setValue(produto.getCor());
                fieldLocal.setValue(produto.getLocal());
            } else {
                fieldBarcodeProduto.clear();
                fieldBarcodeProduto.requestFocus();
                return;
            }
        } else {
            VSdDadosProdutoBarra produto = getDadosProdutoBarra(codigoManual[0], codigoManual[1]);
            if (produto != null) {
                tipoEntradaManual.set(true);
                produtoBarra.set(produto);
                produtoDados.set(getDadosProduto(produto.getCodigo()));

                fieldProduto.setValue(produto.getCodigo());
                fieldTipo.setValue(produtoDados.get().getTipo().equals("J") ? "JEANS" : "MODA");
                fieldMarca.setValue(produtoDados.get().getMarca());
                fieldCor.setValue(produto.getCor());
                fieldLocal.setValue(produto.getLocal());
            } else {
                MessageBox.create(message -> {
                    message.message("Produto e cor digitados não encontrados.");
                    message.type(MessageBox.TypeMessageBox.ALERT);
                    message.showFullScreen();
                });
                fieldBarcodeProduto.clear();
                fieldBarcodeProduto.requestFocus();
                return;
            }
        }
    }

    private void loadEstoque(String barcode) {
        fieldLocal.clear();
        fieldProduto.clear();
        fieldCor.clear();
        fieldEntrega.clear();
        boxGradeEstoque.getChildren().clear();

        String[] codigoManual = barcode.split("-");
        if (barcode.length() < 16 && codigoManual.length < 2) {
            MessageBox.create(message -> {
                message.message("Código de barras: " + barcode + " inválido para produto!");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showFullScreen();
            });
            fieldBarcodeProduto.clear();
            fieldBarcodeProduto.requestFocus();
            return;
        }

        VSdEstoqueProdutoCor produto = null;
        if (codigoManual.length < 2) {
            produto = getEstoqueProdutoBarra(barcode);
        } else {
            produto = getEstoqueProdutoBarra(codigoManual[0], codigoManual[1]);
        }
        if (produto != null) {
            JPAUtils.getEntityManager().refresh(produto);
            produtoDados.set(getDadosProduto(produto.getCodigo()));
            if (produtoDados.get() != null) JPAUtils.getEntityManager().refresh(produtoDados.get());
            fieldLocal.setValue(produto.getLocal());
            fieldProduto.setValue(produto.getCodigo());
            fieldCor.setValue(produto.getCor());
            fieldEntrega.setValue(produtoDados.get().getDtEntrega().getMonth().getDisplayName(TextStyle.SHORT, Locale.getDefault()).toUpperCase());
            VSdEstoqueProdutoCor finalProduto = produto;
            boxGradeEstoque.getChildren().add(FormBox.create(boxGrade -> {
                boxGrade.flutuante();
                boxGrade.expanded();
                for (String grade : finalProduto.getEstoque().split("\\|")) {
                    boxGrade.add(FormBox.create(boxTam -> {
                        boxTam.vertical();
                        boxTam.alignment(Pos.CENTER);
                        boxTam.spacing(0);
                        boxTam.add(FormFieldText.create(field -> {
                            field.withoutTitle();
                            String[] estoqueGrade = grade.split(":");
                            field.label(estoqueGrade[0].trim());
                            field.value.set(estoqueGrade[1].trim());
                            field.editable.set(false);
                            field.addStyle(estoqueGrade[1].trim().equals("0") ? "danger" : "success");
                            field.alignment(Pos.CENTER);
                            field.width(55.0);
                        }).build());
                        VSdDadosProdutoBarra produtoBarra = new FluentDao().selectFrom(VSdDadosProdutoBarra.class)
                                .where(it -> it.equal("codigo", finalProduto.getCodigo()).equal("cor", finalProduto.getCor()).equal("tam", grade.split(":")[0].trim())).singleResult();
                        Label label = new Label(produtoBarra == null ? "" : produtoBarra.getLocal());
                        label.setFont(Font.font("System", 9));
                        boxTam.add(label);
                    }));
                }
            }));
        } else {
            MessageBox.create(message -> {
                message.message("Produto e cor digitados não encontrados.");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showFullScreen();
            });
            fieldLocal.clear();
            fieldProduto.clear();
            fieldCor.clear();
            boxGradeEstoque.getChildren().clear();
        }
        fieldBarcodeProduto.clear();
        fieldBarcodeProduto.requestFocus();
    }


    private void informarReposicao() {
        try {
            incluirAvisoReposicao(produtoBarra.get(), produtoDados.get());
            SysLogger.addSysDelizLog("Inspeção Expedição", TipoAcao.MOVIMENTAR, produtoBarra.get().getCodigo(),
                    "Incluído reposição do produto na cor " + produtoBarra.get().getCor() + " na inspeção da expedição.");
        } catch (SQLException e) {
            e.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(e);
                message.showAndWait();
            });
        }

        clear();
        fieldBarcodeProduto.clear();
        fieldBarcodeProduto.requestFocus();
    }

    private void apontarFuro(VSdProdutoReposicao produto) {
        try {
            informarFuroEstoque(produto);
            SysLogger.addSysDelizLog("Inspeção Expedição", TipoAcao.MOVIMENTAR, produto.getProduto(),
                    "Apontamento de furo de estoque para o produto na cor " + produto.getCor() +
                            " pela reposisção " + (produto.getTipo().equals("J") ? "JEANS" : "MODA"));
            produtosReposicao.remove(produto);
            tblProdutos.refresh();
        } catch (SQLException e) {
            e.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(e);
                message.showAndWait();
            });
        }
    }

    private void confirmarReposicao(VSdProdutoReposicao produto) {
        try {
            resolveReposicao(produto);
            SysLogger.addSysDelizLog("Inspeção Expedição", TipoAcao.MOVIMENTAR, produto.getProduto(),
                    "Confirmando reposição do produto na cor " + produto.getCor() +
                            " pela reposisção " + (produto.getTipo().equals("J") ? "JEANS" : "MODA") +
                            " na prateleira.");
            produtosReposicao.remove(produto);
            tblProdutos.refresh();
        } catch (SQLException e) {
            e.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(e);
                message.showAndWait();
            });
        }
    }

}
