package sysdeliz2.views.expedicao.mostruario;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Separator;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;
import sysdeliz2.controllers.views.expedicao.GerenciarMostruariosController;
import sysdeliz2.models.sysdeliz.comercial.SdMostrRep;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.ti.Represen;
import sysdeliz2.models.view.expedicao.VSdDistribuicaoMostruario;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class GerenciarMostruariosView extends GerenciarMostruariosController {

    // <editor-fold defaultstate="collapsed" desc="bean">
    private final ListProperty<SdMostrRep> mostruariosBean = new SimpleListProperty<>();
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fields">
    private final FormFieldMultipleFind<Colecao> fieldColecaoMostruario = FormFieldMultipleFind.create(Colecao.class, field -> {
        field.title("Coleção");
        field.width(120.0);
    });
    private final FormFieldMultipleFind<Represen> fieldRepresentantes = FormFieldMultipleFind.create(Represen.class, field -> {
        field.title("Representante");
        field.width(170.0);
    });
    private final FormFieldMultipleFind<Marca> fieldMarcas = FormFieldMultipleFind.create(Marca.class, field -> {
        field.title("Marca");
        field.width(80.0);
    });

    private final FormTableView<SdMostrRep> tblMostruarios = FormTableView.create(SdMostrRep.class, table -> {
        table.title("Mostruários");
        table.expanded();
        table.items.bind(mostruariosBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Col.");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMostrRep, SdMostrRep>, ObservableValue<SdMostrRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getColvenda()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Col.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Tipo");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMostrRep, SdMostrRep>, ObservableValue<SdMostrRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTipo()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<SdMostrRep, String>(){
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(item.equals("M") ? "Moda" : "Jeans");
                                }
                            }
                        };
                    });
                }).build() /*Tipo*/,
                FormTableColumn.create(cln -> {
                    cln.title("Mostr.");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMostrRep, SdMostrRep>, ObservableValue<SdMostrRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getMostruario()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Mostr.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Marca");
                    cln.width(40.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMostrRep, SdMostrRep>, ObservableValue<SdMostrRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getMarca()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Marca*/,
                FormTableColumn.create(cln -> {
                    cln.title("Representante");
                    cln.width(320.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMostrRep, SdMostrRep>, ObservableValue<SdMostrRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodrep().toString()));
                }).build() /*Representante*/,
                FormTableColumn.create(cln -> {
                    cln.title("Status");
                    cln.width(100.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMostrRep, SdMostrRep>, ObservableValue<SdMostrRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getStatusExpedicao()));
                }).build() /*Status*/
        );
    });

    private final FormFieldText fieldColecaoProduto = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Coleção");
        field.addStyle("lg");
        field.width(140.0);
        field.toUpper();
    });
    private final FormFieldText fieldCodigoProduto = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Código");
        field.addStyle("lg");
        field.width(170.0);
        field.toUpper();
    });
    private final FormFieldText fieldColecao = FormFieldText.create(field -> {
        field.title("Coleção");
        field.width(100.0);
        field.editable.set(false);
        field.addStyle("lg");
    });
    private final FormFieldText fieldCodigo = FormFieldText.create(field -> {
        field.title("Código");
        field.width(100.0);
        field.editable.set(false);
        field.addStyle("lg");
    });
    private final FormFieldText fieldMostruarioTipo = FormFieldText.create(field -> {
        field.title("Tipo");
        field.width(80.0);
        field.editable.set(false);
        field.addStyle("lg");
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldMostruarioMarca = FormFieldText.create(field -> {
        field.title("Marca");
        field.expanded();
//        field.width(110.0);
        field.addStyle("lg");
        field.editable.set(false);
    });
    private final FormFieldText fieldMostruarioCaixas = FormFieldText.create(field -> {
        field.title("Caixas");
        field.width(100.0);
        field.editable.set(false);
        field.addStyle("lg");
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldMostruarioCaixasExcluidas = FormFieldText.create(field -> {
        field.title("Exceto");
        field.expanded();
        field.alignment(Pos.CENTER_LEFT);
        field.label(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
//        field.width(60.0);
        field.editable.set(false);
    });
    // </editor-fold>

    public GerenciarMostruariosView() {
        super("Gerenciar Mostruários", ImageUtils.getImage(ImageUtils.Icon.PRIORIZAR_PRODUTO));
        init();
    }

    private void init() {
        super.box.getChildren().add(FormBox.create(container -> {
            container.horizontal();
            container.expanded();
            container.add(FormBox.create(left -> {
                left.vertical();
                left.expanded();
                left.add(FormBox.create(boxFilter -> {
                    boxFilter.horizontal();
                    boxFilter.add(FormTitledPane.create(filter -> {
                        filter.filter();
                        filter.add(FormBox.create(boxFieldsFilter -> {
                            boxFieldsFilter.horizontal();
                            boxFieldsFilter.expanded();
                            boxFieldsFilter.add(FormBox.create(boxFields -> {
                                boxFields.vertical();
                                boxFields.add(fieldColecaoMostruario.build());
                                boxFields.add(fieldMarcas.build());
                            }));
                            boxFieldsFilter.add(FormBox.create(boxFields -> {
                                boxFields.vertical();
                                boxFields.add(fieldRepresentantes.build());
                            }));
                        }));
                        filter.find.setOnAction(evt -> {
                            carregarMostruarios(fieldColecaoMostruario.objectValues.get(),
                                    fieldMarcas.objectValues.get(),
                                    fieldRepresentantes.objectValues.get());
                        });
                        filter.clean.setOnAction(evt -> {
                            fieldColecaoMostruario.clear();
                            fieldRepresentantes.clear();
                            fieldMarcas.clear();
                            tblMostruarios.clear();
                        });
                    }));
                }));
                left.add(tblMostruarios.build());
            }));
            container.add(new Separator(Orientation.VERTICAL));
            container.add(FormBox.create(right -> {
                right.vertical();
                right.expanded();
                right.add(FormBox.create(content -> {
                    content.vertical();
                    content.expanded();
                    content.add(FormBox.create(header -> {
                        header.horizontal();
                        header.add(fieldColecaoProduto.build(), fieldCodigoProduto.build());
                        header.add(FormButton.create(btnProcurar -> {
                            btnProcurar.icon(ImageUtils.getIcon(ImageUtils.Icon.SEARCH, ImageUtils.IconSize._24));
                            btnProcurar.addStyle("info");
                            btnProcurar.setAction(evt -> loadProdutoMostruario(fieldColecaoProduto.value.get(), fieldCodigoProduto.value.get()));
                        }));
                    }));
                    content.add(new Separator(Orientation.HORIZONTAL));
                    content.add(FormBox.create(boxDadosProduto -> {
                        boxDadosProduto.vertical();
                        boxDadosProduto.expanded();
                        boxDadosProduto.add(FormBox.create(boxFields -> {
                            boxFields.horizontal();
                            boxFields.add(fieldColecao.build());
                            boxFields.add(fieldCodigo.build());
                            boxFields.add(fieldMostruarioMarca.build());
                        }));
                        boxDadosProduto.add(FormBox.create(boxFields -> {
                            boxFields.horizontal();
                            boxFields.alignment(Pos.TOP_LEFT);
                            boxFields.add(fieldMostruarioTipo.build());
                            boxFields.add(fieldMostruarioCaixas.build());
                            boxFields.add(fieldMostruarioCaixasExcluidas.build());
                        }));
                    }));
                }));
            }));
        }));
    }

    private void loadProdutoMostruario(String colecao, String codigo) {
        fieldMostruarioCaixas.clear();
        fieldMostruarioCaixasExcluidas.clear();
        fieldMostruarioTipo.clear();
        fieldMostruarioMarca.clear();
        fieldCodigo.clear();
        fieldColecao.clear();

        if (colecao == null || codigo == null) {
            MessageBox.create(message -> {
                message.message("É necessário preencher os campos de consulta: COLEÇÃO e CÓDIGO");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showAndWait();
            });
            return;
        }

        VSdDistribuicaoMostruario produto = getProdutoCodigo(colecao, codigo);
        if (produto != null) {
            fieldColecao.setValue(produto.getColvenda());
            fieldCodigo.setValue(produto.getCodigo());
            fieldMostruarioMarca.setValue(produto.getDescmarca());
            fieldMostruarioTipo.setValue(produto.getDesctipo());
            fieldMostruarioCaixas.setValue(produto.getCaixas());
            fieldMostruarioCaixasExcluidas.setValue(produto.getMenos());
        } else {
            MessageBox.create(message -> {
                message.message("Produto não encontrado ou já concluído no mostruário!");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showFullScreen();
            });
        }

        fieldColecaoProduto.clear();
        fieldCodigoProduto.clear();
        fieldColecaoProduto.requestFocus();
    }

    private void carregarMostruarios(ObservableList<Colecao> colecoes, ObservableList<Marca> marcas, ObservableList<Represen> represens) {
        Object[] codigoColecao = colecoes.stream().map(Colecao::getCodigo).toArray();
        Object[] marcasProduto = marcas.stream().map(Marca::getCodigo).toArray();
        Object[] representantes = represens.stream().map(Represen::getCodRep).toArray();
        AtomicReference<List<SdMostrRep>> mostruarios = new AtomicReference<>();

        new RunAsyncWithOverlay(this).exec(task -> {
            mostruarios.set(getMostruarios(codigoColecao, marcasProduto, representantes));
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                mostruariosBean.set(FXCollections.observableList(mostruarios.get()));
            }
        });
    }
}
