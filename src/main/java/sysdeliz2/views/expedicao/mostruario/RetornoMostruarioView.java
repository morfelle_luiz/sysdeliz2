package sysdeliz2.views.expedicao.mostruario;

import javafx.application.Platform;
import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.util.Callback;
import sysdeliz2.controllers.views.expedicao.RetornoMostruarioController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.sysdeliz.SdItensCaixaRemessa;
import sysdeliz2.models.sysdeliz.SdMostrRepBarraRetorno;
import sysdeliz2.models.sysdeliz.comercial.SdMostrRep;
import sysdeliz2.models.ti.Represen;
import sysdeliz2.models.view.VSdDadosProdutoBarra;
import sysdeliz2.models.view.expedicao.VSdDevolucaoMostRep;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.exceptions.FormValidationException;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.InputBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsync;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.SysLogger;

import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class RetornoMostruarioView extends RetornoMostruarioController {

    // <editor-fold defaultstate="collapsed" desc="beans">
    private final ListProperty<SdMostrRep> mostruariosColecaoBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<VSdDevolucaoMostRep> itensDevolucaoBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final BooleanProperty emLeitura = new SimpleBooleanProperty(false);
    private final IntegerProperty qtdeDevolucaoBean = new SimpleIntegerProperty(0);
    private final IntegerProperty qtdeDevolvidoBean = new SimpleIntegerProperty(0);
    private final IntegerProperty saldoDevolucaoBean = new SimpleIntegerProperty(0);
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="components">
    private final FormFieldText fieldRepresentante = FormFieldText.create(field -> {
        field.title("Código Representante");
        field.addStyle("lg");
        field.expanded();
        field.tooltip("Pressione F4 para procurar o representante.");
        field.alignment(Pos.CENTER);
        field.editable.bind(emLeitura.not());
        field.keyReleased(event -> {
            if (field.value.length().greaterThanOrEqualTo(4).get() && field.editable.get()) {
                carregarMostruariosRepresentante(field.value.get());
            }

            if (event.getCode().equals(KeyCode.F4) && field.editable.get()){
                InputBox<Represen> fieldRepresentante = InputBox.build(Represen.class, boxInput -> {
                    boxInput.message("Selecione um representante:");
                    boxInput.showAndWait();
                });
                if (fieldRepresentante.value.get() == null)
                    return;

                field.value.set(fieldRepresentante.value.get().getCodRep());
                carregarMostruariosRepresentante(field.value.get());
            }
        });
    });
    private final FormFieldText fieldNomeRepresentante = FormFieldText.create(field -> {
        field.title("Representante");
        field.editable.set(false);
    });
    private final FormTableView<SdMostrRep> tblMostruariosRep = FormTableView.create(SdMostrRep.class, table -> {
        table.title("Mostruários");
        table.expanded();
        table.items.bind(mostruariosColecaoBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Mostr.");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMostrRep, SdMostrRep>, ObservableValue<SdMostrRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getMostruario()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Mostr.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Marca");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMostrRep, SdMostrRep>, ObservableValue<SdMostrRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getMarca()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Marca*/,
                FormTableColumn.create(cln -> {
                    cln.title("Tipo");
                    cln.width(40.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMostrRep, SdMostrRep>, ObservableValue<SdMostrRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTipo()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<SdMostrRep, String>() {
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(ImageUtils.getIcon(item.equals("J") ? ImageUtils.Icon.PRODUTO : ImageUtils.Icon.MODA, ImageUtils.IconSize._32));
                                }
                            }
                        };
                    });
                }).build() /*Tipo*/,
                FormTableColumn.create(cln -> {
                    cln.title("Pedido");
                    cln.width(150.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMostrRep, SdMostrRep>, ObservableValue<SdMostrRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumero()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Pedido*/,
                FormTableColumn.create(cln -> {
                    cln.title("Remessa");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdMostrRep, SdMostrRep>, ObservableValue<SdMostrRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getRemessa()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Remessa*/
        );
        table.factoryRow(param -> {
            return new TableRow<SdMostrRep>() {
                @Override
                protected void updateItem(SdMostrRep item, boolean empty) {
                    super.updateItem(item, empty);
                    getStyleClass().add("lg");
                    getStyleClass().remove("table-row-success");
                    if (item != null && !empty) {
                        getStyleClass().add(item.getStatus().equals("D") ? "table-row-success" : "");
                    }
                }
            };
        });
        table.indices(
                table.indice("Pendente", "default", ""),
                table.indice("Devolvido", "success", "")
        );
    });
    private final FormFieldText fieldQtdeDevolucao = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("P/ Retorno");
        field.editable.set(false);
        field.expanded();
        field.alignment(Pos.CENTER);
        field.value.bind(qtdeDevolucaoBean.asString());
        field.addStyle("warning").addStyle("lg");
    });
    private final FormFieldText fieldQtdeDevolvido = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Retornado");
        field.editable.set(false);
        field.expanded();
        field.alignment(Pos.CENTER);
        field.value.bind(qtdeDevolvidoBean.asString());
        field.addStyle("success").addStyle("lg");
    });
    private final FormFieldText fieldSaldoDevolucao = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Saldo");
        field.editable.set(false);
        field.expanded();
        field.alignment(Pos.CENTER);
        field.value.bind(saldoDevolucaoBean.asString());
        field.addStyle("primary").addStyle("lg");
    });
    
    private final FormTableView<VSdDevolucaoMostRep> tblItensDevolucao = FormTableView.create(VSdDevolucaoMostRep.class, table -> {
        table.title("Itens p/ Devolução");
        table.expanded();
        table.items.bind(itensDevolucaoBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Produto");
                    cln.width(400.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDevolucaoMostRep, VSdDevolucaoMostRep>, ObservableValue<VSdDevolucaoMostRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo()));
                }).build() /*Produto*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cor");
                    cln.width(90.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDevolucaoMostRep, VSdDevolucaoMostRep>, ObservableValue<VSdDevolucaoMostRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCor().getCor()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Cor*/,
                FormTableColumn.create(cln -> {
                    cln.title("Tam");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDevolucaoMostRep, VSdDevolucaoMostRep>, ObservableValue<VSdDevolucaoMostRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTam()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Tam*/,
                FormTableColumn.create(cln -> {
                    cln.title("Pend.");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDevolucaoMostRep, VSdDevolucaoMostRep>, ObservableValue<VSdDevolucaoMostRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Pend.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Devolv.");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDevolucaoMostRep, VSdDevolucaoMostRep>, ObservableValue<VSdDevolucaoMostRep>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtded()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Devolv.*/
        );
        table.factoryRow(param -> {
            return new TableRow<VSdDevolucaoMostRep>() {
                @Override
                protected void updateItem(VSdDevolucaoMostRep item, boolean empty) {
                    super.updateItem(item, empty);
                    getStyleClass().add("lg");
                    getStyleClass().remove("table-row-success");
                    if (item != null && !empty) {
                        getStyleClass().add(item.getStatus().equals("D") ? "table-row-success" : "");
                    }
                }
            };
        });
    });
    private final FormFieldText fieldBarcodeProduto = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label(ImageUtils.getIcon(ImageUtils.Icon.QRCODE, ImageUtils.IconSize._32));
        field.addStyle("lg");
        field.editable.bind(emLeitura);
        field.keyPressed(evt -> {
            if (evt.getCode().equals(KeyCode.ENTER)) {
                try {
                    leituraBarraProduto(field.value.get());
                    field.clear();
                    field.requestFocus();
                } catch (SQLException e) {
                    e.printStackTrace();
                    ExceptionBox.build(message -> {
                        message.exception(e);
                        message.showAndWait();
                    });
                }
            }
        });
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldProduto = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Produto");
        field.addStyle("lg");
        field.expanded();
        field.editable.set(false);
    });
    private final FormFieldText fieldCor = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Cor");
        field.addStyle("lg");
        field.expanded();
        field.editable.set(false);
    });
    private final FormFieldText fieldTamanho = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Tam");
        field.addStyle("lg");
        field.editable.set(false);
        field.width(100.0);
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText fieldLocal = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Local");
        field.addStyle("lg").addStyle("info");
        field.editable.set(false);
        field.width(250.0);
        field.alignment(Pos.CENTER);
    });
    // </editor-fold>

    public RetornoMostruarioView() {
        super("Retorno de Mostruário", ImageUtils.getImage(ImageUtils.Icon.ENTRADA_PRODUTO), Globals.isPortatil());

        if (Globals.isPortatil()) {
            JPAUtils.getEntityManager();
        }
        init();
        Platform.runLater(() -> {
            fieldRepresentante.requestFocus();
        });
    }

    private void init() {
        saldoDevolucaoBean.bind(qtdeDevolucaoBean.subtract(qtdeDevolvidoBean));
        super.box.getChildren().add(FormBox.create(content -> {
            content.horizontal();
            content.expanded();
            content.add(FormBox.create(left -> {
                left.vertical();
                left.add(FormBox.create(boxCarregarRemessa -> {
                    boxCarregarRemessa.horizontal();
                    boxCarregarRemessa.alignment(Pos.BOTTOM_LEFT);
                    boxCarregarRemessa.add(fieldRepresentante.build());
                    boxCarregarRemessa.add(FormButton.create(btn -> {
                        btn.title("Parar Leitura");
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._32));
                        btn.addStyle("lg").addStyle("danger");
                        btn.disable.bind(emLeitura.not());
                        btn.setAction(evt -> pararLeitura());
                    }));
                }));
                left.add(new Separator(Orientation.HORIZONTAL));
                left.add(FormBox.create(boxSaldoDevolucao -> {
                    boxSaldoDevolucao.horizontal();
                    boxSaldoDevolucao.add(fieldQtdeDevolucao.build(), fieldQtdeDevolvido.build(), fieldSaldoDevolucao.build());
                }));
                left.add(new Separator(Orientation.HORIZONTAL));
                left.add(fieldNomeRepresentante.build());
                left.add(tblMostruariosRep.build());
            }));
            content.add(FormBox.create(right -> {
                right.vertical();
                right.expanded();
                right.add(FormBox.create(boxProdutoLeitura -> {
                    boxProdutoLeitura.vertical();
                    boxProdutoLeitura.title("Produto em Leitura");
                    boxProdutoLeitura.add(fieldBarcodeProduto.build());
                    boxProdutoLeitura.add(new Separator(Orientation.HORIZONTAL));
                    boxProdutoLeitura.add(fieldProduto.build());
                    boxProdutoLeitura.add(FormBox.create(boxDadosProduto -> {
                        boxDadosProduto.horizontal();
                        boxDadosProduto.add(fieldCor.build());
                        boxDadosProduto.add(fieldTamanho.build());
                        boxDadosProduto.add(fieldLocal.build());
                    }));
                }));
                right.add(tblItensDevolucao.build());
            }));
        }));
    }

    private void limparDadosProduto() {
        fieldProduto.clear();
        fieldCor.clear();
        fieldTamanho.clear();
        fieldLocal.clear();
    }

    private void pararLeitura() {
        emLeitura.set(false);

        mostruariosColecaoBean.clear();
        fieldNomeRepresentante.clear();

        limparDadosProduto();
        itensDevolucaoBean.clear();
        qtdeDevolvidoBean.set(0);
        qtdeDevolucaoBean.set(0);

        fieldRepresentante.clear();
        fieldRepresentante.requestFocus();
    }

    private void carregarMostruariosRepresentante(String codrep) {
        new RunAsyncWithOverlay(this).exec(task -> {
            JPAUtils.clearEntitys(mostruariosColecaoBean.get());
            getMostruariosNaoDevolvidosRepresentante(codrep);
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                if (mostruarios.size() == 0) {
                    MessageBox.create(message -> {
                        message.message("Não foi encontrado nenhum mostruário pendente para devolução para o representante: " + codrep);
                        message.type(MessageBox.TypeMessageBox.WARNING);
                        message.showFullScreenMobile();
                    });
                    fieldRepresentante.clear();
                    fieldRepresentante.requestFocus();
                } else {
                    List<String> colecoesPendentes = mostruarios.stream().map(SdMostrRep::getColvenda).distinct().collect(Collectors.toList());
                    colecoesPendentes.sort(String::compareTo);
                    Fragment.ReturnType returnFragment = new Fragment().show(fragment -> {
                        fragment.title("Selecionar Coleção");
                        fragment.size(200.0, 350.0);
                        final FormListView<String> lviewColecoesRep = FormListView.create(list -> {
                            list.withoutHeader();
                            list.expanded();
                            list.items.set(FXCollections.observableList(colecoesPendentes));
                            list.listProperties().setCellFactory(param -> {
                                return new ListCell<String>() {
                                    @Override
                                    protected void updateItem(String item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setGraphic(null);
                                        setAlignment(Pos.CENTER);
                                        setPrefHeight(60.0);
                                        if (item != null && !empty) {
                                            setGraphic(FormButton.create(btn -> {
                                                btn.title(item);
                                                btn.width(160.0);
                                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.PROXIMO, ImageUtils.IconSize._32));
                                                btn.addStyle("lg").addStyle(getIndex() % 2 == 0 ? "info" : "secundary");
                                                btn.setAction(evt -> {
                                                    fragment.close(Fragment.ReturnType.OK);
                                                    abrirMostruarioColecao(codrep, item);
                                                });
                                            }));
                                        }
                                    }
                                };
                            });
                        });
                        fragment.box.getChildren().add(lviewColecoesRep.build());
                    });

                    if (returnFragment.equals(Fragment.ReturnType.CANCEL)) {
                        fieldRepresentante.clear();
                        fieldRepresentante.requestFocus();
                    }
                }
            }
        });
    }

    private void abrirMostruarioColecao(String codrep, String colecao) {
        AtomicReference<Exception> expMostruarios = new AtomicReference<>();
        new RunAsyncWithOverlay(this).exec(task -> {
            try {
                getMostruariosColecaoRepresentante(codrep, colecao);
            } catch (SQLException e) {
                expMostruarios.set(e);
                return ReturnAsync.EXCEPTION.value;
            } catch (FormValidationException e) {
                expMostruarios.set(e);
                return ReturnAsync.NOT_FOUND.value;
            }
            SysLogger.addSysDelizLog("Retorno Mostruario", TipoAcao.ENTRAR, codrep,
                    (Globals.getUsuarioLogado() == null ? Globals.getNomeUsuario() : Globals.getUsuarioLogado().getUsuario()) +
                    " carregando mostruários para retorno na coleção " + colecao);
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                fieldNomeRepresentante.setValue(mostruariosColecao.get(0).getCodrep().toString());
                mostruariosColecaoBean.set(FXCollections.observableList(mostruariosColecao));
                emLeitura.set(true);
                carregarItensMostruarioDevolucao(codrep, colecao);
            } else if (taskReturn.equals(ReturnAsync.EXCEPTION.value)) {
                ExceptionBox.build(exceptionBox -> {
                    exceptionBox.exception(expMostruarios.get());
                    exceptionBox.showAndWait();
                });
            } else if (taskReturn.equals(ReturnAsync.NOT_FOUND.value)) {
                MessageBox.create(message -> {
                    message.message(expMostruarios.get().getMessage());
                    message.type(MessageBox.TypeMessageBox.ALERT);
                    message.showFullScreenMobile();
                });
                fieldRepresentante.clear();
                fieldRepresentante.requestFocus();
            }
        });
    }

    private void carregarItensMostruarioDevolucao(String codrep, String colecao) {
        new RunAsyncWithOverlay(this).exec(task -> {
            JPAUtils.clearEntitys(itensDevolucaoBean.get());
            itensDevolucaoBean.get().clear();
            carregarItensMostruario(codrep, colecao);
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                itensDevolucaoBean.set(FXCollections.observableList(itensDevolucao));
                calculoRetornoItens();

                fieldBarcodeProduto.clear();
                fieldBarcodeProduto.requestFocus();
            }
        });
    }

    private void calculoRetornoItens() {
        //itensDevolucaoBean.forEach(VSdDevolucaoMostRep::refresh);
        qtdeDevolucaoBean.set(itensDevolucaoBean.stream().mapToInt(VSdDevolucaoMostRep::getQtde).sum() + itensDevolucaoBean.stream().mapToInt(VSdDevolucaoMostRep::getQtded).sum());
        qtdeDevolvidoBean.set(itensDevolucaoBean.stream().mapToInt(VSdDevolucaoMostRep::getQtded).sum());
    }

    private boolean leituraBarraProduto(String barra) throws SQLException {
        if (barra != null) {
            String barra28 = barra.substring(0, 6);
            limparDadosProduto();

            // validação de barra já lida
            SdMostrRepBarraRetorno barraRetorno = new FluentDao().selectFrom(SdMostrRepBarraRetorno.class)
                    .where(eb->eb.equal("barra", barra)).findAny();
            if (barraRetorno != null) {
                MessageBox.create(message -> {
                    message.message("A barra " + barra + " lida já foi retornada no rep.: " + barraRetorno.getCodrep() + " coleção: " + barraRetorno.getColecao());
                    message.type(MessageBox.TypeMessageBox.ALERT);
                    message.showFullScreenMobile();
                });
                return false;
            }

            // validação se a barra28 é de um produto válido
            VSdDadosProdutoBarra produtoBarra = new FluentDao().selectFrom(VSdDadosProdutoBarra.class)
                    .where(eb -> eb.equal("barra28", barra28)).singleResult();
            if (produtoBarra == null) {
                MessageBox.create(message -> {
                    message.message("Barra lida não pertence a um produto cadastrado.");
                    message.type(MessageBox.TypeMessageBox.ALERT);
                    message.showFullScreenMobile();
                });
                return false;
            }

            // validação de o produto é um produto do mostruário
            AtomicReference<VSdDevolucaoMostRep> itemDevolucao = new AtomicReference<>(null);
            itensDevolucaoBean.stream().filter(item -> item.getBarra28().equals(barra28)).findFirst().ifPresent(item -> itemDevolucao.set(item));
            if (itemDevolucao.get() == null) {
                MessageBox.create(message -> {
                    message.message("O produto da barra lida não pertence ao mostruário do representante.");
                    message.type(MessageBox.TypeMessageBox.ALERT);
                    message.showFullScreenMobile();
                });
                return false;
            }

            // validação se a barra foi lida em uma remessa do representante
            SdItensCaixaRemessa barraCaixa = new FluentDao().selectFrom(SdItensCaixaRemessa.class)
                    .where(eb -> eb.equal("id.barra", barra).equal("status","E")).singleResult();
            if (barraCaixa != null){
                Integer remessa = barraCaixa.getId().getCaixa().getRemessa().getRemessa();
                if (!itemDevolucao.get().getRemessas().contains(String.valueOf(remessa))) {
                    SdItensCaixaRemessa finalBarraCaixa = barraCaixa;
                    SdMostrRep mostruarioBarra = new FluentDao().selectFrom(SdMostrRep.class)
                            .where(eb -> eb
                                    .equal("remessa", finalBarraCaixa.getId().getCaixa().getRemessa().getRemessa())
                                    .equal("tipo", itemDevolucao.get().getTipo())).findAny();
                    MessageBox.create(message -> {
                        message.message("A barra lida não está vinculada a este representante. Dados da barra:\n" +
                                "Remessa: " + mostruarioBarra.getRemessa() + "\n" +
                                "Mostruário: " + mostruarioBarra.getMostruario() + "\n" +
                                "Represen: " + mostruarioBarra.getCodrep().toString());
                        message.type(MessageBox.TypeMessageBox.WARNING);
                        message.showFullScreenMobile();
                    });
                    return false;
                }
            }

            // validação se existem peças devolvidas para retorno
            if (itemDevolucao.get().getQtde() == 0 && itemDevolucao.get().getQtded() == 0) {
                MessageBox.create(message -> {
                    message.message("Esta barra não consta em uma NF devolvida do representante.");
                    message.type(MessageBox.TypeMessageBox.ALERT);
                    message.showFullScreenMobile();
                });
                return false;
            }

            // validação se existem peças para retorno
            if (itemDevolucao.get().getQtde() == 0) {
                MessageBox.create(message -> {
                    message.message("O produto desta barra já foi todo retornado do mostruário.");
                    message.type(MessageBox.TypeMessageBox.ALERT);
                    message.showFullScreenMobile();
                });
                return false;
            }

            // devolvendo barra para leitura
            barraCaixa = devolverBarraParaEstoque(itemDevolucao.get(), barraCaixa);

            // retornando peça no pedido
            retornaPecaPedido(itemDevolucao.get(), barraCaixa);

            // marcando peca como devolvida no mostruario
            devolverItemMostruario(itemDevolucao.get(), barraCaixa);

            // guardando barra lida e salvando log
            guardarBarraRetorno(barra, itemDevolucao.get());
            SysLogger.addSysDelizLog("Retorno Mostruario", TipoAcao.CADASTRAR, itemDevolucao.get().getCodrep(),
                    "Retornando barra " + barra + " do código " + itemDevolucao.get().getCodigo().getCodigo() + " do pedido " + barraCaixa.getPedido());

            // colocando item em devolução para primeiro da lista de itens
            itensDevolucaoBean.remove(itemDevolucao.get());
            itensDevolucaoBean.add(0, itemDevolucao.get());

            // atualizando dados de tela
            JPAUtils.getEntityManager().refresh(itemDevolucao.get());
            new RunAsync().exec(task -> {
                verificaMostruariosDevolvidos();
                return ReturnAsync.OK.value;
            }).addTaskEndNotification(taskReturn -> {
                if (taskReturn.equals(ReturnAsync.OK.value)) {
                    mostruariosColecaoBean.set(FXCollections.observableList(mostruariosColecao));
                    tblMostruariosRep.refresh();
                }
            });
            tblItensDevolucao.refresh();
            calculoRetornoItens();

            //carregando dados do produto lido
            fieldProduto.setValue(itemDevolucao.get().getCodigo().toString());
            fieldCor.setValue(itemDevolucao.get().getCor().toString());
            fieldTamanho.setValue(itemDevolucao.get().getTam());
            fieldLocal.setValue(itemDevolucao.get().getLocal());
        }

        return true;
    }
}
