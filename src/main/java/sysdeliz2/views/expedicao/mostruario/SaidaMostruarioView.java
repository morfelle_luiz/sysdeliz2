package sysdeliz2.views.expedicao.mostruario;

import javafx.application.Platform;
import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.ListCell;
import javafx.scene.control.Separator;
import javafx.scene.control.TableColumn;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Callback;
import sysdeliz2.controllers.views.expedicao.SaidaMostruarioController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.sysdeliz.*;
import sysdeliz2.models.sysdeliz.comercial.SdMostrRep;
import sysdeliz2.models.sysdeliz.comercial.SdMostrRepItens;
import sysdeliz2.models.sysdeliz.comercial.SdTransfProdMostruario;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Entidade;
import sysdeliz2.models.ti.Pedido;
import sysdeliz2.models.ti.Represen;
import sysdeliz2.models.view.VSdDadosCliente;
import sysdeliz2.models.view.VSdDadosProduto;
import sysdeliz2.models.view.VSdDadosProdutoBarra;
import sysdeliz2.models.view.expedicao.VSdProdsTransfMostRemessa;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.exceptions.FormValidationException;
import sysdeliz2.utils.exceptions.LambdaException;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.InputBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class SaidaMostruarioView extends SaidaMostruarioController {

    // <editor-fold defaultstate="collapsed" desc="bean">
    private final LongProperty qtdeTotalMostruario = new SimpleLongProperty(0);
    private final LongProperty qtdeLidoMostruario = new SimpleLongProperty(0);
    private final LongProperty qtdeSaldoMostruario = new SimpleLongProperty(0);
    private final BooleanProperty emColeta = new SimpleBooleanProperty(false);
    private final BooleanProperty hasCaixaAberta = new SimpleBooleanProperty(false);
    private final BooleanProperty isExcluirBarra = new SimpleBooleanProperty(false);
    private final ListProperty<SdMostrRepItens> itensMostruario = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<SdItensCaixaRemessa> itensCaixa = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final AtomicReference<String> marcaMostruario = new AtomicReference<>();
    private final AtomicReference<SdRemessaCliente> remessaMostruario = new AtomicReference<>(null);
    private final AtomicReference<Pedido> pedidoMostruario = new AtomicReference<>(null);
    private final AtomicReference<SdCaixaRemessa> caixaAbertaRemessa = new AtomicReference<>(null);
    private final AtomicReference<SdCaixaRemessa> ultimaCaixaAberta = new AtomicReference<>(null);
    private final AtomicReference<Represen> representanteMostruario = new AtomicReference<>(null);
    private final AtomicReference<Entidade> entidadeRepresentanteMostruario = new AtomicReference<>(null);
    private final AtomicReference<VSdDadosCliente> dadosClienteRepresentanteMostruario = new AtomicReference<>(null);
    private final AtomicReference<Colecao> colecaoMostruario = new AtomicReference<>(null);
    private final AtomicReference<List<SdCaixaRemessa>> caixasRemessaMostruario = new AtomicReference<>(new ArrayList<>());
    private final AtomicReference<List<SdMostrRep>> mostruarios = new AtomicReference<>(new ArrayList<>());
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="components">
    private final FormFieldText fieldMostruario = FormFieldText.create(field -> {
        field.title("Mostruário");
        field.width(70.0);
        field.addStyle("lg");
        field.alignment(Pos.CENTER);
        field.editable.bind(emColeta.not());
        field.keyReleased(kvt -> {
            if (kvt.getCode().equals(KeyCode.ENTER))
                abrirMostruario(field.value.get());
        });
    });
    private final FormFieldText fieldMarcaMostruario = FormFieldText.create(field -> {
        field.title("Marca");
        field.addStyle("lg");
        field.width(200.0);
        field.editable.set(false);
    });
    private final FormFieldText fieldColecaoMostruario = FormFieldText.create(field -> {
        field.title("Coleção");
        field.width(80.0);
        field.addStyle("lg");
        field.alignment(Pos.CENTER);
        field.editable.set(false);
    });
    private final FormFieldText fieldRepresentante = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Repres.");
        field.editable.set(false);
    });
    private final FormFieldText fieldJeansMostruario = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Jeans");
        field.width(120.0);
        field.addStyle("lg");
        field.editable.set(false);
    });
    private final FormFieldText fieldModaMostruario = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Moda");
        field.width(120.0);
        field.addStyle("lg");
        field.editable.set(false);
    });
    private final FormFieldText fieldTotalMostruario = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Total");
        field.expanded();
        field.editable.set(false);
        field.addStyle("lg").addStyle("primary");
    });
    private final FormFieldText fieldLidoMostruario = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Lido");
        field.expanded();
        field.editable.set(false);
        field.addStyle("lg").addStyle("success");
    });
    private final FormFieldText fieldSaldoMostruario = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Saldo");
        field.expanded();
        field.editable.set(false);
        field.addStyle("lg").addStyle("warning");
    });

    private final FormFieldText fieldColaborador = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Coletor");
        field.editable.set(false);
    });

    private final FormFieldText fieldRemessaMostruario = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Remessa");
        field.expanded();
        field.addStyle("lg");
        field.alignment(Pos.CENTER);
        field.editable.set(false);
    });
    private final FormFieldText fieldPedidoMostruario = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Pedido");
        field.expanded();
        field.addStyle("lg");
        field.alignment(Pos.CENTER);
        field.editable.set(false);
    });

    private final FormFieldText fieldNumeroCaixa = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label(ImageUtils.getIcon(ImageUtils.Icon.QRCODE, ImageUtils.IconSize._32));
        field.width(200.0);
        field.toUpper();
        field.addStyle("lg").addStyle("barcodeinput");
        field.alignment(Pos.CENTER);
        field.editable.bind(emColeta.and(hasCaixaAberta.not()));
        field.keyReleased(event -> criarCaixa(event));
    });
    private final FormBox boxCaixas = FormBox.create(boxCaixas -> {
        boxCaixas.flutuante();
        boxCaixas.expanded();
    });
    private final FormButton btnFecharCaixa = FormButton.create(btnFecharCaixa -> {
        //btnFecharCaixa.title("Fechar");
        btnFecharCaixa.icon(ImageUtils.getIcon(ImageUtils.Icon.FECHAR_CAIXA, ImageUtils.IconSize._32));
        btnFecharCaixa.addStyle("primary");
        btnFecharCaixa.disable.bind(hasCaixaAberta.not());
        btnFecharCaixa.setAction(event -> fecharCaixa());
    });
    private final FormButton btnTrocarCaixa = FormButton.create(btnFecharCaixa -> {
        //btnFecharCaixa.title("Trocar");
        btnFecharCaixa.icon(ImageUtils.getIcon(ImageUtils.Icon.TROCAR, ImageUtils.IconSize._32));
        btnFecharCaixa.addStyle("warning");
        btnFecharCaixa.disable.bind(hasCaixaAberta.not());
        btnFecharCaixa.setAction(event -> trocarCaixa());
    });

    private final FormFieldText fieldBarcodeProduto = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label(ImageUtils.getIcon(ImageUtils.Icon.LEITURA_BARRA, ImageUtils.IconSize._32));
        field.addStyle("xl").addStyle("barcodeinput");
        field.alignment(Pos.CENTER);
        field.expanded();
        field.editable.bind(hasCaixaAberta);
        field.keyReleased(event -> leituraBarra(event));
    });
    private final FormListView<SdMostrRepItens> listDadosProduto = FormListView.create(list -> {
        list.title("Produtos");
        list.expanded();
        list.width(250.0);
        list.listProperties().setCellFactory(param -> {
            ListCell<SdMostrRepItens> cell = new ListCell<SdMostrRepItens>() {
                @Override
                protected void updateItem(SdMostrRepItens item, boolean empty) {
                    super.updateItem(item, empty);
                    getStyleClass().removeAll("success", "danger", "warning");
                    setText(null);
                    if (item != null && !empty) {
                        if (item.getStatus().equals("E"))
                            getStyleClass().add("success");
                        else if (item.getStatus().equals("X"))
                            getStyleClass().add("danger");
                        else if (item.getStatus().equals("T"))
                            getStyleClass().add("warning");
                        setText(item.getCodigo().toString());
                    }
                }
            };
            cell.setOnMouseClicked(evt -> {
                if (evt.getClickCount() == 2) {
                    if (((SdMostrRepItens)list.selectedItem()).getStatus().equals("T")) {
                        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                            message.message("Deseja realmente incluir item de tranferência manualmente?");
                            message.showAndWait();
                        }).value.get())) {
                            SdMostrRepItens itemMost = ((SdMostrRepItens)list.selectedItem());
                            List<VSdProdsTransfMostRemessa> produtosTransferidos = (List<VSdProdsTransfMostRemessa>) new FluentDao().selectFrom(VSdProdsTransfMostRemessa.class)
                                    .where(eb -> eb
                                            .equal("status", "T")
                                            .equal("id.produto", itemMost.getCodigo().getCodigo())
                                            .equal("id.marca", itemMost.getCodigo().getCodMarca())
                                            .equal("id.colecao", colecaoMostruario.get().getCodigo())
                                            .equal("id.representante", representanteMostruario.get().getCodRep()))
                                    .resultList();
                            produtosTransferidos.forEach(VSdProdsTransfMostRemessa::refresh);
                            produtosTransferidos.stream().filter(itemM -> itemM.getStatus().equals("T")).forEach(produtoTransferido -> {
                                try {
                                    incluirBarra(produtoTransferido.getBarra());
                                    new NativeDAO().runNativeQueryUpdate("update sd_transf_prod_mostruario_001 set status = 'E' where colecao = '%s' and representante = '%s' and produto = '%s' and mostruario = '%s'",
                                            produtoTransferido.getId().getColecao(), produtoTransferido.getId().getRepresentante(), produtoTransferido.getId().getProduto(), produtoTransferido.getId().getMostruario());
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    ExceptionBox.build(message -> {
                                        message.exception(e);
                                        message.showAndWait();
                                    });
                                }
                            });
                            list.refresh();
                            fieldBarcodeProduto.requestFocus();
                        }
                    }
                }
            });
            return cell;
        });
        list.items.bind(itensMostruario);
    });
    private final FormButton btnTipoBarra = FormButton.create(btnTipoBarra -> {
        btnTipoBarra.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._48));
        btnTipoBarra.addStyle("lg").addStyle("success");
        btnTipoBarra.disable.bind(hasCaixaAberta.not());
        btnTipoBarra.setAction(evt -> trocarTipoLeitura());
    });
    private final FormFieldText fieldCodigo = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Ref.");
        field.addStyle("xl");
        field.editable(false);
        field.alignment(Pos.CENTER);
        field.width(200.0);
    });
    private final FormFieldText fieldCor = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Cor");
        field.alignment(Pos.CENTER);
        field.addStyle("lg");
        field.editable(false);
        field.width(150.0);
    });
    private final FormFieldText fieldTamanho = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Tam.");
        field.alignment(Pos.CENTER);
        field.addStyle("lg");
        field.editable(false);
        field.width(100.0);
    });
    private final FormFieldText fieldDescricaoProduto = FormFieldText.create(field -> {
        field.withoutTitle();
        field.editable(false);
    });
    private final FormTableView<SdItensCaixaRemessa> tblItensLidos = FormTableView.create(SdItensCaixaRemessa.class, table -> {
        table.title("Produtos Lidos");
        table.expanded();
        table.items.bind(itensCaixa);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Caixa");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItensCaixaRemessa, SdItensCaixaRemessa>, ObservableValue<SdItensCaixaRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCaixa().getNumero()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Caixa*/,
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(200.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItensCaixaRemessa, SdItensCaixaRemessa>, ObservableValue<SdItensCaixaRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().toString()));
                }).build() /*Código*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cor");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItensCaixaRemessa, SdItensCaixaRemessa>, ObservableValue<SdItensCaixaRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCor()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Cor*/,
                FormTableColumn.create(cln -> {
                    cln.title("Tam");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItensCaixaRemessa, SdItensCaixaRemessa>, ObservableValue<SdItensCaixaRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTam()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Tam*/,
                FormTableColumn.create(cln -> {
                    cln.title("Barra");
                    cln.width(150.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItensCaixaRemessa, SdItensCaixaRemessa>, ObservableValue<SdItensCaixaRemessa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getBarra()));
                }).build() /*Barra*/
        );
    });
    // </editor-fold>

    public SaidaMostruarioView() {
        super("Saída de Mostruário", ImageUtils.getImage(ImageUtils.Icon.SAIDA_PRODUTO), Globals.isPortatil());
        JPAUtils.getEntityManager();

        SdColaborador coletor = getColetor(Globals.getNomeUsuario());
        if (coletor == null) {
            MessageBox.create(message -> {
                message.message("Nenhum colaborador vinculado a esse coletor.");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showFullScreenMobile();
            });
        } else {
            Globals.setColaboradorSistema(coletor);
            init();
        }
    }

    private void init() {
        // construção da tela
        super.box.getChildren().add(FormBox.create(container -> {
            container.horizontal();
            container.expanded();
            container.add(FormBox.create(left -> {
                left.vertical();
                left.add(FormBox.create(boxMostruario -> {
                    boxMostruario.vertical();
                    boxMostruario.title("Dados do Mostruário");
                    boxMostruario.add(FormBox.create(fields -> {
                        fields.horizontal();
                        fields.add(fieldMostruario.build(), fieldColecaoMostruario.build(), fieldMarcaMostruario.build());
                    }));
                    boxMostruario.add(fieldRepresentante.build());
                    boxMostruario.add(FormBox.create(fields -> {
                        fields.horizontal();
                        fields.add(fieldJeansMostruario.build(), fieldModaMostruario.build());
                    }));
                    boxMostruario.add(FormBox.create(fields -> {
                        fields.horizontal();
                        fields.add(fieldTotalMostruario.build(), fieldLidoMostruario.build(), fieldSaldoMostruario.build());
                    }));
                    boxMostruario.add(new Separator(Orientation.HORIZONTAL));
                    boxMostruario.add(FormBox.create(fields -> {
                        fields.horizontal();
                        fields.add(fieldPedidoMostruario.build(), fieldRemessaMostruario.build());
                    }));
                    boxMostruario.add(FormBox.create(fields -> {
                        fields.horizontal();
                        fields.add(FormButton.create(btn -> {
                            btn.title("CARREGAR OUTRO MOSTRUÁRIO");
                            btn.addStyle("primary").addStyle("lg");
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ABRIR, ImageUtils.IconSize._32));
                            btn.setAction(evt -> {
                                if (isExcluirBarra.get())
                                    trocarTipoLeitura();
                                clearFieldsProduto();
                                clearInit();
                                fieldMostruario.clear();
                                fieldMostruario.requestFocus();
                            });
                        }));
                    }));
                }));
                left.add(FormBox.create(boxCaixa -> {
                    boxCaixa.vertical();
                    boxCaixa.title("Dados da Caixa");
                    boxCaixa.expanded();
                    boxCaixa.add(FormBox.create(boxFields -> {
                        boxFields.horizontal();
                        boxFields.add(fieldNumeroCaixa.build());
                        boxFields.add(FormBadges.create(badges -> {
                            badges.badgedNode(btnFecharCaixa);
                            badges.position(Pos.TOP_RIGHT, -5.0);
                            badges.text(lb -> {
                                lb.value.set("F4");
                                lb.addStyle("default");
                                lb.sizeText(10);
                                lb.boldText();
                                lb.borderRadius(50);
                            });
                        }));
                        boxFields.add(FormBadges.create(badges -> {
                            badges.badgedNode(btnTrocarCaixa);
                            badges.position(Pos.TOP_RIGHT, -5.0);
                            badges.text(lb -> {
                                lb.value.set("F5");
                                lb.addStyle("default");
                                lb.sizeText(10);
                                lb.boldText();
                                lb.borderRadius(50);
                            });
                        }));
                    }));
                    boxCaixa.add(boxCaixas);
                }));
            }));
            container.add(FormBox.create(right -> {
                right.horizontal();
                right.expanded();
                right.add(FormBox.create(boxProdutosMostruario -> {
                    boxProdutosMostruario.vertical();
                    boxProdutosMostruario.add(fieldColaborador.build());
                    boxProdutosMostruario.add(listDadosProduto.build());
                }));
                right.add(FormBox.create(boxLeituraProduto -> {
                    boxLeituraProduto.vertical();
                    boxLeituraProduto.expanded();
                    boxLeituraProduto.add(FormBox.create(boxDadosProduto -> {
                        boxDadosProduto.vertical();
                        boxDadosProduto.title("Leitura do Produto");
                        boxDadosProduto.add(FormBox.create(boxBarra -> {
                            boxBarra.horizontal();
                            boxBarra.alignment(Pos.BOTTOM_LEFT);
                            boxBarra.add(FormBox.create(boxBadges -> {
                                boxBadges.horizontal();
                                boxBadges.add(FormBadges.create(badges -> {
                                    badges.badgedNode(btnTipoBarra);
                                    badges.position(Pos.TOP_RIGHT, -5.0);
                                    badges.text(lb -> {
                                        lb.value.set("F7");
                                        lb.addStyle("default");
                                        lb.sizeText(10);
                                        lb.boldText();
                                        lb.borderRadius(50);
                                    });
                                }));
                            }));
                            boxBarra.add(fieldBarcodeProduto.build());
                        }));
                        boxDadosProduto.add(FormBox.create(boxProdutoBarra -> {
                            boxProdutoBarra.horizontal();
                            boxProdutoBarra.alignment(Pos.TOP_LEFT);
                            boxProdutoBarra.add(fieldCodigo.build());
                            boxProdutoBarra.add(FormBox.create(box -> {
                                box.vertical();
                                box.expanded();
                                box.add(FormBox.create(fields -> {
                                    fields.horizontal();
                                    fields.add(fieldCor.build());
                                    fields.add(fieldTamanho.build());
                                }));
                                box.add(fieldDescricaoProduto.build());
                            }));
                        }));
                    }));
                    boxLeituraProduto.add(tblItensLidos.build());
                }));
            }));
        }));
        // inicialização de componentes fixos
        qtdeSaldoMostruario.bind(qtdeTotalMostruario.subtract(qtdeLidoMostruario));
        fieldTotalMostruario.value.bind(qtdeTotalMostruario.asString());
        fieldLidoMostruario.value.bind(qtdeLidoMostruario.asString());
        fieldSaldoMostruario.value.bind(qtdeSaldoMostruario.asString());
        fieldColaborador.value.set(Globals.getColaboradorSistema().toString());

        // focus no campo
        Platform.runLater(() -> {
            fieldMostruario.requestFocus();
        });
    }

    private void abrirMostruario(String numeroMostruario) {
        // leitura da peça para descobrir a marca do mostruario
        JPAUtils.getEntityManager().clear();
        InputBox<String> inputBarraProduto = null;
        do {
            inputBarraProduto = InputBox.build(String.class, boxInput -> {
                boxInput.message("Leitura da barra do produto:");
                boxInput.setDefaultButtonConfirm();
                boxInput.showAndWait();
            });
        } while (inputBarraProduto == null || inputBarraProduto.value.get() == null);
        String barraProduto = inputBarraProduto.value.get();
        VSdDadosProdutoBarra produtoLido = getProdutoBarra(barraProduto);
        if (produtoLido == null) {
            MessageBox.create(message -> {
                message.message("A barra lida não foi encontrada em um produto.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showFullScreenMobile();
            });
        } else {
            VSdDadosProduto produto = getProduto(produtoLido);

            mostruarios.get().clear();
            mostruarios.set(getMostruariosMarca(numeroMostruario, produto));
            if (mostruarios.get().size() == 0) {
                MessageBox.create(message -> {
                    message.message("Não existe um mostruário pendente com o número digitado, verifique o número de mostruário.");
                    message.type(MessageBox.TypeMessageBox.ALERT);
                    message.showFullScreenMobile();
                });
                return;
            }                
            carregarDadosMostruario(produto, mostruarios.get());
        }
    }

    private void clearInit() {
        fieldColecaoMostruario.clear();
        fieldMarcaMostruario.clear();
        fieldRepresentante.clear();
        fieldRemessaMostruario.clear();
        fieldPedidoMostruario.clear();
        fieldJeansMostruario.clear();
        fieldModaMostruario.clear();
        fieldNumeroCaixa.clear();
        boxCaixas.clear();

        qtdeTotalMostruario.set(0);
        qtdeLidoMostruario.set(0);
        itensMostruario.clear();
        itensCaixa.clear();
        emColeta.set(false);
        hasCaixaAberta.set(false);
        isExcluirBarra.set(false);
        marcaMostruario.set(null);
        remessaMostruario.set(null);
        pedidoMostruario.set(null);
        caixaAbertaRemessa.set(null);
        ultimaCaixaAberta.set(null);
        representanteMostruario.set(null);
        entidadeRepresentanteMostruario.set(null);
        dadosClienteRepresentanteMostruario.set(null);
        colecaoMostruario.set(null);
        caixasRemessaMostruario.get().clear();
    }

    private void clearFieldsProduto() {
        fieldCodigo.clear();
        fieldCor.clear();
        fieldTamanho.clear();
        fieldDescricaoProduto.clear();
        fieldBarcodeProduto.clear();
    }

    private void carregarDadosMostruario(VSdDadosProduto produto, List<SdMostrRep> mostruarios) {
        // limpando a tela do mostruario
        clearInit();

        // carregando os dados do mostruario na tela
        mostruarios.stream().findAny().ifPresent(mostruario -> {
            fieldColecaoMostruario.setValue(mostruario.getColvenda());
            fieldMarcaMostruario.setValue(produto.getMarca());
            fieldRepresentante.setValue(mostruario.getCodrep().toString());
            fieldJeansMostruario.setValue(String.valueOf(mostruarios.stream()
                    .filter(most -> most.getTipo().equals("J"))
                    .mapToLong(most -> most.getItens().stream()
                            .filter(ite -> !ite.getStatus().equals("X"))
                            .mapToInt(SdMostrRepItens::getQtde)
                            .sum())
                    .sum()));
            fieldModaMostruario.setValue(String.valueOf(mostruarios.stream()
                    .filter(most -> most.getTipo().equals("M"))
                    .mapToLong(most -> most.getItens().stream()
                            .filter(ite -> !ite.getStatus().equals("X"))
                            .mapToInt(SdMostrRepItens::getQtde)
                            .sum())
                    .sum()));

            qtdeTotalMostruario.set(mostruarios.stream()
                    .mapToLong(most -> most.getItens().stream()
                            .filter(ite -> !ite.getStatus().equals("X"))
                            .mapToInt(SdMostrRepItens::getQtde)
                            .sum())
                    .sum());
            qtdeLidoMostruario.set(mostruarios.stream()
                    .mapToLong(most -> most.getItens().stream()
                            .filter(ite -> ite.getStatus().equals("E"))
                            .mapToInt(SdMostrRepItens::getQtdelido)
                            .sum())
                    .sum());

            marcaMostruario.set(produto.getCodMarca());
            representanteMostruario.set(mostruario.getCodrep());
            colecaoMostruario.set(new FluentDao().selectFrom(Colecao.class).where(eb -> eb.equal("codigo", mostruario.getColvenda())).singleResult());
            if (mostruario.getRemessa() != null) {
                remessaMostruario.set(getRemessaMostruario(mostruario));
                fieldRemessaMostruario.setValue(remessaMostruario.get().getRemessa().toString());
                pedidoMostruario.set(remessaMostruario.get().getPedidos().stream().findAny().get().getId().getNumero());
                fieldPedidoMostruario.setValue(pedidoMostruario.get().getNumero());
                caixasRemessaMostruario.set(getCaixasRemessaMostruario(mostruario));
            }
        });

        // carregando produtos do mostruario
        mostruarios.forEach(most -> itensMostruario.addAll(most.getItens()));
        itensMostruario.sort((o1, o2) -> o1.getCodigo().getCodigo().compareTo(o2.getCodigo().getCodigo()));

        // carregando dados de entidade do representante
        entidadeRepresentanteMostruario.set(new FluentDao().selectFrom(Entidade.class)
                .where(eb -> eb.equal("cnpj", representanteMostruario.get().getCnpj()))
                .singleResult());
        dadosClienteRepresentanteMostruario.set(new FluentDao().selectFrom(VSdDadosCliente.class)
                .where(eb -> eb.equal("cnpj", representanteMostruario.get().getCnpj()))
                .singleResult());
        if (entidadeRepresentanteMostruario.get() == null || dadosClienteRepresentanteMostruario.get() == null) {
            MessageBox.create(message -> {
                message.message("O representante deste mostruário não tem o cadastro de cliente no ERP. " +
                        "Favor, verificar com o comercial para o cadastro do cliente.");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showFullScreenMobile();
            });
            fieldMostruario.clear();
            fieldMostruario.requestFocus();
            return;
        }

        // identificando que o mostruário está em coleta
        emColeta.set(true);

        // verificando se nas caixas cadastradas na leitura do mostruario existe caixa aberta
        caixasRemessaMostruario.get().stream().filter(caixa -> !caixa.getFechada()).findAny().ifPresent(caixa -> {
            caixaAbertaRemessa.set(caixa);
        });
        // exibindo dados das caixas da remessa do mostruário
        caixasRemessaMostruario.get().forEach(caixa -> {
            exibirDadosCaixaRemessa(caixa);
            itensCaixa.addAll(caixa.getItens());
        });
        // caso remessa do mostruario não estiver aberta, colocar o focus no campo caixa para cadastrar uma nova
        if (caixaAbertaRemessa.get() == null)
            fieldNumeroCaixa.requestFocus();
        else {
            exibirDadosCaixaAberta();
            hasCaixaAberta.set(true);
        }

        if (itensMostruario.stream().noneMatch(item -> item.getStatus().equals("P") || item.getStatus().equals("S"))) {
            finalizarColeta();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="pedido/remessa mostruario">
    private void criarPedidoRemessaMostroruario() {
        Integer numeroMostruario = mostruarios.get().stream().mapToInt(SdMostrRep::getMostruario).distinct().min().getAsInt();
        pedidoMostruario.set(criarPedido(colecaoMostruario.get(),
                entidadeRepresentanteMostruario.get(),
                entidadeRepresentanteMostruario.get().getTransporte(),
                marcaMostruario.get(),
                numeroMostruario));
        remessaMostruario.set(criarRemessa(pedidoMostruario.get(),
                dadosClienteRepresentanteMostruario.get(),
                marcaMostruario.get()));

    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="caixa remessa">
    private void criarCaixa(KeyEvent event) {
        if (event.getCode().equals(KeyCode.ENTER) && fieldNumeroCaixa.value.get() != null) {
            if (fieldNumeroCaixa.value.get().startsWith("CX") && fieldNumeroCaixa.value.get().length() == 8) {
                fieldNumeroCaixa.value.set(fieldNumeroCaixa.value.get().replace("CX", ""));

                // verificação de caixa existente
                SdCaixaRemessa validacaoCaixa = varificaCaixaExistente(fieldNumeroCaixa.value.get());
                if (validacaoCaixa != null) {
                    validacaoCaixa.refresh();
                    if ((remessaMostruario.get() == null && validacaoCaixa.getFechada())
                            || validacaoCaixa.getRemessa().getRemessa().compareTo(remessaMostruario.get().getRemessa()) != 0) {
                        MessageBox.create(message -> {
                            message.message("Esse número de caixa já consta em outra remessa. " +
                                    "Verifique o código de barras lido e faça a substituição. " +
                                    "Remessa alocada: " + validacaoCaixa.getRemessa().getRemessa() +
                                    " Coletor: " + (validacaoCaixa.getRemessa().getColetor() == null
                                    ? "COLETA MANUAL" : validacaoCaixa.getRemessa().getColetor().getNome()));
                            message.type(MessageBox.TypeMessageBox.WARNING);
                            message.showFullScreenMobile();
                        });
//                        SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
//                                "Tentativa de inclusão de uma caixa já existente em outra remessa. Barra lida: " + caixaField.value.get());
                        fieldNumeroCaixa.clear();
                        fieldNumeroCaixa.requestFocus();
                    } else if (validacaoCaixa.getFechada()) {
                        MessageBox.create(message -> {
                            message.message("Esse número de caixa já consta nesta remessa, porém a caixa se encontra como fechada. " +
                                    "Você pode abrir essa caixa se desejar.");
                            message.type(MessageBox.TypeMessageBox.IMPORTANT);
                            message.showFullScreenMobile();
                        });
//                        SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
//                                "Tentativa de inclusão de uma caixa já existente na remessa e marcada como fechada. Barra lida: " + caixaField.value.get());
                        fieldNumeroCaixa.clear();
                        fieldNumeroCaixa.requestFocus();
                    } else {
                        caixaAbertaRemessa.set(validacaoCaixa);
                        limparDadosCaixaAberta();
                        exibirDadosCaixaAberta();
                        hasCaixaAberta.set(true);
                        fieldBarcodeProduto.requestFocus();
                    }
                } else {
                    ultimaCaixaAberta.set(caixaAbertaRemessa.get());
                    if (remessaMostruario.get() == null) {
                        criarPedidoRemessaMostroruario();
                        mostruarios.get().forEach(mostruario -> {
                            mostruario.setNumero(pedidoMostruario.get().getNumero());
                            mostruario.setRemessa(String.valueOf(remessaMostruario.get().getRemessa()));
                            new FluentDao().merge(mostruario);
                        });
                        fieldRemessaMostruario.setValue(remessaMostruario.get().getRemessa().toString());
                        fieldPedidoMostruario.setValue(pedidoMostruario.get().getNumero());
                    }
                    incluirCaixa(new SdCaixaRemessa(Integer.parseInt(fieldNumeroCaixa.value.get()), remessaMostruario.get()));
                    limparDadosCaixaAberta();
                    exibirDadosCaixaAberta();
                    limparDadosCaixaRemessa();
                    caixasRemessaMostruario.get().forEach(this::exibirDadosCaixaRemessa);
                    hasCaixaAberta.set(true);

                    // incluindo peças de transferência caso exista
                    if (remessaMostruario.get() != null/* && remessaMostruario.get().getPedidos().stream().anyMatch(pedido -> pedido.getItens().size() == 0)*/) {
                        String marcaMostruario = mostruarios.get().stream().map(SdMostrRep::getMarca).distinct().findAny().get();
                        List<VSdProdsTransfMostRemessa> produtosTransferidos = (List<VSdProdsTransfMostRemessa>) new FluentDao().selectFrom(VSdProdsTransfMostRemessa.class)
                                .where(eb -> eb
                                        .equal("status", "T")
                                        .equal("id.marca", marcaMostruario)
                                        .equal("id.colecao", colecaoMostruario.get().getCodigo())
                                        .equal("id.representante", representanteMostruario.get().getCodRep()))
                                .resultList();

                        produtosTransferidos.forEach(VSdProdsTransfMostRemessa::refresh);
                        produtosTransferidos.stream().filter(itemM -> itemM.getStatus().equals("T")).forEach(produtoTransferido -> {
                            try {
                                incluirBarra(produtoTransferido.getBarra());
                                new NativeDAO().runNativeQueryUpdate("update sd_transf_prod_mostruario_001 set status = 'E' where colecao = '%s' and representante = '%s' and produto = '%s' and mostruario = '%s'",
                                        produtoTransferido.getId().getColecao(), produtoTransferido.getId().getRepresentante(), produtoTransferido.getId().getProduto(), produtoTransferido.getId().getMostruario());
                            } catch (SQLException e) {
                                e.printStackTrace();
                                ExceptionBox.build(message -> {
                                    message.exception(e);
                                    message.showAndWait();
                                });
                            }
                        });
                    }

                    fieldBarcodeProduto.requestFocus();

//                    SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
//                            "Incluída a caixa " + caixaAberta.getNumero() + " na remessa. Barra lida: " + caixaField.value.get());
                }
            } else {
                MessageBox.create(message -> {
                    message.message("O código de barras lido não é válido para a caixa, verifique a etiqueta.");
                    message.type(MessageBox.TypeMessageBox.ERROR);
                    message.showFullScreenMobile();
                });
//                SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
//                        "Tentativa de inclusão de uma caixa com código de barras inválido. Barra lida: " + caixaField.value.get());

                fieldNumeroCaixa.clear();
                fieldNumeroCaixa.requestFocus();
            }
        }
    }

    private void incluirCaixa(SdCaixaRemessa caixa) {
        caixaAbertaRemessa.set(new FluentDao().merge(caixa));
        caixasRemessaMostruario.get().add(caixaAbertaRemessa.get());
    }

    private void abrirCaixa(SdCaixaRemessa caixa) {
        new Fragment().show(fragment -> {
            fragment.title("LEITURA DO CÓDIGO DE BARRAS DA CAIXA");
            fragment.size(400.0, 100.0);

            final FormFieldText fieldBarcodeCaixa = FormFieldText.create(field -> {
                field.withoutTitle();
                field.expanded();
                field.toUpper();
                field.label(ImageUtils.getIcon(ImageUtils.Icon.LEITURA_BARRA, ImageUtils.IconSize._24));
                field.addStyle("lg");
                field.alignment(Pos.CENTER);
                field.keyReleased(event -> {
                    if (event.getCode().equals(KeyCode.ENTER))
                        if (field.value.get().startsWith("CX") && field.value.get().length() == 8) {
                            field.value.set(field.value.get().replace("CX", ""));
                        } else {
                            MessageBox.create(message -> {
                                message.message("O código de barras lido não é válido para a caixa, verifique a etiqueta.");
                                message.type(MessageBox.TypeMessageBox.ERROR);
                                message.showFullScreen();
                            });

//                            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.MOVIMENTAR, String.valueOf(remessaEmColeta.getRemessa()),
//                                    "Tentativa de confirmação de uma caixa com código de barra inválido. Barra lida: " + field.value.get());
                            field.clear();
                            field.requestFocus();
                        }
                });
            });
            fieldBarcodeCaixa.requestFocus();
            fragment.box.getChildren().add(fieldBarcodeCaixa.build());
            fragment.buttonsBox.getChildren().add(FormButton.create(btn -> {
                btn.title("EXCLUIR");
                btn.addStyle("lg").addStyle("warning");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EXCLUIR_CAIXA, ImageUtils.IconSize._32));
                btn.setAction(evt -> {
                    if (caixa.getNumero()
                            .compareTo(Integer.parseInt(fieldBarcodeCaixa.value.get().replace("CX", ""))) == 0) {
                        // verificação de caixa existente
                        if (caixa.getItens().size() == 0) {
                            new FluentDao().delete(caixa);

                            caixasRemessaMostruario.get().remove(caixa);
                            limparDadosCaixaRemessa();
                            caixasRemessaMostruario.get().forEach(this::exibirDadosCaixaRemessa);

                            if (caixaAbertaRemessa.get().getNumero().compareTo(caixa.getNumero()) == 0) {
                                caixaAbertaRemessa.set(null);
                                hasCaixaAberta.set(false);
                                fieldNumeroCaixa.clear();
                                fieldNumeroCaixa.requestFocus();
                            }

//                            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.EXCLUIR, String.valueOf(remessaEmColeta.getRemessa()),
//                                    "Excluindo caixa vazia da remessa. Barra lida: " + fieldBarcodeCaixa.value.get());
                        } else {
                            MessageBox.create(message -> {
                                message.message("Só é permitido excluir uma caixa vazia, você deve primeiro excluir os itens da caixa para sua exclusão.");
                                message.type(MessageBox.TypeMessageBox.ERROR);
                                message.showFullScreen();
                            });

//                            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.EXCLUIR, String.valueOf(remessaEmColeta.getRemessa()),
//                                    "Tentativa de exclusão de uma caixa com itens. Barra caixa: " + caixa.getNumero() + " Barra lida: " + fieldBarcodeCaixa.value.get());
                        }
                        fragment.close();
                    } else {
                        MessageBox.create(message -> {
                            message.message("O código lido não confere com a caixa que está sendo excluida, verifique o número da caixa e leia novamente.");
                            message.type(MessageBox.TypeMessageBox.ERROR);
                            message.showFullScreen();
                        });

//                        SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.EXCLUIR, String.valueOf(remessaEmColeta.getRemessa()),
//                                "Tentativa de exclusão de uma caixa com o número diferente da caixa. Barra caixa: " + caixa.getNumero() + " Barra lida: " + fieldBarcodeCaixa.value.get());
                        fieldBarcodeCaixa.clear();
                        fieldBarcodeCaixa.requestFocus();
                    }
                });
            }));
            fragment.buttonsBox.getChildren().add(FormButton.create(btn -> {
                btn.title("ABRIR");
                btn.addStyle("lg").addStyle("success");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CAIXA_ABERTA, ImageUtils.IconSize._32));
                btn.setAction(evt -> {
                    if (caixa.getNumero().compareTo(Integer.parseInt(fieldBarcodeCaixa.value.get())) == 0) {
                        // verificação de caixa existente
                        if (caixaAbertaRemessa.get() != null)
                            if (!fecharCaixa()) {
                                fragment.close();
                                return;
                            }

                        caixaAbertaRemessa.set(caixa);
                        caixaAbertaRemessa.get().setFechada(false);
                        hasCaixaAberta.set(true);
                        new FluentDao().merge(caixaAbertaRemessa.get());
                        limparDadosCaixaRemessa();
                        caixasRemessaMostruario.get().forEach(this::exibirDadosCaixaRemessa);
                        exibirDadosCaixaAberta();

//                        SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
//                                "Abrindo caixa para inclusão de peças. Barra lida: " + fieldBarcodeCaixa.value.get());
                        fragment.close();
                    } else {
                        MessageBox.create(message -> {
                            message.message("O código lido não confere com a caixa que está sendo aberta, verifique o número da caixa e leia novamente.");
                            message.type(MessageBox.TypeMessageBox.ERROR);
                            message.showFullScreen();
                        });

//                        SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
//                                "Tentativa de abertura de uma caixa com o número diferente da caixa. Barra caixa: " + caixa.getNumero() + " Barra lida: " + fieldBarcodeCaixa.value.get());
                        fieldBarcodeCaixa.clear();
                        fieldBarcodeCaixa.requestFocus();
                    }
                });
            }));
        });
        if (caixaAbertaRemessa.get() == null) {
            hasCaixaAberta.set(false);
            fieldNumeroCaixa.clear();
            fieldNumeroCaixa.requestFocus();
        }
    }

    private void trocarCaixa() {
        new Fragment().show(fragment -> {
            fragment.title("SUBSTITUIÇÃO DE CAIXAS");
            fragment.size(300.0, 100.0);

            final FormFieldText fieldBarcodeCaixa = FormFieldText.create(field -> {
                field.withoutTitle();
                field.expanded();
                field.toUpper();
                field.label(ImageUtils.getIcon(ImageUtils.Icon.LEITURA_BARRA, ImageUtils.IconSize._24));
                field.addStyle("lg");
                field.alignment(Pos.CENTER);
            });
            fieldBarcodeCaixa.requestFocus();
            fragment.box.getChildren().add(fieldBarcodeCaixa.build());
            fragment.buttonsBox.getChildren().add(FormButton.create(btn -> {
                btn.title("TROCAR");
                btn.addStyle("lg").addStyle("success");
                btn.defaultButton(true);
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.TROCAR, ImageUtils.IconSize._32));
                btn.setAction(evt -> {
                    if (fieldBarcodeCaixa.value.get().startsWith("CX") && fieldBarcodeCaixa.value.get().length() == 8) {
                        fieldBarcodeCaixa.value.set(fieldBarcodeCaixa.value.get().replace("CX", ""));
                        try {
                            // verificação de caixa existente
                            SdCaixaRemessa validacaoCaixa = varificaCaixaExistente(fieldBarcodeCaixa.value.get());
                            if (validacaoCaixa != null) {
                                validacaoCaixa.refresh();
                                if (validacaoCaixa.getRemessa().getRemessa().compareTo(remessaMostruario.get().getRemessa()) != 0) {
                                    MessageBox.create(message -> {
                                        message.message("Esse número de caixa já consta em outra remessa. Verifique o código de barras lido e faça a substituição. Remessa alocada: " + validacaoCaixa.getRemessa().getRemessa() +
                                                " Coletor: " + validacaoCaixa.getRemessa().getColetor().getNome());
                                        message.type(MessageBox.TypeMessageBox.WARNING);
                                        message.showFullScreen();
                                    });
//                                    SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
//                                            "Tentativa de troca de caixa por uma caixa já existente em outra remessa. Barra lida: " + fieldBarcodeCaixa.value.get());
                                    fieldBarcodeCaixa.clear();
                                    fieldBarcodeCaixa.requestFocus();
                                } else if (validacaoCaixa.getFechada()) {
                                    MessageBox.create(message -> {
                                        message.message("Esse número de caixa já consta nesta remessa, porém a caixa se encontra como fechada. Você pode abrir essa caixa se desejar.");
                                        message.type(MessageBox.TypeMessageBox.IMPORTANT);
                                        message.showFullScreen();
                                    });
//                                    SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
//                                            "Tentativa de troca de caixa por uma caixa já existente na remessa e marcada como fechada. Barra lida: " + fieldBarcodeCaixa.value.get());
                                    fieldBarcodeCaixa.clear();
                                    fieldBarcodeCaixa.requestFocus();
                                }
                            } else {
//                                SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
//                                        "Trocando a caixa " + caixaAberta.getNumero() + " para a barra " + fieldBarcodeCaixa.value.get());

                                caixaAbertaRemessa.set(substituirCaixa(fieldBarcodeCaixa.value.get(), caixaAbertaRemessa.get(),
                                        remessaMostruario.get(), caixasRemessaMostruario.get()));
                                limparDadosCaixaRemessa();
                                caixasRemessaMostruario.get().forEach(this::exibirDadosCaixaRemessa);
                                fragment.close();
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                            ExceptionBox.build(message -> {
                                message.exception(e);
                                message.showAndWait();
                            });
                        }
                    } else {
                        MessageBox.create(message -> {
                            message.message("O código de barras lido não é válido para a caixa, verifique a etiqueta.");
                            message.type(MessageBox.TypeMessageBox.ERROR);
                            message.showFullScreen();
                        });

//                        SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
//                                "Tentativa de troca de caixa com código de barras inválido. Barra lida: " + fieldBarcodeCaixa.value.get());
                        fieldBarcodeCaixa.clear();
                        fieldBarcodeCaixa.requestFocus();
                    }
                });
            }));
        });
        if (caixaAbertaRemessa.get() == null) {
            hasCaixaAberta.set(false);
            fieldNumeroCaixa.clear();
            fieldNumeroCaixa.requestFocus();
        } else {
            exibirDadosCaixaAberta();
            limparDadosCaixaRemessa();
            caixasRemessaMostruario.get().forEach(this::exibirDadosCaixaRemessa);
        }
    }

    private boolean fecharCaixa() {
        if (caixaAbertaRemessa.get().getItens().size() == 0) {
            MessageBox.create(message -> {
                message.message("Você não pode fechar a caixa pois ela não contém nenhum item dentro dela. Você pode utilizar a opção de trocar caixa caso não queira mais utilizar essa.");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showFullScreen();
            });

//            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
//                    "Tentativa de fechar a caixa " + caixaAberta.getNumero() + " sem itens na caixa.");
            return false;
        }

//        SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
//                "Fechando a caixa " + caixaAberta.getNumero() + " com " + caixaAberta.getItens().size() + " itens.");
        caixaAbertaRemessa.get().setFechada(true);
        new FluentDao().merge(caixaAbertaRemessa.get());
        hasCaixaAberta.set(false);
        limparDadosCaixaAberta();
        caixaAbertaRemessa.set(null);
        limparDadosCaixaRemessa();
        caixasRemessaMostruario.get().forEach(this::exibirDadosCaixaRemessa);
        fieldNumeroCaixa.clear();
        fieldNumeroCaixa.requestFocus();
        return true;
    }

    private void exibirDadosCaixaAberta() {
        fieldNumeroCaixa.value.set(String.valueOf(caixaAbertaRemessa.get().getNumero()));
        fieldBarcodeProduto.requestFocus();
    }

    private void limparDadosCaixaAberta() {
        fieldNumeroCaixa.clear();
    }

    private void exibirDadosCaixaRemessa(SdCaixaRemessa caixa) {
        if (caixa != null)
            boxCaixas.add(FormBadges.create(bg -> {
                bg.position(Pos.TOP_RIGHT, -3.0);
                bg.text(lb -> {
                    lb.boldText();
                    lb.sizeText(14);
                    lb.addStyle("info");
                    lb.borderRadius(50);
                    lb.value.bind(caixa.qtdeProperty().asString());
                });
                bg.badgedNode(FormOverlap.create(ovl -> {
                    ovl.add(ImageUtils.getIcon(caixa.getFechada()
                            ? ImageUtils.Icon.CAIXA : ImageUtils.Icon.CAIXA_ABERTA, ImageUtils.IconSize._64));
                    ovl.add(FormLabel.create(lb -> {
                        lb.value.set(String.valueOf(caixa.getNumero()));
                        lb.mouseClicked(event -> abrirCaixa(caixa));
                    }));
                }));
            }));
    }

    private void limparDadosCaixaRemessa() {
        boxCaixas.clear();
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="leitura produto">
    private void trocarTipoLeitura() {
//            SysLogger.addSysDelizLog("Coleta Produtos", TipoAcao.CADASTRAR, String.valueOf(remessaEmColeta.getRemessa()),
//                    "Trocando tipo de leitura de barra para " + (isExcluirBarra.not().get() ? "ENTRADA DE BARRAS" : "EXCLUSÃO DE BARRAS"));
        isExcluirBarra.set(isExcluirBarra.not().get());
        btnTipoBarra.removeStyle(isExcluirBarra.get() ? "success" : "danger");
        btnTipoBarra.addStyle(isExcluirBarra.get() ? "danger" : "success");
        btnTipoBarra.icon(ImageUtils.getIcon(isExcluirBarra.get() ? ImageUtils.Icon.CANCEL : ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._48));
        fieldBarcodeProduto.removeStyle(!isExcluirBarra.get() ? "barcodeoutput" : "barcodeinput");
        fieldBarcodeProduto.addStyle(isExcluirBarra.get() ? "barcodeoutput" : "barcodeinput");
        fieldBarcodeProduto.clear();
        fieldBarcodeProduto.requestFocus();
    }

    private void finalizarColeta() {
        AtomicReference<Boolean> encerrar = new AtomicReference<>(false);
        if (itensMostruario.stream().anyMatch(item -> item.getStatus().equals("P") || item.getStatus().equals("S"))) {
            MessageBox.create(message -> {
                message.message("Ainda constam produtos deste mostruário para serem lidos. Não é possível finalizar a leitura.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showFullScreen();
            });
        } else {
            encerrar.set(true);
        }

        if (encerrar.get()) {

            // verificação de caixa aberta
            if (caixaAbertaRemessa.get() != null)
                fecharCaixa();
            String caixasFinalizadas = caixasRemessaMostruario.get().stream()
                    .map(caixa -> String.valueOf(caixa.getNumero())).collect(Collectors.joining(", "));
            caixasRemessaMostruario.get().clear();

            remessaMostruario.get().refresh();
            remessaMostruario.get().getPedidos().forEach(pedido -> {
                pedido.refresh();
                pedido.getItens().forEach(SdItensPedidoRemessa::refresh);
                pedido.setQtde(pedido.getItens().stream().mapToInt(item -> item.getQtde()).sum());
                pedido.setValor(new BigDecimal(pedido.getItens().stream().mapToDouble(item -> item.getValor().doubleValue()).sum()));
                new FluentDao().merge(pedido);
            });
            remessaMostruario.get().setQtde(remessaMostruario.get().getPedidos().stream().mapToInt(item -> item.getQtde()).sum());
            remessaMostruario.get().setValor(new BigDecimal(remessaMostruario.get().getPedidos().stream().mapToDouble(item -> item.getValor().doubleValue()).sum()));
            remessaMostruario.get().setStatus(_STATUS_T_REMESSA_MOSTRUARIO.get());
            remessaMostruario.get().setDtFimColeta(LocalDateTime.now());
            new FluentDao().merge(remessaMostruario.get());
            mostruarios.get().forEach(mostr -> {
                mostr.setStatus("F");
                new FluentDao().merge(mostr);
            });

            MessageBox.create(message -> {
                message.message("Coleta da remessa " + remessaMostruario.get().getRemessa() + " finalizada! Encaminhe a(s) caixa(s): "
                        + caixasFinalizadas + " para área de fechamento.");
                message.type(MessageBox.TypeMessageBox.SUCCESS);
                message.showFullScreen();
            });

            clearFieldsProduto();
            clearInit();

            fieldMostruario.clear();
            fieldMostruario.requestFocus();
        }
    }

    private void carregarDadosProduto(VSdDadosProdutoBarra produtoBarra, String descricaoProduto) {
        // exibe os dados do produto em coleta
        fieldCodigo.value.set(produtoBarra.getCodigo());
        fieldCor.value.set(produtoBarra.getCor());
        fieldTamanho.value.set(produtoBarra.getTam());
        fieldDescricaoProduto.value.set(descricaoProduto);

        fieldCodigo.removeStyle("danger").removeStyle("primary");
        fieldCor.removeStyle("danger").removeStyle("primary");
        fieldTamanho.removeStyle("danger").removeStyle("primary");
        fieldDescricaoProduto.removeStyle("info");

        if (isExcluirBarra.not().get()) {
            fieldCodigo.addStyle("primary");
            fieldCor.addStyle("primary");
            fieldTamanho.addStyle("primary");
        } else {
            fieldCodigo.addStyle("danger");
            fieldCor.addStyle("danger");
            fieldTamanho.addStyle("danger");
        }
        fieldDescricaoProduto.addStyle("info");
        tblItensLidos.refresh();
        listDadosProduto.refresh();
    }

    private boolean incluirBarra(String barra) throws SQLException {
        // validação se a barra já foi lida
        SdItensCaixaRemessa itemEmCaixa = barraJaEmCaixa(barra);
        if (itemEmCaixa != null) {
            SdItensCaixaRemessa finalItemEmCaixa = itemEmCaixa;
            MessageBox.create(message -> {
                message.message("A barra " + barra + " já se encontra na caixa " + finalItemEmCaixa.getId().getCaixa().getNumero() +
                        " na remessa " + finalItemEmCaixa.getId().getCaixa().getRemessa().getRemessa());
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showFullScreen();
            });
            return false;
        }

        // get do produto pela barra
        VSdDadosProdutoBarra produtoBarra = getProdutoBarra(barra);
        if (produtoBarra == null) {
            MessageBox.create(message -> {
                message.message("A barra " + barra.substring(0, 6) + " não cadastrada para o produto em leitura.\n" +
                        "Verificar cadastro do produto com a engenharia de produto.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showFullScreen();
            });
            return false;
        }
        AtomicReference<SdMostrRepItens> produtoMostruario = new AtomicReference<>(null);
        itensMostruario.stream()
                .filter(prodMost -> prodMost.getCodigo().getCodigo().equals(produtoBarra.getCodigo()) && !prodMost.getStatus().equals("X"))
                .findAny()
                .ifPresent(prodMost -> produtoMostruario.set(prodMost));

        // validação se o produto está no mostruário do representante
        if (produtoMostruario.get() == null) {
            MessageBox.create(message -> {
                message.message("O produto " + produtoBarra.getCodigo() + " não pertence ao mostruário do representante.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showFullScreenMobile();
            });
            return false;
        }

        // validação se já foi incluído a barra deste produto
        if (produtoMostruario.get().getStatus().equals("E")) {
            MessageBox.create(message -> {
                message.message("O produto " + produtoBarra.getCodigo() + " já se encontra na caixa com outra barra.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showFullScreenMobile();
            });
            return false;
        }
        VSdDadosProduto produto = getProduto(produtoBarra);

        // criação do item no pedido e na remessa mais movimentação do Excia
        incluirItemPedidoRemessa(produtoBarra, produto, pedidoMostruario.get(), remessaMostruario.get(), caixaAbertaRemessa.get());
        // inclusão do item em caixa
        itemEmCaixa = incluirItemCaixa(caixaAbertaRemessa.get(), produtoBarra, produto, barra, pedidoMostruario.get().getNumero());
        incluirItemEmCaixa(itemEmCaixa, produto, caixaAbertaRemessa.get());
        itensCaixa.add(0, itemEmCaixa);
        qtdeLidoMostruario.set(qtdeLidoMostruario.get() + 1);
        remessaMostruario.get().refresh();

        // atualização do item do mostruário
        produtoMostruario.get().setQtdelido(produtoMostruario.get().getQtdelido() + 1);
        if (produtoMostruario.get().getQtde() == produtoMostruario.get().getQtdelido())
            produtoMostruario.get().setStatus("E");
        atualizarStatusItemMost(produtoMostruario.get());

        // carregando os dados do produto barra na tela
        carregarDadosProduto(produtoBarra, produto.getDescricao());

        if (itensMostruario.stream().noneMatch(item -> item.getStatus().equals("P") || item.getStatus().equals("S"))) {
            finalizarColeta();
        }

        return true;
    }

    private boolean removerBarra(String barra) throws SQLException, LambdaException {
        // validação se a barra já foi lida
        SdItensCaixaRemessa itemEmCaixa = barraJaEmCaixa(barra);
        if (itemEmCaixa == null) {
            MessageBox.create(message -> {
                message.message("A barra " + barra + " não se encontra em uma caixa, verifique se o produto pertence a essa caixa ou mostruário.");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showFullScreen();
            });
            return false;
        }

        // validação se a barra é da caixa da remessa do mostruário
        if (!itemEmCaixa.getId().getCaixa().getRemessa().getRemessa().equals(remessaMostruario.get().getRemessa())) {
            MessageBox.create(message -> {
                message.message("A barra que você leu não pertence a remessa do mostruário aberto!\n" +
                        "Remessa da barra: " + itemEmCaixa.getId().getCaixa().getRemessa().getRemessa() + "\n" +
                        "Caixa: " + itemEmCaixa.getId().getCaixa().getNumero());
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showFullScreen();
            });
            return false;
        }

        // get do produto pela barra
        VSdDadosProdutoBarra produtoBarra = getProdutoBarra(barra);
        AtomicReference<SdMostrRepItens> produtoMostruario = new AtomicReference<>(null);
        itensMostruario.stream()
                .filter(prodMost -> prodMost.getCodigo().getCodigo().equals(produtoBarra.getCodigo()))
                .findAny()
                .ifPresent(prodMost -> produtoMostruario.set(prodMost));

        // validação se o produto está no mostruário do representante
        if (produtoMostruario.get() == null) {
            MessageBox.create(message -> {
                message.message("O produto " + produtoBarra.getCodigo() + " não pertence ao mostruário do representante. " +
                        "Verifique se o produto está realmente nesta caixa.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showFullScreen();
            });
            return false;
        }

        // validação se já foi incluído a barra deste produto
        if (!produtoMostruario.get().getStatus().equals("E")) {
            MessageBox.create(message -> {
                message.message("O produto " + produtoBarra.getCodigo() + " desta barra ainda não foi incluído na caixa.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showFullScreen();
            });
            return false;
        }
        VSdDadosProduto produto = getProduto(produtoBarra);

        removerItemPedidoRemessa(produtoBarra, produto, pedidoMostruario.get(), remessaMostruario.get(), caixaAbertaRemessa.get());
        removerItemCaixa(itemEmCaixa);
        itensCaixa.remove(itemEmCaixa);
        qtdeLidoMostruario.set(qtdeLidoMostruario.get() - 1);
        remessaMostruario.get().refresh();

        // atualização do item do mostruário
        produtoMostruario.get().setQtdelido(produtoMostruario.get().getQtdelido() - 1);
        produtoMostruario.get().setStatus("P");
        atualizarStatusItemMost(produtoMostruario.get());

        // exibição dos dados em tela
        carregarDadosProduto(produtoBarra, produto.getDescricao());

        return true;
    }

    private void leituraBarra(KeyEvent event) {
        if (event.getCode().equals(KeyCode.F4)) {
            fieldBarcodeProduto.clear();
            fecharCaixa();
            return;
        }
        else if (event.getCode().equals(KeyCode.F5)) {
            fieldBarcodeProduto.clear();
            trocarCaixa();
            return;
        }
        else if (event.getCode().equals(KeyCode.F7)) {
            fieldBarcodeProduto.clear();
            trocarTipoLeitura();
            return;
        }
        else if (event.getCode().equals(KeyCode.F12)) {
            fieldBarcodeProduto.clear();
            finalizarColeta();
            return;
        }
        else if (event.getCode().equals(KeyCode.ENTER)) {
            try {
                if (fieldBarcodeProduto.value.get() != null && fieldBarcodeProduto.value.get().length() == 16) {
                    String barra = fieldBarcodeProduto.value.get().toUpperCase();

                    if (isExcluirBarra.not().get()) {
                        incluirBarra(barra);
                    } else {
                        removerBarra(barra);
                    }
                } else {
                    MessageBox.create(message -> {
                        message.message("O código de barras é inválido!");
                        message.type(MessageBox.TypeMessageBox.ALERT);
                        message.showFullScreenMobile();
                    });
                }

                fieldBarcodeProduto.clear();
                fieldBarcodeProduto.requestFocus();
            } catch (SQLException | LambdaException e) {
                e.printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(e);
                    message.showAndWait();
                });
            }
        }
    }
    // </editor-fold>
}
