package sysdeliz2.views.financeiro;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.converter.BigDecimalStringConverter;
import org.controlsfx.control.textfield.TextFields;
import sysdeliz2.controllers.views.financeiro.ContratosFIDCController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.models.sysdeliz.financeiro.SdContratoFidic;
import sysdeliz2.models.sysdeliz.financeiro.SdItemContratoFidic;
import sysdeliz2.models.ti.*;
import sysdeliz2.models.view.VSdDadosEntidade;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.ParametroConsulta;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.InputBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.hibernate.DefaultFilter;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.annotations.ColunaFilter;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ContratosFIDCView extends ContratosFIDCController {

    private final BigDecimalStringConverter bigDecimalStringConverter = new BigDecimalStringConverter();

    private final ObjectProperty<SdContratoFidic> contratoSelcionado = new SimpleObjectProperty<>(null);

    // <editor-fold defaultstate="collapsed" desc="Lists">
    private final ListProperty<SdContratoFidic> contratosBean = new SimpleListProperty<>();
    private final ListProperty<SdItemContratoFidic> titulosBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<SdItemContratoFidic> titulosBeanAux = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<Receber> titulosCriacaoBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<Receber> titulosAuxiliaresBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<CadBan> bancosBean = new SimpleListProperty<>();
    private final ListProperty<ParametroConsulta> parametrosBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<ContratosFIDCController.ClienteConsulta> clientesConsultaBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Fields">
    private final FormFieldText fieldValorTotal = FormFieldText.create(field -> {
        field.title("Valor Total");
        field.editable.set(false);
        field.width(150);
    });
    private final FormFieldText fieldValorPago = FormFieldText.create(field -> {
        field.title("Valor Pago");
        field.editable.set(false);
        field.width(150);
    });
    private final FormFieldText fieldValorRestante = FormFieldText.create(field -> {
        field.title("Valor Restante");
        field.editable.set(false);
        field.width(150);
    });
    private final FormFieldText fieldValorRecomprado = FormFieldText.create(field -> {
        field.title("Valor Recomprado");
        field.editable.set(false);
        field.width(150);
    });

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Tables">
    private final FormTableView<SdContratoFidic> tblContratos = FormTableView.create(SdContratoFidic.class, table -> {
        table.title("Contratos");
        table.expanded();
        table.indices(
                table.indice("Sem Bordero", "danger"),
                table.indice("Não Enviado", "warning"),
                table.indice("Enviado", "success")
        );

        table.factoryRow(param -> new TableRow<SdContratoFidic>() {
            @Override
            protected void updateItem(SdContratoFidic item, boolean empty) {
                super.updateItem(item, empty);
                clear();
                if (item != null && !empty) {
                    if (item.getBordero() == null) {
                        getStyleClass().add("table-row-danger");
                    } else if (!item.isEnviado()) {
                        getStyleClass().add("table-row-warning");
                    }
                }
            }

            public void clear() {
                getStyleClass().removeAll("table-row-success", "table-row-danger", "table-row-warning");
            }
        });
        table.items.bind(contratosBean);
        table.selectionModelItem((observable, oldValue, newValue) -> {
            if (newValue != null) {
                contratoSelcionado.set(((SdContratoFidic) newValue));
            }
        });
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Ações");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(180);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdContratoFidic, SdContratoFidic>, ObservableValue<SdContratoFidic>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<SdContratoFidic, SdContratoFidic>() {
                        final HBox boxButtonsRow = new HBox(3);

                        final Button btnEditarContrato = FormButton.create(btn -> {
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EDIT, ImageUtils.IconSize._16));
                            btn.tooltip("Editar contrato");
                            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            btn.addStyle("xs").addStyle("warning");
                        });

                        final Button btnEnviarContrato = FormButton.create(btn -> {
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._16));
                            btn.tooltip("Enviar contrato");
                            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            btn.addStyle("xs").addStyle("primary");
                        });

                        final Button btnCancelarContrato = FormButton.create(btn -> {
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                            btn.tooltip("Cancelar contrato");
                            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            btn.addStyle("xs").addStyle("danger");
                        });

                        final Button btnCadastrarBordero = FormButton.create(btn -> {
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.PEDIDO, ImageUtils.IconSize._16));
                            btn.tooltip("Cadastrar/Alterar bordero do contrato");
                            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            btn.addStyle("xs").addStyle("info");
                        });

                        final Button btnExportarExcel = FormButton.create(btn -> {
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EXCEL, ImageUtils.IconSize._16));
                            btn.tooltip("Exportar para excel");
                            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            btn.addStyle("xs").addStyle("success");
                        });

                        @Override
                        protected void updateItem(SdContratoFidic item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {

                                btnEditarContrato.setOnAction(evt -> editarContrato(item));

                                btnEnviarContrato.setOnAction(evt -> {
                                    enviarContrato(item);
                                });

                                btnCancelarContrato.setOnAction(evt -> {
                                    cancelarContrato(item);
                                });

                                btnCadastrarBordero.setOnAction(evt -> {
                                    cadastrarBordero(item);
                                });

                                btnExportarExcel.setOnAction(evt -> {
                                    exportarExcel(item);
                                });

                                btnEnviarContrato.disableProperty().bind(item.enviadoProperty().or(item.borderoProperty().isNull()));
                                btnEditarContrato.disableProperty().bind(item.enviadoProperty());
                                btnCancelarContrato.disableProperty().bind(item.enviadoProperty());

                                boxButtonsRow.getChildren().clear();
                                boxButtonsRow.getChildren().addAll(btnEditarContrato, btnEnviarContrato, btnCancelarContrato, btnCadastrarBordero, btnExportarExcel);
                                setGraphic(boxButtonsRow);
                            }
                        }
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Número");
                    cln.width(60);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdContratoFidic, SdContratoFidic>, ObservableValue<SdContratoFidic>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Bordero");
                    cln.width(60);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdContratoFidic, SdContratoFidic>, ObservableValue<SdContratoFidic>>) param -> new ReadOnlyObjectWrapper(param.getValue().getBordero() == null ? "" : param.getValue().getBordero().getNumero()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Conta");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdContratoFidic, SdContratoFidic>, ObservableValue<SdContratoFidic>>) param -> new ReadOnlyObjectWrapper(param.getValue().getBordero() == null || param.getValue().getBordero().getContaObj() == null ? "" : param.getValue().getBordero().getContaObj().getConta()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Carteira");
                    cln.width(50);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdContratoFidic, SdContratoFidic>, ObservableValue<SdContratoFidic>>) param -> new ReadOnlyObjectWrapper(param.getValue().getBordero() == null || param.getValue().getBordero().getCarteiraObj() == null ? "" : param.getValue().getBordero().getCarteiraObj().getCarteira()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Banco");
                    cln.width(180);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdContratoFidic, SdContratoFidic>, ObservableValue<SdContratoFidic>>) param -> new ReadOnlyObjectWrapper(param.getValue().getBordero() == null || param.getValue().getBordero().getContaObj() == null ? "" : param.getValue().getBordero().getContaObj().getBanco().toString()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Valor Total");
                    cln.width(100);
                    cln.order(Comparator.comparing(o -> ((SdContratoFidic) o).getValor()));
                    cln.value((Callback<TableColumn.CellDataFeatures<SdContratoFidic, SdContratoFidic>, ObservableValue<SdContratoFidic>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<SdContratoFidic, SdContratoFidic>() {
                        @Override
                        protected void updateItem(SdContratoFidic item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                setText(StringUtils.toMonetaryFormat(item.getValor(), 2));
                            }
                        }
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Data");
                    cln.width(60);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdContratoFidic, SdContratoFidic>, ObservableValue<SdContratoFidic>>) param -> new ReadOnlyObjectWrapper(param.getValue().getData()));
                    cln.order(Comparator.comparing(o -> ((LocalDate) o)));
                    cln.format(param -> new TableCell<LocalDate, LocalDate>() {
                        @Override
                        protected void updateItem(LocalDate item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                setText(StringUtils.toShortDateFormat(item));
                            }
                        }
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Usuário");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdContratoFidic, SdContratoFidic>, ObservableValue<SdContratoFidic>>) param -> new ReadOnlyObjectWrapper(param.getValue().getUsuario()));
                }).build()
        );
    });

    private final FormTableView<SdItemContratoFidic> tblTitulos = FormTableView.create(SdItemContratoFidic.class, table -> {
        table.title("Títulos");
        table.items.bind(titulosBean);
        table.expanded();
        table.factoryRow(param -> new TableRow<SdItemContratoFidic>() {
            @Override
            protected void updateItem(SdItemContratoFidic item, boolean empty) {
                super.updateItem(item, empty);
                clear();
                if (item != null && !empty) {
                    setContextMenu(FormContextMenu.create(cmenu -> {
                        cmenu.addItem(menuItem -> {
                            menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.FINANCEIRO, ImageUtils.IconSize._16));
                            menuItem.setOnAction(evt -> {
                                recomprarTitulo(item);
                            });
                            menuItem.setText("Recomprar Título");
                        });
                    }));

                    if (item.isRecomprado())
                        getStyleClass().add("table-row-warning");
//                    else if (item.getTitulo().getTituloContrato() != null && item.getTitulo().getTituloContrato().getDtPagto() != null)
//                        getStyleClass().add("table-row-success");

                }
            }

            public void clear() {
                getStyleClass().removeAll("table-row-success", "table-row-warning");
            }
        });
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Número");
                    cln.width(70);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemContratoFidic, SdItemContratoFidic>, ObservableValue<SdItemContratoFidic>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTitulo().getNumero()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Cliente");
                    cln.width(320);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemContratoFidic, SdItemContratoFidic>, ObservableValue<SdItemContratoFidic>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTitulo().getEntidade()));
                    cln.order(Comparator.comparing(o -> ((VSdDadosEntidade) o).getRazaosocial()));
                    cln.format(param -> new TableCell<VSdDadosEntidade, VSdDadosEntidade>() {
                        @Override
                        protected void updateItem(VSdDadosEntidade item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                setText(item.toString());
                            }
                        }
                    });
                }).build(),
//                FormTableColumn.create(cln -> {
//                    cln.title("Conta Borderô");
//                    cln.width(90);
//                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemContratoFidic, SdItemContratoFidic>, ObservableValue<SdItemContratoFidic>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTitulo().getTituloContrato() == null ? "" : param.getValue().getTitulo().getTituloContrato().getContaBordero().getConta()));
//                }).build(),
//                FormTableColumn.create(cln -> {
//                    cln.title("Conta Baixa");
//                    cln.width(90);
//                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemContratoFidic, SdItemContratoFidic>, ObservableValue<SdItemContratoFidic>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTitulo().getTituloContrato() == null || param.getValue().getTitulo().getTituloContrato().getContaBaixa() == null ? "" : param.getValue().getTitulo().getTituloContrato().getContaBaixa().getConta()))
//                    ;
//                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Valor");
                    cln.width(90);
                    cln.order(Comparator.comparing(o -> ((SdItemContratoFidic) o).getTitulo().getValorTitulo()));
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemContratoFidic, SdItemContratoFidic>, ObservableValue<SdItemContratoFidic>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<SdItemContratoFidic, SdItemContratoFidic>() {
                        @Override
                        protected void updateItem(SdItemContratoFidic item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                setText(StringUtils.toMonetaryFormat(item.getTitulo().getValorTitulo(), 2));
                            }
                        }
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Dt Vencimento");
                    cln.width(90);
                    cln.dateColumn();
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemContratoFidic, SdItemContratoFidic>, ObservableValue<SdItemContratoFidic>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTitulo().getDtvencto()));
                }).build()
//                FormTableColumn.create(cln -> {
//                    cln.title("Data Pagamento");
//                    cln.width(120);
//                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemContratoFidic, SdItemContratoFidic>, ObservableValue<SdItemContratoFidic>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTitulo().getTituloContrato() == null || param.getValue().getTitulo().getTituloContrato().getDtPagto() == null ? "" : StringUtils.toShortDateFormat(param.getValue().getTitulo().getTituloContrato().getDtPagto())));
//                }).build()
        );
    });

    private final FormTableView<Receber> tblTitulosCriacao = FormTableView.create(Receber.class, table -> {
        table.title("Títulos");
        table.expanded();
        table.items.bind(titulosCriacaoBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(80);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<Receber, Receber>, ObservableValue<Receber>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<Receber, Receber>() {
                        final HBox boxButtonsRow = new HBox(3);

                        final Button btnRemover = FormButton.create(btn -> {
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                            btn.tooltip("Remover do contrato");
                            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            btn.addStyle("xs").addStyle("danger");
                        });

                        final Button btnRemoverCliente = FormButton.create(btn -> {
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ENTIDADE, ImageUtils.IconSize._16));
                            btn.tooltip("Remover cliente do contrato");
                            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            btn.addStyle("xs").addStyle("warning");
                        });

                        @Override
                        protected void updateItem(Receber item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                btnRemover.setOnAction(evt -> {
                                    titulosCriacaoBean.remove(item);
                                    titulosRecomendados.remove(item);
                                    titulosAuxiliaresBean.add(item);
                                    titulosAuxiliares.add(item);
                                });

                                btnRemoverCliente.setOnAction(evt -> {
                                    List<Receber> receberCliente = titulosCriacaoBean.stream().filter(it -> it.getCodcli().equals(item.getCodcli())).collect(Collectors.toList());
                                    titulosCriacaoBean.removeAll(receberCliente);
                                    titulosRecomendados.removeAll(receberCliente);
                                    titulosAuxiliaresBean.addAll(receberCliente);
                                    titulosAuxiliares.addAll(receberCliente);
                                });
                                boxButtonsRow.setAlignment(Pos.CENTER);
                                boxButtonsRow.getChildren().clear();
                                boxButtonsRow.getChildren().addAll(btnRemover, btnRemoverCliente);
                                setGraphic(boxButtonsRow);
                            }
                        }
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Número");
                    cln.width(65);
                    cln.value((Callback<TableColumn.CellDataFeatures<Receber, Receber>, ObservableValue<Receber>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumero()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Cliente");
                    cln.width(300);
                    cln.order(Comparator.comparing(o -> ((VSdDadosEntidade) o).getRazaosocial()));
                    cln.format(param -> new TableCell<VSdDadosEntidade, VSdDadosEntidade>() {
                        @Override
                        protected void updateItem(VSdDadosEntidade item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                setText(item.toString());
                            }
                        }
                    });
                    cln.value((Callback<TableColumn.CellDataFeatures<Receber, Receber>, ObservableValue<Receber>>) param -> new ReadOnlyObjectWrapper(param.getValue().getEntidade()));

                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Tipo");
                    cln.width(50);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<Receber, Receber>, ObservableValue<Receber>>) param -> new ReadOnlyObjectWrapper(param.getValue().getEntidade() == null ? "" : param.getValue().getEntidade().getClassecliente()));

                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("UF");
                    cln.width(40);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<Receber, Receber>, ObservableValue<Receber>>) param -> new ReadOnlyObjectWrapper(param.getValue().getEntidade() == null || param.getValue().getEntidade().getCodCid() == null ? "" : param.getValue().getEntidade().getCodCid().getCodEst().getSigla()));

                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Representante");
                    cln.width(150);
                    cln.order(Comparator.comparing(o -> ((Represen) o).getNome()));
                    cln.value((Callback<TableColumn.CellDataFeatures<Receber, Receber>, ObservableValue<Receber>>) param -> new ReadOnlyObjectWrapper(param.getValue().getRepresentante()));
                    cln.format(param -> new TableCell<Represen, Represen>() {
                        @Override
                        protected void updateItem(Represen item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                setText(item.toString());
                            }
                        }
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Emissão");
                    cln.width(75);
                    cln.dateColumn();
                    cln.value((Callback<TableColumn.CellDataFeatures<Receber, Receber>, ObservableValue<Receber>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDtemissao()));

                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Vencimento");
                    cln.width(75);
                    cln.value((Callback<TableColumn.CellDataFeatures<Receber, Receber>, ObservableValue<Receber>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDtvencto()));
                    cln.dateColumn();
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Valor");
                    cln.width(65);
                    cln.value((Callback<TableColumn.CellDataFeatures<Receber, Receber>, ObservableValue<Receber>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.order(Comparator.comparing(o -> ((Receber) o).getValorTitulo()));
                    cln.format(param -> new TableCell<Receber, Receber>() {
                        @Override
                        protected void updateItem(Receber item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                setText(StringUtils.toMonetaryFormat(item.getValorTitulo(), 2));
                            }
                        }
                    });
                }).build()

        );
    });

    private final FormTableView<Receber> tblTitulosCriacaoAuxiliar = FormTableView.create(Receber.class, table -> {
        table.title("Títulos Auxiliares");
        table.expanded();
        table.items.bind(titulosAuxiliaresBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(80);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<Receber, Receber>, ObservableValue<Receber>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<Receber, Receber>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnAdd = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._16));
                                btn.tooltip("Adicionar ao contrato");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("success");
                            });
                            final Button btnAddCliente = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ENTIDADE, ImageUtils.IconSize._16));
                                btn.tooltip("Adicionar cliente ao contrato");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("primary");
                            });

                            @Override
                            protected void updateItem(Receber item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    btnAdd.setOnAction(evt -> {
                                        titulosCriacaoBean.add(item);
                                        titulosRecomendados.add(item);
                                        titulosAuxiliaresBean.remove(item);
                                        titulosAuxiliares.remove(item);
                                    });
                                    btnAddCliente.setOnAction(evt -> {
                                        List<Receber> receberCliente = titulosAuxiliaresBean.stream().filter(it -> it.getCodcli().equals(item.getCodcli())).collect(Collectors.toList());
                                        titulosAuxiliaresBean.removeAll(receberCliente);
                                        titulosAuxiliares.removeAll(receberCliente);
                                        titulosCriacaoBean.addAll(receberCliente);
                                        titulosRecomendados.addAll(receberCliente);
                                    });
                                    boxButtonsRow.setAlignment(Pos.CENTER);
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnAdd, btnAddCliente);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Número");
                    cln.width(65);
                    cln.value((Callback<TableColumn.CellDataFeatures<Receber, Receber>, ObservableValue<Receber>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumero()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Cliente");
                    cln.width(300);
                    cln.order(Comparator.comparing(o -> ((VSdDadosEntidade) o).getRazaosocial()));
                    cln.format(param -> new TableCell<VSdDadosEntidade, VSdDadosEntidade>() {
                        @Override
                        protected void updateItem(VSdDadosEntidade item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                setText(item.toString());
                            }
                        }
                    });
                    cln.value((Callback<TableColumn.CellDataFeatures<Receber, Receber>, ObservableValue<Receber>>) param -> new ReadOnlyObjectWrapper(param.getValue().getEntidade()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Emissão");
                    cln.width(75);
                    cln.dateColumn();
                    cln.value((Callback<TableColumn.CellDataFeatures<Receber, Receber>, ObservableValue<Receber>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDtemissao()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Vencimento");
                    cln.width(75);
                    cln.dateColumn();
                    cln.value((Callback<TableColumn.CellDataFeatures<Receber, Receber>, ObservableValue<Receber>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDtvencto()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Valor");
                    cln.width(65);
                    cln.value((Callback<TableColumn.CellDataFeatures<Receber, Receber>, ObservableValue<Receber>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.order(Comparator.comparing(o -> ((Receber) o).getValorTitulo()));
                    cln.format(param -> new TableCell<Receber, Receber>() {
                        @Override
                        protected void updateItem(Receber item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                setText(StringUtils.toMonetaryFormat(item.getValorTitulo(), 2));
                            }
                        }
                    });
                }).build()

        );
    });

    private final FormTableView<CadBan> tblBancos = FormTableView.create(CadBan.class, table -> {
        table.title("Bancos");
        table.items.bind(bancosBean);
        table.expanded();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Número");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<CadBan, CadBan>, ObservableValue<CadBan>>) param -> new ReadOnlyObjectWrapper(param.getValue().getBanco()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Nome");
                    cln.width(350);
                    cln.value((Callback<TableColumn.CellDataFeatures<CadBan, CadBan>, ObservableValue<CadBan>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNomeBanco()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Gera Contrato");
                    cln.width(80);
                    cln.order(Comparator.comparing(o -> ((CadBan) o).isGeraContrato()));
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<CadBan, CadBan>, ObservableValue<CadBan>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<CadBan, CadBan>() {
                            @Override
                            protected void updateItem(CadBan item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(FormButton.create(btn -> {
                                        btn.addStyle(item.isGeraContrato() ? "success" : "danger");
                                        btn.icon(ImageUtils.getIcon(item.isGeraContrato() ? ImageUtils.Icon.STATUS_SIM : ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._24));
                                        btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                        btn.setOnAction(evt -> {
                                            item.setGeraContrato(!item.isGeraContrato());
                                            new FluentDao().merge(item);
                                            MessageBox.create(message -> {
                                                message.message("Status alterado com sucesso!");
                                                message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                message.position(Pos.TOP_RIGHT);
                                                message.notification();
                                            });
                                            tblBancos.refresh();
                                        });
                                    }));
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Fornecedor");
                    cln.width(450);
                    cln.order(Comparator.comparing(o -> ((CadBan) o).getCodFor() == null ? ((CadBan) o).toString() : ((CadBan) o).getCodFor().getCodcli()));
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<CadBan, CadBan>, ObservableValue<CadBan>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<CadBan, CadBan>() {
                            @Override
                            protected void updateItem(CadBan item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(FormFieldSingleFind.create(Entidade.class, field -> {
                                        field.withoutTitle();
                                        field.dividedWidth(450);
                                        if (item.getCodFor() != null) field.value.setValue(item.getCodFor());
                                        field.code.focusedProperty().addListener((observable, oldValue, newValue) -> {
                                            if (!newValue && field.value.getValue() != null && ((VSdDadosEntidade) field.value.getValue()).getCodcli().equals(item.getCodFor().getCodcli())) {
                                                item.setCodFor((VSdDadosEntidade) field.value.getValue());
                                                new FluentDao().merge(item);
                                                MessageBox.create(message -> {
                                                    message.message("Fornecedor alterado com sucesso!");
                                                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                    message.position(Pos.TOP_RIGHT);
                                                    message.notification();
                                                });
                                            }
                                        });
                                    }).build());
                                }
                            }
                        };
                    });
                }).build()
        );
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Filter">
    private final FormFieldMultipleFind<Bordero> borderoFilter = FormFieldMultipleFind.create(Bordero.class, field -> {
        field.title("Bordero");
    });

    private final FormFieldMultipleFind<CadConta> contaFilter = FormFieldMultipleFind.create(CadConta.class, field -> {
        field.title("Conta");
    });

    private final FormFieldMultipleFind<CadBan> bancoFilter = FormFieldMultipleFind.create(CadBan.class, field -> {
        field.title("Banco");
    });

    private final FormFieldMultipleFind<Carteira> carteiraFilter = FormFieldMultipleFind.create(Carteira.class, field -> {
        field.title("Carteira");
    });

    private final FormFieldMultipleFind<CadBan> filterBancoTabControle = FormFieldMultipleFind.create(CadBan.class, field -> {
        field.title("Banco");
        field.width(200);
    });

    private final FormFieldText filterNomeBancoTabControle = FormFieldText.create(field -> {
        field.title("Nome do Banco");
        field.width(450);
    });

    private final FormFieldText textFilterTitulo = FormFieldText.create(field -> {
        field.title("Número Título");
        field.width(200);
        field.toUpper();
    });

    private final FormFieldText textFilterNomeCliente = FormFieldText.create(field -> {
        field.title("Nome Cliente");
        field.width(400);
        field.toUpper();
    });
    // </editor-fold>

    //<editor-fold desc="TABS">
    private final VBox tabListagem = (VBox) this.tabs.getTabs().get(0).getContent();
    private final VBox tabCriacao = (VBox) this.tabs.getTabs().get(1).getContent();
    private final VBox tabControle = (VBox) this.tabs.getTabs().get(2).getContent();
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Criacao">

    private final FormFieldText textValorTotalContrato = FormFieldText.create(field -> {
        field.title("Valor Contrato");
        field.label("R$");
        field.width(155);
        field.value.setValue("1000000");
    });

    private final FormFieldDatePeriod datePeriodDataEmissao = FormFieldDatePeriod.create(field -> {
        field.valueBegin.set(LocalDate.now().minusMonths(3));
        field.valueEnd.set(LocalDate.now());
        field.withLabels();
        field.title("Data de Emissão");
    });

    private final FormFieldRange fieldRangeAtrasoCliente = FormFieldRange.create(field -> {
        field.title("Atraso Cliente");
        field.width(300);
        field.getFieldBegin().width(160);
        field.getFieldEnd().width(160);
        field.defaultValue(0, 30);
    });

    private final FormFieldRange rangeValorTitulo = FormFieldRange.create(field -> {
        field.title("Valor Títulos");
        field.width(300);
        field.getFieldBegin().width(155);
        field.getFieldEnd().width(155);
        field.defaultValue(800, 1000);
    });

    private final FormFieldSegmentedButton<String> segBtnClientesSerasa = FormFieldSegmentedButton.create(field -> {
        field.title("Clientes c/ dívidas no Serasa");
        field.alwaysSelected();
        field.options(
                field.option("Incluir", "S", FormFieldSegmentedButton.Style.SUCCESS),
                field.option("Não Incluir", "N", FormFieldSegmentedButton.Style.WARNING),
                field.option("Ambos", "A", FormFieldSegmentedButton.Style.PRIMARY)
        );
        field.select(1);
    });

    private final FormFieldDatePeriod datePeriodDataVencto = FormFieldDatePeriod.create(field -> {
        field.valueBegin.set(LocalDate.now());
        field.valueEnd.set(LocalDate.now().plusMonths(3));
        field.withLabels();
        field.title("Data de Vencimento");
    });

    private final FormFieldText textTaxa = FormFieldText.create(txt -> {
        txt.title("Taxa");
        txt.value.set("1");
        txt.label("%");
        txt.width(130);
    });

    private final FormFieldMultipleFind<TabSit> multFindTabSit = FormFieldMultipleFind.create(TabSit.class, field -> {

        field.objectValues.add(new FluentDao().selectFrom(TabSit.class).where(it -> it.equal("codigo", "25")).singleResult());
    });

    private final FormBox boxParametros = FormBox.create(boxParans -> {
        boxParans.vertical();
        boxParans.verticalScroll();

    });

    private final FormFieldText textValorTotalCriacao = FormFieldText.create(text -> {
        text.title("Valor Total");
        text.editable.set(false);
        text.width(220);
    });

    private final FormFieldText textValorEsperadoTotal = FormFieldText.create(text -> {
        text.title("Valor Esperado + Taxa");
        text.addStyle("info");
        text.editable.set(false);
        text.width(220);
    });

    private final FormFieldText textNumeroContratoSelecionado = FormFieldText.create(text -> {
        text.title("Número Contrato");
        text.editable.set(false);
        text.width(100);
    });

    private final FormFieldText textBorderoContrato = FormFieldText.create(text -> {
        text.title("Bordero Contrato");
        text.editable.set(false);
        text.width(100);
    });

    private final FormFieldSegmentedButton<String> segBtnManterTitulos = FormFieldSegmentedButton.create(field -> {
        field.title("Manter Títulos");
        field.alwaysSelected();
        field.options(
                field.option("Títulos", "T", FormFieldSegmentedButton.Style.SUCCESS),
                field.option("Auxiliares", "A", FormFieldSegmentedButton.Style.WARNING),
                field.option("Ambos", "T.A", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("Não Manter", "", FormFieldSegmentedButton.Style.DANGER)
        );
        field.select(3);
    });

    private final FormFieldMultipleFind<SitCli> multiFindSitCli = FormFieldMultipleFind.create(SitCli.class, field -> {
        field.title("Tipo de Cliente");
        field.width(150);
        field.toUpper();
    });

    private final CheckBox checkBoxTitulosSemObs = new CheckBox("Apenas Sem Observação");

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Buttons">
    private final FormButton btnSalvarContrato = FormButton.create(btn -> {
        btn.addStyle("primary");
        btn.title("Salvar");
        btn.width(125);
        btn.tooltip("Salvar Contrato");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.SALVAR, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                message.message("Tem certeza que deseja salvar esse contrato?");
                message.showAndWait();
            }).value.get())) {
                if (titulosCriacaoBean.size() == 0) {
                    MessageBox.create(message -> {
                        message.message("Selecione ao menos um título para criar um contrato.");
                        message.type(MessageBox.TypeMessageBox.WARNING);
                        message.position(Pos.TOP_RIGHT);
                        message.notification();
                    });
                    return;
                }
                salvarContrato();
                limparCampos();
            }
        });
    });

    private final FormButton btnExcluirContrato = FormButton.create(btn -> {
        btn.addStyle("danger");
        btn.title("Excluir");
        btn.width(125);
        btn.tooltip("Excluir Contrato");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> cancelarContrato(contratoSelcionado.get()));
    });

    private final FormButton btnCancelarAlteracoesContrato = FormButton.create(btn -> {
        btn.addStyle("warning");
        btn.title("Cancelar");
        btn.width(125);
        btn.tooltip("Cancelar Alterações no Contrato");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.RETORNAR, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                message.message("Tem certeza que deseja cancelar as alterações nesse contrato?");
                message.showAndWait();
            }).value.get())) {
                limparCampos();
            }
        });
    });

    private final FormButton btnFinalizarContrato = FormButton.create(btn -> {
        btn.addStyle("success");
        btn.title("Finalizar");
        btn.width(125);
        btn.tooltip("Confirmar Contrato");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                message.message("Tem certeza que deseja salvar esse contrato?");
                message.showAndWait();
            }).value.get())) {
                if (titulosCriacaoBean.size() == 0) {
                    MessageBox.create(message -> {
                        message.message("Selecione ao menos um título para criar um contrato.");
                        message.type(MessageBox.TypeMessageBox.WARNING);
                        message.position(Pos.TOP_RIGHT);
                        message.notification();
                    });
                    return;
                }
                salvarContrato();
                cadastrarBordero(contratoSelcionado.get());
                limparCampos();
            }
        });
    });

    private final FormButton btnAdicionarCliente = FormButton.create(btn -> {
        btn.addStyle("success");
        btn.title("Adicionar Cliente");
        btn.setMaxWidth(Double.MAX_VALUE);
        btn.tooltip("Adicionar Filtro de Cliente");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            InputBox<Entidade> inputMessage = InputBox.build(Entidade.class, inputBox -> {
                inputBox.type(MessageBox.TypeMessageBox.INPUT);
                inputBox.message("Selecione o Cliente:");
            });
            inputMessage.showAndWait();
            if (inputMessage.value.getValue() == null) return;

            adicionarFiltroCliente(new ClienteConsulta(inputMessage.value.getValue()));
        });
    });

    private final FormButton btnBuscarTitulos = FormButton.create(btn -> {
        btn.addStyle("primary");
        btn.title("Buscar Títulos");
        btn.tooltip("Buscar Títulos");
//        btn.width(125);
        btn.setMaxWidth(Double.MAX_VALUE);
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.SEARCH, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            if (validarCampos()) {
                MessageBox.create(message -> {
                    message.message("Preencha todos os campos antas.");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
                return;
            }
            buscarTitulos(
                    contratoSelcionado.get(),
                    textValorTotalContrato.value.getValueSafe(),
                    datePeriodDataEmissao.valueBegin.getValue(),
                    datePeriodDataEmissao.valueEnd.getValue(),
                    segBtnClientesSerasa.value.getValue(),
                    datePeriodDataVencto.valueBegin.getValue(),
                    datePeriodDataVencto.valueEnd.getValue(),
                    fieldRangeAtrasoCliente.valueBegin.getValue(),
                    fieldRangeAtrasoCliente.valueEnd.getValue(),
                    rangeValorTitulo.valueBegin.doubleValue(),
                    rangeValorTitulo.valueEnd.doubleValue(),
                    multFindTabSit.objectValues.stream().map(TabSit::getCodigo).toArray(),
                    textTaxa.value.getValueSafe(),
                    multiFindSitCli.objectValues.stream().map(SitCli::getCodigo).toArray(),
                    parametrosBean,
                    clientesConsultaBean,
                    segBtnManterTitulos.value.getValue(),
                    checkBoxTitulosSemObs.isSelected()
            );
            carregaCamposCriacao();
        });
    });

    private final FormButton btnLimparFiltros = FormButton.create(btn -> {
        btn.addStyle("warning");
        btn.setMaxWidth(Double.MAX_VALUE);
        btn.title("Limpar Filtros");
        btn.tooltip("Limpar Filtros");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.LIMPAR, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            limparFiltrosCriacao();
        });
    });

    private final FormButton btnLimparFiltrosListagem = FormButton.create(btn -> {
        btn.addStyle("warning");
        btn.setMaxWidth(Double.MAX_VALUE);
        btn.title("Limpar Filtros");
        btn.setAlignment(Pos.BOTTOM_LEFT);
        btn.tooltip("Limpar Filtros");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.LIMPAR, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {

            limparFiltrosListagem();
        });
    });

    // </editor-fold>

    public ContratosFIDCView() {
        super("Contratos FIDC", ImageUtils.getImage(ImageUtils.Icon.FATURAR_PEDIDO), new String[]{"Listagem", "Criação", "Controle"});
        initListagem();
        initTabCriacao();
        initControle();
        configureContratoSelection();
        titulosCriacaoBean.sizeProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                BigDecimal valorTotalTitulos = titulosCriacaoBean.stream().map(Receber::getValorTitulo).reduce(BigDecimal.ZERO, BigDecimal::add);
                if (!textValorTotalContrato.value.getValueSafe().equals("") && !textTaxa.value.getValueSafe().equals("")) {
                    BigDecimal valorMaximo = new BigDecimal(textValorTotalContrato.value.getValue()).multiply(BIGDECIMAL_100.add(new BigDecimal(textTaxa.value.getValueSafe().replace(",", "."))).divide(BIGDECIMAL_100, 2, RoundingMode.HALF_UP));
                    BigDecimal diferenca = valorTotalTitulos.subtract(valorMaximo);
                    textValorTotalCriacao.addStyle(diferenca.compareTo(BigDecimal.ZERO) >= 0 ? "success" : "danger");
                    textValorTotalCriacao.value.setValue(StringUtils.toMonetaryFormat(valorTotalTitulos, 2) + " ( " + (diferenca.compareTo(BigDecimal.ZERO) >= 0 ? "+" : "-") + StringUtils.toMonetaryFormat(diferenca.setScale(2, RoundingMode.HALF_UP).abs(), 2) + ")");
                } else {
                    textValorTotalCriacao.value.setValue(StringUtils.toMonetaryFormat(valorTotalTitulos, 2));
                }

            }
        });
        desenhaBoxParametros();
    }

    public void initListagem() {
        tabListagem.getChildren().add(FormBox.create(root -> {
            root.horizontal();
            root.expanded();
            root.add(col -> {
                col.vertical();
                col.add(FormBox.create(box -> {
                    box.horizontal();
                    box.add(FormTitledPane.create(filter -> {
                        filter.filter();
                        filter.add(FormBox.create(boxFilter -> {
                            boxFilter.horizontal();
                            boxFilter.add(borderoFilter.build(), bancoFilter.build(), contaFilter.build(), carteiraFilter.build());
                        }));
                        filter.find.setOnAction(evt -> {
                            /**
                             * task:  * return:
                             */
                            new RunAsyncWithOverlay(this).exec(task -> {
                                buscarContratos(
                                        borderoFilter.objectValues.stream().map(Bordero::getNumero).toArray(),
                                        bancoFilter.objectValues.stream().map(CadBan::getBanco).toArray(),
                                        contaFilter.objectValues.stream().map(CadConta::getConta).toArray(),
                                        carteiraFilter.objectValues.stream().map(Carteira::getCarteira).toArray()
                                );
                                return ReturnAsync.OK.value;
                            }).addTaskEndNotification(taskReturn -> {
                                if (taskReturn.equals(ReturnAsync.OK.value)) {
                                    contratosBean.set(FXCollections.observableArrayList(contratos));
                                }
                            });
                        });
                        filter.clean.setOnAction(evt -> {
                            borderoFilter.clear();
                            bancoFilter.clear();
                            contaFilter.clear();
                        });
                    }).build());
                }).build());
                col.add(tblContratos.build());
            });
            root.add(col -> {
                col.vertical();
                col.expanded();
                col.add(boxTotais -> {
                    boxTotais.vertical();
                    boxTotais.title("Totais");
                    boxTotais.add(row -> {
                        row.horizontal();
                        row.add(fieldValorTotal.build(), fieldValorPago.build(), fieldValorRecomprado.build(), fieldValorRestante.build());
                    });
                });
                col.add(boxFiltros -> {
                    boxFiltros.horizontal();
                    boxFiltros.title("Filtros");
                    boxFiltros.alignment(Pos.BOTTOM_LEFT);
                    boxFiltros.add(textFilterTitulo.build(), textFilterNomeCliente.build(), btnLimparFiltrosListagem);
                });
                col.add(tblTitulos.build());
            });
        }));
    }

    public void initTabCriacao() {
        tabCriacao.getChildren().add(FormBox.create(root -> {
            root.horizontal();
            root.expanded();

            root.add(FormBox.create(boxRoot -> {
                boxRoot.vertical();
                boxRoot.verticalScroll();
                boxRoot.add(boxBtnAdd -> {
                    boxBtnAdd.vertical();
                    contratoSelcionado.addListener((observable, oldValue, newValue) -> {
                        if (newValue == null) {
                            boxRoot.add(boxBtnAdd, 0);
                        } else {
                            boxRoot.remove(boxBtnAdd);
                        }
                    });
                    boxBtnAdd.add(FormButton.create(btn -> {
                        btn.addStyle("success");
                        btn.title("Criar novo contrato");
                        btn.tooltip("Iniciar novo contrato");
                        btn.setPrefWidth(300);
                        btn.setMaxWidth(Double.MAX_VALUE);
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._24));
                        btn.disable.bind(contratoSelcionado.isNotNull());
                        btn.setOnAction(evt -> criarNovoContrato());
                    }));
                });
                FormBox.create(boxFilter -> {
                    contratoSelcionado.addListener((observable, oldValue, newValue) -> {
                        if (newValue == null) {
                            boxRoot.remove(boxFilter);
                        } else {
                            try {
                                boxRoot.add(boxFilter, 0);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    boxFilter.vertical();
                    boxFilter.verticalScroll();
                    boxFilter.disable.bind(contratoSelcionado.isNull());
                    boxFilter.add(FormTitledPane.create(titledPaneFilters -> {
                        titledPaneFilters.title("Filtros");
                        titledPaneFilters.addStyle("info");
                        titledPaneFilters.icon(ImageUtils.getImage(ImageUtils.Icon.FILTRO));
                        titledPaneFilters.add(boxTitledPaneFilter -> {
                            boxTitledPaneFilter.vertical();
                            boxTitledPaneFilter.expanded();
                            boxTitledPaneFilter.add(linha -> {
                                linha.horizontal();
                                linha.add(textValorTotalContrato.build(), multFindTabSit.build());
                            });
                            boxTitledPaneFilter.add(linha -> {
                                linha.horizontal();
                                linha.add(segBtnClientesSerasa.build(), textTaxa.build());
                            });
                            boxTitledPaneFilter.add(linha -> {
                                linha.horizontal();
                                linha.add(datePeriodDataEmissao.build());
                            });
                            boxTitledPaneFilter.add(linha -> {
                                linha.horizontal();
                                linha.add(datePeriodDataVencto.build());
                            });
                            boxTitledPaneFilter.add(linha -> {
                                linha.horizontal();
                                linha.add(fieldRangeAtrasoCliente.build());
                            });
                            boxTitledPaneFilter.add(linha -> {
                                linha.horizontal();
                                linha.add(rangeValorTitulo.build());
                            });
                            boxTitledPaneFilter.add(linha -> {
                                linha.horizontal();
                                linha.add(segBtnManterTitulos.build());
                            });
                            boxTitledPaneFilter.add(linha -> {
                                linha.horizontal();
                                linha.add(multiFindSitCli.build(), checkBoxTitulosSemObs);
                            });

                            boxTitledPaneFilter.add(btnLimparFiltros);
                        });
                    }));
                    boxFilter.add(boxFilterClients -> {
                        boxFilterClients.vertical();
                        boxFilterClients.setId("boxFilterClients");
                        boxFilterClients.alignment(Pos.CENTER);
                    });
                    boxFilter.add(box -> {
                        box.vertical();
                        box.add(btnAdicionarCliente);
                    });
                    boxFilter.add(boxTitledFilters -> {
                        boxTitledFilters.vertical();
                        boxTitledFilters.expanded();
                        boxTitledFilters.add(FormTitledPane.create(titledPaneParam -> {
                            titledPaneParam.title("Parâmetros");
                            titledPaneParam.addStyle("warning");
                            titledPaneParam.icon(ImageUtils.getImage(ImageUtils.Icon.AJUSTES));
                            titledPaneParam.setContextMenu(FormContextMenu.create(cmenu -> {
                                cmenu.addItem(item -> {
                                    item.setText("Adicionar Parâmetro");
                                    item.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._16));
                                    item.setOnAction(evt -> {
                                        if (parametrosBean.stream().anyMatch(it -> it.getField().equals(""))) {
                                            MessageBox.create(message -> {
                                                message.message("Você já possui um parâmetro vazio!");
                                                message.type(MessageBox.TypeMessageBox.WARNING);
                                                message.position(Pos.TOP_RIGHT);
                                                message.notification();
                                            });
                                            return;
                                        }
                                        parametrosBean.add(new ParametroConsulta(parametrosBean.getSize(), "", "ASC"));
                                        desenhaBoxParametros();
                                    });
                                });
                                cmenu.addItem(item -> {
                                    item.setText("Remover Parêmtros");
                                    item.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                    item.setOnAction(evt -> {
                                        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                                            message.message("Tem certeza que deseja excluir todos os parâmetros?");
                                            message.showAndWait();
                                        }).value.get())) {
                                            parametrosBean.clear();
                                            desenhaBoxParametros();
                                        }
                                    });
                                });
                            }));
                            titledPaneParam.add(boxParams -> {
                                boxParams.vertical();
                                boxParams.add(boxParametros);
                            });
                        }));

                        boxTitledFilters.add(footer -> {
                            footer.vertical();
                            footer.add(btnBuscarTitulos);
                        });
                    });
                });

            }));
            FormFieldMultipleFind<Receber> filterTitulosToAdd = FormFieldMultipleFind.create(Receber.class, field -> {
                field.title("Título Exclusivo");
                field.width(350);
            });
            root.add(col2 -> {
                col2.vertical();
                col2.expanded();
                col2.add(boxH -> {
                    boxH.horizontal();
                    boxH.alignment(Pos.BOTTOM_LEFT);
                    boxH.add(filterTitulosToAdd.build());
                    boxH.add(FormButton.create(btn -> {
                        btn.addStyle("success");
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._24));
                        btn.tooltip("Adicionar Título");
                        btn.setAlignment(Pos.BOTTOM_LEFT);
                        btn.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                        btn.setOnAction(evt -> {
                            if (filterTitulosToAdd.objectValues.size() != 0) {
                                filterTitulosToAdd.objectValues.forEach(item -> {
                                    if (titulosCriacaoBean.stream().noneMatch(it -> it.getNumero().equals(item.getNumero()))) {
                                        titulosCriacaoBean.add(item);
                                    } else {
                                        MessageBox.create(message -> {
                                            message.message("Título " + item.getNumero() + " já adicionado!");
                                            message.type(MessageBox.TypeMessageBox.WARNING);
                                            message.position(Pos.TOP_RIGHT);
                                            message.notification();
                                        });
                                    }
                                });
                                filterTitulosToAdd.clear();
                            }
                        });
                    }));
                });
                col2.add(tblTitulosCriacaoAuxiliar.build());
            });
            root.add(col3 -> {
                col3.vertical();
                col3.expanded();
                col3.add(header -> {
                    header.horizontal();
                    header.title("Informações Contrato");
                    header.add(textNumeroContratoSelecionado.build(), textBorderoContrato.build(), textValorTotalCriacao.build(), textValorEsperadoTotal.build());
                });
                col3.add(tblTitulosCriacao.build());
                col3.add(footer -> {
                    footer.horizontal();
                    footer.border();
                    footer.alignment(Pos.CENTER_RIGHT);
                    footer.disable.bind(contratoSelcionado.isNull());
                    footer.add(btnCancelarAlteracoesContrato, btnSalvarContrato, btnExcluirContrato, btnFinalizarContrato);
                });
            });
        }));
    }

    public void initControle() {
        tabControle.getChildren().add(FormBox.create(root -> {
            root.vertical();
            root.expanded();
            root.add(filterBox -> {
                filterBox.horizontal();
                filterBox.add(FormTitledPane.create(filter -> {
                    filter.filter();
                    filter.add(FormBox.create(boxFilter -> {
                        boxFilter.horizontal();
                        boxFilter.add(filterBancoTabControle.build(), filterNomeBancoTabControle.build());
                        TextFields.bindAutoCompletion(filterNomeBancoTabControle.textField, ((List<CadBan>) new FluentDao().selectFrom(CadBan.class).get().resultList()).stream().map(CadBan::getNomeBanco).collect(Collectors.toList()));
                    }));
                    filter.find.setOnAction(evt -> {
                        new RunAsyncWithOverlay(this).exec(task -> {
                            buscarBancos(filterBancoTabControle.objectValues.stream().map(CadBan::getBanco).toArray(), filterNomeBancoTabControle.value.getValueSafe());
                            return ReturnAsync.OK.value;
                        }).addTaskEndNotification(taskReturn -> {
                            if (taskReturn.equals(ReturnAsync.OK.value)) {
                                bancosBean.set(FXCollections.observableArrayList(bancos));
                                tblBancos.refresh();
                            }
                        });
                    });
                    filter.clean.setOnAction(evt -> {
                        filterBancoTabControle.clear();
                        filterNomeBancoTabControle.clear();
                    });
                }));
            });
            root.add(boxTbl -> {
                boxTbl.horizontal();
                boxTbl.expanded();
                boxTbl.add(tblBancos.build());
            });
        }));
        bancosBean.set(FXCollections.observableArrayList(((List<CadBan>) new FluentDao().selectFrom(CadBan.class).get().orderBy("geraContrato", OrderType.DESC).resultList())));
    }

    public void configureContratoSelection() {
        contratoSelcionado.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                carregaTitulos(newValue);
                carregaCampos(newValue);
            }
        });
    }

    private void carregaCampos(SdContratoFidic contrato) {
        if (contrato.getId() != 0) {
            contrato.setValor(contrato.getItems().stream().filter(it -> !it.isRecomprado()).reduce(BigDecimal.ZERO, (acc, obj) -> acc.add(obj.getTitulo().getValorTitulo()), BigDecimal::add));
            contrato = new FluentDao().merge(contrato);
        }

        fieldValorPago.value.setValue(contrato.getItems().stream().map(it -> it.getTitulo().getValorpago()).reduce(BigDecimal.ZERO, BigDecimal::add).toString());
        fieldValorTotal.value.setValue(contrato.getItems().stream().map(it -> it.getTitulo().getValorTitulo()).reduce(BigDecimal.ZERO, BigDecimal::add).toString());
        fieldValorRecomprado.value.setValue(contrato.getItems().stream().filter(SdItemContratoFidic::isRecomprado).map(it -> it.getTitulo().getValorTitulo()).reduce(BigDecimal.ZERO, BigDecimal::add).toString());
        fieldValorRestante.value.setValue(contrato.getItems().stream().map(it -> it.getTitulo().getValorTitulo().subtract(it.getTitulo().getValorpago())).reduce(BigDecimal.ZERO, BigDecimal::add).toString());

        tblContratos.refresh();
    }

    private void carregaTitulos(SdContratoFidic contrato) {
        titulosBean.set(FXCollections.observableArrayList(contrato.getItems()));
        listenersFiltro();
    }

    private void desenhaBoxParametros() {
        boxParametros.clear();
        if (parametrosBean.size() == 0) {
            boxParametros.add(FormLabel.create(lbl -> {
                lbl.setId("lblParametros");
                lbl.setText("Parâmetros vazios!\nClique com o botão direito para adicionar.");
            }));
            return;
        }
        for (ParametroConsulta parametro : parametrosBean.sorted(Comparator.comparing(ParametroConsulta::getIndice))) {
            boxParametros.add(box -> {
                box.horizontal();
                box.border();
                box.alignment(Pos.CENTER);
                box.add(FormButton.create(btn -> {
                    btn.addStyle("danger");
                    btn.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                    btn.tooltip("Remover Parâmetro");
                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
                    btn.setOnAction(evt -> {
                        try {
                            parametrosBean.remove(parametro);
                            parametrosBean.forEach(it -> it.setIndice(parametrosBean.indexOf(it)));
                            desenhaBoxParametros();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    });
                }));
                box.add(boxBtns -> {
                    boxBtns.vertical();
                    boxBtns.add(FormButton.create(btn -> {
                        btn.addStyle("warning").addStyle("xs");
                        btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CIMA, ImageUtils.IconSize._16));
                        if (parametro.getIndice() == 0) btn.disable.set(true);
                        btn.setOnAction(evt -> {
                            ParametroConsulta parametroConsulta = parametrosBean.stream().filter(it -> it.getIndice() == parametro.getIndice() - 1).findFirst().orElse(null);
                            if (parametroConsulta != null) {
                                parametro.setIndice(parametro.getIndice() - 1);
                                parametroConsulta.setIndice(parametroConsulta.getIndice() + 1);
                                desenhaBoxParametros();
                            }
                        });
                    }));
                    boxBtns.add(FormButton.create(btn -> {
                        btn.addStyle("warning").addStyle("xs");
                        btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.BAIXO, ImageUtils.IconSize._16));
                        if (parametro.getIndice() == parametrosBean.size() - 1) btn.disable.set(true);
                        btn.setOnAction(evt -> {
                            ParametroConsulta parametroConsulta = parametrosBean.stream().filter(it -> it.getIndice() == parametro.getIndice() + 1).findFirst().orElse(null);
                            if (parametroConsulta != null) {
                                parametro.setIndice(parametro.getIndice() + 1);
                                parametroConsulta.setIndice(parametroConsulta.getIndice() - 1);
                                desenhaBoxParametros();
                            }
                        });
                    }));
                });
                box.add(FormLabel.create(lbl -> {
                    lbl.setText(String.valueOf(parametro.getIndice()));
                }));
                box.add(FormFieldComboBox.create(String.class, combo -> {
                    combo.title("Atributo");
                    combo.width(130);
                    List<ColunaFilter> collect = Arrays.stream(Receber.class.getDeclaredFields()).filter(it -> it.isAnnotationPresent(ColunaFilter.class)).map(it -> it.getAnnotation(ColunaFilter.class)).collect(Collectors.toList());
                    combo.items(FXCollections.observableArrayList(collect.stream().map(ColunaFilter::descricao).collect(Collectors.toList())));
                    collect.stream().filter(it -> it.coluna().equals(parametro.getField())).findFirst().ifPresent(it -> combo.select(it.descricao()));
                    combo.getSelectionModel((observable, oldValue, newValue) -> {
                        if (newValue != null) {
                            parametro.setField(collect.stream().filter(it -> it.descricao().equals(newValue)).findFirst().get().coluna());
                        }
                    });

                }).build());
                box.add(FormFieldComboBox.create(String.class, combo -> {
                    combo.title("Ordenar Por");
                    combo.width(90);
                    combo.items(FXCollections.observableList(Arrays.asList("Maior", "Menor")));
                    combo.select(parametro.getTipoOrder().equals("DESC") ? "Maior" : "Menor");
                    combo.getSelectionModel((observable, oldValue, newValue) -> {
                        if (newValue != null) {
                            parametro.setTipoOrder(newValue.equals("Maior") ? "DESC" : "ASC");
                        }
                    });
                }).build());
            });
        }
    }

    private void carregaCamposCriacao() {
        BigDecimal valorMaximo = new BigDecimal(textValorTotalContrato.value.getValue()).multiply(BIGDECIMAL_100.add(new BigDecimal(textTaxa.value.getValueSafe().replace(",", "."))).divide(BIGDECIMAL_100, 2, RoundingMode.HALF_UP));
        titulosCriacaoBean.set(FXCollections.observableArrayList(titulosRecomendados));
        titulosAuxiliaresBean.set(FXCollections.observableArrayList(titulosAuxiliares));
        textValorEsperadoTotal.value.setValue(StringUtils.toMonetaryFormat(valorMaximo, 2));
        tblTitulosCriacao.refresh();
        tblTitulosCriacaoAuxiliar.refresh();
    }

    private boolean validarCampos() {
        return textValorTotalContrato.value.getValueSafe().equals("") ||
                datePeriodDataEmissao.valueBegin.getValue() == null ||
                datePeriodDataEmissao.valueEnd.getValue() == null ||
                segBtnClientesSerasa.value.getValue().equals("") ||
                datePeriodDataVencto.valueBegin.getValue() == null ||
                datePeriodDataVencto.valueEnd.getValue() == null ||
                multFindTabSit.objectValues.stream().map(TabSit::getCodigo).toArray().length == 0 ||
                textTaxa.value.getValueSafe().equals("");
    }

    private void adicionarFiltroCliente(ClienteConsulta cliente) {

        if (clientesConsultaBean.stream().anyMatch(it -> it.getCodcli() == cliente.getCodcli())) {
            MessageBox.create(message -> {
                message.message("Cliente já adiconado aos filtros!");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            return;
        }
        clientesConsultaBean.add(cliente);
        FormBox boxFilters = (FormBox) this.lookup("#boxFilterClients");
        boxFilters.add(FormTitledPane.create(titledPaneFilters -> {
            titledPaneFilters.title(cliente.getCodcli().toString());
            titledPaneFilters.addStyle("success");
            titledPaneFilters.setWrapText(true);
            titledPaneFilters.setGraphic(FormBox.create(boxGraphic -> {
                boxGraphic.horizontal();
                boxGraphic.alignment(Pos.CENTER);
                boxGraphic.add(FormButton.create(btn -> {
                    btn.addStyle("danger").addStyle("xs");
                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                    btn.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                    btn.tooltip("Remover esse filtro de cliente");
                    btn.setOnAction(evt -> {
                        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                            message.message("Tem certeza que deseja remover esse cliente dos filtros?");
                            message.showAndWait();
                        }).value.get())) {
                            clientesConsultaBean.remove(cliente);
                            boxFilters.remove(titledPaneFilters);
                            MessageBox.create(message -> {
                                message.message("Cliente removido dos filtros com sucesso!");
                                message.type(MessageBox.TypeMessageBox.CONFIRM);
                                message.position(Pos.TOP_RIGHT);
                                message.notification();
                            });
                        }
                    });
                }));
                ImageView iconEntidade = new ImageView();
                iconEntidade.setFitHeight(16.0);
                iconEntidade.setFitWidth(16.0);
                iconEntidade.setPickOnBounds(true);
                iconEntidade.setPreserveRatio(true);
                iconEntidade.setImage(ImageUtils.getImage(ImageUtils.Icon.ENTIDADE));
                boxGraphic.add(iconEntidade);
            }));
            titledPaneFilters.add(col1 -> {
                col1.vertical();
                col1.add(linha -> {
                    linha.horizontal();
                    linha.add(FormFieldText.create(field -> {
                        field.title("Valor Máximo do Cliente");
                        field.width(150);
                        field.label("R$");
                        field.value.bindBidirectional(cliente.valorMaximoProperty(), bigDecimalStringConverter);
                    }).build());
                    linha.add(FormFieldMultipleFind.create(TabSit.class, field -> {
                        field.width(125);
                        field.objectValues.bindBidirectional(cliente.situacoesProperty());
                    }).build());
                });

                col1.add(linha -> {
                    linha.horizontal();
                    linha.add(FormFieldDatePeriod.create(field -> {
                        field.title("Data Emissão");
                        field.withLabels();
                        field.valueBegin.bindBidirectional(cliente.dtEmissaoBeginProperty());
                        field.valueEnd.bindBidirectional(cliente.dtEmissaoEndProperty());
                    }).build());
                });

                col1.add(linha -> {
                    linha.horizontal();
                    linha.add(FormFieldDatePeriod.create(field -> {
                        field.title("Data Vencimento");
                        field.withLabels();
                        field.valueBegin.bindBidirectional(cliente.dtVenctoBeginProperty());
                        field.valueEnd.bindBidirectional(cliente.dtVenctoEndProperty());
                    }).build());
                });

                col1.add(linha -> {
                    linha.horizontal();
                    linha.add(FormFieldRange.create(field -> {
                        field.title("Valor Títulos");
                        field.defaultValue(800, 1000);
                        field.width(250);
                        field.getFieldBegin().width(125);
                        field.getFieldEnd().width(125);
                        field.valueBegin.bindBidirectional(cliente.valTituloBeginProperty());
                        field.valueEnd.bindBidirectional(cliente.valTituloEndProperty());
                    }).build());
                });

            });
        }));
    }

    private void limparFiltrosCriacao() {
        textValorTotalContrato.value.setValue("1000000");
        multFindTabSit.clear();
        datePeriodDataEmissao.valueBegin.set(LocalDate.now().minusMonths(3));
        datePeriodDataEmissao.valueEnd.set(LocalDate.now());
        fieldRangeAtrasoCliente.valueBegin.setValue(0);
        fieldRangeAtrasoCliente.valueEnd.setValue(30);
        rangeValorTitulo.defaultValue(800, 1000);
        segBtnClientesSerasa.select(1);
        datePeriodDataVencto.valueBegin.set(LocalDate.now());
        datePeriodDataVencto.valueEnd.set(LocalDate.now().plusMonths(3));
        textTaxa.value.set("1");
//        multFindTabSit.objectValues.clear();
//        multFindTabSit.objectValues.add(new FluentDao().selectFrom(TabSit.class).where(it -> it.equal("codigo", "25")).singleResult());
        parametrosBean.clear();
        clientesConsultaBean.clear();
    }

    private void limparTitulos() {
        titulosCriacaoBean.clear();
        titulosAuxiliaresBean.clear();
        titulosRecomendados.clear();
        titulosAuxiliares.clear();
    }

    private void salvarContrato() {

        contratoSelcionado.get().getItems().clear();
        contratoSelcionado.get().getItems().addAll(titulosCriacaoBean.stream().map(it -> new SdItemContratoFidic(contratoSelcionado.get(), it)).collect(Collectors.toList()));
        contratoSelcionado.get().setData(LocalDate.now());
        contratoSelcionado.get().setUsuario(Globals.getNomeUsuario());


        try {
            if (contratoSelcionado.get().getId() == 0) {
                contratoSelcionado.set(new FluentDao().persist(contratoSelcionado.get()));
//                new FluentDao().persistAll(contratoSelcionado.get().getItems());
            } else {
//                new FluentDao().mergeAll(contratoSelcionado.get().getItems());
                contratoSelcionado.set(new FluentDao().merge(contratoSelcionado.get()));
            }
            JPAUtils.refresh(contratoSelcionado.get());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (contratoSelcionado.get().getBordero() != null)
            atualizarBorderoItens(contratoSelcionado.get());

        MessageBox.create(message -> {
            message.message("Contrato salvo com sucesso!");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });

    }

    private void atualizarBorderoItens(SdContratoFidic sdContratoFidic) {
        if (sdContratoFidic.getBordero() != null) {

            for (SdItemContratoFidic itemContratoFidic : sdContratoFidic.getItems().stream().filter(it -> it.getTitulo().getBordero() != sdContratoFidic.getBordero().getNumero()).collect(Collectors.toList())) {
                itemContratoFidic.getTitulo().setBordero(sdContratoFidic.getBordero().getNumero());
                new FluentDao().merge(itemContratoFidic.getTitulo());
            }

            List<Receber> titulos = (List<Receber>) new FluentDao().selectFrom(Receber.class).where(it -> it.equal("bordero", sdContratoFidic.getBordero().getNumero())).resultList();
            for (Receber titulo : titulos.stream().filter(it -> sdContratoFidic.getItems().stream().noneMatch(eb -> eb.getTitulo().getNumero().equals(it.getNumero()))).collect(Collectors.toList())) {
                titulo.setBordero(0);
                titulo = new FluentDao().merge(titulo);
            }
            MessageBox.create(message -> {
                message.message("Contrato atualizado com sucesso.");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
//                    SysLogger.addSysDelizLog("Contrato FIDIC", TipoAcao.CADASTRAR, String.valueOf(contratoSelcionado.get().getId()), "Contrato " + contratoSelcionado.get().getId() + " criado com sucesso!");
        }
    }

    private void limparCampos() {
        contratoSelcionado.set(null);
        textBorderoContrato.clear();
        textNumeroContratoSelecionado.clear();
        textValorEsperadoTotal.clear();
        textValorTotalCriacao.clear();
        limparTitulos();
        limparFiltrosCriacao();
        tblTitulos.clear();
        tblContratos.tableProperties().getSelectionModel().clearSelection();
    }

    private void criarNovoContrato() {
        contratoSelcionado.set(new SdContratoFidic());
    }

    private void cancelarContrato(SdContratoFidic item) {
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Tem certeza que deseja excluir esse contrato?");
            message.showAndWait();
        }).value.get())) {

            try {
                if (item != null) JPAUtils.refresh(item);

                for (SdItemContratoFidic itemContratoFidic : item.getItems().stream().filter(it -> it.getTitulo().getBordero() != 0).collect(Collectors.toList())) {
                    itemContratoFidic.getTitulo().setBordero(0);
                }

                new FluentDao().mergeAll(item.getItems().stream().map(SdItemContratoFidic::getTitulo).collect(Collectors.toList()));
//                if (item.getItems().size() > 0) new FluentDao().deleteAll(item.getItems());
                Bordero bordero = item.getBordero();

                new FluentDao().delete(item);
                if (bordero != null) {
                    JPAUtils.refresh(bordero);
                    new FluentDao().delete(bordero);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                contratosBean.remove(item);
                titulosBean.clear();
                tblContratos.refresh();

                if (contratoSelcionado.isNotNull().get() && contratoSelcionado.get().getId() == item.getId())
                    limparCampos();
                MessageBox.create(message -> {
                    message.message("Contrato excluído com sucesso!");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void enviarContrato(SdContratoFidic item) {
        item.setEnviado(true);
        item = new FluentDao().merge(item);
        MessageBox.create(message -> {
            message.message("Contrato enviado com succeso!");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }

    private void editarContrato(SdContratoFidic item) {
        if (contratoSelcionado.get() != null && !contratoSelcionado.get().equals(item)) {
            MessageBox.create(message -> {
                message.message("Outro contrato está sendo editado no momento, finalize a edição dele antes de editar outro.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }
        carregarContrato(item);
        tabs.getSelectionModel().select(1);
    }

    private void carregarContrato(SdContratoFidic item) {
        contratoSelcionado.set(item);
        titulosRecomendados.addAll(item.getItems().stream().map(SdItemContratoFidic::getTitulo).collect(Collectors.toList()));
        titulosCriacaoBean.set(FXCollections.observableArrayList(item.getItems().stream().map(SdItemContratoFidic::getTitulo).collect(Collectors.toList())));
        textNumeroContratoSelecionado.setValue(String.valueOf(item.getId()));
        textBorderoContrato.setValue(String.valueOf(item.getBordero().getNumero()));

        if (item.getItems().size() > 0) {
            multiFindSitCli.objectValues.clear();
            multiFindSitCli.objectValues.addAll(item.getItems().stream().map(it -> it.getTitulo().getEntidade().getClassecliente()).distinct().collect(Collectors.toList()));

            multFindTabSit.objectValues.clear();
            multFindTabSit.objectValues.addAll(item.getItems().stream().map(it -> it.getTitulo().getSituacao()).distinct().map(it -> ((TabSit) new FluentDao().selectFrom(TabSit.class).where(eb -> eb.equal("codigo", it)).singleResult())).collect(Collectors.toList()));

            datePeriodDataEmissao.valueBegin.set(item.getItems().stream().map(it -> it.getTitulo().getDtemissao()).distinct().min(LocalDate::compareTo).get());
            datePeriodDataEmissao.valueEnd.set(item.getItems().stream().map(it -> it.getTitulo().getDtemissao()).distinct().max(LocalDate::compareTo).get());

            datePeriodDataVencto.valueBegin.set(item.getItems().stream().map(it -> it.getTitulo().getDtvencto()).distinct().min(LocalDate::compareTo).get());
            datePeriodDataVencto.valueEnd.set(item.getItems().stream().map(it -> it.getTitulo().getDtvencto()).distinct().max(LocalDate::compareTo).get());


            rangeValorTitulo.valueBegin.set(item.getItems().stream().map(it -> it.getTitulo().getValorTitulo().intValue()).distinct().min(Integer::compareTo).get());
            rangeValorTitulo.valueEnd.set(item.getItems().stream().map(it -> it.getTitulo().getValorTitulo().intValue()).distinct().max(Integer::compareTo).get());

            segBtnManterTitulos.select(0);

        }


    }

    private void cadastrarBordero(SdContratoFidic item) {

        new Fragment().show(fg -> {

            FormFieldSingleFind<Carteira> singleFindCarteira = FormFieldSingleFind.create(Carteira.class, field -> {
                field.title("Carteira");
            });

            FormFieldSingleFind<CadConta> singleFindConta = FormFieldSingleFind.create(CadConta.class, field -> {
                field.title("Conta");
            });

            FormFieldDate formFieldDate = FormFieldDate.create(date -> {
                date.title.setText("Data");
                date.value.setValue(LocalDate.now());
            });

            singleFindConta.value.addListener((observable, oldValue, newValue) -> {
                if (newValue != null) {
                    List<Carteira> carteiras = (List<Carteira>) new FluentDao().selectFrom(Carteira.class).where(it -> it.equal("conta.conta", newValue.getConta())).resultList();
                    if (carteiras.size() == 0) return;

                    if (carteiras.size() == 1) {
                        singleFindCarteira.value.setValue(carteiras.get(0));
                    } else {
                        singleFindCarteira.defaults.add(new DefaultFilter(newValue.getConta(), "conta.conta"));
                    }
                } else {
                    singleFindConta.defaults.clear();
                }
            });

            fg.size(600.0, 100.0);

            fg.title("Criação do Bordero");
            fg.box.getChildren().add(FormBox.create(box -> {
                box.horizontal();
                box.add(singleFindConta.build(), singleFindCarteira.build(), formFieldDate.build());
            }));
            fg.buttonsBox.getChildren().add(FormButton.create(btn -> {
                btn.addStyle("success");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                btn.title("Confirmar");
                btn.setOnAction(evt -> {
                    if (singleFindCarteira.value.getValue() != null && singleFindConta.value.getValue() != null && formFieldDate.value.getValue() != null) {
                        if (item.getBordero() != null) {
                            Bordero borderoToRemove = item.getBordero();
                            item.setBordero(null);
                            new FluentDao().delete(borderoToRemove);
                        }
                        Bordero bordero = new Bordero(singleFindConta.value.getValue().getBanco().getBanco() + formFieldDate.value.getValue().format(DateTimeFormatter.ofPattern("ddMM")), singleFindConta.value.getValue(), singleFindCarteira.value.getValue(), formFieldDate.value.getValue());
                        item.setBordero(bordero);
                        fg.close();
                        try {
                            bordero = new FluentDao().persist(bordero);
                            new FluentDao().merge(item);

                            for (SdItemContratoFidic itemContratoFidic : item.getItems()) {
                                itemContratoFidic.getTitulo().setBordero(bordero.getNumero());
                                itemContratoFidic.getTitulo().setNrbanco("");
                            }
                            new FluentDao().mergeAll(item.getItems());
                        } catch (SQLException e) {
                            throw new RuntimeException(e);
                        }
                        JPAUtils.refresh(item);
                        tblContratos.refresh();
                        MessageBox.create(message -> {
                            message.message("Contrato e bordero criados com sucesso!");
                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                            message.position(Pos.TOP_RIGHT);
                            message.notification();
                        });
                    }
                });
                fg.setOnKeyReleased(evt -> {
                    if (evt.getCode().equals(KeyCode.ENTER)) {
                        btn.fire();
                    }
                });
            }));
        });
    }

    private void recomprarTitulo(SdItemContratoFidic item) {
        FormFieldComboBox<TabSit> comboBox = FormFieldComboBox.create(TabSit.class, field -> {
            field.title("Motivo");
            field.width(300);
            field.items(FXCollections.observableArrayList(new FluentDao().selectFrom(TabSit.class).where(it -> it.isIn("codigo", new Object[]{"25", "70"})).resultList()));
            field.select(0);
        });
        new Fragment().show(fg -> {
            fg.title("Informações de Recompra");
            fg.size(600.0, 100.0);
            fg.box.getChildren().add(FormBox.create(box -> {
                box.horizontal();
                box.add(comboBox.build());
            }));
            fg.buttonsBox.getChildren().add(FormButton.create(btn -> {
                btn.addStyle("success");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                btn.title("Confirmar");
                btn.setOnAction(evt -> {

                    try {
                        Receber receber = item.getTitulo();
                        Pagto pagto = new Pagto(item, item.getContrato().getBordero().getContaObj().getBanco());
                        if (comboBox.value.getValue().getCodigo().equals("70")) {
                            receber.setBanco("999");
                        } else {
                            receber.setBanco("341");
                        }

                        receber.setBordero(0);
                        receber.setSituacao(comboBox.value.getValue().getCodigo());

                        item.setRecomprado(true);
                        new FluentDao().merge(receber);
                        new FluentDao().merge(item);
                        new FluentDao().persist(pagto);
                        MessageBox.create(message -> {
                            message.message("Item recomprado com sucesso!");
                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                            message.position(Pos.TOP_RIGHT);
                            message.notification();
                        });
                        fg.close();
                        tblTitulos.refresh();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                });
            }));
        });

    }

    private void removerTitulo(SdItemContratoFidic item) {
        SdContratoFidic contrato = item.getContrato();
        item.setContrato(null);
        item.getTitulo().setBordero(0);
        item.getTitulo().setBanco("237");
        contrato.getItems().remove(item);

        item = new FluentDao().merge(item);

        titulosBean.remove(item);
        titulosCriacaoBean.remove(item.getTitulo());
        titulosAuxiliaresBean.add(item.getTitulo());

        MessageBox.create(message -> {
            message.message("Título removido com sucesso!");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }

    private void exportarExcel(SdContratoFidic item) {
        // TODO: 24/09/2022 Fazer Excel
    }

    private void limparFiltrosListagem() {
        textFilterTitulo.clear();
        textFilterNomeCliente.clear();
    }

    private void listenersFiltro() {
        titulosBeanAux.clear();
        titulosBeanAux.addAll(titulosBean);

        textFilterNomeCliente.value.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                if (oldValue != null && newValue.length() < oldValue.length()) {
                    titulosBean.clear();
                    titulosBean.addAll(titulosBeanAux);
                }
                if (newValue.length() >= 3) {
                    titulosBean.removeIf(it -> !it.getTitulo().getEntidade().getRazaosocial().contains(newValue) || !it.getTitulo().getNumero().contains(textFilterTitulo.value.getValueSafe()));
                }
            }

        });

        textFilterTitulo.value.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                if (oldValue != null && newValue.length() < oldValue.length()) {
                    titulosBean.clear();
                    titulosBean.addAll(titulosBeanAux);
                }
                if (newValue.length() >= 3) {
                    titulosBean.removeIf(it -> !it.getTitulo().getEntidade().getRazaosocial().contains(textFilterNomeCliente.value.getValueSafe()) || !it.getTitulo().getNumero().contains(newValue));
                }
            }

        });
    }
}
