package sysdeliz2.views.financeiro;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.StringConverter;
import retrofit2.HttpException;
import sysdeliz2.controllers.fxml.financeiro.ScenePagtoCartaoPedidoController;
import sysdeliz2.controllers.views.financeiro.RecebimentoComCartaoController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.sysdeliz.financeiro.SdDoctoTransacaoCartao;
import sysdeliz2.models.sysdeliz.financeiro.SdErroAbecs;
import sysdeliz2.models.sysdeliz.financeiro.SdErroApiCielo;
import sysdeliz2.models.sysdeliz.financeiro.SdTransacaoCartao;
import sysdeliz2.models.ti.Entidade;
import sysdeliz2.models.ti.Receber;
import sysdeliz2.models.view.VSdDadosPedido;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.apis.cielo.WSCielo;
import sysdeliz2.utils.apis.cielo.models.request.Sales;
import sysdeliz2.utils.apis.cielo.models.response.Cancel;
import sysdeliz2.utils.apis.cielo.models.response.Capture;
import sysdeliz2.utils.apis.cielo.models.response.ErroApi;
import sysdeliz2.utils.apis.cielo.models.response.Sale;
import sysdeliz2.utils.enums.CardType;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.exceptions.FormValidationException;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.mails.SimpleMail;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.imageio.ImageIO;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class RecebimentoComCartaoCieloView extends RecebimentoComCartaoController {

    // <editor-fold defaultstate="collapsed" desc="bean">
    private final BooleanProperty isPedidoMode = new SimpleBooleanProperty(true);
    private final BooleanProperty isBandeiraAceita = new SimpleBooleanProperty(false);
    private final BooleanProperty emTransacao = new SimpleBooleanProperty(false);
    private final ObjectProperty<CardType> bandeiraCartao = new SimpleObjectProperty<>();
    private final ListProperty<VSdDadosPedido> pedidosSelecionados = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<Receber> titulosSelecionados = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ObjectProperty<BigDecimal> valorBrutoRecebimento = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> valorDescontoRecebimento = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> valorLiquidoRecebimento = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> valorRecebimento = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> valorRecebido = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> valorParcela = new SimpleObjectProperty<>(BigDecimal.ZERO);

    private final ListProperty<SdTransacaoCartao> transacoes = new SimpleListProperty<>();
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="views">
    VBox tabRecebimento = (VBox) tabs.getTabs().get(0).getContent();
    VBox tabListagem = (VBox) tabs.getTabs().get(1).getContent();
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="tab recebimento">
    // inclusão dos doctos para pagto
    private final FormFieldText fieldNumeroPedido = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Pedido");
        field.width(210.0);
        field.editable.bind(isPedidoMode);
        field.keyReleased(evt -> {
            if (evt.getCode().equals(KeyCode.ENTER))
                adicionarPedido(field.value.get());
        });
    });
    private final FormTableView<VSdDadosPedido> tblPedidosSelecionados = FormTableView.create(VSdDadosPedido.class, table -> {
        table.title("Pedidos Selecionados");
        table.items.bind(pedidosSelecionados);
        table.height(200.0);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Numero");
                    cln.width(75.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosPedido, VSdDadosPedido>, ObservableValue<VSdDadosPedido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumero()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Numero*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cliente");
                    cln.width(250.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosPedido, VSdDadosPedido>, ObservableValue<VSdDadosPedido>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumero().getCodcli()));
                }).build() /*Cliente*/,
                FormTableColumn.create(cln -> {
                    cln.title("Valor");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosPedido, VSdDadosPedido>, ObservableValue<VSdDadosPedido>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.alignment(Pos.CENTER_RIGHT);
                    cln.format(param -> {
                        return new TableCell<VSdDadosPedido, VSdDadosPedido>() {
                            @Override
                            protected void updateItem(VSdDadosPedido item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toMonetaryFormat(item.getValorFat().add(item.getValorPend()), 2));
                                }
                            }
                        };
                    });
                }).build() /*Valor*/,
                FormTableColumn.create(cln -> {
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(50.0);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosPedido, VSdDadosPedido>, ObservableValue<VSdDadosPedido>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<VSdDadosPedido, VSdDadosPedido>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnExcluir = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                btn.tooltip("Excluir Pedido");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("danger");
                            });

                            @Override
                            protected void updateItem(VSdDadosPedido item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {

                                    btnExcluir.setOnAction(evt -> {
                                        excluirPedidoSeleciona(item);
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnExcluir);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build()
        );
    });
    private final FormTitledPane tpanePedidos = FormTitledPane.create(tpane -> {
        tpane.title("Pagamento de PEDIDOS");
        tpane.collapse(true);
        tpane.addStyle("info");
        tpane.collapsabled.addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                isPedidoMode.set(true);
                this.tpaneTitulos.collapse(false);
            }
        });
        tpane.add(FormBox.create(content -> {
            content.vertical();
            content.add(FormBox.create(boxFields -> {
                boxFields.horizontal();
                boxFields.add(fieldNumeroPedido.build());
                boxFields.add(FormButton.create(btn -> {
                    btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, 20.0));
                    btn.addStyle("success");
                    btn.setAction(evt -> adicionarPedido(fieldNumeroPedido.value.get()));
                }));
            }));
            content.add(tblPedidosSelecionados.build());
        }));
    });
    private final FormFieldText fieldNumeroTitulo = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Título");
        field.width(210.0);
        field.editable.bind(isPedidoMode.not());
        field.keyReleased(evt -> {
            if (evt.getCode().equals(KeyCode.ENTER))
                adicionarTitulo(field.value.get());
        });
    });
    private final FormTableView<Receber> tblTitulosSelecionados = FormTableView.create(Receber.class, table -> {
        table.title("Títulos Selecionados");
        table.items.bind(titulosSelecionados);
        table.height(200.0);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Numero");
                    cln.width(75.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<Receber, Receber>, ObservableValue<Receber>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumero()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Numero*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cliente");
                    cln.width(250.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<Receber, Receber>, ObservableValue<Receber>>) param -> new ReadOnlyObjectWrapper(getClienteTitulo(param.getValue().getCodcli())));
                }).build() /*Cliente*/,
                FormTableColumn.create(cln -> {
                    cln.title("Valor");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<Receber, Receber>, ObservableValue<Receber>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.alignment(Pos.CENTER_RIGHT);
                    cln.format(param -> {
                        return new TableCell<Receber, Receber>() {
                            @Override
                            protected void updateItem(Receber item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toMonetaryFormat(item.getValor2(), 2));
                                }
                            }
                        };
                    });
                }).build() /*Valor*/,
                FormTableColumn.create(cln -> {
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(50.0);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<Receber, Receber>, ObservableValue<Receber>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<Receber, Receber>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnExcluir = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                btn.tooltip("Excluir Pedido");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("danger");
                            });

                            @Override
                            protected void updateItem(Receber item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {

                                    btnExcluir.setOnAction(evt -> {
                                        excluirTituloSeleciona(item);
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnExcluir);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build()
        );
    });
    private final FormTitledPane tpaneTitulos = FormTitledPane.create(tpane -> {
        tpane.title("Pagamento de TÍTULOS");
        tpane.collapse(false);
        tpane.addStyle("info");
        tpane.collapsabled.addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                isPedidoMode.set(false);
                this.tpanePedidos.collapse(false);
            }
        });
        tpane.add(FormBox.create(content -> {
            content.vertical();
            content.add(FormBox.create(boxFields -> {
                boxFields.horizontal();
                boxFields.add(fieldNumeroTitulo.build());
                boxFields.add(FormButton.create(btn -> {
                    btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, 20.0));
                    btn.addStyle("success");
                    btn.setAction(evt -> adicionarTitulo(fieldNumeroTitulo.value.get()));
                }));
            }));
            content.add(tblTitulosSelecionados.build());
        }));
    });
    private final FormFieldText fieldClienteTelefone = FormFieldText.create(field -> {
        field.title("Telefone");
        field.editable.set(false);
    });
    private final FormFieldText fieldClienteEmail = FormFieldText.create(field -> {
        field.title("Email");
        field.editable.set(false);
        field.toLower();
        field.expanded();
    });
    private final FormFieldText fieldClienteCidade = FormFieldText.create(field -> {
        field.title("Cidade/UF");
        field.editable.set(false);
    });
    private final FormFieldTextArea fieldObservacoesRecebimento = FormFieldTextArea.create(field -> {
        field.title("Observações");
        field.expanded();
        field.editable.set(false);
        field.value.set("");
    });
    // valores e dados do pagto
    private final FormFieldText fieldPagtoValorBruto = FormFieldText.create(field -> {
        field.title("Valor Bruto");
        field.label("R$");
        field.editable.set(false);
        field.addStyle("primary").addStyle("lg");
        field.mask(FormFieldText.Mask.MONEY);
        field.expanded();
        field.value.bindBidirectional(valorBrutoRecebimento, new StringConverter<BigDecimal>() {
            @Override
            public String toString(BigDecimal object) {
                return StringUtils.toMonetaryFormat(object, 2);
            }

            @Override
            public BigDecimal fromString(String string) {
                return new BigDecimal(string.replace(",", "."));
            }
        });
    });
    private final FormFieldText fieldPagtoValorDesconto = FormFieldText.create(field -> {
        field.title("Valor Desconto");
        field.label("R$");
        field.width(160.0);
        field.editable.set(false);
        field.addStyle("warning").addStyle("lg");
        field.mask(FormFieldText.Mask.MONEY);
        field.value.bindBidirectional(valorDescontoRecebimento, new StringConverter<BigDecimal>() {
            @Override
            public String toString(BigDecimal object) {
                return StringUtils.toMonetaryFormat(object, 2);
            }

            @Override
            public BigDecimal fromString(String string) {
                return new BigDecimal(string.replace(",", "."));
            }
        });
    });
    private final FormFieldText fieldPagtoValorLiquido = FormFieldText.create(field -> {
        field.title("Valor Total");
        field.label("R$");
        field.editable.set(false);
        field.addStyle("success").addStyle("xl");
        field.mask(FormFieldText.Mask.MONEY);
        field.value.bindBidirectional(valorLiquidoRecebimento, new StringConverter<BigDecimal>() {
            @Override
            public String toString(BigDecimal object) {
                return StringUtils.toMonetaryFormat(object, 2);
            }

            @Override
            public BigDecimal fromString(String string) {
                return new BigDecimal(string.replace(",", "."));
            }
        });
    });
    private final FormFieldToggleSingle fieldPagtoIsParcial = FormFieldToggleSingle.create(field -> {
        field.title("Pagto Parcial:");
        field.value.set(false);
        field.editable.bind(pedidosSelecionados.emptyProperty().not().or(titulosSelecionados.emptyProperty().not()));
        field.value.addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                this.fieldPagtoValorParcial.requestFocus();
            } else {
                valorRecebimento.set(valorLiquidoRecebimento.get());
            }
        });
    });
    private final FormFieldText fieldPagtoValorParcial = FormFieldText.create(field -> {
        field.withoutTitle().label("Valor R$:");
        field.editable.bind(fieldPagtoIsParcial.value);
        field.width(200.0);
        field.addStyle("primary");
        field.mask(FormFieldText.Mask.MONEY);
        field.value.bindBidirectional(valorRecebimento, new StringConverter<BigDecimal>() {
            @Override
            public String toString(BigDecimal object) {
                return StringUtils.toMonetaryFormat(object, 2);
            }

            @Override
            public BigDecimal fromString(String string) {
                return new BigDecimal(string.replace(".", "").replace(",", "."));
            }
        });
        valorRecebimento.addListener((observable, oldValue, newValue) -> {
            if (newValue != null)
                valorParcela.set(valorRecebimento.get().divide(new BigDecimal(this.fieldPagtoParcelas.value.get()), 2, RoundingMode.HALF_UP));
        });
    });
    private final FormFieldText fieldValorRecebido = FormFieldText.create(field -> {
        field.withoutTitle().label("Valor Recebido R$:");
        field.editable.set(false);
        field.addStyle("success");
        field.mask(FormFieldText.Mask.MONEY);
        field.value.bindBidirectional(valorRecebido, new StringConverter<BigDecimal>() {
            @Override
            public String toString(BigDecimal object) {
                return StringUtils.toMonetaryFormat(object, 2);
            }

            @Override
            public BigDecimal fromString(String string) {
                return new BigDecimal(string.replace(".", "").replace(",", "."));
            }
        });
    });
    private final FormFieldComboBox<String> fieldPagtoParcelas = FormFieldComboBox.create(String.class, field -> {
        field.title("Parcelas");
        field.width(60.0);
        field.editable.bind(pedidosSelecionados.emptyProperty().not().or(titulosSelecionados.emptyProperty().not()));
        field.items(FXCollections.observableList(Arrays.asList("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12")));
        field.select(0);
        field.getSelectionModel((observable, oldValue, newValue) -> {
            if (newValue != null)
                valorParcela.set(valorRecebimento.get().divide(new BigDecimal((String) newValue), 2, RoundingMode.HALF_UP));
        });
    });
    private final FormFieldText fieldPagtoValorParcela = FormFieldText.create(field -> {
        field.title("Valor Parcela");
        field.label("R$");
        field.editable.set(false);
        field.width(150.0);
        field.addStyle("info");
        field.mask(FormFieldText.Mask.MONEY);
        field.value.bindBidirectional(valorParcela, new StringConverter<BigDecimal>() {
            @Override
            public String toString(BigDecimal object) {
                return StringUtils.toMonetaryFormat(object, 2);
            }

            @Override
            public BigDecimal fromString(String string) {
                return new BigDecimal(string.replace(",", "."));
            }
        });
    });
    // dados do cartão
    private final FormFieldText fieldCartaoNome = FormFieldText.create(field -> {
        field.title("Titular");
        field.withoutTitle();
        field.label("Titular");
        field.toUpper();
        field.editable.bind(isBandeiraAceita.and(pedidosSelecionados.emptyProperty().not().or(titulosSelecionados.emptyProperty().not())));
    });
    private final FormFieldText fieldCartaoCvv = FormFieldText.create(field -> {
        field.title("CVV");
        field.alignment(Pos.CENTER);
        field.toUpper();
        field.width(80.0);
        field.label(ImageUtils.getIcon(ImageUtils.Icon.CVV, 20.0));
        field.editable.bind(isBandeiraAceita.and(pedidosSelecionados.emptyProperty().not().or(titulosSelecionados.emptyProperty().not())));
    });
    private final FormFieldComboBox<String> fieldCartaoMesVencimento = FormFieldComboBox.create(String.class, field -> {
        field.title("Mês");
        field.width(60.0);
        field.editable.bind(isBandeiraAceita.and(pedidosSelecionados.emptyProperty().not().or(titulosSelecionados.emptyProperty().not())));
        field.items(FXCollections.observableArrayList("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"));
        field.select(0);
    });
    private final FormFieldComboBox<String> fieldCartaoAnoVencimento = FormFieldComboBox.create(String.class, field -> {
        field.title("Ano");
        field.width(80.0);
        field.editable.bind(isBandeiraAceita.and(pedidosSelecionados.emptyProperty().not().or(titulosSelecionados.emptyProperty().not())));
        for (int i = 0; i <= 20; i++) {
            field.items.add((LocalDate.now().getYear() + i) + "");
        }
        field.select(0);
    });
    private final FormFieldText fieldCartaoNumeroQuad1 = FormFieldText.create(field -> {
        field.withoutTitle();
        field.alignment(Pos.CENTER);
        field.width(80.0);
        field.editable.bind(pedidosSelecionados.emptyProperty().not().or(titulosSelecionados.emptyProperty().not()));
        field.addStyle("lg");
        field.mask(FormFieldText.Mask.INTEGER);
        field.keyReleased(evt -> {
            if (field.value.get() != null && field.value.get().length() == 4) {
                this.fieldCartaoNumeroQuad2.clear();
                this.fieldCartaoNumeroQuad2.requestFocus();
            }
        });
        field.value.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                exibeBandeiraCartao(newValue);
            }
        });
    });
    private final FormFieldText fieldCartaoNumeroQuad2 = FormFieldText.create(field -> {
        field.withoutTitle();
        field.alignment(Pos.CENTER);
        field.width(80.0);
        field.editable.bind(pedidosSelecionados.emptyProperty().not().or(titulosSelecionados.emptyProperty().not()));
        field.addStyle("lg");
        field.mask(FormFieldText.Mask.INTEGER);
        field.keyReleased(evt -> {
            if (field.value.get() != null && field.value.get().length() == 4) {
                this.fieldCartaoNumeroQuad3.clear();
                this.fieldCartaoNumeroQuad3.requestFocus();
            } else if (evt.getCode().equals(KeyCode.BACK_SPACE) && field.value.get().length() == 0) {
                this.fieldCartaoNumeroQuad1.requestFocus();
            }
        });
        field.value.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                exibeBandeiraCartao(this.fieldCartaoNumeroQuad1.value.get()
                        .concat(newValue));
            }
        });
    });
    private final FormFieldText fieldCartaoNumeroQuad3 = FormFieldText.create(field -> {
        field.withoutTitle();
        field.alignment(Pos.CENTER);
        field.editable.bind(pedidosSelecionados.emptyProperty().not().or(titulosSelecionados.emptyProperty().not()));
        field.addStyle("lg");
        field.width(80.0);
        field.mask(FormFieldText.Mask.INTEGER);
        field.keyReleased(evt -> {
            if (field.value.get() != null && field.value.get().length() == 4) {
                this.fieldCartaoNumeroQuad4.clear();
                this.fieldCartaoNumeroQuad4.requestFocus();
            } else if (evt.getCode().equals(KeyCode.BACK_SPACE) && field.value.get().length() == 0) {
                this.fieldCartaoNumeroQuad2.requestFocus();
            }
        });
        field.value.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                exibeBandeiraCartao(this.fieldCartaoNumeroQuad1.value.get()
                        .concat(this.fieldCartaoNumeroQuad2.value.get())
                        .concat(newValue));
            }
        });
    });
    private final FormFieldText fieldCartaoNumeroQuad4 = FormFieldText.create(field -> {
        field.withoutTitle();
        field.alignment(Pos.CENTER);
        field.editable.bind(pedidosSelecionados.emptyProperty().not().or(titulosSelecionados.emptyProperty().not()));
        field.addStyle("lg");
        field.width(90.0);
        field.mask(FormFieldText.Mask.INTEGER);
        field.keyReleased(evt -> {
            if (field.value.get() != null && field.value.get().length() == 4) {
                this.fieldCartaoNome.clear();
                this.fieldCartaoNome.requestFocus();
            } else if (evt.getCode().equals(KeyCode.BACK_SPACE) && field.value.get().length() == 0) {
                this.fieldCartaoNumeroQuad3.requestFocus();
            }
        });
        field.value.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                exibeBandeiraCartao(this.fieldCartaoNumeroQuad1.value.get()
                        .concat(this.fieldCartaoNumeroQuad2.value.get())
                        .concat(this.fieldCartaoNumeroQuad3.value.get())
                        .concat(newValue));
            }
        });
    });
    private final FormBox boxBandeiraCartao = FormBox.create(boxBandeiraCartao -> {
        boxBandeiraCartao.vertical();
        boxBandeiraCartao.expanded();
        boxBandeiraCartao.alignment(Pos.TOP_RIGHT);
    });
    // dados recibo
    private final FormBox boxRecibo = FormBox.create(boxRecibo -> {
        boxRecibo.vertical();
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="tab listagem">
    private final FormFieldMultipleFind<Entidade> fieldFilterCliente = FormFieldMultipleFind.create(Entidade.class, field -> {
        field.title("Cliente");
        field.width(120.0);
    });
    private final FormFieldDatePeriod fieldFilterData = FormFieldDatePeriod.create(field -> {
        field.title("Dt. Transação");
        field.valueBegin.set(LocalDate.of(1900, 01, 01));
        field.valueEnd.set(LocalDate.of(2050, 12, 31));
    });
    private final FormFieldText fieldFilterDocto = FormFieldText.create(field -> {
        field.title("Documento");
        field.width(150.0);
        field.toUpper();
        field.tooltip("Separe os documentos por vírgula(,)");
    });
    private final FormTableView<SdTransacaoCartao> tblTransacoes = FormTableView.create(SdTransacaoCartao.class, table -> {
        table.title("Transações Realizadas");
        table.expanded();
        table.items.bind(transacoes);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(120.0);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdTransacaoCartao, SdTransacaoCartao>, ObservableValue<SdTransacaoCartao>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdTransacaoCartao, SdTransacaoCartao>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnVisualizarDoctos = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ABRIR, ImageUtils.IconSize._16));
                                btn.tooltip("Visualizar documentos pagos");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("warning");
                            });
                            final Button btnCapturarPagamento = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._16));
                                btn.tooltip("Capturar transação de cartão");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("primary");
                            });
                            final Button btnAbrirComprovante = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._16));
                                btn.tooltip("Enviar comprovante cliente");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("success");
                            });
                            final Button btnCancelarTransacao = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                btn.tooltip("Cancelar transação de cartão");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("danger");
                            });

                            @Override
                            protected void updateItem(SdTransacaoCartao item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {

                                    btnVisualizarDoctos.setOnAction(evt -> {
                                        abrirDocumentosPagos(item);
                                    });
                                    btnCapturarPagamento.setOnAction(evt -> {
                                        capturarTransacaoManual(item);
                                    });
                                    btnAbrirComprovante.setOnAction(evt -> {
                                        enviarComprovanteCliente(item);
                                    });
                                    btnCancelarTransacao.setOnAction(evt -> {
                                        cancelarTransacao(item);
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnVisualizarDoctos);
                                    if (!item.isCapturado())
                                        boxButtonsRow.getChildren().addAll(btnCapturarPagamento);
                                    if (item.isCapturado())
                                        boxButtonsRow.getChildren().addAll(btnAbrirComprovante);
                                    if (item.isCapturado() && (item.getValor() - item.getValorcancelado()) > 0)
                                        boxButtonsRow.getChildren().addAll(btnCancelarTransacao);
                                    boxButtonsRow.setAlignment(Pos.CENTER);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Transação");
                    cln.width(120.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdTransacaoCartao, SdTransacaoCartao>, ObservableValue<SdTransacaoCartao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Transação*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cliente");
                    cln.width(350.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdTransacaoCartao, SdTransacaoCartao>, ObservableValue<SdTransacaoCartao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodcli()));
                }).build() /*Cliente*/,
                FormTableColumn.create(cln -> {
                    cln.title("Dt. Transação");
                    cln.width(110.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdTransacaoCartao, SdTransacaoCartao>, ObservableValue<SdTransacaoCartao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDttransacao()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<SdTransacaoCartao, LocalDateTime>() {
                            @Override
                            protected void updateItem(LocalDateTime item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDateTimeFormat(item));
                                }
                            }
                        };
                    });
                }).build() /*Dt. Transação*/,
                FormTableColumn.create(cln -> {
                    cln.title("Valor");
                    cln.width(120.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdTransacaoCartao, SdTransacaoCartao>, ObservableValue<SdTransacaoCartao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValor()));
                    cln.alignment(Pos.CENTER_RIGHT);
                    cln.format(param -> {
                        return new TableCell<SdTransacaoCartao, Integer>() {
                            @Override
                            protected void updateItem(Integer item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toMonetaryFormat(item / 100.0, 2));
                                }
                            }
                        };
                    });
                }).build() /*Valor*/,
                FormTableColumn.create(cln -> {
                    cln.title("Parcelas");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdTransacaoCartao, SdTransacaoCartao>, ObservableValue<SdTransacaoCartao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getParcelas()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<SdTransacaoCartao, Integer>() {
                            @Override
                            protected void updateItem(Integer item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(item == 1 ? "à vista" : item + "x");
                                }
                            }
                        };
                    });
                }).build() /*Parcelas*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cartão");
                    cln.width(120.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdTransacaoCartao, SdTransacaoCartao>, ObservableValue<SdTransacaoCartao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumerocartao()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Cartão*/,
                FormTableColumn.create(cln -> {
                    cln.title("Band.");
                    cln.width(35.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdTransacaoCartao, SdTransacaoCartao>, ObservableValue<SdTransacaoCartao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getBandeira()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<SdTransacaoCartao, String>() {
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    switch (item.toLowerCase()) {
                                        case "amex":
                                            setGraphic(ImageUtils.getIcon(ImageUtils.Icon.AMEX_CARD, 18.0));
                                            break;
                                        case "elo":
                                            setGraphic(ImageUtils.getIcon(ImageUtils.Icon.ELO_CARD, 18.0));
                                            break;
                                        case "hipercard":
                                            setGraphic(ImageUtils.getIcon(ImageUtils.Icon.HIPER_CARD, 18.0));
                                            break;
                                        case "mastercard":
                                            setGraphic(ImageUtils.getIcon(ImageUtils.Icon.MASTER_CARD, 18.0));
                                            break;
                                        case "master":
                                            setGraphic(ImageUtils.getIcon(ImageUtils.Icon.MASTER_CARD, 18.0));
                                            break;
                                        case "visa":
                                            setGraphic(ImageUtils.getIcon(ImageUtils.Icon.VISA_CARD, 18.0));
                                            break;
                                    }
                                }
                            }
                        };
                    });
                }).build() /*Band.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Titular");
                    cln.width(180.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdTransacaoCartao, SdTransacaoCartao>, ObservableValue<SdTransacaoCartao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTitular()));
                }).build() /*Titular*/,
                FormTableColumn.create(cln -> {
                    cln.title("Dt. Cancel.");
                    cln.width(110.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdTransacaoCartao, SdTransacaoCartao>, ObservableValue<SdTransacaoCartao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDtcancelamento()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<SdTransacaoCartao, LocalDateTime>() {
                            @Override
                            protected void updateItem(LocalDateTime item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDateTimeFormat(item));
                                }
                            }
                        };
                    });
                }).build() /*Dt. Cancelamento*/,
                FormTableColumn.create(cln -> {
                    cln.title("Vlr. Cancel.");
                    cln.width(100.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdTransacaoCartao, SdTransacaoCartao>, ObservableValue<SdTransacaoCartao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValorcancelado()));
                    cln.format(param -> {
                        return new TableCell<SdTransacaoCartao, Integer>() {
                            @Override
                            protected void updateItem(Integer item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toMonetaryFormat(item / 100.0, 2));
                                }
                            }
                        };
                    });
                    cln.alignment(Pos.CENTER_RIGHT);
                }).build() /*Vlr. Cancel.*/
        );
        table.factoryRow(param -> {
            return new TableRow<SdTransacaoCartao>() {
                @Override
                protected void updateItem(SdTransacaoCartao item, boolean empty) {
                    super.updateItem(item, empty);
                    getStyleClass().removeAll("table-row-warning", "table-row-danger");
                    if (item != null && !empty) {
                        if (!item.isCapturado())
                            getStyleClass().add("table-row-warning");
                        else if (item.getValor() - item.getValorcancelado() <= 0)
                            getStyleClass().add("table-row-danger");
                    }
                }
            };
        });
        table.indices(
                table.indice("Não Capturador", "warning", ""),
                table.indice("Cancelados", "danger", "")
        );
    });
    // </editor-fold>

    public RecebimentoComCartaoCieloView() {
        super("Recebimento Com Cartão (CIELO)", ImageUtils.getImage(ImageUtils.Icon.PAGTO_CARTAO),
                new String[]{"Recebimento", "Listagem"});
        init();
    }

    private void init() {
        initTabRecebimento();
        initTabListagem();
    }

    private void initTabListagem() {
        tabListagem.getChildren().add(FormBox.create(content -> {
            content.vertical();
            content.expanded();
            content.add(FormBox.create(boxFilter -> {
                boxFilter.horizontal();
                boxFilter.add(FormTitledPane.create(filter -> {
                    filter.filter();
                    filter.add(FormBox.create(contentFilter -> {
                        contentFilter.vertical();
                        contentFilter.add(FormBox.create(boxFields -> {
                            boxFields.horizontal();
                            boxFields.add(fieldFilterData.build());
                        }));
                        contentFilter.add(FormBox.create(boxFields -> {
                            boxFields.horizontal();
                            boxFields.add(fieldFilterCliente.build());
                            boxFields.add(fieldFilterDocto.build());
                        }));
                    }));
                    filter.find.setOnAction(evt -> {
                        AtomicReference<List<SdTransacaoCartao>> selectTransacoes = new AtomicReference<>();
                        new RunAsyncWithOverlay(this).exec(task -> {
                            LocalDateTime dataInicio = LocalDateTime.of(fieldFilterData.valueBegin.getValue(), LocalTime.of(0, 0, 0));
                            LocalDateTime dataFim = LocalDateTime.of(fieldFilterData.valueEnd.getValue(), LocalTime.of(23, 59, 59));
                            selectTransacoes.set(
                                    getTransacoes(dataInicio, dataFim,
                                            fieldFilterDocto.value.get(),
                                            fieldFilterCliente.objectValues.stream().map(Entidade::getCodcli).toArray()));
                            return ReturnAsync.OK.value;
                        }).addTaskEndNotification(taskReturn -> {
                            if (taskReturn.equals(ReturnAsync.OK.value)) {
                                transacoes.set(FXCollections.observableList(selectTransacoes.get()));
                                tblTransacoes.refresh();
                            }
                        });
                    });
                    filter.clean.setOnAction(evt -> {
                        fieldFilterData.valueBegin.set(LocalDate.of(1900, 01, 01));
                        fieldFilterData.valueEnd.set(LocalDate.of(2050, 12, 31));
                        fieldFilterCliente.clear();
                        fieldFilterDocto.clear();
                        transacoes.clear();
                    });
                }));
            }));
            content.add(tblTransacoes.build());
        }));
    }

    private void initTabRecebimento() {
        tabRecebimento.getChildren().add(FormBox.create(content -> {
            content.horizontal();
            content.expanded();
            content.add(FormBox.create(container -> {
                container.vertical();
                container.add(tpanePedidos);
                container.add(tpaneTitulos);
                container.add(FormBox.create(boxDadosCliente -> {
                    boxDadosCliente.vertical();
                    boxDadosCliente.title("Dados do Cliente");
                    boxDadosCliente.expanded();
                    boxDadosCliente.add(FormBox.create(boxFields -> {
                        boxFields.horizontal();
                        boxFields.add(fieldClienteTelefone.build());
                        boxFields.add(fieldClienteEmail.build());
                    }));
                    boxDadosCliente.add(fieldClienteCidade.build());
                    boxDadosCliente.add(fieldObservacoesRecebimento.build());
                }));
            }));
            content.add(FormBox.create(container -> {
                container.vertical();
                container.add(FormBox.create(boxContainer -> {
                    boxContainer.horizontal();
                    boxContainer.add(FormBox.create(boxDadosPagto -> {
                        boxDadosPagto.vertical();
                        boxDadosPagto.title("Valores Recebimento");
                        boxDadosPagto.add(FormBox.create(boxFields -> {
                            boxFields.horizontal();
                            boxFields.add(fieldPagtoValorBruto.build(), fieldPagtoValorDesconto.build());
                        }));
                        boxDadosPagto.add(fieldValorRecebido.build());
                        boxDadosPagto.add(fieldPagtoValorLiquido.build());
                        boxDadosPagto.add(FormBox.create(boxFields -> {
                            boxFields.horizontal();
                            boxFields.add(fieldPagtoIsParcial.build(), fieldPagtoValorParcial.build());
                        }));
                        boxDadosPagto.add(FormBox.create(boxFields -> {
                            boxFields.horizontal();
                            boxFields.alignment(Pos.CENTER_RIGHT);
                            boxFields.add(fieldPagtoParcelas.build(), fieldPagtoValorParcela.build());
                        }));
                    }));
                }));
                container.add(FormBox.create(boxDadosCartao -> {
                    boxDadosCartao.vertical();
                    boxDadosCartao.title("Dados do Cartão");
                    boxDadosCartao.add(FormBox.create(boxHeadCartao -> {
                        boxHeadCartao.horizontal();
                        boxHeadCartao.alignment(Pos.TOP_LEFT);
                        boxHeadCartao.add(ImageUtils.getIcon(ImageUtils.Icon.CIELO, ImageUtils.IconSize._80));
                        boxHeadCartao.add(boxBandeiraCartao);
                    }));
                    boxDadosCartao.add(FormBox.create(boxCartao -> {
                        boxCartao.horizontal();
                        boxCartao.add(fieldCartaoNumeroQuad1.build());
                        boxCartao.add(fieldCartaoNumeroQuad2.build());
                        boxCartao.add(fieldCartaoNumeroQuad3.build());
                        boxCartao.add(fieldCartaoNumeroQuad4.build());
                    }));
                    boxDadosCartao.add(fieldCartaoNome.build());
                    boxDadosCartao.add(FormBox.create(boxFooCartao -> {
                        boxFooCartao.horizontal();
                        boxFooCartao.add(FormBox.create(boxFields -> {
                            boxFields.horizontal();
                            boxFields.expanded();
                            boxFields.add(fieldCartaoMesVencimento.build(), fieldCartaoAnoVencimento.build());
                        }));
                        boxFooCartao.add(fieldCartaoCvv.build());
                    }));
                }));
                container.add(FormBox.create(toolbar -> {
                    toolbar.horizontal();
                    toolbar.add(FormButton.create(btn -> {
                        btn.title("PAGAR");
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.PAGAMENTO_CARTAO, ImageUtils.IconSize._48));
                        btn.contentDisplay(ContentDisplay.TOP);
                        btn.addStyle("success");
                        btn.setAction(evt -> confirmarPagamento());
                        btn.disable.bind(isBandeiraAceita.not()
                                .or(pedidosSelecionados.emptyProperty().and(titulosSelecionados.emptyProperty()))
                                .or(emTransacao));
                    }));
                    toolbar.add(FormButton.create(btn -> {
                        btn.title("Cancelar");
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                        btn.addStyle("danger");
                        btn.setAction(evt -> cancelarPagamento());
                        btn.disable.bind(isBandeiraAceita.not()
                                .or(pedidosSelecionados.emptyProperty().and(titulosSelecionados.emptyProperty()))
                                .or(emTransacao));
                    }));
                }));
            }));
            content.add(boxRecibo);
        }));
    }

    private void exibeBandeiraCartao(String numeroCartao) {
        CardType typeCard = CardType.detect(numeroCartao);
        bandeiraCartao.set(typeCard);
        boxBandeiraCartao.clear();
        isBandeiraAceita.set(false);

        switch (typeCard) {
            case UNKNOWN:
                boxBandeiraCartao.add(ImageUtils.getIcon(ImageUtils.Icon.CARTAO_INVALIDO, 70.0));
                isBandeiraAceita.set(false);
                break;
            case amex:
                boxBandeiraCartao.add(ImageUtils.getIcon(ImageUtils.Icon.AMEX_CARD, ImageUtils.IconSize._80));
                isBandeiraAceita.set(true);
                break;
            case elo:
                boxBandeiraCartao.add(ImageUtils.getIcon(ImageUtils.Icon.ELO_CARD, ImageUtils.IconSize._80));
                isBandeiraAceita.set(true);
                break;
            case hipercard:
                boxBandeiraCartao.add(ImageUtils.getIcon(ImageUtils.Icon.HIPER_CARD, ImageUtils.IconSize._80));
                isBandeiraAceita.set(true);
                break;
            case mastercard:
                boxBandeiraCartao.add(ImageUtils.getIcon(ImageUtils.Icon.MASTER_CARD, ImageUtils.IconSize._80));
                isBandeiraAceita.set(true);
                break;
            case visa:
                boxBandeiraCartao.add(ImageUtils.getIcon(ImageUtils.Icon.VISA_CARD, ImageUtils.IconSize._80));
                isBandeiraAceita.set(true);
                break;
        }
    }

    private void somatoriosPedido() {
        valorRecebido.set(new BigDecimal(pedidosSelecionados.stream()
                .mapToDouble(pedido -> pedido.getValorRecebido().doubleValue())
                .sum()));

        valorBrutoRecebimento.set(new BigDecimal(pedidosSelecionados.stream()
                .mapToDouble(pedido -> pedido.getValorFat().add(pedido.getValorPend()).add(pedido.getValorDesc()).doubleValue())
                .sum()));
        valorDescontoRecebimento.set(new BigDecimal(pedidosSelecionados.stream()
                .mapToDouble(pedido -> pedido.getValorDesc().doubleValue())
                .sum()));
        valorLiquidoRecebimento.set(new BigDecimal(pedidosSelecionados.stream()
                .mapToDouble(pedido -> pedido.getValorFat().add(pedido.getValorPend()).doubleValue())
                .sum()).subtract(valorRecebido.get()));

        valorRecebimento.set(valorLiquidoRecebimento.get());
    }

    private void adicionarPedido(String numero) {
        if (numero != null && pedidosSelecionados.stream().noneMatch(pedido -> pedido.getNumero().getNumero().equals(numero))) {
            VSdDadosPedido dadosPedido = new FluentDao().selectFrom(VSdDadosPedido.class)
                    .where(eb -> eb.equal("numero.numero", numero))
                    .singleResult();
            if (dadosPedido != null) {
                boxRecibo.clear();
                pedidosSelecionados.add(dadosPedido);

                List<SdDoctoTransacaoCartao> doctos = (List<SdDoctoTransacaoCartao>) new FluentDao().selectFrom(SdDoctoTransacaoCartao.class)
                        .where(eb -> eb.equal("id.docto", dadosPedido.getNumero().getNumero()))
                        .resultList();
                dadosPedido.setValorRecebido(new BigDecimal(doctos.stream().mapToDouble(doc -> doc.getValor().doubleValue()).sum()));
                somatoriosPedido();

                carregaDadosCliente(pedidosSelecionados.get(0).getNumero().getCodcli());
            }
        }
        fieldNumeroPedido.clear();
        fieldNumeroPedido.requestFocus();
    }

    private void excluirPedidoSeleciona(VSdDadosPedido numero) {
        pedidosSelecionados.remove(numero);
        somatoriosPedido();

        if (pedidosSelecionados.size() > 0)
            carregaDadosCliente(pedidosSelecionados.get(0).getNumero().getCodcli());
        else
            clearDadosCliente();

    }

    private void somatoriosTitulo() {
        valorRecebido.set(new BigDecimal(titulosSelecionados.stream()
                .mapToDouble(titulo -> titulo.getValorRecebido().add(titulo.getValorpago()).doubleValue())
                .sum()));

        valorBrutoRecebimento.set(new BigDecimal(titulosSelecionados.stream()
                .mapToDouble(titulo -> titulo.getValor2().doubleValue())
                .sum()));
        valorDescontoRecebimento.set(BigDecimal.ZERO);
        valorLiquidoRecebimento.set(valorBrutoRecebimento.get().subtract(valorRecebido.get()));

        valorRecebimento.set(valorLiquidoRecebimento.get());
    }

    private void adicionarTitulo(String numero) {
        if (numero != null && titulosSelecionados.stream().noneMatch(titulo -> titulo.getNumero().equals(numero))) {
            Receber dadosTitulo = new FluentDao().selectFrom(Receber.class)
                    .where(eb -> eb.equal("numero", numero))
                    .singleResult();
            if (dadosTitulo != null) {
                boxRecibo.clear();
                titulosSelecionados.add(dadosTitulo);

                List<SdDoctoTransacaoCartao> doctos = (List<SdDoctoTransacaoCartao>) new FluentDao().selectFrom(SdDoctoTransacaoCartao.class)
                        .where(eb -> eb.equal("id.docto", dadosTitulo.getNumero()))
                        .resultList();
                dadosTitulo.setValorRecebido(new BigDecimal(doctos.stream().mapToDouble(doc -> doc.getValor().doubleValue()).sum()));
                somatoriosTitulo();

                carregaDadosCliente(getClienteTitulo(titulosSelecionados.get(0).getCodcli()));
            }
        }
        fieldNumeroTitulo.clear();
        fieldNumeroTitulo.requestFocus();
    }

    private void excluirTituloSeleciona(Receber numero) {
        titulosSelecionados.remove(numero);
        somatoriosTitulo();

        if (pedidosSelecionados.size() > 0)
            carregaDadosCliente(getClienteTitulo(titulosSelecionados.get(0).getCodcli()));
        else
            clearDadosCliente();

    }

    private Entidade getClienteTitulo(String codcli) {
        return new FluentDao().selectFrom(Entidade.class)
                .where(eb -> eb.equal("codcli", codcli))
                .singleResult();
    }

    private void carregaDadosCliente(Entidade cliente) {
        fieldClienteTelefone.setValue(cliente.getTelefone());
        fieldClienteEmail.setValue(cliente.getEmail());
        if (cliente.getCep() == null || cliente.getCep().getCidade() == null) {
            fieldClienteCidade.setValue("Cadastro de CEP/CIDADE ausente.");
        } else {
            fieldClienteCidade.setValue(cliente.getCep().getCidade().getNomeCid() + "/" +
                    cliente.getCep().getCidade().getCodEst().getId().getSiglaEst());
        }

        fieldObservacoesRecebimento.clear();
        pedidosSelecionados.forEach(dadosPedido -> {
            fieldObservacoesRecebimento.value.set((fieldObservacoesRecebimento.value.get() == null ? "" : fieldObservacoesRecebimento.value.get())
                    + "Pagto pedido " + dadosPedido.getNumero().getNumero() + ": " + StringUtils.toMonetaryFormat(dadosPedido.getValorRecebido(), 2)
                    + (dadosPedido.getNumero().getObs() == null ? "" : "\nObs: " + dadosPedido.getNumero().getObs())
                    + "\n");
        });
        titulosSelecionados.forEach(dadosTitulo -> {
            fieldObservacoesRecebimento.value.set((fieldObservacoesRecebimento.value.get() == null ? "" : fieldObservacoesRecebimento.value.get())
                    + "Pagto pedido " + dadosTitulo.getNumero() + ": " + StringUtils.toMonetaryFormat(dadosTitulo.getValorRecebido().add(dadosTitulo.getValorpago()), 2)
                    + (dadosTitulo.getObs() == null ? "" : "\nObs: " + dadosTitulo.getObs())
                    + "\n");
        });
    }

    private void confirmarPagamento() {
        if (valorRecebimento.get().compareTo(BigDecimal.ONE) <= 0) {
            MessageBox.create(message -> {
                message.message("O valor para recebimento não pode ser ZERO.");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showAndWait();
            });
            return;
        }
        try {
            fieldCartaoNome.validate();
            fieldCartaoCvv.validate();
            emTransacao.set(true);

            // O merchantOrder será formado pelo prefixo P (Pedido) ou T (Título) mais um lpad de 15 caracteres com o ID da transação
            String merchantOrder = (pedidosSelecionados.size() > 0 ? "P" : "T") +
                    Globals.getProximoCodigo("SD_TR_CARTAO", "TRANSACAO");
            Sales recebimento = new Sales(merchantOrder)
                    .addPayment(valorRecebimento.get().multiply(new BigDecimal(100)).intValue(),
                            new Integer(fieldPagtoParcelas.value.get()),
                            fieldCartaoNumeroQuad1.value.get()
                                    .concat(fieldCartaoNumeroQuad2.value.get())
                                    .concat(fieldCartaoNumeroQuad3.value.get())
                                    .concat(fieldCartaoNumeroQuad4.value.get()),
                            fieldCartaoNome.value.get(),
                            fieldCartaoMesVencimento.value.get()
                                    .concat("/")
                                    .concat(fieldCartaoAnoVencimento.value.get()),
                            fieldCartaoCvv.value.get(),
                            bandeiraCartao.get().name().replace("mastercard", "master"));
            Sale recibo = new WSCielo().sale(recebimento);
            SysLogger.addSysDelizLog("Receber Cartão CIELO", TipoAcao.CADASTRAR, recebimento.getMerchantOrderId(),
                    "Retorno CIELO da tentativa de transação: " + recibo.toJson().length());

            if (recibo.getPayment().getStatus() == 1 || recibo.getPayment().getStatus() == 2) {

                SdTransacaoCartao transacao = new SdTransacaoCartao();
                transacao.setCodigo(merchantOrder);
                transacao.setCapturado(recibo.getPayment().isCapture());
                transacao.setTid(recibo.getPayment().getTid());
                transacao.setProofofsale(recibo.getPayment().getProofOfSale());
                transacao.setAuthorizationcode(recibo.getPayment().getAuthorizationCode());
                transacao.setPaymentid(recibo.getPayment().getPaymentId());
                transacao.setValor(recibo.getPayment().getAmount());
                transacao.setParcelas(recibo.getPayment().getInstallments());
                transacao.setDttransacao(LocalDateTime.now());
                transacao.setNumerocartao(recibo.getPayment().getCreditCard().getCardNumber());
                transacao.setValidade(recibo.getPayment().getCreditCard().getExpirationDate());
                transacao.setBandeira(recibo.getPayment().getCreditCard().getBrand());
                transacao.setTitular(recibo.getPayment().getCreditCard().getHolder());
                transacao.setCodcli(isPedidoMode.get() ? pedidosSelecionados.get(0).getNumero().getCodcli()
                        : getClienteTitulo(titulosSelecionados.get(0).getCodcli()));
                final BigDecimal[] valorParaAbatimento = {valorRecebimento.get()};
                pedidosSelecionados.forEach(pedido -> {
                    if (pedido.getValorFat().add(pedido.getValorPend()).compareTo(valorParaAbatimento[0]) > 0) {
                        transacao.getDoctos().add(new SdDoctoTransacaoCartao(transacao, pedido.getNumero().getNumero(), valorParaAbatimento[0]));
                        valorParaAbatimento[0] = valorParaAbatimento[0].subtract(valorParaAbatimento[0]);
                    } else {
                        transacao.getDoctos().add(new SdDoctoTransacaoCartao(transacao, pedido.getNumero().getNumero(), pedido.getValorFat().add(pedido.getValorPend())));
                        valorParaAbatimento[0] = valorParaAbatimento[0].subtract(pedido.getValorFat().add(pedido.getValorPend()));
                    }
                });
                titulosSelecionados.forEach(titulo -> {
                    if (titulo.getValor2().compareTo(valorParaAbatimento[0]) > 0) {
                        transacao.getDoctos().add(new SdDoctoTransacaoCartao(transacao, titulo.getNumero(), valorParaAbatimento[0]));
                        valorParaAbatimento[0] = valorParaAbatimento[0].subtract(valorParaAbatimento[0]);
                    } else {
                        transacao.getDoctos().add(new SdDoctoTransacaoCartao(transacao, titulo.getNumero(), titulo.getValor2()));
                        valorParaAbatimento[0] = valorParaAbatimento[0].subtract(titulo.getValor2());
                    }
                });
                sendTransacaoEmail(transacao);
                new FluentDao().persist(transacao);

                SysLogger.addSysDelizLog("Receber Cartão CIELO", TipoAcao.CADASTRAR, transacao.getCodigo(),
                        "Criado transção de cartão para o cliente " + transacao.getCodcli().toString() + " no valor de " + StringUtils.toMonetaryFormat(transacao.getValor() / 100.0, 2)
                                + " referente aos documentos " + transacao.getDoctos().stream().map(docto -> docto.getId().getDocto()).collect(Collectors.joining(", ")));

                criarAntecipacao(transacao.getValor() / 100.0,
                        transacao.getValor() / 100.0,
                        transacao.getCodcli().getStringCodcli(),
                        "CARTÃO: " + transacao.getNumerocartao() + " - DATA/HORA: " + StringUtils.toDateTimeFormat(transacao.getDttransacao()),
                        transacao.getCodigo());

                SysLogger.addSysDelizLog("Receber Cartão CIELO", TipoAcao.CADASTRAR, transacao.getCodigo(),
                        "Criado antecipação para o pagamento com cartão.");

                boxRecibo.add(FormBox.create(boxCarregaRecibo -> {
                    boxCarregaRecibo.vertical();
                    FormBox comprovante = FormBox.create(boxViewRecibo -> {
                        boxViewRecibo.vertical();
                        boxViewRecibo.add(exibirComprovante(transacao));
                    });
                    boxCarregaRecibo.add(comprovante);
                    boxCarregaRecibo.add(FormBox.create(toolbar -> {
                        toolbar.horizontal();
                        toolbar.add(FormButton.create(btn -> {
                            btn.title("Enviar");
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._16));
                            btn.addStyle("success");
                            btn.setAction(evt -> sendReciboEmail(transacao, comprovante));
                        }));
                    }));
                }));
                MessageBox.create(message -> {
                    message.message("Transação realizada com sucesso.");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
                cancelarPagamento();

//                Incluído captura no envio da transação
//                Capture reciboCaptura = new WSCielo().capture(recibo.getPayment().getPaymentId());
//                if (reciboCaptura.getStatus() == 2) {
//                    transacao.setCapturado(true);
//                    new FluentDao().merge(transacao);
//
//                    boxRecibo.add(FormBox.create(boxCarregaRecibo -> {
//                        boxCarregaRecibo.vertical();
//                        FormBox comprovante = FormBox.create(boxViewRecibo -> {
//                            boxViewRecibo.vertical();
//                            boxViewRecibo.add(exibirComprovante(transacao));
//                        });
//                        boxCarregaRecibo.add(comprovante);
//                        boxCarregaRecibo.add(FormBox.create(toolbar -> {
//                            toolbar.horizontal();
//                            toolbar.add(FormButton.create(btn -> {
//                                btn.title("Enviar");
//                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._16));
//                                btn.addStyle("success");
//                                btn.setAction(evt -> sendReciboEmail(transacao, comprovante));
//                            }));
//                        }));
//                    }));
//                    MessageBox.create(message -> {
//                        message.message("Transação realizada com sucesso.");
//                        message.type(MessageBox.TypeMessageBox.CONFIRM);
//                        message.position(Pos.TOP_RIGHT);
//                        message.notification();
//                    });
//                    cancelarPagamento();
//
//                } else {
//                    MessageBox.create(message -> {
//                        message.message("Não foi possível capturar a transação do cartão.\n" +
//                                "Erro: " + reciboCaptura.getReturnCode() + " Mensagem: " + reciboCaptura.getReturnMessage());
//                        message.type(MessageBox.TypeMessageBox.ALERT);
//                        message.showAndWait();
//                    });
//                }

            } else {
                List<SdErroAbecs> erroTransacao = (List<SdErroAbecs>) new FluentDao().selectFrom(SdErroAbecs.class)
                        .where(eb -> eb
                                .equal("codigoAbecs", recibo.getPayment().getReturnCorde())
                                .and(or -> or.equal("bandeira", recibo.getPayment().getCreditCard().getBrand()
                                                .toUpperCase().replace("MASTER", "MASTERCARD"))
                                        .equal("bandeira", "ALL", TipoExpressao.OR, true)))
                        .orderBy("bandeira", OrderType.DESC)
                        .resultList();

                if (erroTransacao.size() == 0) {
                    MessageBox.create(message -> {
                        message.message("Ocorreu um erro durante a transação do cartão. Erro: " + recibo.getPayment().getReturnCorde() +
                                " Descrição: " + recibo.getPayment().getReturnMessage());
                        message.type(MessageBox.TypeMessageBox.ERROR);
                        message.showAndWait();
                    });
                } else {
                    MessageBox.create(message -> {
                        message.message("Ocorreu um erro durante a transação do cartão.\n" +
                                "Erro: " + erroTransacao.get(0).getCodigoAbecs() + " Mensagem: " + erroTransacao.get(0).getDefinicao() + "\n" +
                                "Ação: " + erroTransacao.get(0).getAcao());
                        message.type(MessageBox.TypeMessageBox.ERROR);
                        message.showAndWait();
                    });
                }
            }

        } catch (FormValidationException e) {
            MessageBox.create(message -> {
                message.message("Existem campos obrigatórios não preenchidos:\n" + e.getMessage());
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
        } catch (IOException e) {
            e.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(e);
                message.showAndWait();
            });
        } catch (HttpException e) {
            try {
                ErroApi[] errosApi = new Gson().fromJson(e.response().errorBody().string(), new TypeToken<ErroApi[]>() {
                }.getType());
                SdErroApiCielo erroCielo = new FluentDao().selectFrom(SdErroApiCielo.class)
                        .where(eb -> eb.equal("codigo", errosApi[0].getCode()))
                        .singleResult();
                if (erroCielo != null) {
                    MessageBox.create(message -> {
                        message.message("Ocorreu um erro durante a transação do cartão.\n" +
                                "Erro: " + erroCielo.getCodigo() + " Mensagem: " + erroCielo.getMensagem() + "\n" +
                                "Descrição: " + erroCielo.getDescricao());
                        message.type(MessageBox.TypeMessageBox.ERROR);
                        message.showAndWait();
                    });
                } else {
                    MessageBox.create(message -> {
                        message.message("Ocorreu um erro durante a transação do cartão. Erro: " + errosApi[0].getCode() +
                                " Descrição: " + errosApi[0].getMessage());
                        message.type(MessageBox.TypeMessageBox.ERROR);
                        message.showAndWait();
                    });
                }
            } catch (IOException ex) {
                ex.printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(ex);
                    message.showAndWait();
                });
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(throwables);
                message.showAndWait();
            });
        }
    }

    private void sendReciboEmail(SdTransacaoCartao transacao, FormBox comprovante) {
        WritableImage image = comprovante.snapshot(new SnapshotParameters(), null);
        File file = new File("c:/SysDelizLocal/local_files/comp_pag.png");
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);

            SimpleMail.INSTANCE
                    .addDestinatario(transacao.getCodcli().getEmail())
                    .addCC(Globals.getUsuarioLogado().getEmail())
                    .addCCO("diego@deliz.com.br")
                    .addReplyTo(Globals.getUsuarioLogado().getEmail())
                    .comAssunto("Comprovante de pagamento Deliz Fashion")
                    .comMultipartBody(mimeMultipart -> {
                        try {
                            BodyPart messageBodyPartHtml = new MimeBodyPart();
                            BodyPart imageBodyPart = new MimeBodyPart();

                            // Primeira parte - o conteúdo
                            String htmlText = "<!DOCTYPE html>"
                                    + "<html lang=\"pt-br\">"
                                    + "	<head>"
                                    + "		<title>Comprovante de pagamento Deliz Fashion</title>"
                                    + "		<meta charset=\"utf-8\">"
                                    + "		<style type=\"text/css\">"
                                    + "			body {"
                                    + "				color: #444;"
                                    + "				font: 100%/25px 'Helvetica Neue', helvetica, arial, sans-serif;"
                                    + "			}"
                                    + "			table {"
                                    + "				background: #f5f5f5;"
                                    + "				border-collapse: separate;"
                                    + "				box-shadow: inset 0 1px 0 #fff;"
                                    + "				font-size: 12px;"
                                    + "				line-height: 14px;"
                                    + "				text-align: left;"
                                    + "			}"
                                    + "			th {"
                                    + "				background-color: #777;"
                                    + "				border-left: 1px solid #555;"
                                    + "				border-right: 1px solid #777;"
                                    + "				border-top: 1px solid #555;"
                                    + "				border-bottom: 1px solid #333;"
                                    + "				box-shadow: inset 0 1px 0 #999;"
                                    + "				color: #fff;"
                                    + "			  	font-weight: bold;"
                                    + "				padding: 5px 10px;"
                                    + "				position: relative;"
                                    + "				text-shadow: 0 1px 0 #000;	"
                                    + "			}"
                                    + "			td {"
                                    + "				border-right: 1px solid #fff;"
                                    + "				border-left: 1px solid #e8e8e8;"
                                    + "				border-top: 1px solid #fff;"
                                    + "				border-bottom: 1px solid #e8e8e8;"
                                    + "				padding: 5px 10px;"
                                    + "				position: relative;"
                                    + "				transition: all 300ms;"
                                    + "				text-transform: uppercase;"
                                    + "			}"
                                    + "			span {"
                                    + "				font: 90%/10px 'Helvetica Neue', helvetica, arial, sans-serif;"
                                    + "			}"
                                    + "		</style>"
                                    + "	</head>"
                                    + "	<body>"
                                    + "		<h3>Prezado cliente,</h3>"
                                    + "		Informamos que foi efetuado o pagamento com um cartão de crédito: "
                                    + "		<br/>"
                                    + "		<table>"
                                    + "			<thead>"
                                    + "				<tr>"
                                    + "					<th>TITULAR</th>"
                                    + "					<th>DATA</th>"
                                    + "					<th>PEDIDO</th>"
                                    + "					<th>VALOR</th>"
                                    + "					<th>PARCELA</th>"
                                    + "					<th>CARTÃO</th>"
                                    + "					<th>BANDEIRA</th>"
                                    + "				</tr>"
                                    + "			</thead>"
                                    + "			<tbody>"
                                    + "				<tr>"
                                    + "					<td>" + transacao.getTitular() + "</td>"
                                    + "					<td>" + StringUtils.toDateTimeFormat(transacao.getDttransacao()) + "</td>"
                                    + "					<td>" + transacao.getDoctos().stream().map(docto -> docto.getId().getDocto()).collect(Collectors.joining(",")) + "</td>"
                                    + "					<td>" + StringUtils.toMonetaryFormat(transacao.getValor() / 100.0, 2) + "</td>"
                                    + "					<td>" + (transacao.getParcelas() == 1 ? "à vista" : transacao.getParcelas() + "x") + "</td>"
                                    + "					<td>" + transacao.getNumerocartao() + "</td>"
                                    + "					<td>" + transacao.getBandeira() + "</td>"
                                    + "				</tr>"
                                    + "			</tbody>"
                                    + "		</table>"
                                    + "		<br/>"
                                    + "		<br/>"
                                    + "		<img src=\"cid:imagecomprovante\" />"
                                    + "		<br/>"
                                    + "		<br/>"
                                    + "		Em caso de contestação deste pagamento, entre em contato com o setor comercial e informe o seu TITULAR e o número do pedido "
                                    + "		pelo telefone/whatsapp: <b>(048) 3641-1900</b><br/>"
                                    + "		<br/>"
                                    + "		Para qualquer informação a Deliz está à disposição nos seguintes contatos:<br/>"
                                    + "		<span>"
                                    + "			<b>" + Globals.getUsuarioLogado().getDisplayName() + "</b><br/>"
                                    + "			<b>Telefone/Whatsapp:</b> (048) 3641-1900<br/>"
                                    + "			<b>Email:</b> " + Globals.getUsuarioLogado().getEmail() + "<br/>"
                                    + "		</span>"
                                    + "	</body>"
                                    + "</html>" +
                                    "" +
                                    "" +
                                    "" +
                                    "<html>";
                            messageBodyPartHtml.setContent(htmlText, "text/html");
                            messageBodyPartHtml.setHeader("charset", "utf-8");

                            MimeBodyPart imagePart = new MimeBodyPart();
                            imageBodyPart.setDataHandler(new DataHandler(new FileDataSource(new File("c:/SysDelizLocal/local_files/comp_pag.png"))));
                            imageBodyPart.setHeader("Content-ID", "<imagecomprovante>");
                            imageBodyPart.setDisposition(MimeBodyPart.INLINE);

                            mimeMultipart.addBodyPart(messageBodyPartHtml);
                            mimeMultipart.addBodyPart(imageBodyPart);
                        } catch (MessagingException | NullPointerException e) {
                            e.printStackTrace();
                            ExceptionBox.build(message -> {
                                message.exception(e);
                                message.showAndWait();
                            });
                        }
                        return mimeMultipart;
                    })
                    .send();
            SysLogger.addSysDelizLog("Receber Cartão CIELO", TipoAcao.CADASTRAR, transacao.getCodigo(),
                    "Enviado recibo de transação de cartão para o cliente.");

            MessageBox.create(message -> {
                message.message("Recibo enviado para o cliente.");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            boxRecibo.clear();
        } catch (IOException ex) {
            Logger.getLogger(ScenePagtoCartaoPedidoController.class.getName()).log(Level.SEVERE, null, ex);
            GUIUtils.showException(ex);
        }
    }

    private void sendTransacaoEmail(SdTransacaoCartao transacao) {
        SimpleMail.INSTANCE
                .addDestinatario("diego@deliz.com.br")
                .comAssunto("Transação com Cartão - CIELO (" + transacao.getCodigo() + ")")
                .comCorpoHtml(() -> {
                    String html = "<!DOCTYPE html>"
                            + "<html lang=\"pt-br\">"
                            + "	<head>"
                            + "		<title>Transação com Cartão - CIELO (" + transacao.getCodigo() + ")</title>"
                            + "		<meta charset=\"utf-8\">"
                            + "		<style type=\"text/css\">"
                            + "			body {"
                            + "				color: #444;"
                            + "				font: 100%/25px 'Helvetica Neue', helvetica, arial, sans-serif;"
                            + "			}"
                            + "			table {"
                            + "				background: #f5f5f5;"
                            + "				border-collapse: separate;"
                            + "				box-shadow: inset 0 1px 0 #fff;"
                            + "				font-size: 12px;"
                            + "				line-height: 14px;"
                            + "				text-align: left;"
                            + "			}"
                            + "			th {"
                            + "				background-color: #777;"
                            + "				border-left: 1px solid #555;"
                            + "				border-right: 1px solid #777;"
                            + "				border-top: 1px solid #555;"
                            + "				border-bottom: 1px solid #333;"
                            + "				box-shadow: inset 0 1px 0 #999;"
                            + "				color: #fff;"
                            + "			  	font-weight: bold;"
                            + "				padding: 5px 10px;"
                            + "				position: relative;"
                            + "				text-shadow: 0 1px 0 #000;	"
                            + "			}"
                            + "			td {"
                            + "				border-right: 1px solid #fff;"
                            + "				border-left: 1px solid #e8e8e8;"
                            + "				border-top: 1px solid #fff;"
                            + "				border-bottom: 1px solid #e8e8e8;"
                            + "				padding: 5px 10px;"
                            + "				position: relative;"
                            + "				transition: all 300ms;"
                            + "				text-transform: uppercase;"
                            + "			}"
                            + "			span {"
                            + "				font: 90%/10px 'Helvetica Neue', helvetica, arial, sans-serif;"
                            + "			}"
                            + "		</style>"
                            + "	</head>"
                            + "	<body>"
                            + "	    <h2>Transação</h2>"
                            + "		<table>"
                            + "			<thead>"
                            + "				<tr>"
                            + "					<th>CODIGO</th>"
                            + "					<th>CAPTURADO</th>"
                            + "					<th>TID</th>"
                            + "					<th>PROOFOFSALE</th>"
                            + "					<th>AUTHORIZATIONCODE</th>"
                            + "					<th>PAYMENTID</th>"
                            + "					<th>VALOR</th>"
                            + "					<th>PARCELAS</th>"
                            + "					<th>DT TRANSAÇÃO</th>"
                            + "					<th>NUMERO CARTÃO</th>"
                            + "					<th>VALIDADE</th>"
                            + "					<th>BANDEIRA</th>"
                            + "					<th>TITULAR</th>"
                            + "					<th>CLIENTE</th>"
                            + "				</tr>"
                            + "			</thead>"
                            + "			<tbody>"
                            + "				<tr>"
                            + "					<td>" + transacao.getCodigo() + "</td>"
                            + "					<td>" + transacao.isCapturado() + "</td>"
                            + "					<td>" + transacao.getTid() + "</td>"
                            + "					<td>" + transacao.getProofofsale() + "</td>"
                            + "					<td>" + transacao.getAuthorizationcode() + "</td>"
                            + "					<td>" + transacao.getPaymentid() + "</td>"
                            + "					<td>" + transacao.getValor() + "</td>"
                            + "					<td>" + transacao.getParcelas() + "</td>"
                            + "					<td>" + StringUtils.toDateTimeFormat(transacao.getDttransacao()) + "</td>"
                            + "					<td>" + transacao.getNumerocartao() + "</td>"
                            + "					<td>" + transacao.getValidade() + "</td>"
                            + "					<td>" + transacao.getBandeira() + "</td>"
                            + "					<td>" + transacao.getTitular() + "</td>"
                            + "					<td>" + transacao.getCodcli().getCodcli() + "</td>"
                            + "				</tr>"
                            + "			</tbody>"
                            + "		</table>"
                            + "		<h2>Documentos</h2>"
                            + "		<table>"
                            + "			<thead>"
                            + "				<tr>"
                            + "					<th>DOCUMENTO</th>"
                            + "					<th>VALOR</th>"
                            + "				</tr>"
                            + "			</thead>"
                            + "			<tbody>";
                    for (SdDoctoTransacaoCartao docto : transacao.getDoctos()) {
                        html += "				<tr>"
                                + "					<td>" + docto.getId().getDocto() + "</td>"
                                + "					<td>" + StringUtils.toMonetaryFormat(docto.getValor(), 2) + "</td>"
                                + "				</tr>";
                    }
                    html += "			</tbody>"
                            + "		</table>"
                            + "	</body>"
                            + "</html>";

                    return html;
                })
                .send();
    }

    private FormBox exibirComprovante(SdTransacaoCartao transacao) {
        return FormBox.create(boxDadosRecibo -> {
            boxDadosRecibo.vertical();
            boxDadosRecibo.addStyle("info-light");
            boxDadosRecibo.width(400.0);
            boxDadosRecibo.title("Recibo de Pagamento");
            boxDadosRecibo.add(FormBox.create(headerRecibo -> {
                headerRecibo.horizontal();
                headerRecibo.alignment(Pos.TOP_LEFT);
                headerRecibo.add(new ImageView(ImageUtils.getImage(getClass().getResource("/images/logo cielo.png").toExternalForm())));
                headerRecibo.add(FormBox.create(boxDataTransacao -> {
                    boxDataTransacao.vertical();
                    boxDataTransacao.expanded();
                    boxDataTransacao.alignment(Pos.TOP_RIGHT);
                    boxDataTransacao.add(FormLabel.create(label -> {
                        label.value.set(StringUtils.toDateTimeDescFormat(transacao.getDttransacao()));
                    }));
                }));
            }));
            boxDadosRecibo.add(new Separator(Orientation.HORIZONTAL));
            boxDadosRecibo.add(FormBox.create(infosEmpresaBandeira -> {
                infosEmpresaBandeira.horizontal();
                infosEmpresaBandeira.alignment(Pos.TOP_LEFT);
                infosEmpresaBandeira.add(FormBox.create(boxDataTransacao -> {
                    boxDataTransacao.vertical();
                    boxDataTransacao.expanded();
                    boxDataTransacao.withoutSpace();
                    boxDataTransacao.alignment(Pos.TOP_LEFT);
                    boxDataTransacao.add(FormLabel.create(label -> {
                        label.value.set("DELIZ FASHION GROUP");
                        label.sizeText(12);
                        label.boldText();
                    }));
                    boxDataTransacao.add(FormLabel.create(label -> {
                        label.value.set("RUA ANTONIO BAYER, TIJUCAS/SC");
                        label.sizeText(10);
                    }));
                    boxDataTransacao.add(FormLabel.create(label -> {
                        label.value.set("CNPJ: 03.306.680/0001-64");
                        label.sizeText(10);
                    }));
                }));
                infosEmpresaBandeira.add(FormBox.create(boxDataTransacao -> {
                    boxDataTransacao.vertical();
                    boxDataTransacao.alignment(Pos.TOP_RIGHT);
                    switch (transacao.getBandeira()) {
                        case "Amex":
                            boxDataTransacao.add(ImageUtils.getIcon(ImageUtils.Icon.AMEX_CARD, ImageUtils.IconSize._64));
                            break;
                        case "Elo":
                            boxDataTransacao.add(ImageUtils.getIcon(ImageUtils.Icon.ELO_CARD, ImageUtils.IconSize._64));
                            break;
                        case "Hipercard":
                            boxDataTransacao.add(ImageUtils.getIcon(ImageUtils.Icon.HIPER_CARD, ImageUtils.IconSize._64));
                            break;
                        case "Master":
                            boxDataTransacao.add(ImageUtils.getIcon(ImageUtils.Icon.MASTER_CARD, ImageUtils.IconSize._64));
                            break;
                        case "Visa":
                            boxDataTransacao.add(ImageUtils.getIcon(ImageUtils.Icon.VISA_CARD, ImageUtils.IconSize._64));
                            break;
                    }
                }));
            }));
            boxDadosRecibo.add(new Separator(Orientation.HORIZONTAL));
            boxDadosRecibo.add(FormBox.create(infosPagto -> {
                infosPagto.horizontal();
                infosPagto.alignment(Pos.TOP_LEFT);
                infosPagto.withoutSpace();
                infosPagto.add(FormBox.create(boxDataTransacao -> {
                    boxDataTransacao.vertical();
                    boxDataTransacao.expanded();
                    boxDataTransacao.withoutSpace();
                    boxDataTransacao.alignment(Pos.TOP_LEFT);
                    boxDataTransacao.add(FormLabel.create(label -> {
                        label.value.set("CRÉDITO " + (transacao.getParcelas() == 1 ? "À VISTA" : "PARCELADO"));
                        label.sizeText(13);
                    }));
                    boxDataTransacao.add(FormLabel.create(label -> {
                        label.value.set(transacao.getParcelas() == 1 ? "" : "Parcelado em " + transacao.getParcelas() + " x");
                        label.sizeText(11);
                    }));
                }));
                infosPagto.add(new Separator(Orientation.VERTICAL));
                infosPagto.add(FormBox.create(boxDataTransacao -> {
                    boxDataTransacao.vertical();
                    boxDataTransacao.expanded();
                    boxDataTransacao.alignment(Pos.TOP_RIGHT);
                    boxDataTransacao.add(FormLabel.create(label -> {
                        label.value.set(StringUtils.toMonetaryFormat(transacao.getValor() / 100.0, 2));
                        label.sizeText(16);
                        label.boldText();
                    }));
                }));
            }));
            boxDadosRecibo.add(FormBox.create(infosCliente -> {
                infosCliente.horizontal();
                infosCliente.alignment(Pos.TOP_LEFT);
                infosCliente.withoutSpace();
                infosCliente.add(FormBox.create(boxDataTransacao -> {
                    boxDataTransacao.vertical();
                    boxDataTransacao.expanded();
                    boxDataTransacao.withoutSpace();
                    boxDataTransacao.alignment(Pos.TOP_LEFT);
                    boxDataTransacao.add(FormLabel.create(label -> {
                        label.value.set(transacao.getCodcli().getNome());
                        label.sizeText(14);
                        label.boldText();
                    }));
                    boxDataTransacao.add(FormLabel.create(label -> {
                        label.value.set(transacao.getNumerocartao());
                        label.sizeText(12);
                    }));
                    boxDataTransacao.add(FormLabel.create(label -> {
                        label.value.set("Doctos: " + transacao.getDoctos().stream().map(docto -> docto.getId().getDocto()).collect(Collectors.joining(", ")));
                        label.sizeText(10);
                    }));
                }));
            }));
            boxDadosRecibo.add(new Separator(Orientation.HORIZONTAL));
            boxDadosRecibo.add(FormBox.create(infosTransacao -> {
                infosTransacao.vertical();
                infosTransacao.alignment(Pos.TOP_LEFT);
                infosTransacao.withoutSpace();
                infosTransacao.add(FormBox.create(boxDataTransacao -> {
                    boxDataTransacao.horizontal();
                    boxDataTransacao.alignment(Pos.TOP_LEFT);
                    boxDataTransacao.add(FormLabel.create(label -> {
                        label.value.set(transacao.getTid());
                        label.sizeText(10);
                    }));
                    boxDataTransacao.add(FormLabel.create(label -> {
                        label.value.set("DOC=" + transacao.getProofofsale());
                        label.sizeText(10);
                    }));
                    boxDataTransacao.add(FormLabel.create(label -> {
                        label.value.set("AUT=" + transacao.getAuthorizationcode());
                        label.sizeText(10);
                    }));
                }));
                infosTransacao.add(FormBox.create(boxDataTransacao -> {
                    boxDataTransacao.horizontal();
                    boxDataTransacao.alignment(Pos.TOP_LEFT);
                    boxDataTransacao.add(FormLabel.create(label -> {
                        label.value.set(transacao.getCodigo());
                        label.sizeText(10);
                    }));
                    boxDataTransacao.add(FormLabel.create(label -> {
                        label.value.set("VIA - CLIENTE");
                        label.alignment(Pos.TOP_RIGHT);
                        label.sizeText(10);
                    }));
                }));
            }));
        });
    }

    private void cancelarPagamento() {
        clearDadosCliente();
        clearDadosCartao();
        List<VSdDadosPedido> pedidos = new ArrayList<>(pedidosSelecionados.get());
        List<Receber> titulos = new ArrayList<>(titulosSelecionados.get());
        pedidos.forEach(this::excluirPedidoSeleciona);
        titulos.forEach(this::excluirTituloSeleciona);
        emTransacao.set(false);
    }

    private void clearDadosCartao() {
        fieldCartaoNome.clear();
        fieldCartaoCvv.clear();
        fieldCartaoAnoVencimento.select(0);
        fieldCartaoMesVencimento.select(0);
        fieldCartaoNumeroQuad4.clear();
        fieldCartaoNumeroQuad3.clear();
        fieldCartaoNumeroQuad2.clear();
        fieldCartaoNumeroQuad1.clear();
        boxBandeiraCartao.clear();
        isBandeiraAceita.set(false);
        bandeiraCartao.set(null);
    }

    private void clearDadosCliente() {
        fieldClienteTelefone.clear();
        fieldClienteEmail.clear();
        fieldClienteCidade.clear();
        fieldObservacoesRecebimento.clear();
    }

    private void cancelarTransacao(SdTransacaoCartao item) {
        final ObjectProperty<BigDecimal> valorCancelamento = new SimpleObjectProperty<>(
                new BigDecimal((item.getValor() - item.getValorcancelado()) / 100.0));
        final FormFieldText fieldValorCancelamento = FormFieldText.create(field -> {
            field.withoutTitle().label("Valor R$:");
            field.width(200.0);
            field.mask(FormFieldText.Mask.MONEY);
            field.value.bindBidirectional(valorCancelamento, new StringConverter<BigDecimal>() {
                @Override
                public String toString(BigDecimal object) {
                    return StringUtils.toMonetaryFormat(object, 2);
                }

                @Override
                public BigDecimal fromString(String string) {
                    return new BigDecimal(string.replace(".", "").replace(",", "."));
                }
            });
            valorRecebimento.addListener((observable, oldValue, newValue) -> {
                if (newValue != null)
                    valorParcela.set(valorRecebimento.get().divide(new BigDecimal(this.fieldPagtoParcelas.value.get()), 2, RoundingMode.HALF_UP));
            });
        });
        new Fragment().show(fragment -> {
            fragment.title("Estornar Valor de Transação");
            fragment.size(300.0, 150.0);
            fragment.box.getChildren().add(FormBox.create(boxCarregaRecibo -> {
                boxCarregaRecibo.vertical();
                boxCarregaRecibo.add(fieldValorCancelamento.build());
            }));
            fragment.buttonsBox.getChildren().add(FormButton.create(btn -> {
                btn.title("Estornar Valor");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CARTAO_INVALIDO, ImageUtils.IconSize._24));
                btn.addStyle("warning");
                btn.setAction(evt -> {
                    if (new BigDecimal(item.getValor()).compareTo(valorCancelamento.get()) < 0) {
                        MessageBox.create(message -> {
                            message.message("O valor de estorno não pode ser maior que o valor da transação.");
                            message.type(MessageBox.TypeMessageBox.ALERT);
                            message.showAndWait();
                        });
                        return;
                    }
                    cancelarValorTransacao(item, valorCancelamento.get());
                    tblTransacoes.refresh();
                    fragment.close();
                });
            }));
        });
    }

    private void cancelarValorTransacao(SdTransacaoCartao transacao, BigDecimal valor) {
        Integer valorCancelado = valor.multiply(new BigDecimal(100)).intValue();
        try {
            Cancel cancel = new WSCielo().cancel(transacao.getPaymentid(), valorCancelado);
            if (cancel.getStatus() == 2 || cancel.getStatus() == 10 || cancel.getStatus() == 11) {
                transacao.setDtcancelamento(LocalDateTime.now());
                transacao.setValorcancelado(transacao.getValorcancelado() + valorCancelado);
                new FluentDao().merge(transacao);

                SysLogger.addSysDelizLog("Receber Cartão CIELO", TipoAcao.EDITAR, transacao.getCodigo(),
                        "Estornado valor " + StringUtils.toMonetaryFormat(valorCancelado / 100.0, 2) + " da transação.");
                tblTransacoes.refresh();
            } else {
                Cancel finalReciboCaptura = cancel;
                MessageBox.create(message -> {
                    message.message("Não foi possível estornar a transação do cartão.\n" +
                            "Erro: " + finalReciboCaptura.getReturnCode() + " Mensagem: " + finalReciboCaptura.getReturnMessage());
                    message.type(MessageBox.TypeMessageBox.ALERT);
                    message.showAndWait();
                });
            }
        } catch (IOException e) {
            e.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(e);
                message.showAndWait();
            });
        } catch (HttpException e) {
            try {
                ErroApi[] errosApi = new Gson().fromJson(e.response().errorBody().string(), new TypeToken<ErroApi[]>() {
                }.getType());
                SdErroApiCielo erroCielo = new FluentDao().selectFrom(SdErroApiCielo.class)
                        .where(eb -> eb.equal("codigo", errosApi[0].getCode()))
                        .singleResult();
                if (erroCielo != null) {
                    MessageBox.create(message -> {
                        message.message("Ocorreu um erro durante a transação do cartão.\n" +
                                "Erro: " + erroCielo.getCodigo() + " Mensagem: " + erroCielo.getMensagem() + "\n" +
                                "Descrição: " + erroCielo.getDescricao());
                        message.type(MessageBox.TypeMessageBox.ERROR);
                        message.showAndWait();
                    });
                } else {
                    MessageBox.create(message -> {
                        message.message("Ocorreu um erro durante a transação do cartão. Erro: " + errosApi[0].getCode() +
                                " Descrição: " + errosApi[0].getMessage());
                        message.type(MessageBox.TypeMessageBox.ERROR);
                        message.showAndWait();
                    });
                }
            } catch (IOException ex) {
                ex.printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(ex);
                    message.showAndWait();
                });
            }
        }
    }

    private void enviarComprovanteCliente(SdTransacaoCartao item) {
        new Fragment().show(fragment -> {
            fragment.title("Comprovante Transação");
            fragment.size(400.0, 500.0);
            FormBox comprovante = FormBox.create(boxViewRecibo -> {
                boxViewRecibo.vertical();
                boxViewRecibo.add(exibirComprovante(item));
            });
            fragment.box.getChildren().add(FormBox.create(boxCarregaRecibo -> {
                boxCarregaRecibo.vertical();
                boxCarregaRecibo.add(comprovante);
            }));
            fragment.buttonsBox.getChildren().add(FormButton.create(btn -> {
                btn.title("Enviar");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._24));
                btn.addStyle("success");
                btn.setAction(evt -> {
                    sendReciboEmail(item, comprovante);
                    fragment.close();
                });
            }));
        });
        tblTransacoes.refresh();
    }

    private void capturarTransacaoManual(SdTransacaoCartao item) {
        Capture reciboCaptura = null;
        try {
            reciboCaptura = new WSCielo().capture(item.getPaymentid());
            if (reciboCaptura.getStatus() == 2) {
                item.setCapturado(true);
                new FluentDao().merge(item);
                enviarComprovanteCliente(item);
                SysLogger.addSysDelizLog("Receber Cartão CIELO", TipoAcao.EDITAR, item.getCodigo(),
                        "Capturado valor da transação.");
                tblTransacoes.refresh();
            } else {
                Capture finalReciboCaptura = reciboCaptura;
                MessageBox.create(message -> {
                    message.message("Não foi possível capturar a transação do cartão.\n" +
                            "Erro: " + finalReciboCaptura.getReturnCode() + " Mensagem: " + finalReciboCaptura.getReturnMessage());
                    message.type(MessageBox.TypeMessageBox.ALERT);
                    message.showAndWait();
                });
            }
        } catch (IOException e) {
            e.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(e);
                message.showAndWait();
            });
        } catch (HttpException e) {
            try {
                ErroApi[] errosApi = new Gson().fromJson(e.response().errorBody().string(), new TypeToken<ErroApi[]>() {
                }.getType());
                SdErroApiCielo erroCielo = new FluentDao().selectFrom(SdErroApiCielo.class)
                        .where(eb -> eb.equal("codigo", errosApi[0].getCode()))
                        .singleResult();
                if (erroCielo != null) {
                    MessageBox.create(message -> {
                        message.message("Ocorreu um erro durante a transação do cartão.\n" +
                                "Erro: " + erroCielo.getCodigo() + " Mensagem: " + erroCielo.getMensagem() + "\n" +
                                "Descrição: " + erroCielo.getDescricao());
                        message.type(MessageBox.TypeMessageBox.ERROR);
                        message.showAndWait();
                    });
                } else {
                    MessageBox.create(message -> {
                        message.message("Ocorreu um erro durante a transação do cartão. Erro: " + errosApi[0].getCode() +
                                " Descrição: " + errosApi[0].getMessage());
                        message.type(MessageBox.TypeMessageBox.ERROR);
                        message.showAndWait();
                    });
                }
            } catch (IOException ex) {
                ex.printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(ex);
                    message.showAndWait();
                });
            }
        }
    }

    private void abrirDocumentosPagos(SdTransacaoCartao item) {
        new Fragment().show(fragment -> {
            fragment.title("Documentos da Transação");
            fragment.size(300.0, 350.0);
            FormTableView<SdDoctoTransacaoCartao> tblDocumentos = FormTableView.create(SdDoctoTransacaoCartao.class, table -> {
                table.title("Documentos");
                table.expanded();
                table.items.set(FXCollections.observableList(item.getDoctos()));
                table.columns(
                        FormTableColumn.create(cln -> {
                            cln.title("Documento");
                            cln.width(100.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdDoctoTransacaoCartao, SdDoctoTransacaoCartao>, ObservableValue<SdDoctoTransacaoCartao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getDocto()));
                            cln.alignment(Pos.CENTER);
                        }).build() /*Documento*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Valor");
                            cln.width(150.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdDoctoTransacaoCartao, SdDoctoTransacaoCartao>, ObservableValue<SdDoctoTransacaoCartao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValor()));
                            cln.alignment(Pos.CENTER_RIGHT);
                            cln.format(param -> {
                                return new TableCell<SdDoctoTransacaoCartao, BigDecimal>() {
                                    @Override
                                    protected void updateItem(BigDecimal item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        if (item != null && !empty) {
                                            setText(StringUtils.toMonetaryFormat(item, 2));
                                        }
                                    }
                                };
                            });
                        }).build() /*Valor*/
                );
            });
            fragment.box.getChildren().add(FormBox.create(boxCarregaRecibo -> {
                boxCarregaRecibo.vertical();
                boxCarregaRecibo.add(tblDocumentos.build());
            }));
        });
    }
}
