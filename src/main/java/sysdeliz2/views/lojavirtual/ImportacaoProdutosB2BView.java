package sysdeliz2.views.lojavirtual;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.util.Callback;
import sysdeliz2.controllers.views.lojavirtual.ImportacaoProdutosB2BController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.ti.PaIten;
import sysdeliz2.models.ti.Pedido3Bean;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.SortUtils;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.stream.Collectors;

public class ImportacaoProdutosB2BView extends ImportacaoProdutosB2BController {

    private final ListProperty<Pedido3Bean> itensPedidoBean = new SimpleListProperty<>();
    private final ListProperty<Pedido3Bean> pedidosBean = new SimpleListProperty<>();

    private final FormFieldText numeroPedidoFilter = FormFieldText.create(field -> {
        field.title("Nº Pedido");
        field.width(150);
    });

    private final FormFieldText notaFilter = FormFieldText.create(field -> {
        field.title("Nota");
        field.width(150);
    });

    private final FormFieldDate dtLeituraFilter = FormFieldDate.create(field -> {
        field.withoutTitle();
        field.value.setValue(LocalDate.now());
    });

    private final FormFieldToggle tsDataLeitura = FormFieldToggle.create(fft -> {
        fft.value.set(true);
        fft.withoutTitle();
        fft.changed((observable, oldValue, newValue) -> {
            dtLeituraFilter.disable.set(!newValue);
        });
    });

    private final Button btnImportarProdutos = FormButton.create(btn -> {
        btn.setText("Importar");
        btn.addStyle("success");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            importarProdutos();
        });
    });

    private final FormTableView<Pedido3Bean> tblPedidos = FormTableView.create(Pedido3Bean.class, table -> {
        table.title("Pedidos");
        table.expanded();
        table.items.bind(pedidosBean);
        table.selectionModelItem((observable, oldValue, newValue) -> {
            if (newValue != null) {
                Pedido3Bean pedido = (Pedido3Bean) newValue;
                Comparator<Pedido3Bean> comparePedido3 = Comparator
                        .comparing((Pedido3Bean o1) -> o1.getId().getCodigo())
                        .thenComparing((Pedido3Bean o2) -> o2.getId().getCor())
                        .thenComparing((o1, o2) -> SortUtils.sortTamanhos(o1.getId().getTam(), o2.getId().getTam()));
                if (pedido.getItens() != null) {
                    pedido.getItens().sort(comparePedido3);
                    itensPedidoBean.set(FXCollections.observableArrayList(pedido.getItens()));
                }
            }
        });

        table.factoryRow(param -> new TableRow<Pedido3Bean>() {
            @Override
            protected void updateItem(Pedido3Bean item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                clear();
                if (item != null && !empty && item.getItens() != null) {
                    if (item.getItens().stream().allMatch(Pedido3Bean::getImportado)) {
                        getStyleClass().add("table-row-success");
                    } else {
                        getStyleClass().add("table-row-danger");
                    }
                }
            }

            private void clear() {
                getStyleClass().removeAll("table-row-danger", "table-row-success");
            }
        });
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Nº Pedido");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<Pedido3Bean, Pedido3Bean>, ObservableValue<Pedido3Bean>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getNumero()));

                }).build(), /*Nº Pedido*/
                FormTableColumn.create(cln -> {
                    cln.title("Nota");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<Pedido3Bean, Pedido3Bean>, ObservableValue<Pedido3Bean>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNotaFiscal()));

                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Data");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<Pedido3Bean, Pedido3Bean>, ObservableValue<Pedido3Bean>>) param -> new ReadOnlyObjectWrapper(StringUtils.toShortDateFormat(param.getValue().getData())));

                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Qtde Itens");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<Pedido3Bean, Pedido3Bean>, ObservableValue<Pedido3Bean>>) param -> new ReadOnlyObjectWrapper(param.getValue().getItens().size()));

                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Status");
                    cln.width(170);
                    cln.value((Callback<TableColumn.CellDataFeatures<Pedido3Bean, Pedido3Bean>, ObservableValue<Pedido3Bean>>) param -> new ReadOnlyObjectWrapper(param.getValue().getImportado() ? "PRODUTOS IMPORTADOS" : "NÃO IMPORTADOS"));

                }).build()
        );
    });

    private final FormTableView<Pedido3Bean> tblItensPedido = FormTableView.create(Pedido3Bean.class, table -> {
        table.title("Itens Pedido");
        table.expanded();
        table.factoryRow(param -> new TableRow<Pedido3Bean>() {
            @Override
            protected void updateItem(Pedido3Bean item, boolean empty) {
                super.updateItem(item, empty);
                setText(null);
                clear();
                if (item != null && !empty && item.getItens() != null) {
                    getStyleClass().add(item.getImportado() ? "table-row-success" : "table-row-danger");
                }
            }

            private void clear() {
                getStyleClass().removeAll("table-row-danger", "table-row-success");
            }
        });
        table.items.bind(itensPedidoBean);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<Pedido3Bean, Pedido3Bean>, ObservableValue<Pedido3Bean>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCodigo()));

                }).build(), /*Nº Pedido*/
                FormTableColumn.create(cln -> {
                    cln.title("Cor");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<Pedido3Bean, Pedido3Bean>, ObservableValue<Pedido3Bean>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCor()));

                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Tam");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<Pedido3Bean, Pedido3Bean>, ObservableValue<Pedido3Bean>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getTam()));

                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Qtde");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<Pedido3Bean, Pedido3Bean>, ObservableValue<Pedido3Bean>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtdeF()));

                }).build()
        );
    });

    public ImportacaoProdutosB2BView() {
        super("Importação Produtos", ImageUtils.getImage(ImageUtils.Icon.ADICIONAR_PEDIDO));
        init();
    }

    private void init() {
        super.box.getChildren().add(FormBox.create(principal -> {
            principal.horizontal();
            principal.expanded();
            principal.add(FormBox.create(col1 -> {
                col1.vertical();
                col1.add(FormBox.create(boxTitledPane -> {
                    boxTitledPane.vertical();
                    boxTitledPane.add(FormTitledPane.create(filter -> {
                        filter.filter();
                        filter.add(FormBox.create(box -> {
                            box.horizontal();
                            box.add(FormBox.create(le -> {
                                le.horizontal();
                                le.add(numeroPedidoFilter.build(), notaFilter.build());
                            }));
                            box.add(FormBox.create(ld -> {
                                ld.horizontal();
                                ld.title("Dt. Leitura");
                                ld.add(tsDataLeitura.build(), dtLeituraFilter.build());
                            }));
                        }));
                        filter.find.setOnAction(evt -> {
                            new RunAsyncWithOverlay(this).exec(task -> {
                                bucarPedidos(
                                        numeroPedidoFilter.value.getValueSafe(),
                                        notaFilter.value.getValueSafe(),
                                        dtLeituraFilter.value.getValue(),
                                        tsDataLeitura.value.getValue()
                                );
                                return ReturnAsync.OK.value;
                            }).addTaskEndNotification(taskReturn -> {
                                if (taskReturn.equals(ReturnAsync.OK.value)) {
                                    itensPedidoBean.clear();
                                    pedidosBean.set(FXCollections.observableArrayList(pedidos));
                                }
                            });

                        });
                        filter.clean.setOnAction(evt -> {
                            numeroPedidoFilter.clear();
                            notaFilter.clear();
                            dtLeituraFilter.value.setValue(LocalDate.now());
                        });
                    }));
                }));
                col1.add(tblPedidos.build());
                col1.add(FormBox.create(boxBottom -> {
                    boxBottom.horizontal();
                    boxBottom.add(btnImportarProdutos);
                }));
            }));
            principal.add(FormBox.create(col2 -> {
                col2.vertical();
                col2.setPrefWidth(420);
                col2.add(tblItensPedido.build());
            }));
        }));
    }

    private void importarProdutos() {

        if (itensPedidoBean.size() == 0) {
            MessageBox.create(message -> {
                message.message("Nenhum pedido selecionado.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            return;
        }

        if (itensPedidoBean.stream().allMatch(Pedido3Bean::getImportado)) {
            MessageBox.create(message -> {
                message.message("Todos os produtos importados.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            return;
        }

        new RunAsyncWithOverlay(this).exec(task -> {
            for (Pedido3Bean itemPedido : itensPedidoBean.stream().filter(it -> !it.getImportado()).collect(Collectors.toList())) {
                PaIten estq = new FluentDao().selectFrom(PaIten.class)
                        .where(it -> it
                                .equal("id.tam", itemPedido.getId().getTam())
                                .equal("id.cor", itemPedido.getId().getCor())
                                .equal("id.codigo", itemPedido.getId().getCodigo())
                                .equal("id.deposito", "0023")
                        ).singleResult();

                if (estq != null)
                    estq.setQuantidade(estq.getQuantidade().add(itemPedido.getQtdeF()));
                else
                    estq = new PaIten(itemPedido.getQtdeF().intValue(), itemPedido.getId().getTam(), itemPedido.getId().getCodigo(), itemPedido.getId().getCor(), "0023");

                estq = new FluentDao().merge(estq);
                SysLogger.addSysDelizLog("Importação de Produtos B2C", TipoAcao.EDITAR,
                        itemPedido.getId().getCodigo(),
                        "Alteração no estoque B2C do produto " + itemPedido.getId().getCodigo() + " na cor " + itemPedido.getId().getCor() + " e tamanho " + itemPedido.getId().getTam() + ". Nova quantidade: " + estq.getQuantidade());
            }
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                for (Pedido3Bean itemPedido : itensPedidoBean.stream().filter(it -> !it.getImportado()).collect(Collectors.toList())) {
                    itemPedido.setImportado(true);
                    itemPedido = new FluentDao().merge(itemPedido);
                }
                tblPedidos.selectedItem().setImportado(true);
                tblPedidos.refresh();
                tblItensPedido.clear();
                MessageBox.create(message -> {
                    message.message("Importação Concluída com sucesso!");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            }
        });

    }

}
