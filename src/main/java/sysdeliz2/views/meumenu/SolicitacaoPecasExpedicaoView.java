package sysdeliz2.views.meumenu;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.util.Callback;
import sysdeliz2.controllers.views.meumenu.SolicitacaoPecasExpedicaoController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.sysdeliz.solicitacaoExp.SdItemSolicitacaoExp;
import sysdeliz2.models.sysdeliz.solicitacaoExp.SdSolicitacaoExp;
import sysdeliz2.models.sysdeliz.solicitacaoExp.SdStatusSolicitacaoExp;
import sysdeliz2.models.sysdeliz.solicitacaoExp.SdTipoSolicitacaoExp;
import sysdeliz2.models.ti.*;
import sysdeliz2.models.view.VSdDadosProdutoCorTam;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.SortUtils;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class SolicitacaoPecasExpedicaoView extends SolicitacaoPecasExpedicaoController {

    // <editor-fold defaultstate="collapsed" desc="Status">
    private final SdStatusSolicitacaoExp STATUS_CRIADA = new FluentDao().selectFrom(SdStatusSolicitacaoExp.class).where(it -> it.equal("codigo", 1)).singleResult();
    private final SdStatusSolicitacaoExp STATUS_ENVIADA = new FluentDao().selectFrom(SdStatusSolicitacaoExp.class).where(it -> it.equal("codigo", 2)).singleResult();
    private final SdStatusSolicitacaoExp STATUS_CANCELADA = new FluentDao().selectFrom(SdStatusSolicitacaoExp.class).where(it -> it.equal("codigo", 3)).singleResult();
    private final SdStatusSolicitacaoExp STATUS_VISUALIZADA = new FluentDao().selectFrom(SdStatusSolicitacaoExp.class).where(it -> it.equal("codigo", 4)).singleResult();
    private final SdStatusSolicitacaoExp STATUS_LIBERADO_LEITURA = new FluentDao().selectFrom(SdStatusSolicitacaoExp.class).where(it -> it.equal("codigo", 5)).singleResult();
    private final SdStatusSolicitacaoExp STATUS_COLETADO = new FluentDao().selectFrom(SdStatusSolicitacaoExp.class).where(it -> it.equal("codigo", 6)).singleResult();
    private final SdStatusSolicitacaoExp STATUS_PCS_ENTREGUES = new FluentDao().selectFrom(SdStatusSolicitacaoExp.class).where(it -> it.equal("codigo", 7)).singleResult();
    private final SdStatusSolicitacaoExp STATUS_PCS_DEVOLVIDAS = new FluentDao().selectFrom(SdStatusSolicitacaoExp.class).where(it -> it.equal("codigo", 8)).singleResult();
    private final SdStatusSolicitacaoExp STATUS_FINALIZADA = new FluentDao().selectFrom(SdStatusSolicitacaoExp.class).where(it -> it.equal("codigo", 9)).singleResult();
    // </editor-fold>

    //<editor-fold desc="Lists Bean">
    private final ListProperty<SdSolicitacaoExp> solicitacoesBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<SdItemSolicitacaoExp> itensSolicitacaoBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<VSdDadosProdutoCorTam> gradeBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private List<SdStatusSolicitacaoExp> listStatus = (List<SdStatusSolicitacaoExp>) new FluentDao().selectFrom(SdStatusSolicitacaoExp.class).get().orderBy("codigo", OrderType.ASC).resultList();
    //</editor-fold>

    private final BooleanProperty inEdition = new SimpleBooleanProperty(false);
    private SdSolicitacaoExp solicitacaoEscolhida = null;

    //<editor-fold desc="Components">
    private final FormFieldDatePeriod filterDataSolicitacao = FormFieldDatePeriod.create(field -> {
        field.title("Data Solicitação");
        field.valueBegin.setValue(LocalDate.now().minusMonths(1));
        field.valueEnd.setValue(LocalDate.now().plusMonths(1));
    });

    private final FormFieldSingleFind<SdTipoSolicitacaoExp> filterTipoSolicitacao = FormFieldSingleFind.create(SdTipoSolicitacaoExp.class, field -> {
        field.title("Tipo");
        field.dividedWidth(300);
    });

    private final FormFieldMultipleFind<Produto> filterProduto = FormFieldMultipleFind.create(Produto.class, field -> {
        field.title("Produto");
    });

    private final FormFieldMultipleFind<SdSolicitacaoExp> filterSolicitacaoExp = FormFieldMultipleFind.create(SdSolicitacaoExp.class, field -> {
        field.title("Código");
    });

    private final FormFieldTextArea fieldObservacao = FormFieldTextArea.create(field -> {
        field.title("Observação");
        field.expanded();
    });
    //</editor-fold>

    //<editor-fold desc="TableView">
    private final FormTableView<SdSolicitacaoExp> tblSolicitacao = FormTableView.create(SdSolicitacaoExp.class, table -> {
        table.title("Solicitações");
        table.expanded();
        table.items.bind(solicitacoesBean);
        table.factoryRow(param -> new TableRow<SdSolicitacaoExp>() {
            @Override
            protected void updateItem(SdSolicitacaoExp item, boolean empty) {
                super.updateItem(item, empty);
                clear();

                if (item != null && !empty) {
                    getStyleClass().add("table-row-" + item.getStatus().getStyle());
                }
            }

            private void clear() {
                getStyleClass().removeIf(it -> it.contains("table-row-") && !it.equals("table-row-cell"));
            }
        });
        table.selectionModelItem((observable, oldValue, newValue) -> {
            if (newValue != null) carregaSolicitacao((SdSolicitacaoExp) newValue);
        });
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(90);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdSolicitacaoExp, SdSolicitacaoExp>, ObservableValue<SdSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId()));
                }).build(), /**/
                FormTableColumn.create(cln -> {
                    cln.title("Tipo");
                    cln.width(230);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdSolicitacaoExp, SdSolicitacaoExp>, ObservableValue<SdSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTipo().getTipo() + " - " + param.getValue().getTipo().getDescricao()));
                }).build(), /**/
                FormTableColumn.create(cln -> {
                    cln.title("Dt Solicitação");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdSolicitacaoExp, SdSolicitacaoExp>, ObservableValue<SdSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(StringUtils.toShortDateFormat(param.getValue().getDtSolicitacao())));
                }).build(), /**/
                FormTableColumn.create(cln -> {
                    cln.title("Status");
                    cln.width(160);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdSolicitacaoExp, SdSolicitacaoExp>, ObservableValue<SdSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(param.getValue().getStatus().getStatus()));
                }).build() /**/
        );
    });

    private final FormTableView<SdItemSolicitacaoExp> tblPrdutos = FormTableView.create(SdItemSolicitacaoExp.class, table -> {
        table.title("Produtos");
        table.items.bind(itensSolicitacaoBean);
        table.expanded();
        table.selectionModelItem((observable, oldValue, newValue) -> {
            if (newValue != null) {
                carregaGradeProduto((SdItemSolicitacaoExp) newValue);
            }
        });
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Ações");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(60);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemSolicitacaoExp, SdItemSolicitacaoExp>, ObservableValue<SdItemSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<SdItemSolicitacaoExp, SdItemSolicitacaoExp>() {
                        @Override
                        protected void updateItem(SdItemSolicitacaoExp item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                setGraphic(FormButton.create(btn -> {
                                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                    btn.tooltip("Remover item da solicitação");
                                    btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                    btn.addStyle("danger");
                                    btn.setOnAction(evt -> {
                                        removerItem(item);
                                    });
                                }));
                            }
                        }
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemSolicitacaoExp, SdItemSolicitacaoExp>, ObservableValue<SdItemSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(param.getValue().getProduto().getCodigo()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(250);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemSolicitacaoExp, SdItemSolicitacaoExp>, ObservableValue<SdItemSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(param.getValue().getProduto().getDescricao()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Cor");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemSolicitacaoExp, SdItemSolicitacaoExp>, ObservableValue<SdItemSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(param.getValue().getProduto().getCor()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Marca");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemSolicitacaoExp, SdItemSolicitacaoExp>, ObservableValue<SdItemSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(param.getValue().getProduto().getMarca()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Coleção");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemSolicitacaoExp, SdItemSolicitacaoExp>, ObservableValue<SdItemSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(param.getValue().getProduto().getColecao()));
                }).build(), /*Código*/
                FormTableColumn.create(cln -> {
                    cln.title("Qtde");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdItemSolicitacaoExp, SdItemSolicitacaoExp>, ObservableValue<SdItemSolicitacaoExp>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<SdItemSolicitacaoExp, SdItemSolicitacaoExp>() {
                        @Override
                        protected void updateItem(SdItemSolicitacaoExp item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                setText(String.valueOf(solicitacaoEscolhida.getItens().stream().filter(it ->
                                        it.getProduto().getCodigo().equals(item.getProduto().getCodigo()) &&
                                                it.getProduto().getCor().equals(item.getProduto().getCor())
                                ).mapToInt(SdItemSolicitacaoExp::getQtde).sum()));
                            }
                        }
                    });
                }).build() /*Código*/
        );
    });

    private final FormTableView<VSdDadosProdutoCorTam> tblGrade = FormTableView.create(VSdDadosProdutoCorTam.class, table -> {
        table.items.bind(gradeBean);
        table.title("Grade");
        table.expanded();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Tam");
                    cln.width(130);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosProdutoCorTam, VSdDadosProdutoCorTam>, ObservableValue<VSdDadosProdutoCorTam>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> new TableCell<VSdDadosProdutoCorTam, VSdDadosProdutoCorTam>() {
                        @Override
                        protected void updateItem(VSdDadosProdutoCorTam item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            if (item != null && !empty) {
                                setText(item.getTam());
                                getStyleClass().add("lg");
                            }
                        }
                    });
                }).build(), /*Tam*/
                FormTableColumn.create(cln -> {
                    cln.title("Qtde Estoque");
                    cln.width(130);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosProdutoCorTam, VSdDadosProdutoCorTam>, ObservableValue<VSdDadosProdutoCorTam>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> new TableCell<VSdDadosProdutoCorTam, VSdDadosProdutoCorTam>() {
                        @Override
                        protected void updateItem(VSdDadosProdutoCorTam item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            if (item != null && !empty) {
                                setText(String.valueOf(item.getQtde()));
                                getStyleClass().add("lg");
                            }
                        }
                    });
                }).build(), /*Tam*/
                FormTableColumn.create(cln -> {
                    cln.title("Qtde");
                    cln.width(130);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosProdutoCorTam, VSdDadosProdutoCorTam>, ObservableValue<VSdDadosProdutoCorTam>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> new TableCell<VSdDadosProdutoCorTam, VSdDadosProdutoCorTam>() {
                        @Override
                        protected void updateItem(VSdDadosProdutoCorTam item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                setGraphic(FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.addStyle("lg");
                                    field.alignment(Pos.CENTER);
                                    field.decimalField(0);
                                    field.value.set("0");
                                    getItemSolicitacao(item).ifPresent(il -> field.value.set(String.valueOf(il.getQtde())));
                                    field.mouseClicked(evt -> field.textField.selectAll());
                                    field.focusedListener((observable, oldValue, newValue) -> {
                                        String valor = field.value.getValueSafe();
                                        if (valor.equals("")) valor = "0";
                                        if (Integer.parseInt(valor) > item.getQtde()) {
                                            field.value.setValue("0");
                                            MessageBox.create(message -> {
                                                message.message("Valor pedido excede a quantidade em estoque");
                                                message.type(MessageBox.TypeMessageBox.WARNING);
                                                message.position(Pos.TOP_RIGHT);
                                                message.notification();
                                            });
                                        } else atualizaQuantidadeItem(item, valor);
                                    });
                                }).build());
                            }
                        }
                    });
                }).build() /*Tam*/
        );
    });
    //</editor-fold>

    //<editor-fold desc="Button">
    private final Button btnNovaSolicitacao = FormButton.create(btn -> {
        btn.setText("Criar Nova");
        btn.addStyle("info");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.NOTA_FISCAL, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            criarSolicitacao();
        });
    });

    private final Button btnExcluirSolicitacao = FormButton.create(btn -> {
        btn.setText("Excluir");
        btn.addStyle("danger");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            excluirSolicitacao();
        });
    });

    private final Button btnEnviarSolicitacao = FormButton.create(btn -> {
        btn.setText("Enviar");
        btn.addStyle("primary");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._24));
        btn.disable.bind(inEdition.not());
        btn.setOnAction(evt -> {
            salvarSolicitacao();
        });
    });

    private final Button btnCancelarSolicitacao = FormButton.create(btn -> {
        btn.setText("Cancelar");
        btn.addStyle("danger");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
        btn.disable.bind(inEdition.not());
        btn.setOnAction(evt -> {
            cancelarSolicitacao();
        });
    });

    private final Button btnAdicionarProdutos = FormButton.create(btn -> {
        btn.setText("Adicionar Produto");
        btn.addStyle("success");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._24));
        btn.disable.bind(inEdition.not());
        btn.setOnAction(evt -> {
            adicionarProduto();
        });
    });

    private final Button btnEditarSolicitacao = FormButton.create(btn -> {
        btn.setText("Editar");
        btn.addStyle("warning");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EDIT, ImageUtils.IconSize._24));
        btn.disable.bind(inEdition);
        btn.setOnAction(evt -> {
            editarSolicitacao();
        });
    });

    //</editor-fold>

    //<editor-fold desc="Field Text">
    private final FormFieldText numeroSolicitacaoField = FormFieldText.create(field -> {
        field.title("Código");
        field.editable.set(false);
    });

    private final FormFieldDate dataSolicitacaoField = FormFieldDate.create(field -> {
        field.title("Data");
        field.editable.bind(inEdition);
    });

    private final FormFieldSingleFind<SdTipoSolicitacaoExp> tipoSolicitacaoField = FormFieldSingleFind.create(SdTipoSolicitacaoExp.class, field -> {
        field.title("Tipo");
        field.width(400.0);
        field.code.setPrefWidth(100);
    });

    private final FormFieldText qtdeProdutosField = FormFieldText.create(field -> {
        field.title("Qtde Produtos");
        field.editable.set(false);
        field.width(120);
    });

    private final FormFieldText qtdeTotalItensField = FormFieldText.create(field -> {
        field.title("Qtde Total");
        field.editable.set(false);
        field.width(120);
    });
    //</editor-fold>

    public SolicitacaoPecasExpedicaoView() {
        super("Solicitação de Peças para Expedição", ImageUtils.getImage(ImageUtils.Icon.EXPEDICAO));
        init();
        carregarSolicitacoesPendentes();
    }

    private void init() {
        box.getChildren().add(FormBox.create(principal -> {
            principal.horizontal();
            principal.expanded();
            principal.add(FormBox.create(boxClnPedidos -> {
                boxClnPedidos.vertical();
                boxClnPedidos.disableProperty().bind(inEdition);
                boxClnPedidos.add(FormBox.create(boxFiltroPedido -> {
                    boxFiltroPedido.vertical();
                    boxFiltroPedido.add(FormTitledPane.create(filter -> {
                        filter.filter();
                        filter.width(600);
                        filter.add(FormBox.create(boxFiltro -> {
                            boxFiltro.vertical();
                            boxFiltro.add(FormBox.create(linha1 -> {
                                linha1.horizontal();
                                linha1.add(filterSolicitacaoExp.build(), filterTipoSolicitacao.build());
                            }));
                            boxFiltro.add(FormBox.create(linha2 -> {
                                linha2.horizontal();
                                linha2.add(filterDataSolicitacao.build(), filterProduto.build());
                            }));
                        }));
                        filter.find.setOnAction(evt -> {
                            new RunAsyncWithOverlay(this).exec(task -> {
                                buscarSolicitacoes(
                                        filterSolicitacaoExp.objectValues.stream().map(SdSolicitacaoExp::getId).toArray(),
                                        filterTipoSolicitacao.value.get() == null ? new SdTipoSolicitacaoExp() : filterTipoSolicitacao.value.get(),
                                        filterDataSolicitacao.valueBegin.getValue(),
                                        filterDataSolicitacao.valueEnd.getValue(),
                                        filterProduto.objectValues.stream().map(Produto::getCodigo).toArray()
                                );
                                return ReturnAsync.OK.value;
                            }).addTaskEndNotification(taskReturn -> {
                                if (taskReturn.equals(ReturnAsync.OK.value)) {
                                    solicitacoesBean.set(FXCollections.observableArrayList(solicitacoesExp));
                                }
                            });
                        });
                        filter.clean.setOnAction(evt -> {
                            filterSolicitacaoExp.clear();
                            filterTipoSolicitacao.clear();
                            filterDataSolicitacao.clear();
                            filterProduto.clear();
                        });
                    }));
                }));
                boxClnPedidos.add(FormBox.create(boxTblPedidos -> {
                    boxTblPedidos.horizontal();
                    boxTblPedidos.expanded();
                    boxTblPedidos.add(tblSolicitacao.build());
                }));
                boxClnPedidos.add(FormBox.create(boxButtons -> {
                    boxButtons.horizontal();
                    boxButtons.add(btnExcluirSolicitacao, btnNovaSolicitacao, btnEditarSolicitacao);
                }));
            }));
            principal.add(FormBox.create(clnProdutos -> {
                clnProdutos.vertical();
                clnProdutos.expanded();
                clnProdutos.disableProperty().bind(inEdition.not());
                clnProdutos.add(FormBox.create(boxInfosSolicitacao -> {
                    boxInfosSolicitacao.horizontal();
                    boxInfosSolicitacao.expanded();
                    boxInfosSolicitacao.setMaxHeight(50);
                    boxInfosSolicitacao.add(FormBox.create(boxInfos -> {
                        boxInfos.vertical();
                        boxInfos.title("Informações da Solicitação");
                        boxInfos.add(FormBox.create(boxLinha -> {
                            boxLinha.horizontal();
                            boxLinha.add(numeroSolicitacaoField.build(), dataSolicitacaoField.build(), tipoSolicitacaoField.build());
                        }));
                        boxInfos.add(FormBox.create(boxLinha -> {
                            boxLinha.horizontal();
                            boxLinha.add(qtdeProdutosField.build(), qtdeTotalItensField.build());
                        }));
                    }));
                    boxInfosSolicitacao.add(FormBox.create(boxObs -> {
                        boxObs.vertical();
                        boxObs.expanded();
                        boxObs.add(fieldObservacao.build());
                    }));

                }));
                clnProdutos.add(FormBox.create(boxTblGrade -> {
                    boxTblGrade.horizontal();
                    boxTblGrade.expanded();
                    boxTblGrade.add(FormBox.create(boxTbl -> {
                        boxTbl.horizontal();
                        boxTbl.expanded();
                        boxTbl.add(tblPrdutos.build());
                    }));
                    boxTblGrade.add(FormBox.create(boxGrade -> {
                        boxGrade.vertical();
                        boxGrade.add(tblGrade.build());
                    }));
                }));
                clnProdutos.add(FormBox.create(boxButtons -> {
                    boxButtons.horizontal();
                    boxButtons.add(FormBox.create(boxE -> {
                        boxE.horizontal();
                        boxE.add(btnAdicionarProdutos);
                    }));
                    boxButtons.add(FormBox.create(boxD -> {
                        boxD.horizontal();
                        boxD.alignment(Pos.CENTER_RIGHT);
                        boxD.add(btnCancelarSolicitacao, btnEnviarSolicitacao);
                    }));
                }));
            }));
        }));
    }

    private void criarSolicitacao() {
        limparCampos();
        inEdition.set(true);
        solicitacaoEscolhida = new SdSolicitacaoExp();
        solicitacaoEscolhida.setStatus(STATUS_CRIADA);
        solicitacaoEscolhida.setDtSolicitacao(LocalDate.now());
        solicitacaoEscolhida.setUsuario(Globals.getNomeUsuario());
        carregaCampos();
    }

    private void excluirSolicitacao() {
        if (solicitacaoEscolhida == null) return;

        if (solicitacaoEscolhida.getStatus().getCodigo() != STATUS_CRIADA.getCodigo() && solicitacaoEscolhida.getStatus().getCodigo() != STATUS_ENVIADA.getCodigo()) {
            MessageBox.create(message -> {
                message.message("Solicitação já foi vizualizada pelo expedição, impossível excluir");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }

        new FluentDao().delete(solicitacaoEscolhida);
        solicitacoesBean.remove(solicitacaoEscolhida);
        SysLogger.addSysDelizLog("Analise Solicitação de Peças", TipoAcao.EXCLUIR, String.valueOf(solicitacaoEscolhida.getId()), "Solicitação " + solicitacaoEscolhida.getId() + " excluída");
        tblSolicitacao.refresh();
        limparCampos();
        MessageBox.create(message -> {
            message.message("Solicitação excluída com sucesso");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }

    private void salvarSolicitacao() {

        if (tipoSolicitacaoField.value.getValue() == null) {
            MessageBox.create(message -> {
                message.message("Tipo de solicitação não informada");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
            return;
        }

        solicitacaoEscolhida.setStatus(STATUS_ENVIADA);
        solicitacaoEscolhida.setTipo(tipoSolicitacaoField.value.getValue());
        solicitacaoEscolhida.setObservacao(fieldObservacao.value.getValue());
        solicitacaoEscolhida.setDtSolicitacao(dataSolicitacaoField.value.getValue());
        if (solicitacaoEscolhida.getId() == null) solicitacaoEscolhida.setId("OS" + StringUtils.lpad(Globals.getProximoCodigo("SD_SOLICITACAO_EXP", "ID"), 4, "0"));

        solicitacaoEscolhida = new FluentDao().merge(solicitacaoEscolhida);
        SysLogger.addSysDelizLog("Analise Solicitação de Peças", TipoAcao.CADASTRAR, String.valueOf(solicitacaoEscolhida.getId()), "Solicitação " + solicitacaoEscolhida.getId() + " criada");
        for (SdItemSolicitacaoExp item : solicitacaoEscolhida.getItens()) {
            SysLogger.addSysDelizLog("Analise Solicitação de Peças", TipoAcao.CADASTRAR, String.valueOf(solicitacaoEscolhida.getId()),
                    item.getQtde() + " peças adicionadas ao item " + item.getProduto().getCodigo() + "-" + item.getProduto().getCor() + "/ " + item.getProduto().getTam() + " adicionado a solicitação " + solicitacaoEscolhida.getId());
        }

        inEdition.setValue(false);
        carregaCampos();
        if (solicitacoesBean.stream().noneMatch(it -> Objects.equals(it.getId(), solicitacaoEscolhida.getId())))
            solicitacoesBean.add(solicitacaoEscolhida);
    }

    private void editarSolicitacao() {
        if (solicitacaoEscolhida == null) return;

        if (solicitacaoEscolhida.getStatus().getCodigo() != STATUS_CRIADA.getCodigo() && solicitacaoEscolhida.getStatus().getCodigo() != STATUS_ENVIADA.getCodigo()) {
            MessageBox.create(message -> {
                message.message("Solicitação já foi vizualizada pelo expedição, impossível editar");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }

        inEdition.set(true);
    }

    private void adicionarProduto() {
        final ListProperty<VSdDadosProdutoCorTam> produtosToAddBean = new SimpleListProperty<>(FXCollections.observableArrayList());
        final ListProperty<VSdDadosProdutoCorTam> produtosAdicionadosBean = new SimpleListProperty<>(FXCollections.observableArrayList());

        final FormFieldMultipleFind<Produto> codigoFilter = FormFieldMultipleFind.create(Produto.class, field -> {
            field.title("Código");
            field.width(200);
            field.toUpper();
        });

        final FormFieldMultipleFind<Cor> corFilter = FormFieldMultipleFind.create(Cor.class, field -> {
            field.title("Cor");
            field.width(200);
            field.toUpper();
        });

        final FormFieldMultipleFind<Marca> marcaFilter = FormFieldMultipleFind.create(Marca.class, field -> {
            field.title("Marca");
            field.width(200);
            field.toUpper();
        });

        final FormFieldMultipleFind<Linha> linhaFilter = FormFieldMultipleFind.create(Linha.class, field -> {
            field.title("Linha");
            field.width(200);
            field.toUpper();
        });

        final FormFieldMultipleFind<Colecao> colecaoFilter = FormFieldMultipleFind.create(Colecao.class, field -> {
            field.title("Coleção");
            field.width(200);
            field.toUpper();
        });

        final FormTableView<VSdDadosProdutoCorTam> tblProdutosToAdd = FormTableView.create(VSdDadosProdutoCorTam.class, table -> {
            table.withoutHeader();
            table.items.bind(produtosToAddBean);
            table.expanded();
            table.tableview().setOnMouseClicked(evt -> {
                if (evt.getClickCount() == 2 && evt.getButton().equals(MouseButton.PRIMARY)) {
                    VSdDadosProdutoCorTam produto = (VSdDadosProdutoCorTam) table.selectedItem();
                    table.tableview().getSelectionModel().selectPrevious();
                    if (table.selectedItem() != null) table.tableview().getSelectionModel().selectFirst();
                    produtosToAddBean.remove(produto);
                    produtosAdicionadosBean.add(produto);
                    table.refresh();
                }
            });
            table.columns(
                    FormTableColumn.create(cln -> {
                        cln.title("Código");
                        cln.width(80);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosProdutoCorTam, VSdDadosProdutoCorTam>, ObservableValue<VSdDadosProdutoCorTam>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo()));
                    }).build(), /**/
                    FormTableColumn.create(cln -> {
                        cln.title("Cor");
                        cln.width(80);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosProdutoCorTam, VSdDadosProdutoCorTam>, ObservableValue<VSdDadosProdutoCorTam>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCor()));
                    }).build(), /**/
                    FormTableColumn.create(cln -> {
                        cln.title("Marca");
                        cln.width(80);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosProdutoCorTam, VSdDadosProdutoCorTam>, ObservableValue<VSdDadosProdutoCorTam>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodmarca()));
                    }).build(), /**/
                    FormTableColumn.create(cln -> {
                        cln.title("Coleção");
                        cln.width(80);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosProdutoCorTam, VSdDadosProdutoCorTam>, ObservableValue<VSdDadosProdutoCorTam>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodcol()));
                    }).build(), /**/
                    FormTableColumn.create(cln -> {
                        cln.title("Qtde");
                        cln.width(80);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosProdutoCorTam, VSdDadosProdutoCorTam>, ObservableValue<VSdDadosProdutoCorTam>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                    }).build() /**/
            );
        });

        final FormTableView<VSdDadosProdutoCorTam> tblProdutosAdicionados = FormTableView.create(VSdDadosProdutoCorTam.class, table -> {
            table.withoutHeader();
            table.items.bind(produtosAdicionadosBean);
            table.expanded();
            table.tableview().setOnMouseClicked(evt -> {
                if (evt.getClickCount() == 2 && evt.getButton().equals(MouseButton.PRIMARY)) {
                    VSdDadosProdutoCorTam produto = (VSdDadosProdutoCorTam) table.selectedItem();
                    table.tableview().getSelectionModel().selectPrevious();
                    if (table.selectedItem() != null) table.tableview().getSelectionModel().selectFirst();
                    produtosToAddBean.add(produto);
                    produtosAdicionadosBean.remove(produto);
                    table.refresh();
                }
            });
            table.columns(
                    FormTableColumn.create(cln -> {
                        cln.title("Código");
                        cln.width(80);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosProdutoCorTam, VSdDadosProdutoCorTam>, ObservableValue<VSdDadosProdutoCorTam>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo()));
                    }).build(), /**/
                    FormTableColumn.create(cln -> {
                        cln.title("Cor");
                        cln.width(80);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosProdutoCorTam, VSdDadosProdutoCorTam>, ObservableValue<VSdDadosProdutoCorTam>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCor()));
                    }).build(), /**/
                    FormTableColumn.create(cln -> {
                        cln.title("Marca");
                        cln.width(80);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosProdutoCorTam, VSdDadosProdutoCorTam>, ObservableValue<VSdDadosProdutoCorTam>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodmarca()));
                    }).build(), /**/
                    FormTableColumn.create(cln -> {
                        cln.title("Coleção");
                        cln.width(80);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosProdutoCorTam, VSdDadosProdutoCorTam>, ObservableValue<VSdDadosProdutoCorTam>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodcol()));
                    }).build(), /**/
                    FormTableColumn.create(cln -> {
                        cln.title("Qtde");
                        cln.width(80);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosProdutoCorTam, VSdDadosProdutoCorTam>, ObservableValue<VSdDadosProdutoCorTam>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                    }).build() /**/
            );
        });

        new Fragment().show(fg -> {

            fg.buttonsBox.getChildren().add(FormButton.create(btn -> {
                btn.title("Confimar");
                btn.addStyle("success");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                btn.setOnAction(evt -> {
                    criaItensSolicitacao(produtosAdicionadosBean);
                    fg.close();
                });
            }));

            fg.title("Adicionar Produtos");
            fg.size(1000.0, 600.0);

            fg.box.getChildren().add(FormBox.create(principal -> {
                principal.vertical();
                principal.add(FormBox.create(boxHeader -> {
                    boxHeader.vertical();
                    boxHeader.add(FormTitledPane.create(filter -> {
                        filter.filter();
                        filter.add(FormBox.create(boxFilter -> {
                            boxFilter.vertical();
                            boxFilter.add(FormBox.create(linha1 -> {
                                linha1.horizontal();
                                linha1.add(codigoFilter.build(), corFilter.build(), linhaFilter.build());
                            }));
                            boxFilter.add(FormBox.create(linha2 -> {
                                linha2.horizontal();
                                linha2.add(colecaoFilter.build(), marcaFilter.build());
                            }));
                        }));
                        filter.find.setOnAction(evt -> {
                            new RunAsyncWithOverlay(fg).exec(task -> {
                                produtosToAddBean.set(FXCollections.observableArrayList(
                                        buscarProdutos(
                                                codigoFilter.objectValues.stream().map(Produto::getCodigo).toArray(),
                                                corFilter.objectValues.stream().map(Cor::getCor).toArray(),
                                                colecaoFilter.objectValues.stream().map(Colecao::getCodigo).toArray(),
                                                marcaFilter.objectValues.stream().map(Marca::getCodigo).toArray(),
                                                linhaFilter.objectValues.stream().map(Linha::getCodigo).toArray()
                                        )));
                                return ReturnAsync.OK.value;
                            }).addTaskEndNotification(taskReturn -> {
                                if (taskReturn.equals(ReturnAsync.OK.value)) {
                                    produtosToAddBean.set(FXCollections.observableArrayList(
                                            convertToProdutoCorUnico(produtosToAddBean.stream().filter(it ->
                                                            itensSolicitacaoBean.stream().noneMatch(eb -> it.
                                                                    getCodigo().equals(eb.getProduto().getCodigo()) &&
                                                                    it.getCor().equals(eb.getProduto().getCor())))
                                                    .collect(Collectors.toList()))));
                                    tblProdutosToAdd.refresh();
                                }
                            });

                        });
                        filter.clean.setOnAction(evt -> {
                            codigoFilter.clear();
                            corFilter.clear();
                            colecaoFilter.clear();
                            marcaFilter.clear();
                        });
                    }));
                }));
                principal.add(FormBox.create(boxTbls -> {
                    boxTbls.horizontal();
                    boxTbls.expanded();
                    boxTbls.add(tblProdutosToAdd.build());
                    boxTbls.add(FormBox.create(boxBtns -> {
                        boxBtns.vertical();
                        boxBtns.expanded();
                        boxBtns.alignment(Pos.CENTER);
                        boxBtns.add(
                                FormButton.create(btn -> {
                                    btn.addStyle("success-outline");
                                    btn.setText(">>");
                                    btn.tooltip("Adicionar todos");
                                    btn.width(40);
                                    btn.setOnAction(evt -> {
                                        produtosAdicionadosBean.addAll(produtosToAddBean);
                                        produtosToAddBean.clear();
                                        tblProdutosAdicionados.refresh();
                                        tblProdutosToAdd.refresh();
                                    });
                                })
                        );
                        boxBtns.add(
                                FormButton.create(btn -> {
                                    btn.addStyle("success-outline");
                                    btn.setText(">");
                                    btn.tooltip("Adicionar selecionado");
                                    btn.width(40);
                                    btn.setOnAction(evt -> {
                                        if (tblProdutosToAdd.selectedItem() != null) {
                                            VSdDadosProdutoCorTam produto = tblProdutosToAdd.selectedItem();
                                            tblProdutosToAdd.tableview().getSelectionModel().selectPrevious();
                                            produtosToAddBean.remove(produto);
                                            produtosAdicionadosBean.add(produto);
                                            tblProdutosAdicionados.refresh();
                                            tblProdutosToAdd.refresh();
                                        }
                                    });
                                })
                        );
                        boxBtns.add(
                                FormButton.create(btn -> {
                                    btn.addStyle("danger-outline");
                                    btn.setText("<");
                                    btn.tooltip("Remover selecionado");
                                    btn.width(40);
                                    btn.setOnAction(evt -> {
                                        VSdDadosProdutoCorTam produto = tblProdutosAdicionados.selectedItem();
                                        tblProdutosAdicionados.tableview().getSelectionModel().selectPrevious();
                                        produtosToAddBean.add(produto);
                                        produtosAdicionadosBean.remove(produto);
                                        tblProdutosAdicionados.refresh();
                                        tblProdutosToAdd.refresh();
                                    });
                                })
                        );
                        boxBtns.add(
                                FormButton.create(btn -> {
                                    btn.addStyle("danger-outline");
                                    btn.setText("<<");
                                    btn.tooltip("Remover todos");
                                    btn.width(40);
                                    btn.setOnAction(evt -> {
                                        produtosToAddBean.addAll(produtosAdicionadosBean);
                                        produtosAdicionadosBean.clear();
                                        tblProdutosAdicionados.refresh();
                                        tblProdutosToAdd.refresh();
                                    });
                                })
                        );
                    }));
                    boxTbls.add(tblProdutosAdicionados.build());
                }));
            }));

        });
    }

    private void cancelarSolicitacao() {
        solicitacaoEscolhida = null;
        limparCampos();
        inEdition.set(false);
    }

    private void carregaCampos() {
        numeroSolicitacaoField.value.setValue(solicitacaoEscolhida.getId() == null ? "" : solicitacaoEscolhida.getId());
        dataSolicitacaoField.value.setValue(solicitacaoEscolhida.getDtSolicitacao());
        if (tipoSolicitacaoField.value.getValue() == null) tipoSolicitacaoField.value.setValue(solicitacaoEscolhida.getTipo());
        qtdeProdutosField.value.setValue(String.valueOf(solicitacaoEscolhida.getItens().stream().map(it -> it.getProduto().getCodigo() + it.getProduto().getCor()).distinct().count()));
        qtdeTotalItensField.value.setValue(String.valueOf(solicitacaoEscolhida.getItens().stream().mapToInt(SdItemSolicitacaoExp::getQtde).sum()));
        fieldObservacao.value.setValue(solicitacaoEscolhida.getObservacao());
    }

    private void limparCampos() {
        numeroSolicitacaoField.clear();
        dataSolicitacaoField.value.setValue(null);
        tipoSolicitacaoField.clear();
        tipoSolicitacaoField.clear();
        qtdeProdutosField.clear();
        qtdeTotalItensField.clear();
        fieldObservacao.clear();
        tblGrade.clear();
        tblPrdutos.clear();
        itensSolicitacaoBean.clear();
        gradeBean.clear();
    }

    private void carregarSolicitacoesPendentes() {
        solicitacoesBean.set(FXCollections.observableArrayList((List<SdSolicitacaoExp>) new FluentDao()
                .selectFrom(SdSolicitacaoExp.class)
                .where(it -> it
                        .equal("usuario", Globals.getNomeUsuario())
                        .isIn("status.codigo", new String[]{"1", "2"}))
                .resultList()));
    }

    private void criaItensSolicitacao(List<VSdDadosProdutoCorTam> produtosAdicionadosBean) {
        produtosAdicionadosBean.stream().filter(it ->
                itensSolicitacaoBean.stream().noneMatch(ob -> ob
                        .getProduto().getCodigo().equals(it.getCodigo()) &&
                        ob.getProduto().getCor().equals(it.getCor())
                )
        ).forEach(item -> itensSolicitacaoBean.add(new SdItemSolicitacaoExp(item, 0, solicitacaoEscolhida)));
        tblPrdutos.refresh();
    }

    private List<VSdDadosProdutoCorTam> buscarProdutos(Object[] codigos, Object[] cor, Object[] colecao, Object[] marca, Object[] linha) {
        List<VSdDadosProdutoCorTam> produtos = (List<VSdDadosProdutoCorTam>) new FluentDao().selectFrom(VSdDadosProdutoCorTam.class).where(it -> it
                .isIn("codigo", codigos, TipoExpressao.AND, when -> codigos.length > 0)
                .isIn("cor", cor, TipoExpressao.AND, when -> cor.length > 0)
                .isIn("codmarca", marca, TipoExpressao.AND, when -> marca.length > 0)
                .isIn("codcol", colecao, TipoExpressao.AND, when -> colecao.length > 0)
                .isIn("linha", linha, TipoExpressao.AND, when -> linha.length > 0)
        ).resultList();

        return convertToProdutoCorUnico(produtos);
    }

    private List<VSdDadosProdutoCorTam> convertToProdutoCorUnico(List<VSdDadosProdutoCorTam> produtos) {
        List<VSdDadosProdutoCorTam> retorno = new ArrayList<>();

        Map<String, Map<String, List<VSdDadosProdutoCorTam>>> collect = produtos.stream().collect(Collectors.groupingBy(VSdDadosProdutoCorTam::getCodigo, Collectors.groupingBy(VSdDadosProdutoCorTam::getCor)));

        collect.forEach((codigo, mapCores) -> mapCores.forEach((codCor, list) -> retorno.add(new VSdDadosProdutoCorTam(codigo, codCor, list))));

        retorno.sort(Comparator.comparing(VSdDadosProdutoCorTam::getCodigo).thenComparing(VSdDadosProdutoCorTam::getCor));
        return retorno;
    }

    private void carregaGradeProduto(SdItemSolicitacaoExp newValue) {
        tblGrade.clear();

        List<VSdDadosProdutoCorTam> grade = (List<VSdDadosProdutoCorTam>) new FluentDao().selectFrom(VSdDadosProdutoCorTam.class).where(it -> it
                        .equal("codigo", newValue.getProduto().getCodigo())
                        .equal("cor", newValue.getProduto().getCor()))
                .resultList();

        if (grade != null) {
            grade.sort((o1, o2) -> SortUtils.sortTamanhos(o1.getTam(), o2.getTam()));
            gradeBean.set(FXCollections.observableArrayList(grade));
        }
    }

    private void atualizaQuantidadeItem(VSdDadosProdutoCorTam item, String valor) {
        SdItemSolicitacaoExp sdItemSolicitacaoExp = getItemSolicitacao(item).orElse(null);
        if ((valor.equals("") || valor.equals("0")) && sdItemSolicitacaoExp == null) {
            return;
        } else if (sdItemSolicitacaoExp == null) {
            solicitacaoEscolhida.getItens().add(new SdItemSolicitacaoExp(item, Integer.parseInt(valor), solicitacaoEscolhida));
        } else if (valor.equals("") || valor.equals("0")) {
            new FluentDao().delete(sdItemSolicitacaoExp);
            solicitacaoEscolhida.getItens().remove(sdItemSolicitacaoExp);
        } else {
            sdItemSolicitacaoExp.setQtde(Integer.parseInt(valor));
        }

        qtdeProdutosField.value.setValue(String.valueOf(solicitacaoEscolhida.getItens().stream().map(it -> it.getProduto().getCodigo() + it.getProduto().getCor()).distinct().count()));
        qtdeTotalItensField.value.setValue(String.valueOf(solicitacaoEscolhida.getItens().stream().mapToInt(SdItemSolicitacaoExp::getQtde).sum()));
        tblPrdutos.refresh();
    }

    private Optional<SdItemSolicitacaoExp> getItemSolicitacao(VSdDadosProdutoCorTam item) {
        return solicitacaoEscolhida.getItens().stream().filter(it ->
                it.getProduto().getCodigo().equals(item.getCodigo()) &&
                        it.getProduto().getCor().equals(item.getCor()) &&
                        it.getProduto().getTam().equals(item.getTam())
        ).findFirst();
    }

    private void removerItem(SdItemSolicitacaoExp item) {

        if (tblPrdutos.selectedItem() != null && tblPrdutos.selectedItem().equals(item)) tblGrade.clear();

        itensSolicitacaoBean.removeIf(it -> it
                .getProduto().getCodigo().equals(item.getProduto().getCodigo()) &&
                it.getProduto().getCor().equals(item.getProduto().getCor()));

        itensSolicitacaoBean.remove(item);

    }

    private void carregaSolicitacao(SdSolicitacaoExp newValue) {
        solicitacaoEscolhida = newValue;
        itensSolicitacaoBean.clear();
        carregaCampos();
        criaItensSolicitacao(convertToProdutoCorUnico(solicitacaoEscolhida.getItens().stream().map(SdItemSolicitacaoExp::getProduto).collect(Collectors.toList())));
    }

}
