package sysdeliz2.views.pcp;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import sysdeliz2.controllers.fxml.SceneMainController;
import sysdeliz2.controllers.views.pcp.AnaliseBaixaAutomaticaController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.sysdeliz.SdMateriaisFaltantes;
import sysdeliz2.models.sysdeliz.SdReservaMateriais;
import sysdeliz2.models.ti.*;
import sysdeliz2.models.view.*;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

public class AnaliseBaixaAutomaticaView extends AnaliseBaixaAutomaticaController {
    
    // <editor-fold defaultstate="collapsed" desc="Declaração: View">
    private final VBox listagemTab = (VBox) super.tabs.getTabs().get(0).getContent();
    private final VBox manutencaoTab = (VBox) super.tabs.getTabs().get(1).getContent();
    private final VBox faltasTab = (VBox) super.tabs.getTabs().get(2).getContent();
    private final BooleanProperty isColetaBtn = new SimpleBooleanProperty(false);
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Filtros O.F.">
    private final FormFieldSegmentedButton<String> filtroStatusCorteField = FormFieldSegmentedButton.create(field -> {
        field.title("Status Corte");
        field.options(
                field.option("Todos", "TODOS", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("Baixado", "BAIXADO", FormFieldSegmentedButton.Style.SUCCESS),
                field.option("Parcial", "PARCIAL", FormFieldSegmentedButton.Style.WARNING),
                field.option("Não Baixado", "NAO BAIXADO", FormFieldSegmentedButton.Style.DANGER)
        );
        field.select(0);
    });
    private final FormFieldSegmentedButton<String> filtroStatusCosturaField = FormFieldSegmentedButton.create(field -> {
        field.title("Status Costura");
        field.options(
                field.option("Todos", "TODOS", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("Baixado", "BAIXADO", FormFieldSegmentedButton.Style.SUCCESS),
                field.option("Parcial", "PARCIAL", FormFieldSegmentedButton.Style.WARNING),
                field.option("Não Baixado", "NAO BAIXADO", FormFieldSegmentedButton.Style.DANGER)
        );
        field.select(0);
    });
    private final FormFieldSegmentedButton<String> filtroStatusAcabamentoField = FormFieldSegmentedButton.create(field -> {
        field.title("Status Acabamento");
        field.options(
                field.option("Todos", "TODOS", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("Baixado", "BAIXADO", FormFieldSegmentedButton.Style.SUCCESS),
                field.option("Parcial", "PARCIAL", FormFieldSegmentedButton.Style.WARNING),
                field.option("Não Baixado", "NAO BAIXADO", FormFieldSegmentedButton.Style.DANGER)
        );
        field.select(0);
    });
    private final FormFieldSegmentedButton<String> filtroStatusOutrosField = FormFieldSegmentedButton.create(field -> {
        field.title("Status Outros");
        field.options(
                field.option("Todos", "TODOS", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("Baixado", "BAIXADO", FormFieldSegmentedButton.Style.SUCCESS),
                field.option("Parcial", "PARCIAL", FormFieldSegmentedButton.Style.WARNING),
                field.option("Não Baixado", "NAO BAIXADO", FormFieldSegmentedButton.Style.DANGER)
        );
        field.select(0);
    });
    private final FormFieldSegmentedButton<String> filtroLavanderia = FormFieldSegmentedButton.create(field -> {
        field.title("Lavanderia?");
        field.options(
                field.option("Ambos", "AMBOS"),
                field.option("Sim", "true"),
                field.option("Não", "false")
        );
        field.select(0);
    });
    private final FormFieldText filtroOfField = FormFieldText.create(field -> {
        field.title("OF");
        field.width(80.0);
    });
    private final FormFieldMultipleFind<CadFluxo> filtroSetor = FormFieldMultipleFind.create(CadFluxo.class, field -> {
        field.title("Setor O.F.");
        field.toUpper();
    });
    private final FormFieldMultipleFind<Produto> filtroProduto = FormFieldMultipleFind.create(Produto.class, field -> {
        field.title("Produto");
        field.toUpper();
    });
    private final FormFieldMultipleFind<Marca> filtroMarca = FormFieldMultipleFind.create(Marca.class, field -> {
        field.title("Marca");
        field.toUpper();
    });
    private final FormFieldMultipleFind<Colecao> filtroColecao = FormFieldMultipleFind.create(Colecao.class, field -> {
        field.title("Coleção Prod.");
        field.toUpper();
    });
    private final FormFieldMultipleFind<Linha> filtroLinha = FormFieldMultipleFind.create(Linha.class, field -> {
        field.title("Linha Prod.");
        field.toUpper();
    });
    private final FormFieldMultipleFind<Familia> filtroFamilia = FormFieldMultipleFind.create(Familia.class, field -> {
        field.title("Família Prod.");
        field.toUpper();
    });
    private final FormFieldMultipleFind<Entidade> filtroFaccao = FormFieldMultipleFind.create(Entidade.class, field -> {
        field.title("Facção");
        field.toUpper();
    });
    private final FormFieldSegmentedButton<String> filtroPais = FormFieldSegmentedButton.create(field -> {
        field.title("País");
        field.options(
                field.option("Ambos", "A", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("Brasil", "BR", FormFieldSegmentedButton.Style.SUCCESS),
                field.option("Paraguai", "PY", FormFieldSegmentedButton.Style.DANGER)
        );
        field.select(0);
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Fields Manutenção">
    private final FormFieldText ofNumero = FormFieldText.create().title("Número").width(90);
    private final FormFieldText ofPeriodo = FormFieldText.create().title("Período").width(70);
    private final FormFieldText ofMarca = FormFieldText.create().title("Marca").width(100);
    private final FormFieldText ofProduto = FormFieldText.create().title("Produto").width(300);
    private final FormFieldText ofColecao = FormFieldText.create().title("Coleção").width(150);
    private final FormFieldText ofSetorAtual = FormFieldText.create().title("Setor Atual").width(150);
    private final FormFieldText ofFaccao = FormFieldText.create().title("Facção").width(400);
    private final FormFieldText ofFluxo = FormFieldText.create().title("Fluxo").width(300);
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Filtros Faltantes">
    private final FormFieldText filtroFaltNumero = FormFieldText.create(field -> {
        field.title("O.F.");
    });
    private final FormFieldMultipleFind<CadFluxo> filtroFaltSetor = FormFieldMultipleFind.create(CadFluxo.class, field -> {
        field.title("Setor Consumo");
    });
    private final FormFieldMultipleFind<Material> filtroFaltMaterial = FormFieldMultipleFind.create(Material.class, field -> {
        field.title("Material");
        field.width(450.0);
    });
    private final FormFieldMultipleFind<Cor> filtroFaltCor = FormFieldMultipleFind.create(Cor.class, field -> {
        field.title("Cor Material");
        field.width(280.0);
    });
    private final FormFieldMultipleFind<TabPrz> filtroFaltPeridodo = FormFieldMultipleFind.create(TabPrz.class, field -> {
        field.title("Período");
    });
    private final FormFieldSegmentedButton<String> filtroFaltStatus = FormFieldSegmentedButton.create(filtroFaltStatus -> {
        filtroFaltStatus.title("Status");
        filtroFaltStatus.options(
                filtroFaltStatus.option("Todos", "T", FormFieldSegmentedButton.Style.SECUNDARY),
                filtroFaltStatus.option("Aberto", "A", FormFieldSegmentedButton.Style.DANGER),
                filtroFaltStatus.option("Com Compra", "C", FormFieldSegmentedButton.Style.WARNING),
                filtroFaltStatus.option("Aberto + Compra", "D", FormFieldSegmentedButton.Style.PRIMARY),
                filtroFaltStatus.option("Resolvido", "R", FormFieldSegmentedButton.Style.SUCCESS)
        );
        filtroFaltStatus.select(0);
    });
    private final FormFieldSegmentedButton<String> filtroFaltTipoProduto = FormFieldSegmentedButton.create(field -> {
        field.title("Tipo Produto");
        field.options(
                field.option("Ambos", "T", FormFieldSegmentedButton.Style.SECUNDARY),
                field.option("Jeans", "J", FormFieldSegmentedButton.Style.SUCCESS),
                field.option("Moda", "M", FormFieldSegmentedButton.Style.WARNING)
        );
        field.select(0);
    });
    private final FormFieldSegmentedButton<String> filtroFaltSubstituto = FormFieldSegmentedButton.create(filtroFaltSubstituto -> {
        filtroFaltSubstituto.title("Substituto");
        filtroFaltSubstituto.options(
                filtroFaltSubstituto.option("Ambos", "T", FormFieldSegmentedButton.Style.SECUNDARY),
                filtroFaltSubstituto.option("Sim", "true", FormFieldSegmentedButton.Style.SUCCESS),
                filtroFaltSubstituto.option("Não", "false", FormFieldSegmentedButton.Style.DANGER)
        );
        filtroFaltSubstituto.select(0);
    });
    private final FormFieldSegmentedButton<String> filtroFaltPais = FormFieldSegmentedButton.create(field -> {
        field.title("País");
        field.options(
                field.option("Ambos", "A", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("Brasil", "BR", FormFieldSegmentedButton.Style.SUCCESS),
                field.option("Paraguai", "PY", FormFieldSegmentedButton.Style.DANGER)
        );
        field.select(0);
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Tables">
    // <editor-fold defaultstate="collapsed" desc="Table OFS">
    private final FormTableColumn<VSdDadosOfPendente, VSdDadosOfPendente> colAcoesOf = FormTableColumn.create(cln -> {
        cln.title("Ações");
        cln.width(95);
        cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
        cln.order(Comparator.comparing(o -> ((VSdDadosOfPendente) o).getId().getNumero()));
        cln.format(param -> {
            return new TableCell<VSdDadosOfPendente, VSdDadosOfPendente>() {
                final HBox boxButtonsRow = new HBox(3);
                final Button btnVisualizarBaixa = FormButton.create(btn -> {
                    btn.icon(ImageUtils.getIconButton(ImageUtils.ButtonIcon.FIND, ImageUtils.IconSize._16));
                    btn.tooltip("Visualizar Baixas OF");
                    btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                    btn.addStyle("primary").addStyle("xs");
                });
                final Button btnLiberarColeta = FormButton.create(btn -> {
                    btn.icon(ImageUtils.getIconButton(ImageUtils.ButtonIcon.SEND, ImageUtils.IconSize._16));
                    btn.tooltip("Liberar OF para coleta");
                    btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                    btn.addStyle("success").addStyle("xs");
                });
                
                @Override
                protected void updateItem(VSdDadosOfPendente item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);
                    if (item != null && !empty) {
                        
                        btnVisualizarBaixa.setOnAction(evt -> {
                            loadBaixasOf(item);
                        });
                        btnLiberarColeta.setOnAction(evt -> {
                            sendColetaOf(item);
                            MessageBox.create(message -> {
                                message.message("OF enviada para programação de coleta.");
                                message.type(MessageBox.TypeMessageBox.CONFIRM);
                                message.position(Pos.TOP_RIGHT);
                                message.notification();
                            });
                            JPAUtils.getEntityManager().refresh(item);
                        });
                        btnLiberarColeta.disableProperty().bind(item.liberadocoletaProperty());
                        boxButtonsRow.getChildren().clear();
                        boxButtonsRow.getChildren().add(btnVisualizarBaixa);
                        boxButtonsRow.getChildren().add(btnLiberarColeta);
                        setGraphic(boxButtonsRow);
                    }
                }
            };
        });
    });
    private final FormTableColumn<VSdDadosOfPendente, String> colStatusCorteOf = FormTableColumn.create(cln -> {
        cln.title("Corte");
        cln.width(40);
        cln.alignment(FormTableColumn.Alignment.CENTER);
        cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCort()));
        cln.format(param -> {
            return new TableCell<VSdDadosOfPendente, String>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);
                    if (item != null && !empty) {
                        setGraphic(item.equals("SEM MAT") ? ImageUtils.getIcon(ImageUtils.Icon.CONSUMO_SEM, ImageUtils.IconSize._16) :
                                item.equals("BAIXADO") ? ImageUtils.getIcon(ImageUtils.Icon.CONSUMO_BAIXADO, ImageUtils.IconSize._16) :
                                        item.equals("PARCIAL") ? ImageUtils.getIcon(ImageUtils.Icon.CONSUMO_PARCIAL, ImageUtils.IconSize._16) :
                                                ImageUtils.getIcon(ImageUtils.Icon.CONSUMO_ABERTO, ImageUtils.IconSize._16));
                    }
                }
            };
        });
    });
    private final FormTableColumn<VSdDadosOfPendente, String> colStatusCosturaOf = FormTableColumn.create(cln -> {
        cln.title("Costu.");
        cln.width(40);
        cln.alignment(FormTableColumn.Alignment.CENTER);
        cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCost()));
        cln.format(param -> {
            return new TableCell<VSdDadosOfPendente, String>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);
                    if (item != null && !empty) {
                        setGraphic(item.equals("SEM MAT") ? ImageUtils.getIcon(ImageUtils.Icon.CONSUMO_SEM, ImageUtils.IconSize._16) :
                                item.equals("BAIXADO") ? ImageUtils.getIcon(ImageUtils.Icon.CONSUMO_BAIXADO, ImageUtils.IconSize._16) :
                                        item.equals("PARCIAL") ? ImageUtils.getIcon(ImageUtils.Icon.CONSUMO_PARCIAL, ImageUtils.IconSize._16) :
                                                ImageUtils.getIcon(ImageUtils.Icon.CONSUMO_ABERTO, ImageUtils.IconSize._16));
                    }
                }
            };
        });
    });
    private final FormTableColumn<VSdDadosOfPendente, String> colStatusAcabamentoOf = FormTableColumn.create(cln -> {
        cln.title("Acab.");
        cln.width(40);
        cln.alignment(FormTableColumn.Alignment.CENTER);
        cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getAcab()));
        cln.format(param -> {
            return new TableCell<VSdDadosOfPendente, String>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);
                    if (item != null && !empty) {
                        setGraphic(item.equals("SEM MAT") ? ImageUtils.getIcon(ImageUtils.Icon.CONSUMO_SEM, ImageUtils.IconSize._16) :
                                item.equals("BAIXADO") ? ImageUtils.getIcon(ImageUtils.Icon.CONSUMO_BAIXADO, ImageUtils.IconSize._16) :
                                        item.equals("PARCIAL") ? ImageUtils.getIcon(ImageUtils.Icon.CONSUMO_PARCIAL, ImageUtils.IconSize._16) :
                                                ImageUtils.getIcon(ImageUtils.Icon.CONSUMO_ABERTO, ImageUtils.IconSize._16));
                    }
                }
            };
        });
    });
    private final FormTableColumn<VSdDadosOfPendente, String> colStatusOutrosOf = FormTableColumn.create(cln -> {
        cln.title("Outros");
        cln.width(40);
        cln.alignment(FormTableColumn.Alignment.CENTER);
        cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getAplic()));
        cln.format(param -> {
            return new TableCell<VSdDadosOfPendente, String>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);
                    if (item != null && !empty) {
                        setGraphic(item.equals("SEM MAT") ? ImageUtils.getIcon(ImageUtils.Icon.CONSUMO_SEM, ImageUtils.IconSize._16) :
                                item.equals("BAIXADO") ? ImageUtils.getIcon(ImageUtils.Icon.CONSUMO_BAIXADO, ImageUtils.IconSize._16) :
                                        item.equals("PARCIAL") ? ImageUtils.getIcon(ImageUtils.Icon.CONSUMO_PARCIAL, ImageUtils.IconSize._16) :
                                                ImageUtils.getIcon(ImageUtils.Icon.CONSUMO_ABERTO, ImageUtils.IconSize._16));
                    }
                }
            };
        });
    });
    private final FormTableColumnGroup<VSdDadosOfPendente, VSdDadosOfPendente> groupColMateriais = FormTableColumnGroup.createGroup(group -> {
        group.title("Materiais");
        group.addColumn(colStatusCorteOf);
        group.addColumn(colStatusCosturaOf);
        group.addColumn(colStatusAcabamentoOf);
        group.addColumn(colStatusOutrosOf);
    });
    private final FormTableColumn<VSdDadosOfPendente, String> colNumeroOf = FormTableColumn.create(cln -> {
        cln.title("O.F.");
        cln.width(80);
        cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getNumero()));
    });
    private final FormTableColumn<VSdDadosOfPendente, String> colPeriodoOf = FormTableColumn.create(cln -> {
        cln.title("Período");
        cln.width(50);
        cln.alignment(FormTableColumn.Alignment.CENTER);
        cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPeriodo()));
    });
    private final FormTableColumn<VSdDadosOfPendente, Integer> colQtdeOf = FormTableColumn.create(cln -> {
        cln.title("Qtde Pend.");
        cln.width(60);
        cln.alignment(FormTableColumn.Alignment.CENTER);
        cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
    });
    private final FormTableColumn<VSdDadosOfPendente, String> colMarcaOf = FormTableColumn.create(cln -> {
        cln.title("Marca");
        cln.width(40);
        cln.alignment(FormTableColumn.Alignment.CENTER);
        cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getCodMarca()));
    });
    private final FormTableColumn<VSdDadosOfPendente, VSdDadosProduto> colProdutoOf = FormTableColumn.create(cln -> {
        cln.title("Produto");
        cln.width(250);
        cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo()));
    });
    private final FormTableColumn<VSdDadosOfPendente, String> colColecaoOf = FormTableColumn.create(cln -> {
        cln.title("Coleção");
        cln.width(60);
        cln.alignment(FormTableColumn.Alignment.CENTER);
        cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getCodCol()));
    });
    private final FormTableColumn<VSdDadosOfPendente, String> colLinhaOf = FormTableColumn.create(cln -> {
        cln.title("Linha");
        cln.width(120);
        cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getLinha()));
    });
    private final FormTableColumn<VSdDadosOfPendente, String> colFamiliaOf = FormTableColumn.create(cln -> {
        cln.title("Família");
        cln.width(100);
        cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getFamilia()));
    });
    private final FormTableColumnGroup<VSdDadosOfPendente, VSdDadosOfPendente> groupColProduto = FormTableColumnGroup.createGroup(group -> {
        group.title("Produto");
        group.addColumn(colMarcaOf);
        group.addColumn(colProdutoOf);
        group.addColumn(colColecaoOf);
        group.addColumn(colLinhaOf);
        group.addColumn(colFamiliaOf);
    });
    private final FormTableColumn<VSdDadosOfPendente, CadFluxo> colSetorOf = FormTableColumn.create(cln -> {
        cln.title("Setor");
        cln.width(150);
        cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getSetor()));
    });
    private final FormTableColumn<VSdDadosOfPendente, Entidade> colFaccaoOf = FormTableColumn.create(cln -> {
        cln.title("Facção");
        cln.width(300);
        cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getFaccao()));
    });
    private final FormTableColumn<VSdDadosOfPendente, String> colParteOf = FormTableColumn.create(cln -> {
        cln.title("Parte");
        cln.width(40);
        cln.alignment(FormTableColumn.Alignment.CENTER);
        cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getParte()));
    });
    private final FormTableColumn<VSdDadosOfPendente, String> colPaisOf = FormTableColumn.create(cln -> {
        cln.title("País");
        cln.width(35);
        cln.alignment(FormTableColumn.Alignment.CENTER);
        cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPais()));
    });
    private final FormTableColumn<VSdDadosOfPendente, Boolean> colLavanderiaOf = FormTableColumn.create(cln -> {
        cln.title("Lava.");
        cln.width(40);
        cln.alignment(FormTableColumn.Alignment.CENTER);
        cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(param.getValue().isLavand()));
        cln.format(param -> {
            return new TableCell<VSdDadosOfPendente, Boolean>() {
                @Override
                protected void updateItem(Boolean item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);
                    if (item != null && !empty) {
                        setGraphic(item ? ImageUtils.getIcon(ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._16) : ImageUtils.getIcon(ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                    }
                }
            };
        });
    });
    private final FormTableColumn<VSdDadosOfPendente, CadFluxo> colProximoSetorOf = FormTableColumn.create(cln -> {
        cln.title("Próximo Setor");
        cln.width(150);
        cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getProxsetor()));
    });
    private final FormTableColumn<VSdDadosOfPendente, String> colFluxoOf = FormTableColumn.create(cln -> {
        cln.title("Fluxo");
        cln.width(40);
        cln.alignment(FormTableColumn.Alignment.CENTER);
        cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getFluxo()));
    });
    private final FormTableColumn<VSdDadosOfPendente, String> colDescFluxoOf = FormTableColumn.create(cln -> {
        cln.title("Desc. Fluxo");
        cln.width(250);
        cln.fontSize(10);
        cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDescfluxo()));
    });
    private final FormTableColumnGroup<VSdDadosOfPendente, VSdDadosOfPendente> groupColRoteiro = FormTableColumnGroup.createGroup(group -> {
        group.title("Roteiro");
        group.addColumn(colSetorOf);
        group.addColumn(colFaccaoOf);
        group.addColumn(colParteOf);
        group.addColumn(colPaisOf);
        group.addColumn(colLavanderiaOf);
        group.addColumn(colProximoSetorOf);
        group.addColumn(colFluxoOf);
        group.addColumn(colDescFluxoOf);
    });
    private final FormTableView<VSdDadosOfPendente> tblOfs = FormTableView.create(VSdDadosOfPendente.class, table -> {
        table.expanded();
        table.title("OFs Pendentes");
        table.items.bind(super.ofsPendentes);
        table.columns(colAcoesOf.build(), groupColMateriais.build(), colNumeroOf.build(), colPeriodoOf.build(), colQtdeOf.build(), groupColProduto.build(), groupColRoteiro.build());
        table.indices(
                table.indice(ImageUtils.getIcon(ImageUtils.Icon.CONSUMO_ABERTO, ImageUtils.IconSize._24), "Materiais ainda não baixados"),
                table.indice(ImageUtils.getIcon(ImageUtils.Icon.CONSUMO_PARCIAL, ImageUtils.IconSize._24), "Materiais baixados parcialmento"),
                table.indice(ImageUtils.getIcon(ImageUtils.Icon.CONSUMO_BAIXADO, ImageUtils.IconSize._24), "Materiais baixados"),
                table.indice(ImageUtils.getIcon(ImageUtils.Icon.CONSUMO_SEM, ImageUtils.IconSize._24), "Sem materiais de consumo")
        );
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Table RESERVAS OF">
    private final FormTableColumn<SdReservaMateriais, String> clnDhRequisicao = FormTableColumn.create(cln -> {
        cln.title("Dt. Req.");
        cln.width(95);
        cln.value((Callback<TableColumn.CellDataFeatures<SdReservaMateriais, SdReservaMateriais>, ObservableValue<SdReservaMateriais>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getDhRequisicao()));
        cln.format(param -> {
            return new TableCell<SdReservaMateriais, LocalDateTime>() {
                @Override
                protected void updateItem(LocalDateTime item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    
                    if (item != null && !empty) {
                        setText(StringUtils.toDateTimeFormat(item));
                    }
                }
            };
        });
    });
    private final FormTableColumn<SdReservaMateriais, String> clnOf = FormTableColumn.create(cln -> {
        cln.title("O.F.");
        cln.width(45);
        cln.value((Callback<TableColumn.CellDataFeatures<SdReservaMateriais, SdReservaMateriais>, ObservableValue<SdReservaMateriais>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getNumero()));
    });
    private final FormTableColumn<SdReservaMateriais, Material> clnMaterial = FormTableColumn.create(cln -> {
        cln.title("Material");
        cln.width(300);
        cln.value((Callback<TableColumn.CellDataFeatures<SdReservaMateriais, SdReservaMateriais>, ObservableValue<SdReservaMateriais>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getInsumo()));
    });
    private final FormTableColumn<SdReservaMateriais, Cor> clnCorI = FormTableColumn.create(cln -> {
        cln.title("Cor Mat.");
        cln.width(150);
        cln.value((Callback<TableColumn.CellDataFeatures<SdReservaMateriais, SdReservaMateriais>, ObservableValue<SdReservaMateriais>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCorI()));
    });
    private final FormTableColumn<SdReservaMateriais, String> clnLote = FormTableColumn.create(cln -> {
        cln.title("Lote");
        cln.width(65);
        cln.alignment(FormTableColumn.Alignment.CENTER);
        cln.value((Callback<TableColumn.CellDataFeatures<SdReservaMateriais, SdReservaMateriais>, ObservableValue<SdReservaMateriais>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getLote()));
    });
    private final FormTableColumn<SdReservaMateriais, String> clnDeposito = FormTableColumn.create(cln -> {
        cln.title("Dep.");
        cln.width(35);
        cln.alignment(FormTableColumn.Alignment.CENTER);
        cln.value((Callback<TableColumn.CellDataFeatures<SdReservaMateriais, SdReservaMateriais>, ObservableValue<SdReservaMateriais>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getDeposito()));
    });
    private final FormTableColumn<SdReservaMateriais, String> clnSetor = FormTableColumn.create(cln -> {
        cln.title("Setor");
        cln.width(40);
        cln.alignment(FormTableColumn.Alignment.CENTER);
        cln.value((Callback<TableColumn.CellDataFeatures<SdReservaMateriais, SdReservaMateriais>, ObservableValue<SdReservaMateriais>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getSetor()));
    });
    private final FormTableColumn<SdReservaMateriais, String> clnFaixa = FormTableColumn.create(cln -> {
        cln.title("Faixa");
        cln.width(40);
        cln.alignment(FormTableColumn.Alignment.CENTER);
        cln.value((Callback<TableColumn.CellDataFeatures<SdReservaMateriais, SdReservaMateriais>, ObservableValue<SdReservaMateriais>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getFaixa()));
    });
    private final FormTableColumn<SdReservaMateriais, String> clnId = FormTableColumn.create(cln -> {
        cln.title("ID");
        cln.width(20);
        cln.alignment(FormTableColumn.Alignment.CENTER);
        cln.value((Callback<TableColumn.CellDataFeatures<SdReservaMateriais, SdReservaMateriais>, ObservableValue<SdReservaMateriais>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getIdPcpftof()));
    });
    private final FormTableColumn<SdReservaMateriais, String> clnUnidade = FormTableColumn.create(cln -> {
        cln.title("U.N.");
        cln.alignment(FormTableColumn.Alignment.CENTER);
        cln.width(30);
        cln.value((Callback<TableColumn.CellDataFeatures<SdReservaMateriais, SdReservaMateriais>, ObservableValue<SdReservaMateriais>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getInsumo().getUnidade()));
    });
    private final FormTableColumn<SdReservaMateriais, BigDecimal> clnQtde = FormTableColumn.create(cln -> {
        cln.title("Qtde B");
        cln.width(50);
        cln.alignment(FormTableColumn.Alignment.RIGHT);
        cln.value((Callback<TableColumn.CellDataFeatures<SdReservaMateriais, SdReservaMateriais>, ObservableValue<SdReservaMateriais>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtdeB()));
        cln.format(param -> {
            return new TableCell<SdReservaMateriais, BigDecimal>() {
                @Override
                protected void updateItem(BigDecimal item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    if (item != null && !empty) {
                        setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                    }
                }
            };
        });
    });
    private final FormTableColumn<SdReservaMateriais, Boolean> clnReserva = FormTableColumn.create(cln -> {
        cln.title("Reser.");
        cln.width(40);
        cln.alignment(FormTableColumn.Alignment.CENTER);
        cln.value((Callback<TableColumn.CellDataFeatures<SdReservaMateriais, SdReservaMateriais>, ObservableValue<SdReservaMateriais>>) param -> new ReadOnlyObjectWrapper(param.getValue().isReserva()));
        cln.format(param -> {
            return new TableCell<SdReservaMateriais, Boolean>() {
                @Override
                protected void updateItem(Boolean item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);
                    if (item != null && !empty) {
                        setGraphic(item ? ImageUtils.getIcon(ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._16) : ImageUtils.getIcon(ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                    }
                }
            };
        });
    });
    private final FormTableColumn<SdReservaMateriais, Boolean> clnColetado = FormTableColumn.create(cln -> {
        cln.title("Colet.");
        cln.width(40);
        cln.alignment(FormTableColumn.Alignment.CENTER);
        cln.value((Callback<TableColumn.CellDataFeatures<SdReservaMateriais, SdReservaMateriais>, ObservableValue<SdReservaMateriais>>) param -> new ReadOnlyObjectWrapper(param.getValue().isColeta()));
        cln.format(param -> {
            return new TableCell<SdReservaMateriais, Boolean>() {
                @Override
                protected void updateItem(Boolean item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);
                    if (item != null && !empty) {
                        setGraphic(item ? ImageUtils.getIcon(ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._16) : ImageUtils.getIcon(ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                    }
                }
            };
        });
    });
    private final FormTableColumn<SdReservaMateriais, Boolean> clnSubstituto = FormTableColumn.create(cln -> {
        cln.title("Subs.");
        cln.width(40);
        cln.alignment(FormTableColumn.Alignment.CENTER);
        cln.value((Callback<TableColumn.CellDataFeatures<SdReservaMateriais, SdReservaMateriais>, ObservableValue<SdReservaMateriais>>) param -> new ReadOnlyObjectWrapper(param.getValue().isSubstituto()));
        cln.format(param -> {
            return new TableCell<SdReservaMateriais, Boolean>() {
                @Override
                protected void updateItem(Boolean item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);
                    if (item != null && !empty) {
                        setGraphic(item ? ImageUtils.getIcon(ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._16) : ImageUtils.getIcon(ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                    }
                }
            };
        });
    });
    private final FormTableColumn<SdReservaMateriais, SdReservaMateriais> colAcoesReserva = FormTableColumn.create(cln -> {
        cln.title("Ações");
        cln.width(80);
        cln.value((Callback<TableColumn.CellDataFeatures<SdReservaMateriais, SdReservaMateriais>, ObservableValue<SdReservaMateriais>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
        cln.order(Comparator.comparing(o -> ((SdReservaMateriais) o).getId().getNumero()));
        cln.format(param -> {
            return new TableCell<SdReservaMateriais, SdReservaMateriais>() {
                final HBox boxButtonsRow = new HBox(3);
                Button btnExtornarMaterial = FormButton.create(btn -> {
                    btn.icon(ImageUtils.getIconButton(ImageUtils.ButtonIcon.NEGATIVE, ImageUtils.IconSize._16));
                    btn.tooltip("Extornar quantidade do material");
                    btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                    btn.addStyle("danger").addStyle("xs");
                });
                final Button btnAbrirMaterial = FormButton.create(btn -> {
                    btn.icon(ImageUtils.getIconButton(ImageUtils.ButtonIcon.NEW_REGISTER, ImageUtils.IconSize._16));
                    btn.tooltip("Liberar material para nova baixa");
                    btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                    btn.addStyle("success").addStyle("xs");
                });
                
                @Override
                protected void updateItem(SdReservaMateriais item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);
                    if (item != null && !empty) {
                        
                        if (item.getTipo().equals("M")) {
                            btnExtornarMaterial = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.TROCAR, ImageUtils.IconSize._16));
                                btn.tooltip("Movimento estoque do material");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("warning").addStyle("xs");
                            });
                        } else {
                            btnExtornarMaterial = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIconButton(ImageUtils.ButtonIcon.NEGATIVE, ImageUtils.IconSize._16));
                                btn.tooltip("Extornar quantidade do material");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("danger").addStyle("xs");
                            });
                        }
                        btnExtornarMaterial.setOnAction(evt -> {
                            new Fragment().show(fragment -> {
                                fragment.title.setText(item.getTipo().equals("M") ? "Movimento Estoque Material" : "Extorno Baixa Material");
                                final FormFieldText qtdeField = FormFieldText.create(field -> {
                                    field.title("Qtde. Extorno");
                                    field.value.set(StringUtils.toDecimalFormat(item.getQtdeB().doubleValue(), 4));
                                    field.mask(FormFieldText.Mask.DOUBLE);
                                    field.editable(!item.getTipo().equals("M"));
                                });
                                final FormFieldText depositoSaida = FormFieldText.create(field -> {
                                    field.title("Origem");
                                    field.editable(false);
                                });
                                final FormFieldText depositoEntrada = FormFieldText.create(field -> {
                                    field.title("Destino");
                                    field.editable(false);
                                });
                                final Button btnExtornar = new Button(item.getTipo().equals("M") ? "Movimentar" : "Extornar", ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                                btnExtornar.getStyleClass().addAll("sm", "success");
                                fragment.box.getChildren().add(qtdeField.build());
                                fragment.size(300.0, 250.0);
                                
                                MatMov movSaida = null;
                                MatMov movEntrada = null;
                                if (item.getTipo().equals("M")) {
                                    if (item.getMovimento() == 0) {
                                        MessageBox.create(message -> {
                                            message.message("A reserva deste material não tem um movimento alocado a ela. Verifique com o setor de TI informando o material e OF.");
                                            message.type(MessageBox.TypeMessageBox.ERROR);
                                            message.showAndWait();
                                        });
                                        btnExtornar.setDisable(true);
                                    } else {
                                        movSaida = new FluentDao().selectFrom(MatMov.class).where(it -> it.equal("ordem", item.getMovimento())).singleResult();
                                        movEntrada = new FluentDao().selectFrom(MatMov.class).where(it -> it.equal("ordem", item.getMovEstorno())).singleResult();
                                        
                                        depositoEntrada.value.set(movSaida.getDeposito());
                                        depositoSaida.value.set(movEntrada.getDeposito());
                                        fragment.box.getChildren().add(FormBox.create(box -> {
                                            box.horizontal();
                                            box.add(depositoSaida.build(), depositoEntrada.build());
                                        }));
                                    }
                                }
                                
                                MatMov finalMovSaida = movSaida;
                                MatMov finalMovEntrada = movEntrada;
                                btnExtornar.setOnAction(evtExtornar -> {
                                    if (qtdeField.value.getValue() == null) {
                                        MessageBox.create(message -> {
                                            message.message("Você precisa primeiro selecionar uma condição de pagamento.");
                                            message.type(MessageBox.TypeMessageBox.IMPORTANT);
                                            message.showAndWait();
                                        });
                                        return;
                                    }
                                    if (new BigDecimal(qtdeField.value.getValue().replace(".", "").replace(",", ".")).compareTo(item.getQtdeB()) > 0) {
                                        MessageBox.create(message -> {
                                            message.message("Você está tentando estornar uma quantidade maior que o consumo baixado. Você pode estornar no máximo: "
                                                    + StringUtils.toDecimalFormat(item.getQtdeB().doubleValue(), 4)
                                                    + " " + item.getId().getInsumo().getUnidade());
                                            message.type(MessageBox.TypeMessageBox.WARNING);
                                            message.showAndWait();
                                        });
                                        return;
                                    }
                                    if (item.isColeta() && !item.getTipo().equals("M")) {
                                        if (!(Boolean) QuestionBox.build(Boolean.class, message -> {
                                            message.message("Material selecionado está marcado como coletado, deseja realmente realizar o estorno?");
                                            message.showAndWait();
                                        }).value.get()) {
                                            fragment.close();
                                        }
                                    }
                                    
                                    if (item.getTipo().equals("M")) {
                                        movimentarEstoqueMaterial(item, finalMovSaida, finalMovEntrada);
                                        
                                        SysLogger.addSysDelizLog("Analise Baixa Automatica", TipoAcao.EDITAR, item.getId().getNumero(),
                                                "Movimento de estoque do material: " + item.getId().getInsumo() + " na cor: " + item.getId().getCorI() + " qtde: " + qtdeField.value.getValue() +
                                                        " do depósito: " + finalMovEntrada.getDeposito() + " para o depósito: " + finalMovSaida.getDeposito());
                                        MessageBox.create(message -> {
                                            message.message("Material movimentado com sucesso");
                                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                                            message.position(Pos.TOP_RIGHT);
                                            message.notification();
                                        });
                                    } else {
                                        estornarReservaMaterial(item, new BigDecimal(qtdeField.value.getValue().replace(".", "").replace(",", ".")));
                                        SysLogger.addSysDelizLog("Analise Baixa Automatica", TipoAcao.EDITAR, item.getId().getNumero(),
                                                "Extorno do material: " + item.getId().getInsumo() + " na cor: " + item.getId().getCorI() + " qtde: " + qtdeField.value.getValue());
                                        MessageBox.create(message -> {
                                            message.message("Material extornado com sucesso");
                                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                                            message.position(Pos.TOP_RIGHT);
                                            message.notification();
                                        });
                                    }
                                    tblFaltantesOf.refresh();
                                    tblReserva.refresh();
                                    fragment.modalStage.close();
                                });
                                fragment.buttonsBox.getChildren().addAll(btnExtornar);
                            });
                        });
                        btnAbrirMaterial.setOnAction(evt -> {
                            abrirReservaMaterial(item);
                            SysLogger.addSysDelizLog("Analise Baixa Automatica", TipoAcao.EDITAR, item.getId().getNumero(),
                                    "Aberto material: " + item.getId().getInsumo() + " na cor: " + item.getId().getCorI() + " qtde: " + StringUtils.toDecimalFormat(item.getQtdeB().doubleValue(), 4) + " para nova baixa.");
                            MessageBox.create(message -> {
                                message.message("Material aberto para nova baixa.");
                                message.type(MessageBox.TypeMessageBox.CONFIRM);
                                message.position(Pos.TOP_RIGHT);
                                message.notification();
                            });
                        });
                        boxButtonsRow.getChildren().clear();
                        boxButtonsRow.getChildren().add(btnExtornarMaterial);
                        if (!item.getTipo().equals("M") && !item.getTipo().equals("R"))
                            boxButtonsRow.getChildren().add(btnAbrirMaterial);
                        setGraphic(boxButtonsRow);
                    }
                }
            };
        });
    });
    private final FormTableView<SdReservaMateriais> tblReserva = FormTableView.create(SdReservaMateriais.class, table -> {
        table.title("Reservas");
        table.items.bind(super.reservaMateriaisOf);
        table.expanded();
        table.addColumn(clnDhRequisicao);
        table.addColumn(clnOf);
        table.addColumn(clnMaterial);
        table.addColumn(clnCorI);
        table.addColumn(clnFaixa);
        table.addColumn(clnId);
        table.addColumn(clnLote);
        table.addColumn(clnDeposito);
        table.addColumn(clnSetor);
        table.addColumn(clnUnidade);
        table.addColumn(clnQtde);
        table.addColumn(clnSubstituto);
        table.addColumn(clnReserva);
        table.addColumn(clnColetado);
        table.addColumn(colAcoesReserva);
        table.contextMenu(FormContextMenu.create(menu -> {
            menu.addItem(item -> {
                item.setText("Incluir Reposição");
                item.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._16));
                item.setOnAction(evt -> {
                    new Fragment().show(fragment -> {
                        fragment.title("Reposição de Material");
                        fragment.size(500.0, 500.0);
                        SdReservaMateriais materialSelecionado = (SdReservaMateriais) table.selectedItem();
                        List<MatIten> estoqueMaterial = (List<MatIten>) new FluentDao().selectFrom(MatIten.class)
                                .where(it -> it
                                        .equal("id.codigo", materialSelecionado.getId().getInsumo().getCodigo())
                                        .equal("id.cor", materialSelecionado.getId().getCorI().getCor())
                                        .notEqual("id.deposito.codigo", "07153")
                                        .greaterThan("qtde", 0))
                                .resultList();
                        
                        MatIten depositoMaterial = null;
                        try {
                            depositoMaterial = estoqueMaterial.stream().filter(it -> it.getId().getDeposito().getCodigo().equals(materialSelecionado.getId().getDeposito())).findFirst().get();
                        } catch (NoSuchElementException ex) {
                            depositoMaterial = estoqueMaterial.size() > 0 ? estoqueMaterial.get(0) : null;
                        }
                        
                        final FormFieldText materialRepField = FormFieldText.create(field -> {
                            field.title("Material");
                            field.editable(false);
                            field.width(320.0);
                            field.value.set(materialSelecionado.getId().getInsumo().toString());
                        });
                        final FormFieldText corRepField = FormFieldText.create(field -> {
                            field.title("Cor");
                            field.editable(false);
                            field.width(180.0);
                            field.value.set(materialSelecionado.getId().getCorI().toString());
                        });
                        final FormFieldText qtdeRepField = FormFieldText.create(field -> {
                            field.title("Qtde");
                            field.mask(FormFieldText.Mask.DOUBLE);
                            field.width(150.0);
                        });
                        final FormFieldText unidadeMatRepField = FormFieldText.create(field -> {
                            field.title("U.N.");
                            field.width(50.0);
                            field.value.set(materialSelecionado.getId().getInsumo().getUnidade().description());
                        });
                        MatIten finalDepositoMaterial = depositoMaterial;
                        final FormTableView<MatIten> tblEstoqueMatReposicao = FormTableView.create(MatIten.class, tableRep -> {
                            tableRep.expanded();
                            tableRep.title("Depósito");
                            tableRep.items.set(FXCollections.observableList(estoqueMaterial));
                            tableRep.selectItem(finalDepositoMaterial == null ? 0 : finalDepositoMaterial);
                            tableRep.columns(
                                    FormTableColumn.create(cln -> {
                                        cln.title("Depósito");
                                        cln.width(200.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<MatIten, MatIten>, ObservableValue<MatIten>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getDeposito()));
                                    }).build(),
                                    FormTableColumn.create(cln -> {
                                        cln.title("Lote");
                                        cln.width(60.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<MatIten, MatIten>, ObservableValue<MatIten>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getLote()));
                                        cln.alignment(FormTableColumn.Alignment.CENTER);
                                    }).build(),
                                    FormTableColumn.create(cln -> {
                                        cln.title("Qtde");
                                        cln.width(80.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<MatIten, MatIten>, ObservableValue<MatIten>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                                        cln.alignment(FormTableColumn.Alignment.RIGHT);
                                    }).build()
                            );
                        });
                        fragment.box.getChildren().add(FormBox.create(box -> {
                            box.horizontal();
                            box.add(materialRepField.build(), corRepField.build());
                        }));
                        fragment.box.getChildren().add(FormBox.create(box -> {
                            box.horizontal();
                            box.expanded();
                            box.alignment(Pos.TOP_LEFT);
                            box.add(FormBox.create(box1 -> {
                                box1.vertical();
                                box1.add(qtdeRepField.build(), unidadeMatRepField.build());
                            }), tblEstoqueMatReposicao.build());
                        }));
                        
                        Button btnReposicao = FormButton.create(btn -> {
                            btn.title("Adicionar Reposição");
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._24));
                            btn.addStyle("sm").addStyle("success");
                            btn.setAction(event -> {
                                if (estoqueMaterial.size() == 0) {
                                    MessageBox.create(message -> {
                                        message.message("Não é possível fazer a reposição do material seleciona pois o mesmo não se encontra em estoque.");
                                        message.type(MessageBox.TypeMessageBox.IMPORTANT);
                                        message.showAndWait();
                                    });
                                    return;
                                }
                                if (new BigDecimal(StringUtils.unformatDecimal(qtdeRepField.value.get())).compareTo(BigDecimal.ZERO) <= 0 &&
                                        new BigDecimal(StringUtils.unformatDecimal(qtdeRepField.value.get())).compareTo(tblEstoqueMatReposicao.selectedItem().getQtde()) > 0) {
                                    MessageBox.create(message -> {
                                        message.message("A quantidade de reposição não pode ser 0(zero) ou maior que o estoque do depósito/lote selecionado.");
                                        message.type(MessageBox.TypeMessageBox.IMPORTANT);
                                        message.showAndWait();
                                    });
                                    return;
                                }
                                
                                SdReservaMateriais reservaReposicao = new SdReservaMateriais(materialSelecionado.getId().getNumero(), materialSelecionado.getId().getInsumo(), materialSelecionado.getId().getCorI(),
                                        materialSelecionado.getId().getSetor(), tblEstoqueMatReposicao.selectedItem().getId().getDeposito().getCodigo(), tblEstoqueMatReposicao.selectedItem().getId().getLote(),
                                        materialSelecionado.getId().getFaixa(), materialSelecionado.getId().getIdPcpftof(), LocalDateTime.now(), 0, new BigDecimal(StringUtils.unformatDecimal(qtdeRepField.value.get())),
                                        "B", true, false, false, materialSelecionado.isSubstituto(), 0, tblEstoqueMatReposicao.selectedItem().getLocal(),
                                        tblEstoqueMatReposicao.selectedItem().getId().getDeposito().getCodigo());
                                reservaReposicao.setTipo("R");
                                MatIten depositoBaixa = tblEstoqueMatReposicao.selectedItem();
                                depositoBaixa.setQtde(depositoBaixa.getQtde().subtract(reservaReposicao.getQtdeB()));
                                MatMov movSaidaReposicao = new MatMov(reservaReposicao.getId().getInsumo().getCodigo(),
                                        "S",
                                        null,
                                        reservaReposicao.getId().getNumero(),
                                        reservaReposicao.getQtdeB(),
                                        reservaReposicao.getId().getInsumo().getPreco(),
                                        reservaReposicao.getId().getInsumo().getPreco(),
                                        reservaReposicao.getId().getCorI().getCor(),
                                        "9",
                                        null,
                                        reservaReposicao.getId().getDeposito(),
                                        null,
                                        LocalDate.now(),
                                        reservaReposicao.getId().getLote(),
                                        SceneMainController.getAcesso().getUsuario() + "- Reposição de Material",
                                        null,
                                        null,
                                        "BC",
                                        LocalDateTime.now());
                                try {
                                    movSaidaReposicao = new FluentDao().persist(movSaidaReposicao);
                                    reservaReposicao.setMovimento(movSaidaReposicao.getOrdem());
                                    new FluentDao().persist(reservaReposicao);
                                    new FluentDao().merge(depositoBaixa);
                                    new FluentDao().runNativeQueryUpdate(String.format("update sd_of1_001 set liberado_coleta = 'S' where numero = '%s'", reservaReposicao.getId().getNumero()));
                                    SysLogger.addSysDelizLog("Analise Baixa Automatica", TipoAcao.CADASTRAR, reservaReposicao.getId().getNumero(),
                                            "Adicionado reposição de " + reservaReposicao.getQtdeB() + " " + reservaReposicao.getId().getInsumo().getUnidade().getDescricao() +
                                                    " do material " + reservaReposicao.getId().getInsumo() + " na cor " + reservaReposicao.getId().getCorI() + " do depósito " + reservaReposicao.getId().getDeposito());
                                    reservaMateriaisOf.add(reservaReposicao);
                                    MessageBox.create(message -> {
                                        message.message("Reposição adicionada para o material na OF.");
                                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                                        message.position(Pos.TOP_RIGHT);
                                        message.notification();
                                    });
                                    fragment.close();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    MatMov finalMovSaidaReposicao = movSaidaReposicao;
                                    ExceptionBox.build(message -> {
                                        message.exception(e);
                                        message.objects(reservaReposicao, depositoBaixa, finalMovSaidaReposicao);
                                        message.showAndWait();
                                    });
                                }
                            });
                        });
                        fragment.buttonsBox.getChildren().add(btnReposicao);
                    });
                });
            });
        }));
        table.factoryRow(param -> {
            return new TableRow<SdReservaMateriais>() {
                @Override
                protected void updateItem(SdReservaMateriais item, boolean empty) {
                    super.updateItem(item, empty);
                    clear();
                    
                    if (item != null && !empty)
                        if (item.getTipo().equals("R"))
                            getStyleClass().add("table-row-warning");
                        else if (item.getTipo().equals("M"))
                            getStyleClass().add("table-row-primary");
                }
                
                private void clear() {
                    getStyleClass().removeAll("table-row-warning", "table-row-primary");
                }
            };
        });
        table.indices(table.indice(Color.GOLD, "Material de Reposição", false), table.indice(Color.BLUE, "Movimento Extra para Facção", false));
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Table MATERIAIS FALTANTES OF">
    private final FormTableColumn<SdMateriaisFaltantes, LocalDateTime> clnDtAnaliseOf = FormTableColumn.create(cln -> {
        cln.title("Data");
        cln.width(95);
        cln.value((Callback<TableColumn.CellDataFeatures<SdMateriaisFaltantes, SdMateriaisFaltantes>, ObservableValue<SdMateriaisFaltantes>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDhAnalise()));
        cln.format(param -> {
            return new TableCell<SdMateriaisFaltantes, LocalDateTime>() {
                @Override
                protected void updateItem(LocalDateTime item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    if (item != null && !empty) {
                        setText(StringUtils.toDateTimeFormat(item));
                    }
                }
            };
        });
    });
    private final FormTableColumn<SdMateriaisFaltantes, String> clnOfFaltanteOf = FormTableColumn.create(cln -> {
        cln.title("O.F.");
        cln.width(50.0);
        cln.value((Callback<TableColumn.CellDataFeatures<SdMateriaisFaltantes, SdMateriaisFaltantes>, ObservableValue<SdMateriaisFaltantes>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getNumero()));
    });
    private final FormTableColumn<SdMateriaisFaltantes, Material> clnMaterialFaltanteOf = FormTableColumn.create(cln -> {
        cln.title("Material");
        cln.width(280);
        cln.value((Callback<TableColumn.CellDataFeatures<SdMateriaisFaltantes, SdMateriaisFaltantes>, ObservableValue<SdMateriaisFaltantes>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getInsumo()));
    });
    private final FormTableColumn<SdMateriaisFaltantes, Cor> clnCorIFaltanteOf = FormTableColumn.create(cln -> {
        cln.title("Cor Mat.");
        cln.width(135);
        cln.value((Callback<TableColumn.CellDataFeatures<SdMateriaisFaltantes, SdMateriaisFaltantes>, ObservableValue<SdMateriaisFaltantes>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCorI()));
    });
    private final FormTableColumn<SdMateriaisFaltantes, String> clnSetorFaltanteOf = FormTableColumn.create(cln -> {
        cln.title("Setor");
        cln.width(40.0);
        cln.alignment(FormTableColumn.Alignment.CENTER);
        cln.value((Callback<TableColumn.CellDataFeatures<SdMateriaisFaltantes, SdMateriaisFaltantes>, ObservableValue<SdMateriaisFaltantes>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getSetor()));
    });
    private final FormTableColumn<SdMateriaisFaltantes, String> clnFaixaFaltanteOf = FormTableColumn.create(cln -> {
        cln.title("Faixa");
        cln.width(40);
        cln.alignment(FormTableColumn.Alignment.CENTER);
        cln.value((Callback<TableColumn.CellDataFeatures<SdMateriaisFaltantes, SdMateriaisFaltantes>, ObservableValue<SdMateriaisFaltantes>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getFaixa()));
    });
    private final FormTableColumn<SdMateriaisFaltantes, String> clnIdFaltanteOf = FormTableColumn.create(cln -> {
        cln.title("ID");
        cln.width(20);
        cln.alignment(FormTableColumn.Alignment.CENTER);
        cln.value((Callback<TableColumn.CellDataFeatures<SdMateriaisFaltantes, SdMateriaisFaltantes>, ObservableValue<SdMateriaisFaltantes>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getIdPcpftof()));
    });
    private final FormTableColumn<SdMateriaisFaltantes, String> clnUnidadeFaltantanteOf = FormTableColumn.create(cln -> {
        cln.title("U.N.");
        cln.width(30);
        cln.alignment(FormTableColumn.Alignment.CENTER);
        cln.value((Callback<TableColumn.CellDataFeatures<SdMateriaisFaltantes, SdMateriaisFaltantes>, ObservableValue<SdMateriaisFaltantes>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getInsumo().getUnidade()));
    });
    private final FormTableColumn<SdMateriaisFaltantes, BigDecimal> clnQtdeFaltanteOf = FormTableColumn.create(cln -> {
        cln.title("Qtde");
        cln.width(65.0);
        cln.alignment(FormTableColumn.Alignment.RIGHT);
        cln.value((Callback<TableColumn.CellDataFeatures<SdMateriaisFaltantes, SdMateriaisFaltantes>, ObservableValue<SdMateriaisFaltantes>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
        cln.format(param -> {
            return new TableCell<SdMateriaisFaltantes, BigDecimal>() {
                @Override
                protected void updateItem(BigDecimal item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    if (item != null && !empty) {
                        setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                    }
                }
            };
        });
    });
    private final FormTableColumn<SdMateriaisFaltantes, String> clnStatusFaltanteOf = FormTableColumn.create(cln -> {
        cln.title("Status");
        cln.width(70.0);
        cln.value((Callback<TableColumn.CellDataFeatures<SdMateriaisFaltantes, SdMateriaisFaltantes>, ObservableValue<SdMateriaisFaltantes>>) param -> new ReadOnlyObjectWrapper(param.getValue().getStatus()));
        cln.format(param -> {
            return new TableCell<SdMateriaisFaltantes, String>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    if (item != null && !empty) {
                        setText(item.equals("A") ? "Aberto" : item.equals("C") ? "Com Compra" : "Resolvido");
                    }
                }
            };
        });
    });
    private final FormTableColumn<SdMateriaisFaltantes, Boolean> clnSubstitutoFaltanteOf = FormTableColumn.create(cln -> {
        cln.title("Subs.");
        cln.width(40);
        cln.alignment(FormTableColumn.Alignment.CENTER);
        cln.value((Callback<TableColumn.CellDataFeatures<SdMateriaisFaltantes, SdMateriaisFaltantes>, ObservableValue<SdMateriaisFaltantes>>) param -> new ReadOnlyObjectWrapper(param.getValue().isSubstituto()));
        cln.format(param -> {
            return new TableCell<SdMateriaisFaltantes, Boolean>() {
                @Override
                protected void updateItem(Boolean item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    setGraphic(null);
                    if (item != null && !empty) {
                        setGraphic(item ? ImageUtils.getIcon(ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._16) : ImageUtils.getIcon(ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                    }
                }
            };
        });
    });
    private final FormTableView<SdMateriaisFaltantes> tblFaltantesOf = FormTableView.create(SdMateriaisFaltantes.class, table -> {
        table.title("Materiais Faltantes");
        table.width(870.0);
        table.items.bind(super.materiaisFaltantesOf);
        table.addColumn(clnDtAnaliseOf);
        table.addColumn(clnOfFaltanteOf);
        table.addColumn(clnMaterialFaltanteOf);
        table.addColumn(clnCorIFaltanteOf);
        table.addColumn(clnFaixaFaltanteOf);
        table.addColumn(clnIdFaltanteOf);
        table.addColumn(clnSetorFaltanteOf);
        table.addColumn(clnUnidadeFaltantanteOf);
        table.addColumn(clnQtdeFaltanteOf);
        table.addColumn(clnSubstitutoFaltanteOf);
        table.addColumn(clnStatusFaltanteOf);
        table.contextMenu(FormContextMenu.create(menu -> {
            menu.addItem(item -> {
                item.setText("Substituir material");
                item.setGraphic(ImageUtils.getIconButton(ImageUtils.ButtonIcon.SUBSTITUIR, ImageUtils.IconSize._16));
                item.setOnAction(evt -> {
                    SdMateriaisFaltantes materialFaltanteSelecionado = (SdMateriaisFaltantes) table.selectedItem();
                    new Fragment().show(fragment -> {
                        fragment.title("Substituição de Material");
                        fragment.size(900.0, 600.0);
                        final ListProperty<VSdEstoqSugestaoSubstituto> estoqSubstitutos = new SimpleListProperty<>();
                        estoqSubstitutos.set(FXCollections.observableList((List<VSdEstoqSugestaoSubstituto>) new FluentDao().selectFrom(VSdEstoqSugestaoSubstituto.class)
                                .where(it -> it
                                        .equal("id.numero", materialFaltanteSelecionado.getId().getNumero())
                                        .equal("id.insumo", materialFaltanteSelecionado.getId().getInsumo().getCodigo())
                                        .equal("id.cori", materialFaltanteSelecionado.getId().getCorI().getCor()))
                                .resultList()));
                        final ListProperty<VSdEstoqueMatCorBr> estoqMatCores = new SimpleListProperty<>();
                        estoqMatCores.set(FXCollections.observableList((List<VSdEstoqueMatCorBr>) new FluentDao().selectFrom(VSdEstoqueMatCorBr.class)
                                .where(it -> it
                                        .equal("codigo.codigo", materialFaltanteSelecionado.getId().getInsumo().getCodigo())
                                        .greaterThan("qtde", BigDecimal.ZERO))
                                .resultList()));
                        final FormFieldSingleFind<Material> materialOriginalField = FormFieldSingleFind.create(Material.class, field -> {
                            field.title("Material Original");
                            field.editable.set(false);
                            field.setDefaultCode(materialFaltanteSelecionado.getId().getInsumo().getCodigo());
                            field.width(350);
                            field.widthCode(80.0);
                        });
                        final FormFieldSingleFind<Cor> corIOiginalField = FormFieldSingleFind.create(Cor.class, field -> {
                            field.title("Cor Original");
                            field.editable.set(false);
                            field.setDefaultCode(materialFaltanteSelecionado.getId().getCorI().getCor());
                            field.width(250);
                            field.widthCode(80.0);
                        });
                        final FormTableView<VSdEstoqueMatCorBr> tableCoresMat = FormTableView.create(VSdEstoqueMatCorBr.class, tableCores -> {
                            tableCores.title("Cores Substitutas Material");
                            tableCores.expanded();
                            tableCores.items.bind(estoqMatCores);
                            tableCores.columns(
                                    FormTableColumn.create(cln -> {
                                        cln.title("Cor");
                                        cln.width(60.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<VSdEstoqueMatCorBr, VSdEstoqueMatCorBr>, ObservableValue<VSdEstoqueMatCorBr>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCor().getCor()));
                                    }).build() /*Cor*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Descrição");
                                        cln.width(240.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<VSdEstoqueMatCorBr, VSdEstoqueMatCorBr>, ObservableValue<VSdEstoqueMatCorBr>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCor().getDescricao()));
                                    }).build() /*Descrição*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Qtde");
                                        cln.width(90.0);
                                        cln.alignment(FormTableColumn.Alignment.RIGHT);
                                        cln.value((Callback<TableColumn.CellDataFeatures<VSdEstoqueMatCorBr, VSdEstoqueMatCorBr>, ObservableValue<VSdEstoqueMatCorBr>>) param -> new ReadOnlyObjectWrapper(StringUtils.toDecimalFormat(param.getValue().getQtde().doubleValue(),4)));
                                    }).build() /*Qtde*/
                            );
                            tableCores.selectItem(0);
                        });
                        final FormFieldSingleFind<Material> materialSubtitutoField = FormFieldSingleFind.create(Material.class, field -> {
                            field.title("Material Substituto");
                            field.setDefaultCode(materialFaltanteSelecionado.getId().getInsumo().getCodigo());
                            field.width(350);
                            field.widthCode(80.0);
                            field.postSelected((observable, oldValue, newValue) -> {
                                //getCoresMaterial(((Material) field.value.get()).getCodigo());
                                estoqMatCores.set(FXCollections.observableList((List<VSdEstoqueMatCorBr>) new FluentDao().selectFrom(VSdEstoqueMatCorBr.class)
                                        .where(it -> it
                                                .equal("codigo.codigo", ((Material) field.value.get()).getCodigo())
                                                .greaterThan("qtde", BigDecimal.ZERO))
                                        .resultList()));
                                VSdEstoqueMatCorBr selectedCor = getSelectedCor(estoqMatCores, corIOiginalField.value.get().getCor());
                                if (selectedCor != null)
                                    tableCoresMat.selectItem(selectedCor);
                            });
                        });
                        final FormFieldComboBox<Cor> corISubstitutoField = FormFieldComboBox.create(Cor.class, field -> {
                            field.title("Cor");
                            //getCoresMaterial(materialSubtitutoField.value.get().getCodigo());
                            field.items.bind(coresMaterial);
                            field.select(corIOiginalField.value.get());
                            field.width(300.0);
                        });
                        final FormFieldSingleFind<Cor> corISubtitutoField = FormFieldSingleFind.create(Cor.class, field -> {
                            field.title("Cor Substituta");
                            field.setDefaultCode(materialFaltanteSelecionado.getId().getCorI().getCor());
                            field.width(250);
                        });
                        final FormTableView<VSdEstoqSugestaoSubstituto> tblSugestaoSubstituto = FormTableView.create(VSdEstoqSugestaoSubstituto.class, tableSugestao -> {
                            tableSugestao.title("Sugestões Compras/Desenvolvimento");
                            tableSugestao.expanded();
                            tableSugestao.items.set(estoqSubstitutos);
                            tableSugestao.columns(
                                    FormTableColumn.create(cln -> {
                                        cln.title("Material");
                                        cln.width(280.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<VSdEstoqSugestaoSubstituto, VSdEstoqSugestaoSubstituto>, ObservableValue<VSdEstoqSugestaoSubstituto>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getInsumosubs()));
                                    }).build(),
                                    FormTableColumn.create(cln -> {
                                        cln.title("Cor");
                                        cln.width(120.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<VSdEstoqSugestaoSubstituto, VSdEstoqSugestaoSubstituto>, ObservableValue<VSdEstoqSugestaoSubstituto>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCorsub()));
                                    }).build(),
                                    FormTableColumn.create(cln -> {
                                        cln.title("Qtde");
                                        cln.width(80);
                                        cln.alignment(FormTableColumn.Alignment.RIGHT);
                                        cln.value((Callback<TableColumn.CellDataFeatures<VSdEstoqSugestaoSubstituto, VSdEstoqSugestaoSubstituto>, ObservableValue<VSdEstoqSugestaoSubstituto>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                                    }).build()
                            );
                            tableSugestao.tableProperties().setOnMouseClicked(event -> {
                                if (event.getClickCount() >= 2) {
                                    if (((VSdEstoqSugestaoSubstituto) tableSugestao.selectedItem()) != null) {
                                        materialSubtitutoField.setDefaultCode(((VSdEstoqSugestaoSubstituto) tableSugestao.selectedItem()).getId().getInsumosubs().getCodigo());
                                        //corISubtitutoField.setDefaultCode(((VSdEstoqSugestaoSubstituto) tableSugestao.selectedItem()).getId().getCorsub().getCor());
                                        //corISubstitutoField.select(((VSdEstoqSugestaoSubstituto) tableSugestao.selectedItem()).getId().getCorsub());
                                        VSdEstoqueMatCorBr selectedCor = getSelectedCor(estoqMatCores, corIOiginalField.value.get().getCor());
                                        if (selectedCor != null)
                                            tableCoresMat.selectItem(selectedCor);
                                    }
                                }
                            });
                            tableSugestao.contextMenu(FormContextMenu.create(cmenuSugestao -> {
                                cmenuSugestao.addItem(itemDepSugestao -> {
                                    itemDepSugestao.setText("Ver Depósitos");
                                    itemDepSugestao.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.DEPOSITO, ImageUtils.IconSize._16));
                                    itemDepSugestao.setOnAction(evtDepSugestao -> {
                                        new Fragment().show(fragmentDeposito -> {
                                            VSdEstoqSugestaoSubstituto substitutoSelecionado = (VSdEstoqSugestaoSubstituto) tableSugestao.selectedItem();
                                            fragmentDeposito.title("Depósitos Material");
                                            fragmentDeposito.size(300.0, 300.0);
                                            final ObservableList<MatIten> depositos = FXCollections.observableList((List<MatIten>) new FluentDao().selectFrom(MatIten.class)
                                                    .where(it -> it
                                                            .equal("id.codigo", substitutoSelecionado.getId().getInsumosubs().getCodigo())
                                                            .equal("id.cor", substitutoSelecionado.getId().getCorsub().getCor())
                                                            .equal("id.deposito.pais", "BR")
                                                            .greaterThan("qtde", BigDecimal.ZERO))
                                                    .resultList());
                                            final FormTableView<MatIten> tblDepositos = FormTableView.create(MatIten.class, tableDepositos -> {
                                                tableDepositos.expanded();
                                                tableDepositos.title("Depósitos Material");
                                                tableDepositos.items.set(depositos);
                                                tableDepositos.columns(
                                                        FormTableColumn.create(cln -> {
                                                            cln.title("Depósito");
                                                            cln.width(80);
                                                            cln.value((Callback<TableColumn.CellDataFeatures<MatIten, MatIten>, ObservableValue<MatIten>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getDeposito()));
                                                            cln.alignment(FormTableColumn.Alignment.CENTER);
                                                        }).build(),
                                                        FormTableColumn.create(cln -> {
                                                            cln.title("Qtde");
                                                            cln.width(120);
                                                            cln.value((Callback<TableColumn.CellDataFeatures<MatIten, MatIten>, ObservableValue<MatIten>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                                                            cln.alignment(FormTableColumn.Alignment.RIGHT);
                                                        }).build()
                                                );
                                            });
                                            fragmentDeposito.box.getChildren().add(tblDepositos.build());
                                        });
                                    });
                                });
                            }));
                        });
                        VSdEstoqueMatCorBr selectedCor = getSelectedCor(estoqMatCores, corIOiginalField.value.get().getCor());
                        if (selectedCor != null)
                            tableCoresMat.selectItem(selectedCor);
                        
                        fragment.box.getChildren().add(FormBox.create(boxFormulario -> {
                            boxFormulario.vertical();
                            boxFormulario.expanded();
                            boxFormulario.add(FormBox.create(boxMateriais -> {
                                boxMateriais.horizontal();
                                boxMateriais.add(FormBox.create(boxFields -> {
                                    boxFields.vertical();
                                    boxFields.expanded();
                                    boxFields.add(materialOriginalField.build(), corIOiginalField.build(), materialSubtitutoField.build(), tableCoresMat.build());
                                }));
                                boxMateriais.add(tblSugestaoSubstituto.build());
                            }));
//                            boxFormulario.add(FormBox.create(boxOriginal -> {
//                                boxOriginal.horizontal();
//                                boxOriginal.add(materialOriginalField.build(), corIOiginalField.build());
//                            }));
//                            boxFormulario.add(FormBox.create(boxSubstituto -> {
//                                boxSubstituto.vertical();
//                                boxSubstituto.add(materialSubtitutoField.build(), tableCoresMat.build());
//                            }));
//                            boxFormulario.add();
                        }));
    
                        final Button btnSubstituir = FormButton.create(btnSubs -> {
                            btnSubs.title("Substituir");
                            btnSubs.icon(ImageUtils.getIcon(ImageUtils.Icon.SUBSTITUIR, ImageUtils.IconSize._16));
                            btnSubs.addStyle("success");
                            btnSubs.setAction(evtSubstituir -> {
                                if (tableCoresMat.selectedItem() == null || materialSubtitutoField.value.get() == null) {
                                    MessageBox.create(message -> {
                                        message.message("Você deve informar um Material e Cor para a substituição, verifique se os campos estão preenchidos e selecionados.");
                                        message.type(MessageBox.TypeMessageBox.IMPORTANT);
                                        message.showAndWait();
                                    });
                                    return;
                                }
                                substituirMaterial(materialFaltanteSelecionado, materialSubtitutoField.value.getValue(), tableCoresMat.selectedItem().getCor());
                                new FluentDao().runNativeQueryUpdate(String.format("update sd_materiais_faltantes_001 set substituto = 'S', insumo = '%s', cor_i = '%s' where numero = '%s' and insumo = '%s' and cor_i = '%s' and setor = '%s'",
                                        materialSubtitutoField.value.getValue().getCodigo(),
                                        tableCoresMat.selectedItem().getCor().getCor(),
                                        materialFaltanteSelecionado.getId().getNumero(),
                                        materialFaltanteSelecionado.getId().getInsumo().getCodigo(),
                                        materialFaltanteSelecionado.getId().getCorI().getCor(),
                                        materialFaltanteSelecionado.getId().getSetor()));
                                materialFaltanteSelecionado.setSubstituto(true);
                                materialFaltanteSelecionado.getId().setInsumo(materialSubtitutoField.value.getValue());
                                materialFaltanteSelecionado.getId().setCorI(tableCoresMat.selectedItem().getCor());
                                MessageBox.create(message -> {
                                    message.message("Material Subtituido com Sucesso");
                                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                                    message.position(Pos.TOP_RIGHT);
                                    message.notification();
                                });
                                fragment.modalStage.close();
                                table.refresh();
                            });
                        });
                        fragment.buttonsBox.getChildren().addAll(btnSubstituir);
                        
//                        fragment.title("Substituição de Material");
//                        fragment.size(600.0, 400.0);
//                        final ListProperty<VSdEstoqSugestaoSubstituto> estoqSubstitutos = new SimpleListProperty<>();
//                        estoqSubstitutos.set(FXCollections.observableList((List<VSdEstoqSugestaoSubstituto>) new FluentDao().selectFrom(VSdEstoqSugestaoSubstituto.class)
//                                .where(it -> it
//                                        .equal("id.numero", materialFaltanteSelecionado.getId().getNumero())
//                                        .equal("id.insumo", materialFaltanteSelecionado.getId().getInsumo().getCodigo())
//                                        .equal("id.cori", materialFaltanteSelecionado.getId().getCorI().getCor()))
//                                .resultList()));
//                        final FormFieldSingleFind<Material> materialOriginalField = FormFieldSingleFind.create(Material.class, field -> {
//                            field.title("Material Original");
//                            field.editable.set(false);
//                            field.setDefaultCode(materialFaltanteSelecionado.getId().getInsumo().getCodigo());
//                            field.width(350);
//                        });
//                        final FormFieldSingleFind<Cor> corIOiginalField = FormFieldSingleFind.create(Cor.class, field -> {
//                            field.title("Cor Original");
//                            field.editable.set(false);
//                            field.setDefaultCode(materialFaltanteSelecionado.getId().getCorI().getCor());
//                            field.width(250);
//                        });
//                        final FormFieldSingleFind<Material> materialSubtitutoField = FormFieldSingleFind.create(Material.class, field -> {
//                            field.title("Material Substituto");
//                            field.setDefaultCode(materialFaltanteSelecionado.getId().getInsumo().getCodigo());
//                            field.width(350);
//                            field.postSelected((observable, oldValue, newValue) -> {
//                                getCoresMaterial(((Material) field.value.get()).getCodigo());
//                            });
//                        });
//                        final FormFieldComboBox<Cor> corISubstitutoField = FormFieldComboBox.create(Cor.class, field -> {
//                            field.title("Cor");
//                            getCoresMaterial(materialSubtitutoField.value.get().getCodigo());
//                            field.items.bind(coresMaterial);
//                            field.select(corIOiginalField.value.get());
//                            field.width(300.0);
//                        });
//                        final FormFieldSingleFind<Cor> corISubtitutoField = FormFieldSingleFind.create(Cor.class, field -> {
//                            field.title("Cor Substituta");
//                            field.setDefaultCode(materialFaltanteSelecionado.getId().getCorI().getCor());
//                            field.width(250);
//                        });
//                        final FormTableView<VSdEstoqSugestaoSubstituto> tblSugestaoSubstituto = FormTableView.create(VSdEstoqSugestaoSubstituto.class, tableSugestao -> {
//                            tableSugestao.title("Sugestões Compras/Desenvolvimento");
//                            tableSugestao.expanded();
//                            tableSugestao.items.set(estoqSubstitutos);
//                            tableSugestao.columns(
//                                    FormTableColumn.create(cln -> {
//                                        cln.title("Material");
//                                        cln.width(300);
//                                        cln.value((Callback<TableColumn.CellDataFeatures<VSdEstoqSugestaoSubstituto, VSdEstoqSugestaoSubstituto>, ObservableValue<VSdEstoqSugestaoSubstituto>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getInsumosubs()));
//                                    }).build(),
//                                    FormTableColumn.create(cln -> {
//                                        cln.title("Cor");
//                                        cln.width(150);
//                                        cln.value((Callback<TableColumn.CellDataFeatures<VSdEstoqSugestaoSubstituto, VSdEstoqSugestaoSubstituto>, ObservableValue<VSdEstoqSugestaoSubstituto>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCorsub()));
//                                    }).build(),
//                                    FormTableColumn.create(cln -> {
//                                        cln.title("Qtde");
//                                        cln.width(80);
//                                        cln.alignment(FormTableColumn.Alignment.RIGHT);
//                                        cln.value((Callback<TableColumn.CellDataFeatures<VSdEstoqSugestaoSubstituto, VSdEstoqSugestaoSubstituto>, ObservableValue<VSdEstoqSugestaoSubstituto>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
//                                    }).build()
//                            );
//                            tableSugestao.tableProperties().setOnMouseClicked(event -> {
//                                if (event.getClickCount() >= 2) {
//                                    if (((VSdEstoqSugestaoSubstituto) tableSugestao.selectedItem()) != null) {
//                                        materialSubtitutoField.setDefaultCode(((VSdEstoqSugestaoSubstituto) tableSugestao.selectedItem()).getId().getInsumosubs().getCodigo());
//                                        //corISubtitutoField.setDefaultCode(((VSdEstoqSugestaoSubstituto) tableSugestao.selectedItem()).getId().getCorsub().getCor());
//                                        corISubstitutoField.select(((VSdEstoqSugestaoSubstituto) tableSugestao.selectedItem()).getId().getCorsub());
//                                    }
//                                }
//                            });
//                            tableSugestao.contextMenu(FormContextMenu.create(cmenuSugestao -> {
//                                cmenuSugestao.addItem(itemDepSugestao -> {
//                                    itemDepSugestao.setText("Ver Depósitos");
//                                    itemDepSugestao.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.DEPOSITO, ImageUtils.IconSize._16));
//                                    itemDepSugestao.setOnAction(evtDepSugestao -> {
//                                        new Fragment().show(fragmentDeposito -> {
//                                            VSdEstoqSugestaoSubstituto substitutoSelecionado = (VSdEstoqSugestaoSubstituto) tableSugestao.selectedItem();
//                                            fragmentDeposito.title("Depósitos Material");
//                                            fragmentDeposito.size(300.0, 300.0);
//                                            final ObservableList<MatIten> depositos = FXCollections.observableList((List<MatIten>) new FluentDao().selectFrom(MatIten.class)
//                                                    .where(it -> it
//                                                            .equal("id.codigo", substitutoSelecionado.getId().getInsumosubs().getCodigo())
//                                                            .equal("id.cor", substitutoSelecionado.getId().getCorsub().getCor())
//                                                            .equal("id.deposito.pais", "BR")
//                                                            .greaterThan("qtde", BigDecimal.ZERO))
//                                                    .resultList());
//                                            final FormTableView<MatIten> tblDepositos = FormTableView.create(MatIten.class, tableDepositos -> {
//                                                tableDepositos.expanded();
//                                                tableDepositos.title("Depósitos Material");
//                                                tableDepositos.items.set(depositos);
//                                                tableDepositos.columns(
//                                                        FormTableColumn.create(cln -> {
//                                                            cln.title("Depósito");
//                                                            cln.width(80);
//                                                            cln.value((Callback<TableColumn.CellDataFeatures<MatIten, MatIten>, ObservableValue<MatIten>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getDeposito()));
//                                                            cln.alignment(FormTableColumn.Alignment.CENTER);
//                                                        }).build(),
//                                                        FormTableColumn.create(cln -> {
//                                                            cln.title("Qtde");
//                                                            cln.width(120);
//                                                            cln.value((Callback<TableColumn.CellDataFeatures<MatIten, MatIten>, ObservableValue<MatIten>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
//                                                            cln.alignment(FormTableColumn.Alignment.RIGHT);
//                                                        }).build()
//                                                );
//                                            });
//                                            fragmentDeposito.box.getChildren().add(tblDepositos.build());
//                                        });
//                                    });
//                                });
//                            }));
//                        });
//                        fragment.box.getChildren().add(FormBox.create(boxFormulario -> {
//                            boxFormulario.vertical();
//                            boxFormulario.expanded();
//                            boxFormulario.add(FormBox.create(boxOriginal -> {
//                                boxOriginal.horizontal();
//                                boxOriginal.add(materialOriginalField.build(), corIOiginalField.build());
//                            }));
//                            boxFormulario.add(FormBox.create(boxSubstituto -> {
//                                boxSubstituto.horizontal();
//                                boxSubstituto.add(materialSubtitutoField.build(), corISubstitutoField.build());
//                            }));
//                            boxFormulario.add(tblSugestaoSubstituto.build());
//                        }));
//
//                        final Button btnSubstituir = FormButton.create(btnSubs -> {
//                            btnSubs.title("Substituir");
//                            btnSubs.icon(ImageUtils.getIcon(ImageUtils.Icon.SUBSTITUIR, ImageUtils.IconSize._16));
//                            btnSubs.addStyle("success");
//                            btnSubs.setAction(evtSubstituir -> {
//                                if (corISubstitutoField.value.getValue() == null || materialSubtitutoField.value.get() == null) {
//                                    MessageBox.create(message -> {
//                                        message.message("Você deve informar um Material e Cor para a substituição, verifique se os campos estão preenchidos e selecionados.");
//                                        message.type(MessageBox.TypeMessageBox.IMPORTANT);
//                                        message.showAndWait();
//                                    });
//                                    return;
//                                }
//                                substituirMaterial(materialFaltanteSelecionado, materialSubtitutoField.value.getValue(), corISubstitutoField.value.getValue());
//                                new FluentDao().runNativeQueryUpdate(String.format("update sd_materiais_faltantes_001 set substituto = 'S', insumo = '%s', cor_i = '%s' where numero = '%s' and insumo = '%s' and cor_i = '%s' and setor = '%s'",
//                                        materialSubtitutoField.value.getValue().getCodigo(),
//                                        corISubstitutoField.value.getValue().getCor(),
//                                        materialFaltanteSelecionado.getId().getNumero(),
//                                        materialFaltanteSelecionado.getId().getInsumo().getCodigo(),
//                                        materialFaltanteSelecionado.getId().getCorI().getCor(),
//                                        materialFaltanteSelecionado.getId().getSetor()));
//                                materialFaltanteSelecionado.setSubstituto(true);
//                                MessageBox.create(message -> {
//                                    message.message("Material Subtituido com Sucesso");
//                                    message.type(MessageBox.TypeMessageBox.CONFIRM);
//                                    message.position(Pos.TOP_RIGHT);
//                                    message.notification();
//                                });
//                                fragment.modalStage.close();
//                                table.refresh();
//                            });
//                        });
//                        fragment.buttonsBox.getChildren().addAll(btnSubstituir);
                    });
                });
            });
            menu.addSeparator();
            menu.addItem(item -> {
                item.setText("Considerar material baixado");
                item.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.BAIXAR_MATERIAL, ImageUtils.IconSize._16));
                item.setOnAction(evt -> {
                    if (!((Boolean) QuestionBox.build(Boolean.class, message -> {
                            message.message("Deseja realmente marcar o material faltante como baixado?");
                            message.showAndWait();
                        }).value.get())) {
                        return;
                    }
                    considerarMaterialBaixado((SdMateriaisFaltantes) table.selectedItem());
                    MessageBox.create(message -> {
                        message.message("Material removido do controle de faltas.");
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.position(Pos.TOP_RIGHT);
                        message.notification();
                    });
                });
            });
        }));
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Table MATERIAIS FALTANTES">
    private final FormTableView<VSdGestaoMatFaltante> tblFaltantes = FormTableView.create(VSdGestaoMatFaltante.class, table -> {
        table.title("Materiais Faltantes");
        table.expanded();
        table.items.bind(super.materiaisFaltantes);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("D.H. Criação");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDhcriacao()));
                    cln.format(param -> {
                        return new TableCell<VSdGestaoMatFaltante, LocalDateTime>() {
                            @Override
                            protected void updateItem(LocalDateTime item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDateTimeFormat(item));
                                }
                            }
                        };
                    });
                }).build() /*D.H. Criação*/,
                FormTableColumn.create(cln -> {
                    cln.title("D.H. Análise");
                    cln.width(100);
                    cln.hide();
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDhanalise()));
                    cln.format(param -> {
                        return new TableCell<VSdGestaoMatFaltante, LocalDateTime>() {
                            @Override
                            protected void updateItem(LocalDateTime item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDateTimeFormat(item));
                                }
                            }
                        };
                    });
                }).build() /*D.H. Análise*/,
                FormTableColumn.create(cln -> {
                    cln.title("O.F.");
                    cln.width(70.0);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumero()));
                }).build() /*O.F.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Período");
                    cln.width(60.0);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPeriodo()));
                }).build() /*Período*/,
                FormTableColumn.create(cln -> {
                    cln.title("Ref.");
                    cln.width(60.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getCodigo()));
                }).build() /*Ref.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Produto");
                    cln.width(200.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getDescricao()));
                    cln.hide();
                }).build() /*Produto*/,
                FormTableColumn.create(cln -> {
                    cln.title("País");
                    cln.width(35.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPais()));
                    cln.alignment(Pos.CENTER);
                }).build() /*País*/,
                FormTableColumn.create(cln -> {
                    cln.title("Setor");
                    cln.width(40.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getSetor().getCodigo()));
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                }).build() /*Setor*/,
                FormTableColumn.create(cln -> {
                    cln.title("Desc. Setor");
                    cln.width(120.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getSetor().getDescricao()));
                    cln.hide();
                }).build() /*Desc. Setor*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cód. Material");
                    cln.width(100.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getInsumo().getCodigo()));
                }).build() /*Cód. Material*/,
                FormTableColumn.create(cln -> {
                    cln.title("Material");
                    cln.width(330.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getInsumo().getDescricao()));
                }).build() /*Material*/,
                FormTableColumn.create(cln -> {
                    cln.title("Unid.");
                    cln.width(40.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getInsumo().getUnidade()));
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                }).build() /*U.N.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Un. Compra");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getInsumo().getUnicom()));
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.hide();
                }).build() /*Un. Compra*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cód. Cor");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCori().getCor()));
                
                }).build() /*Cód. Cor*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cor");
                    cln.width(180.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCori().getDescricao()));
                }).build() /*Cor*/,
                FormTableColumn.create(cln -> {
                    cln.title("Subs.");
                    cln.width(40.0);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().isSubstituto()));
                    cln.format(param -> {
                        return new TableCell<VSdGestaoMatFaltante, Boolean>() {
                            @Override
                            protected void updateItem(Boolean item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(item ? ImageUtils.getIcon(ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._16) : ImageUtils.getIcon(ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                                }
                            }
                        };
                    });
                }).build() /*Subs.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Grupo");
                    cln.width(100.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getInsumo().getGrupo()));
                    cln.hide();
                }).build() /*Grupo*/,
                FormTableColumn.create(cln -> {
                    cln.title("Subgrupo");
                    cln.width(150.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getInsumo().getSubgrupo()));
                    cln.hide();
                }).build() /*Subgrupo*/,
                FormTableColumn.create(cln -> {
                    cln.title("Qtde");
                    cln.width(90.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                    cln.alignment(FormTableColumn.Alignment.RIGHT);
                    cln.format(param -> {
                        return new TableCell<VSdGestaoMatFaltante, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                                }
                            }
                        };
                    });
                }).build() /*Qtde*/,
                FormTableColumn.create(cln -> {
                    cln.title("Status");
                    cln.width(120.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue().getStatus()));
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.format(param -> {
                        return new TableCell<VSdGestaoMatFaltante, String>() {
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setText(item.equals("A") ? "Aberto" : item.equals("C") ? "Com Compra" : "Resolvido");
                                    setGraphic(ImageUtils.getIcon(item.equals("A") ? ImageUtils.Icon.STATUS_VERMELHO : item.equals("C") ? ImageUtils.Icon.STATUS_AMARELO : ImageUtils.Icon.STATUS_VERDE, ImageUtils.IconSize._16));
                                    if (item.equals("C"))
                                        setGraphic(FormButton.create(btn -> {
                                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.STATUS_AMARELO, ImageUtils.IconSize._16));
                                            btn.tooltip("Visualizar Ordens de Compra");
                                            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                            btn.addStyle("xs").addStyle("info");
                                            btn.setAction(evt -> {
                                                new Fragment().show(fragment -> {
                                                    fragment.title("Visualizar Ordens de Compras");
                                                    fragment.size(500.0, 400.0);
                                                    VSdGestaoMatFaltante materialSelecionado = (VSdGestaoMatFaltante) table.selectedItem();
                                                    final FormTableView<VSdComprasMaterial> tblComprasMaterial = FormTableView.create(VSdComprasMaterial.class, tableCompras -> {
                                                        tableCompras.title("Ordens de Compra/Tecelagem");
                                                        tableCompras.expanded();
                                                        tableCompras.items.set(FXCollections.observableList((List<VSdComprasMaterial>) new FluentDao().selectFrom(VSdComprasMaterial.class)
                                                                .where(it -> it
                                                                        .equal("codigo", materialSelecionado.getInsumo().getCodigo())
                                                                        .equal("cor", materialSelecionado.getCori().getCor()))
                                                                .resultList()));
                                                        tableCompras.columns(
                                                                FormTableColumn.create(cln -> {
                                                                    cln.title("O.C.");
                                                                    cln.width(80.0);
                                                                    cln.value((Callback<TableColumn.CellDataFeatures<VSdComprasMaterial, VSdComprasMaterial>, ObservableValue<VSdComprasMaterial>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumero()));
                                                                    cln.alignment(FormTableColumn.Alignment.CENTER);
                                                                }).build() /*O.C.*/,
                                                                FormTableColumn.create(cln -> {
                                                                    cln.title("Depósito");
                                                                    cln.width(80.0);
                                                                    cln.value((Callback<TableColumn.CellDataFeatures<VSdComprasMaterial, VSdComprasMaterial>, ObservableValue<VSdComprasMaterial>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDeposito()));
                                                                    cln.alignment(FormTableColumn.Alignment.CENTER);
                                                                }).build() /*Depósito*/,
                                                                FormTableColumn.create(cln -> {
                                                                    cln.title("Qtde");
                                                                    cln.width(100.0);
                                                                    cln.value((Callback<TableColumn.CellDataFeatures<VSdComprasMaterial, VSdComprasMaterial>, ObservableValue<VSdComprasMaterial>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                                                                    cln.alignment(FormTableColumn.Alignment.RIGHT);
                                                                    cln.format(param -> {
                                                                        return new TableCell<VSdComprasMaterial, BigDecimal>() {
                                                                            @Override
                                                                            protected void updateItem(BigDecimal item, boolean empty) {
                                                                                super.updateItem(item, empty);
                                                                                setText(null);
                                                                                if (item != null && !empty) {
                                                                                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                                                                                }
                                                                            }
                                                                        };
                                                                    });
                                                                }).build() /*Qtde*/,
                                                                FormTableColumn.create(cln -> {
                                                                    cln.title("Faturado em");
                                                                    cln.width(90.0);
                                                                    cln.value((Callback<TableColumn.CellDataFeatures<VSdComprasMaterial, VSdComprasMaterial>, ObservableValue<VSdComprasMaterial>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDtfatura()));
                                                                    cln.alignment(FormTableColumn.Alignment.CENTER);
                                                                    cln.format(param -> {
                                                                        return new TableCell<VSdComprasMaterial, LocalDate>() {
                                                                            @Override
                                                                            protected void updateItem(LocalDate item, boolean empty) {
                                                                                super.updateItem(item, empty);
                                                                                setText(null);
                                                                                if (item != null && !empty) {
                                                                                    setText(StringUtils.toDateFormat(item));
                                                                                }
                                                                            }
                                                                        };
                                                                    });
                                                                }).build() /*Faturado em*/,
                                                                FormTableColumn.create(cln -> {
                                                                    cln.title("Ent. Prevista");
                                                                    cln.width(80.0);
                                                                    cln.value((Callback<TableColumn.CellDataFeatures<VSdComprasMaterial, VSdComprasMaterial>, ObservableValue<VSdComprasMaterial>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDtentrega()));
                                                                    cln.alignment(FormTableColumn.Alignment.CENTER);
                                                                    cln.format(param -> {
                                                                        return new TableCell<VSdComprasMaterial, LocalDate>() {
                                                                            @Override
                                                                            protected void updateItem(LocalDate item, boolean empty) {
                                                                                super.updateItem(item, empty);
                                                                                setText(null);
                                                                                if (item != null && !empty) {
                                                                                    setText(StringUtils.toDateFormat(item));
                                                                                }
                                                                            }
                                                                        };
                                                                    });
                                                                }).build() /*Ent. Prevista*/,
                                                                FormTableColumn.create(cln -> {
                                                                    cln.title("Ações");
                                                                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                                                                    cln.width(80.0);
                                                                    cln.alignment(FormTableColumn.Alignment.CENTER);
                                                                    cln.value((Callback<TableColumn.CellDataFeatures<VSdComprasMaterial, VSdComprasMaterial>, ObservableValue<VSdComprasMaterial>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                                                    cln.format(param -> {
                                                                        return new TableCell<VSdComprasMaterial, VSdComprasMaterial>() {
                                                                            final HBox boxButtonsRow = new HBox(3);
                                                                            final Button btnFaturaOc = FormButton.create(btn1 -> {
                                                                                btn1.icon(ImageUtils.getIcon(ImageUtils.Icon.FATURAR_PEDIDO, ImageUtils.IconSize._16));
                                                                                btn1.tooltip("Visualizar Pedido e Fatura");
                                                                                btn1.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                                                                btn1.addStyle("xs").addStyle("success");
                                                                            });
                                                                            final Button btnObservacaoOc = FormButton.create(btn2 -> {
                                                                                btn2.icon(ImageUtils.getIcon(ImageUtils.Icon.INFORMACAO, ImageUtils.IconSize._16));
                                                                                btn2.tooltip("Visualizar Observações");
                                                                                btn2.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                                                                btn2.addStyle("xs").addStyle("info");
                                                                            });
                                                                        
                                                                            @Override
                                                                            protected void updateItem(VSdComprasMaterial item, boolean empty) {
                                                                                super.updateItem(item, empty);
                                                                                setText(null);
                                                                                setGraphic(null);
                                                                                if (item != null && !empty) {
                                                                                
                                                                                    btnFaturaOc.setOnAction(evt -> {
                                                                                        MessageBox.create(message -> {
                                                                                            message.message("Pedido: " + item.getPedfornecedor() + "\n" +
                                                                                                    "Fatura: " + item.getNffornecedor());
                                                                                            message.type(MessageBox.TypeMessageBox.INPUT);
                                                                                            message.showAndWait();
                                                                                        });
                                                                                    });
                                                                                    btnObservacaoOc.setOnAction(evt -> {
                                                                                        MessageBox.create(message -> {
                                                                                            message.message(item.getObservacao());
                                                                                            message.type(MessageBox.TypeMessageBox.INPUT);
                                                                                            message.showAndWait();
                                                                                        });
                                                                                    });
                                                                                    boxButtonsRow.getChildren().clear();
                                                                                    boxButtonsRow.getChildren().addAll(btnFaturaOc, btnObservacaoOc);
                                                                                    setGraphic(boxButtonsRow);
                                                                                }
                                                                            }
                                                                        };
                                                                    });
                                                                }).build()
                                                        );
                                                    });
                                                    fragment.box.getChildren().add(tblComprasMaterial.build());
                                                });
                                            });
                                        }));
                                }
                            }
                        };
                    });
                }).build() /*Status*/,
                FormTableColumn.create(cln -> {
                    cln.title("Extras");
                    cln.width(40.0);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdGestaoMatFaltante, VSdGestaoMatFaltante>, ObservableValue<VSdGestaoMatFaltante>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<VSdGestaoMatFaltante, VSdGestaoMatFaltante>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnObservacoes = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.INFORMACAO, ImageUtils.IconSize._16));
                                btn.tooltip("Ver Observações da OF");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("info");
                            });
                        
                            @Override
                            protected void updateItem(VSdGestaoMatFaltante item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    btnObservacoes.setOnAction(evt -> {
                                        MessageBox.create(message -> {
                                            message.message(item.getObsv());
                                            message.type(MessageBox.TypeMessageBox.INPUT);
                                            message.showAndWait();
                                        });
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnObservacoes);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build() /* Extras */
        );
        table.factoryRow(param -> {
            return new TableRow<VSdGestaoMatFaltante>() {
                @Override
                protected void updateItem(VSdGestaoMatFaltante item, boolean empty) {
                    super.updateItem(item, empty);
                    clear();
                
                    if (item != null && !empty) {
                        getStyleClass().add(item.getStatus().equals("A") ? "table-row-danger" : item.getStatus().equals("C") ? "table-row-warning" : "table-row-success");
                    
                    }
                }
            
                private void clear() {
                    getStyleClass().removeAll("table-row-danger", "table-row-success", "table-row-warning");
                }
            };
        });
        table.contextMenu(FormContextMenu.create(menu -> {
            menu.addItem(item -> {
                item.setText("Marcar como Baixado");
                item.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.BAIXAR_MATERIAL, ImageUtils.IconSize._16));
                item.setOnAction(evt -> {
                    if ((Boolean) QuestionBox.build(Boolean.class, message -> {
                            message.message("Você deseja realmente atualizar o status da falta para baixado?");
                            message.showAndWait();
                        }).value.get()) {
                        considerarFaltaComoBaixado((VSdGestaoMatFaltante) table.selectedItem());
                    }
                });
            });
        }));
        table.indices(
                table.indice(Color.LIGHTCORAL, "Falta em Aberta", false),
                table.indice(Color.LIGHTYELLOW, "Faltas com Compra", false),
                table.indice(Color.LIGHTGREEN, "Faltas Resolvidas", false)
        );
    });
    // </editor-fold>
    // </editor-fold>
    
    public AnaliseBaixaAutomaticaView() {
        super("Análise de Baixa Automática", ImageUtils.getImage(ImageUtils.Icon.BAIXA_MATERIAL), new String[]{"Listagem", "Manutenção","Faltas"});
        init();
    }
    
    private void init() {
        this.initListagemTab();
        this.initManutencaoTab();
        this.initFaltasTab();
        
        this.loadOfsPendentes();
        super.getFaltantes(null, null, null, null, "T", "A", null, "A");
    }
    
    private VSdEstoqueMatCorBr getSelectedCor(ObservableList<VSdEstoqueMatCorBr> list, String cor){
        Optional<VSdEstoqueMatCorBr> optSelect = list.stream().filter(i -> i.getCor().getCor().equals(cor)).distinct().findFirst();
        VSdEstoqueMatCorBr select = optSelect.isPresent() ? optSelect.get() : null;
        if (select != null) {
            int indexSelected = list.indexOf(select);
            list.remove(indexSelected);
            list.add(0, select);
        }
        return select;
    }
    
    private void initListagemTab() {
        listagemTab.getChildren().add(FormBox.create(boxHeader -> {
            boxHeader.horizontal();
            boxHeader.add(FormTitledPane.create(pane -> {
                pane.filter();
                pane.collapse(false);
                pane.add(FormBox.create(boxFilter -> {
                    boxFilter.horizontal();
                    boxFilter.add(FormBox.create(box -> {
                        box.vertical();
                        box.add(filtroStatusCorteField.build());
                        box.add(filtroStatusCosturaField.build());
                        box.add(filtroStatusAcabamentoField.build());
                        box.add(filtroStatusOutrosField.build());
                    }));
                    boxFilter.add(FormBox.create(box -> {
                        box.vertical();
                        box.add(filtroOfField.build());
                        box.add(filtroSetor.build());
                        box.add(filtroMarca.build());
                        box.add(filtroProduto.build());
                    }));
                    boxFilter.add(FormBox.create(box -> {
                        box.vertical();
                        box.add(filtroColecao.build());
                        box.add(filtroLinha.build());
                        box.add(filtroFamilia.build());
                        box.add(filtroLavanderia.build());
                    }));
                    boxFilter.add(FormBox.create(box -> {
                        box.vertical();
                        box.add(filtroFaccao.build());
                        box.add(filtroPais.build());
                    }));
                }));
                pane.find.setOnAction(evt -> {
                    super.loadOfsPendentes(
                            filtroStatusCorteField.value.get(),
                            filtroStatusCosturaField.value.get(),
                            filtroStatusAcabamentoField.value.get(),
                            filtroStatusOutrosField.value.get(),
                            filtroLavanderia.value.get(),
                            filtroOfField.value.get() == null || filtroOfField.value.get().equals("") ? null : filtroOfField.value.get().split(","),
                            filtroMarca.objectValues.stream().map(it -> it.getCodigoFilter()).toArray(),
                            filtroProduto.objectValues.stream().map(it -> it.getCodigoFilter()).toArray(),
                            filtroColecao.objectValues.stream().map(it -> it.getCodigoFilter()).toArray(),
                            filtroLinha.objectValues.stream().map(it -> it.getCodigoFilter()).toArray(),
                            filtroFamilia.objectValues.stream().map(it -> it.getCodigoFilter()).toArray(),
                            filtroSetor.objectValues.stream().map(it -> it.getCodigoFilter()).toArray(),
                            filtroFaccao.objectValues.stream().map(it -> it.getCodigoFilter()).toArray(),
                            filtroPais.value.get()
                    );
                });
                pane.clean.setOnAction(evt -> {
                    filtroStatusCorteField.select(0);
                    filtroStatusCosturaField.select(0);
                    filtroStatusAcabamentoField.select(0);
                    filtroStatusOutrosField.select(0);
                    filtroFaltPais.select(0);
                    filtroLavanderia.select(0);
                    filtroOfField.clear();
                    filtroSetor.clear();
                    filtroProduto.clear();
                    filtroMarca.clear();
                    filtroColecao.clear();
                    filtroLinha.clear();
                    filtroFamilia.clear();
                    filtroFaccao.clear();
                });
            }));
        }));
        listagemTab.getChildren().add(tblOfs.build());
    }
    
    private void initManutencaoTab() {
        manutencaoTab.getChildren().add(FormBox.create(boxHeader -> {
            boxHeader.horizontal();
            boxHeader.add(FormBox.create(boxDadosOf -> {
                boxDadosOf.horizontal();
                boxDadosOf.title("Dados O.F.");
                boxDadosOf.add(FormBox.create(box -> {
                    box.vertical();
                    box.add(FormBox.create(box1 -> {
                        box1.horizontal();
                        box1.add(ofNumero.build());
                        box1.add(ofPeriodo.build());
                        box1.add(ofMarca.build());
                        box1.add(ofProduto.build());
                        box1.add(ofColecao.build());
                    }));
                    box.add(FormBox.create(box1 -> {
                        box1.horizontal();
                        box1.add(ofSetorAtual.build());
                        box1.add(ofFaccao.build());
                        box1.add(ofFluxo.build());
                    }));
                }));
            }));
            boxHeader.add(FormBox.create(boxActions -> {
                boxActions.vertical();
                boxActions.expanded();
                boxActions.alignment(Pos.BOTTOM_LEFT);
                boxActions.add(FormButton.create(btn -> {
                    btn.title("Enviar O.F. para coleta");
                    btn.icon(ImageUtils.getIconButton(ImageUtils.ButtonIcon.SEND, ImageUtils.IconSize._24));
                    btn.addStyle("success");
                    btn.addStyle("lg");
                    btn.disableProperty().bind(isColetaBtn.not());
                    btn.setAction(evt -> {
                        sendColetaOf(ofSelecionada.get());
                        MessageBox.create(message -> {
                            message.message("OF enviada para programação de coleta.");
                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                            message.position(Pos.TOP_RIGHT);
                            message.notification();
                        });
                        JPAUtils.getEntityManager().refresh(ofSelecionada.get());
                        tblOfs.refresh();
                    });
                }));
            }));
        }));
        manutencaoTab.getChildren().add(FormBox.create(boxTables -> {
            boxTables.horizontal();
            boxTables.expanded();
            boxTables.add(FormBox.create(boxTableReservas -> {
                boxTableReservas.vertical();
                boxTableReservas.expanded();
                boxTableReservas.add(tblReserva.build());
                boxTableReservas.add(FormBox.create(boxButtonsReserva -> {
                    boxButtonsReserva.horizontal();
                    boxButtonsReserva.alignment(Pos.BOTTOM_RIGHT);
                    boxButtonsReserva.add(FormButton.create(btnExtornaAll -> {
                        btnExtornaAll.icon(ImageUtils.getIconButton(ImageUtils.ButtonIcon.NEGATIVE, ImageUtils.IconSize._16));
                        btnExtornaAll.title("Extornar todos");
                        btnExtornaAll.addStyle("danger");
                        btnExtornaAll.disableProperty().bind(reservaMateriaisOf.emptyProperty());
                        btnExtornaAll.setAction(evt -> {
                            if (reservaMateriaisOf.stream().map(it -> it.isColeta()).distinct().filter(it -> it).count() > 0) {
                                if (!(Boolean) QuestionBox.build(Boolean.class, message -> {
                                    message.message("Existem materiais com coleta nesta OF, deseja realmente estorar todos os materiais?");
                                    message.showAndWait();
                                }).value.get()) {
                                    return;
                                }
                            }
                            reservaMateriaisOf.forEach(it -> {
                                if (!it.getTipo().equals("M")) {
                                    estornarReservaMaterial(it, it.getQtdeB());
                                    SysLogger.addSysDelizLog("Analise Baixa Automatica", TipoAcao.EDITAR, it.getId().getNumero(),
                                            "Extorno do material: " + it.getId().getInsumo() + " na cor: " + it.getId().getCorI() + " qtde: " + StringUtils.toDecimalFormat(it.getQtdeB().doubleValue(), 4));
                                }
                            });
                            MessageBox.create(message -> {
                                message.message("Todos os materiais foram extornados.");
                                message.type(MessageBox.TypeMessageBox.CONFIRM);
                                message.position(Pos.TOP_RIGHT);
                                message.notification();
                            });
                        });
                    }));
                    boxButtonsReserva.add(FormButton.create(btnAbrirAll -> {
                        btnAbrirAll.icon(ImageUtils.getIconButton(ImageUtils.ButtonIcon.NEW_REGISTER, ImageUtils.IconSize._16));
                        btnAbrirAll.title("Abrir todos");
                        btnAbrirAll.addStyle("success");
                        btnAbrirAll.disableProperty().bind(reservaMateriaisOf.emptyProperty());
                        btnAbrirAll.setAction(evt -> {
                            reservaMateriaisOf.forEach(it -> {
                                if (it.getTipo().equals("N")) {
                                    abrirReservaMaterial(it);
                                    SysLogger.addSysDelizLog("Analise Baixa Automatica", TipoAcao.EDITAR, it.getId().getNumero(),
                                            "Aberto material: " + it.getId().getInsumo() + " na cor: " + it.getId().getCorI() + " qtde: " + StringUtils.toDecimalFormat(it.getQtdeB().doubleValue(), 4) + " para nova baixa.");
                                }
                            });
                            MessageBox.create(message -> {
                                message.message("Todos os materiais abertos para nova baixa.");
                                message.type(MessageBox.TypeMessageBox.CONFIRM);
                                message.position(Pos.TOP_RIGHT);
                                message.notification();
                            });
                        });
                    }));
                }));
            }));
            boxTables.add(tblFaltantesOf.build());
        }));
    }
    
    private void initFaltasTab() {
        faltasTab.getChildren().add(FormBox.create(boxHeaderFilter -> {
            boxHeaderFilter.horizontal();
            boxHeaderFilter.add(FormTitledPane.create(filter -> {
                filter.filter();
                filter.add(FormBox.create(boxFields -> {
                    boxFields.horizontal();
                    boxFields.add(FormBox.create(box -> {
                        box.vertical();
                        box.add(filtroFaltStatus.build());
                        box.add(FormBox.create(box1 -> {
                            box1.horizontal();
                            box1.add(filtroFaltSubstituto.build());
                            box1.add(filtroFaltCor.build());
                        }));
                        box.add(filtroFaltMaterial.build());
                    }));
                    boxFields.add(FormBox.create(box -> {
                        box.vertical();
                        box.add(filtroFaltNumero.build());
                        box.add(filtroFaltPeridodo.build());
                        box.add(filtroFaltSetor.build());
                    }));
                    boxFields.add(FormBox.create(box -> {
                        box.vertical();
                        box.add(filtroFaltTipoProduto.build());
                        box.add(filtroFaltPais.build());
                    }));
                }));
                filter.find.setOnAction(evt -> {
                    super.getFaltantes(
                            filtroFaltNumero.value.get() == null || filtroFaltNumero.value.get().equals("") ? null : filtroFaltNumero.value.get().split(","),
                            filtroFaltMaterial.objectValues.stream().map(it -> it.getCodigoFilter()).toArray(),
                            filtroFaltCor.objectValues.stream().map(it -> it.getCodigoFilter()).toArray(),
                            filtroFaltSetor.objectValues.stream().map(it -> it.getCodigoFilter()).toArray(),
                            filtroFaltSubstituto.value.get(),
                            filtroFaltStatus.value.get(),
                            filtroFaltTipoProduto.value.get(),
                            filtroPais.value.getValue()
                    );
                });
                filter.clean.setOnAction(evt -> {
                    filtroFaltCor.clear();
                    filtroFaltMaterial.clear();
                    filtroFaltNumero.clear();
                    filtroFaltSetor.clear();
                    filtroFaltPeridodo.clear();
                    filtroFaltStatus.select(0);
                    filtroFaltSubstituto.select(0);
                    filtroFaltTipoProduto.select(0);
                    filtroFaltPais.select(0);
                });
            }));
        }));
        faltasTab.getChildren().add(FormBox.create(boxTable -> {
            boxTable.vertical();
            boxTable.expanded();
            boxTable.add(tblFaltantes.build());
        }));
    }
    
    private void loadBaixasOf(VSdDadosOfPendente ofPendente) {
        ofNumero.value.set(ofPendente.getId().getNumero());
        ofPeriodo.value.set(ofPendente.getPeriodo());
        ofMarca.value.set(ofPendente.getCodigo().getMarca());
        ofProduto.value.set(ofPendente.getCodigo().toString());
        ofColecao.value.set(ofPendente.getCodigo().getColecao());
        ofSetorAtual.value.set(ofPendente.getId().getSetor().toString());
        ofFaccao.value.set(ofPendente.getFaccao().toString());
        ofFluxo.value.set(ofPendente.getDescfluxo());
        
        getReservasOf(ofPendente.getId().getNumero());
        getFaltantesOf(ofPendente.getId().getNumero());
        ofSelecionada.set(ofPendente);
        isColetaBtn.set(ofPendente.liberadocoletaProperty().not().get());
        tabs.getSelectionModel().select(1);
    }
}
