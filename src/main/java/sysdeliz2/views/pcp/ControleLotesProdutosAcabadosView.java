package sysdeliz2.views.pcp;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import sysdeliz2.controllers.views.pcp.ControleLotesProdutosAcabadosController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.sysdeliz.EntradaPA.*;
import sysdeliz2.models.sysdeliz.SdItemEmbarque;
import sysdeliz2.models.ti.Entidade;
import sysdeliz2.models.ti.Of1;
import sysdeliz2.models.ti.PaIten;
import sysdeliz2.models.view.*;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.ReportUtils;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class ControleLotesProdutosAcabadosView extends ControleLotesProdutosAcabadosController {

    // <editor-fold defaultstate="collapsed" desc="Bean">
    private final ListProperty<SdLotePa> lotesBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Table">
    private final FormTableView<SdLotePa> loteTbl = FormTableView.create(SdLotePa.class, table -> {
        table.title("Lotes");
        table.expanded();
        table.items.bind(lotesBean);
        table.factoryRow(param -> {
            return new TableRow<SdLotePa>() {
                @Override
                protected void updateItem(SdLotePa item, boolean empty) {
                    super.updateItem(item, empty);
                    clear();

                    if (item != null && !empty) {
                        if (item.getStatus().getCodigo() == 2) getStyleClass().add("table-row-primary");
                        if (item.getStatus().getCodigo() == 3) getStyleClass().add("table-row-info");
                        if (item.getStatus().getCodigo() == 4) getStyleClass().add("table-row-warning");
                        if (item.getStatus().getCodigo() == 5) getStyleClass().add("table-row-danger");
                        if (item.getStatus().getCodigo() == 6) getStyleClass().add("table-row-success");
                    }
                }

                private void clear() {
                    getStyleClass().removeAll("table-row-danger", "table-row-success", "table-row-warning", "table-row-info", "table-row-primary");
                }
            };
        });
        List<FormTableView<SdLotePa>.ItemIndice> indices = new ArrayList<>();
        indices.add(table.indice(Color.BLUE, "Em Leitura", true));
        indices.add(table.indice(Color.ALICEBLUE, "Encerrado", true));
        indices.add(table.indice(Color.YELLOW, "Em Transporte", true));
        indices.add(table.indice(Color.RED, "Em Recebimento", true));
        indices.add(table.indice(Color.GREEN, "Finalizado", true));
        table.indices(
                table.indice("Em Leitura", "primary", ""),
                table.indice("Encerrado", "info", ""),
                table.indice("Em Transporte", "warning", ""),
                table.indice("Em Recebimento", "danger", ""),
                table.indice("Finalizado", "success", "")
        );
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdLotePa, SdLotePa>, ObservableValue<SdLotePa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId()));
                }).build(), /**/
                FormTableColumn.create(cln -> {
                    cln.title("Dt Envio");
                    cln.width(200);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdLotePa, SdLotePa>, ObservableValue<SdLotePa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDtenvio() == null ? "" : StringUtils.toShortDateFormat(param.getValue().getDtenvio())));
                }).build(), /**/
                FormTableColumn.create(cln -> {
                    cln.title("Dt Emissão");
                    cln.width(200);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdLotePa, SdLotePa>, ObservableValue<SdLotePa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDtemissao() == null ? "" : StringUtils.toShortDateFormat(param.getValue().getDtemissao())));
                }).build(), /**/
                FormTableColumn.create(cln -> {
                    cln.title("CodFor");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdLotePa, SdLotePa>, ObservableValue<SdLotePa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodfor() == null ? "" : param.getValue().getCodfor().getCodcli()));
                }).build(), /**/
                FormTableColumn.create(cln -> {
                    cln.title("Nome Fornecedor");
                    cln.width(400);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdLotePa, SdLotePa>, ObservableValue<SdLotePa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodfor() == null ? "" : param.getValue().getCodfor().getNome()));
                }).build(), /**/
                FormTableColumn.create(cln -> {
                    cln.title("Status");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdLotePa, SdLotePa>, ObservableValue<SdLotePa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getStatus().getStatus()));
                }).build(), /**/
                FormTableColumn.create(cln -> {
                    cln.title("Qtde Itens");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdLotePa, SdLotePa>, ObservableValue<SdLotePa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                }).build(), /**/
                FormTableColumn.create(cln -> {
                    cln.title("Volumes");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdLotePa, SdLotePa>, ObservableValue<SdLotePa>>) param -> new ReadOnlyObjectWrapper(param.getValue().getVolumes()));
                }).build() /**/
        );
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Filter">
    private final FormFieldMultipleFind<SdLotePa> loteFilter = FormFieldMultipleFind.create(SdLotePa.class, field -> {
        field.title("Lote");
        field.width(250);
    });
    private final FormFieldMultipleFind<Entidade> faccaoFilter = FormFieldMultipleFind.create(Entidade.class, field -> {
        field.title("Facção");
        field.width(250);
    });

    private final FormFieldDate dtEmissaoInicioFilter = FormFieldDate.create(field -> {
        field.withoutTitle();
        field.width(125);
        field.value.setValue(LocalDate.now().minusWeeks(1));
    });
    private final FormFieldDate dtEmissaoFimFilter = FormFieldDate.create(field -> {
        field.withoutTitle();
        field.width(125);
        field.value.setValue(LocalDate.now().plusWeeks(1));
    });
    private final FormFieldDate dtEnvioInicioFilter = FormFieldDate.create(field -> {
        field.withoutTitle();
        field.width(125);
        field.value.setValue(LocalDate.now().minusWeeks(1));
    });
    private final FormFieldDate dtEnvioFimFilter = FormFieldDate.create(field -> {
        field.withoutTitle();
        field.width(125);
        field.value.setValue(LocalDate.now().plusWeeks(1));
    });

    private final FormFieldMultipleFind<Of1> ofLoteFilter = FormFieldMultipleFind.create(Of1.class, field -> {
        field.title("Of");
        field.width(250);
    });
    private final FormFieldMultipleFind<SdCaixaPA> caixaLoteFilter = FormFieldMultipleFind.create(SdCaixaPA.class, field -> {
        field.title("Caixa");
        field.width(250);
    });
    private final FormFieldMultipleFind<VSdDadosProduto> produtoFilter = FormFieldMultipleFind.create(VSdDadosProduto.class, field -> {
        field.title("Produto");
        field.width(250);
    });
    private final FormFieldMultipleFind<SdBarraCaixaPA> barraLoteFilter = FormFieldMultipleFind.create(SdBarraCaixaPA.class, field -> {
        field.title("Barra");
        field.width(250);
    });

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Buttons">
    private Button emTransporteBtn = FormButton.create(btn -> {
        btn.setText("Em Transporte");
        btn.addStyle("warning");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.AGENDA_ENTREGA, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            SdLotePa sdLotePa = loteTbl.selectedItem();
            if (sdLotePa != null) {
                definirLoteEmTransporte(sdLotePa);
            }
        });
    });

    private Button retirarOfsBtn = FormButton.create(btn -> {
        btn.setText("Retirar Of's");
        btn.addStyle("danger");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCELAR_PEDIDO, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            SdLotePa sdLotePa = loteTbl.selectedItem();
            if (sdLotePa != null) {
                retirarOf(sdLotePa);
            }
        });
    });

    private Button visualizarLoteBtn = FormButton.create(btn -> {
        btn.setText("Visualizar");
        btn.addStyle("primary");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.VISUALIZAR_PEDIDO, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            SdLotePa sdLotePa = loteTbl.selectedItem();
            if (sdLotePa != null) {
                visualizarLote(sdLotePa);
            }
        });
    });

    private Button imprimirRomaneioBtn = FormButton.create(btn -> {
        btn.setText("Imprimir Romaneio");
        btn.addStyle("info");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.IMPRIMIR, ImageUtils.IconSize._24));
        btn.setOnAction(evt -> {
            SdLotePa sdLotePa = loteTbl.selectedItem();
            if (sdLotePa != null) {
                imprimirRomaneio(sdLotePa);
            }
        });
    });

    // </editor-fold>

    public ControleLotesProdutosAcabadosView() {
        super("Controle Entrada de Produtos Acabados", ImageUtils.getImage(ImageUtils.Icon.AGENDA_ENTREGA));
        init();
    }

    private void init() {
        box.getChildren().add(FormBox.create(principal -> {
            principal.vertical();
            principal.expanded();
            principal.add(FormBox.create(boxHeader -> {
                boxHeader.horizontal();
                boxHeader.add(FormBox.create(boxTitledFilter -> {
                    boxTitledFilter.vertical();
                    boxTitledFilter.add(FormTitledPane.create(filter -> {
                        filter.filter();
                        filter.add(FormBox.create(boxFilter -> {
                            boxFilter.horizontal();
                            boxFilter.add(FormBox.create(boxLoteFilter -> {
                                boxLoteFilter.vertical();
                                boxLoteFilter.setPadding(new Insets(7, 0, 0, 0));
                                boxLoteFilter.width(250);
                                boxLoteFilter.add(FormBox.create(l1 -> {
                                    l1.horizontal();
                                    l1.add(loteFilter.build());
                                }));
                                boxLoteFilter.add(FormBox.create(l2 -> {
                                    l2.horizontal();
                                    l2.setPadding(new Insets(10, 0, 0, 0));
                                    l2.add(faccaoFilter.build());
                                }));
                            }));
                            boxFilter.add(new Separator(Orientation.VERTICAL));
                            boxFilter.add(FormBox.create(boxDataFilter -> {
                                boxDataFilter.vertical();
                                boxDataFilter.width(350);
                                boxDataFilter.add(FormBox.create(l1 -> {
                                    l1.horizontal();
                                    l1.title("Data de Emissão");
                                    l1.add(FormLabel.create(lb -> {
                                        lb.setText("De: ");
                                    }));
                                    l1.add(dtEmissaoInicioFilter.build());
                                    l1.add(FormLabel.create(lb -> {
                                        lb.setText("Até: ");
                                    }));
                                    l1.add(dtEmissaoFimFilter.build());
                                }));
                                boxDataFilter.add(FormBox.create(l2 -> {
                                    l2.horizontal();
                                    l2.setPadding(new Insets(7, 0, 0, 0));
                                    l2.title("Data de Envio");
                                    l2.add(FormLabel.create(lb -> {
                                        lb.setText("De: ");
                                    }));
                                    l2.add(dtEnvioInicioFilter.build());
                                    l2.add(FormLabel.create(lb -> {
                                        lb.setText("Até: ");
                                    }));
                                    l2.add(dtEnvioFimFilter.build());
                                }));
                            }));
                            boxFilter.add(new Separator(Orientation.VERTICAL));
                            boxFilter.add(FormBox.create(boxItensFilter -> {
                                boxItensFilter.vertical();
                                boxItensFilter.setPadding(new Insets(7, 0, 0, 0));
                                boxItensFilter.width(500);
                                boxItensFilter.add(FormBox.create(l1 -> {
                                    l1.horizontal();
                                    l1.add(ofLoteFilter.build());
                                    l1.add(produtoFilter.build());
                                }));
                                boxItensFilter.add(FormBox.create(l2 -> {
                                    l2.horizontal();
                                    l2.setPadding(new Insets(10, 0, 0, 0));
                                    l2.add(caixaLoteFilter.build());
                                    l2.add(barraLoteFilter.build());
                                }));
                            }));
                        }));
                        filter.find.setOnAction(evt -> {
                            new RunAsyncWithOverlay(this).exec(task -> {
                                buscarLotes(
                                        loteFilter.objectValues.isEmpty() ? new String[]{} : loteFilter.objectValues.getValue().stream().map(SdLotePa::getId).toArray(),
                                        faccaoFilter.objectValues.isEmpty() ? new String[]{} : faccaoFilter.objectValues.getValue().stream().map(Entidade::getCodcli).toArray(),
                                        ofLoteFilter.objectValues.getValue(),
                                        caixaLoteFilter.objectValues.getValue(),
                                        produtoFilter.objectValues.getValue(),
                                        barraLoteFilter.objectValues.getValue(),
                                        dtEnvioInicioFilter.value.getValue(),
                                        dtEnvioFimFilter.value.getValue(),
                                        dtEmissaoInicioFilter.value.getValue(),
                                        dtEmissaoFimFilter.value.getValue()
                                );
                                return ReturnAsync.OK.value;
                            }).addTaskEndNotification(taskReturn -> {
                                if (taskReturn.equals(ReturnAsync.OK.value)) {
                                    lotesBean.set(FXCollections.observableArrayList(listLotes));
                                }
                            });
                        });
                        filter.clean.setOnAction(evt -> {
                            loteFilter.clear();
                            faccaoFilter.clear();
                            dtEnvioInicioFilter.value.setValue(LocalDate.now());
                            dtEnvioFimFilter.value.setValue(LocalDate.now().plusWeeks(1L));
                            dtEmissaoInicioFilter.value.setValue(LocalDate.now());
                            dtEmissaoFimFilter.value.setValue(LocalDate.now().plusWeeks(1L));
                            ofLoteFilter.clear();
                            caixaLoteFilter.clear();
                            produtoFilter.clear();
                            barraLoteFilter.clear();
                        });
                    }));
                }));
            }));
            principal.add(FormBox.create(boxCenter -> {
                boxCenter.horizontal();
                boxCenter.expanded();
                boxCenter.add(loteTbl.build());
            }));
            principal.add(FormBox.create(boxFooter -> {
                boxFooter.horizontal();
                boxFooter.add(emTransporteBtn, retirarOfsBtn, visualizarLoteBtn, imprimirRomaneioBtn);
            }));
        }));
    }

    private void definirLoteEmTransporte(SdLotePa lote) {
        if (lote.getStatus().getCodigo() == 1) {
            MessageBox.create(message -> {
                message.message("Lote não encerrado ainda!");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }
        if (lote.getStatus().getCodigo() == 2) {
            MessageBox.create(message -> {
                message.message("Lote em edição ainda!");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }
        if (lote.getStatus().getCodigo() == 4) {
            MessageBox.create(message -> {
                message.message("Lote já está em transporte!");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }
        if (lote.getStatus().getCodigo() == 5) {
            MessageBox.create(message -> {
                message.message("Lote já foi finalizado!");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }

        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Tem certeza que deseja definir o lote em transporte?");
            message.showAndWait();
        }).value.get())) {
            lote.setStatus(new FluentDao().selectFrom(SdStatusLotePA.class).where(it -> it.equal("codigo", 4)).singleResult());
            lote = new FluentDao().merge(lote);
        }
    }

    private void retirarOf(SdLotePa lote) {
        if (lote.getStatus().getCodigo() != 3) {
            MessageBox.create(message -> {
                message.message("Só é possível remover as Of's do Lote se ele estiver encerrado");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }
        new Fragment().show(frag -> {
            frag.title.setText("Gerenciar Of's");
            frag.size(600.0, 600.0);

            FormTableView<SdItemLotePA> ofsLote = FormTableView.create(SdItemLotePA.class, table -> {
                table.title("Ofs");
                table.items.set(FXCollections.observableArrayList(lote.getItensLote()));
                table.expanded();
                table.columns(
                        FormTableColumn.create(cln -> {
                            cln.title("Ações");
                            cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                            cln.width(85);
                            cln.alignment(FormTableColumn.Alignment.CENTER);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdItemLotePA, SdItemLotePA>, ObservableValue<SdItemLotePA>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                            cln.format(param -> {
                                return new TableCell<SdItemLotePA, SdItemLotePA>() {
                                    final HBox boxButtonsRow = new HBox(2);
                                    final Button removerOf = FormButton.create(btn -> {
                                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                        btn.tooltip("Remover OF");
                                        btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                        btn.addStyle("sm").addStyle("danger");
                                    });
                                    final Button transferirOf = FormButton.create(btn -> {
                                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._16));
                                        btn.tooltip("Transferir Of");
                                        btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                        btn.addStyle("sm").addStyle("primary");
                                    });

                                    @Override
                                    protected void updateItem(SdItemLotePA item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        setGraphic(null);
                                        if (item != null && !empty) {

                                            removerOf.setOnAction(evt -> {
                                                if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                                                    message.message("Tem certeza que deseja remover a Of?");
                                                    message.showAndWait();
                                                }).value.get())) {
                                                    lote.getItensLote().remove(item);
                                                    table.items.remove(item);
                                                    calculaTotaisLote(lote);
                                                    new FluentDao().delete(item);
                                                    new FluentDao().merge(lote);
                                                    MessageBox.create(message -> {
                                                        message.message("Of removida com sucesso!");
                                                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                        message.position(Pos.TOP_RIGHT);
                                                        message.notification();
                                                    });
                                                }
                                            });
                                            transferirOf.setOnAction(evt -> {
                                                if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                                                    message.message("Tem certeza que deseja transferir a Of?");
                                                    message.showAndWait();
                                                }).value.get())) {
                                                    SdLotePa proxLote = new FluentDao().selectFrom(SdLotePa.class).where(it -> it
                                                                    .equal("codfor.codcli", lote.getCodfor().getCodcli())
                                                                    .notEqual("id", lote.getId())
                                                                    .greaterThanOrEqualTo("dtenvio", lote.getDtenvio()))
                                                            .findFirst();
                                                    if (proxLote == null) {
                                                        proxLote = new SdLotePa();
                                                        proxLote.setUsuario(lote.getUsuario());
                                                        proxLote.setCodfor(lote.getCodfor());
                                                        proxLote.setStatus(new FluentDao().selectFrom(SdStatusLotePA.class).where(it -> it.equal("codigo", 1)).singleResult());
                                                        proxLote.setDtenvio(janelaDataLote());
                                                        try {
                                                            proxLote = new FluentDao().persist(proxLote);
                                                        } catch (SQLException throwables) {
                                                            throwables.printStackTrace();
                                                        }
                                                        int idLote = proxLote.getId();
                                                        MessageBox.create(message -> {
                                                            message.message("Lote Futuro não encontrado, um novo lote, com o código " + idLote + " foi criado com essa Of");
                                                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                            message.showAndWait();
                                                        });

                                                    } else {
                                                        int idLote = proxLote.getId();
                                                        MessageBox.create(message -> {
                                                            message.message("Of transferida para o lote " + idLote);
                                                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                            message.showAndWait();
                                                        });
                                                    }
                                                    SdLotePa loteAnterior = loteTbl.selectedItem();
                                                    loteAnterior.getItensLote().remove(item);
                                                    proxLote.getItensLote().add(item);
                                                    calculaTotaisLote(loteAnterior);
                                                    calculaTotaisLote(proxLote);
                                                    new FluentDao().merge(proxLote);
                                                    new FluentDao().merge(loteAnterior);
                                                    table.items.remove(item);
                                                }
                                            });

                                            boxButtonsRow.getChildren().clear();
                                            boxButtonsRow.getChildren().addAll(removerOf, transferirOf);
                                            boxButtonsRow.setAlignment(Pos.CENTER);
                                            setGraphic(boxButtonsRow);
                                        }
                                    }
                                };
                            });
                        }).build(),
                        FormTableColumn.create(cln -> {
                            cln.title("Of");
                            cln.width(100);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdItemLotePA, SdItemLotePA>, ObservableValue<SdItemLotePA>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumero()));
                        }).build(), /*Of*/
                        FormTableColumn.create(cln -> {
                            cln.title("Produto");
                            cln.width(100);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdItemLotePA, SdItemLotePA>, ObservableValue<SdItemLotePA>>) param -> new ReadOnlyObjectWrapper(param.getValue().getProduto()));
                        }).build(), /*Of*/
                        FormTableColumn.create(cln -> {
                            cln.title("Qtde Lida");
                            cln.width(70);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdItemLotePA, SdItemLotePA>, ObservableValue<SdItemLotePA>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtdeLida()));
                        }).build(), /*Of*/
                        FormTableColumn.create(cln -> {
                            cln.title("Qtde Total");
                            cln.width(70);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdItemLotePA, SdItemLotePA>, ObservableValue<SdItemLotePA>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtdeTotal()));
                        }).build(), /*Of*/
                        FormTableColumn.create(cln -> {
                            cln.title("Volumes");
                            cln.width(90);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdItemLotePA, SdItemLotePA>, ObservableValue<SdItemLotePA>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtdecaixas()));
                        }).build() /*Of*/
                );
            });

            frag.box.getChildren().add(FormBox.create(principal -> {
                principal.horizontal();
                principal.expanded();
                principal.add(FormBox.create(boxOfs -> {
                    boxOfs.vertical();
                    boxOfs.expanded();
                    boxOfs.add(ofsLote.build());
                }));
            }));
        });
    }

    private void calculaTotaisLote(SdLotePa lote) {
        lote.setQtde(lote.getItensLote().stream().mapToInt(SdItemLotePA::getQtdeLida).sum());
        lote.setVolumes(lote.getItensLote().stream().mapToInt(SdItemLotePA::getQtdecaixas).sum());
        lote.setPeso(BigDecimal.valueOf(lote.getItensLote().stream().mapToDouble(it -> it.getPeso().doubleValue()).sum()));
    }

    private void visualizarLote(SdLotePa lote) {
        new FluentDao().getEntityManager().refresh(lote);

        new Fragment().show(frag -> {
            frag.size(1700.0, 800.0);
            frag.title("Resumo Dados Lote");

            final FormTabPane tabPane = FormTabPane.create();

            final FormTableView<SdItemLotePA> itensLoteTbl = FormTableView.create(SdItemLotePA.class, table -> {
                table.items.set(FXCollections.observableArrayList(lote.getItensLote()));
                table.withoutHeader();
                table.expanded();
                table.contextMenu(FormContextMenu.create(cmenu -> {
                    cmenu.addItem(menuItem -> {
                        menuItem.setText("Limpar Seleção");
                        menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.LIMPAR, ImageUtils.IconSize._16));
                        menuItem.setOnAction(evt -> {
                            table.tableview().getSelectionModel().clearSelection();
                        });
                    });
                }));
                table.factoryRow(param -> new TableRow<SdItemLotePA>() {
                    @Override
                    protected void updateItem(SdItemLotePA item, boolean empty) {
                        super.updateItem(item, empty);
                        clear();
                        if (item != null && !empty) {

                            if (item.getStatus().getCodigo() == 3 && !item.getListCaixas().stream().allMatch(SdCaixaPA::isRecebida)) {
                                getStyleClass().add("table-row-warning");
                            } else if (item.getStatus().getCodigo() == 3) {
                                getStyleClass().add("table-row-success");
                            } else {
                                getStyleClass().add("table-row-danger");
                            }
                        }
                    }

                    private void clear() {
                        getStyleClass().removeAll("table-row-danger", "table-row-success", "table-row-warning");
                    }
                });
                table.columns(
                        FormTableColumn.create(cln -> {
                            cln.title("OF");
                            cln.width(70);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdItemLotePA, SdItemLotePA>, ObservableValue<SdItemLotePA>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumero()));
                        }).build(), /*OF*/
                        FormTableColumn.create(cln -> {
                            cln.title("Produto");
                            cln.width(100);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdItemLotePA, SdItemLotePA>, ObservableValue<SdItemLotePA>>) param -> new ReadOnlyObjectWrapper(param.getValue().getProduto()));
                        }).build(), /*OF*/
                        FormTableColumn.create(cln -> {
                            cln.title("Qtde Caixas");
                            cln.width(100);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdItemLotePA, SdItemLotePA>, ObservableValue<SdItemLotePA>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtdecaixas()));
                        }).build(), /*OF*/
                        FormTableColumn.create(cln -> {
                            cln.title("Qtde Lida");
                            cln.width(80);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdItemLotePA, SdItemLotePA>, ObservableValue<SdItemLotePA>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtdeLida()));
                        }).build(), /*OF*/
                        FormTableColumn.create(cln -> {
                            cln.title("Qtde Perdida");
                            cln.width(80);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdItemLotePA, SdItemLotePA>, ObservableValue<SdItemLotePA>>) param -> new ReadOnlyObjectWrapper(param.getValue().getListCaixas().stream().filter(SdCaixaPA::isPerdida).mapToInt(SdCaixaPA::getQtde).sum()));
                        }).build(), /*OF*/
                        FormTableColumn.create(cln -> {
                            cln.title("Qtde Incompleta");
                            cln.width(80);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdItemLotePA, SdItemLotePA>, ObservableValue<SdItemLotePA>>) param -> new ReadOnlyObjectWrapper(param.getValue().getListCaixas().stream().filter(SdCaixaPA::isIncompleta).mapToInt(SdCaixaPA::getQtde).sum()));
                        }).build(), /*OF*/
                        FormTableColumn.create(cln -> {
                            cln.title("Qtde Total");
                            cln.width(80);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdItemLotePA, SdItemLotePA>, ObservableValue<SdItemLotePA>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtdeTotal()));
                        }).build()  /*OF*/
                );
            });

            final FormTableView<SdItemCaixaPA> itensCaixaTbl = FormTableView.create(SdItemCaixaPA.class, table -> {
                ListProperty<SdItemCaixaPA> itensCaixaBean = new SimpleListProperty<>(FXCollections.observableArrayList());
                generateItensCaixaBean(itensCaixaBean, null);
                table.items.bind(itensCaixaBean);
                itensLoteTbl.selectionModelItem((observable, oldValue, newValue) -> {
                    if (newValue != null) {
                        tabPane.getTabs().clear();
                        ((SdItemLotePA) newValue).getListCaixas().stream().sorted(Comparator.comparing(SdCaixaPA::getId)).forEach(cx -> {
                            tabPane.addTab(FormTab.create(tab -> {
                                createTabCaixa((SdItemLotePA) newValue, cx, tab);
                            }));
                        });
                        generateItensCaixaBean(itensCaixaBean, newValue);
                    } else {
                        tabPane.getTabs().clear();
                        generateItensCaixaBean(itensCaixaBean, null);
                    }
                });
                table.title("Resumo Total");
                table.expanded();
                table.columns(
                        FormTableColumn.create(cln -> {
                            cln.title("Produto");
                            cln.width(90);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdItemCaixaPA, SdItemCaixaPA>, ObservableValue<SdItemCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue().getProduto()));
                        }).build(), /*Produto*/
                        FormTableColumn.create(cln -> {
                            cln.title("Cor");
                            cln.width(70);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdItemCaixaPA, SdItemCaixaPA>, ObservableValue<SdItemCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCor()));
                        }).build(), /*Produto*/
                        FormTableColumn.create(cln -> {
                            cln.title("Tam");
                            cln.width(70);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdItemCaixaPA, SdItemCaixaPA>, ObservableValue<SdItemCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTam()));
                        }).build(), /*Produto*/
                        FormTableColumn.create(cln -> {
                            cln.title("Local");
                            cln.width(100);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdItemCaixaPA, SdItemCaixaPA>, ObservableValue<SdItemCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLocal()));
                        }).build(), /*Produto*/
                        FormTableColumn.create(cln -> {
                            cln.title("Qtde");
                            cln.width(60);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdItemCaixaPA, SdItemCaixaPA>, ObservableValue<SdItemCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                        }).build() /*Produto*/
                );
            });

            final FormFieldText codLoteField = FormFieldText.create(field -> {
                field.title("Lote");
                field.value.set(String.valueOf(lote.getId()));
                field.width(100);
                field.editable.set(false);
            });

            final FormFieldText dtEnvioField = FormFieldText.create(field -> {
                field.title("Dt Envio");
                field.value.set(StringUtils.toShortDateFormat(lote.getDtenvio()));
                field.width(100);
                field.editable.set(false);
            });

            final FormFieldText dtEmissaoField = FormFieldText.create(field -> {
                field.title("Dt Emissao");
                field.value.set(StringUtils.toShortDateFormat(lote.getDtemissao()));
                field.width(100);
                field.editable.set(false);
            });

            final FormFieldText qtdeLoteField = FormFieldText.create(field -> {
                field.title("Qtde");
                field.value.set(String.valueOf(lote.getQtde()));
                field.width(100);
                field.editable.set(false);
            });

            final FormFieldText volumesField = FormFieldText.create(field -> {
                field.title("Volumes");
                field.width(100);
                field.editable.set(false);
                field.value.set(String.valueOf(lote.getVolumes()));
            });

            final FormFieldText fornecedorField = FormFieldText.create(field -> {
                field.title("Fornecedor");
                field.width(310);
                field.editable.set(false);
                field.value.set(lote.getCodfor() == null ? "" : lote.getCodfor().getNome());
            });

            double widthField = 150;

            final FormFieldText qtdeOfsField = FormFieldText.create(field -> {
                field.title("Qtde Ofs");
                field.width(widthField);
                field.editable.set(false);
                field.value.set(String.valueOf(lote.getItensLote().size()));
            });

            final FormFieldText qtdeCaixasField = FormFieldText.create(field -> {
                field.title("Qtde Caixas Total");
                field.width(widthField);
                field.addStyle("warning");
                field.editable.set(false);
                field.value.set(String.valueOf(lote.getItensLote().stream().mapToInt(SdItemLotePA::getQtdecaixas).sum()));
            });

            final FormFieldText qtdeCaixasBoasField = FormFieldText.create(field -> {
                field.title("Qtde Caixas Boas");
                field.width(widthField);
                field.editable.set(false);
                field.addStyle("success");
                field.value.set(String.valueOf(lote.getItensLote().stream().mapToInt(it -> (int) it.getListCaixas().stream().filter(cx -> !cx.isIncompleta() && !cx.isPerdida()).count()).sum()));
            });

            final FormFieldText qtdeCaixasSegundaField = FormFieldText.create(field -> {
                field.title("Qtde Caixas Segunda");
                field.width(widthField);
                field.editable.set(false);
                field.addStyle("info");
                field.value.set(String.valueOf(lote.getItensLote().stream().mapToInt(it -> (int) it.getListCaixas().stream().filter(SdCaixaPA::isSegunda).count()).sum()));
            });

            final FormFieldText qtdePecasTotal = FormFieldText.create(field -> {
                field.title("Qtde Total");
                field.width(widthField);
                field.editable.set(false);
                field.value.set(String.valueOf(lote.getItensLote().stream().mapToInt(SdItemLotePA::getQtdeTotal).sum()));
            });

            final FormFieldText qtdePecasLidasField = FormFieldText.create(field -> {
                field.title("Peças Lidas");
                field.width(widthField);
                field.editable.set(false);
                field.addStyle("warning");
                field.value.set(String.valueOf(lote.getItensLote().stream().mapToInt(it -> it.getListCaixas().stream().filter(cx -> !cx.isIncompleta() && !cx.isPerdida()).mapToInt(SdCaixaPA::getQtde).sum()).sum()));
            });

            final FormFieldText qtdePecasBoasField = FormFieldText.create(field -> {
                field.title("Peças Boas");
                field.width(widthField);
                field.editable.set(false);
                field.addStyle("success");
                field.value.set(String.valueOf(lote.getItensLote().stream().mapToInt(it -> it.getListCaixas().stream().filter(cx -> !cx.isSegunda() && !cx.isIncompleta() && !cx.isPerdida()).mapToInt(SdCaixaPA::getQtde).sum()).sum()));
            });

            final FormFieldText qtdePecasSegundaField = FormFieldText.create(field -> {
                field.title("Peças Segunda");
                field.width(widthField);
                field.editable.set(false);
                field.addStyle("info");
                field.value.set(String.valueOf(lote.getItensLote().stream().mapToInt(it -> it.getListCaixas().stream().filter(SdCaixaPA::isSegunda).mapToInt(SdCaixaPA::getQtde).sum()).sum()));
            });

            final FormFieldText qtdePecasPerdidasField = FormFieldText.create(field -> {
                field.title("Peças Perdidas");
                field.width(widthField);
                field.editable.set(false);
                field.addStyle("danger");
                field.value.set(String.valueOf(lote.getItensLote().stream().mapToInt(it -> it.getListCaixas().stream().filter(SdCaixaPA::isPerdida).mapToInt(SdCaixaPA::getQtde).sum()).sum()));
            });

            final FormFieldText qtdePecasIncompletasField = FormFieldText.create(field -> {
                field.title("Peças Incompletas");
                field.width(widthField);
                field.editable.set(false);
                field.addStyle("secundary");
                field.value.set(String.valueOf(lote.getItensLote().stream().mapToInt(it -> it.getListCaixas().stream().filter(SdCaixaPA::isIncompleta).mapToInt(SdCaixaPA::getQtde).sum()).sum()));
            });

            frag.box.getChildren().add(FormBox.create(principal -> {
                principal.horizontal();
                principal.expanded();
                principal.add(FormBox.create(col1 -> {
                    col1.vertical();
                    col1.add(FormBox.create(boxDadosGerais -> {
                        boxDadosGerais.vertical();
                        boxDadosGerais.title("Infos");
                        boxDadosGerais.size(550, 150);
                        boxDadosGerais.add(FormBox.create(l1 -> {
                            l1.horizontal();
                            l1.add(codLoteField.build(), fornecedorField.build());
                        }));
                        boxDadosGerais.add(FormBox.create(l2 -> {
                            l2.horizontal();
                            l2.add(dtEmissaoField.build(), dtEnvioField.build(), qtdeLoteField.build(), volumesField.build());
                        }));
                    }));
                    col1.add(FormBox.create(boxTblOfs -> {
                        boxTblOfs.vertical();
                        boxTblOfs.title("Ofs");
                        boxTblOfs.expanded();
                        boxTblOfs.add(itensLoteTbl.build());
                    }));

                }));
                principal.add(FormBox.create(col2 -> {
                    col2.vertical();
                    col2.expanded();
                    col2.add(FormBox.create(l1 -> {
                        l1.vertical();
                        l1.setMaxHeight(150);
                        l1.expanded();
                        l1.title("Totais");
                        l1.add(FormBox.create(linha1 -> {
                            linha1.horizontal();
                            linha1.add(qtdeOfsField.build(), qtdeCaixasField.build(), qtdeCaixasBoasField.build(), qtdeCaixasSegundaField.build());
                        }));
                        l1.add(FormBox.create(linha2 -> {
                            linha2.horizontal();
                            linha2.add(qtdePecasTotal.build(), qtdePecasLidasField.build(), qtdePecasBoasField.build(), qtdePecasSegundaField.build(), qtdePecasPerdidasField.build(), qtdePecasIncompletasField.build());
                        }));
                    }));
                    col2.add(FormBox.create(l2 -> {
                        l2.horizontal();
                        l2.expanded();
                        l2.add(FormBox.create(col1 -> {
                            col1.horizontal();
                            col1.expanded();
                            col1.add(itensCaixaTbl.build());
                        }));
                        l2.add(FormBox.create(coluna2 -> {
                            coluna2.horizontal();
                            coluna2.expanded();
                            coluna2.title("Resumo Caixas");
                            tabPane.expanded();
                            coluna2.add(tabPane);
                        }));
                    }));
                }));
            }));
        });
    }

    private void createTabCaixa(SdItemLotePA newValue, SdCaixaPA cx, FormTab tab) {
        tab.setText(String.valueOf(cx.getId()));
        tab.setGraphic(FormBox.create(boxIcons -> {
            boxIcons.horizontal();
            boxIcons.alignment(Pos.CENTER);
            if (!cx.isRecebida() && cx.getItemlote().getStatus().getCodigo() == 3) {
                boxIcons.add(ImageUtils.getIcon(ImageUtils.Icon.ALERT_WARNING, ImageUtils.IconSize._16));
                boxIcons.tooltip("Caixa não recebida!");
            }
            boxIcons.add(ImageUtils.getIcon(getCaixaIcon(cx), ImageUtils.IconSize._16));
        }));
        tab.setContent(FormBox.create(principal -> {
            principal.horizontal();
            principal.add(FormBox.create(boxTotais -> {
                boxTotais.vertical();
                boxTotais.add(
                        FormFieldText.create(field -> {
                            field.title("Nº Caixa");
                            field.editable.set(false);
                            field.value.setValue(String.valueOf(cx.getId()));
                        }).build());
                boxTotais.add(
                        FormFieldText.create(field -> {
                            field.title("Produto");
                            field.editable.set(false);
                            field.value.setValue(cx.getProduto());
                        }).build());
                boxTotais.add(
                        FormFieldText.create(field -> {
                            field.title("Qtde");
                            field.editable.set(false);
                            field.value.setValue(String.valueOf(cx.getItensCaixa().stream().mapToInt(it -> it.getQtde()).sum()));
                        }).build()
                );
                boxTotais.add(
                        FormFieldText.create(field -> {
                            field.title("Peso");
                            field.editable.set(false);
                            field.value.setValue(String.valueOf(cx.getPeso()));
                        }).build()
                );
            }));
            principal.add(FormBox.create(boxTabela -> {
                boxTabela.vertical();
                boxTabela.add(FormTableView.create(SdItemCaixaPA.class, table2 -> {
                    table2.items.set(FXCollections.observableArrayList(cx.getItensCaixa().stream().sorted(Comparator.comparing(SdItemCaixaPA::getCor).thenComparing(((o1, o2) -> sortTamanhos(o1.getTam(), o2.getTam())))).collect(Collectors.toList())));
                    table2.title("Of " + newValue.getNumero());
                    table2.columns(
                            FormTableColumn.create(cln -> {
                                cln.title("Cor");
                                cln.width(90);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdItemCaixaPA, SdItemCaixaPA>, ObservableValue<SdItemCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCor()));

                            }).build(), /*Produto*/
                            FormTableColumn.create(cln -> {
                                cln.title("Tamanho");
                                cln.width(90);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdItemCaixaPA, SdItemCaixaPA>, ObservableValue<SdItemCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTam()));

                            }).build(), /*Produto*/
                            FormTableColumn.create(cln -> {
                                cln.title("Qtde");
                                cln.width(90);
                                cln.value((Callback<TableColumn.CellDataFeatures<SdItemCaixaPA, SdItemCaixaPA>, ObservableValue<SdItemCaixaPA>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));

                            }).build() /*Produto*/
                    );
                }).build());
            }));
        }));
    }

    private void generateItensCaixaBean(ListProperty<SdItemCaixaPA> itensCaixaBean, SdItemLotePA sdItemLotePA) {
        itensCaixaBean.clear();

        if (sdItemLotePA != null) {
            Map<String, Map<String, List<SdItemCaixaPA>>> map = sdItemLotePA.getListCaixas().stream().filter(it -> !it.isIncompleta() && !it.isPerdida()).flatMap(it -> it.getItensCaixa().stream()).collect(Collectors.groupingBy(SdItemCaixaPA::getCor, Collectors.groupingBy(SdItemCaixaPA::getTam)));
            map.forEach((cor, hMap) -> {
                hMap.forEach((tam, list) -> {
                    itensCaixaBean.add(new SdItemCaixaPA(sdItemLotePA.getProduto(), cor, tam, list.stream().mapToInt(SdItemCaixaPA::getQtde).sum()));
                });
            });
        } else itensCaixaBean.set(FXCollections.observableArrayList(getListItensCaixa(loteTbl.selectedItem())));

        ordenaItensCaixa(itensCaixaBean);
        carregaLocaisProdutos(itensCaixaBean);
    }

    private void ordenaItensCaixa(ListProperty<SdItemCaixaPA> itensCaixaBean) {
        itensCaixaBean.set(FXCollections.observableArrayList(itensCaixaBean.stream().sorted(Comparator.comparing(SdItemCaixaPA::getProduto).thenComparing(SdItemCaixaPA::getCor).thenComparing(((o1, o2) -> sortTamanhos(o1.getTam(), o2.getTam())))).collect(Collectors.toList())));
    }

    private void carregaLocaisProdutos(ListProperty<SdItemCaixaPA> itensCaixaBean) {
        for (SdItemCaixaPA itemCaixa : itensCaixaBean) {
            if (itemCaixa.getLocal() == null || itemCaixa.getLocal().equals("") || itemCaixa.getLocal().equals("S/LOCAL")) {
                VSdDadosProdutoBarra local = new FluentDao().selectFrom(VSdDadosProdutoBarra.class).where(it -> it.equal("codigo", itemCaixa.getProduto()).equal("cor", itemCaixa.getCor()).equal("tam", itemCaixa.getTam())).singleResult();
                if (local.getLocal() != null) itemCaixa.setLocal(local.getLocal());
                else itemCaixa.setLocal("S/LOCAL");
            }
        }
    }

    private List<SdItemCaixaPA> getListItensCaixa(SdLotePa lote) {
        List<SdItemCaixaPA> itens = new ArrayList<>();
        try {
            List<Object> objects = new NativeDAO().runNativeQuery(String.format("select \n" +
                    "  bar.produto, \n" +
                    "  bar.cor, \n" +
                    "  bar.tam, \n" +
                    "  count(bar.barra) qtde\n" +
                    "  from sd_lote_pa_001 lote\n" +
                    "  join sd_item_lote_pa_001 ilote\n" +
                    "    on ilote.lote = lote.id\n" +
                    "  join sd_caixa_pa_001 cx\n" +
                    "    on cx.item_lote = ilote.id\n" +
                    "  join sd_item_caixa_pa_001 icx\n" +
                    "    on icx.caixa = cx.id\n" +
                    "  join sd_barra_caixa_pa_001 bar\n" +
                    "    on bar.itemcaixa = icx.id\n" +
                    "  join produto_001 prd\n" +
                    "    on prd.codigo = bar.produto\n" +
                    "  join faixa_iten_001 fiten\n" +
                    "    on fiten.faixa = prd.faixa\n" +
                    "   and fiten.tamanho = bar.tam\n" +
                    "\n" +
                    " where lote.id = '%s' \n" +
                    " and cx.incompleta = 'N' \n" +
                    "  \n" +
                    "\n" +
                    " group by bar.produto, bar.cor, bar.tam, fiten.posicao\n" +
                    " order by bar.cor, fiten.posicao", lote.getId()));

            for (Object item : objects) {
                HashMap<String, Object> map = (HashMap<String, Object>) item;
                itens.add(new SdItemCaixaPA(map.get("PRODUTO"), map.get("COR"), map.get("TAM"), map.get("QTDE")));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return itens;
    }

    private void imprimirRomaneio(SdLotePa lote) {

        if (lote.getStatus().getCodigo() == 1) {
            MessageBox.create(message -> {
                message.message("Lote não encerrado ainda!");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }
        if (lote.getStatus().getCodigo() == 2) {
            MessageBox.create(message -> {
                message.message("Lote em edição ainda!");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }

        try {
            new ReportUtils().config().addReport(ReportUtils.ReportFile.ROMANEIO_PRODUTOS_ACABADOS,
                            new ReportUtils.ParameterReport("codEmpresa", "1000"),
                            new ReportUtils.ParameterReport("lote", lote.getId().toString()),
                            new ReportUtils.ParameterReport("faccao", lote.getCodfor().getCodcli() + " - " + lote.getCodfor().getNome()),
                            new ReportUtils.ParameterReport("dtEmissao", StringUtils.toShortDateFormat(lote.getDtemissao())),
                            new ReportUtils.ParameterReport("dtEnvio", StringUtils.toShortDateFormat(lote.getDtenvio())),
                            new ReportUtils.ParameterReport("usuario", lote.getUsuario()),
                            new ReportUtils.ParameterReport("qtdeCaixas", lote.getItensLote().stream().mapToInt(item -> (int) item.getListCaixas().stream().filter(cx -> !cx.isIncompleta() && !cx.isPerdida()).count()).sum()),
                            new ReportUtils.ParameterReport("qtdeItens", lote.getItensLote().stream().mapToInt(item -> (int) item.getListCaixas().stream().filter(cx -> !cx.isIncompleta() && !cx.isPerdida()).mapToInt(SdCaixaPA::getQtde).sum()).sum()),
                            new ReportUtils.ParameterReport("peso", lote.getItensLote().stream().mapToDouble(item -> item.getListCaixas().stream().filter(cx -> !cx.isIncompleta() && !cx.isPerdida()).mapToDouble(cx -> cx.getPeso().doubleValue()).sum()).sum())
                    )
                    .view()
                    .printWithDialog();
        } catch (JRException | SQLException | IOException e) {
            e.printStackTrace();
        }
        MessageBox.create(message -> {
            message.message("Romaneio impresso com sucesso!");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }

    private LocalDate janelaDataLote() {
        AtomicReference<LocalDate> data = new AtomicReference<>();
        new Fragment().show(fragment -> {
            fragment.title("Data de Saída do Lote");
            fragment.size(300.0, 170.0);
            FormFieldDate dataField = FormFieldDate.create(field -> {
                field.title.setText("Data");
                field.datePicker.setPrefWidth(600);
                field.value.setValue(LocalDate.now());
                field.addStyle("lg");
            });

            ((Button) fragment.boxFullButtons.getChildren().get(1)).getStyleClass().add("lg");
            fragment.box.getChildren().add(FormBox.create(principal -> {
                principal.vertical();
                principal.expanded();
                principal.add(dataField.build());
            }));
            fragment.buttonsBox.getChildren().add(FormButton.create(btn -> {
                fragment.box.setOnKeyReleased(evt -> {
                    if (evt.getCode() == KeyCode.ENTER) {
                        btn.fire();
                    }
                });
                btn.addStyle("success").addStyle("lg");
                btn.setText("Confirmar");
                btn.setOnAction(event -> {
                    if (dataField.value.getValue().isBefore(LocalDate.now())) {
                        MessageBox.create(message -> {
                            message.message("Data digitada inválida!\n A data do lote tem que ser maior que o dia atual.");
                            message.type(MessageBox.TypeMessageBox.WARNING);
                            message.showAndWait();
                        });
                        return;
                    }
                    if (dataField.value.getValue() == null) {
                        MessageBox.create(message -> {
                            message.message("Data digitada inválida!");
                            message.type(MessageBox.TypeMessageBox.WARNING);
                            message.showAndWait();
                        });
                        return;
                    }
                    data.set(dataField.value.get());
                    fragment.close();

                });
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
            }));
        });
        return data.get();
    }

}
