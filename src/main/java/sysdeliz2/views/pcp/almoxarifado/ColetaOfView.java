package sysdeliz2.views.pcp.almoxarifado;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import sysdeliz2.controllers.views.pcp.almoxarifado.ColetaOfController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.sysdeliz.SdOfsColetor;
import sysdeliz2.models.sysdeliz.SdReservaMateriais;
import sysdeliz2.models.ti.Of1;
import sysdeliz2.models.ti.PcpFtOf;
import sysdeliz2.models.view.VSdFaccoesOf;
import sysdeliz2.models.view.VSdMateriaisParaColeta;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.StaticVersion;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.gui.window.NumericKeyboard;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.mails.SimpleMail;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.List;

public class ColetaOfView extends ColetaOfController {
    
    // <editor-fold defaultstate="collapsed" desc="Declaração: Controle">
    private final String usuario;
    private VSdMateriaisParaColeta materialColeta = null;
    private SdOfsColetor ofColetaSelecionada = null;
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Fields Material">
    private final FormFieldText depositoField = FormFieldText.create(field -> {
        //field.withoutTitle();
        field.title("Depósito");
        field.editable(false);
        field.width(70.0);
        field.addStyle("lg");
    });
    private final FormFieldText localField = FormFieldText.create(field -> {
        field.title("LOCAL");
        field.editable(false);
        field.addStyle("lg");
        field.width(90.0);
        field.mouseClicked(event -> {
            if (event.getClickCount() >= 2) {
                if ((Boolean) QuestionBox.build(Boolean.class, message -> {
                    message.message("Deseja realmente solicitar alteração de local do material?");
                    message.showAndWait();
                }).value.get()) {
                    SimpleMail.INSTANCE.addDestinatario("giza@deliz.com.br").addCCO("diego@deliz.com.br")
                            .comAssunto("Alteração de local de material")
                            .comCorpo("Solicitação de " + colaborador.getNome() + " para troca de local do material " + materialColeta.getInsumo())
                            .send();
                }
            }
        });
    });
    private final FormFieldText materialField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("MAT:");
        field.editable(false);
        field.addStyle("sm");
    });
    private final FormFieldText codMaterialField = FormFieldText.create(field -> {
        //field.withoutTitle();
        field.title("Código");
        field.editable(false);
        field.width(160.0);
        field.addStyle("lg");
        field.mouseClicked(evt -> {
            if (evt.getClickCount() >= 2)
                new Fragment().show(fragment -> {
                    fragment.title("Imagem Material");
                    fragment.size(500.0, 500.0);
                    ImageView imageMaterial = null;
                    try {
                        imageMaterial = new ImageView(new Image(new FileInputStream(new File("K:\\TI_ERP\\Arquivos\\imagens\\material\\" + materialColeta.getInsumo().getCodigo() + ".jpg"))));
                    } catch (FileNotFoundException e) {
                        try {
                            imageMaterial = new ImageView(new Image(new FileInputStream(new File("C:\\SysDelizLocal\\local_files\\no-photo.png"))));
                        } catch (FileNotFoundException ex) {
                            ex.printStackTrace();
                            ExceptionBox.build(message -> {
                                message.exception(ex);
                                message.showAndWait();
                            });
                        }
                    }
                    imageMaterial.setPreserveRatio(true);
                    imageMaterial.setPickOnBounds(true);
                    if (imageMaterial.getImage().getHeight() > imageMaterial.getImage().getWidth())
                        imageMaterial.setFitHeight(500.0);
                    else
                        imageMaterial.setFitWidth(500.0);
                    fragment.box.getChildren().add(FormFieldText.create(fieldCod -> {
                        fieldCod.title("Código Material");
                        fieldCod.editable(false);
                        fieldCod.value.set(field.value.get());
                    }).build());
                    fragment.box.getChildren().add(imageMaterial);
                });
        });
    });
    private final FormFieldText corField = FormFieldText.create(field -> {
        //field.withoutTitle();
        field.title("Cor");
        field.expanded();
        field.editable(false);
        field.addStyle("lg");
    });
    private final FormFieldText qtdeField = FormFieldText.create(field -> {
        field.title("QTDE");
        field.editable(false);
        field.addStyle("lg");
        field.width(120.0);
    });
    private final FormFieldText unidadeMedidaField = FormFieldText.create(field -> {
        field.title("U.N.");
        field.editable(false);
        field.addStyle("sm");
        field.width(35.0);
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Fields OF Coleta">
    private final FormFieldText faccaoOfEmColetaField = FormFieldText.create(field -> {
        //field.title("Facção OF em Coleta");
        field.withoutTitle();
        field.label("FAC:");
        field.editable(false);
        field.width(300.0);
    });
    private final FormFieldText referenciaOfEmColetaField = FormFieldText.create(field -> {
        //field.title("Referência OF em Coleta");
        field.withoutTitle();
        field.label("REF:");
        field.editable(false);
        field.width(200.0);
    });
    private final FormFieldText qtdeOfEmColetaField = FormFieldText.create(field -> {
        //field.title("Qtde OF em Coleta");
        field.withoutTitle();
        field.label("QTDE:");
        field.editable(false);
        field.width(100.0);
    });
    private final FormFieldText ofEmColetaField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("OF:");
        field.editable(false);
        field.width(150.0);
        field.addStyle("lg").addStyle("success");
        field.mouseClicked(evt -> {
            if (evt.getClickCount() >= 2)
                if (ofColetaSelecionada != null) {
                    new Fragment().show(fragment -> {
                        fragment.title("Informações de Baixa");
                        fragment.size(400.0, 500.0);
                        fragment.box.getChildren().add(FormTableView.create(SdReservaMateriais.class, table -> {
                            table.title("Materiais em Outros Depósitos");
                            table.expanded();
                            table.items.set(FXCollections.observableList((List<SdReservaMateriais>) new FluentDao().selectFrom(SdReservaMateriais.class)
                                    .where(it -> it
                                            .equal("id.numero", ofColetaSelecionada.getNumero())
                                            .equal("id.setor", ofColetaSelecionada.getSetor())
                                            .isIn("id.deposito", new String[]{
                                                    ofColetaSelecionada.getDeposito().getCodigo().equals("0002") ? "X" : "0002",
                                                    ofColetaSelecionada.getDeposito().getCodigo().equals("0008") ? "X" : "0008",
                                                    ofColetaSelecionada.getDeposito().getCodigo().equals("0009") ? "X" : "0009"}))
                                    .resultList()));
                            table.columns(
                                    FormTableColumn.create(cln -> {
                                        cln.title("Depósito");
                                        cln.width(80.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<SdReservaMateriais, SdReservaMateriais>, ObservableValue<SdReservaMateriais>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDepagrupador()));
                                        cln.alignment(FormTableColumn.Alignment.CENTER);
                                    }).build(),
                                    FormTableColumn.create(cln -> {
                                        cln.title("Material");
                                        cln.width(120.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<SdReservaMateriais, SdReservaMateriais>, ObservableValue<SdReservaMateriais>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getInsumo().getCodigo()));
                                        cln.alignment(FormTableColumn.Alignment.CENTER);
                                    }).build(),
                                    FormTableColumn.create(cln -> {
                                        cln.title("Cor");
                                        cln.width(60.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<SdReservaMateriais, SdReservaMateriais>, ObservableValue<SdReservaMateriais>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCorI().getCor()));
                                        cln.alignment(FormTableColumn.Alignment.CENTER);
                                    }).build(),
                                    FormTableColumn.create(cln -> {
                                        cln.title("Qtde");
                                        cln.width(80.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<SdReservaMateriais, SdReservaMateriais>, ObservableValue<SdReservaMateriais>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtdeB()));
                                        cln.alignment(FormTableColumn.Alignment.RIGHT);
                                    }).build()
                            );
                        }).build());
                        fragment.box.getChildren().add(FormTableView.create(PcpFtOf.class, table -> {
                            table.title("Materiais não Baixados");
                            table.expanded();
                            table.items.set(FXCollections.observableList((List<PcpFtOf>) new FluentDao().selectFrom(PcpFtOf.class)
                                    .where(it -> it
                                            .equal("pcpFtOfID.numero", ofColetaSelecionada.getNumero())
                                            .equal("pcpFtOfID.setor", ofColetaSelecionada.getSetor())
                                            .notEqual("pcpFtOfID.corI", "*****")
                                            .equal("situacao", "A"))
                                    .resultList()));
                            table.columns(
                                    FormTableColumn.create(cln -> {
                                        cln.title("Material");
                                        cln.width(120.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<PcpFtOf, PcpFtOf>, ObservableValue<PcpFtOf>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPcpFtOfID().getInsumo()));
                                        cln.alignment(FormTableColumn.Alignment.CENTER);
                                    }).build(),
                                    FormTableColumn.create(cln -> {
                                        cln.title("Cor");
                                        cln.width(60.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<PcpFtOf, PcpFtOf>, ObservableValue<PcpFtOf>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPcpFtOfID().getCorI()));
                                        cln.alignment(FormTableColumn.Alignment.CENTER);
                                    }).build(),
                                    FormTableColumn.create(cln -> {
                                        cln.title("Consumo");
                                        cln.width(80.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<PcpFtOf, PcpFtOf>, ObservableValue<PcpFtOf>>) param -> new ReadOnlyObjectWrapper(param.getValue().getConsumo()));
                                        cln.alignment(FormTableColumn.Alignment.RIGHT);
                                    }).build()
                            );
                        }).build());
                    });
                }
        });
    });
    private final FormFieldText setorEmColetaField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("SET:");
        field.editable(false);
        field.width(100.0);
        field.addStyle("lg");
    });
    private final FormFieldText proximaOfColetaField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("OF:");
        field.editable(false);
        field.width(150.0);
        field.addStyle("primary");
    });
    private final FormFieldText proximoSetorColetaField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("SET:");
        field.editable(false);
        field.width(100.0);
    });
    private final FormFieldText ultimaOfColetaField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("OF:");
        field.editable(false);
        field.width(150.0);
        field.addStyle("dark");
        field.value.set("-");
    });
    private final FormFieldText ultimaRefColetaField = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("REF:");
        field.editable(false);
        field.width(100.0);
        field.value.set("-");
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Tables">
    FormTableView<VSdMateriaisParaColeta> tblMatsParaColeta = FormTableView.create(VSdMateriaisParaColeta.class, table -> {
        table.title("Materiais para Coleta");
        table.items.bind(materiaisParaColeta);
        table.expanded();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Depósito");
                    cln.width(60.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisParaColeta, VSdMateriaisParaColeta>, ObservableValue<VSdMateriaisParaColeta>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDeposito().getCodigo()));
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Local");
                    cln.width(100.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisParaColeta, VSdMateriaisParaColeta>, ObservableValue<VSdMateriaisParaColeta>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLocal()));
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Material");
                    cln.width(350.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisParaColeta, VSdMateriaisParaColeta>, ObservableValue<VSdMateriaisParaColeta>>) param -> new ReadOnlyObjectWrapper(param.getValue().getInsumo()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Cor");
                    cln.width(180.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisParaColeta, VSdMateriaisParaColeta>, ObservableValue<VSdMateriaisParaColeta>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCori()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Qtde");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisParaColeta, VSdMateriaisParaColeta>, ObservableValue<VSdMateriaisParaColeta>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtdeb()));
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                }).build()
        );
        table.factoryRow(param -> {
            return new TableRow<VSdMateriaisParaColeta>() {
                @Override
                protected void updateItem(VSdMateriaisParaColeta item, boolean empty) {
                    super.updateItem(item, empty);
                    clear();
                    getStyleClass().add("lg");
                    if (item != null && !empty) {
                        if (item.getSituacao().equals("P"))
                            getStyleClass().add("info");
                        else if (!item.getDeposito().getCodigo().equals("0000") && !item.getDeposito().getCodigo().equals(item.getDepagrupador()))
                            getStyleClass().add("danger");
                    }
                }
                
                private void clear() {
                    getStyleClass().removeAll("primary", "danger", "success", "warining", "info");
                }
            };
        });
        table.indices(
                table.indice(Color.LIGHTBLUE, "Baixa de Material Parcial", false),
                table.indice(Color.TOMATO, "Material em outro ddepósito", false)
        );
    });
    FormTableView<VSdMateriaisParaColeta> tblMatsColetados = FormTableView.create(VSdMateriaisParaColeta.class, table -> {
        table.title("Materiais Coletados");
        table.items.bind(materiaisColetados);
        table.height(300.0);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Depósito");
                    cln.width(60.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisParaColeta, VSdMateriaisParaColeta>, ObservableValue<VSdMateriaisParaColeta>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDeposito().getCodigo()));
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Local");
                    cln.width(100.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisParaColeta, VSdMateriaisParaColeta>, ObservableValue<VSdMateriaisParaColeta>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLocal()));
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Material");
                    cln.width(350.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisParaColeta, VSdMateriaisParaColeta>, ObservableValue<VSdMateriaisParaColeta>>) param -> new ReadOnlyObjectWrapper(param.getValue().getInsumo()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Cor");
                    cln.width(180.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisParaColeta, VSdMateriaisParaColeta>, ObservableValue<VSdMateriaisParaColeta>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCori()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Qtde");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisParaColeta, VSdMateriaisParaColeta>, ObservableValue<VSdMateriaisParaColeta>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtdeb()));
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                }).build()
        );
        table.factoryRow(param -> {
            return new TableRow<VSdMateriaisParaColeta>() {
                @Override
                protected void updateItem(VSdMateriaisParaColeta item, boolean empty) {
                    super.updateItem(item, empty);
                    clear();
                    getStyleClass().add("sm");
                    if (item != null && !empty) {
                        if (item.getSituacao().equals("P"))
                            getStyleClass().add("info");
                    }
                }
                
                private void clear() {
                    getStyleClass().removeAll("primary", "danger", "success", "warining", "info");
                }
            };
        });
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: GUI">
    private final FormFieldText versaoSistema = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("Versão:");
        field.editable(false);
        field.addStyle("xs");
        field.width(130.0);
        field.value.set(StaticVersion.versaoSistema);
    });
    private final FormFieldText qtdeUnCompra = FormFieldText.create(field -> {
        field.withoutTitle();
        field.label("UN Compra: ");
        field.editable(false);
    });
    private FormButton movQtdeFaccao = FormButton.create(btnAddQtde -> {
        btnAddQtde.icon(ImageUtils.getIcon(ImageUtils.Icon.ADD_QTDE_MATERIAL, 37.0));
        btnAddQtde.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
        btnAddQtde.height(37.0);
        btnAddQtde.width(40.0);
        btnAddQtde.addStyle("info");
        btnAddQtde.setAction(evt -> {
            new Fragment().show(fragment -> {
                fragment.title.setText("Movimentar Material para Faccção");
                final FormFieldText qtdeConsumoField = FormFieldText.create(field -> {
                    field.title("Qtde. Consumo");
                    field.editable(false);
                    field.addStyle("lg");
                    field.value.set(StringUtils.toDecimalFormat(materialColeta.getQtdeb().doubleValue(), 4));
                    field.mask(FormFieldText.Mask.DOUBLE);
                });
                final FormFieldSegmentedButton<String> tipoUnidadeMaterial = FormFieldSegmentedButton.create(field -> {
                    field.title("UN Material");
                    field.options(
                            field.option("Em [" + materialColeta.getInsumo().getUnidade().getUnidade() + "] "
                                    + materialColeta.getInsumo().getUnidade().getDescricao(), "estoque", FormFieldSegmentedButton.Style.PRIMARY),
                            field.option("Em [" + materialColeta.getInsumo().getUnicom().getUnidade() + "] "
                                    + materialColeta.getInsumo().getUnicom().getDescricao(), "compra", FormFieldSegmentedButton.Style.SECUNDARY)
                    );
                    field.select(0);
                });
                final FormFieldText qtdeExtraField = FormFieldText.create(field -> {
                    field.title("Qtde. Extra");
                    field.addStyle("lg");
                    field.width(300.0);
                });
                final FormFieldText unidadeCompraField = FormFieldText.create(field -> {
                    field.title("UN Comp.");
                    field.addStyle("sm");
                    field.width(70.0);
                    field.value.set(materialColeta.getInsumo().getUnicom().toString());
                });
                final FormFieldText unidadeEstoqueField = FormFieldText.create(field -> {
                    field.title("UN Estq.");
                    field.addStyle("sm");
                    field.width(70.0);
                    field.value.set(materialColeta.getInsumo().getUnidade().toString());
                });
                final Button btnKeyBoard = FormButton.create(btn -> {
                    btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.NUMPAD, ImageUtils.IconSize._48));
                    btn.height(64.0);
                    btn.addStyle("info");
                    btn.setAction(evtNumPad -> {
                        Double qtdeMovimento = NumericKeyboard.show(NumericKeyboard.KeyboardValue.DECIMAL, null).doubleKeyboard.getValue();
                        qtdeExtraField.value.set(StringUtils.toDecimalFormat(qtdeMovimento, 4));
                    });
                });
                fragment.box.getChildren().add(qtdeConsumoField.build());
                fragment.box.getChildren().add(FormBox.create(boxTipoUnMat -> {
                    boxTipoUnMat.horizontal();
                    boxTipoUnMat.add(tipoUnidadeMaterial.build());
                    //boxTipoUnMat.add(unidadeCompraField.build());
                    //boxTipoUnMat.add(unidadeEstoqueField.build());
                }));
                fragment.box.getChildren().add(FormBox.create(boxQtdeMov -> {
                    boxQtdeMov.horizontal();
                    boxQtdeMov.add(qtdeExtraField.build(), btnKeyBoard);
                }));
                fragment.size(350.0, 300.0);
                
                Button btnMovimentar = new Button("Enviar", ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._24));
                btnMovimentar.getStyleClass().addAll("success");
                btnMovimentar.setOnAction(evtMov -> {
                    try {
                        BigDecimal qtdeMovimento = new BigDecimal(qtdeExtraField.value.get().replace(".", "").replace(",", "."));
                        if (tipoUnidadeMaterial.value.get().equals("compra")) {
                            if (materialColeta.getInsumo().getDivisor().compareTo(BigDecimal.ZERO) == 0) {
                                MessageBox.create(message -> {
                                    message.message("O divisor do material está cadastrado com o valor 0(zero)");
                                    message.type(MessageBox.TypeMessageBox.IMPORTANT);
                                    message.showAndWait();
                                });
                                return;
                            }
                            qtdeMovimento = qtdeMovimento.multiply(materialColeta.getInsumo().getDivisor());
                            qtdeExtraField.value.set(StringUtils.toDecimalFormat(qtdeMovimento.doubleValue(), 4));
                        }
                        if (qtdeMovimento.compareTo(materialColeta.getQtdeb()) <= 0) {
                            MessageBox.create(message -> {
                                message.message("O valor de envio deve ser maior que o consumo.\n" +
                                        "Você deve informar a quantidade total que está sendo enviado, considerando nela o que será consumido na OF.");
                                message.type(MessageBox.TypeMessageBox.IMPORTANT);
                                message.showAndWait();
                            });
                            return;
                        }
                        if (qtdeMovimento.compareTo(BigDecimal.ZERO) > 0) {
                            adicionarQtdeMaterial(
                                    materialColeta,
                                    qtdeMovimento.subtract(materialColeta.getQtdeb()),
                                    ofColetaSelecionada);
                            
                            coletarMaterial(materialColeta);
                            if (materiaisParaColeta.size() == 0) {
                                finalizarColeta(ofColetaSelecionada);
                                MessageBox.create(message -> {
                                    message.message("Finalizado coleta da OF/SETOR");
                                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                                    message.position(Pos.CENTER);
                                    message.notification();
                                });
                                if ((ofColetaSelecionada.getDeposito().getCodigo().equals("0002") || ofColetaSelecionada.getDeposito().getCodigo().equals("0008")) && ofColetaSelecionada.getTipo().equals("N")) {
                                    try {
                                        imprimirRomaneio(ofColetaSelecionada);
                                    } catch (IOException | JRException | SQLException e) {
                                        e.printStackTrace();
                                        ExceptionBox.build(message -> {
                                            message.exception(e);
                                            message.showAndWait();
                                        });
                                    }
                                }
                                ultimaOfColetaField.value.set(ofColetaSelecionada.getNumero() + "/" + ofColetaSelecionada.getSetor());
                                ultimaRefColetaField.value.set(referenciaOfEmColetaField.value.get());
                                carregarOfColeta();
                            } else {
                                materialColeta = materiaisParaColeta.get(0);
                                carregaMaterial(materialColeta);
                                materiaisParaColeta.remove(0);
                                tblMatsParaColeta.refresh();
                                tblMatsColetados.refresh();
                                MessageBox.create(message -> {
                                    message.message("Material coletado.");
                                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                                    message.position(Pos.BOTTOM_RIGHT);
                                    message.notification();
                                });
                            }
                        }
                        fragment.modalStage.close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        ExceptionBox.build(message -> {
                            message.exception(ex);
                            message.sendAutomaticMail();
                            message.showAndWait();
                        });
                    }
                });
                fragment.buttonsBox.getChildren().addAll(btnMovimentar);
            });
        });
    });
    private FormButton apontaFaltaMaterial = FormButton.create(btnAddQtde -> {
        btnAddQtde.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, 37.0));
        btnAddQtde.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
        btnAddQtde.height(37.0);
        btnAddQtde.width(40.0);
        btnAddQtde.addStyle("danger");
        btnAddQtde.setAction(evt -> {
            if ((Boolean) QuestionBox.build(Boolean.class, message -> {
                message.message("APONTAR FALTA DO MATERIAL EM ESTOQUE?");
                message.showAndWait();
            }).value.get()) {
                try {
                    apontaFaltaMaterial(materialColeta);
                    SimpleMail.INSTANCE.addDestinatario("giza@deliz.com.br")
                            .addCCO("diego@deliz.com.br")
                            .comAssunto("Apontamento de falta de material - Coletor")
                            .comCorpo("O coletor: " + colaborador.getNome() + " apontou falta do material: " + materialColeta.getInsumo() + " cor: " + materialColeta.getCori() + " qtde: " + materialColeta.getQtdeb() +
                                    " no depósito: " + materialColeta.getDeposito() + " da OF: " + materialColeta.getNumero() + " no setor: " + materialColeta.getSetor())
                            .send();
                    
                    if (materiaisParaColeta.size() == 0) {
                        finalizarColeta(ofColetaSelecionada);
                        MessageBox.create(message -> {
                            message.message("Finalizado coleta da OF/SETOR");
                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                            message.position(Pos.CENTER);
                            message.notification();
                        });
                        if (ofColetaSelecionada.getDeposito().getCodigo().equals("0002")) {
                            try {
                                imprimirRomaneio(ofColetaSelecionada);
                            } catch (IOException | JRException | SQLException e) {
                                e.printStackTrace();
                                ExceptionBox.build(message -> {
                                    message.exception(e);
                                    message.showAndWait();
                                });
                            }
                        }
                        carregarOfColeta();
                    } else {
                        materialColeta = materiaisParaColeta.get(0);
                        carregaMaterial(materialColeta);
                        materiaisParaColeta.remove(0);
                        tblMatsParaColeta.refresh();
                        tblMatsColetados.refresh();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                    ExceptionBox.build(message -> {
                        message.exception(e);
                        message.showAndWait();
                    });
                }
            }
        });
    });
    private FormButton btnColetarMaterial = FormButton.create(btnColetar -> {
        btnColetar.icon(ImageUtils.getIcon(ImageUtils.Icon.BAIXAR, 88.0));
        btnColetar.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
        btnColetar.height(88.0);
        btnColetar.width(88.0);
        btnColetar.addStyle("success");
        btnColetar.setAction(evt -> {
            if (ofColetaSelecionada.getStatus().equals("A"))
                ofColetaSelecionada.setStatus("E");
            coletarMaterial(materialColeta);
            carregaMateriaisOfColeta(ofColetaSelecionada.getNumero(), ofColetaSelecionada.getSetor(), ofColetaSelecionada.getTipo());
            limparMaterial();
            if (materiaisParaColeta.size() == 0) {
                finalizarColeta(ofColetaSelecionada);
                MessageBox.create(message -> {
                    message.message("Finalizado coleta da OF/SETOR");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.CENTER);
                    message.notification();
                });
                if ((ofColetaSelecionada.getDeposito().getCodigo().equals("0002") || ofColetaSelecionada.getDeposito().getCodigo().equals("0008")) && ofColetaSelecionada.getTipo().equals("N")) {
                    try {
                        imprimirRomaneio(ofColetaSelecionada);
                    } catch (IOException | JRException | SQLException e) {
                        e.printStackTrace();
                        ExceptionBox.build(message -> {
                            message.exception(e);
                            message.showAndWait();
                        });
                    }
                }
                ultimaOfColetaField.value.set(ofColetaSelecionada.getNumero() + "/" + ofColetaSelecionada.getSetor());
                ultimaRefColetaField.value.set(referenciaOfEmColetaField.value.get());
                carregarOfColeta();
            } else {
                materialColeta = materiaisParaColeta.get(0);
                carregaMaterial(materialColeta);
                materiaisParaColeta.remove(0);
                tblMatsParaColeta.refresh();
                tblMatsColetados.refresh();
                MessageBox.create(message -> {
                    message.message("Material coletado.");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.BOTTOM_RIGHT);
                    message.notification();
                });
            }
        });
    });
    private VBox boxMaterialColeta = FormBox.create(boxMaterialColeta -> {
        boxMaterialColeta.horizontal();
        boxMaterialColeta.border();
        boxMaterialColeta.add(FormBox.create(boxQtdeLocal -> {
                    boxQtdeLocal.vertical();
                    boxQtdeLocal.add(FormBox.create(boxRow1 -> {
                        boxRow1.horizontal();
                        boxRow1.add(localField.build(),
                                qtdeField.build(),
                                unidadeMedidaField.build());
                    }));
                    boxQtdeLocal.add(qtdeUnCompra.build());
                }),
                FormBox.create(boxButtons -> {
                    boxButtons.vertical();
                    boxButtons.add(movQtdeFaccao, apontaFaltaMaterial);
                }),
                FormBox.create(dadosMaterial -> {
                    dadosMaterial.vertical();
                    dadosMaterial.expanded();
                    dadosMaterial.add(
                            materialField.build(),
                            FormBox.create(linha2 -> {
                                linha2.horizontal();
                                linha2.add(codMaterialField.build(), corField.build(), depositoField.build());
                            }));
                }),
                btnColetarMaterial);
    });
    private FormBox boxOfEmColeta = FormBox.create(ofAtual -> {
        ofAtual.horizontal();
        ofAtual.title("Coletar OF");
        ofAtual.add(FormBox.create(boxRef -> {
            boxRef.vertical();
            boxRef.add(FormBox.create(boxOf -> {
                boxOf.horizontal();
                boxOf.add(ofEmColetaField.build(),
                        setorEmColetaField.build());
            }));
            boxRef.add(FormBox.create(boxOf -> {
                boxOf.horizontal();
                boxOf.add(new Label("OF Repos", ImageUtils.getIcon(ImageUtils.Icon.ALERT_WARNING, ImageUtils.IconSize._16)),
                        new Label("OF Out. Dep", ImageUtils.getIcon(ImageUtils.Icon.ALERT_ERROR, ImageUtils.IconSize._16)),
                        new Label("Mat. Aberto", ImageUtils.getIcon(ImageUtils.Icon.ALERT_INFO, ImageUtils.IconSize._16)));
            }));
        }));
    });
    // </editor-fold>
    
    public ColetaOfView() {
        super("Coletar Materiais", ImageUtils.getImage(ImageUtils.Icon.BAIXAR_MATERIAL), Globals.isPortatil());
        this.usuario = Globals.getNomeUsuario();
        JPAUtils.getEntityManager();
        
        colaboradorLogado(this.usuario);
        if (colaborador == null) {
            MessageBox.create(message -> {
                message.message("Usuário não habilitado para coleta de materiais.");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showAndWait();
            });
            Stage main = (Stage) this.getScene().getWindow();
            main.close();
        } else
            init();
    }
    
    private void init() {
        carregarOfColeta();
        super.box.getChildren().add(FormBox.create(header -> {
            header.vertical();
            header.add(FormBox.create(boxColaborador -> {
                boxColaborador.horizontal();
                boxColaborador.add(FormFieldText.create(field -> {
                            field.withoutTitle();
                            field.label("Coletor:");
                            field.editable(false);
                            field.width(380.0);
                            field.value.set(colaborador.toString());
                        }).build(),
                        FormFieldText.create(field -> {
                            field.withoutTitle();
                            field.label("Depósito: ");
                            field.editable(false);
                            field.width(250.0);
                            field.value.set(colaborador.getDeposito().toString());
                        }).build());
            }));
            header.add(FormBox.create(ofColeta -> {
                ofColeta.horizontal();
                ofColeta.add(boxOfEmColeta);
                ofColeta.add(FormBox.create(ofsAnteriorProxima -> {
                    ofsAnteriorProxima.horizontal();
                    ofsAnteriorProxima.add(FormBox.create(ultimaOf -> {
                        ultimaOf.vertical();
                        ultimaOf.width(150.0);
                        ultimaOf.title("Última OF");
                        ultimaOf.add(ultimaOfColetaField.build(),
                                ultimaRefColetaField.build());
                    }));
                    ofsAnteriorProxima.add(FormBox.create(proxOf -> {
                        proxOf.vertical();
                        proxOf.width(120.0);
                        proxOf.title("Próxima OF");
                        proxOf.add(proximaOfColetaField.build(),
                                proximoSetorColetaField.build());
                    }));
                }));
                ofColeta.add(FormButton.create(btn -> {
                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ATUALIZAR, ImageUtils.IconSize._64));
                    btn.height(64.0);
                    btn.width(74.0);
                    btn.addStyle("info");
                    btn.setAction(evt -> {
                        carregarOfColeta();
                    });
                }));
            }));
            header.add(FormBox.create(boxDadosOf -> {
                boxDadosOf.horizontal();
                boxDadosOf.add(faccaoOfEmColetaField.build(), referenciaOfEmColetaField.build(), qtdeOfEmColetaField.build());
            }));
        }));
        super.box.getChildren().add(new Separator(Orientation.HORIZONTAL));
        super.box.getChildren().add(boxMaterialColeta);
        super.box.getChildren().add(tblMatsParaColeta.build());
        super.box.getChildren().add(new Separator(Orientation.HORIZONTAL));
        super.box.getChildren().add(tblMatsColetados.build());
        super.box.getChildren().add(FormBox.create(boxFooter -> {
            boxFooter.horizontal();
            boxFooter.add(versaoSistema.build());
        }));
    }
    
    private void limparOfs() {
        ofsColetor.clear();
        materiaisParaColeta.clear();
        materiaisColetados.clear();
        ofEmColetaField.value.set("-");
        setorEmColetaField.value.set("-");
        faccaoOfEmColetaField.value.set("-");
        referenciaOfEmColetaField.value.set("-");
        qtdeOfEmColetaField.value.set("-");
        proximaOfColetaField.value.set("-");
        proximoSetorColetaField.value.set("-");
        if (boxOfEmColeta.itens().size() > 1)
            boxOfEmColeta.itens().remove(1);
    }
    
    private void limparMaterial() {
        localField.value.set("-");
        depositoField.value.set("-");
        materialField.value.set("-");
        codMaterialField.value.set("-");
        qtdeUnCompra.clear();
        qtdeUnCompra.postLabel("-");
        materialField.removeStyle("info");
        corField.value.set("-");
        qtdeField.value.set("-");
        btnColetarMaterial.disable.set(true);
        movQtdeFaccao.disable.set(true);
        apontaFaltaMaterial.disable.set(true);
        boxMaterialColeta.getStyleClass().remove("danger");
    }
    
    private void carregarOfColeta() {
        carregaOfsColetor();
        //limpeza de tela
        btnColetarMaterial.disable.set(true);
        movQtdeFaccao.disable.set(true);
        apontaFaltaMaterial.disable.set(true);
        if (boxOfEmColeta.itens().size() > 1)
            boxOfEmColeta.itens().remove(1);
        
        if (ofsColetor.size() > 0) {
            ofColetaSelecionada = ofsColetor.get(0);
            JPAUtils.getEntityManager().refresh(ofColetaSelecionada);
            ofEmColetaField.value.set(ofColetaSelecionada.getNumero());
            setorEmColetaField.value.set(ofColetaSelecionada.getSetor());
            faccaoOfEmColetaField.value.set(ofColetaSelecionada.getFaccao().toString());
            Of1 of1Coleta = new FluentDao().selectFrom(Of1.class)
                    .where(it -> it
                            .equal("numero", ofColetaSelecionada.getNumero()))
                    .singleResult();
            referenciaOfEmColetaField.value.set(of1Coleta.getProduto().getCodigo() + " - " + of1Coleta.getProduto().getDescricao());
            qtdeOfEmColetaField.value.set(of1Coleta.getAtivos().toString());
            
            ofEmColetaField.removeStyle("mostruario").removeStyle("success");
            ofEmColetaField.addStyle("success");
            if (of1Coleta.getPeriodo().equals("M")) {
                ofEmColetaField.removeStyle("success");
                ofEmColetaField.addStyle("mostruario");
            }
            
            List<SdReservaMateriais> coletasOutrosDepositos = (List<SdReservaMateriais>) new FluentDao().selectFrom(SdReservaMateriais.class)
                    .where(it -> it
                            .equal("id.numero", ofColetaSelecionada.getNumero())
                            .equal("id.setor", ofColetaSelecionada.getSetor())
                            .isIn("id.deposito", new String[]{
                                    ofColetaSelecionada.getDeposito().getCodigo().equals("0002") ? "X" : "0002",
                                    ofColetaSelecionada.getDeposito().getCodigo().equals("0008") ? "X" : "0008",
                                    ofColetaSelecionada.getDeposito().getCodigo().equals("0009") ? "X" : "0009"}))
                    .resultList();
            List<PcpFtOf> materiaisSemBaixa = (List<PcpFtOf>) new FluentDao().selectFrom(PcpFtOf.class)
                    .where(it -> it
                            .equal("pcpFtOfID.numero", ofColetaSelecionada.getNumero())
                            .equal("pcpFtOfID.setor", ofColetaSelecionada.getSetor())
                            .notEqual("pcpFtOfID.corI", "*****")
                            .equal("situacao", "A"))
                    .resultList();
            
            if (ofColetaSelecionada.getTipo().equals("R")) {
                boxOfEmColeta.add(ImageUtils.getIcon(ImageUtils.Icon.ALERT_WARNING, ImageUtils.IconSize._48));
            } else if (materiaisSemBaixa.stream().count() > 0) {
                boxOfEmColeta.add(ImageUtils.getIcon(ImageUtils.Icon.ALERT_INFO, ImageUtils.IconSize._48));
            } else if (coletasOutrosDepositos.stream().count() > 0) {
                boxOfEmColeta.add(ImageUtils.getIcon(ImageUtils.Icon.ALERT_ERROR, ImageUtils.IconSize._48));
            }
            if (ofsColetor.size() > 1) {
                proximaOfColetaField.value.set(ofsColetor.get(1).getNumero());
                proximoSetorColetaField.value.set(ofsColetor.get(1).getSetor());
            } else {
                proximaOfColetaField.value.set("-");
                proximoSetorColetaField.value.set("-");
            }
            carregaMateriaisOfColeta(ofColetaSelecionada.getNumero(), ofColetaSelecionada.getSetor(), ofColetaSelecionada.getTipo());
            if (materiaisParaColeta.size() > 0) {
                btnColetarMaterial.disable.set(false);
                movQtdeFaccao.disable.set(false);
                apontaFaltaMaterial.disable.set(false);
                materialColeta = materiaisParaColeta.get(0);
                carregaMaterial(materialColeta);
                materiaisParaColeta.remove(0);
            }
        } else {
            limparMaterial();
            limparOfs();
        }
    }
    
    private void carregaMaterial(VSdMateriaisParaColeta material) {
        VSdFaccoesOf faccaoOf = getFaccaoOfSetor(ofColetaSelecionada.getNumero(), ofColetaSelecionada.getSetor());
        if (faccaoOf != null && !faccaoOf.getFaccao().equals(ofColetaSelecionada.getFaccao())) {
            ofColetaSelecionada.setFaccao(faccaoOf.getFaccao());
            new FluentDao().merge(ofColetaSelecionada);
        }
        
        btnColetarMaterial.disable.set(false);
        movQtdeFaccao.disable.set(false);
        apontaFaltaMaterial.disable.set(false);
        boxMaterialColeta.getStyleClass().remove("danger");
        localField.value.set(material.getLocal());
        depositoField.value.set(material.getDeposito().getCodigo());
        unidadeMedidaField.value.set(material.getInsumo().getUnidade().toString());
        codMaterialField.value.set(material.getInsumo().getCodigo());
        materialField.value.set(material.getInsumo().getDescricao());
        BigDecimal qtdeCompra = material.getQtdeb().divide(material.getInsumo().getDivisor(), 10, RoundingMode.HALF_UP);
        qtdeUnCompra.value.set(StringUtils.toDecimalFormat(qtdeCompra.doubleValue(), 4));
        qtdeUnCompra.postLabel(material.getInsumo().getUnicom().getDescricao());
        materialField.removeStyle("info");
        if (material.getSituacao().equals("P"))
            materialField.addStyle("info");
        corField.value.set(material.getCori().toString());
        qtdeField.value.set(StringUtils.toDecimalFormat(material.getQtdeb().doubleValue(), 5));
//        if (!material.getDeposito().getCodigo().equals("0000") && !material.getDeposito().getCodigo().equals(material.getDepagrupador())) {
//            movQtdeFaccao.disable.set(true);
//            apontaFaltaMaterial.disable.set(true);
//            boxMaterialColeta.getStyleClass().add("danger");
//        } else
        if (material.getDeposito().getCodigo().equals("0000")) {
            movQtdeFaccao.disable.set(true);
            apontaFaltaMaterial.disable.set(true);
        }
    }
    
}
