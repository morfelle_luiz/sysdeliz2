package sysdeliz2.views.pcp.almoxarifado;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import sysdeliz2.controllers.views.pcp.almoxarifado.GerenciadorColetasAlmoxarifadoController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.sysdeliz.SdColaborador;
import sysdeliz2.models.sysdeliz.SdOfsColetor;
import sysdeliz2.models.sysdeliz.SdReservaMateriais;
import sysdeliz2.models.ti.*;
import sysdeliz2.models.view.VSdFaccoesOf;
import sysdeliz2.models.view.VSdOfsColetaLiberada;
import sysdeliz2.models.view.VSdOfsParaColeta;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.ReportUtils;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GerenciadorColetasAlmoxarifadoView extends GerenciadorColetasAlmoxarifadoController {

    // <editor-fold defaultstate="collapsed" desc="Declaração: View">
    private final VBox listagemTab = (VBox) super.tabs.getTabs().get(0).getContent();
    private final VBox kanbanTab = (VBox) super.tabs.getTabs().get(1).getContent();
    private final VBox impressaoTab = (VBox) super.tabs.getTabs().get(2).getContent();
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Fields Filter">
    //Filters listagem
    private final FormFieldText ofField = FormFieldText.create(field -> {
        field.title("O.F.");
    });
    private final FormFieldMultipleFind<CadFluxo> setorField = FormFieldMultipleFind.create(CadFluxo.class, field -> {
        field.title("Setor");
    });
    private final FormFieldSegmentedButton<String> paisListField = FormFieldSegmentedButton.create(field -> {
        field.title("País");
        field.options(
                field.option("Ambos", "A", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("Brasil", "BR", FormFieldSegmentedButton.Style.SUCCESS),
                field.option("Paraguai", "PY", FormFieldSegmentedButton.Style.DANGER)
        );
        field.select(0);
    });
    //Filters impressao
    private final FormFieldText ofImpressaoField = FormFieldText.create(field -> {
        field.title("O.F.");
    });
    private final FormFieldMultipleFind<CadFluxo> setorImpressaoField = FormFieldMultipleFind.create(CadFluxo.class, field -> {
        field.title("Setor");
    });
    private final FormFieldMultipleFind<Deposito> depositoImpressaoField = FormFieldMultipleFind.create(Deposito.class, field -> {
        field.title("Depósito");
    });
    private final FormFieldMultipleFind<Entidade> faccaoImpressaoField = FormFieldMultipleFind.create(Entidade.class, field -> {
        field.title("Facção");
    });
    private final FormFieldMultipleFind<SdColaborador> coletorImpressaoField = FormFieldMultipleFind.create(SdColaborador.class, field -> {
        field.title("Coletor");
    });
    private final FormFieldSegmentedButton<String> statusColetaField = FormFieldSegmentedButton.create(statusColetaField -> {
        statusColetaField.title("Status Coleta");
        statusColetaField.options(
                statusColetaField.option("Todos", "T", FormFieldSegmentedButton.Style.SECUNDARY),
                statusColetaField.option("Aberto", "A", FormFieldSegmentedButton.Style.DANGER),
                statusColetaField.option("Em Coleta", "E", FormFieldSegmentedButton.Style.WARNING),
                statusColetaField.option("Coletado", "C", FormFieldSegmentedButton.Style.SUCCESS),
                statusColetaField.option("Parcial", "P", FormFieldSegmentedButton.Style.PRIMARY)
        );
        statusColetaField.select(0);
    });
    private final FormFieldSegmentedButton<String> impressoColetaField = FormFieldSegmentedButton.create(statusColetaField -> {
        statusColetaField.title("Status Impressão");
        statusColetaField.options(
                statusColetaField.option("Todos", "T", FormFieldSegmentedButton.Style.SECUNDARY),
                statusColetaField.option("Impresso", "true", FormFieldSegmentedButton.Style.SUCCESS),
                statusColetaField.option("Não Impresso", "false", FormFieldSegmentedButton.Style.DANGER)
        );
        statusColetaField.select(0);
    });
    private final FormFieldSegmentedButton<String> paisImpressaoField = FormFieldSegmentedButton.create(field -> {
        field.title("País");
        field.options(
                field.option("Ambos", "A", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("Brasil", "BR", FormFieldSegmentedButton.Style.SUCCESS),
                field.option("Paraguai", "PY", FormFieldSegmentedButton.Style.DANGER)
        );
        field.select(0);
    });
    //Filters kanban
    private final FormFieldSingleFind<CadFluxo> setorColetaField = FormFieldSingleFind.create(CadFluxo.class, field -> {
        field.title("Setor");
        field.width(100.0);
    });
    private final FormFieldMultipleFind<Deposito> depositoColetaField = FormFieldMultipleFind.create(Deposito.class, field -> {
        field.title("Depósito");
        field.width(100.0);
    });
    private final FormFieldSegmentedButton<String> paisKanbanField = FormFieldSegmentedButton.create(field -> {
        field.withoutTitle();
        field.options(
                field.option("Ambos", "A", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("Brasil", "BR", FormFieldSegmentedButton.Style.SUCCESS),
                field.option("Paraguai", "PY", FormFieldSegmentedButton.Style.DANGER)
        );
        field.select(0);
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Boxes">
    final FormBox boxColetores = FormBox.create();
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Objetos">
    private List<VSdOfsParaColeta> ofsSelecionadasParaColeta = new ArrayList<>();
    private List<SdOfsColetor> ofsColetorSelecionadas = new ArrayList<>();
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Tables">
    private final FormTableView<VSdOfsColetaLiberada> tblOfsColetaLiberada = FormTableView.create(VSdOfsColetaLiberada.class, table -> {
        table.items.bind(ofsColetaLiberadas);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("O.F.");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdOfsColetaLiberada, VSdOfsColetaLiberada>, ObservableValue<VSdOfsColetaLiberada>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumero()));
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Período");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdOfsColetaLiberada, VSdOfsColetaLiberada>, ObservableValue<VSdOfsColetaLiberada>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPeriodo()));
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Produto");
                    cln.width(350);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdOfsColetaLiberada, VSdOfsColetaLiberada>, ObservableValue<VSdOfsColetaLiberada>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Setor Coleta");
                    cln.width(180);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdOfsColetaLiberada, VSdOfsColetaLiberada>, ObservableValue<VSdOfsColetaLiberada>>) param -> new ReadOnlyObjectWrapper(param.getValue().getSetor()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Depóstio");
                    cln.width(160);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdOfsColetaLiberada, VSdOfsColetaLiberada>, ObservableValue<VSdOfsColetaLiberada>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDeposito()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("País");
                    cln.width(35);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdOfsColetaLiberada, VSdOfsColetaLiberada>, ObservableValue<VSdOfsColetaLiberada>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDeposito().getPais()));
                    cln.alignment(Pos.CENTER);
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Seq.");
                    cln.width(50);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdOfsColetaLiberada, VSdOfsColetaLiberada>, ObservableValue<VSdOfsColetaLiberada>>) param -> new ReadOnlyObjectWrapper(param.getValue().getSequencia()));
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Itens Coleta");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdOfsColetaLiberada, VSdOfsColetaLiberada>, ObservableValue<VSdOfsColetaLiberada>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.format(param -> {
                        return new TableCell<VSdOfsColetaLiberada, VSdOfsColetaLiberada>() {
                            @Override
                            protected void updateItem(VSdOfsColetaLiberada item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(String.valueOf(item.getTotaberto() - item.getTotcoletado()));
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("% Coleta");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdOfsColetaLiberada, VSdOfsColetaLiberada>, ObservableValue<VSdOfsColetaLiberada>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPerccoleta()));
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.format(param -> {
                        return new TableCell<VSdOfsColetaLiberada, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                clear();
                                if (item != null && !empty) {
                                    setText(StringUtils.toPercentualFormat(item.doubleValue(), 2));
                                    getStyleClass().add(item.doubleValue() >= 1 ? "success" : item.doubleValue() < .7 ? "danger" : "warning");
                                }
                            }

                            private void clear() {
                                getStyleClass().remove("success");
                                getStyleClass().remove("warning");
                                getStyleClass().remove("danger");
                            }
                        };
                    });
                }).build()
        );
        table.expanded();
    });
    private final FormTableView<VSdOfsParaColeta> tblOfsParaColeta = FormTableView.create(VSdOfsParaColeta.class, table -> {
        table.items.bind(ofsParaColeta);
        table.expanded();
        table.multipleSelection();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("O.F.");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdOfsParaColeta, VSdOfsParaColeta>, ObservableValue<VSdOfsParaColeta>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumero()));
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Setor");
                    cln.width(50);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdOfsParaColeta, VSdOfsParaColeta>, ObservableValue<VSdOfsParaColeta>>) param -> new ReadOnlyObjectWrapper(param.getValue().getSetor()));
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Depóstio");
                    cln.width(60);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdOfsParaColeta, VSdOfsParaColeta>, ObservableValue<VSdOfsParaColeta>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDeposito()));
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Tipo");
                    cln.width(40);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdOfsParaColeta, VSdOfsParaColeta>, ObservableValue<VSdOfsParaColeta>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTipo()));
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.format(param -> {
                        return new TableCell<VSdOfsParaColeta, String>() {
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(item.equals("N") ? "Nor" : "Rep");
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Itens");
                    cln.width(50);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdOfsParaColeta, VSdOfsParaColeta>, ObservableValue<VSdOfsParaColeta>>) param -> new ReadOnlyObjectWrapper(param.getValue().getItens()));
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                }).build());
        table.contextMenu(FormContextMenu.create(menu -> {
            menu.addItem(item -> {
                item.setText("Imprimir Requisição");
                item.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.IMPRIMIR, ImageUtils.IconSize._16));
                item.setOnAction(evt -> {
                    if (table.selectedRegisters().size() > 1) {
                        MessageBox.create(message -> {
                            message.message("Para impressão, você deve selecionar soment uma OF.");
                            message.type(MessageBox.TypeMessageBox.IMPORTANT);
                            message.showAndWait();
                        });
                        return;
                    }
                    if (!((Boolean) QuestionBox.build(Boolean.class, message -> {
                        message.message("Deseja realmente imprimir a OF para coleta com romaneio? Essa operação é irreversível.");
                        message.showAndWait();
                    }).value.get())) {
                        return;
                    }
                    imprimirRequisicao((VSdOfsParaColeta) table.selectedItem());
                });
            });
            menu.addItem(item -> {
                item.setText("Agrupar Coleta");
                item.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.AGRUPAR, ImageUtils.IconSize._16));
                item.setOnAction(evt -> {
                    agruparColeta((ObservableList<VSdOfsParaColeta>) table.selectedRegisters());
                });
            });
        }));
        table.factoryRow(constructRow -> {
            TableRow<VSdOfsParaColeta> row = new TableRow<VSdOfsParaColeta>() {

                @Override
                protected void updateItem(VSdOfsParaColeta item, boolean empty) {
                    super.updateItem(item, empty);
                    clear();

                    if (item != null && !empty)
                        if (item.getTipo() != null && item.getTipo().equals("R"))
                            getStyleClass().add("table-row-warning");
                        else if (item.getPeriodo() != null && item.getPeriodo().equals("M"))
                            getStyleClass().add("table-row-mostruario");
                }

                private void clear() {
                    getStyleClass().removeAll("table-row-warning", "table-row-mostruario");
                }
            };
            row = table.setDragAndDrop(row,
                    rowDrag -> {
                        ofsSelecionadasParaColeta.clear();
                        ofsColetorSelecionadas.clear();
                        ofsSelecionadasParaColeta.addAll(table.selectedRegisters());
                    },
                    rowDrop -> {
                    });
            return row;
        });
        table.indices(
                table.indice(Color.GOLD, "Reposição Material", false)
        );
    });
    private final FormTableView<SdOfsColetor> tblOfsProgramadas = FormTableView.create(VSdOfsParaColeta.class, table -> {
        table.items.bind(ofsProgramadas);
        table.expanded();
        table.multipleSelection();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Coleta");
                    cln.width(50);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdOfsColetor, SdOfsColetor>, ObservableValue<SdOfsColetor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getProgramacao()));
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Status");
                    cln.width(70);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdOfsColetor, SdOfsColetor>, ObservableValue<SdOfsColetor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getStatus()));
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.format(param -> {
                        return new TableCell<SdOfsColetor, String>() {
                            @Override
                            protected void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(item.equals("A") ? "Aberto" : item.equals("E") ? "Em coleta" : item.equals("P") ? "Parcial" : "Coletado");
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("O.F.");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdOfsColetor, SdOfsColetor>, ObservableValue<SdOfsColetor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumero()));
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Setor");
                    cln.width(60);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdOfsColetor, SdOfsColetor>, ObservableValue<SdOfsColetor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getSetor()));
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Depóstio");
                    cln.width(60);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdOfsColetor, SdOfsColetor>, ObservableValue<SdOfsColetor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDeposito().getCodigo()));
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("País");
                    cln.width(35);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdOfsColetor, SdOfsColetor>, ObservableValue<SdOfsColetor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDeposito().getPais()));
                    cln.alignment(Pos.CENTER);
                }).build() /*País*/,
                FormTableColumn.create(cln -> {
                    cln.title("Dt. Coleta");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdOfsColetor, SdOfsColetor>, ObservableValue<SdOfsColetor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDtcoleta()));
                    cln.format(param -> {
                        return new TableCell<SdOfsColetor, LocalDateTime>() {
                            @Override
                            protected void updateItem(LocalDateTime item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDateTimeFormat(item));
                                }
                            }
                        };
                    });
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Impresso");
                    cln.width(60);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdOfsColetor, SdOfsColetor>, ObservableValue<SdOfsColetor>>) param -> new ReadOnlyObjectWrapper(param.getValue().isImpresso()));
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.format(param -> {
                        return new TableCell<SdOfsColetor, Boolean>() {
                            @Override
                            protected void updateItem(Boolean item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(item ? ImageUtils.getIcon(ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._16) : ImageUtils.getIcon(ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Coletor");
                    cln.width(200);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdOfsColetor, SdOfsColetor>, ObservableValue<SdOfsColetor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getColetor()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Facção");
                    cln.width(350);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdOfsColetor, SdOfsColetor>, ObservableValue<SdOfsColetor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getFaccao()));
                }).build());

        table.contextMenu(FormContextMenu.create(menu -> {
            menu.addItem(item -> {
                item.setText("Imprimir Requisição");
                item.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.IMPRIMIR, ImageUtils.IconSize._16));
                item.setOnAction(evt -> {
                    table.selectedRegisters().forEach(it -> {
                        SdOfsColetor programacao = (SdOfsColetor) it;
                        String deposito = "";
                        if (!((Boolean) QuestionBox.build(Boolean.class, message -> {
                            message.message("Imprimir todos os depósitos da OF/Setor " + programacao.getNumero() + "/" + programacao.getSetor() + "?");
                            message.showAndWait();
                        }).value.get())) {
                            deposito = programacao.getDeposito().getCodigo();
                        }
                        if (!programacao.isImpresso())
                            if (!((Boolean) QuestionBox.build(Boolean.class, message -> {
                                message.message("Marcar a OF/Setor " + programacao.getNumero() + "/" + programacao.getSetor() + " como impresso?");
                                message.showAndWait();
                            }).value.get())) {
                                programacao.setImpresso(true);
                                new FluentDao().merge(programacao);
                            }

                        try {
                            new ReportUtils().config().addReport(ReportUtils.ReportFile.ROMANEIO_COLETA_DEPOSITO,
                                    new ReportUtils.ParameterReport[]{
                                            new ReportUtils.ParameterReport("pNumero", programacao.getNumero()),
                                            new ReportUtils.ParameterReport("pSetor", programacao.getSetor()),
                                            new ReportUtils.ParameterReport("pDeposito", deposito),
                                            new ReportUtils.ParameterReport("pTipo", programacao.getTipo()),
                                            new ReportUtils.ParameterReport("codEmpresa", Globals.getEmpresaLogada().getCodigo())
                                    }).view().toView();
                        } catch (JRException | SQLException e) {
                            e.printStackTrace();
                            ExceptionBox.build(message -> {
                                message.exception(e);
                                message.showAndWait();
                            });
                        }
                    });

                });
            });
        }));
        table.indices(
                table.indice(Color.TOMATO, "Para Coletar", false),
                table.indice(Color.LIGHTYELLOW, "Em Coleta", false),
                table.indice(Color.LIGHTGREEN, "Coletado", false),
                table.indice(Color.LIGHTBLUE, "Coleta Parcial", false));
        table.factoryRow(param -> {
            return new TableRow<SdOfsColetor>() {
                @Override
                protected void updateItem(SdOfsColetor item, boolean empty) {
                    super.updateItem(item, empty);
                    clean();
                    if (item != null && !empty)
                        switch (item.getStatus()) {
                            case "C":
                                getStyleClass().add("success");
                                break;
                            case "P":
                                getStyleClass().add("info");
                                break;
                            case "A":
                                getStyleClass().add("danger");
                                break;
                            case "E":
                                getStyleClass().add("warning");
                                break;
                        }
                }

                private void clean() {
                    getStyleClass().removeAll("danger", "success", "info", "warning");
                }
            };
        });
    });
    // </editor-fold>

    public GerenciadorColetasAlmoxarifadoView() {
        super("Gerenciar Coletas Materiais", ImageUtils.getImage(ImageUtils.Icon.COLETA), new String[]{"Listagem", "Kanban", "Impressão"});
        initListagem();
        initKanban();
        initImpressao();
        getOfsColetaLiberadas("", "", "A");
    }

    private void initListagem() {
        listagemTab.getChildren().add(FormBox.create(boxFilter -> {
            boxFilter.horizontal();
            boxFilter.add(FormTitledPane.create(filter -> {
                filter.filter();
                filter.add(FormBox.create(boxFiels -> {
                    boxFiels.vertical();
                    boxFiels.add(ofField.build());
                    boxFiels.add(fields -> fields.addHorizontal(setorField.build(), paisListField.build()));
                }));
                filter.collapse(false);
                filter.find.setOnAction(evt -> {
                    String numero = ofField.value.get() == null ? "" : ofField.value.get();
                    String setor = setorField.textValue.get() == null ? "" : setorField.textValue.get();
                    String pais = paisListField.value.getValue() == null ? "A" : paisListField.value.getValue();
                    getOfsColetaLiberadas(numero, setor, pais);
                });
                filter.clean.setOnAction(evt -> {
                    ofField.clear();
                    setorField.clear();
                    paisListField.select(0);
                });
            }));
        }));
        listagemTab.getChildren().add(tblOfsColetaLiberada.build());
    }

    private void initKanban() {
        boxColetores.horizontal();
        boxColetores.expanded();
        kanbanTab.getChildren().add(FormBox.create(tabKanban -> {
            tabKanban.horizontal();
            tabKanban.expanded();
            tabKanban.add(FormBox.create(boxOfs -> {
                boxOfs.vertical();
                boxOfs.add(FormButton.create(btn -> {
                    btn.title("Carregar Kanban");
                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.COLABORADOR, ImageUtils.IconSize._32));
                    btn.addStyle("lg").addStyle("primary");
                    btn.setAction(evt -> {
                        getOfsParaColeta(null, null, paisKanbanField.value.getValue());
                        addColetores(null, paisKanbanField.value.getValue());
                    });
                }));
                boxOfs.add(paisKanbanField.build());
                boxOfs.add(FormTitledPane.create(filter -> {
                    filter.filter();
                    filter.collapse(false);
                    filter.width(300.0);
                    filter.add(FormBox.create(boxFieldsFilter -> {
                        boxFieldsFilter.vertical();
                        boxFieldsFilter.add(setorColetaField.build(), depositoColetaField.build());
                    }));
                    filter.find.setOnAction(evt -> {
                        getOfsParaColeta(setorColetaField.value.get() == null ? null : setorColetaField.value.get().getCodigo(),
                                depositoColetaField.objectValues.stream().map(deposito -> deposito.getCodigo()).toArray(),
                                paisKanbanField.value.getValue());
                        addColetores(depositoColetaField.objectValues.stream().map(deposito -> deposito.getCodigo()).toArray(), paisKanbanField.value.getValue());
                    });
                    filter.clean.setOnAction(evt -> {
                        setorColetaField.clear();
                        depositoColetaField.clear();
                    });
                }));
                boxOfs.add(tblOfsParaColeta.build());
            }));
            tabKanban.add(boxColetores.build());
        }));
    }

    private void initImpressao() {
        impressaoTab.getChildren().add(FormBox.create(boxFilter -> {
            boxFilter.horizontal();
            boxFilter.add(FormTitledPane.create(filter -> {
                filter.filter();
                filter.add(FormBox.create(boxFields -> {
                    boxFields.horizontal();
                    boxFields.add(FormBox.create(boxCol1 -> {
                        boxCol1.vertical();
                        boxCol1.add(statusColetaField.build());
                        boxCol1.add(FormBox.create(boxCol1Row2 -> {
                            boxCol1Row2.horizontal();
                            boxCol1Row2.add(depositoImpressaoField.build(), setorImpressaoField.build());
                        }));
                    }));
                    boxFields.add(FormBox.create(boxCol1 -> {
                        boxCol1.vertical();
                        boxCol1.add(faccaoImpressaoField.build(), coletorImpressaoField.build());
                    }));
                    boxFields.add(FormBox.create(boxCol1 -> {
                        boxCol1.vertical();
                        boxCol1.add(ofImpressaoField.build(), impressoColetaField.build(), paisImpressaoField.build());
                    }));
                }));
                filter.find.setOnAction(evt -> {
                    String numero = ofImpressaoField.value.get() == null ? "" : ofImpressaoField.value.get();
                    String setor = setorImpressaoField.textValue.get() == null ? "" : setorImpressaoField.textValue.get();
                    String deposito = depositoImpressaoField.textValue.get() == null ? "" : depositoImpressaoField.textValue.get();
                    Object[] faccao = faccaoImpressaoField.textValue.get() == null ? new Object[]{} : faccaoImpressaoField.objectValues.stream().map(it -> it.codcliProperty().getValue()).toArray();
                    Object[] coletor = coletorImpressaoField.textValue.get() == null ? new Object[]{} : coletorImpressaoField.objectValues.stream().map(it -> it.codigoProperty().getValue()).toArray();
                    String status = statusColetaField.value.get() == null ? "" : statusColetaField.value.get();
                    String impresso = impressoColetaField.value.get() == null ? "" : impressoColetaField.value.get();
                    String pais = paisImpressaoField.value.getValue() == null ? "A" : paisImpressaoField.value.getValue();
                    getOfsProgramadas(numero, setor, deposito, faccao, coletor, status, impresso, pais);
                });
                filter.clean.setOnAction(evt -> {
                    ofImpressaoField.clear();
                    setorImpressaoField.clear();
                    depositoImpressaoField.clear();
                    statusColetaField.select(0);
                    faccaoImpressaoField.clear();
                    coletorImpressaoField.clear();
                    paisImpressaoField.select(0);
                });
            }));
        }));
        impressaoTab.getChildren().add(tblOfsProgramadas.build());
    }

    private void addColetores(Object[] deposito, String pais) {
        JPAUtils.clearEntitys(coletores);
        getColetores(deposito, pais);
        boxColetores.clear();
        coletores.forEach(coletor -> {
            FormTableView<SdOfsColetor> tblOfsColaborador = FormTableView.create(SdOfsColetor.class, table -> {
                table.expanded();
                table.items.set(getOfsColetor(coletor.getCodigo()));
                if (table.items.size() == 0)
                    table.items.add(new SdOfsColetor());
                table.columns(
                        FormTableColumn.create(cln -> {
                            cln.title("");
                            cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                            cln.width(34);
                            cln.alignment(FormTableColumn.Alignment.CENTER);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdOfsColetor, SdOfsColetor>, ObservableValue<SdOfsColetor>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                            cln.format(param -> {
                                return new TableCell<SdOfsColetor, SdOfsColetor>() {
                                    final HBox boxButtonsRow = new HBox(3);
                                    final Button deletarOfColetor = FormButton.create(btn -> {
                                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                        btn.tooltip("Excluir OF para coletor");
                                        btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                        btn.width(30);
                                        btn.addStyle("xs").addStyle("danger");
                                    });

                                    @Override
                                    protected void updateItem(SdOfsColetor item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        setGraphic(null);
                                        if (item != null && !empty) {
                                            deletarOfColetor.setOnAction(evt -> {
                                                if (!((Boolean) QuestionBox.build(Boolean.class, message -> {
                                                    message.message("Deseja realmente excluir a OF da fila do coletor?");
                                                    message.showAndWait();
                                                }).value.get())) {
                                                    return;
                                                }
                                                SdOfsColetor ofColetor = item;
                                                if (ofColetor.getNumero() != null) {
                                                    if (!ofColetor.getStatus().equals("A")) {
                                                        MessageBox.create(message -> {
                                                            message.message("Essa OF não pode ser excluída, coletor já iniciou a coleta dos materiais.");
                                                            message.type(MessageBox.TypeMessageBox.WARNING);
                                                            message.showAndWait();
                                                        });
                                                    } else {
                                                        VSdOfsParaColeta ofExcluida = new VSdOfsParaColeta(ofColetor.getNumero(), ofColetor.getSetor(), ofColetor.getDeposito().getCodigo(), ofColetor.getTipo(), ofColetor.getPeriodo(), ofColetor.getDeposito().getPais());
                                                        deleteOfColetor(ofColetor);
                                                        tblOfsParaColeta.items.add(ofExcluida);
                                                        table.items.remove(ofColetor);
                                                        table.refresh();
                                                        tblOfsParaColeta.refresh();
                                                    }
                                                }
                                            });
                                            boxButtonsRow.getChildren().clear();
                                            boxButtonsRow.getChildren().add(deletarOfColetor);
                                            setGraphic(boxButtonsRow);
                                        }
                                    }
                                };
                            });
                        }).build() /* Ações */,
                        FormTableColumn.create(cln -> {
                            cln.title("OF");
                            cln.width(45);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdOfsColetor, SdOfsColetor>, ObservableValue<SdOfsColetor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumero()));
                            cln.alignment(FormTableColumn.Alignment.CENTER);
                        }).build() /* OF */,
                        FormTableColumn.create(cln -> {
                            cln.title("Dt. Ent.");
                            cln.width(80.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdOfsColetor, SdOfsColetor>, ObservableValue<SdOfsColetor>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                            cln.hide();
                            cln.format(param -> {
                                return new TableCell<SdOfsColetor, SdOfsColetor>() {
                                    @Override
                                    protected void updateItem(SdOfsColetor item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        if (item != null && !empty) {
                                            Of1 ofColeta = (Of1) new FluentDao().selectFrom(Of1.class).where(it -> it.equal("numero", item.getNumero())).singleResult();
                                            setText(ofColeta == null ? "-" : StringUtils.toDateFormat(ofColeta.getDtFinal()));
                                        }
                                    }
                                };
                            });
                        }).build() /*Dt. Ent.*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Dt. Cort.");
                            cln.width(80.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdOfsColetor, SdOfsColetor>, ObservableValue<SdOfsColetor>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                            cln.format(param -> {
                                return new TableCell<SdOfsColetor, SdOfsColetor>() {
                                    @Override
                                    protected void updateItem(SdOfsColetor item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        if (item != null && !empty) {
                                            Faccao3 ofColeta = (Faccao3) new FluentDao().selectFrom(Faccao3.class).where(it -> it.equal("id.numero", item.getNumero()).isIn("id.op", new String[]{"103", "130", "133", "135", "203", "219"})).findFirst();
                                            setText(ofColeta == null ? "-" : StringUtils.toDateFormat(ofColeta.getDtlan()));
                                        }
                                    }
                                };
                            });
                        }).build() /*Dt. Cort.*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Setor");
                            cln.width(40);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdOfsColetor, SdOfsColetor>, ObservableValue<SdOfsColetor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getSetor()));
                            cln.alignment(FormTableColumn.Alignment.CENTER);
                        }).build() /* Setor */,
                        FormTableColumn.create(cln -> {
                            cln.title("Dep.");
                            cln.width(40);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdOfsColetor, SdOfsColetor>, ObservableValue<SdOfsColetor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDeposito()));
                            cln.alignment(FormTableColumn.Alignment.CENTER);
                        }).build() /* Dep. */,
                        FormTableColumn.create(cln -> {
                            cln.title("Fac.");
                            cln.width(120.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdOfsColetor, SdOfsColetor>, ObservableValue<SdOfsColetor>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                            cln.alignment(FormTableColumn.Alignment.LEFT);
                            cln.format(param -> {
                                return new TableCell<SdOfsColetor, SdOfsColetor>() {
                                    @Override
                                    protected void updateItem(SdOfsColetor item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        if (item != null && !empty) {
                                            if (item.getFaccao() != null)
                                                setText(item.getFaccao().toString());
                                        }
                                    }
                                };
                            });
                        }).build() /* Fac. */,
                        FormTableColumn.create(cln -> {
                            cln.title("Ord.");
                            cln.width(40);
                            cln.value((Callback<TableColumn.CellDataFeatures<SdOfsColetor, SdOfsColetor>, ObservableValue<SdOfsColetor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getOrdem()));
                            cln.alignment(FormTableColumn.Alignment.CENTER);
                        }).build() /* Ord. */);
                table.factoryRow(constructRow -> {
                    TableRow<SdOfsColetor> row = new TableRow<SdOfsColetor>() {
                        @Override
                        protected void updateItem(SdOfsColetor item, boolean empty) {
                            super.updateItem(item, empty);
                            clear();
                            if (item != null && !empty)
                                if (item.isPintarOf())
                                    getStyleClass().add("table-row-primary");
                                else if (item.getTipo() != null && item.getTipo().equals("R"))
                                    getStyleClass().add("table-row-warning");
                                else if (item.getPeriodo() != null && item.getPeriodo().equals("M"))
                                    getStyleClass().add("table-row-mostruario");
                                else
                                    getStyleClass().add(item.getStatus().equals("E") ? "table-row-success" : item.getStatus().equals("P") ? "table-row-info" : "");
                        }

                        private void clear() {
                            getStyleClass().removeAll("table-row-danger", "table-row-success", "table-row-warning", "table-row-info", "table-row-mostruario", "table-row-primary");
                        }
                    };
                    row = table.setDragAndDrop(row,
                            dragRow -> {
                                ofsSelecionadasParaColeta.clear();
                                ofsColetorSelecionadas.clear();
                                if (!((SdOfsColetor) table.selectedItem()).getStatus().equals("A")) {
                                    MessageBox.create(message -> {
                                        message.message("Você pode reordenar somente as OFs que não iniciaram a coleta de materiais.");
                                        message.type(MessageBox.TypeMessageBox.WARNING);
                                        message.showAndWait();
                                    });
                                } else
                                    ofsColetorSelecionadas.add((SdOfsColetor) table.selectedItem());
                            },
                            dropRow -> {
                                if (ofsSelecionadasParaColeta.size() > 0) {
                                    List<String> depositosOfsSelecionadas = ofsSelecionadasParaColeta.stream().map(ofColeta -> ofColeta.getDeposito()).distinct().collect(Collectors.toList());

                                    if (depositosOfsSelecionadas.stream().anyMatch(s -> {
                                        Deposito depositoOf = (Deposito) new FluentDao().selectFrom(Deposito.class).where(it -> it.equal("codigo", s)).singleResult();
                                        return !s.equals(coletor.getDeposito().getCodigo()) && depositoOf.getCodcli() == null;
                                    })) {
                                        MessageBox.create(message -> {
                                            message.message("Você está atribuindo uma coleta a um depósito que o coletor não está configurado.");
                                            message.type(MessageBox.TypeMessageBox.ALERT);
                                            message.showAndWait();
                                        });
                                    } else {
                                        if (((SdOfsColetor) table.items.get(0)).getNumero() == null)
                                            table.items.remove(0);

                                        ofsSelecionadasParaColeta.forEach(ofParaColeta -> {
                                            VSdFaccoesOf faccaoOfSetor = getFaccaoOfSetor(ofParaColeta.getNumero(), ofParaColeta.getSetor());
                                            if (faccaoOfSetor == null || faccaoOfSetor.getFaccao() == null) {
                                                MessageBox.create(message -> {
                                                    message.message("Não foi possível encontrar a facção para o setor: " + ofParaColeta.getSetor() + " da OF:" + ofParaColeta.getNumero());
                                                    message.type(MessageBox.TypeMessageBox.IMPORTANT);
                                                    message.showAndWait();
                                                });
                                            } else {
                                                table.items.set(getOfsColetor(coletor.getCodigo()));

                                                SdOfsColetor ofProgramada = new FluentDao().selectFrom(SdOfsColetor.class)
                                                        .where(it -> it
                                                                .equal("numero", ofParaColeta.getNumero())
                                                                .equal("setor", ofParaColeta.getSetor())
                                                                .equal("deposito", ofParaColeta.getDeposito())
                                                                .equal("tipo", ofParaColeta.getTipo())
                                                                .isIn("status", new String[]{"A", "E"}))
                                                        .singleResult();

                                                if (ofProgramada == null)
                                                    ofProgramada = new SdOfsColetor(
                                                            coletor,
                                                            ofParaColeta.getNumero(),
                                                            ofParaColeta.getSetor(),
                                                            new FluentDao().selectFrom(Deposito.class).where(eb -> eb.equal("codigo",ofParaColeta.getDeposito())).singleResult(),
                                                            table.items.getSize(),
                                                            faccaoOfSetor.getFaccao(),
                                                            ofParaColeta.getTipo(),
                                                            false);
                                                List<SdReservaMateriais> materiais = getReservasColeta(ofProgramada);
                                                if (materiais.size() == 0) {
                                                    MessageBox.create(message -> {
                                                        message.message("Não foi possível carregar os materiais para o setor: " + ofParaColeta.getSetor() + " da OF:" + ofParaColeta.getNumero());
                                                        message.type(MessageBox.TypeMessageBox.IMPORTANT);
                                                        message.showAndWait();
                                                    });
                                                } else {
                                                    try {
                                                        addOfColetor(ofProgramada, materiais);

                                                        table.items.add(ofProgramada);
                                                        tblOfsParaColeta.items.remove(ofParaColeta);
                                                        MessageBox.create(message -> {
                                                            message.message("OF adicionada para o coletor.");
                                                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                            message.position(Pos.TOP_RIGHT);
                                                            message.notification();
                                                        });
                                                        JPAUtils.clearEntitys(table.items);
                                                    } catch (SQLException e) {
                                                        e.printStackTrace();
                                                        ExceptionBox.build(message -> {
                                                            message.exception(e);
                                                            message.showAndWait();
                                                        });
                                                    }
                                                }
                                            }
                                        });
                                    }
                                } else if (ofsColetorSelecionadas.size() > 0) {
                                    TableRow<SdOfsColetor> finalRow = (TableRow<SdOfsColetor>) dropRow;
                                    int dropIndex;
                                    SdOfsColetor dI = null;

                                    if (finalRow.isEmpty()) {
                                        dropIndex = table.items.size();
                                    } else {
                                        dropIndex = finalRow.getIndex();
                                        dI = (SdOfsColetor) table.items.get(dropIndex);
                                    }
                                    int delta = 0;
                                    if (dI != null)
                                        while (ofsColetorSelecionadas.contains(dI)) {
                                            delta = 1;
                                            --dropIndex;
                                            if (dropIndex < 0) {
                                                dI = null;
                                                dropIndex = 0;
                                                break;
                                            }
                                            dI = (SdOfsColetor) table.items.get(dropIndex);
                                        }

                                    for (SdOfsColetor sI : ofsColetorSelecionadas) {
                                        table.items.remove(sI);
                                    }

                                    if (dI != null)
                                        dropIndex = table.items.indexOf(dI) + delta;
                                    else if (dropIndex != 0)
                                        dropIndex = table.items.size();

                                    table.tableProperties().getSelectionModel().clearSelection();

                                    for (SdOfsColetor sI : ofsColetorSelecionadas) {
                                        //draggedIndex = selections.get(i);
                                        table.items.add(dropIndex, sI);
                                        table.tableProperties().getSelectionModel().select(dropIndex);
                                        dropIndex++;
                                    }

                                    table.items.stream().forEach(coleta -> {
                                        ((SdOfsColetor) coleta).setOrdem(table.items.indexOf(coleta));
                                        new FluentDao().runNativeQueryUpdate(String.format("update sd_ofs_coletor_001 set ordem = %s where programacao = %s", ((SdOfsColetor) coleta).getOrdem(), ((SdOfsColetor) coleta).getProgramacao()));
                                    });
                                    SysLogger.addSysDelizLog("Gerenciar Coletas Almoxarifado", TipoAcao.EDITAR, String.valueOf(coletor.getCodigo()), "Reodenado OFs para coleta no almoxarifado.");
                                }
                                ofsColetorSelecionadas.clear();
                                ofsSelecionadasParaColeta.clear();
                            });
                    return row;
                });
                table.indices(
                        table.indice(Color.GOLD, "Rep", false),
                        table.indice(Color.GREEN, "Em col", false)
                );
            });
            FormFieldText fieldOfColaborador = FormFieldText.create(field -> {
                field.withoutTitle();
                field.label("OF:");
                field.width(150.0);
                field.keyReleased(evt -> {
                    if (evt.getCode() == KeyCode.ENTER) {
                        tblOfsColaborador.items.forEach(ofColaborador -> {
                            if (ofColaborador.getNumero().equals(field.value.get())) {
                                ofColaborador.setPintarOf(true);
                                tblOfsColaborador.refresh();
                            }
                        });
                    }
                });
            });
            boxColetores.add(FormBox.create(boxColetor -> {
                boxColetor.vertical();
                boxColetor.width(400.0);
                boxColetor.add(FormFieldText.create(field -> {
                    field.title("Colaborador");
                    field.value.set(coletor.toString());
                }).build());
                boxColetor.add(FormFieldText.create(field -> {
                    field.title("Depósito");
                    field.value.set(coletor.getDeposito().toString());
                }).build());
                boxColetor.add(tblOfsColaborador.build());
                boxColetor.add(FormBox.create(boxFooter -> {
                    boxFooter.horizontal();
                    boxFooter.add(fieldOfColaborador.build());
                    boxFooter.add(FormButton.create(btnLimpar -> {
                        btnLimpar.title("Limpar");
                        btnLimpar.icon(ImageUtils.getIcon(ImageUtils.Icon.LIMPAR, ImageUtils.IconSize._16));
                        btnLimpar.addStyle("warning");
                        btnLimpar.setAction(evt -> {
                            tblOfsColaborador.items.forEach(ofColetor -> ofColetor.setPintarOf(false));
                            fieldOfColaborador.clear();
                            tblOfsColaborador.refresh();
                        });
                    }));
                }));
            }));
        });
        JPAUtils.clearEntitys(coletores);
    }

    private void imprimirRequisicao(VSdOfsParaColeta selectedItem) {
        try {
            SdColaborador coletorRomaneio = new FluentDao().selectFrom(SdColaborador.class).where(it -> it.equal("codigo", 581)).singleResult();
            VSdFaccoesOf faccaoOfSetor = getFaccaoOfSetor(selectedItem.getNumero(), selectedItem.getSetor());
            if (faccaoOfSetor == null || faccaoOfSetor.getFaccao() == null) {
                MessageBox.create(message -> {
                    message.message("Não foi possível encontrar a facção para o setor: " + selectedItem.getSetor() + " da OF:" + selectedItem.getNumero());
                    message.type(MessageBox.TypeMessageBox.IMPORTANT);
                    message.showAndWait();
                });
            } else {
                SdOfsColetor ofProgramada = new SdOfsColetor(
                        coletorRomaneio,
                        selectedItem.getNumero(),
                        selectedItem.getSetor(),
                        new FluentDao().selectFrom(Deposito.class).where(eb -> eb.equal("codigo", selectedItem.getDeposito())).singleResult(),
                        0,
                        faccaoOfSetor.getFaccao(),
                        selectedItem.getTipo(),
                        true);
                List<SdReservaMateriais> materiais = getReservasColeta(ofProgramada);
                if (materiais.size() == 0) {
                    MessageBox.create(message -> {
                        message.message("Não foi possível carregar os materiais para o setor: " + selectedItem.getSetor() + " da OF:" + selectedItem.getNumero());
                        message.type(MessageBox.TypeMessageBox.IMPORTANT);
                        message.showAndWait();
                    });
                } else {
                    addOfColetaRequisicao(ofProgramada, materiais);
                    tblOfsParaColeta.items.remove(selectedItem);
                    new ReportUtils().config().addReport(ReportUtils.ReportFile.ROMANEIO_COLETA_DEPOSITO,
                            new ReportUtils.ParameterReport[]{
                                    new ReportUtils.ParameterReport("pNumero", ofProgramada.getNumero()),
                                    new ReportUtils.ParameterReport("pSetor", ofProgramada.getSetor()),
                                    new ReportUtils.ParameterReport("pDeposito", ofProgramada.getDeposito()),
                                    new ReportUtils.ParameterReport("pTipo", selectedItem.getTipo().equals("N"))
                            }).view().toView();
                }
            }

        } catch (JRException | SQLException e) {
            e.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(e);
                message.showAndWait();
            });
        }
    }

    private void agruparColeta(ObservableList<VSdOfsParaColeta> selectedItems) {
        if (selectedItems.stream().map(it -> it.getNumero()).distinct().count() > 1) {
            MessageBox.create(message -> {
                message.message("Você não pode agrupar OFs diferentes, verifique que as OFs devem ser do mesmo número.");
                message.type(MessageBox.TypeMessageBox.IMPORTANT);
                message.showAndWait();
            });
            return;
        }
        if (selectedItems.stream().map(it -> it.getSetor()).distinct().count() > 1) {
            MessageBox.create(message -> {
                message.message("Você não pode agrupar setores diferentes, verifique que as OFs devem estar no mesmo setor de consumo.");
                message.type(MessageBox.TypeMessageBox.IMPORTANT);
                message.showAndWait();
            });
            return;
        }
        if (selectedItems.stream().filter(it -> !it.getTipo().equals("M")).map(it -> it.getTipo()).distinct().count() > 1) {
            MessageBox.create(message -> {
                message.message("Você não pode agrupar coletas normais com coletas de reposição.");
                message.type(MessageBox.TypeMessageBox.IMPORTANT);
                message.showAndWait();
            });
            return;
        }

        new Fragment().show(fragment -> {
            fragment.title("Agrupar Coleta de OFs");
            fragment.size(300.0, 280.0);
            final FormFieldSingleFind<Deposito> depositoAgrupadorField = FormFieldSingleFind.create(Deposito.class, field -> {
                field.title("Depósito Agrupador");
            });
            final FormTableView<VSdOfsParaColeta> tblOfsParaAgrupar = FormTableView.create(VSdOfsParaColeta.class, table -> {
                table.title("OFs para Agrupar");
                table.expanded();
                table.items.set(selectedItems);
                table.columns(
                        FormTableColumn.create(cln -> {
                            cln.title("OF");
                            cln.width(60.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdOfsParaColeta, VSdOfsParaColeta>, ObservableValue<VSdOfsParaColeta>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumero()));
                            cln.alignment(FormTableColumn.Alignment.CENTER);
                        }).build(),
                        FormTableColumn.create(cln -> {
                            cln.title("Setor");
                            cln.width(50.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdOfsParaColeta, VSdOfsParaColeta>, ObservableValue<VSdOfsParaColeta>>) param -> new ReadOnlyObjectWrapper(param.getValue().getSetor()));
                            cln.alignment(FormTableColumn.Alignment.CENTER);
                        }).build(),
                        FormTableColumn.create(cln -> {
                            cln.title("Depósito");
                            cln.width(50.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdOfsParaColeta, VSdOfsParaColeta>, ObservableValue<VSdOfsParaColeta>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDeposito()));
                            cln.alignment(FormTableColumn.Alignment.CENTER);
                        }).build(),
                        FormTableColumn.create(cln -> {
                            cln.title("Itens");
                            cln.width(50.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdOfsParaColeta, VSdOfsParaColeta>, ObservableValue<VSdOfsParaColeta>>) param -> new ReadOnlyObjectWrapper(param.getValue().getItens()));
                            cln.alignment(FormTableColumn.Alignment.CENTER);
                        }).build()
                );
            });
            fragment.box.getChildren().addAll(depositoAgrupadorField.build(), tblOfsParaAgrupar.build());
            final Button btnAgrupar = FormButton.create(btn -> {
                btn.title("Agrupar");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.AGRUPAR, ImageUtils.IconSize._24));
                btn.addStyle("primary");
                btn.setAction(evt -> {
                    if (depositoAgrupadorField.value.get() == null) {
                        MessageBox.create(message -> {
                            message.message("Você precisa informar o depósito agrupador.");
                            message.type(MessageBox.TypeMessageBox.IMPORTANT);
                            message.showAndWait();
                        });
                        return;
                    }
                    if (selectedItems.stream().filter(it -> it.getDeposito().equals(depositoAgrupadorField.value.get().getCodigo())).count() == 0) {
                        MessageBox.create(message -> {
                            message.message("O depósito agrupador deve obrigatóriamente ser um dos depósitos das OFs selecionadas.");
                            message.type(MessageBox.TypeMessageBox.IMPORTANT);
                            message.showAndWait();
                        });
                        return;
                    }
                    agruparOfs(selectedItems, depositoAgrupadorField.value.get());
                    fragment.close();
                });
            });
            fragment.buttonsBox.getChildren().add(btnAgrupar);
        });
    }

}
