package sysdeliz2.views.pcp.almoxarifado;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.image.Image;
import javafx.util.Callback;
import sysdeliz2.controllers.views.pcp.almoxarifado.ImpressaoEtiquetaPAController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.daos.generics.helpers.Ordenacao;
import sysdeliz2.models.ti.Cor;
import sysdeliz2.models.ti.Of1;
import sysdeliz2.models.ti.OfIten;
import sysdeliz2.models.ti.Produto;
import sysdeliz2.models.view.VSdComposicaoCorOf;
import sysdeliz2.models.view.VSdDadosProdutoCorTam;
import sysdeliz2.models.view.pcp.VStatusProdutoCor;
import sysdeliz2.utils.SortUtils;
import sysdeliz2.utils.enums.PredicateType;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.hibernate.DefaultFilter;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

public class ImpressaoEtiquetaPAView extends ImpressaoEtiquetaPAController {

    // <editor-fold defaultstate="collapsed" desc="Imagens">
    private FormFieldImage imagem = FormFieldImage.create(img -> {
        img.withoutTitle();
        img.height(400);
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Filtros">
    private final FormFieldSingleFind<Of1> singleFindOf = FormFieldSingleFind.create(Of1.class, field -> {
        field.title("OF");
        field.autoFind.setValue(false);
    });

    private final FormFieldSingleFind<Produto> singleFindProduto = FormFieldSingleFind.create(Produto.class, field -> {
        field.title("Produto");
        field.root.setVisible(false);
    });

    private final FormFieldMultipleFind<Cor> multipleFindCores = FormFieldMultipleFind.create(Cor.class, cores -> {
        cores.title("Cores");
        cores.width(350);
    });

    private Of1 campoFiltroOf = singleFindOf.value.get();
    private Produto campoFiltroProduto = singleFindProduto.value.get();
    private List<Cor> campoFiltroCor = multipleFindCores.objectValues.get();
    private List<Cor> cores = new ArrayList<>();
    private List<OfIten> ofItensList = new ArrayList<>();
    private List<VSdComposicaoCorOf> composicaoCorOf = new ArrayList<>();

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Seleção">

    private final FormLabel labelReferenciaSelecao = FormLabel.create(lbl -> {
        lbl.setText("Referência:");
        lbl.boldText();
    });

    private final FormButton btnZerarGrade = FormButton.create(btn -> {
        btn.addStyle("danger");
        btn.title("Zerar Grade");
        btn.tooltip("Altera a quantidade dos tamanhos para ZERO");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.LIMPAR, ImageUtils.IconSize._16));
        btn.disable.setValue(true);
        btn.setOnAction(event -> {
            zerarGrade();
        });
    });

    private final ListProperty<OfIten> listaItensGradeBean = new SimpleListProperty<>(FXCollections.observableArrayList());

    private final FormTableView<VSdComposicaoCorOf> tabelaGrade = FormTableView.create(VSdComposicaoCorOf.class, table -> {
        table.title("Grade");
        table.items.bind(listaItensGradeBean);

        table.columns(FormTableColumn.create(cln -> {
            cln.title("Cor");
            cln.width(50);
            cln.value((Callback<TableColumn.CellDataFeatures<OfIten, OfIten>, ObservableValue<OfIten>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCor().getCor()));
        }).build(), FormTableColumn.create(cln -> {
            cln.title("Composição Cor");
            cln.width(300);
            cln.value((Callback<TableColumn.CellDataFeatures<OfIten, OfIten>, ObservableValue<OfIten>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
            cln.format(param -> new TableCell<OfIten, OfIten>() {
                @Override
                protected void updateItem(OfIten item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(null);
                    if (item != null && !empty) {
                        if (composicaoCorOf != null) {
                            if (composicaoCorOf.size() > 0) {
                                List<VSdComposicaoCorOf> collect = composicaoCorOf.stream().filter(comp -> comp.getCor().equals(item.getId().getCor().getCor())).collect(Collectors.toList());
                                if(!collect.isEmpty()) {
                                    setText(collect.get(0).getComposicao());
                                }
                            }
                        }
                    }
                }
            });
        }).build());
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Ações">
    private static final Integer TAMANHO_BTN_ACOES = 190;

    CheckBox checkboxTxt = new CheckBox();

    {
        checkboxTxt.setText("Gerar arquivo TXT");
    }

    private final FormButton btnImpressao = FormButton.create(btn -> {
        btn.setPrefWidth(TAMANHO_BTN_ACOES);
        btn.addStyle("md");
        btn.addStyle("success");
        btn.title("Imprimir");
        btn.tooltip("Imprime a tag criada");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.IMPRIMIR, ImageUtils.IconSize._24));
        btn.disable.setValue(true);
        btn.setOnAction(event -> {
            if (super.imprimirAction(checkboxTxt.isSelected(), btn.getScene().getWindow())) {
                singleFindOf.clear();
                singleFindProduto.clear();
                multipleFindCores.clear();
                btnZerarGrade.disable.setValue(true);
                imagem.clear();
                btn.disable.setValue(true);
            }
        });
    });

    private final FormButton btnTagComposicao = FormButton.create(btn -> {
        btn.setPrefWidth(TAMANHO_BTN_ACOES);
        btn.addStyle("secundary");
        btn.title("Criar Tag composição");
        btn.tooltip("Cria Tag composição");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.TAG_TIPO, ImageUtils.IconSize._16));
        btn.disable.setValue(true);
        btn.setOnAction(event -> {
            if (singleFindOf.value.getValue() != null) {
                Image image = super.criarPreviewComposicao(campoFiltroOf.getItens(), multipleFindCores.objectValues.get(), campoFiltroOf.getProduto());
                if (image != null) {
                    btnImpressao.disable.setValue(false);
                    checkboxTxt.setDisable(false);
                    imagem.image(image);
                }
            }
        });
    });

    private final FormButton btnTagPA = FormButton.create(btn -> {
        btn.setPrefWidth(TAMANHO_BTN_ACOES);
        btn.addStyle("secundary");
        btn.title("Criar Tag de produto acabado");
        btn.tooltip("Cria Tag de produto acabado");
        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.TAG_TIPO, ImageUtils.IconSize._16));
        btn.disable.setValue(true);
        btn.setOnAction(event -> {
            if (singleFindOf.value.getValue() != null) {
                Image image = super.criarPreviewProduto(campoFiltroOf.getItens(), multipleFindCores.objectValues.get(), campoFiltroOf.getProduto());
                if (image != null) {
                    btnImpressao.disable.setValue(false);
                    checkboxTxt.setDisable(false);
                    imagem.image(image);
                }
            } else if (campoFiltroProduto != null) {

                Image image = super.criarPreviewProduto(ofItensList, multipleFindCores.objectValues.get(), campoFiltroProduto);
                if (image != null) {
                    btnImpressao.disable.setValue(false);
                    checkboxTxt.setDisable(false);
                    imagem.image(image);
                }
            }
        });
    });


    // </editor-fold>

    public ImpressaoEtiquetaPAView() {
        super("Impressão de Etiquetas de Produtos Acabados", ImageUtils.getImage(ImageUtils.Icon.IMPRIMIR_TAG));
        init();

        singleFindOf.value.addListener((observable, oldValue, newValue) -> {
            campoFiltroOf = newValue;

            if (newValue != null) {
                desabilitarCampoProduto(true);

                List<VStatusProdutoCor> viewStatusProdutoCor = (List<VStatusProdutoCor>) new FluentDao().selectFrom(VStatusProdutoCor.class).where(it -> it.equal("id.produto.codigo", newValue.getProduto().getCodigo())).orderBy("id.cor.cor", OrderType.ASC).resultList();
                if (viewStatusProdutoCor == null) return;

                cores = viewStatusProdutoCor.stream().map(it -> it.getId().getCor()).filter(cor -> newValue.getItens().stream().anyMatch(ofIten -> ofIten.getId().getCor().getCor().equals(cor.getCor()))).collect(Collectors.toList());
                if (cores.isEmpty()) return;

                adicionaCoresTabela(newValue.getProduto().getCodigo());

                JPAUtils.clearEntity(newValue);
                JPAUtils.clearEntitys(newValue.getItens());

                desabilitarBotoes(false);
            } else {
                ofItensList = new ArrayList<>();
                limpaTabela();
                desabilitarCampoProduto(false);
                desabilitarBotoes(true);
            }
        });
        /*singleFindProduto.value.addListener((observable, oldValue, newValue) -> {

            campoFiltroProduto = newValue;
            if (newValue != null) {
                desabilitarCampoOf(true);

                List<VSdDadosProdutoCorTam> viewStatusProdutoCorTam = (List<VSdDadosProdutoCorTam>) new FluentDao().selectFrom(VSdDadosProdutoCorTam.class).where(it -> it.equal("codigo", newValue.getCodigo())).resultList();
                if (viewStatusProdutoCorTam == null) return;

                cores = (List<Cor>) new FluentDao().selectFrom(Cor.class).where(it -> it.isIn("cor", viewStatusProdutoCorTam.stream().map(VSdDadosProdutoCorTam::getCor).toArray())).resultList();
                if (cores == null || cores.isEmpty()) return;

                viewStatusProdutoCorTam.forEach(it -> ofItensList.add(new OfIten(cores.stream().filter(cor -> cor.getCor().equals(it.getCor())).findFirst().get(), it.getTam(), it.getCodigo())));

                adicionaCoresTabela(newValue.getCodigo());

                JPAUtils.clearEntity(newValue);

                desabilitarBotoes(false);
                btnTagComposicao.disable.setValue(true);
            } else {
                ofItensList = new ArrayList<>();
                limpaTabela();
                desabilitarCampoOf(false);
                desabilitarBotoes(true);
            }

        });*///filtro por produto desativado
        multipleFindCores.objectValues.addListener((observable, oldValue, newValue) -> {

            campoFiltroCor = newValue;

            if (newValue == null) {
                limpaTabela();
                return;
            }
            listaItensGradeBean.clear();
            construirTabela();
        });
    }

    private void construirTabela() {

        // Remove colunas
        int sizeTabela = tabelaGrade.tableProperties().getColumns().size();
        tabelaGrade.removeColumn(2, sizeTabela);

        List<String> tamanhos = new ArrayList<>();
        if (campoFiltroOf != null) {

            composicaoCorOf = (List<VSdComposicaoCorOf>) new FluentDao().selectFrom(VSdComposicaoCorOf.class).where(it -> it.equal("numero", campoFiltroOf.getNumero())).orderBy(Arrays.asList(new Ordenacao("desc", "codigocomposicao"), new Ordenacao("desc", "numero"))).resultList();

            cores = cores.stream().filter(cor -> composicaoCorOf.stream().anyMatch(comp -> comp.getCor().equals(cor.getCor()))).collect(Collectors.toList());

            tamanhos = campoFiltroOf.getItens().stream().map(it -> it.getId().getTamanho()).distinct().sorted((o1, o2) -> SortUtils.sortTamanhos(o1, o2)).collect(Collectors.toList());
            Map<Cor, List<OfIten>> collect = campoFiltroOf.getItens().stream().filter(ofIten -> campoFiltroCor.stream().anyMatch(cor -> cor.getCor().equals(ofIten.getId().getCor().getCor()))).filter(ofIten -> cores.stream().anyMatch(cor -> cor.getCor().equals(ofIten.getId().getCor().getCor()))).collect(Collectors.groupingBy(cor -> cor.getId().getCor()));

            collect.forEach((cor, itens) -> listaItensGradeBean.addAll(itens.get(0)));

        } else if (campoFiltroProduto != null) {
            composicaoCorOf = (List<VSdComposicaoCorOf>) new FluentDao().selectFrom(VSdComposicaoCorOf.class).where(it -> it.equal("produto", campoFiltroProduto.getCodigo())).orderBy(Arrays.asList(new Ordenacao("desc", "codigocomposicao"), new Ordenacao("desc", "numero"))).resultList();

            cores = cores.stream().filter(cor -> composicaoCorOf.stream().anyMatch(comp -> comp.getCor().equals(cor.getCor()))).collect(Collectors.toList());

            tamanhos = ofItensList.stream().map(it -> it.getId().getTamanho()).distinct().sorted((o1, o2) -> SortUtils.sortTamanhos(o1, o2)).collect(Collectors.toList());
            Map<Cor, List<OfIten>> collect = ofItensList.stream().filter(ofIten -> campoFiltroCor.stream().anyMatch(cor -> cor.getCor().equals(ofIten.getId().getCor().getCor()))).filter(ofIten -> cores.stream().anyMatch(cor -> cor.getCor().equals(ofIten.getId().getCor().getCor()))).collect(Collectors.groupingBy(cor -> cor.getId().getCor()));

            collect.forEach((cor, itens) -> listaItensGradeBean.addAll(itens.get(0)));
        }


        for (String tamanho : tamanhos) {
            tabelaGrade.addColumn(FormTableColumn.create(cln -> {
                cln.setId(tamanho);
                cln.title(tamanho);
                cln.width(50);
                cln.format(param -> {
                    return new TableCell<OfIten, OfIten>() {
                        @Override
                        protected void updateItem(OfIten item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                setGraphic(FormFieldText.create(it -> {
                                    it.withoutTitle();
                                    it.maxheight(12);
                                    it.decimalField(0);

                                    Optional<OfIten> optionalOfIten = campoFiltroOf != null ? campoFiltroOf.getItens().stream().filter(ofItem -> ofItem.getId().getTamanho().equals(tamanho) && item.getId().getCor().getCor().equals(ofItem.getId().getCor().getCor())).findFirst() : null;
                                    Optional<OfIten> optionalOfItenProduto = ofItensList != null ? ofItensList.stream().filter(cor -> cor.getId().getTamanho().equals(tamanho) && item.getId().getCor().getCor().equals(cor.getId().getCor().getCor())).findFirst() : null;
                                    OfIten ofIten = optionalOfIten != null && optionalOfIten.isPresent() ? optionalOfIten.get() : (optionalOfItenProduto != null && optionalOfItenProduto.isPresent() ? optionalOfItenProduto.get() : null);

                                    it.focusedListener((observable1, oldValue1, newValue1) -> {
                                        if (!newValue1) {
                                            try {
                                                ofIten.setQtde(new BigDecimal(it.value.getValue()));
                                            } catch (Exception e) {
                                                ofIten.setQtde(BigDecimal.ZERO);
                                                it.value.setValue("0");
                                            }
                                        }
                                    });
                                    if (ofIten != null) {
                                        it.setValue(ofIten.getQtde().toString());
                                    } else {
                                        it.setValue("0");
                                    }
                                }).build());
                            }
                        }
                    };
                });
                cln.value((Callback<TableColumn.CellDataFeatures<OfIten, OfIten>, ObservableValue<OfIten>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
            }).build());
        }
    }

    private void adicionaCoresTabela(String codigo) {
        limpaTabela();

        multipleFindCores.defaults.add(new DefaultFilter(cores.stream().map(Cor::getCor).toArray(), "cor", PredicateType.IN));
        multipleFindCores.objectValues.addAll(cores);

        labelReferenciaSelecao.setText("Referência: " + codigo);
    }

    private void limpaTabela() {
        int sizeTabela = tabelaGrade.tableProperties().getColumns().size();
        tabelaGrade.removeColumn(2, sizeTabela);
        listaItensGradeBean.clear();
        labelReferenciaSelecao.setText("Referência: ");
        multipleFindCores.clear();
    }

    private void desabilitarCampoOf(boolean desabilitado) {
        if (desabilitado) {
            singleFindOf.clear();
            singleFindOf.disable.setValue(true);
        } else {
            singleFindOf.disable.setValue(false);
            multipleFindCores.clear();
            multipleFindCores.defaults.clear();
        }
    }

    private void desabilitarCampoProduto(boolean desabilitado) {
        if (desabilitado) {
            singleFindProduto.clear();
            singleFindProduto.disable.setValue(true);
        } else {
            singleFindProduto.disable.setValue(false);
            multipleFindCores.clear();
            multipleFindCores.defaults.clear();
        }
    }

    private void desabilitarBotoes(boolean bool) {
        tabelaGrade.refresh();
        btnTagComposicao.disable.setValue(bool);
        btnTagPA.disable.setValue(bool);
        btnZerarGrade.disable.setValue(bool);
        if (bool) {
            checkboxTxt.setSelected(false);
            checkboxTxt.setDisable(true);
            btnImpressao.disable.setValue(true);
            imagem.clear();
        }
    }

    private void init() {

        this.box.getChildren().add(FormBoxPane.create(root -> {
            root.expanded();
            root.top(FormBox.create(header -> {
                header.setMaxHeight(400);
                header.horizontal();
                header.add(FormTitledPane.create(filtros -> {
                    filtros.setCollapsible(false);
                    filtros.addStyle("info");
                    filtros.title("Filtro");
                    filtros.setPrefWidth(350);
                    filtros.icon(ImageUtils.getImage(ImageUtils.Icon.FILTRO));
                    filtros.add(singleFindOf.build());
                    filtros.add(multipleFindCores.build());
                    filtros.add(singleFindProduto.build());//inverter ordem quando ativar
                }));
                header.add(FormTitledPane.create(selecao -> {
                    selecao.setCollapsible(false);
                    selecao.addStyle("info");
                    selecao.title("Seleção");
                    selecao.setMinWidth(720);

                    selecao.icon(ImageUtils.getImage(ImageUtils.Icon.CAIXA_ABERTA));
                    selecao.add(FormBox.create(grade -> {
                        grade.vertical();
                        grade.add(labelReferenciaSelecao);
                        grade.add(FormBox.create(btn -> {
                            btn.vertical();
                            btn.alignment(Pos.BOTTOM_RIGHT);
                            btn.add(btnZerarGrade);
                        }));
                        grade.add(tabelaGrade.build());
                    }));
                }));
                header.add(FormTitledPane.create(acoes -> {
                    acoes.setCollapsible(false);
                    acoes.addStyle("info");
                    acoes.title("Ações");
                    acoes.icon(ImageUtils.getImage(ImageUtils.Icon.EXECUTAR));
                    acoes.setPrefWidth(TAMANHO_BTN_ACOES + 10);
                    acoes.getBox().setAlignment(Pos.TOP_CENTER);
                    acoes.add(btnTagComposicao);
                    acoes.add(btnTagPA);
                    acoes.add(checkboxTxt);
                    acoes.add(btnImpressao);
                }));
            }));
            root.bottom(FormBox.create(footer -> {
                footer.horizontal();
                footer.add(imagem.build());
            }));
        }));
    }

    private void zerarGrade() {
        if (singleFindOf.value.get() != null) {
            singleFindOf.value.get().getItens().forEach(ofItem -> ofItem.setQtde(BigDecimal.ZERO));
        } else {
            ofItensList.forEach(ofItem -> ofItem.setQtde(BigDecimal.ZERO));
        }
        tabelaGrade.refresh();
    }

}
