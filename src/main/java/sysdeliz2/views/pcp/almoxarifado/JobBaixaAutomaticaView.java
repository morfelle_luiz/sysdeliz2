package sysdeliz2.views.pcp.almoxarifado;

import sysdeliz2.controllers.views.pcp.almoxarifado.JobBaixaAutomaticaController;
import sysdeliz2.utils.gui.components.FormBox;
import sysdeliz2.utils.gui.components.FormButton;
import sysdeliz2.utils.gui.components.FormFieldText;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.sys.ImageUtils;

import java.sql.SQLException;

public class JobBaixaAutomaticaView extends JobBaixaAutomaticaController {
    
    // <editor-fold defaultstate="collapsed" desc="Fields">
    private final FormFieldText inicioUltimaRodadaField = FormFieldText.create(field -> {
        field.title("Início Última Rodada MRP");
        field.editable(false);
        field.value.bind(super.inicioUltimoLog);
    });
    private final FormFieldText tempoUltimaRodadaField = FormFieldText.create(field -> {
        field.title("Tempo Última Rodada MRP");
        field.editable(false);
        field.value.bind(super.tempoUltimoLog);
    });
    private final FormFieldText estadoJobField = FormFieldText.create(field -> {
        field.title("Estado MRP");
        field.editable(false);
        field.value.bind(super.statusJobLog);
        field.value.addListener((observable, oldValue, newValue) -> {
            if (newValue != null){
                if (newValue.equals("EM EXECUÇÃO")){
                    field.addStyle("danger");
                } else {
                    field.removeStyle("danger");
                }
            }
        });
    });
    private final FormFieldText dataProximaRodadaField = FormFieldText.create(field -> {
        field.title("Próxima Rodada MRP");
        field.editable(false);
        field.value.bind(super.dataProximoLog);
    });
    // </editor-fold>
    
    public JobBaixaAutomaticaView() {
        super("Job Baixa Automática", ImageUtils.getImage(ImageUtils.Icon.EXECUTAR));
        init();
    }
    
    private void init() {
        try {
            getStatusJob();
        } catch (SQLException e) {
            e.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(e);
                message.showAndWait();
            });
        }
        super.box.getChildren().add(FormBox.create(boxPrincipal -> {
            boxPrincipal.horizontal();
            boxPrincipal.add(FormBox.create(boxJob -> {
                boxJob.vertical();
                boxJob.title("Job Baixa Materiais");
                boxJob.add(estadoJobField.build());
                boxJob.add(inicioUltimaRodadaField.build());
                boxJob.add(tempoUltimaRodadaField.build());
                boxJob.add(dataProximaRodadaField.build());
                boxJob.add(FormBox.create(lo1 -> {
                    lo1.horizontal();
                    lo1.add(FormButton.create(btn -> {
                        btn.title("Rodar Baixa");
                        btn.addStyle("lg").addStyle("success");
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._32));
                        btn.setAction(event -> {
                            try {
                                runJob();
                            } catch (SQLException e) {
                                e.printStackTrace();
                                ExceptionBox.build(message -> {
                                    message.exception(e);
                                    message.showAndWait();
                                });
                            }
                        });
                    }));
                    lo1.add(FormButton.create(btn -> {
                        btn.title("Atualizar status Baixa");
                        btn.addStyle("sm").addStyle("primary");
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ATUALIZAR, ImageUtils.IconSize._24));
                        btn.setAction(event -> {
                            try {
                                getStatusJob();
                            } catch (SQLException e) {
                                e.printStackTrace();
                                ExceptionBox.build(message -> {
                                    message.exception(e);
                                    message.showAndWait();
                                });
                            }
                        });
                    }));
                }));
            }));
        }));
    }
}
