package sysdeliz2.views.pcp.enfesto;

import javafx.beans.binding.Bindings;
import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxListCell;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import javafx.util.converter.NumberStringConverter;
import net.sf.jasperreports.engine.JRException;
import org.controlsfx.control.CheckListView;
import sysdeliz2.controllers.views.pcp.enfesto.PlanejamentoEnfestoController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.sysdeliz.*;
import sysdeliz2.models.ti.*;
import sysdeliz2.models.view.*;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.ReportUtils;
import sysdeliz2.utils.converters.BigDecimalAttributeConverter;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import javax.persistence.EntityNotFoundException;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.rtf.RTFEditorKit;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class PlanejamentoEnfestoView extends PlanejamentoEnfestoController {
    
    // <editor-fold defaultstate="collapsed" desc="controller bean">
    // beans de dados de carga da OF
    private final ObjectProperty<Of2> beanOfCarregada = new SimpleObjectProperty<>();
    private final ListProperty<VSdCorOf> beanGradeOf = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<VSdMateriaisCorte> beanMateriaisOf = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<VSdMatAplicacaoOf> beanAplicacoesOf = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<SdPlanejamentoEncaixe> beanPlanejamentosOf = new SimpleListProperty<>();
    
    // beans do planejamento de encaixe
    //private final ObjectProperty<SdPlanejamentoEncaixe> planejamento = new SimpleObjectProperty<>();
    
    // controles de negócio da tela
    private final BooleanProperty emEdicao = new SimpleBooleanProperty(false);
    private final BooleanProperty criandoRisco = new SimpleBooleanProperty(false);
    private final BooleanProperty isPt = new SimpleBooleanProperty(false);

    // lists para tela
    private final ListProperty<SdTipoRisco> riscos = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<VSdCorOf> saldoGrade = new SimpleListProperty<>(FXCollections.observableArrayList());
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="view">
    private final VBox tabPlanejamento = (VBox) super.tabs.getTabs().get(0).getContent();
    private final VBox tabListagem = (VBox) super.tabs.getTabs().get(1).getContent();
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="form fields">
    //carga
    private final FormFieldText numeroOfField = FormFieldText.create(field -> {
        field.title("O.F.");
        field.addStyle("lg");
        field.width(100.0);
        field.editable.bind(emEdicao.not());
        field.keyReleased(event -> {
            if (event.getCode() == KeyCode.ENTER && emEdicao.not().get())
                carregarOf(field.value.get());
            else if (event.isControlDown() && event.getCode() == KeyCode.A) {
                cleanFieldsCargaOf();
                field.requestFocus();
            }
        });
    });
    //dados encaixe
    private final FormFieldDateTime dhEncaixeField = FormFieldDateTime.create(field -> {
        field.title("Data/Hora");
        field.editable.set(false);
        //field.value.bindBidirectional(planejamento.get().dtplanoProperty());
    });
    private final FormFieldText usuarioEncaixeField = FormFieldText.create(field -> {
        field.title("Encaixe por");
        field.editable(false);
        //field.value.bindBidirectional(planejamento.get().usuarioProperty());
    });
    private final FormFieldToggleSingle encaixeComGabaritoField = FormFieldToggleSingle.create(field -> {
        field.title("Gabarito");
        field.editable.bind(emEdicao);
        //field.value.bindBidirectional(planejamento.get().gabaritoProperty());
    });
    private final FormFieldToggleSingle tecidoTubularField = FormFieldToggleSingle.create(field -> {
        field.title("Tubular");
        field.editable.bind(emEdicao);
        //field.value.bindBidirectional(planejamento.get().gabaritoProperty());
    });
    //dados of/produto
    private final FormFieldText referenciaFild = FormFieldText.create(field -> {
        field.title("Referência");
        field.editable(false);
        field.width(230.0);
        field.addStyle("xl");
        field.alignment(Pos.CENTER);
    });
    private final FormFieldText descricaoProdutoField = FormFieldText.create(field -> {
        field.title("Produto");
        field.editable(false);
        field.width(320.0);
        field.addStyle("xs");
    });
    private final FormFieldText linhaProdutoField = FormFieldText.create(field -> {
        field.title("Linha");
        field.editable(false);
        field.width(100.0);
        field.addStyle("xs");
    });
    private final FormFieldText gradeProdutoField = FormFieldText.create(field -> {
        field.title("Grade");
        field.editable(false);
        field.width(50.0);
        field.addStyle("xs");
    });
    private final FormFieldText familiaProdutoField = FormFieldText.create(field -> {
        field.title("Família");
        field.editable(false);
        field.addStyle("xs");
    });
    private final FormFieldText marcaProdutoField = FormFieldText.create(field -> {
        field.title("Marca");
        field.editable(false);
        field.width(80.0);
        field.addStyle("xs");
    });
    private final FormFieldText periodoOfField = FormFieldText.create(field -> {
        field.title("Período OF");
        field.editable(false);
        field.width(80.0);
        field.alignment(FormFieldText.Alignment.CENTER);
        field.addStyle("lg");
    });
    private final FormFieldTextArea observacaoOfField = FormFieldTextArea.create(field -> {
        field.title("Observação OF");
        field.editable(false);
        field.width(500.0);
        field.height(150.0);
        field.expanded();
    });
    private final FormTableView<VSdCorOf> tblGradeOf = FormTableView.create(VSdCorOf.class, table -> {
        table.expanded();
        table.title("Grade OF");
        table.items.bind(beanGradeOf);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdCorOf, VSdCorOf>, ObservableValue<VSdCorOf>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCor().getCor()));
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                }) /*Código*/,
                FormTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(150.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdCorOf, VSdCorOf>, ObservableValue<VSdCorOf>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCor().getDescricao()));
                }) /*Descrição*/
        );
    });
    //add risco
    private final FormFieldComboBox<SdTipoRisco> tipoRiscoField = FormFieldComboBox.create(SdTipoRisco.class, field -> {
        field.title("Tipo do Risco");
        field.items.setValue(FXCollections.observableList((List<SdTipoRisco>) new FluentDao().selectFrom(SdTipoRisco.class).get().resultList()));
        field.width(120.0);
        field.select(0);
        field.editable.bind(emEdicao);
    });
    private final FormTabPane tabRiscos = FormTabPane.create(tabPane -> {
        tabPane.expanded();
        tabPane.reorderTabs();
    });
    //fields filter
    private final FormFieldText findNumero = FormFieldText.create(field -> {
        field.title("OF");
        field.width(150.0);
    });
    private final FormFieldText findProgramador = FormFieldText.create(field -> {
        field.title("Programador");
        field.width(200.0);
    });
    private final FormFieldDatePeriod findPeriodo = FormFieldDatePeriod.create(field -> {
        field.title("Data Encaixe");
        field.valueBegin.set(LocalDate.of(1900, 01, 01));
        field.valueEnd.set(LocalDate.of(2050, 12, 31));
    });
    private final FormFieldSingleFind<VSdDadosProduto> findProduto = FormFieldSingleFind.create(VSdDadosProduto.class, field -> {
        field.title("Referância");
    });
    private final FormTableView<SdPlanejamentoEncaixe> tableFilters = FormTableView.create(SdPlanejamentoEncaixe.class, table -> {
        table.title("OFs planejadas");
        table.expanded();
        table.items.bind(beanPlanejamentosOf);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Ações");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(60.0);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdPlanejamentoEncaixe, SdPlanejamentoEncaixe>, ObservableValue<SdPlanejamentoEncaixe>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdPlanejamentoEncaixe, SdPlanejamentoEncaixe>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnRealCortado = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ATUALIZAR, ImageUtils.IconSize._16));
                                btn.tooltip("Cadastrar Real Cortado");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("warning");
                            });
                            final Button btnImprimir = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.IMPRIMIR, ImageUtils.IconSize._16));
                                btn.tooltip("Imprimir Ficha de Corte");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("info");
                            });
                            
                            @Override
                            protected void updateItem(SdPlanejamentoEncaixe item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    
                                    btnRealCortado.setOnAction(evt -> {
                                        new Fragment().show(fragment -> {
                                            fragment.title("Real Cortado OF " + item.getNumero().getNumero());
                                            fragment.size(300.0, 550.0);
                                            fragment.box.getChildren().add(FormBox.create(principal -> {
                                                principal.vertical();
                                                principal.verticalScroll();
                                                principal.add(FormBox.create(boxRisco -> {
                                                    boxRisco.horizontal();
                                                    boxRisco.add(FormFieldText.create(field -> {
                                                        field.withoutTitle();
                                                        field.value.set("RISCOS");
                                                        field.editable(false);
                                                        field.addStyle("dark");
                                                        field.width(70.0);
                                                    }).build());
                                                    boxRisco.add(FormFieldText.create(field -> {
                                                        field.withoutTitle();
                                                        field.value.set("FOLHAS");
                                                        field.editable(false);
                                                        field.alignment(Pos.CENTER);
                                                        field.addStyle("dark");
                                                        field.width(60.0);
                                                    }).build());
                                                    boxRisco.add(FormFieldText.create(field -> {
                                                        field.withoutTitle();
                                                        field.value.set("ROLOS");
                                                        field.editable(false);
                                                        field.alignment(Pos.CENTER);
                                                        field.addStyle("dark");
                                                        field.width(60.0);
                                                    }).build());
                                                    boxRisco.add(FormFieldText.create(field -> {
                                                        field.withoutTitle();
                                                        field.value.set("FOLHAS");
                                                        field.editable(false);
                                                        field.alignment(Pos.CENTER);
                                                        field.addStyle("dark");
                                                        field.width(60.0);
                                                    }).build());
                                                }));
                                                item.getRiscos().sort(Comparator.comparingInt(SdRiscoPlanejamentoEncaixe::getId));
                                                item.getRiscos().forEach(riscos -> {
                                                    riscos.getCores().sort(Comparator.comparingInt(SdCorRisco::getSeq));
                                                    riscos.getCores().forEach(cores -> {
                                                        principal.add(FormBox.create(boxRisco -> {
                                                            boxRisco.horizontal();
                                                            boxRisco.add(FormFieldText.create(field -> {
                                                                field.withoutTitle();
                                                                field.value.set(riscos.getTiporisco().getCodrisco() + riscos.getSeqTipo() + "." + cores.getSeq());
                                                                field.editable(false);
                                                                field.width(70.0);
                                                            }).build());
                                                            FormBox boxRolosRc = FormBox.create(box -> box.vertical());
                                                            boxRisco.add(FormBox.create(boxFolhasCor -> {
                                                                boxFolhasCor.vertical();
                                                                boxFolhasCor.add(FormFieldText.create(field -> {
                                                                    field.withoutTitle();
                                                                    field.mask(FormFieldText.Mask.INTEGER);
                                                                    cores.setFolhasrc(item.isRealcortado() ? cores.getFolhasrc() : cores.getFolhas());
                                                                    field.value.bindBidirectional(cores.folhasrcProperty(), new NumberStringConverter());
                                                                    field.editable(false);
                                                                    field.alignment(Pos.CENTER);
                                                                    field.addStyle("warning");
                                                                    field.width(60.0);
                                                                }).build());
                                                                boxFolhasCor.add(FormButton.create(btnAddRolo -> {
                                                                    btnAddRolo.title("Rolo");
                                                                    btnAddRolo.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._16));
                                                                    btnAddRolo.addStyle("success");
                                                                    btnAddRolo.setAction(event -> {
                                                                        new Fragment().show(fragSelectRolo -> {
                                                                            fragSelectRolo.title("Selecione um rolo dos riscos para seleção");
                                                                            fragSelectRolo.size(350.0, 200.0);
                                                                            List<SdMateriaisRiscoCor> rolosRisco = new ArrayList<>();
                                                                            item.getRiscos().forEach(it -> it.getCores().forEach(it2 -> rolosRisco.addAll(it2.getMateriais())));
                                                                            final FormFieldText idRoloField = FormFieldText.create(field -> {
                                                                                field.title("Id. do Rolo");
                                                                                field.mask(FormFieldText.Mask.INTEGER);
                                                                            });
                                                                            final FormFieldText folhasRoloField = FormFieldText.create(field -> {
                                                                                field.title("Folhas");
                                                                                field.mask(FormFieldText.Mask.INTEGER);
                                                                                field.value.set("0");
                                                                            });
                                                                            fragSelectRolo.box.getChildren().add(FormBox.create(boxFragmente -> {
                                                                                boxFragmente.vertical();
                                                                                boxFragmente.add(idRoloField.build(), folhasRoloField.build());
                                                                            }));
                                                                            fragSelectRolo.buttonsBox.getChildren().add(FormButton.create(btnNovoRolo -> {
                                                                                btnNovoRolo.title("Adicionar Rolo ao Risco");
                                                                                btnNovoRolo.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._16));
                                                                                btnNovoRolo.addStyle("success");
                                                                                btnNovoRolo.setAction(event1 -> {
                                                                                    if (idRoloField.value.get() == null || idRoloField.value.get().equals("")) {
                                                                                        MessageBox.create(message -> {
                                                                                            message.message("Você deve inserir o Id. de um rolo dos riscos.");
                                                                                            message.type(MessageBox.TypeMessageBox.ALERT);
                                                                                            message.showAndWait();
                                                                                        });
                                                                                        return;
                                                                                    }
                                                                                    Optional<SdMateriaisRiscoCor> optRoloParaCopiar = rolosRisco.stream().filter(it -> it.getId().compareTo(Integer.parseInt(idRoloField.value.get())) == 0).findFirst();
                                                                                    if (!optRoloParaCopiar.isPresent()) {
                                                                                        MessageBox.create(message -> {
                                                                                            message.message("O ID informado deve ser um ID de um rolo em qualquer um dos riscos deste planejamento.");
                                                                                            message.type(MessageBox.TypeMessageBox.ALERT);
                                                                                            message.showAndWait();
                                                                                        });
                                                                                        return;
                                                                                    }
                                                                                    SdMateriaisRiscoCor roloParaCopiar = optRoloParaCopiar.get();
                                                                                    if (folhasRoloField.value.get() == null || folhasRoloField.value.get().equals("0")) {
                                                                                        MessageBox.create(message -> {
                                                                                            message.message("Você deve inserir um número de folhas para o consumo.");
                                                                                            message.type(MessageBox.TypeMessageBox.ALERT);
                                                                                            message.showAndWait();
                                                                                        });
                                                                                        return;
                                                                                    }
                                                                                    
                                                                                    SdMateriaisRiscoCor novoRoloRisco = new SdMateriaisRiscoCor(cores.getId(), roloParaCopiar.getCor(), roloParaCopiar.getMaterial(), roloParaCopiar.getCori(),
                                                                                            roloParaCopiar.getLote(), roloParaCopiar.getTonalidade(), roloParaCopiar.getPartida(), roloParaCopiar.getLargura(), roloParaCopiar.getGramatura(),
                                                                                            roloParaCopiar.getSequencia(), BigDecimal.ZERO, 0, 0, BigDecimal.ZERO, cores.getMateriais().stream().mapToInt(SdMateriaisRiscoCor::getSeqEnfesto).max().getAsInt() + 1);
                                                                                    novoRoloRisco.setQtdeFolhasRc(Integer.parseInt(folhasRoloField.value.get()));
                                                                                    try {
                                                                                        novoRoloRisco = new FluentDao().merge(novoRoloRisco);
                                                                                        cores.getMateriais().add(novoRoloRisco);
                                                                                        MessageBox.create(message -> {
                                                                                            message.message("Incluído reserva do rolo para o risco.");
                                                                                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                                                            message.position(Pos.TOP_RIGHT);
                                                                                            message.notification();
                                                                                        });
                                                                                        boxRolosRc.clear();
                                                                                        boxRolosRc.add(FormBox.create(boxRolo -> {
                                                                                            boxRolo.vertical();
                                                                                            cores.getMateriais().sort(Comparator.comparingInt(SdMateriaisRiscoCor::getSeqEnfesto));
                                                                                            cores.getMateriais().forEach(rolos -> {
                                                                                                boxRolo.add(FormBox.create(boxDadosRolo -> {
                                                                                                    boxDadosRolo.horizontal();
                                                                                                    boxDadosRolo.add(FormFieldText.create(field -> {
                                                                                                        field.withoutTitle();
                                                                                                        field.value.set(String.valueOf(rolos.getId()));
                                                                                                        field.editable(false);
                                                                                                        field.width(60.0);
                                                                                                    }).build());
                                                                                                    boxDadosRolo.add(FormFieldText.create(field -> {
                                                                                                        field.withoutTitle();
                                                                                                        field.mask(FormFieldText.Mask.INTEGER);
                                                                                                        rolos.setQtdeFolhasRc(item.isRealcortado() ? rolos.getQtdeFolhasRc() : rolos.getQtdefolhas());
                                                                                                        field.value.bindBidirectional(rolos.qtdeFolhasRcProperty(), new NumberStringConverter());
                                                                                                        rolos.qtdeFolhasRcProperty().addListener((observable, oldValue, newValue) -> {
                                                                                                            if (newValue != null)
                                                                                                                cores.setFolhasrc(cores.getMateriais().stream().mapToInt(SdMateriaisRiscoCor::getQtdeFolhasRc).sum());
                                                                                                        });
                                                                                                        field.alignment(Pos.CENTER);
                                                                                                        field.width(60.0);
                                                                                                    }).build());
                                                                                                }));
                                                                                            });
                                                                                        }));
                                                                                        SysLogger.addSysDelizLog("Real Cortado", TipoAcao.EDITAR, String.valueOf(cores.getId()),
                                                                                                "Incluído rolo referente planejado " + roloParaCopiar.getId() + " com " + folhasRoloField.value.get() + " folhas");
                                                                                        fragSelectRolo.close();
                                                                                    } catch (Exception e) {
                                                                                        e.printStackTrace();
                                                                                        ExceptionBox.build(message -> {
                                                                                            message.exception(e);
                                                                                            message.showAndWait();
                                                                                        });
                                                                                    }
                                                                                });
                                                                            }));
                                                                        });
                                                                    });
                                                                }));
                                                            }));
                                                            boxRolosRc.add(FormBox.create(boxRolo -> {
                                                                boxRolo.vertical();
                                                                cores.getMateriais().sort(Comparator.comparingInt(SdMateriaisRiscoCor::getSeqEnfesto));
                                                                cores.getMateriais().forEach(rolos -> {
                                                                    boxRolo.add(FormBox.create(boxDadosRolo -> {
                                                                        boxDadosRolo.horizontal();
                                                                        boxDadosRolo.add(FormFieldText.create(field -> {
                                                                            field.withoutTitle();
                                                                            field.value.set(String.valueOf(rolos.getId()));
                                                                            field.editable(false);
                                                                            field.width(60.0);
                                                                        }).build());
                                                                        boxDadosRolo.add(FormFieldText.create(field -> {
                                                                            field.withoutTitle();
                                                                            field.mask(FormFieldText.Mask.INTEGER);
                                                                            rolos.setQtdeFolhasRc(item.isRealcortado() ? rolos.getQtdeFolhasRc() : rolos.getQtdefolhas());
                                                                            field.value.bindBidirectional(rolos.qtdeFolhasRcProperty(), new NumberStringConverter());
                                                                            rolos.qtdeFolhasRcProperty().addListener((observable, oldValue, newValue) -> {
                                                                                if (newValue != null)
                                                                                    cores.setFolhasrc(cores.getMateriais().stream().mapToInt(SdMateriaisRiscoCor::getQtdeFolhasRc).sum());
                                                                            });
                                                                            field.alignment(Pos.CENTER);
                                                                            field.width(60.0);
                                                                        }).build());
                                                                    }));
                                                                });
                                                            }));
                                                            boxRisco.add(boxRolosRc.build());
                                                        }));
                                                    });
                                                });
                                            }));
                                            fragment.buttonsBox.getChildren().add(FormButton.create(btnSalvar -> {
                                                btnSalvar.title("Salvar");
                                                btnSalvar.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                                                btnSalvar.addStyle("success");
                                                btnSalvar.setAction(evtSalvar -> {
                                                    item.setRealcortado(true);
                                                    item.getRiscos().forEach(riscos -> {
                                                        riscos.getCores().sort(Comparator.comparingInt(SdCorRisco::getSeq));
                                                        riscos.getCores().forEach(cores -> {
                                                            cores.getGrade().forEach(grade -> {
                                                                grade.setRealizadorc(cores.getFolhasrc() * grade.getGrade());
                                                            });
                                                            cores.setTotalRealizadoRc(cores.getGrade().stream().mapToInt(it -> it.getRealizadorc()).sum());
                                                        });
                                                    });
                                                    new FluentDao().merge(item);
                                                    
                                                    try {
                                                        new ReportUtils()
                                                                .config()
                                                                .addReport(ReportUtils.ReportFile.PLANEJAMENTO_ENCAIXE_RC,
                                                                        new ReportUtils.ParameterReport("id_planejamento", item.getId()),
                                                                        new ReportUtils.ParameterReport("codEmpresa", Globals.getEmpresaLogada().getCodigo()))
                                                                .view().toPdf("L:\\Encaixe\\Planejamento\\" + item.getNumero().getNumero() + "-RC.pdf")
                                                                .viewPdf("L:\\Encaixe\\Planejamento\\" + item.getNumero().getNumero() + "-RC.pdf");
                                                    } catch (IOException | JRException | SQLException e) {
                                                        e.printStackTrace();
                                                        ExceptionBox.build(message -> {
                                                            message.exception(e);
                                                            message.showAndWait();
                                                        });
                                                    }
                                                    
                                                    table.refresh();
                                                    MessageBox.create(message -> {
                                                        message.message("Real cortado realizado com sucesso para a OF " + item.getNumero().getNumero());
                                                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                        message.position(Pos.TOP_RIGHT);
                                                        message.notification();
                                                    });
                                                    fragment.close();
                                                });
                                            }));
                                        });
                                    });
                                    btnImprimir.setOnAction(evt -> {
                                        new Fragment().show(fragment -> {
                                            fragment.title("Imprimir Planejamento");
                                            fragment.size(350.0, 200.0);
                                            final FormFieldToggleSingle printPlano = FormFieldToggleSingle.create(field -> {
                                                field.title("Imprimir Planejamento");
                                                field.value.set(true);
                                            });
                                            final FormFieldToggleSingle printMateriais = FormFieldToggleSingle.create(field -> {
                                                field.title("Imprimir Lista de Materiais");
                                                field.value.set(true);
                                            });
                                            final FormFieldToggleSingle printConferencia = FormFieldToggleSingle.create(field -> {
                                                field.title("Imprimir Folha de Conferência");
                                                field.value.set(true);
                                            });
                                            
                                            fragment.box.getChildren().addAll(printPlano.build(), printMateriais.build(), printConferencia.build());
                                            fragment.buttonsBox.getChildren().add(FormButton.create(btnImprimir -> {
                                                btnImprimir.title("Imprimir");
                                                btnImprimir.icon(ImageUtils.getIcon(ImageUtils.Icon.IMPRIMIR, ImageUtils.IconSize._24));
                                                btnImprimir.addStyle("success");
                                                btnImprimir.setAction(evtPrint -> {
                                                    try {
                                                        if (printPlano.value.get())
                                                            new ReportUtils().config()
                                                                    .addReport(ReportUtils.ReportFile.PLANEJAMENTO_ENCAIXE,
                                                                            new ReportUtils.ParameterReport("id_planejamento", item.getId()),
                                                                            new ReportUtils.ParameterReport("codEmpresa", Globals.getEmpresaLogada().getCodigo()))
                                                                    .view().toView();
                                                        if (printMateriais.value.get())
                                                            new ReportUtils().config()
                                                                    .addReport(ReportUtils.ReportFile.ROMANEIO_MATERIAIS_ENCAIXE,
                                                                            new ReportUtils.ParameterReport("id_planejamento", String.valueOf(item.getId())),
                                                                            new ReportUtils.ParameterReport("numero", item.getNumero().getNumero()),
                                                                            new ReportUtils.ParameterReport("codEmpresa", Globals.getEmpresaLogada().getCodigo()))
                                                                    .view().toView();
                                                        if (printConferencia.value.get())
                                                            new ReportUtils().config()
                                                                    .addReport(ReportUtils.ReportFile.PLANEJAMENTO_ENCAIXE_CONFERENCIA,
                                                                            new ReportUtils.ParameterReport("id_planejamento", item.getId()),
                                                                            new ReportUtils.ParameterReport("codEmpresa", Globals.getEmpresaLogada().getCodigo()))
                                                                    .view().toView();
                                                    } catch (JRException | SQLException e) {
                                                        e.printStackTrace();
                                                    }
                                                });
                                            }));
                                        });
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnRealCortado, btnImprimir);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(60.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdPlanejamentoEncaixe, SdPlanejamentoEncaixe>, ObservableValue<SdPlanejamentoEncaixe>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Código*/,
                FormTableColumn.create(cln -> {
                    cln.title("OF");
                    cln.width(60.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdPlanejamentoEncaixe, SdPlanejamentoEncaixe>, ObservableValue<SdPlanejamentoEncaixe>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumero()));
                    cln.format(param -> {
                        return new TableCell<SdPlanejamentoEncaixe, Of2>() {
                            @Override
                            protected void updateItem(Of2 item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(item.getNumero());
                                }
                            }
                        };
                    });
                }).build() /*OF*/,
                FormTableColumn.create(cln -> {
                    cln.title("Programador");
                    cln.width(100.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdPlanejamentoEncaixe, SdPlanejamentoEncaixe>, ObservableValue<SdPlanejamentoEncaixe>>) param -> new ReadOnlyObjectWrapper(param.getValue().getUsuario()));
                }).build() /*Programador*/,
                FormTableColumn.create(cln -> {
                    cln.title("Dt. Encaixe");
                    cln.width(120.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdPlanejamentoEncaixe, SdPlanejamentoEncaixe>, ObservableValue<SdPlanejamentoEncaixe>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDtplano()));
                    cln.format(param -> {
                        return new TableCell<SdPlanejamentoEncaixe, LocalDateTime>() {
                            @Override
                            protected void updateItem(LocalDateTime item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDateTimeFormat(item));
                                }
                            }
                        };
                    });
                }).build() /*Dt. Encaixe*/,
                FormTableColumn.create(cln -> {
                    cln.title("Referência");
                    cln.width(300.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdPlanejamentoEncaixe, SdPlanejamentoEncaixe>, ObservableValue<SdPlanejamentoEncaixe>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumero()));
                    cln.format(param -> {
                        return new TableCell<SdPlanejamentoEncaixe, Of2>() {
                            @Override
                            protected void updateItem(Of2 item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(item.getProduto().toString());
                                }
                            }
                        };
                    });
                }).build() /*Referência*/,
                FormTableColumn.create(cln -> {
                    cln.title("Qtde. OF");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdPlanejamentoEncaixe, SdPlanejamentoEncaixe>, ObservableValue<SdPlanejamentoEncaixe>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.alignment(Pos.CENTER_RIGHT);
                    cln.format(param -> {
                        return new TableCell<SdPlanejamentoEncaixe, SdPlanejamentoEncaixe>() {
                            @Override
                            protected void updateItem(SdPlanejamentoEncaixe item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toIntegerFormat(item.getNumero().getItens().stream().mapToInt(it -> it.getQtde().intValue()).sum()));
                                }
                            }
                        };
                    });
                }).build() /*Qtde. OF*/,
//                FormTableColumn.create(cln -> {
//                    cln.title("Qtde. Plano");
//                    cln.width(80.0);
//                    cln.value((Callback<TableColumn.CellDataFeatures<SdPlanejamentoEncaixe, SdPlanejamentoEncaixe>, ObservableValue<SdPlanejamentoEncaixe>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
//                    cln.alignment(Pos.CENTER_RIGHT);
//                    cln.format(param -> {
//                        return new TableCell<SdPlanejamentoEncaixe, SdPlanejamentoEncaixe>() {
//                            @Override
//                            protected void updateItem(SdPlanejamentoEncaixe item, boolean empty) {
//                                super.updateItem(item, empty);
//                                setText(null);
//                                if (item != null && !empty) {
//                                    setText(StringUtils.toIntegerFormat(item.getRiscos().stream().filter(it -> it.getTiporisco().getCodigo() == 1).mapToInt(it -> it.getCores().stream().mapToInt(SdCorRisco::getTotalRealizado).sum()).sum()));
//                                }
//                            }
//                        };
//                    });
//                }).build() /*Qtde. Plano*/,
                FormTableColumn.create(cln -> {
                    cln.title("RC");
                    cln.width(30);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdPlanejamentoEncaixe, SdPlanejamentoEncaixe>, ObservableValue<SdPlanejamentoEncaixe>>) param -> new ReadOnlyObjectWrapper(param.getValue().isRealcortado()));
                    cln.format(param -> {
                        return new TableCell<SdPlanejamentoEncaixe, Boolean>() {
                            @Override
                            protected void updateItem(Boolean item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(item ? ImageUtils.getIcon(ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._16) : ImageUtils.getIcon(ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                                }
                            }
                        };
                    });
                }).build() /*RC*/
        );
        table.tableview().setOnMouseClicked(evt -> {
            if (evt.getClickCount() > 1) {
                SdPlanejamentoEncaixe selectPlano = (SdPlanejamentoEncaixe) table.selectedItem();
                
                if (selectPlano != null) {
                    cleanFieldsCargaOf();
                    abrirPlanejamento(selectPlano);
                }
            }
        });
    });
    // </editor-fold>
    
    public PlanejamentoEnfestoView() {
        super("Planejamento de Enfesto", ImageUtils.getImage(ImageUtils.Icon.ENFESTO));
        init();
    }
    
    private void init() {
        initListagem();
        initPlanejamento();
    }
    
    private void initPlanejamento() {
        tabPlanejamento.getChildren().add(FormBox.create(layout -> {
            layout.vertical();
            layout.expanded();
            layout.add(FormBox.create(boxPrincipal -> {
                boxPrincipal.horizontal();
                boxPrincipal.expanded();
                boxPrincipal.add(FormBox.create(coluna1 -> {
                    coluna1.vertical();
                    coluna1.width(650.0);
                    coluna1.add(FormBox.create(dadosCargaOf -> {
                        dadosCargaOf.horizontal();
                        dadosCargaOf.alignment(Pos.BOTTOM_LEFT);
                        dadosCargaOf.add(numeroOfField.build());
                        dadosCargaOf.add(FormButton.create(btnCarregarOf -> {
                            btnCarregarOf.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            btnCarregarOf.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._32));
                            btnCarregarOf.addStyle("lg").addStyle("success");
                            btnCarregarOf.disable.bind(emEdicao);
                            btnCarregarOf.setAction(event -> carregarOf(numeroOfField.value.get()));
                        }));
                        dadosCargaOf.add(periodoOfField.build());
                        dadosCargaOf.add(FormBox.create(layout1 -> {
                            layout1.horizontal();
                            layout1.expanded();
                            layout1.alignment(Pos.TOP_RIGHT);
                            layout1.add(FormBox.create(boxDadosEncaixe -> {
                                boxDadosEncaixe.horizontal();
                                boxDadosEncaixe.add(dhEncaixeField.build());
                                boxDadosEncaixe.add(usuarioEncaixeField.build());
                                boxDadosEncaixe.add(FormBox.create(boxSelects -> {
                                    boxSelects.vertical();
                                    boxSelects.add(encaixeComGabaritoField.build());
                                    boxSelects.add(tecidoTubularField.build());
                                }));
                            }));
                        }));
                    }));
                    coluna1.add(FormBox.create(dadosOfProduto -> {
                        dadosOfProduto.vertical();
                        dadosOfProduto.height(250.0);
                        dadosOfProduto.title("Dados OF/Produto");
                        dadosOfProduto.add(FormBox.create(dadosProduto -> {
                                    dadosProduto.horizontal();
                                    dadosProduto.add(referenciaFild.build(),
                                            FormBox.create(fieldsProduto -> {
                                                fieldsProduto.vertical();
                                                fieldsProduto.add(
                                                        descricaoProdutoField.build(),
                                                        FormBox.create(fieldsInfoProduto -> {
                                                            fieldsInfoProduto.horizontal();
                                                            fieldsInfoProduto.add(
                                                                    linhaProdutoField.build(),
                                                                    familiaProdutoField.build(),
                                                                    marcaProdutoField.build(),
                                                                    gradeProdutoField.build());
                                                        }));
                                            }));
                                }),
                                observacaoOfField.build());
                    }));
                    coluna1.add(tblGradeOf.build());
                    coluna1.add(FormBox.create(boxBotoes -> {
                        boxBotoes.horizontal();
                        boxBotoes.add(FormButton.create(btnCarregarMateriais -> {
                            btnCarregarMateriais.title("Materias/Aplicações");
                            btnCarregarMateriais.icon(ImageUtils.getIcon(ImageUtils.Icon.MATERIAIS, ImageUtils.IconSize._24));
                            btnCarregarMateriais.addStyle("warning");
                            btnCarregarMateriais.disable.bind(emEdicao.not());
                            btnCarregarMateriais.setAction(event -> exibirMateriaisAplicacaoOf());
                        }));
                        boxBotoes.add(FormButton.create(btnExplodirGrade -> {
                            btnExplodirGrade.title("Infos Grade");
                            btnExplodirGrade.setVisible(false);
                            btnExplodirGrade.icon(ImageUtils.getIcon(ImageUtils.Icon.FRAGMENT, ImageUtils.IconSize._24));
                            btnExplodirGrade.addStyle("info");
                            btnExplodirGrade.disable.bind(emEdicao.not());
                            btnExplodirGrade.setAction(event -> explodirInfosGrade());
                        }));
                        boxBotoes.add(FormBox.create(boxCancelarPlano -> {
                            boxCancelarPlano.horizontal();
                            boxCancelarPlano.expanded();
                            boxCancelarPlano.alignment(Pos.TOP_RIGHT);
                            boxCancelarPlano.add(FormButton.create(selectCores -> {
                                selectCores.title("Fechar Planejamento");
                                selectCores.addStyle("info");
                                selectCores.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
                                selectCores.setAction(evt -> {
                                    cleanFieldsCargaOf();
                                    numeroOfField.requestFocus();
                                });
                            }));
                        }));
                    }));
                }));
                boxPrincipal.add(new Separator(Orientation.VERTICAL));
                boxPrincipal.add(FormBox.create(coluna2 -> {
                    coluna2.vertical();
                    coluna2.expanded();
                    coluna2.add(FormBox.create(boxHeaderRiscos -> {
                        boxHeaderRiscos.horizontal();
                        boxHeaderRiscos.add(tipoRiscoField.build());
                        boxHeaderRiscos.add(FormButton.create(btnCriarRisco -> {
                            btnCriarRisco.title("Criar Risco");
                            btnCriarRisco.icon(ImageUtils.getIcon(ImageUtils.Icon.ENCAIXE, ImageUtils.IconSize._32));
                            btnCriarRisco.addStyle("lg").addStyle("primary");
                            btnCriarRisco.disable.bind(emEdicao.not());
                            btnCriarRisco.setAction(evtCriarRisco -> criarRisco());
                        }));
                    }));
                    coluna2.add(tabRiscos);
                    coluna2.add(FormBox.create(boxBotoes -> {
                        boxBotoes.horizontal();
                        boxBotoes.alignment(Pos.BOTTOM_RIGHT);
                        boxBotoes.add(FormButton.create(btnEditar -> {
                            btnEditar.title("Modo Edição");
                            btnEditar.addStyle("warning");
                            btnEditar.icon(ImageUtils.getIcon(ImageUtils.Icon.EDIT, ImageUtils.IconSize._24));
                            btnEditar.setAction(evt -> {
                                //criandoRisco.set(true);
                                emEdicao.set(true);
                                
                                SysLogger.addSysDelizLog("Planejamento Encaixe", TipoAcao.EDITAR, planejamentoAberto.getNumero().getNumero(), "Iniciando edição do planejamento ID " + planejamentoAberto.getId());
                            });
                        }));
                        boxBotoes.add(FormButton.create(btnCancelarPlano -> {
                            btnCancelarPlano.title("Excluir Plano");
                            btnCancelarPlano.disable.bind(emEdicao.not());
                            btnCancelarPlano.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
                            btnCancelarPlano.addStyle("danger");
                            btnCancelarPlano.setAction(evtCancelarPlano -> cancelarPlano());
                        }));
                        boxBotoes.add(FormButton.create(btnFinalizarPlano -> {
                            btnFinalizarPlano.title("Finalizar Plano");
                            btnFinalizarPlano.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                            btnFinalizarPlano.addStyle("success");
                            btnFinalizarPlano.disable.bind(emEdicao.not());
                            btnFinalizarPlano.setAction(evt -> finalizarPlano());
                        }));
                    }));
                }));
            }));
        }));
    }
    
    private void initListagem() {
        tabListagem.getChildren().add(FormBox.create(listagem -> {
            listagem.vertical();
            listagem.expanded();
            listagem.add(FormBox.create(loFilter -> {
                loFilter.horizontal();
                loFilter.add(FormTitledPane.create(filter -> {
                    filter.filter();
                    filter.add(FormBox.create(formFilter -> {
                        formFilter.vertical();
                        formFilter.add(FormBox.create(box -> {
                            box.horizontal();
                            box.add(findPeriodo.build(), findNumero.build());
                        }));
                        formFilter.add(FormBox.create(box -> {
                            box.horizontal();
                            box.add(findProgramador.build(), findProduto.build());
                        }));
                    }));
                    filter.find.setOnAction(evt -> {
                        new RunAsyncWithOverlay(this).exec(task -> {
                            JPAUtils.clearEntitys(planejamentosDaOf);
                            getPlanejamentosOf(findNumero.value.get(),
                                    findProgramador.value.get(),
                                    findProduto.value.get() == null ? null : findProduto.value.get().getCodigo(),
                                    LocalDateTime.of(findPeriodo.valueBegin.get(), LocalTime.MIN),
                                    LocalDateTime.of(findPeriodo.valueEnd.get(), LocalTime.MAX));
                            return ReturnAsync.OK.value;
                        }).addTaskEndNotification(taskReturn -> {
                            if (taskReturn.equals(ReturnAsync.OK.value)) {
                                beanPlanejamentosOf.set(FXCollections.observableList(planejamentosDaOf));
                            }
                        });
                    });
                    filter.clean.setOnAction(evt -> {
                        findNumero.clear();
                        findProgramador.clear();
                        findPeriodo.valueBegin.set(LocalDate.of(1900, 01, 01));
                        findPeriodo.valueEnd.set(LocalDate.of(2050, 12, 31));
                        findProduto.clear();
                    });
                }));
            }));
            listagem.add(tableFilters.build());
            listagem.add(FormBox.create(boxButtons -> {
                boxButtons.horizontal();
                boxButtons.add(
                        FormButton.create(btnAbrir -> {
                            btnAbrir.title("Abrir Planejamento");
                            btnAbrir.icon(ImageUtils.getIcon(ImageUtils.Icon.ABRIR, ImageUtils.IconSize._24));
                            btnAbrir.addStyle("success");
                            btnAbrir.setAction(evt -> {
                                SdPlanejamentoEncaixe selectPlano = tableFilters.selectedItem();
                                
                                if (selectPlano != null) {
                                    cleanFieldsCargaOf();
                                    abrirPlanejamento(selectPlano);
                                }
                            });
                        }),
                        FormButton.create(btnExcluir -> {
                            btnExcluir.title("Excluir");
                            btnExcluir.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
                            btnExcluir.addStyle("danger");
                            btnExcluir.setAction(evt -> {
                                SdPlanejamentoEncaixe selectPlano = tableFilters.selectedItem();
                                if (selectPlano != null) {
                                    if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                                        message.message("Deseja realmente excluir o planejamento selecionado?");
                                        message.showAndWait();
                                    }).value.get())) {
                                        selectPlano.getRiscos().forEach(risco -> {
                                            risco.getCores().forEach(cor -> {
                                                cor.getGrade().forEach(it -> new FluentDao().delete(it));
                                                cor.getMateriais().forEach(it -> new FluentDao().delete(it));
                                                new FluentDao().delete(cor);
                                            });
                                            new FluentDao().delete(risco);
                                        });
                                        new FluentDao().delete(selectPlano);
                                        
                                        MessageBox.create(message -> {
                                            message.message("Planejamento de encaixe excluido com sucesso!");
                                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                                            message.position(Pos.TOP_RIGHT);
                                            message.notification();
                                        });
                                    }
                                }
                            });
                        })
                );
            }));
        }));
    }
    
    private void abrirPlanejamento(SdPlanejamentoEncaixe selectPlano) {
        String numeroOf = selectPlano.getNumero().getNumero();
        
        // clear JPA
        // carga da OF digitada
        beanOfCarregada.set(getOf(numeroOf));
        if (beanOfCarregada.get() != null) {
            new RunAsyncWithOverlay(this).exec(task -> {
                getMateriais(beanOfCarregada.get().getNumero());
                getAplicacoes(beanOfCarregada.get().getNumero());
                getMaterialAplicacoes(beanOfCarregada.get().getNumero());
                getFaixaProduto(beanOfCarregada.get().getProduto().getFaixa());
                getPlanejamentosOf(beanOfCarregada.get().getNumero());
                return ReturnAsync.OK.value;
            }).addTaskEndNotification(taskReturn -> {
                if (taskReturn.equals(ReturnAsync.OK.value)) {
                    // inits de beans da OF e grade
                    beanGradeOf.clear();
                    beanGradeOf.addAll(FXCollections.observableList(beanOfCarregada.get().getItens()));
                    configBindFieldsAndTables();
                    
                    // inits de beans dos materiais e aplicacoes
                    beanMateriaisOf.addAll(FXCollections.observableList(super.materiasOf));
                    beanAplicacoesOf.addAll(FXCollections.observableList(super.materialAplicacoesOf));
                    
                    // ativando mode de edição da programação
                    emEdicao.set(false);
                    
                    // limpando o campo de gabarito
                    if (planejamentoAberto != null) {
                        dhEncaixeField.value.unbindBidirectional(planejamentoAberto.dtplanoProperty());
                        usuarioEncaixeField.value.unbindBidirectional(planejamentoAberto.usuarioProperty());
                        encaixeComGabaritoField.value.unbindBidirectional(planejamentoAberto.gabaritoProperty());
                        tecidoTubularField.value.unbindBidirectional(planejamentoAberto.tubularProperty());
                    }
                    
                    planejamentoAberto = selectPlano;
                    criarRisco(planejamentoAberto);
                }
            });
        } else {
            MessageBox.create(message -> {
                message.message("Não foi encontrado uma OF com este número");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showAndWait();
            });
        }
        
        // mudando para a aba de planejamento
        tabs.getSelectionModel().select(0);
    }
    
    private void atualizarSaldosPlanejamento(SdRiscoPlanejamentoEncaixe risco) {
        List<SdRiscoPlanejamentoEncaixe> riscosParaAtualizar = planejamentoAberto.getRiscos().stream()
                .filter(it -> it.getAplicacaoft().getCodigo().equals(risco.getAplicacaoft().getCodigo()) && it.getSeq() >= risco.getSeq())
                .collect(Collectors.toList());
        
        riscosParaAtualizar.sort(Comparator.comparingInt(SdRiscoPlanejamentoEncaixe::getSeq));
        riscosParaAtualizar.forEach(it -> {
            List<SdRiscoPlanejamentoEncaixe> riscosAnteriores = planejamentoAberto.getRiscos().stream()
                    .filter(it2 -> it2.getAplicacaoft().getCodigo().equals(it.getAplicacaoft().getCodigo()) && it2.getSeq() <= it.getSeq())
                    .collect(Collectors.toList());
            
            it.getCores().forEach(cor -> {
                cor.getGrade().forEach(grade -> {
                    Integer qtdeOf = it.getTiporisco().getCodigo() == 1
                            ? planejamentoAberto.getNumero().getItens().stream()
                            .filter(it2 -> checkRiscoAgrupado(cor.getCor())
                                    ? Arrays.asList(cor.getCor().replace("'", "").split(",")).contains(it2.getId().getCor().getCor())
                                    : cor.getCor().equals(it2.getId().getCor().getCor()))
                            .mapToInt(it2 -> it2.getTams().stream()
                                    .filter(it3 -> it3.getId().getTam().equals(grade.getTam()))
                                    .mapToInt(it3 -> it3.getQtdeof().intValue())
                                    .sum())
                            .sum()
                            : (it.getTiporisco().getCodigo() == 3 || it.getTiporisco().getCodigo() == 5)
                            ? cor.getRiscoCorpo().stream().
                            filter(it2 -> checkRiscoAgrupado(it2.getCor()) ? true : cor.isGrupoCor() ? it2.getCor().equals(cor.getCor()) :
                                    getCorIds(cor.getCor()).stream().map(rCor -> String.valueOf(rCor.getId())).distinct().collect(Collectors.toList()).contains(String.valueOf(it2.getId())))
                            .mapToInt(it2 -> it2.getGrade().stream()
                                    .filter(it3 -> it3.getTam().equals(grade.getTam()))
                                    .mapToInt(SdGradeRiscoCor::getRealizado)
                                    .sum())
                            .sum()
                            : planejamentoAberto.getRiscos().stream()
                            .filter(it2 -> it2.getTiporisco().getCodigo() == 1)
                            .mapToInt(it2 -> it2.getCores().stream()
                                    .filter(it3 -> checkRiscoAgrupado(cor.getCor())
                                            ? !checkRiscoAgrupado(it3.getCor()) ?
                                                Arrays.asList(cor.getCor().replace("'", "").split(",")).contains(it3.getCor()) :
                                                Arrays.asList(cor.getCor().replace("'", "").split(",")).stream().anyMatch(corAgrupada -> Arrays.asList(it3.getCor().replace("'", "").split(",")).contains(corAgrupada))
                                            : cor.getCor().equals(it3.getCor()))
                                    .mapToInt(it3 -> it3.getGrade().stream()
                                            .filter(it4 -> it4.getTam().equals(grade.getTam()))
                                            .mapToInt(it4 -> it4.getRealizado())
                                            .sum())
                                    .sum())
                            .sum();
                    Integer qtdeRiscada = Arrays.asList(1, 2, 4).contains(it.getTiporisco().getCodigo())
                            ? riscosAnteriores.stream()
                            .filter(it2 -> it2.getAplicacaoft().getCodigo() == it.getAplicacaoft().getCodigo())
                            .mapToInt(it2 -> it2.getCores().stream()
                                    .filter(it3 -> checkRiscoAgrupado(cor.getCor())
                                            ? Arrays.asList(cor.getCor().replace("'", "").split(",")).stream().anyMatch(corAgrupada -> Arrays.asList(it3.getCor().replace("'", "").split(",")).contains(corAgrupada))
                                            : cor.getCor().equals(it3.getCor()))
                                    .mapToInt(it3 -> it3.getGrade().stream()
                                            .filter(it4 -> it4.getTam().equals(grade.getTam()))
                                            .mapToInt(SdGradeRiscoCor::getRealizado)
                                            .sum())
                                    .sum())
                            .sum()
                            : riscosAnteriores.stream()
                            .filter(it2 -> it2.getAplicacaoft().getCodigo() == it.getAplicacaoft().getCodigo())
                            .mapToInt(it2 -> it2.getCores().stream()
                                    .filter(it3 -> checkRiscoAgrupado(it3.getCor()) ? it3.getRiscoCorpo().stream().anyMatch(it4 -> cor.getRiscoCorpo().stream().anyMatch(it5 -> it5.getId() == it4.getId()))
                                            : it3.getCor().equals(cor.getCor()))
                                    .mapToInt(it3 -> it3.getGrade().stream()
                                            .filter(it4 -> it4.getTam().equals(grade.getTam()))
                                            .mapToInt(SdGradeRiscoCor::getRealizado)
                                            .sum())
                                    .sum())
                            .sum();
                    
                    grade.setSaldoOf(qtdeOf - qtdeRiscada);
                });
            });
        });
        
        planejamentoAberto.getRiscos().stream().filter(it -> it.getTiporisco().getCodigo() == 1).forEach(riscos -> {
            riscos.getCores().forEach(cor -> {
                cor.getGrade().forEach(grade -> {
                    grade.setSomatorioCor(
                            planejamentoAberto.getRiscos().stream()
                                    .filter(it -> it.getAplicacaoft().getCodigo().equals(riscos.getAplicacaoft().getCodigo()))
                                    .mapToInt(it -> it.getCores().stream()
                                            .filter(it2 -> checkRiscoAgrupado(it2.getCor()) ? true : !cor.isGrupoCor() ? it2.getCor().equals(cor.getCor()) :
                                                    getCorIds(cor.getCor()).stream().map(rCor -> String.valueOf(rCor.getId())).distinct().collect(Collectors.toList()).contains(String.valueOf(it2.getId())))
                                            .mapToInt(it2 -> it2.getGrade().stream()
                                                    .filter(it3 -> it3.getTam().equals(grade.getTam()))
                                                    .mapToInt(SdGradeRiscoCor::getRealizado)
                                                    .sum())
                                            .sum())
                                    .sum()
                    );
                });
                cor.setTotalSomCor(cor.getGrade().stream().mapToInt(SdGradeRiscoCor::getSomatorioCor).sum());
            });
        });
    }
    
    /**
     * function para carregar a OF para um novo planejamento
     *
     * @param numeroOf
     */
    private void carregarOf(String numeroOf) {
        // clear JPA
        if (beanOfCarregada.get() != null)
            JPAUtils.clearEntity(beanOfCarregada.get());
        // carga da OF digitada
        try {
            beanOfCarregada.set(getOf(numeroOf));
            if (beanOfCarregada.get() != null) {
                new RunAsyncWithOverlay(this).exec(task -> {
                    getMateriais(beanOfCarregada.get().getNumero());
                    getAplicacoes(beanOfCarregada.get().getNumero());
                    getMaterialAplicacoes(beanOfCarregada.get().getNumero());
                    getFaixaProduto(beanOfCarregada.get().getProduto().getFaixa());
                    getPlanejamentosOf(beanOfCarregada.get().getNumero());
                    return ReturnAsync.OK.value;
                }).addTaskEndNotification(taskReturn -> {
                    if (taskReturn.equals(ReturnAsync.OK.value)) {
                        // inits de beans da OF e grade
                        beanGradeOf.clear();
                        beanGradeOf.addAll(FXCollections.observableList(beanOfCarregada.get().getItens()));
                        configBindFieldsAndTables();

                        // inits de beans dos materiais e aplicacoes
                        beanMateriaisOf.addAll(FXCollections.observableList(super.materiasOf));
                        beanAplicacoesOf.addAll(FXCollections.observableList(super.materialAplicacoesOf));

                        // ativando mode de edição da programação
                        emEdicao.set(true);

                        isPt.set(aplicacoesOf.stream()
                                .filter(aplic -> aplic.getAplicacao().getCodigo().equals("717")
                                        && aplic.getAplicacaoft().getCodigo().equals("520"))
                                .count() > 0);

                        // limpando o campo de gabarito
                        if (planejamentoAberto != null) {
                            dhEncaixeField.value.unbindBidirectional(planejamentoAberto.dtplanoProperty());
                            usuarioEncaixeField.value.unbindBidirectional(planejamentoAberto.usuarioProperty());
                            encaixeComGabaritoField.value.unbindBidirectional(planejamentoAberto.gabaritoProperty());
                            tecidoTubularField.value.unbindBidirectional(planejamentoAberto.tubularProperty());
                        }

                        planejamentoAberto = null;

                        // verificação se já existe um planejamento com a OF consultada
                        if (planejamentosDaOf.size() > 0) {
                            new Fragment().show(fragment -> {
                                fragment.title("Seleção de Planejamentos");
                                fragment.size(400.0, 350.0);

                                FormTableView<SdPlanejamentoEncaixe> tblPlanejamentos = FormTableView.create(SdPlanejamentoEncaixe.class, table -> {
                                    table.title("Planejamentos");
                                    table.setItems(FXCollections.observableList(planejamentosDaOf));
                                    table.expanded();
                                    table.columns(
                                            FormTableColumn.create(cln -> {
                                                cln.title("Código");
                                                cln.width(40.0);
                                                cln.value((Callback<TableColumn.CellDataFeatures<SdPlanejamentoEncaixe, SdPlanejamentoEncaixe>, ObservableValue<SdPlanejamentoEncaixe>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId()));
                                            }).build() /*Código*/,
                                            FormTableColumn.create(cln -> {
                                                cln.title("Programador");
                                                cln.width(90.0);
                                                cln.value((Callback<TableColumn.CellDataFeatures<SdPlanejamentoEncaixe, SdPlanejamentoEncaixe>, ObservableValue<SdPlanejamentoEncaixe>>) param -> new ReadOnlyObjectWrapper(param.getValue().getUsuario()));
                                            }).build() /*Programador*/,
                                            FormTableColumn.create(cln -> {
                                                cln.title("Data");
                                                cln.width(100.0);
                                                cln.value((Callback<TableColumn.CellDataFeatures<SdPlanejamentoEncaixe, SdPlanejamentoEncaixe>, ObservableValue<SdPlanejamentoEncaixe>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDtplano()));
                                                cln.format(param -> {
                                                    return new TableCell<SdPlanejamentoEncaixe, LocalDateTime>() {
                                                        @Override
                                                        protected void updateItem(LocalDateTime item, boolean empty) {
                                                            super.updateItem(item, empty);
                                                            setText(null);
                                                            if (item != null && !empty) {
                                                                setText(StringUtils.toDateTimeFormat(item));
                                                            }
                                                        }
                                                    };
                                                });
                                            }).build() /*Data*/,
                                            FormTableColumn.create(cln -> {
                                                cln.title("Qtde. OF");
                                                cln.width(80.0);
                                                cln.value((Callback<TableColumn.CellDataFeatures<SdPlanejamentoEncaixe, SdPlanejamentoEncaixe>, ObservableValue<SdPlanejamentoEncaixe>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                                cln.alignment(Pos.CENTER_RIGHT);
                                                cln.format(param -> {
                                                    return new TableCell<SdPlanejamentoEncaixe, SdPlanejamentoEncaixe>() {
                                                        @Override
                                                        protected void updateItem(SdPlanejamentoEncaixe item, boolean empty) {
                                                            super.updateItem(item, empty);
                                                            setText(null);
                                                            if (item != null && !empty) {
                                                                setText(StringUtils.toIntegerFormat(item.getNumero().getItens().stream().mapToInt(it -> it.getQtde().intValue()).sum()));
                                                            }
                                                        }
                                                    };
                                                });
                                            }).build() /*Qtde. OF*/,
                                            FormTableColumn.create(cln -> {
                                                cln.title("Qtde Plano");
                                                cln.width(80.0);
                                                cln.value((Callback<TableColumn.CellDataFeatures<SdPlanejamentoEncaixe, SdPlanejamentoEncaixe>, ObservableValue<SdPlanejamentoEncaixe>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                                cln.alignment(Pos.CENTER_RIGHT);
                                                cln.format(param -> {
                                                    return new TableCell<SdPlanejamentoEncaixe, SdPlanejamentoEncaixe>() {
                                                        @Override
                                                        protected void updateItem(SdPlanejamentoEncaixe item, boolean empty) {
                                                            super.updateItem(item, empty);
                                                            setText(null);
                                                            if (item != null && !empty) {
                                                                setText(StringUtils.toIntegerFormat(item.getRiscos().stream().filter(it -> it.getTiporisco().getCodigo() == 1).mapToInt(it -> it.getCores().stream().mapToInt(it2 -> it2.getGrade().stream().mapToInt(SdGradeRiscoCor::getRealizado).sum()).sum()).sum()));
                                                            }
                                                        }
                                                    };
                                                });
                                            }).build() /*Qtde Plano*/);
                                });
                                fragment.box.getChildren().add(FormLabel.create(label -> label.value.set("Existem planejamentos criados para essa OF:")).build());
                                fragment.box.getChildren().add(tblPlanejamentos.build());
                                fragment.box.getChildren().add(FormLabel.create(label -> label.value.set("Deseja alterar o planejamento selecionado?")).build());
                                fragment.buttonsBox.getChildren().add(FormButton.create(btn -> {
                                    btn.title("Editar");
                                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EDIT, ImageUtils.IconSize._24));
                                    btn.addStyle("warning");
                                    btn.setAction(evt -> {
                                        if (tblPlanejamentos.selectedRegisters().isEmpty()) {
                                            MessageBox.create(message -> {
                                                message.message("É necessário selecionar o planejamento na lista, antes de editar.");
                                                message.type(MessageBox.TypeMessageBox.ALERT);
                                                message.showAndWait();
                                            });
                                            return;
                                        }

                                        planejamentoAberto = tblPlanejamentos.selectedItem();
                                        dhEncaixeField.value.bindBidirectional(planejamentoAberto.dtplanoProperty());
                                        usuarioEncaixeField.value.bindBidirectional(planejamentoAberto.usuarioProperty());
                                        encaixeComGabaritoField.value.bindBidirectional(planejamentoAberto.gabaritoProperty());
                                        tecidoTubularField.value.bindBidirectional(planejamentoAberto.tubularProperty());
                                        fragment.close();
                                    });
                                }));
                            });
                        }

                        if (planejamentoAberto == null) {
                            // criando objeto do planejamento para of
                            planejamentoAberto = new SdPlanejamentoEncaixe();
                            planejamentoAberto.setNumero(ofCarregada);
                            dhEncaixeField.value.bindBidirectional(planejamentoAberto.dtplanoProperty());
                            usuarioEncaixeField.value.bindBidirectional(planejamentoAberto.usuarioProperty());
                            encaixeComGabaritoField.value.bindBidirectional(planejamentoAberto.gabaritoProperty());
                            tecidoTubularField.value.bindBidirectional(planejamentoAberto.tubularProperty());
                            SysLogger.addSysDelizLog("Planejamento Encaixe", TipoAcao.CADASTRAR, planejamentoAberto.getNumero().getNumero(), "Iniciando planejamento de encaixe");
                        } else {
                            emEdicao.set(false);
                            criarRisco(planejamentoAberto);
                        }
                    }
                });
            } else {
                MessageBox.create(message -> {
                    message.message("Não foi encontrado uma OF com este número");
                    message.type(MessageBox.TypeMessageBox.ALERT);
                    message.showAndWait();
                });
            }
        }catch (EntityNotFoundException noExc) {
            MessageBox.create(message -> {
                message.message("Produto não encontrado, verifique o cadastro do produto.\n" +
                        noExc.getMessage().replaceAll(".*with ","").replace("id", "Código"));
                message.type(MessageBox.TypeMessageBox.NO_ENTRY);
                message.showAndWait();
            });
        }
    }
    
    /**
     * function bind centralizado de dados
     */
    private void configBindFieldsAndTables() {
        calcTotalsGrade();
        bindDadosOfProduto();
        
        // cria as colunas da grade da OF
        if (tblGradeOf.tableview().getColumns().size() > 2)
            tblGradeOf.tableview().getColumns().remove(2, tblGradeOf.tableview().getColumns().size() - 1);
        for (FaixaItem faixa : super.faixaProduto) {
            tblGradeOf.addColumn(FormTableColumn.create(cln -> {
                cln.title(faixa.getFaixaItemId().getTamanho());
                cln.width(40.0);
                cln.alignment(FormTableColumn.Alignment.CENTER);
                cln.value((Callback<TableColumn.CellDataFeatures<VSdCorOf, VSdCorOf>, ObservableValue<VSdCorOf>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                cln.format(param -> {
                    return new TableCell<VSdCorOf, VSdCorOf>() {
                        @Override
                        protected void updateItem(VSdCorOf item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                Optional<VSdCortamOf> tam = item.getTams().stream().filter(itemCor -> itemCor.getId().getTam().equals(faixa.getFaixaItemId().getTamanho())).findFirst();
                                //VSdCortamOf tam = item.getTams().stream().filter(itemCor -> itemCor.getId().getTam().equals(faixa.getFaixaItemId().getTamanho())).findFirst().get();
                                setText(StringUtils.toIntegerFormat(tam.isPresent() ? tam.get().getQtdeof().intValue() : 0));
                                //setGraphic(FormBox.create(dadosTamanho -> {
                                //    dadosTamanho.vertical();
                                //    dadosTamanho.add(
                                //            FormFieldText.create(field -> {
                                //                field.withoutTitle();
                                //                field.alignment(Pos.CENTER);
                                //                field.editable(false);
                                //                field.addStyle("xs");
                                //                field.value.set(StringUtils.toIntegerFormat(tam.getQtdeof().intValue()));
                                //            }).build()
                                //    );
                                //}));
                            }
                        }
                    };
                });
            }));
        }
        tblGradeOf.addColumn(FormTableColumn.create(cln -> {
            cln.title("Total");
            cln.width(40.0);
            cln.alignment(FormTableColumn.Alignment.CENTER);
            cln.value((Callback<TableColumn.CellDataFeatures<VSdCorOf, VSdCorOf>, ObservableValue<VSdCorOf>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
            cln.format(param -> {
                return new TableCell<VSdCorOf, BigDecimal>() {
                    @Override
                    protected void updateItem(BigDecimal item, boolean empty) {
                        super.updateItem(item, empty);
                        setText(null);
                        setGraphic(null);
                        if (item != null && !empty) {
                            setText(StringUtils.toIntegerFormat(item.intValue()));
                        }
                    }
                };
            });
        }));
        
        criaSaldoGrade();
    }
    
    /**
     * function para criar array de saldo da grade
     */
    private void criaSaldoGrade() {
        saldoGrade.addAll(ofCarregada.getItens());
    }
    
    /**
     * function para calcular o total de uma of e colocar na grade
     */
    private void calcTotalsGrade() {
        VSdCorOf totalGrade = new VSdCorOf();
        VSdCorOfPk pkTotalGrade = new VSdCorOfPk();
        Cor totalCor = new Cor();
        totalCor.setCor("Total");
        pkTotalGrade.setCor(totalCor);
        totalGrade.setId(pkTotalGrade);
        totalGrade.setQtde(new BigDecimal(beanOfCarregada.get().getItens().stream().mapToInt(cor -> cor.getTams().stream().mapToInt(tam -> tam.getQtdeof().intValue()).sum()).sum()));
        List<VSdCortamOf> totalTams = new ArrayList<>();
        super.faixaProduto.forEach(faixa -> {
            VSdCortamOfPk pkCortam = new VSdCortamOfPk();
            pkCortam.setTam(faixa.getFaixaItemId().getTamanho());
            VSdCortamOf totalCortam = new VSdCortamOf();
            totalCortam.setId(pkCortam);
            totalCortam.setQtdeof(new BigDecimal(beanOfCarregada.get().getItens().stream().mapToInt(cor -> cor.getTams().stream().filter(tam -> tam.getId().getTam().equals(faixa.getFaixaItemId().getTamanho())).mapToInt(tam -> tam.getQtdeof().intValue()).sum()).sum()));
            totalCortam.setVendpend(new BigDecimal(beanOfCarregada.get().getItens().stream().mapToInt(cor -> cor.getTams().stream().filter(tam -> tam.getId().getTam().equals(faixa.getFaixaItemId().getTamanho())).mapToInt(tam -> tam.getVendpend().intValue()).sum()).sum()));
            totalCortam.setEmprod(new BigDecimal(beanOfCarregada.get().getItens().stream().mapToInt(cor -> cor.getTams().stream().filter(tam -> tam.getId().getTam().equals(faixa.getFaixaItemId().getTamanho())).mapToInt(tam -> tam.getEmprod().intValue()).sum()).sum()));
            totalCortam.setPlanejado(new BigDecimal(beanOfCarregada.get().getItens().stream().mapToInt(cor -> cor.getTams().stream().filter(tam -> tam.getId().getTam().equals(faixa.getFaixaItemId().getTamanho())).mapToInt(tam -> tam.getPlanejado().intValue()).sum()).sum()));
            totalTams.add(totalCortam);
        });
        totalGrade.setTams(totalTams);
        beanGradeOf.add(totalGrade);
    }
    
    /**
     * function com o bind dos dados da OF e produto
     */
    private void bindDadosOfProduto() {
        String obsProduto = "";
        String obsOf = beanOfCarregada.get().getObservacao() != null ? beanOfCarregada.get().getObservacao() : " ";
        
        if (beanOfCarregada.get().getProduto().getObs() != null) {
            RTFEditorKit rtfParser = new RTFEditorKit();
            Document document = rtfParser.createDefaultDocument();
            try {
                rtfParser.read(new ByteArrayInputStream(beanOfCarregada.get().getProduto().getObs().getBytes()), document, 0);
                obsProduto = document.getText(0, document.getLength());
            } catch (IOException e) {
                e.printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(e);
                    message.showAndWait();
                });
            } catch (BadLocationException e) {
                e.printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(e);
                    message.showAndWait();
                });
            }
        }
        
        referenciaFild.value.set(beanOfCarregada.get().getProduto().getCodigo());
        descricaoProdutoField.value.set(beanOfCarregada.get().getProduto().getDescricao());
        linhaProdutoField.value.set(beanOfCarregada.get().getProduto().getLinha());
        gradeProdutoField.value.set(beanOfCarregada.get().getProduto().getGrade());
        familiaProdutoField.value.set(beanOfCarregada.get().getProduto().getFamilia());
        marcaProdutoField.value.set(beanOfCarregada.get().getProduto().getMarca());
        observacaoOfField.value.set(obsOf.concat("\n").concat(obsProduto));
        periodoOfField.value.set(beanOfCarregada.get().getPeriodo());
        periodoOfField.addStyle(beanOfCarregada.get().getPeriodo().equals("M") ? "mostruario" : "");
    }
    
    /**
     * function que verifica se um risco está agrupado ou não
     *
     * @param value
     * @return Boolean
     */
    private boolean checkRiscoAgrupado(String value) {
        return value.split(",").length > 1;
    }
    
    private List<SdCorRisco> getCorIds(String ids) {
        List<SdCorRisco> riscosEncontrados = new ArrayList<>();
        
        for (String s : ids.split(",")) {
            planejamentoAberto.getRiscos().stream().filter(it -> it.getTiporisco().getCodigo() == 1).forEach(risco -> risco.getCores().forEach(cor -> {
                if (cor.getId() == Integer.parseInt(s)) {
                    riscosEncontrados.add(cor);
                }
            }));
        }
        
        return riscosEncontrados;
    }
    
    /**
     * function para criar um novo risco
     */
    private void criarRisco() {
        SdTipoRisco tipoRiscoSelecionado = tipoRiscoField.value.get();
        // validação tipo de risco com as aplicações da OF
        if (aplicacoesOf.stream().filter(aplicacao -> aplicacao.getAplicacao().getCodigo().equals(tipoRiscoSelecionado.getAplicacao().getCodigo())).count() == 0) {
            MessageBox.create(message -> {
                message.message("Não existe aplicação nesta OF para este tipo de risco.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }
//        if (tipoRiscoSelecionado.getCodigo() != 1) {
//            if (planejamentoAberto.getNumero().getItens().stream().mapToInt(it -> it.getQtde().intValue()).sum() > planejamentoAberto.getRiscos().stream().mapToDouble(it -> it.getCores().stream().mapToInt(it2 -> it2.getTotalRealizado()).sum()).sum()) {
//                MessageBox.create(message -> {
//                    message.message("Para iniciar os riscos das demais aplicações da OF, é necessário concluir os riscos da aplicação geral na OF. Finalize os riscos e reinicie esta etapa.");
//                    message.type(MessageBox.TypeMessageBox.IMPORTANT);
//                    message.showAndWait();
//                });
//                return;
//            }
//        }
        
        // cria o planejamento no banco caso seja o primeiro risco
        if (planejamentoAberto.getRiscos().size() == 0) {
            try {
                planejamentoAberto = new FluentDao().persist(planejamentoAberto);
            } catch (SQLException e) {
                e.printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(e);
                    message.showAndWait();
                });
            }
        }
        
        
        // verificando as aplicações da OF (FT) com o tipo de risco
        List<VSdAplicacoesOf> aplicacoesRisco = aplicacoesOf.stream()
                .filter(aplicacao -> aplicacao.getAplicacao().getCodigo().equals(tipoRiscoSelecionado.getAplicacao().getCodigo()))
                .distinct()
                .collect(Collectors.toList());
        
        // selecionando a aplicação caso tenha mais de uma na FT do produto
        AtomicReference<VSdAplicacoesOf> aplicacaoRisco = new AtomicReference<>(aplicacoesRisco.get(0));
        if (aplicacoesRisco.size() > 1) {
            new Fragment().show(fragment -> {
                fragment.title("Aplicações do Tipo de Risco");
                fragment.size(350.0, 420.0);
                final FormTableView<VSdAplicacoesOf> tblAplicacoes = FormTableView.create(VSdAplicacoesOf.class, table -> {
                    table.title("Aplicações");
                    table.expanded();
                    table.items.set(FXCollections.observableList(aplicacoesRisco));
                    table.columns(
                            FormTableColumn.create(cln -> {
                                cln.title("Código");
                                cln.width(40.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdAplicacoesOf, VSdAplicacoesOf>, ObservableValue<VSdAplicacoesOf>>) param -> new ReadOnlyObjectWrapper(param.getValue().getAplicacaoft().getCodigo()));
                            }).build() /*Código*/,
                            FormTableColumn.create(cln -> {
                                cln.title("Descrição");
                                cln.width(200.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdAplicacoesOf, VSdAplicacoesOf>, ObservableValue<VSdAplicacoesOf>>) param -> new ReadOnlyObjectWrapper(param.getValue().getAplicacaoft().getDescricao()));
                                
                            }).build() /*Descrição*/
                    );
                });
                fragment.box.getChildren().add(FormBox.create(principal -> {
                    principal.vertical();
                    principal.expanded();
                    principal.add(FormFieldTextArea.create(field -> {
                        field.withoutTitle();
                        field.editable(false);
                        field.height(35.0);
                        field.value.set("Existe mais de uma aplicação para o tipo de risco selecionado. Selecione a aplicação que será feita esse risco:");
                    }).build());
                    principal.add(tblAplicacoes.build());
                }));
                fragment.buttonsBox.getChildren().add(FormButton.create(btnSelecionar -> {
                    btnSelecionar.title("Selecionar");
                    btnSelecionar.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                    btnSelecionar.addStyle("success");
                    btnSelecionar.disable.bind(tblAplicacoes.items.emptyProperty().or(tblAplicacoes.tableProperties().getSelectionModel().selectedItemProperty().isNull()));
                    btnSelecionar.setAction(evt -> {
                        aplicacaoRisco.set(tblAplicacoes.selectedItem());
                        fragment.close();
                    });
                }));
            });
        }
        
        // obtendo o material utilizado na aplicação do risco selecionado
        List<VSdMatAplicacaoOf> materialAplicacaoSelecionada = (List<VSdMatAplicacaoOf>) new FluentDao().selectFrom(VSdMatAplicacaoOf.class)
                .where(it -> it
                        .equal("numero", aplicacaoRisco.get().getNumero())
                        .equal("aplicacaoft.codigo", aplicacaoRisco.get().getAplicacaoft().getCodigo()))
                .resultList();
        
        // ativando modo de criar um novo risco
        criandoRisco.set(true);
        
        // criação do objeto do cabeçalho do risco
        SdRiscoPlanejamentoEncaixe riscoPlanejamento = new SdRiscoPlanejamentoEncaixe();
        riscoPlanejamento.setIdplanejamento(planejamentoAberto);
        riscoPlanejamento.setTiporisco(tipoRiscoSelecionado);
        riscoPlanejamento.setAplicacaoft(aplicacaoRisco.get().getAplicacaoft());
        riscoPlanejamento.setConsumopc(aplicacaoRisco.get().getConsumopc());
        
        SdRiscoPlanejamentoEncaixe finalRiscoPlanejamento2 = riscoPlanejamento;
        SdModelagemProduto modelagem = new FluentDao().selectFrom(SdModelagemProduto.class)
                .where(it -> it
                        .equal("id.codigo.codigo", planejamentoAberto.getNumero().getProduto().getCodigo())
                        .equal("id.aplicacao.codigo", finalRiscoPlanejamento2.getAplicacaoft().getCodigo()))
                .singleResult();
        riscoPlanejamento.setPartes(modelagem != null ? modelagem.getPartes() : 0);
        
        // criação da identificação do risco
        IntegerProperty contadorRisco = new SimpleIntegerProperty(0);
        do {
            contadorRisco.set(contadorRisco.add(1).get());
        } while (planejamentoAberto.getRiscos().stream().filter(it -> it.getTiporisco().getCodigo() == tipoRiscoSelecionado.getCodigo()).anyMatch(it -> it.getSeqTipo() == contadorRisco.get()));
        //String descRisco = tipoRiscoSelecionado.getCodrisco() + contadorRisco.get();
        riscoPlanejamento.setSeqTipo(contadorRisco.get());
        StringProperty nomeArquivoDiamino = new SimpleStringProperty();
        nomeArquivoDiamino.bind(planejamentoAberto.getNumero().numeroProperty()
                .concat(riscoPlanejamento.getTiporisco().codriscoProperty())
                .concat(riscoPlanejamento.seqTipoProperty().asString())
                .concat(Bindings.when(riscoPlanejamento.riscoAproProperty()).then("APRO").otherwise("")));
        riscoPlanejamento.arqdiaminoProperty().bindBidirectional(nomeArquivoDiamino);
        
        try {
            // field para nome do arquivo diamino (.plx)
            SdRiscoPlanejamentoEncaixe finalRiscoPlanejamento1 = riscoPlanejamento;
            final FormFieldText fieldArquivoDiamino = FormFieldText.create(field -> {
                field.withoutTitle();
                field.label("Arq. Diamino");
                field.width(300.0);
                field.editable.bind(emEdicao);
                field.value.bindBidirectional(finalRiscoPlanejamento1.arqdiaminoProperty());
            });
            
            // persistência do risco
            riscoPlanejamento = new FluentDao().persist(riscoPlanejamento);
            
            // adiciona o risco ao planejamento
            planejamentoAberto.getRiscos().add(riscoPlanejamento);
            
            // criação do objeto do box para adicionar as cores do risco
            final FormBox boxRiscosCores = FormBox.create(boxCores -> {
                boxCores.vertical();
                boxCores.expanded();
                boxCores.bothScroll();
            });
            
            // criação da aba do risco
            SdRiscoPlanejamentoEncaixe finalRiscoPlanejamento = riscoPlanejamento; // variável final para trabalhar dentro da ABA
            FormTab tabRisco = FormTab.create(tab -> {
                tab.title(tipoRiscoSelecionado.getDescricao());
                tab.setGraphic(FormFieldText.create(field -> {
                    field.withoutTitle();
                    field.editable(false);
                    field.focusTraversable(false);
                    field.addStyle("xs").addStyle(tipoRiscoSelecionado.getColor());
                    field.width(50.0);
                    field.alignment(Pos.CENTER);
                    field.value.bind(finalRiscoPlanejamento.getTiporisco().codriscoProperty().concat(finalRiscoPlanejamento.seqTipoProperty().asString()));
                }).build());
                tab.closable();
                tab.setOnCloseRequest(evt -> {
                    if ((Boolean) QuestionBox.build(Boolean.class, message -> {
                        message.message("Deseja realmente excluir o risco?");
                        message.showAndWait();
                    }).value.get()) {
                        cancelarRisco(finalRiscoPlanejamento);
                    } else {
                        evt.consume();
                    }
                });
                tab.add(FormBox.create(boxToolbar -> {
                    boxToolbar.horizontal();
                    boxToolbar.alignment(Pos.TOP_RIGHT);
                    boxToolbar.add(FormBox.create(boxToolBarExtras -> {
                        boxToolBarExtras.horizontal();
                        boxToolBarExtras.expanded();
                        boxToolBarExtras.add(FormButton.create(selectCores -> {
                            selectCores.title("Selecionar Cores");
                            selectCores.addStyle("info");
                            selectCores.icon(ImageUtils.getIcon(ImageUtils.Icon.COR, ImageUtils.IconSize._24));
                            selectCores.setAction(evt -> {
                                new Fragment().show(fragment -> {
                                    fragment.size(250.0, 300.0);

                                    final StringProperty stringCoresAgrupadas = new SimpleStringProperty(null); // string é criada no momento do teste se os riscos poderão ser agrupados
                                    final StringProperty idsCoresAgrupadas = new SimpleStringProperty(null); // string é criada no momento do teste se os riscos poderão ser agrupados

                                    saldoGrade.clear();
                                    saldoGrade.addAll(ofCarregada.getItens());
                                    final CheckListView<VSdCorOf> checkListCores = new CheckListView<VSdCorOf>();
                                    final CheckListView<SdRiscoPlanejamentoEncaixe> checkListRiscos = new CheckListView<SdRiscoPlanejamentoEncaixe>();
                                    final FormFieldToggleSingle toggleIsAgrupado = FormFieldToggleSingle.create(field -> {
                                        field.value.set(false);
                                        field.title("Agrupar Seleção?");
                                    });
                                    final FormFieldToggleSingle toggleIsAgrupaCor = FormFieldToggleSingle.create(field -> {
                                        field.value.set(false);
                                        field.title("Agrupar Cores?");
                                        field.editable.bind(toggleIsAgrupado.value);
                                    });

                                    // teste para verificar se o risco é corpo, detalhe/ribana ou não e criação dos dados dos lists e tela
                                    // * caso o risco é de corpo, seleciona as cores da OF
                                    // * caso o risco é do tipo detalhe/ribana, seleciona os riscos do corpo realizado
                                    // * caso o risco não seja um dos acima, abre as cores da OF para selecionar, porém as qtdes serão do corpo do produto
                                    if (tipoRiscoSelecionado.getCodigo() == 1) {
                                        final FormFieldToggleSingle riscoAproveitamento = FormFieldToggleSingle.create(field -> {
                                            field.value.set(false);
                                            field.title("Risco de Aproveitamento?");
                                            field.value.addListener((observable, oldValue, newValue) -> {
                                                if (newValue) {
                                                    saldoGrade.clear();
                                                    saldoGrade.addAll(ofCarregada.getItens());
                                                    checkListCores.setItems(saldoGrade);
                                                    checkListCores.refresh();
                                                } else {
                                                    saldoGrade.removeIf(it -> it.getQtde().compareTo(new BigDecimal(planejamentoAberto.getRiscos().stream()
                                                            .filter(it2 -> it2.getAplicacaoft().getCodigo() == aplicacaoRisco.get().getAplicacaoft().getCodigo())
                                                            .mapToInt(it2 -> it2.getCores().stream()
                                                                    .filter(it3 -> it3.getCor().equals(it.getId().getCor().getCor()))
                                                                    .mapToInt(it3 -> it3.getGrade().stream()
                                                                            .mapToInt(it4 -> it4.getRealizado())
                                                                            .sum())
                                                                    .sum())
                                                            .sum())) <= 0);
                                                    checkListCores.setItems(saldoGrade);
                                                    checkListCores.refresh();
                                                }
                                                finalRiscoPlanejamento.setRiscoApro(newValue);
                                            });
                                        });
                                        fragment.box.getChildren().addAll(riscoAproveitamento.build());
                                        fragment.title("Selecionar Cores do Produto");
                                        // remove da lista de cores as cores que foram todas planejadas em riscos.
                                        if (riscoAproveitamento.value.not().get())
                                            saldoGrade.removeIf(it -> it.getQtde().compareTo(new BigDecimal(planejamentoAberto.getRiscos().stream()
                                                    .filter(it2 -> it2.getAplicacaoft().getCodigo() == aplicacaoRisco.get().getAplicacaoft().getCodigo())
                                                    .mapToInt(it2 -> it2.getCores().stream()
                                                            .filter(it3 -> it3.getCor().equals(it.getId().getCor().getCor()))
                                                            .mapToInt(it3 -> it3.getGrade().stream()
                                                                    .mapToInt(it4 -> it4.getRealizado())
                                                                    .sum())
                                                            .sum())
                                                    .sum())) <= 0);
                                        checkListCores.setItems(saldoGrade);
                                        fragment.box.getChildren().addAll(checkListCores, toggleIsAgrupado.build());
                                    } else if (tipoRiscoSelecionado.getCodigo() == 3 || tipoRiscoSelecionado.getCodigo() == 5) {
                                        fragment.title("Selecionar Riscos do Produto");
                                        List<SdRiscoPlanejamentoEncaixe> riscosCorpo = planejamentoAberto.getRiscos().stream().filter(it -> it.getTiporisco().getCodigo() == 1).collect(Collectors.toList());
                                        riscosCorpo.forEach(it -> it.setSelected(false));
                                        // remove da lista de cores as cores que foram todas planejadas em riscos.
                                        riscosCorpo.removeIf(it -> {
                                            int qtdeRiscoCorpo = it.getCores().stream().mapToInt(SdCorRisco::getTotalRealizado).sum();
                                            int qtdeRealizadoRisco = planejamentoAberto.getRiscos().stream()
                                                    .filter(it2 -> it2.getAplicacaoft().getCodigo() == aplicacaoRisco.get().getAplicacaoft().getCodigo())// && it2.getCores().stream().anyMatch(it3 -> it3.getRiscoCorpo().equals(it)))
                                                    .mapToInt(it2 -> it2.getCores().stream()
                                                            .filter(it3 -> it3.getRiscoCorpo().stream().anyMatch(it4 -> it.getCores().contains(it4)) && !it3.isCorAgrupada())
                                                            .mapToInt(it3 -> it3.getTotalRealizado())
                                                            .sum())
                                                    .sum();
                                            return qtdeRiscoCorpo <= qtdeRealizadoRisco;
                                        });
                                        checkListRiscos.setItems(FXCollections.observableList(riscosCorpo));
                                        checkListRiscos.setCellFactory(param -> {
                                            return new CheckBoxListCell<SdRiscoPlanejamentoEncaixe>(SdRiscoPlanejamentoEncaixe::selectedProperty) {
                                                @Override
                                                public void updateItem(SdRiscoPlanejamentoEncaixe item, boolean empty) {
                                                    super.updateItem(item, empty);
                                                    setText(null);
                                                    if (item != null && !empty) {
                                                        setText(item.getTiporisco().getCodrisco() + item.getSeq() + " - Cores: " + item.getCores().stream().map(it -> it.getCor()).collect(Collectors.joining(", ")));
                                                    }
                                                }
                                            };
                                        });
                                        fragment.box.getChildren().addAll(checkListRiscos, toggleIsAgrupado.build(), toggleIsAgrupaCor.build());
                                    } else {
                                        fragment.title("Selecionar Cores do Produto");
                                        // remove da lista de cores as cores que foram todas planejadas em riscos.
                                        saldoGrade.removeIf(it -> {
                                            BigDecimal qtdeParaRealizar = new BigDecimal(planejamentoAberto.getRiscos().stream()
                                                    .filter(it2 -> it2.getTiporisco().getCodigo() == 1)
                                                    .mapToInt(it2 -> it2.getCores().stream()
                                                            .filter(it3 -> it3.getCor().equals(it.getId().getCor().getCor()) || checkRiscoAgrupado(it3.getCor()))
                                                            .mapToInt(it3 -> it3.getGrade().stream()
                                                                    .mapToInt(it4 -> it4.getRealizado())
                                                                    .sum())
                                                            .sum())
                                                    .sum());
                                            BigDecimal qtdeRealizado = new BigDecimal(planejamentoAberto.getRiscos().stream()
                                                    .filter(it2 -> it2.getAplicacaoft().getCodigo() == aplicacaoRisco.get().getAplicacaoft().getCodigo())
                                                    .mapToInt(it2 -> it2.getCores().stream()
                                                            .filter(it3 -> it3.getCor().equals(it.getId().getCor().getCor()) || checkRiscoAgrupado(it3.getCor()))
                                                            .mapToInt(it3 -> it3.getGrade().stream()
                                                                    .mapToInt(it4 -> it4.getRealizado())
                                                                    .sum())
                                                            .sum())
                                                    .sum());
                                            return qtdeParaRealizar.compareTo(qtdeRealizado) <= 0;
                                        });
                                        checkListCores.setItems(saldoGrade);
                                        fragment.box.getChildren().addAll(checkListCores, toggleIsAgrupado.build());
                                    }

                                    fragment.buttonsBox.getChildren().add(FormButton.create(btnSelecionarCores -> {
                                        btnSelecionarCores.title("Selecionar Cores");
                                        btnSelecionarCores.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._16));
                                        btnSelecionarCores.addStyle("success");
                                        btnSelecionarCores.setAction(evt2 -> {
                                            if (tipoRiscoSelecionado.getCodigo() == 1) {
                                                List<VSdCorOf> coresSelecionadas = checkListCores.getCheckModel().getCheckedItems();
                                                finalRiscoPlanejamento.getCores().clear(); // limpa as cores do risco, caso esteja alterando as cores

                                                if (coresSelecionadas.size() > 0) {
                                                    // fazer um teste caso o usuário selecione agrupar seleção
                                                    // se ele colocar agrupar seleção e na aplicação do risco, tiver dois materiais diferentes não poderá ser agrupado, o sistema irá montar os subriscos (1.1,1.2)
                                                    if (toggleIsAgrupado.value.get() && coresSelecionadas.size() > 1) {
                                                        Long qtdeMateriaisCores = materialAplicacaoSelecionada.stream()
                                                                .map(it -> "MAT-" + it.getInsumo().getCodigo() + "COR-" + it.getCori().getCor())
                                                                .distinct()
                                                                .count();
                                                        if (qtdeMateriaisCores > 1) {
                                                            toggleIsAgrupado.value.set(false);
                                                        } else {
                                                            stringCoresAgrupadas.set(coresSelecionadas.stream().map(it -> it.getId().getCor().getCor()).distinct().collect(Collectors.joining("','")));
                                                        }
                                                    }

                                                    // verifica se o risco é com cores agrupadas separando as cores e criando os subriscos
                                                    if (toggleIsAgrupado.value.not().get()) {
                                                        // risco com cores não agrupadas
                                                        coresSelecionadas.forEach(corRisco -> {
                                                            // cria o objeto da cor do risco e alimenta os dados
                                                            SdCorRisco riscoCor = new SdCorRisco();
                                                            riscoCor.setIdrisco(finalRiscoPlanejamento.getId());
                                                            riscoCor.setCor(corRisco.getId().getCor().getCor());
                                                            riscoCor.setComprimento(BigDecimal.ZERO);
                                                            riscoCor.setConsumopeca(BigDecimal.ZERO);
                                                            riscoCor.setConsumototal(BigDecimal.ZERO);
                                                            riscoCor.setFolhas(0);
                                                            riscoCor.setSeq(coresSelecionadas.indexOf(corRisco) + 1);
                                                            //riscoCor.descriscoProperty().bind(descricaoRisco.concat(finalRiscoPlanejamento.seqTipoProperty().asString()).concat(new SimpleStringProperty("."+coresSelecionadas.indexOf(corRisco) + 1)));
                                                            //riscoCor.setDescrisco(descRisco + "." + (coresSelecionadas.indexOf(corRisco) + 1));
                                                            try {
                                                                // persistindo a cor-risco no banco
                                                                riscoCor = new FluentDao().persist(riscoCor);
                                                                finalRiscoPlanejamento.getCores().add(riscoCor);

                                                                // criação da grade de tamanhos para a cor do risco
                                                                SdCorRisco finalRiscoCor = riscoCor;
                                                                corRisco.getTams().forEach(tam -> {
                                                                    // criando o objeto do tamanho e alimentado com os dados
                                                                    SdGradeRiscoCor tamanhosRiscoCor = new SdGradeRiscoCor();
                                                                    tamanhosRiscoCor.setIdrisco(finalRiscoCor.getId());
                                                                    tamanhosRiscoCor.setCor(corRisco.getId().getCor().getCor());
                                                                    tamanhosRiscoCor.setTam(tam.getId().getTam());
                                                                    tamanhosRiscoCor.setGrade(0);
                                                                    tamanhosRiscoCor.setParte(0);
                                                                    tamanhosRiscoCor.setRealizado(0);
                                                                    tamanhosRiscoCor.setSaldoOf(tam.getQtdeof().subtract(new BigDecimal(planejamentoAberto.getRiscos().stream()
                                                                            .filter(it -> it.getAplicacaoft().getCodigo() == aplicacaoRisco.get().getAplicacaoft().getCodigo())
                                                                            .mapToInt(it -> it.getCores().stream().mapToInt(it2 -> it2.getGrade().stream()
                                                                                    .filter(it3 -> it3.getCor().equals(corRisco.getId().getCor().getCor()) && it3.getTam().equals(tam.getId().getTam()))
                                                                                    .mapToInt(it3 -> it3.getRealizado())
                                                                                    .sum()).sum())
                                                                            .sum())).intValue());
                                                                    try {
                                                                        tamanhosRiscoCor = new FluentDao().persist(tamanhosRiscoCor);
                                                                        finalRiscoCor.getGrade().add(tamanhosRiscoCor);
                                                                    } catch (SQLException e) {
                                                                        e.printStackTrace();
                                                                        ExceptionBox.build(message -> {
                                                                            message.exception(e);
                                                                            message.showAndWait();
                                                                        });
                                                                    }
                                                                });
                                                                // atualizando os totais da cor conforme o cadastro
                                                                riscoCor.setTotalSaldoOf(riscoCor.getGrade().stream().mapToInt(tam -> tam.getSaldoOf()).sum());
                                                            } catch (SQLException e) {
                                                                e.printStackTrace();
                                                                ExceptionBox.build(message -> {
                                                                    message.exception(e);
                                                                    message.showAndWait();
                                                                });
                                                            }
                                                        });
                                                    } else {
                                                        SdCorRisco riscoCor = new SdCorRisco();
                                                        riscoCor.setIdrisco(finalRiscoPlanejamento.getId());
                                                        riscoCor.setCor(stringCoresAgrupadas.get());
                                                        riscoCor.setComprimento(BigDecimal.ZERO);
                                                        riscoCor.setConsumopeca(BigDecimal.ZERO);
                                                        riscoCor.setConsumototal(BigDecimal.ZERO);
                                                        riscoCor.setFolhas(0);
                                                        riscoCor.setSeq(1);
                                                        //riscoCor.descriscoProperty().bind(descricaoRisco.concat(finalRiscoPlanejamento.seqTipoProperty().asString()).concat(new SimpleStringProperty(".1")));
                                                        //riscoCor.setDescrisco(descRisco + "." + 1);
                                                        try {
                                                            riscoCor = new FluentDao().persist(riscoCor);
                                                            finalRiscoPlanejamento.getCores().add(riscoCor);
                                                        } catch (SQLException e) {
                                                            e.printStackTrace();
                                                            ExceptionBox.build(message -> {
                                                                message.exception(e);
                                                                message.showAndWait();
                                                            });
                                                        }

                                                        SdCorRisco finalRiscoCor = riscoCor;
                                                        coresSelecionadas.get(0).getTams().forEach(tam -> {
                                                            SdGradeRiscoCor tamanhosRiscoCor = new SdGradeRiscoCor();
                                                            tamanhosRiscoCor.setIdrisco(finalRiscoCor.getId());
                                                            tamanhosRiscoCor.setCor(null);
                                                            tamanhosRiscoCor.setTam(tam.getId().getTam());
                                                            tamanhosRiscoCor.setGrade(0);
                                                            tamanhosRiscoCor.setParte(0);
                                                            tamanhosRiscoCor.setRealizado(0);
                                                            tamanhosRiscoCor.setSaldoOf(tam.getQtdeof().subtract(new BigDecimal(planejamentoAberto.getRiscos().stream()
                                                                    .filter(it -> it.getAplicacaoft().getCodigo() == aplicacaoRisco.get().getAplicacaoft().getCodigo())
                                                                    .mapToInt(it -> it.getCores().stream()
                                                                            .mapToInt(it2 -> it2.getGrade().stream()
                                                                                    .filter(it3 -> it3.getTam().equals(tam.getId().getTam()))
                                                                                    .mapToInt(it3 -> it3.getRealizado())
                                                                                    .sum())
                                                                            .sum())
                                                                    .sum())).intValue());
                                                            try {
                                                                tamanhosRiscoCor = new FluentDao().persist(tamanhosRiscoCor);
                                                                finalRiscoCor.getGrade().add(tamanhosRiscoCor);
                                                            } catch (SQLException e) {
                                                                e.printStackTrace();
                                                                ExceptionBox.build(message -> {
                                                                    message.exception(e);
                                                                    message.showAndWait();
                                                                });
                                                            }
                                                        });
                                                        riscoCor.setTotalSaldoOf(riscoCor.getGrade().stream().mapToInt(tam -> tam.getSaldoOf()).sum());
                                                    }
                                                }
                                            } else if (tipoRiscoSelecionado.getCodigo() == 3 || tipoRiscoSelecionado.getCodigo() == 5) {
                                                List<SdRiscoPlanejamentoEncaixe> riscosSelecionados = checkListRiscos.getItems().filtered(it -> it.isSelected());
                                                finalRiscoPlanejamento.getCores().clear(); // limpa as cores do risco, caso esteja alterando as cores

                                                if (riscosSelecionados.size() > 0) {
                                                    // fazer um teste caso o usuário selecione agrupar seleção
                                                    // se ele colocar agrupar seleção e na aplicação do risco, tiver dois materiais diferentes não poderá ser agrupado, o sistema irá montar os subriscos (1.1,1.2)
                                                    if (toggleIsAgrupado.value.get() /*&& riscosSelecionados.size() > 1*/) {
                                                        Long qtdeMateriaisCores = materialAplicacaoSelecionada.stream()
                                                                .map(it -> "MAT-" + it.getInsumo().getCodigo() + (toggleIsAgrupaCor.value.get() ? "" : "COR-" + it.getCori().getCor()))
                                                                .distinct()
                                                                .count();
                                                        Set<String> coresSelecao = new HashSet<>();
                                                        riscosSelecionados.forEach(it -> it.getCores().forEach(it2 -> coresSelecao.add(it2.getCor())));

                                                        if (qtdeMateriaisCores > 1 && coresSelecao.size() > 1) {
                                                            toggleIsAgrupado.value.set(false);
                                                        } else {
                                                            stringCoresAgrupadas.set(riscosSelecionados.stream().map(it -> it.getCores().stream().map(SdCorRisco::getCor).distinct().collect(Collectors.joining("','"))).distinct().collect(Collectors.joining("','")));
                                                            idsCoresAgrupadas.set(riscosSelecionados.stream().map(it -> it.getCores().stream().map(it2 -> String.valueOf(it2.getId())).distinct().collect(Collectors.joining(","))).distinct().collect(Collectors.joining(",")));
                                                        }
                                                    }

                                                    // verifica se o risco é com cores agrupadas separando as cores e criando os subriscos
                                                    if (toggleIsAgrupado.value.not().get()) {
                                                        riscosSelecionados.forEach(riscoCorpo -> {
                                                            riscoCorpo.getCores().forEach(it -> {
                                                                SdCorRisco riscoCor = new SdCorRisco();
                                                                riscoCor.setCor(String.valueOf(it.getId()));
                                                                riscoCor.setCorAgrupada(toggleIsAgrupaCor.value.get());
                                                                riscoCor.setComprimento(BigDecimal.ZERO);
                                                                riscoCor.setConsumopeca(BigDecimal.ZERO);
                                                                riscoCor.setConsumototal(BigDecimal.ZERO);
                                                                riscoCor.setFolhas(0);
                                                                riscoCor.setSeq(riscosSelecionados.indexOf(riscoCorpo) + 1);
                                                                riscoCor.setIdrisco(finalRiscoPlanejamento.getId());
                                                                //riscoCor.descriscoProperty().bind(descricaoRisco.concat(finalRiscoPlanejamento.seqTipoProperty().asString()).concat(new SimpleStringProperty("."+riscosSelecionados.indexOf(riscoCorpo) + 1)));
                                                                //riscoCor.setDescrisco(descRisco + "." + (riscosSelecionados.indexOf(riscoCorpo) + 1));
                                                                riscoCor.getRiscoCorpo().add(it);
                                                                try {
                                                                    riscoCor = new FluentDao().persist(riscoCor);
                                                                    finalRiscoPlanejamento.getCores().add(riscoCor);
                                                                } catch (SQLException e) {
                                                                    e.printStackTrace();
                                                                    ExceptionBox.build(message -> {
                                                                        message.exception(e);
                                                                        message.showAndWait();
                                                                    });
                                                                }

                                                                SdCorRisco finalRiscoCor = riscoCor;
                                                                it.getGrade().forEach(tam -> {
                                                                    SdGradeRiscoCor tamanhosRiscoCor = new SdGradeRiscoCor();
                                                                    tamanhosRiscoCor.setIdrisco(finalRiscoCor.getId());
                                                                    tamanhosRiscoCor.setCor(it.getCor());
                                                                    tamanhosRiscoCor.setTam(tam.getTam());
                                                                    tamanhosRiscoCor.setGrade(0);
                                                                    tamanhosRiscoCor.setParte(0);
                                                                    tamanhosRiscoCor.setRealizado(0);
                                                                    tamanhosRiscoCor.setSaldoOf(tam.getRealizado() - planejamentoAberto.getRiscos().stream()
                                                                            .filter(it2 -> it2.getAplicacaoft().getCodigo() == aplicacaoRisco.get().getAplicacaoft().getCodigo())
                                                                            .mapToInt(it2 -> it2.getCores().stream()
                                                                                    .filter(it3 -> it3.getRiscoCorpo().stream().anyMatch(it4 -> it4.equals(it)) && (it3.getCor().equals(it.getCor())))
                                                                                    .mapToInt(it3 -> it3.getGrade().stream()
                                                                                            .filter(it4 -> it4.getTam().equals(tam.getTam()))
                                                                                            .mapToInt(SdGradeRiscoCor::getRealizado)
                                                                                            .sum())
                                                                                    .sum())
                                                                            .sum());
                                                                    try {
                                                                        tamanhosRiscoCor = new FluentDao().persist(tamanhosRiscoCor);
                                                                        finalRiscoCor.getGrade().add(tamanhosRiscoCor);
                                                                    } catch (SQLException e) {
                                                                        e.printStackTrace();
                                                                        ExceptionBox.build(message -> {
                                                                            message.exception(e);
                                                                            message.showAndWait();
                                                                        });
                                                                    }
                                                                });
                                                                riscoCor.setTotalSaldoOf(riscoCor.getGrade().stream().mapToInt(tam -> tam.getSaldoOf()).sum());
                                                            });
                                                        });
                                                    } else {
                                                        if (toggleIsAgrupaCor.value.not().get()) {
                                                            SdCorRisco riscoCor = new SdCorRisco();
                                                            riscoCor.setCor(idsCoresAgrupadas.get());
                                                            riscoCor.setCorAgrupada(true);
                                                            riscoCor.setComprimento(BigDecimal.ZERO);
                                                            riscoCor.setConsumopeca(BigDecimal.ZERO);
                                                            riscoCor.setConsumototal(BigDecimal.ZERO);
                                                            riscoCor.setFolhas(0);
                                                            riscoCor.setSeq(1);
                                                            riscoCor.setIdrisco(finalRiscoPlanejamento.getId());
                                                            //riscoCor.descriscoProperty().bind(descricaoRisco.concat(finalRiscoPlanejamento.seqTipoProperty().asString()).concat(new SimpleStringProperty(".1")));
                                                            //riscoCor.setDescrisco(descRisco + "." + 1);
                                                            SdCorRisco finalRiscoCor1 = riscoCor;
                                                            riscosSelecionados.forEach(it -> finalRiscoCor1.getRiscoCorpo().addAll(it.getCores()));
                                                            try {
                                                                riscoCor = new FluentDao().persist(riscoCor);
                                                                finalRiscoPlanejamento.getCores().add(riscoCor);
                                                            } catch (SQLException e) {
                                                                e.printStackTrace();
                                                                ExceptionBox.build(message -> {
                                                                    message.exception(e);
                                                                    message.showAndWait();
                                                                });
                                                            }

                                                            SdCorRisco finalRiscoCor = riscoCor;
                                                            riscosSelecionados.get(0).getCores().get(0).getGrade().forEach(it -> {
                                                                SdGradeRiscoCor tamanhosRiscoCor = new SdGradeRiscoCor();
                                                                tamanhosRiscoCor.setIdrisco(finalRiscoCor.getId());
                                                                tamanhosRiscoCor.setCor(null);
                                                                tamanhosRiscoCor.setTam(it.getTam());
                                                                tamanhosRiscoCor.setGrade(0);
                                                                tamanhosRiscoCor.setParte(0);
                                                                tamanhosRiscoCor.setRealizado(0);
                                                                tamanhosRiscoCor.setSaldoOf(riscosSelecionados.stream()
                                                                        .mapToInt(it2 -> it2.getCores().stream()
                                                                                .mapToInt(it3 -> it3.getGrade().stream()
                                                                                        .filter(it4 -> it4.getTam().equals(it.getTam()))
                                                                                        .mapToInt(SdGradeRiscoCor::getRealizado)
                                                                                        .sum())
                                                                                .sum())
                                                                        .sum() - planejamentoAberto.getRiscos().stream()
                                                                        .filter(it2 -> it2.getAplicacaoft().getCodigo() == aplicacaoRisco.get().getAplicacaoft().getCodigo())
                                                                        .mapToInt(it2 -> it2.getCores().stream()
                                                                                .mapToInt(it3 -> it3.getGrade().stream()
                                                                                        .filter(it4 -> it4.getTam().equals(it.getTam()))
                                                                                        .mapToInt(SdGradeRiscoCor::getRealizado)
                                                                                        .sum())
                                                                                .sum())
                                                                        .sum());
                                                                try {
                                                                    tamanhosRiscoCor = new FluentDao().persist(tamanhosRiscoCor);
                                                                    finalRiscoCor.getGrade().add(tamanhosRiscoCor);
                                                                } catch (SQLException e) {
                                                                    e.printStackTrace();
                                                                    ExceptionBox.build(message -> {
                                                                        message.exception(e);
                                                                        message.showAndWait();
                                                                    });
                                                                }
                                                            });
                                                            riscoCor.setTotalSaldoOf(riscoCor.getGrade().stream().mapToInt(tam -> tam.getSaldoOf()).sum());
                                                        } else {
                                                            Set<String> coresSelecao = new HashSet<>();
                                                            riscosSelecionados.forEach(it -> it.getCores().forEach(it2 -> coresSelecao.add(it2.getCor())));

                                                            for (String cor : coresSelecao) {
                                                                SdCorRisco riscoCor = new SdCorRisco();
                                                                riscoCor.setCor(cor);
                                                                riscoCor.setCorAgrupada(true);
                                                                riscoCor.setGrupoCor(true);
                                                                riscoCor.setComprimento(BigDecimal.ZERO);
                                                                riscoCor.setConsumopeca(BigDecimal.ZERO);
                                                                riscoCor.setConsumototal(BigDecimal.ZERO);
                                                                riscoCor.setFolhas(0);
                                                                riscoCor.setSeq(1);
                                                                riscoCor.setIdrisco(finalRiscoPlanejamento.getId());
                                                                //riscoCor.descriscoProperty().bind(descricaoRisco.concat(finalRiscoPlanejamento.seqTipoProperty().asString()).concat(new SimpleStringProperty(".1")));
                                                                //riscoCor.setDescrisco(descRisco + "." + 1);
                                                                SdCorRisco finalRiscoCor1 = riscoCor;
                                                                riscosSelecionados.forEach(it -> finalRiscoCor1.getRiscoCorpo().addAll(it.getCores()));
                                                                try {
                                                                    riscoCor = new FluentDao().persist(riscoCor);
                                                                    finalRiscoPlanejamento.getCores().add(riscoCor);
                                                                } catch (SQLException e) {
                                                                    e.printStackTrace();
                                                                    ExceptionBox.build(message -> {
                                                                        message.exception(e);
                                                                        message.showAndWait();
                                                                    });
                                                                }

                                                                SdCorRisco finalRiscoCor = riscoCor;
                                                                riscosSelecionados.get(0).getCores().get(0).getGrade().forEach(it -> {
                                                                    SdGradeRiscoCor tamanhosRiscoCor = new SdGradeRiscoCor();
                                                                    tamanhosRiscoCor.setIdrisco(finalRiscoCor.getId());
                                                                    tamanhosRiscoCor.setCor(null);
                                                                    tamanhosRiscoCor.setTam(it.getTam());
                                                                    tamanhosRiscoCor.setGrade(0);
                                                                    tamanhosRiscoCor.setParte(0);
                                                                    tamanhosRiscoCor.setRealizado(0);
                                                                    tamanhosRiscoCor.setSaldoOf(riscosSelecionados.stream()
                                                                            .mapToInt(it2 -> it2.getCores().stream()
                                                                                    .filter(it3 -> it3.getCor().equals(cor))
                                                                                    .mapToInt(it3 -> it3.getGrade().stream()
                                                                                            .filter(it4 -> it4.getTam().equals(it.getTam()))
                                                                                            .mapToInt(SdGradeRiscoCor::getRealizado)
                                                                                            .sum())
                                                                                    .sum())
                                                                            .sum() - planejamentoAberto.getRiscos().stream()
                                                                            .filter(it2 -> it2.getAplicacaoft().getCodigo() == aplicacaoRisco.get().getAplicacaoft().getCodigo())
                                                                            .mapToInt(it2 -> it2.getCores().stream()
                                                                                    .filter(it3 -> it3.getCor().equals(cor))
                                                                                    .mapToInt(it3 -> it3.getGrade().stream()
                                                                                            .filter(it4 -> it4.getTam().equals(it.getTam()))
                                                                                            .mapToInt(SdGradeRiscoCor::getRealizado)
                                                                                            .sum())
                                                                                    .sum())
                                                                            .sum());
                                                                    try {
                                                                        tamanhosRiscoCor = new FluentDao().persist(tamanhosRiscoCor);
                                                                        finalRiscoCor.getGrade().add(tamanhosRiscoCor);
                                                                    } catch (SQLException e) {
                                                                        e.printStackTrace();
                                                                        ExceptionBox.build(message -> {
                                                                            message.exception(e);
                                                                            message.showAndWait();
                                                                        });
                                                                    }
                                                                });
                                                                riscoCor.setTotalSaldoOf(riscoCor.getGrade().stream().mapToInt(tam -> tam.getSaldoOf()).sum());
                                                            }
                                                        }
                                                    }
                                                }
                                            } else {
                                                List<VSdCorOf> coresSelecionadas = checkListCores.getCheckModel().getCheckedItems();
                                                finalRiscoPlanejamento.getCores().clear(); // limpa as cores do risco, caso esteja alterando as cores

                                                if (coresSelecionadas.size() > 0) {
                                                    // fazer um teste caso o usuário selecione agrupar seleção
                                                    // se ele colocar agrupar seleção e na aplicação do risco, tiver dois materiais diferentes não poderá ser agrupado, o sistema irá montar os subriscos (1.1,1.2)
                                                    if (toggleIsAgrupado.value.get() && coresSelecionadas.size() > 1) {
                                                        List<VSdMatAplicacaoOfCor> coresAplicacao = (List<VSdMatAplicacaoOfCor>) new FluentDao().selectFrom(VSdMatAplicacaoOfCor.class)
                                                                .where(it -> it
                                                                        .equal("numero", aplicacaoRisco.get().getNumero())
                                                                        .equal("aplicacaoft.codigo", aplicacaoRisco.get().getAplicacaoft().getCodigo())
                                                                        .isIn("cor", coresSelecionadas.stream().map(it2 -> it2.getId().getCor().getCor()).toArray()))
                                                                .resultList();
                                                        Long qtdeMateriaisCores = coresAplicacao.stream()
                                                                .map(it -> "MAT-" + it.getInsumo().getCodigo() + "COR-" + it.getCori().getCor())
                                                                .distinct()
                                                                .count();
                                                        if (qtdeMateriaisCores > 1) {
                                                            toggleIsAgrupado.value.set(false);
                                                        } else {
                                                            stringCoresAgrupadas.set(coresSelecionadas.stream().map(it -> it.getId().getCor().getCor()).distinct().collect(Collectors.joining("','")));
                                                        }
                                                    }

                                                    // verifica se o risco é com cores agrupadas separando as cores e criando os subriscos
                                                    if (toggleIsAgrupado.value.not().get()) {
                                                        coresSelecionadas.forEach(corRisco -> {
                                                            SdCorRisco riscoCor = new SdCorRisco();
                                                            riscoCor.setIdrisco(finalRiscoPlanejamento.getId());
                                                            riscoCor.setCor(corRisco.getId().getCor().getCor());
                                                            riscoCor.setComprimento(BigDecimal.ZERO);
                                                            riscoCor.setConsumopeca(BigDecimal.ZERO);
                                                            riscoCor.setConsumototal(BigDecimal.ZERO);
                                                            riscoCor.setFolhas(0);
                                                            riscoCor.setSeq(coresSelecionadas.indexOf(corRisco) + 1);
                                                            //riscoCor.descriscoProperty().bind(descricaoRisco.concat(finalRiscoPlanejamento.seqTipoProperty().asString()).concat(new SimpleStringProperty("."+coresSelecionadas.indexOf(corRisco) + 1)));
                                                            //riscoCor.setDescrisco(descRisco + "." + (coresSelecionadas.indexOf(corRisco) + 1));
                                                            try {
                                                                riscoCor = new FluentDao().persist(riscoCor);
                                                                finalRiscoPlanejamento.getCores().add(riscoCor);

                                                                SdCorRisco finalRiscoCor = riscoCor;
                                                                corRisco.getTams().forEach(tam -> {
                                                                    SdGradeRiscoCor tamanhosRiscoCor = new SdGradeRiscoCor();
                                                                    tamanhosRiscoCor.setIdrisco(finalRiscoCor.getId());
                                                                    tamanhosRiscoCor.setCor(corRisco.getId().getCor().getCor());
                                                                    tamanhosRiscoCor.setTam(tam.getId().getTam());
                                                                    tamanhosRiscoCor.setGrade(0);
                                                                    tamanhosRiscoCor.setParte(0);
                                                                    tamanhosRiscoCor.setRealizado(0);
                                                                    tamanhosRiscoCor.setSaldoOf(new BigDecimal(planejamentoAberto.getRiscos().stream()
                                                                            .filter(it -> it.getTiporisco().getCodigo() == 1)
                                                                            .mapToInt(it -> it.getCores().stream()
                                                                                    .filter(it2 -> it2.getCor().equals(finalRiscoCor.getCor()))
                                                                                    .mapToInt(it2 -> it2.getGrade().stream()
                                                                                            .filter(it3 -> it3.getTam().equals(tam.getId().getTam()))
                                                                                            .mapToInt(it3 -> it3.getRealizado())
                                                                                            .sum())
                                                                                    .sum())
                                                                            .sum())
                                                                            .subtract(new BigDecimal(planejamentoAberto.getRiscos().stream()
                                                                                    .filter(it -> it.getAplicacaoft().getCodigo() == aplicacaoRisco.get().getAplicacaoft().getCodigo())
                                                                                    .mapToInt(it -> it.getCores().stream()
                                                                                            .filter(it2 -> it2.getCor().equals(corRisco.getId().getCor().getCor()))
                                                                                            .mapToInt(it2 -> it2.getGrade().stream()
                                                                                                    .filter(it3 -> it3.getTam().equals(tam.getId().getTam()))
                                                                                                    .mapToInt(it3 -> it3.getRealizado())
                                                                                                    .sum()).sum())
                                                                                    .sum())).intValue());
                                                                    try {
                                                                        tamanhosRiscoCor = new FluentDao().persist(tamanhosRiscoCor);
                                                                        finalRiscoCor.getGrade().add(tamanhosRiscoCor);
                                                                    } catch (SQLException e) {
                                                                        e.printStackTrace();
                                                                        ExceptionBox.build(message -> {
                                                                            message.exception(e);
                                                                            message.showAndWait();
                                                                        });
                                                                    }
                                                                });
                                                                riscoCor.setTotalSaldoOf(riscoCor.getGrade().stream().mapToInt(tam -> tam.getSaldoOf()).sum());
                                                            } catch (SQLException e) {
                                                                e.printStackTrace();
                                                                ExceptionBox.build(message -> {
                                                                    message.exception(e);
                                                                    message.showAndWait();
                                                                });
                                                            }
                                                        });
                                                    } else {
                                                        SdCorRisco riscoCor = new SdCorRisco();
                                                        riscoCor.setIdrisco(finalRiscoPlanejamento.getId());
                                                        riscoCor.setCor(stringCoresAgrupadas.get());
                                                        riscoCor.setComprimento(BigDecimal.ZERO);
                                                        riscoCor.setConsumopeca(BigDecimal.ZERO);
                                                        riscoCor.setConsumototal(BigDecimal.ZERO);
                                                        riscoCor.setFolhas(0);
                                                        riscoCor.setSeq(1);
                                                        riscoCor.setCorAgrupada(true);
                                                        //riscoCor.descriscoProperty().bind(descricaoRisco.concat(finalRiscoPlanejamento.seqTipoProperty().asString()).concat(new SimpleStringProperty(".1")));
                                                        //riscoCor.setDescrisco(descRisco + "." + 1);
                                                        try {
                                                            riscoCor = new FluentDao().persist(riscoCor);
                                                            finalRiscoPlanejamento.getCores().add(riscoCor);
                                                        } catch (SQLException e) {
                                                            e.printStackTrace();
                                                            ExceptionBox.build(message -> {
                                                                message.exception(e);
                                                                message.showAndWait();
                                                            });
                                                        }

                                                        SdCorRisco finalRiscoCor = riscoCor;
                                                        coresSelecionadas.get(0).getTams().forEach(tam -> {
                                                            SdGradeRiscoCor tamanhosRiscoCor = new SdGradeRiscoCor();
                                                            tamanhosRiscoCor.setIdrisco(finalRiscoCor.getId());
                                                            tamanhosRiscoCor.setCor(null);
                                                            tamanhosRiscoCor.setTam(tam.getId().getTam());
                                                            tamanhosRiscoCor.setGrade(0);
                                                            tamanhosRiscoCor.setParte(0);
                                                            tamanhosRiscoCor.setRealizado(0);
                                                            tamanhosRiscoCor.setSaldoOf(new BigDecimal(planejamentoAberto.getRiscos().stream()
                                                                    .filter(it -> it.getTiporisco().getCodigo() == 1)
                                                                    .mapToInt(it -> it.getCores().stream()
                                                                            .mapToInt(it2 -> it2.getGrade().stream()
                                                                                    .filter(it3 -> it3.getTam().equals(tam.getId().getTam()))
                                                                                    .mapToInt(it3 -> it3.getRealizado())
                                                                                    .sum())
                                                                            .sum())
                                                                    .sum())
                                                                    .subtract(new BigDecimal(planejamentoAberto.getRiscos().stream()
                                                                            .filter(it -> it.getAplicacaoft().getCodigo() == aplicacaoRisco.get().getAplicacaoft().getCodigo())
                                                                            .mapToInt(it -> it.getCores().stream().mapToInt(it2 -> it2.getGrade().stream()
                                                                                    .filter(it3 -> it3.getTam().equals(tam.getId().getTam()))
                                                                                    .mapToInt(it3 -> it3.getRealizado())
                                                                                    .sum()).sum())
                                                                            .sum())).intValue());
                                                            try {
                                                                tamanhosRiscoCor = new FluentDao().persist(tamanhosRiscoCor);
                                                                finalRiscoCor.getGrade().add(tamanhosRiscoCor);
                                                            } catch (SQLException e) {
                                                                e.printStackTrace();
                                                                ExceptionBox.build(message -> {
                                                                    message.exception(e);
                                                                    message.showAndWait();
                                                                });
                                                            }
                                                        });
                                                        riscoCor.setTotalSaldoOf(riscoCor.getGrade().stream().mapToInt(tam -> tam.getSaldoOf()).sum());
                                                    }
                                                }
                                            }

                                            finalRiscoPlanejamento.setStringCoresAgrupadas(stringCoresAgrupadas.get());
                                            finalRiscoPlanejamento.setIdsCoresAgrupadas(idsCoresAgrupadas.get());
                                            criarBoxRiscos(boxRiscosCores, finalRiscoPlanejamento);
                                            fragment.close();
                                            atualizarSaldosPlanejamento(finalRiscoPlanejamento);

                                            SysLogger.addSysDelizLog("Planejamento Encaixe", TipoAcao.EDITAR, planejamentoAberto.getNumero().getNumero(),
                                                    "Adicionando cores no risco ID " + finalRiscoPlanejamento.getId()
                                                            + " (" + finalRiscoPlanejamento.getTiporisco().getCodrisco() + finalRiscoPlanejamento.getSeqTipo() + ") no planejamento ID " + planejamentoAberto.getId());
                                        });
                                    })); // botão da seleção de cores do produto para criar os riscos
                                });
                            });
                        })); // botão com a seleção de cores
                        boxToolBarExtras.add(fieldArquivoDiamino.build()); // field com o nome do arquivo diamino
                        boxToolBarExtras.add(FormButton.create(excluirMats -> {
                            excluirMats.title("Excluir Materiais");
                            excluirMats.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
                            excluirMats.addStyle("danger");
                            excluirMats.disable.bind(emEdicao.not());
                            excluirMats.setAction(evtExcluirMats -> {
                                finalRiscoPlanejamento.getCores().forEach(it -> {
                                    try {
                                        deleteAllMateriaisRisco(it);
                                        it.getMateriais().clear();
                                        
                                        MessageBox.create(message -> {
                                            message.message("Materiais excluídos do risco.");
                                            message.type(MessageBox.TypeMessageBox.SUCCESS);
                                            message.position(Pos.TOP_RIGHT);
                                            message.notification();
                                        });
                                    } catch (SQLException e) {
                                        e.printStackTrace();
                                        ExceptionBox.build(message -> {
                                            message.exception(e);
                                            message.showAndWait();
                                        });
                                    }
                                });
                            });
                        })); // botão para excluir todos os materiais selecionados do risco
                    })); // botão para seleção de cores do produto (box para colocar um toolbar)
                    boxToolbar.add(FormButton.create(btnGravarRisco -> {
                        btnGravarRisco.title("Salvar Risco");
                        btnGravarRisco.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                        btnGravarRisco.addStyle("success");
                        btnGravarRisco.disable.bind(emEdicao.not());
                        btnGravarRisco.setAction(evt -> salvarRisco(finalRiscoPlanejamento));
                    })); // botão para gravar risco feito
                    boxToolbar.add(FormButton.create(btnCancelarRisco -> {
                        btnCancelarRisco.title("Apagar Risco");
                        btnCancelarRisco.icon(ImageUtils.getIcon(ImageUtils.Icon.TRASH, ImageUtils.IconSize._24));
                        btnCancelarRisco.addStyle("warning");
                        btnCancelarRisco.disable.bind(emEdicao.not());
                        btnCancelarRisco.setAction(evt -> {
                            if ((Boolean) QuestionBox.build(Boolean.class, message -> {
                                message.message("Deseja realmente excluir o risco?");
                                message.showAndWait();
                            }).value.get()) {
                                tabRiscos.getTabs().remove(tab);
                                cancelarRisco(finalRiscoPlanejamento);
                            }
                        });
                    })); // botão que cancela e apaga o risco criado
                })); // box toolbar no cabeçalho da ABA
                tab.add(FormFieldText.create(field -> {
                    field.withoutTitle();
                    field.editable(false);
                    field.alignment(Pos.CENTER);
                    field.addStyle("lg");
                    //field.value.set(descRisco + " - " + tipoRiscoSelecionado.getDescricao() + " (" + aplicacaoRisco.get().getAplicacaoft().getDescricao() + ")");
                    field.value.bind(finalRiscoPlanejamento.getTiporisco().codriscoProperty()
                            .concat(finalRiscoPlanejamento.seqTipoProperty().asString())
                            .concat(new SimpleStringProperty(" - "))
                            .concat(finalRiscoPlanejamento.getTiporisco().descricaoProperty())
                            .concat(new SimpleStringProperty(" ("))
                            .concat(finalRiscoPlanejamento.getAplicacaoft().descricaoProperty())
                            .concat(new SimpleStringProperty(")"))
                            .concat(Bindings.when(finalRiscoPlanejamento.riscoAproProperty()).then("(APRO)").otherwise("")));
                }).build()); // cabeçalho com o tipo do risco na ABA
                tab.add(boxRiscosCores); // box com as informações das cores no risco
            });
            tabRisco.idProperty().bindBidirectional(riscoPlanejamento.seqProperty(), new NumberStringConverter());
            tabRiscos.addTab(tabRisco);
            tabRiscos.getTabs().forEach(tab -> tab.setId(String.valueOf(tabRiscos.getTabs().indexOf(tab) + 1)));
            tabRiscos.getTabs().sort(Comparator.comparing(Tab::getId)); // ordena as ABAS pelo ID
            tabRiscos.getSelectionModel().select(tabRisco); // seleciona a última aba criada
            
            riscoPlanejamento.seqProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue != null) {
                    List<SdRiscoPlanejamentoEncaixe> riscosTipo = planejamentoAberto.getRiscos().stream().filter(it -> it.getAplicacaoft().getCodigo().equals(finalRiscoPlanejamento.getAplicacaoft().getCodigo())).collect(Collectors.toList());
                    riscosTipo.sort(Comparator.comparingInt(SdRiscoPlanejamentoEncaixe::getSeq));
                    riscosTipo.forEach(it -> it.setSeqTipo(riscosTipo.indexOf(it) + 1));
                }
            });
            
            SysLogger.addSysDelizLog("Planejamento Encaixe", TipoAcao.CADASTRAR, planejamentoAberto.getNumero().getNumero(),
                    "Criando risco ID " + riscoPlanejamento.getId()
                            + " (" + riscoPlanejamento.getTiporisco().getCodrisco() + riscoPlanejamento.getSeqTipo() + ") no planejamento ID " + planejamentoAberto.getId());
            //riscos.add(tipoRiscoSelecionado);
        } catch (SQLException e) {
            e.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(e);
                message.showAndWait();
            });
        }
    }

    /**
     * function para carregar os riscos de banco
     */
    private void criarRisco(SdPlanejamentoEncaixe planejamentoAberto) {
        
        planejamentoAberto.getRiscos().sort(Comparator.comparingInt(SdRiscoPlanejamentoEncaixe::getId));
        planejamentoAberto.getRiscos().forEach(risco -> {
            
            // add dos riscos das cores (cabeçalho do risco)
            SdRiscoPlanejamentoEncaixe riscoPlanejamento = risco;
            
            // box para add das cores
            final FormBox boxRiscosCores = FormBox.create(boxCores -> {
                boxCores.vertical();
                boxCores.expanded();
                boxCores.bothScroll();
            });
            
            // field para nome do arquivo diamino (.plx)
            SdRiscoPlanejamentoEncaixe finalRiscoPlanejamento = riscoPlanejamento;
            final FormFieldText fieldArquivoDiamino = FormFieldText.create(field -> {
                field.withoutTitle();
                field.label("Arq. Diamino");
                field.width(300.0);
                field.value.bindBidirectional(finalRiscoPlanejamento.arqdiaminoProperty());
            });
            
            SdTipoRisco tipoRiscoSelecionado = riscoPlanejamento.getTiporisco();
            AtomicReference<Pcpapl> aplicacaoRisco = new AtomicReference<>(riscoPlanejamento.getAplicacaoft());
            // obtendo o material utilizado na aplicação do risco selecionado
            List<VSdMatAplicacaoOf> materialAplicacaoSelecionada = (List<VSdMatAplicacaoOf>) new FluentDao().selectFrom(VSdMatAplicacaoOf.class)
                    .where(it -> it
                            .equal("numero", planejamentoAberto.getNumero().getNumero())
                            .equal("aplicacaoft.codigo", aplicacaoRisco.get().getCodigo()))
                    .resultList();
            
            // criação da aba do risco
            FormTab tabRisco = FormTab.create(tab -> {
                tab.title(finalRiscoPlanejamento.getTiporisco().getDescricao());
                tab.setGraphic(FormFieldText.create(field -> {
                    field.withoutTitle();
                    field.editable(false);
                    field.focusTraversable(false);
                    field.addStyle("xs").addStyle(finalRiscoPlanejamento.getTiporisco().getColor());
                    field.width(50.0);
                    field.alignment(Pos.CENTER);
                    field.value.bind(finalRiscoPlanejamento.getTiporisco().codriscoProperty().concat(finalRiscoPlanejamento.seqTipoProperty().asString()));
                }).build());
                tab.closable();
                tab.setOnCloseRequest(evt -> {
                    if ((Boolean) QuestionBox.build(Boolean.class, message -> {
                        message.message("Deseja realmente excluir o risco?");
                        message.showAndWait();
                    }).value.get()) {
                        cancelarRisco(finalRiscoPlanejamento);
                    } else {
                        evt.consume();
                    }
                });
                tab.add(FormBox.create(boxToolbar -> {
                    boxToolbar.horizontal();
                    boxToolbar.alignment(Pos.TOP_RIGHT);
                    boxToolbar.add(FormBox.create(boxToolBarExtras -> {
                        boxToolBarExtras.horizontal();
                        boxToolBarExtras.expanded();
                        boxToolBarExtras.add(FormButton.create(selectCores -> {
                            selectCores.title("Selecionar Cores");
                            selectCores.addStyle("info");
                            selectCores.icon(ImageUtils.getIcon(ImageUtils.Icon.COR, ImageUtils.IconSize._24));
                            selectCores.setAction(evt -> {
                                new Fragment().show(fragment -> {
                                    fragment.size(250.0, 300.0);
                                    
                                    final StringProperty stringCoresAgrupadas = new SimpleStringProperty(null); // string é criada no momento do teste se os riscos poderão ser agrupados
                                    final StringProperty idsCoresAgrupadas = new SimpleStringProperty(null); // string é criada no momento do teste se os riscos poderão ser agrupados
                                    
                                    saldoGrade.clear();
                                    saldoGrade.addAll(ofCarregada.getItens());
                                    final CheckListView<VSdCorOf> checkListCores = new CheckListView<VSdCorOf>();
                                    final CheckListView<SdRiscoPlanejamentoEncaixe> checkListRiscos = new CheckListView<SdRiscoPlanejamentoEncaixe>();
                                    final FormFieldToggleSingle toggleIsAgrupado = FormFieldToggleSingle.create(field -> {
                                        field.value.set(false);
                                        field.title("Agrupar Seleção?");
                                    });
                                    
                                    // teste para verificar se o risco é corpo, detalhe/ribana ou não e criação dos dados dos lists e tela
                                    // * caso o risco é de corpo, seleciona as cores da OF
                                    // * caso o risco é do tipo detalhe/ribana, seleciona os riscos do corpo realizado
                                    // * caso o risco não seja um dos acima, abre as cores da OF para selecionar, porém as qtdes serão do corpo do produto
                                    if (tipoRiscoSelecionado.getCodigo() == 1) {
                                        final FormFieldToggleSingle riscoAproveitamento = FormFieldToggleSingle.create(field -> {
                                            field.value.set(false);
                                            field.title("Risco de Aproveitamento?");
                                            field.value.addListener((observable, oldValue, newValue) -> {
                                                if (newValue) {
                                                    saldoGrade.clear();
                                                    saldoGrade.addAll(ofCarregada.getItens());
                                                    checkListCores.setItems(saldoGrade);
                                                    checkListCores.refresh();
                                                } else {
                                                    saldoGrade.removeIf(it -> it.getQtde().compareTo(new BigDecimal(planejamentoAberto.getRiscos().stream()
                                                            .filter(it2 -> it2.getAplicacaoft().getCodigo() == aplicacaoRisco.get().getCodigo())
                                                            .mapToInt(it2 -> it2.getCores().stream()
                                                                    .filter(it3 -> it3.getCor().equals(it.getId().getCor().getCor()))
                                                                    .mapToInt(it3 -> it3.getGrade().stream()
                                                                            .mapToInt(it4 -> it4.getRealizado())
                                                                            .sum())
                                                                    .sum())
                                                            .sum())) <= 0);
                                                    checkListCores.setItems(saldoGrade);
                                                    checkListCores.refresh();
                                                }
                                                finalRiscoPlanejamento.setRiscoApro(newValue);
                                            });
                                        });
                                        fragment.box.getChildren().addAll(riscoAproveitamento.build());
                                        fragment.title("Selecionar Cores do Produto");
                                        // remove da lista de cores as cores que foram todas planejadas em riscos.
                                        if (riscoAproveitamento.value.not().get())
                                            saldoGrade.removeIf(it -> it.getQtde().compareTo(new BigDecimal(planejamentoAberto.getRiscos().stream()
                                                    .filter(it2 -> it2.getAplicacaoft().getCodigo() == aplicacaoRisco.get().getCodigo())
                                                    .mapToInt(it2 -> it2.getCores().stream()
                                                            .filter(it3 -> it3.getCor().equals(it.getId().getCor().getCor()))
                                                            .mapToInt(it3 -> it3.getGrade().stream()
                                                                    .mapToInt(it4 -> it4.getRealizado())
                                                                    .sum())
                                                            .sum())
                                                    .sum())) <= 0);
                                        checkListCores.setItems(saldoGrade);
                                        fragment.box.getChildren().addAll(checkListCores, toggleIsAgrupado.build());
                                    } else if (tipoRiscoSelecionado.getCodigo() == 3 || tipoRiscoSelecionado.getCodigo() == 5) {
                                        fragment.title("Selecionar Riscos do Produto");
                                        List<SdRiscoPlanejamentoEncaixe> riscosCorpo = planejamentoAberto.getRiscos().stream().filter(it -> it.getTiporisco().getCodigo() == 1).collect(Collectors.toList());
                                        riscosCorpo.forEach(it -> it.setSelected(false));
                                        // remove da lista de cores as cores que foram todas planejadas em riscos.
                                        riscosCorpo.removeIf(it -> {
                                            int qtdeRiscoCorpo = it.getCores().stream().mapToInt(SdCorRisco::getTotalRealizado).sum();
                                            int qtdeRealizadoRisco = planejamentoAberto.getRiscos().stream()
                                                    .filter(it2 -> it2.getAplicacaoft().getCodigo() == aplicacaoRisco.get().getCodigo())// && it2.getCores().stream().anyMatch(it3 -> it3.getRiscoCorpo().equals(it)))
                                                    .mapToInt(it2 -> it2.getCores().stream()
                                                            .filter(it3 -> it3.getRiscoCorpo().stream().anyMatch(it4 -> it.getCores().contains(it4)) && !it3.isCorAgrupada())
                                                            .mapToInt(it3 -> it3.getTotalRealizado())
                                                            .sum())
                                                    .sum();
                                            return qtdeRiscoCorpo <= qtdeRealizadoRisco;
                                        });
                                        checkListRiscos.setItems(FXCollections.observableList(riscosCorpo));
                                        checkListRiscos.setCellFactory(param -> {
                                            return new CheckBoxListCell<SdRiscoPlanejamentoEncaixe>(SdRiscoPlanejamentoEncaixe::selectedProperty) {
                                                @Override
                                                public void updateItem(SdRiscoPlanejamentoEncaixe item, boolean empty) {
                                                    super.updateItem(item, empty);
                                                    setText(null);
                                                    if (item != null && !empty) {
                                                        setText(item.getTiporisco().getCodrisco() + item.getSeq() + " - Cores: " + item.getCores().stream().map(it -> it.getCor()).collect(Collectors.joining(", ")));
                                                    }
                                                }
                                            };
                                        });
                                        fragment.box.getChildren().addAll(checkListRiscos, toggleIsAgrupado.build());
                                    } else {
                                        fragment.title("Selecionar Cores do Produto");
                                        // remove da lista de cores as cores que foram todas planejadas em riscos.
                                        saldoGrade.removeIf(it -> {
                                            BigDecimal qtdeParaRealizar = new BigDecimal(planejamentoAberto.getRiscos().stream()
                                                    .filter(it2 -> it2.getTiporisco().getCodigo() == 1)
                                                    .mapToInt(it2 -> it2.getCores().stream()
                                                            .filter(it3 -> it3.getCor().equals(it.getId().getCor().getCor()))
                                                            .mapToInt(it3 -> it3.getGrade().stream()
                                                                    .mapToInt(it4 -> it4.getRealizado())
                                                                    .sum())
                                                            .sum())
                                                    .sum());
                                            BigDecimal qtdeRealizado = new BigDecimal(planejamentoAberto.getRiscos().stream()
                                                    .filter(it2 -> it2.getAplicacaoft().getCodigo() == aplicacaoRisco.get().getCodigo())
                                                    .mapToInt(it2 -> it2.getCores().stream()
                                                            .filter(it3 -> it3.getCor().equals(it.getId().getCor().getCor()))
                                                            .mapToInt(it3 -> it3.getGrade().stream()
                                                                    .mapToInt(it4 -> it4.getRealizado())
                                                                    .sum())
                                                            .sum())
                                                    .sum());
                                            return qtdeParaRealizar.compareTo(qtdeRealizado) <= 0;
                                        });
                                        checkListCores.setItems(saldoGrade);
                                        fragment.box.getChildren().addAll(checkListCores, toggleIsAgrupado.build());
                                    }
                                    
                                    fragment.buttonsBox.getChildren().add(FormButton.create(btnSelecionarCores -> {
                                        btnSelecionarCores.title("Selecionar Cores");
                                        btnSelecionarCores.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._16));
                                        btnSelecionarCores.addStyle("success");
                                        btnSelecionarCores.setAction(evt2 -> {
                                            if (tipoRiscoSelecionado.getCodigo() == 1) {
                                                List<VSdCorOf> coresSelecionadas = checkListCores.getCheckModel().getCheckedItems();
                                                finalRiscoPlanejamento.getCores().clear(); // limpa as cores do risco, caso esteja alterando as cores
                                                
                                                if (coresSelecionadas.size() > 0) {
                                                    // fazer um teste caso o usuário selecione agrupar seleção
                                                    // se ele colocar agrupar seleção e na aplicação do risco, tiver dois materiais diferentes não poderá ser agrupado, o sistema irá montar os subriscos (1.1,1.2)
                                                    if (toggleIsAgrupado.value.get() && coresSelecionadas.size() > 1) {
                                                        Long qtdeMateriaisCores = materialAplicacaoSelecionada.stream()
                                                                .map(it -> "MAT-" + it.getInsumo().getCodigo() + "COR-" + it.getCori().getCor())
                                                                .distinct()
                                                                .count();
                                                        if (qtdeMateriaisCores > 1) {
                                                            toggleIsAgrupado.value.set(false);
                                                        } else {
                                                            stringCoresAgrupadas.set(coresSelecionadas.stream().map(it -> it.getId().getCor().getCor()).distinct().collect(Collectors.joining("','")));
                                                        }
                                                    }
                                                    
                                                    // verifica se o risco é com cores agrupadas separando as cores e criando os subriscos
                                                    if (toggleIsAgrupado.value.not().get()) {
                                                        // risco com cores não agrupadas
                                                        coresSelecionadas.forEach(corRisco -> {
                                                            // cria o objeto da cor do risco e alimenta os dados
                                                            SdCorRisco riscoCor = new SdCorRisco();
                                                            riscoCor.setIdrisco(finalRiscoPlanejamento.getId());
                                                            riscoCor.setCor(corRisco.getId().getCor().getCor());
                                                            riscoCor.setComprimento(BigDecimal.ZERO);
                                                            riscoCor.setConsumopeca(BigDecimal.ZERO);
                                                            riscoCor.setConsumototal(BigDecimal.ZERO);
                                                            riscoCor.setFolhas(0);
                                                            riscoCor.setSeq(coresSelecionadas.indexOf(corRisco) + 1);
                                                            //riscoCor.setDescrisco(descRisco + "." + (coresSelecionadas.indexOf(corRisco) + 1));
                                                            try {
                                                                // persistindo a cor-risco no banco
                                                                riscoCor = new FluentDao().persist(riscoCor);
                                                                finalRiscoPlanejamento.getCores().add(riscoCor);
                                                                
                                                                // criação da grade de tamanhos para a cor do risco
                                                                SdCorRisco finalRiscoCor = riscoCor;
                                                                corRisco.getTams().forEach(tam -> {
                                                                    // criando o objeto do tamanho e alimentado com os dados
                                                                    SdGradeRiscoCor tamanhosRiscoCor = new SdGradeRiscoCor();
                                                                    tamanhosRiscoCor.setIdrisco(finalRiscoCor.getId());
                                                                    tamanhosRiscoCor.setCor(corRisco.getId().getCor().getCor());
                                                                    tamanhosRiscoCor.setTam(tam.getId().getTam());
                                                                    tamanhosRiscoCor.setGrade(0);
                                                                    tamanhosRiscoCor.setParte(0);
                                                                    tamanhosRiscoCor.setRealizado(0);
                                                                    tamanhosRiscoCor.setSaldoOf(tam.getQtdeof().subtract(new BigDecimal(planejamentoAberto.getRiscos().stream()
                                                                            .filter(it -> it.getAplicacaoft().getCodigo() == aplicacaoRisco.get().getCodigo())
                                                                            .mapToInt(it -> it.getCores().stream().mapToInt(it2 -> it2.getGrade().stream()
                                                                                    .filter(it3 -> it3.getCor().equals(corRisco.getId().getCor().getCor()) && it3.getTam().equals(tam.getId().getTam()))
                                                                                    .mapToInt(it3 -> it3.getRealizado())
                                                                                    .sum()).sum())
                                                                            .sum())).intValue());
                                                                    try {
                                                                        tamanhosRiscoCor = new FluentDao().persist(tamanhosRiscoCor);
                                                                        finalRiscoCor.getGrade().add(tamanhosRiscoCor);
                                                                    } catch (SQLException e) {
                                                                        e.printStackTrace();
                                                                        ExceptionBox.build(message -> {
                                                                            message.exception(e);
                                                                            message.showAndWait();
                                                                        });
                                                                    }
                                                                });
                                                                // atualizando os totais da cor conforme o cadastro
                                                                riscoCor.setTotalSaldoOf(riscoCor.getGrade().stream().mapToInt(tam -> tam.getSaldoOf()).sum());
                                                            } catch (SQLException e) {
                                                                e.printStackTrace();
                                                                ExceptionBox.build(message -> {
                                                                    message.exception(e);
                                                                    message.showAndWait();
                                                                });
                                                            }
                                                        });
                                                    } else {
                                                        SdCorRisco riscoCor = new SdCorRisco();
                                                        riscoCor.setIdrisco(finalRiscoPlanejamento.getId());
                                                        riscoCor.setCor(stringCoresAgrupadas.get());
                                                        riscoCor.setComprimento(BigDecimal.ZERO);
                                                        riscoCor.setConsumopeca(BigDecimal.ZERO);
                                                        riscoCor.setConsumototal(BigDecimal.ZERO);
                                                        riscoCor.setFolhas(0);
                                                        riscoCor.setSeq(1);
                                                        //riscoCor.setDescrisco(descRisco + "." + 1);
                                                        try {
                                                            riscoCor = new FluentDao().persist(riscoCor);
                                                            finalRiscoPlanejamento.getCores().add(riscoCor);
                                                        } catch (SQLException e) {
                                                            e.printStackTrace();
                                                            ExceptionBox.build(message -> {
                                                                message.exception(e);
                                                                message.showAndWait();
                                                            });
                                                        }
                                                        
                                                        SdCorRisco finalRiscoCor = riscoCor;
                                                        coresSelecionadas.get(0).getTams().forEach(tam -> {
                                                            SdGradeRiscoCor tamanhosRiscoCor = new SdGradeRiscoCor();
                                                            tamanhosRiscoCor.setIdrisco(finalRiscoCor.getId());
                                                            tamanhosRiscoCor.setCor(null);
                                                            tamanhosRiscoCor.setTam(tam.getId().getTam());
                                                            tamanhosRiscoCor.setGrade(0);
                                                            tamanhosRiscoCor.setParte(0);
                                                            tamanhosRiscoCor.setRealizado(0);
                                                            tamanhosRiscoCor.setSaldoOf(tam.getQtdeof().subtract(new BigDecimal(planejamentoAberto.getRiscos().stream()
                                                                    .filter(it -> it.getAplicacaoft().getCodigo() == aplicacaoRisco.get().getCodigo())
                                                                    .mapToInt(it -> it.getCores().stream()
                                                                            .mapToInt(it2 -> it2.getGrade().stream()
                                                                                    .filter(it3 -> it3.getTam().equals(tam.getId().getTam()))
                                                                                    .mapToInt(it3 -> it3.getRealizado())
                                                                                    .sum())
                                                                            .sum())
                                                                    .sum())).intValue());
                                                            try {
                                                                tamanhosRiscoCor = new FluentDao().persist(tamanhosRiscoCor);
                                                                finalRiscoCor.getGrade().add(tamanhosRiscoCor);
                                                            } catch (SQLException e) {
                                                                e.printStackTrace();
                                                                ExceptionBox.build(message -> {
                                                                    message.exception(e);
                                                                    message.showAndWait();
                                                                });
                                                            }
                                                        });
                                                        riscoCor.setTotalSaldoOf(riscoCor.getGrade().stream().mapToInt(tam -> tam.getSaldoOf()).sum());
                                                    }
                                                }
                                            } else if (tipoRiscoSelecionado.getCodigo() == 3 || tipoRiscoSelecionado.getCodigo() == 5) {
                                                List<SdRiscoPlanejamentoEncaixe> riscosSelecionados = checkListRiscos.getItems().filtered(it -> it.isSelected());
                                                finalRiscoPlanejamento.getCores().clear(); // limpa as cores do risco, caso esteja alterando as cores
                                                
                                                if (riscosSelecionados.size() > 0) {
                                                    // fazer um teste caso o usuário selecione agrupar seleção
                                                    // se ele colocar agrupar seleção e na aplicação do risco, tiver dois materiais diferentes não poderá ser agrupado, o sistema irá montar os subriscos (1.1,1.2)
                                                    if (toggleIsAgrupado.value.get() && riscosSelecionados.size() > 1) {
                                                        Long qtdeMateriaisCores = materialAplicacaoSelecionada.stream()
                                                                .map(it -> "MAT-" + it.getInsumo().getCodigo() + "COR-" + it.getCori().getCor())
                                                                .distinct()
                                                                .count();
                                                        Set<String> coresSelecao = new HashSet<>();
                                                        riscosSelecionados.forEach(it -> it.getCores().forEach(it2 -> coresSelecao.add(it2.getCor())));
                                                        
                                                        if (qtdeMateriaisCores > 1 && coresSelecao.size() > 1) {
                                                            toggleIsAgrupado.value.set(false);
                                                        } else {
                                                            stringCoresAgrupadas.set(riscosSelecionados.stream().map(it -> it.getCores().stream().map(SdCorRisco::getCor).distinct().collect(Collectors.joining("','"))).distinct().collect(Collectors.joining("','")));
                                                            idsCoresAgrupadas.set(riscosSelecionados.stream().map(it -> it.getCores().stream().map(it2 -> String.valueOf(it2.getId())).distinct().collect(Collectors.joining(","))).distinct().collect(Collectors.joining(",")));
                                                        }
                                                    }
                                                    
                                                    // verifica se o risco é com cores agrupadas separando as cores e criando os subriscos
                                                    if (toggleIsAgrupado.value.not().get()) {
                                                        riscosSelecionados.forEach(riscoCorpo -> {
                                                            riscoCorpo.getCores().forEach(it -> {
                                                                SdCorRisco riscoCor = new SdCorRisco();
                                                                riscoCor.setCor(String.valueOf(it.getId()));
                                                                riscoCor.setComprimento(BigDecimal.ZERO);
                                                                riscoCor.setConsumopeca(BigDecimal.ZERO);
                                                                riscoCor.setConsumototal(BigDecimal.ZERO);
                                                                riscoCor.setFolhas(0);
                                                                riscoCor.setSeq(riscosSelecionados.indexOf(riscoCorpo) + 1);
                                                                riscoCor.setIdrisco(finalRiscoPlanejamento.getId());
                                                                //riscoCor.setDescrisco(descRisco + "." + (riscosSelecionados.indexOf(riscoCorpo) + 1));
                                                                riscoCor.getRiscoCorpo().add(it);
                                                                try {
                                                                    riscoCor = new FluentDao().persist(riscoCor);
                                                                    finalRiscoPlanejamento.getCores().add(riscoCor);
                                                                } catch (SQLException e) {
                                                                    e.printStackTrace();
                                                                    ExceptionBox.build(message -> {
                                                                        message.exception(e);
                                                                        message.showAndWait();
                                                                    });
                                                                }
                                                                
                                                                SdCorRisco finalRiscoCor = riscoCor;
                                                                it.getGrade().forEach(tam -> {
                                                                    SdGradeRiscoCor tamanhosRiscoCor = new SdGradeRiscoCor();
                                                                    tamanhosRiscoCor.setIdrisco(finalRiscoCor.getId());
                                                                    tamanhosRiscoCor.setCor(it.getCor());
                                                                    tamanhosRiscoCor.setTam(tam.getTam());
                                                                    tamanhosRiscoCor.setGrade(0);
                                                                    tamanhosRiscoCor.setParte(0);
                                                                    tamanhosRiscoCor.setRealizado(0);
                                                                    tamanhosRiscoCor.setSaldoOf(tam.getRealizado() - planejamentoAberto.getRiscos().stream()
                                                                            .filter(it2 -> it2.getAplicacaoft().getCodigo() == aplicacaoRisco.get().getCodigo())
                                                                            .mapToInt(it2 -> it2.getCores().stream()
                                                                                    .filter(it3 -> it3.getRiscoCorpo().stream().anyMatch(it4 -> it4.equals(it)) && (it3.getCor().equals(it.getCor())))
                                                                                    .mapToInt(it3 -> it3.getGrade().stream()
                                                                                            .filter(it4 -> it4.getTam().equals(tam.getTam()))
                                                                                            .mapToInt(SdGradeRiscoCor::getRealizado)
                                                                                            .sum())
                                                                                    .sum())
                                                                            .sum());
                                                                    try {
                                                                        tamanhosRiscoCor = new FluentDao().persist(tamanhosRiscoCor);
                                                                        finalRiscoCor.getGrade().add(tamanhosRiscoCor);
                                                                    } catch (SQLException e) {
                                                                        e.printStackTrace();
                                                                        ExceptionBox.build(message -> {
                                                                            message.exception(e);
                                                                            message.showAndWait();
                                                                        });
                                                                    }
                                                                });
                                                                riscoCor.setTotalSaldoOf(riscoCor.getGrade().stream().mapToInt(tam -> tam.getSaldoOf()).sum());
                                                            });
                                                        });
                                                    } else {
                                                        
                                                        SdCorRisco riscoCor = new SdCorRisco();
                                                        riscoCor.setCor(idsCoresAgrupadas.get());
                                                        riscoCor.setCorAgrupada(true);
                                                        riscoCor.setComprimento(BigDecimal.ZERO);
                                                        riscoCor.setConsumopeca(BigDecimal.ZERO);
                                                        riscoCor.setConsumototal(BigDecimal.ZERO);
                                                        riscoCor.setFolhas(0);
                                                        riscoCor.setSeq(riscoCor.getSeq());
                                                        riscoCor.setIdrisco(finalRiscoPlanejamento.getId());
                                                        //riscoCor.setDescrisco(descRisco + "." + 1);
                                                        SdCorRisco finalRiscoCor1 = riscoCor;
                                                        riscosSelecionados.forEach(it -> finalRiscoCor1.getRiscoCorpo().addAll(it.getCores()));
                                                        try {
                                                            riscoCor = new FluentDao().persist(riscoCor);
                                                            finalRiscoPlanejamento.getCores().add(riscoCor);
                                                        } catch (SQLException e) {
                                                            e.printStackTrace();
                                                            ExceptionBox.build(message -> {
                                                                message.exception(e);
                                                                message.showAndWait();
                                                            });
                                                        }
                                                        
                                                        SdCorRisco finalRiscoCor = riscoCor;
                                                        riscosSelecionados.get(0).getCores().get(0).getGrade().forEach(it -> {
                                                            SdGradeRiscoCor tamanhosRiscoCor = new SdGradeRiscoCor();
                                                            tamanhosRiscoCor.setIdrisco(finalRiscoCor.getId());
                                                            tamanhosRiscoCor.setCor(null);
                                                            tamanhosRiscoCor.setTam(it.getTam());
                                                            tamanhosRiscoCor.setGrade(0);
                                                            tamanhosRiscoCor.setParte(0);
                                                            tamanhosRiscoCor.setRealizado(0);
                                                            tamanhosRiscoCor.setSaldoOf(riscosSelecionados.stream()
                                                                    .mapToInt(it2 -> it2.getCores().stream()
                                                                            .mapToInt(it3 -> it3.getGrade().stream()
                                                                                    .filter(it4 -> it4.getTam().equals(it.getTam()))
                                                                                    .mapToInt(SdGradeRiscoCor::getRealizado)
                                                                                    .sum())
                                                                            .sum())
                                                                    .sum() - planejamentoAberto.getRiscos().stream()
                                                                    .filter(it2 -> it2.getAplicacaoft().getCodigo() == aplicacaoRisco.get().getCodigo())
                                                                    .mapToInt(it2 -> it2.getCores().stream()
                                                                            .mapToInt(it3 -> it3.getGrade().stream()
                                                                                    .filter(it4 -> it4.getTam().equals(it.getTam()))
                                                                                    .mapToInt(SdGradeRiscoCor::getRealizado)
                                                                                    .sum())
                                                                            .sum())
                                                                    .sum());
                                                            try {
                                                                tamanhosRiscoCor = new FluentDao().persist(tamanhosRiscoCor);
                                                                finalRiscoCor.getGrade().add(tamanhosRiscoCor);
                                                            } catch (SQLException e) {
                                                                e.printStackTrace();
                                                                ExceptionBox.build(message -> {
                                                                    message.exception(e);
                                                                    message.showAndWait();
                                                                });
                                                            }
                                                        });
                                                        riscoCor.setTotalSaldoOf(riscoCor.getGrade().stream().mapToInt(tam -> tam.getSaldoOf()).sum());
                                                    }
                                                }
                                            } else {
                                                List<VSdCorOf> coresSelecionadas = checkListCores.getCheckModel().getCheckedItems();
                                                finalRiscoPlanejamento.getCores().clear(); // limpa as cores do risco, caso esteja alterando as cores
                                                
                                                if (coresSelecionadas.size() > 0) {
                                                    // fazer um teste caso o usuário selecione agrupar seleção
                                                    // se ele colocar agrupar seleção e na aplicação do risco, tiver dois materiais diferentes não poderá ser agrupado, o sistema irá montar os subriscos (1.1,1.2)
                                                    if (toggleIsAgrupado.value.get() && coresSelecionadas.size() > 1) {
                                                        Long qtdeMateriaisCores = materialAplicacaoSelecionada.stream()
                                                                .map(it -> "MAT-" + it.getInsumo().getCodigo() + "COR-" + it.getCori().getCor())
                                                                .distinct()
                                                                .count();
                                                        if (qtdeMateriaisCores > 1) {
                                                            toggleIsAgrupado.value.set(false);
                                                        } else {
                                                            stringCoresAgrupadas.set(coresSelecionadas.stream().map(it -> it.getId().getCor().getCor()).distinct().collect(Collectors.joining("','")));
                                                        }
                                                    }
                                                    
                                                    // verifica se o risco é com cores agrupadas separando as cores e criando os subriscos
                                                    if (toggleIsAgrupado.value.not().get()) {
                                                        coresSelecionadas.forEach(corRisco -> {
                                                            SdCorRisco riscoCor = new SdCorRisco();
                                                            riscoCor.setIdrisco(finalRiscoPlanejamento.getId());
                                                            riscoCor.setCor(corRisco.getId().getCor().getCor());
                                                            riscoCor.setComprimento(BigDecimal.ZERO);
                                                            riscoCor.setConsumopeca(BigDecimal.ZERO);
                                                            riscoCor.setConsumototal(BigDecimal.ZERO);
                                                            riscoCor.setFolhas(0);
                                                            riscoCor.setSeq(coresSelecionadas.indexOf(corRisco) + 1);
                                                            //riscoCor.setDescrisco(descRisco + "." + (coresSelecionadas.indexOf(corRisco) + 1));
                                                            try {
                                                                riscoCor = new FluentDao().persist(riscoCor);
                                                                finalRiscoPlanejamento.getCores().add(riscoCor);
                                                                
                                                                SdCorRisco finalRiscoCor = riscoCor;
                                                                corRisco.getTams().forEach(tam -> {
                                                                    SdGradeRiscoCor tamanhosRiscoCor = new SdGradeRiscoCor();
                                                                    tamanhosRiscoCor.setIdrisco(finalRiscoCor.getId());
                                                                    tamanhosRiscoCor.setCor(corRisco.getId().getCor().getCor());
                                                                    tamanhosRiscoCor.setTam(tam.getId().getTam());
                                                                    tamanhosRiscoCor.setGrade(0);
                                                                    tamanhosRiscoCor.setParte(0);
                                                                    tamanhosRiscoCor.setRealizado(0);
                                                                    tamanhosRiscoCor.setSaldoOf(new BigDecimal(planejamentoAberto.getRiscos().stream()
                                                                            .filter(it -> it.getTiporisco().getCodigo() == 1)
                                                                            .mapToInt(it -> it.getCores().stream()
                                                                                    .filter(it2 -> it2.getCor().equals(finalRiscoCor.getCor()))
                                                                                    .mapToInt(it2 -> it2.getGrade().stream()
                                                                                            .filter(it3 -> it3.getTam().equals(tam.getId().getTam()))
                                                                                            .mapToInt(it3 -> it3.getRealizado())
                                                                                            .sum())
                                                                                    .sum())
                                                                            .sum())
                                                                            .subtract(new BigDecimal(planejamentoAberto.getRiscos().stream()
                                                                                    .filter(it -> it.getAplicacaoft().getCodigo() == aplicacaoRisco.get().getCodigo())
                                                                                    .mapToInt(it -> it.getCores().stream().mapToInt(it2 -> it2.getGrade().stream()
                                                                                            .filter(it3 -> it3.getCor().equals(corRisco.getId().getCor().getCor()) && it3.getTam().equals(tam.getId().getTam()))
                                                                                            .mapToInt(it3 -> it3.getRealizado())
                                                                                            .sum()).sum())
                                                                                    .sum())).intValue());
                                                                    try {
                                                                        tamanhosRiscoCor = new FluentDao().persist(tamanhosRiscoCor);
                                                                        finalRiscoCor.getGrade().add(tamanhosRiscoCor);
                                                                    } catch (SQLException e) {
                                                                        e.printStackTrace();
                                                                        ExceptionBox.build(message -> {
                                                                            message.exception(e);
                                                                            message.showAndWait();
                                                                        });
                                                                    }
                                                                });
                                                                riscoCor.setTotalSaldoOf(riscoCor.getGrade().stream().mapToInt(tam -> tam.getSaldoOf()).sum());
                                                            } catch (SQLException e) {
                                                                e.printStackTrace();
                                                                ExceptionBox.build(message -> {
                                                                    message.exception(e);
                                                                    message.showAndWait();
                                                                });
                                                            }
                                                        });
                                                    } else {
                                                        SdCorRisco riscoCor = new SdCorRisco();
                                                        riscoCor.setIdrisco(finalRiscoPlanejamento.getId());
                                                        riscoCor.setCor(stringCoresAgrupadas.get());
                                                        riscoCor.setComprimento(BigDecimal.ZERO);
                                                        riscoCor.setConsumopeca(BigDecimal.ZERO);
                                                        riscoCor.setConsumototal(BigDecimal.ZERO);
                                                        riscoCor.setFolhas(0);
                                                        riscoCor.setSeq(1);
                                                        //riscoCor.setDescrisco(descRisco + "." + 1);
                                                        try {
                                                            riscoCor = new FluentDao().persist(riscoCor);
                                                            finalRiscoPlanejamento.getCores().add(riscoCor);
                                                        } catch (SQLException e) {
                                                            e.printStackTrace();
                                                            ExceptionBox.build(message -> {
                                                                message.exception(e);
                                                                message.showAndWait();
                                                            });
                                                        }
                                                        
                                                        SdCorRisco finalRiscoCor = riscoCor;
                                                        coresSelecionadas.get(0).getTams().forEach(tam -> {
                                                            SdGradeRiscoCor tamanhosRiscoCor = new SdGradeRiscoCor();
                                                            tamanhosRiscoCor.setIdrisco(finalRiscoCor.getId());
                                                            tamanhosRiscoCor.setCor(null);
                                                            tamanhosRiscoCor.setTam(tam.getId().getTam());
                                                            tamanhosRiscoCor.setGrade(0);
                                                            tamanhosRiscoCor.setParte(0);
                                                            tamanhosRiscoCor.setRealizado(0);
                                                            tamanhosRiscoCor.setSaldoOf(new BigDecimal(planejamentoAberto.getRiscos().stream()
                                                                    .filter(it -> it.getTiporisco().getCodigo() == 1)
                                                                    .mapToInt(it -> it.getCores().stream()
                                                                            .mapToInt(it2 -> it2.getGrade().stream()
                                                                                    .filter(it3 -> it3.getTam().equals(tam.getId().getTam()))
                                                                                    .mapToInt(it3 -> it3.getRealizado())
                                                                                    .sum())
                                                                            .sum())
                                                                    .sum())
                                                                    .subtract(new BigDecimal(planejamentoAberto.getRiscos().stream()
                                                                            .filter(it -> it.getAplicacaoft().getCodigo() == aplicacaoRisco.get().getCodigo())
                                                                            .mapToInt(it -> it.getCores().stream().mapToInt(it2 -> it2.getGrade().stream()
                                                                                    .filter(it3 -> it3.getTam().equals(tam.getId().getTam()))
                                                                                    .mapToInt(it3 -> it3.getRealizado())
                                                                                    .sum()).sum())
                                                                            .sum())).intValue());
                                                            try {
                                                                tamanhosRiscoCor = new FluentDao().persist(tamanhosRiscoCor);
                                                                finalRiscoCor.getGrade().add(tamanhosRiscoCor);
                                                            } catch (SQLException e) {
                                                                e.printStackTrace();
                                                                ExceptionBox.build(message -> {
                                                                    message.exception(e);
                                                                    message.showAndWait();
                                                                });
                                                            }
                                                        });
                                                        riscoCor.setTotalSaldoOf(riscoCor.getGrade().stream().mapToInt(tam -> tam.getSaldoOf()).sum());
                                                    }
                                                }
                                            }
                                            
                                            finalRiscoPlanejamento.setStringCoresAgrupadas(stringCoresAgrupadas.get());
                                            finalRiscoPlanejamento.setIdsCoresAgrupadas(idsCoresAgrupadas.get());
                                            criarBoxRiscos(boxRiscosCores, finalRiscoPlanejamento);
                                            fragment.close();
                                            SysLogger.addSysDelizLog("Planejamento Encaixe", TipoAcao.EDITAR, planejamentoAberto.getNumero().getNumero(),
                                                    "Adicionando cores no risco ID " + finalRiscoPlanejamento.getId()
                                                            + " (" + finalRiscoPlanejamento.getTiporisco().getCodrisco() + finalRiscoPlanejamento.getSeqTipo() + ") no planejamento ID " + planejamentoAberto.getId());
                                        });
                                    })); // botão da seleção de cores do produto para criar os riscos
                                });
                            });
                        })); // botão com a seleção de cores
                        boxToolBarExtras.add(fieldArquivoDiamino.build()); // field com o nome do arquivo diamino
                        boxToolBarExtras.add(FormButton.create(excluirMats -> {
                            excluirMats.title("Excluir Materiais");
                            excluirMats.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
                            excluirMats.addStyle("danger");
                            excluirMats.disable.bind(emEdicao.not());
                            excluirMats.setAction(evtExcluirMats -> {
                                finalRiscoPlanejamento.getCores().forEach(it -> {
                                    try {
                                        deleteAllMateriaisRisco(it);
                                        it.getMateriais().clear();
                                        
                                        MessageBox.create(message -> {
                                            message.message("Materiais excluídos do risco.");
                                            message.type(MessageBox.TypeMessageBox.SUCCESS);
                                            message.position(Pos.TOP_RIGHT);
                                            message.notification();
                                        });
                                    } catch (SQLException e) {
                                        e.printStackTrace();
                                        ExceptionBox.build(message -> {
                                            message.exception(e);
                                            message.showAndWait();
                                        });
                                    }
                                });
                            });
                        })); // botão para excluir todos os materiais selecionados do risco
                    })); // botão para seleção de cores do produto (box para colocar um toolbar)
                    boxToolbar.add(FormButton.create(btnGravarRisco -> {
                        btnGravarRisco.title("Salvar Risco");
                        btnGravarRisco.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                        btnGravarRisco.addStyle("success");
                        btnGravarRisco.disable.bind(emEdicao.not());
                        btnGravarRisco.setAction(evt -> salvarRisco(finalRiscoPlanejamento));
                    })); // botão para gravar risco feito
                    boxToolbar.add(FormButton.create(btnCancelarRisco -> {
                        btnCancelarRisco.title("Apagar Risco");
                        btnCancelarRisco.icon(ImageUtils.getIcon(ImageUtils.Icon.TRASH, ImageUtils.IconSize._24));
                        btnCancelarRisco.addStyle("warning");
                        btnCancelarRisco.disable.bind(emEdicao.not());
                        btnCancelarRisco.setAction(evt -> {
                            if ((Boolean) QuestionBox.build(Boolean.class, message -> {
                                message.message("Deseja realmente excluir o risco?");
                                message.showAndWait();
                            }).value.get()) {
                                tabRiscos.getTabs().remove(tab);
                                cancelarRisco(finalRiscoPlanejamento);
                            }
                        });
                    })); // botão que cancela e apaga o risco criado
                })); // box toolbar no cabeçalho da ABA
                tab.add(FormFieldText.create(field -> {
                    field.withoutTitle();
                    field.editable(false);
                    field.alignment(Pos.CENTER);
                    field.addStyle("lg");
                    field.value.bind(finalRiscoPlanejamento.getTiporisco().codriscoProperty()
                            .concat(finalRiscoPlanejamento.seqTipoProperty().asString())
                            .concat(new SimpleStringProperty(" - "))
                            .concat(finalRiscoPlanejamento.getTiporisco().descricaoProperty())
                            .concat(new SimpleStringProperty(" ("))
                            .concat(finalRiscoPlanejamento.getAplicacaoft().descricaoProperty())
                            .concat(new SimpleStringProperty(")"))
                            .concat(Bindings.when(finalRiscoPlanejamento.riscoAproProperty()).then("(APRO)").otherwise("")));
                }).build()); // cabeçalho com o tipo do risco na ABA
                tab.add(boxRiscosCores); // box com as informações das cores no risco
            });
            tabRisco.idProperty().bindBidirectional(riscoPlanejamento.seqProperty(), new NumberStringConverter());
            tabRiscos.addTab(tabRisco);
            tabRiscos.getTabs().forEach(tab -> tab.setId(String.valueOf(tabRiscos.getTabs().indexOf(tab) + 1)));
            tabRiscos.getTabs().sort(Comparator.comparing(Tab::getId)); // ordena as ABAS pelo ID
            tabRiscos.getSelectionModel().select(tabRisco); // seleciona a última aba criada
            
            riscoPlanejamento.seqProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue != null) {
                    List<SdRiscoPlanejamentoEncaixe> riscosTipo = planejamentoAberto.getRiscos().stream().filter(it -> it.getAplicacaoft().getCodigo().equals(finalRiscoPlanejamento.getAplicacaoft().getCodigo())).collect(Collectors.toList());
                    riscosTipo.sort(Comparator.comparingInt(SdRiscoPlanejamentoEncaixe::getSeq));
                    riscosTipo.forEach(it -> it.setSeqTipo(riscosTipo.indexOf(it) + 1));
                }
            });
            
            final StringProperty stringCoresAgrupadas = new SimpleStringProperty(null); // string é criada no momento do teste se os riscos poderão ser agrupados
            final StringProperty idsCoresAgrupadas = new SimpleStringProperty(null); // string é criada no momento do teste se os riscos poderão ser agrupados
            
            if (risco.getCores().size() > 0) {
                stringCoresAgrupadas.set(risco.getCores().get(0).getCor());
                if (!Arrays.asList(1, 2, 4).contains(riscoPlanejamento.getTiporisco().getCodigo())) {
                    for (SdCorRisco core : riscoPlanejamento.getCores()) {
                        planejamentoAberto.getRiscos().forEach(it -> {
                            it.getCores().forEach(it2 -> {
                                if (Arrays.asList(core.getCor().split(",")).contains(String.valueOf(it2.getId()))) {
                                    core.getRiscoCorpo().add(it2);
                                }
                            });
                        });
                    }
                    stringCoresAgrupadas.set(risco.getCores().stream().map(it -> it.getRiscoCorpo().stream().map(it2 -> it2.getCor()).distinct().collect(Collectors.joining("','"))).distinct().collect(Collectors.joining("','")));
                    idsCoresAgrupadas.set(risco.getCores().get(0).getCor());
                }
                
                riscoPlanejamento.setIdsCoresAgrupadas(idsCoresAgrupadas.get());
                riscoPlanejamento.setStringCoresAgrupadas(stringCoresAgrupadas.get());
                criarBoxRiscos(boxRiscosCores, riscoPlanejamento);
            }
            atualizarSaldosPlanejamento(risco);
        });
    }
    
    /**
     * function para criação do box com as configurações do risco com as cores selecionadas
     *
     * @param riscoPlanejamento
     * @param boxRiscosCores
     */
    private void criarBoxRiscos(FormBox boxRiscosCores, SdRiscoPlanejamentoEncaixe riscoPlanejamento) {
        boxRiscosCores.clear(); // limpa o box para criação do formulário do risco
        List<SdCorRisco> coresSelecionadas = riscoPlanejamento.getCores(); // verifica as cores selecionadas para o risco
        
        // dados cabeçalho risco
        boxRiscosCores.add(FormBox.create(boxGrade -> {
            boxGrade.horizontal();
            boxGrade.add(FormBox.create(boxPartes -> {
                boxPartes.horizontal();
                boxPartes.width(200.0);
                boxPartes.add(FormFieldText.create(field -> {
                    field.title("Partes");
                    field.alignment(Pos.CENTER);
                    field.width(80.0);
                    field.mask(FormFieldText.Mask.INTEGER);
                    field.editable.bind(emEdicao);
                    field.addStyle("lg");
                    field.value.bindBidirectional(riscoPlanejamento.partesProperty(), new NumberStringConverter());
                    final SdModelagemProduto[] modelagem = {new FluentDao().selectFrom(SdModelagemProduto.class)
                            .where(it -> it
                                    .equal("id.codigo.codigo", planejamentoAberto.getNumero().getProduto().getCodigo())
                                    .equal("id.aplicacao.codigo", riscoPlanejamento.getAplicacaoft().getCodigo()))
                            .singleResult()};
                    riscoPlanejamento.partesProperty().addListener((observable, oldValue, newValue) -> {
                        if (newValue != null) {
                            if (modelagem[0] != null) {
                                modelagem[0].setPartes(newValue.intValue());
                                new FluentDao().merge(modelagem[0]);
                            } else {
                                modelagem[0] = new SdModelagemProduto();
                                modelagem[0].setId(new SdModelagemProdutoPK(planejamentoAberto.getNumero().getProduto(), riscoPlanejamento.getAplicacaoft()));
                                modelagem[0].setPartes(newValue.intValue());
                                new FluentDao().merge(modelagem[0]);
                            }
                            riscoPlanejamento.getCores().forEach(cor -> {
                                cor.getGrade().forEach(tam -> tam.setParte(newValue.intValue() * tam.getGrade()));
                                cor.setTotalParte(cor.getGrade().stream().mapToInt(tam -> tam.getParte()).sum());
                            });
                        }
                    });
                }).build()); // partes
                boxPartes.add(FormFieldText.create(field -> {
                    field.title("Cons. Cad.");
                    field.alignment(Pos.CENTER);
                    field.expanded();
                    field.editable(false);
                    field.addStyle("lg");
                    field.value.set(StringUtils.toDecimalFormat(riscoPlanejamento.getConsumopc().doubleValue(), 4));
                }).build()); // consumo_peca
            })); // partes e consumo peça
            boxGrade.add(FormBox.create(boxDadosGrade -> {
                boxDadosGrade.vertical();
                boxDadosGrade.width(100.0);
                boxDadosGrade.add(FormFieldText.create(field -> {
                    field.withoutTitle();
                    field.alignment(Pos.CENTER);
                    field.value.set("Tamanho");
                    field.addStyle("dark");
                    field.editable(false);
                }).build()); // tamanho
                boxDadosGrade.add(FormFieldText.create(field -> {
                    field.withoutTitle();
                    field.alignment(Pos.CENTER);
                    field.value.set("Qtde. OF");
                    field.addStyle("primary");
                    field.editable(false);
                }).build()); /// qtde_of
            })); // grade de totais das cores selecionadas - (coluna descrição)
            faixaProduto.forEach(grade -> {
                boxGrade.add(FormBox.create(boxDadosGrade -> {
                    boxDadosGrade.vertical();
                    //boxDadosGrade.expanded();
                    boxDadosGrade.add(FormFieldText.create(field -> {
                        field.withoutTitle();
                        field.width(70.0);
                        field.alignment(Pos.CENTER);
                        field.value.set(grade.getFaixaItemId().getTamanho());
                        field.addStyle("dark");
                        field.editable(false);
                    }).build()); // grade
                    boxDadosGrade.add(FormFieldText.create(field -> {
                        field.withoutTitle();
                        field.width(70.0);
                        field.alignment(Pos.CENTER);
                        field.addStyle("primary");
                        Integer qtdeOf = riscoPlanejamento.getTiporisco().getCodigo() == 1
                                ? planejamentoAberto.getNumero().getItens().stream()
                                .filter(it -> checkRiscoAgrupado(coresSelecionadas.get(0).getCor())
                                        ? coresSelecionadas.stream().anyMatch(it2 -> Arrays.asList(it2.getCor().replace("'", "").split(",")).contains(it.getId().getCor().getCor()))
                                        : coresSelecionadas.stream().anyMatch(it2 -> it2.getCor().equals(it.getId().getCor().getCor())))
                                .mapToInt(it -> it.getTams().stream()
                                        .filter(it2 -> it2.getId().getTam().equals(grade.getFaixaItemId().getTamanho()))
                                        .mapToInt(it2 -> it2.getQtdeof().intValue())
                                        .sum())
                                .sum()
                                : (riscoPlanejamento.getTiporisco().getCodigo() == 3 || riscoPlanejamento.getTiporisco().getCodigo() == 5)
                                ? riscoPlanejamento.getCores().stream()
                                .mapToInt(it -> it.getRiscoCorpo().stream()
                                        .mapToInt(it2 -> it2.getGrade().stream()
                                                .filter(it3 -> it3.getTam().equals(grade.getFaixaItemId().getTamanho()))
                                                .mapToInt(it3 -> it3.getRealizado())
                                                .sum())
                                        .sum())
                                .sum()
                                : planejamentoAberto.getRiscos().stream()
                                .filter(it -> it.getTiporisco().getCodigo() == 1)
                                .mapToInt(it -> it.getCores().stream()
                                        .filter(it2 -> checkRiscoAgrupado(coresSelecionadas.get(0).getCor())
                                                ? coresSelecionadas.stream().anyMatch(it3 -> Arrays.asList(it3.getCor().replace("'", "").split(",")).contains(it2.getCor()))
                                                : coresSelecionadas.stream().anyMatch(it3 -> it3.getCor().equals(it2.getCor())))
                                        .mapToInt(it2 -> it2.getGrade().stream()
                                                .filter(it3 -> it3.getTam().equals(grade.getFaixaItemId().getTamanho()))
                                                .mapToInt(it3 -> it3.getRealizado())
                                                .sum())
                                        .sum())
                                .sum();
                        field.value.bind(new SimpleStringProperty(String.valueOf(qtdeOf)));
                        field.editable(false);
                    }).build()); // qtde total of cores selecionadas
                }));
            }); // grade de totais das cores selecionadas - (coluna valores)
            boxGrade.add(FormBox.create(boxDadosGrade -> {
                boxDadosGrade.vertical();
                boxDadosGrade.width(100.0);
                boxDadosGrade.add(FormFieldText.create(field -> {
                    field.withoutTitle();
                    field.alignment(Pos.CENTER);
                    field.value.set("Total");
                    field.addStyle("dark");
                    field.editable(false);
                }).build()); // total
                boxDadosGrade.add(FormFieldText.create(field -> {
                    field.withoutTitle();
                    field.alignment(Pos.CENTER);
                    Integer qtdeOf = riscoPlanejamento.getTiporisco().getCodigo() == 1
                            ? planejamentoAberto.getNumero().getItens().stream()
                            .filter(it -> checkRiscoAgrupado(coresSelecionadas.get(0).getCor())
                                    ? coresSelecionadas.stream().anyMatch(it2 -> Arrays.asList(it2.getCor().replace("'", "").split(",")).contains(it.getId().getCor().getCor()))
                                    : coresSelecionadas.stream().anyMatch(it2 -> it2.getCor().equals(it.getId().getCor().getCor())))
                            .mapToInt(it -> it.getTams().stream()
                                    .mapToInt(it2 -> it2.getQtdeof().intValue())
                                    .sum())
                            .sum()
                            : (riscoPlanejamento.getTiporisco().getCodigo() == 3 || riscoPlanejamento.getTiporisco().getCodigo() == 5)
                            ? riscoPlanejamento.getCores().stream()
                            .mapToInt(it -> it.getRiscoCorpo().stream()
                                    .mapToInt(it2 -> it2.getGrade().stream()
                                            .mapToInt(it3 -> it3.getRealizado())
                                            .sum())
                                    .sum())
                            .sum()
                            : planejamentoAberto.getRiscos().stream()
                            .filter(it -> it.getTiporisco().getCodigo() == 1)
                            .mapToInt(it -> it.getCores().stream()
                                    .filter(it2 -> checkRiscoAgrupado(coresSelecionadas.get(0).getCor())
                                            ? coresSelecionadas.stream().anyMatch(it3 -> Arrays.asList(it3.getCor().replace("'", "").split(",")).contains(it2.getCor()))
                                            : coresSelecionadas.stream().anyMatch(it3 -> it3.getCor().equals(it2.getCor())))
                                    .mapToInt(it2 -> it2.getGrade().stream()
                                            .mapToInt(it3 -> it3.getRealizado())
                                            .sum())
                                    .sum())
                            .sum();
                    field.value.bind(new SimpleStringProperty(String.valueOf(qtdeOf)));
                    field.addStyle("primary");
                    field.editable(false);
                }).build()); // valor
            })); // grade de totais das cores selecionadas - (coluna totais)
        })); // box com os dados gerais do risco (QTDE_PARTES, CONSUMO_PECA, GRADE_OF)
        // criação dos sub-riscos (caso escadinha ou duas cores uma sobre a outra) por cor
        coresSelecionadas.sort(Comparator.comparingInt(SdCorRisco::getSeq));
        coresSelecionadas.forEach(corSelecionada -> {
            final FormFieldToggleSingle fixarGrade = FormFieldToggleSingle.create(field -> {
                field.title("Fixar Grade");
                field.editable.bind(emEdicao);
                field.value.bindBidirectional(corSelecionada.gradeFixadaProperty());
            });
            final FormFieldToggleSingle corTubular = FormFieldToggleSingle.create(field -> {
                field.title("Tubular");
                field.editable.bind(emEdicao);
                field.value.bindBidirectional(corSelecionada.tubularProperty());
            });
            boxRiscosCores.add(FormBox.create(boxCores -> {
                boxCores.vertical();
                boxCores.add(new Separator(Orientation.HORIZONTAL));
                boxCores.add(FormBox.create(boxDadosRiscoCor -> {
                    boxDadosRiscoCor.horizontal();
                    boxDadosRiscoCor.add(FormBox.create(boxSetInfosRisco -> {
                        boxSetInfosRisco.vertical();
                        boxSetInfosRisco.width(150.0);
                        boxSetInfosRisco.add(FormBox.create(boxBtnsRiscoCor -> {
                            boxBtnsRiscoCor.horizontal();
                            boxBtnsRiscoCor.add(FormButton.create(btn -> {
                                btn.title("Excluir");
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                btn.addStyle("danger");
                                btn.disable.bind(emEdicao.not());
                                btn.setAction(evt -> {
                                    if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                                        message.message("Deseja realmente excluir este risco?");
                                        message.showAndWait();
                                    }).value.get())) {
                                        corSelecionada.getMateriais().forEach(it -> new FluentDao().delete(it));
                                        corSelecionada.getGrade().forEach(it -> new FluentDao().delete(it));
                                        new FluentDao().delete(corSelecionada);
                                        
                                        SysLogger.addSysDelizLog("Planejamento Encaixe", TipoAcao.EDITAR, planejamentoAberto.getNumero().getNumero(),
                                                "Excluindo cor ID " + corSelecionada.getId() + " no risco ID " + riscoPlanejamento.getId()
                                                        + " (" + riscoPlanejamento.getTiporisco().getCodrisco() + riscoPlanejamento.getSeqTipo() + ") no planejamento ID " + planejamentoAberto.getId());
                                        riscoPlanejamento.getCores().remove(corSelecionada);
                                        boxRiscosCores.remove(boxCores);
                                        MessageBox.create(message -> {
                                            message.message("Risco excluído com sucesso!");
                                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                                            message.position(Pos.TOP_RIGHT);
                                            message.notification();
                                        });
                                    }
                                });
                            }));
                            boxBtnsRiscoCor.add(FormButton.create(btn -> {
                                btn.title("Escada");
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ENCAIXE, ImageUtils.IconSize._16));
                                btn.addStyle("primary");
                                btn.disable.bind(emEdicao.not());
                                btn.setAction(evt -> {
                                    if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                                        message.message("Deseja realmente incluir a escada para este risco?");
                                        message.showAndWait();
                                    }).value.get())) {
                                        corSelecionada.setEscada(true);
                                        // cria o objeto da cor do risco e alimenta os dados
                                        SdCorRisco riscoCor = new SdCorRisco();
                                        riscoCor.setIdrisco(riscoPlanejamento.getId());
                                        riscoCor.setCor(corSelecionada.getCor());
                                        riscoCor.setComprimento(BigDecimal.ZERO);
                                        riscoCor.setConsumopeca(BigDecimal.ZERO);
                                        riscoCor.setConsumototal(BigDecimal.ZERO);
                                        riscoCor.setFolhas(0);
                                        riscoCor.setEscada(true);
                                        riscoCor.setSeq(coresSelecionadas.size() + 1);
                                        try {
                                            // persistindo a cor-risco no banco
                                            riscoCor = new FluentDao().persist(riscoCor);
                                            riscoPlanejamento.getCores().add(riscoCor);
                                            
                                            // criação da grade de tamanhos para a cor do risco
                                            SdCorRisco finalRiscoCor = riscoCor;
                                            corSelecionada.getGrade().forEach(tam -> {
                                                // criando o objeto do tamanho e alimentado com os dados
                                                SdGradeRiscoCor tamanhosRiscoCor = new SdGradeRiscoCor();
                                                tamanhosRiscoCor.setIdrisco(finalRiscoCor.getId());
                                                tamanhosRiscoCor.setCor(corSelecionada.getCor());
                                                tamanhosRiscoCor.setTam(tam.getTam());
                                                tamanhosRiscoCor.setGrade(0);
                                                tamanhosRiscoCor.setParte(0);
                                                tamanhosRiscoCor.setRealizado(0);
                                                tamanhosRiscoCor.setSaldoOf(tam.getSaldoOf());
                                                try {
                                                    tamanhosRiscoCor = new FluentDao().persist(tamanhosRiscoCor);
                                                    finalRiscoCor.getGrade().add(tamanhosRiscoCor);
                                                } catch (SQLException e) {
                                                    e.printStackTrace();
                                                    ExceptionBox.build(message -> {
                                                        message.exception(e);
                                                        message.showAndWait();
                                                    });
                                                }
                                            });
                                            // atualizando os totais da cor conforme o cadastro
                                            riscoCor.setTotalSaldoOf(riscoCor.getGrade().stream().mapToInt(tam -> tam.getSaldoOf()).sum());
                                        } catch (SQLException e) {
                                            e.printStackTrace();
                                            ExceptionBox.build(message -> {
                                                message.exception(e);
                                                message.showAndWait();
                                            });
                                        }
                                        criarBoxRiscos(boxRiscosCores, riscoPlanejamento);
                                    }
                                });
                            }));
                        }));
                        boxSetInfosRisco.add(FormBox.create(boxRiscoCorCor -> {
                            boxRiscoCorCor.horizontal();
                            boxRiscoCorCor.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.editable(false);
                                field.addStyle("amber");
                                field.width(60.0);
                                field.alignment(Pos.CENTER);
                                field.mask(FormFieldText.Mask.INTEGER);
                                field.mouseClicked(evtMouse -> {
                                    if (/*corSelecionada.isEscada() && */evtMouse.getClickCount() > 1 && emEdicao.get()) {
                                        field.editable(true);
                                    }
                                });
                                field.value.bindBidirectional(corSelecionada.seqProperty(), new NumberStringConverter());
                            }).build()); // Código do risco
                            boxRiscoCorCor.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.editable(false);
                                field.addStyle("dark");
                                field.expanded();
                                field.label("COR");
                                field.alignment(Pos.CENTER);
                                field.value.set(
                                        checkRiscoAgrupado(corSelecionada.getCor())
                                                ? "AGRP"
                                                : Arrays.asList(1, 2, 4).contains(riscoPlanejamento.getTiporisco().getCodigo())
                                                ? corSelecionada.getCor()
                                                : checkRiscoAgrupado(corSelecionada.getCor()) ? corSelecionada.getRiscoCorpo().get(0).getCor() : corSelecionada.getCor());
                            }).build()); // Cor selecionada no risco
                        })); // Código do risco e código da cor
                        boxSetInfosRisco.add(FormFieldText.create(field -> {
                            field.withoutTitle();
                            field.label("Comp:");
                            field.postLabel("mts");
                            field.width(150.0);
                            field.editable.bind(emEdicao);
                            field.decimalField(2);
                            field.alignment(Pos.CENTER);
                            field.value.bindBidirectional(corSelecionada.comprimentoProperty(), new BigDecimalAttributeConverter());
                            corSelecionada.comprimentoProperty().addListener((observable, oldValue, newValue) -> {
                                if (newValue != null) {
                                    if (newValue.doubleValue() > 0) {
                                        try {
                                            BigDecimal qtdeFolhas = new BigDecimal(corSelecionada.getFolhas());
                                            BigDecimal comprimentoComCabiceira = corSelecionada.getComprimento().add(new BigDecimal(.06)).setScale(4, RoundingMode.CEILING);
                                            BigDecimal consumoTotal = qtdeFolhas.multiply(comprimentoComCabiceira).setScale(4, RoundingMode.CEILING);
                                            BigDecimal consumoTotalArredondado = consumoTotal.setScale(4, RoundingMode.CEILING);
                                            corSelecionada.setConsumototal(consumoTotalArredondado);
                                            BigDecimal totalRealizado = new BigDecimal(corSelecionada.getTotalRealizado());
                                            BigDecimal consumoPorPeca = consumoTotalArredondado.divide(totalRealizado, BigDecimal.ROUND_UP).setScale(4, RoundingMode.CEILING);
                                            corSelecionada.setConsumopeca(consumoPorPeca);
                                        } catch (ArithmeticException ex) {
                                            corSelecionada.setConsumopeca(BigDecimal.ZERO);
                                        }
                                    } else {
                                        corSelecionada.setConsumototal(BigDecimal.ZERO);
                                        corSelecionada.setConsumopeca(BigDecimal.ZERO);
                                    }
                                    
                                    SysLogger.addSysDelizLog("Planejamento Encaixe", TipoAcao.EDITAR, planejamentoAberto.getNumero().getNumero(),
                                            "Adicionado comprimento " + newValue.doubleValue() + " cor ID " + corSelecionada.getId() + " no risco ID " + riscoPlanejamento.getId()
                                                    + " (" + riscoPlanejamento.getTiporisco().getCodrisco() + riscoPlanejamento.getSeqTipo() + ") no planejamento ID " + planejamentoAberto.getId());
                                }
                            });
                        }).build()); // field comprimento do risco
                        boxSetInfosRisco.add(FormFieldText.create(field -> {
                            field.withoutTitle();
                            field.label("Folhas:");
                            field.mask(FormFieldText.Mask.INTEGER);
                            field.width(150.0);
                            field.alignment(Pos.CENTER);
                            field.editable.bind(emEdicao);
                            field.value.bindBidirectional(corSelecionada.folhasProperty(), new NumberStringConverter());
                            // calculo da grade com base no numero de folhas digitados
                            corSelecionada.folhasProperty().addListener((observable, oldValue, newValue) -> {
                                if (newValue != null) {
                                    // limpa o saldo da of para o original
                                    if (fixarGrade.value.not().get())
                                        corSelecionada.getGrade().forEach(grade -> {
                                            grade.setRealizado(0);
                                            Integer qtdeOf = riscoPlanejamento.getTiporisco().getCodigo() == 1
                                                    ? planejamentoAberto.getNumero().getItens().stream()
                                                    .filter(it -> checkRiscoAgrupado(corSelecionada.getCor())
                                                            ? Arrays.asList(corSelecionada.getCor().replace("'", "").split(",")).contains(it.getId().getCor().getCor())
                                                            : corSelecionada.getCor().equals(it.getId().getCor().getCor()))
                                                    .mapToInt(it -> it.getTams().stream()
                                                            .filter(it2 -> it2.getId().getTam().equals(grade.getTam()))
                                                            .mapToInt(it2 -> it2.getQtdeof().intValue())
                                                            .sum())
                                                    .sum()
                                                    : (riscoPlanejamento.getTiporisco().getCodigo() == 3 || riscoPlanejamento.getTiporisco().getCodigo() == 5)
                                                    ? corSelecionada.getRiscoCorpo().stream()
                                                    .filter(it -> checkRiscoAgrupado(it.getCor()) ? true : corSelecionada.isGrupoCor() ? it.getCor().equals(corSelecionada.getCor()) :
                                                            getCorIds(corSelecionada.getCor()).stream().map(rCor -> String.valueOf(rCor.getId())).distinct().collect(Collectors.toList()).contains(String.valueOf(it.getId())))
                                                    .mapToInt(it -> it.getGrade().stream()
                                                            .filter(it2 -> it2.getTam().equals(grade.getTam()))
                                                            .mapToInt(SdGradeRiscoCor::getRealizado)
                                                            .sum())
                                                    .sum()
                                                    : planejamentoAberto.getRiscos().stream()
                                                    .filter(it -> it.getTiporisco().getCodigo() == 1)
                                                    .mapToInt(it -> it.getCores().stream()
                                                            .filter(it2 -> checkRiscoAgrupado(corSelecionada.getCor())
                                                                    ? Arrays.asList(corSelecionada.getCor().replace("'", "").split(",")).contains(it2.getCor())
                                                                    : corSelecionada.getCor().equals(it2.getCor()))
                                                            .mapToInt(it2 -> it2.getGrade().stream()
                                                                    .filter(it3 -> it3.getTam().equals(grade.getTam()))
                                                                    .mapToInt(it3 -> it3.getRealizado())
                                                                    .sum())
                                                            .sum())
                                                    .sum();
                                            Integer qtdeRiscada = Arrays.asList(1, 2, 4).contains(riscoPlanejamento.getTiporisco().getCodigo())
                                                    ? planejamentoAberto.getRiscos().stream()
                                                    .filter(it -> it.getAplicacaoft().getCodigo() == riscoPlanejamento.getAplicacaoft().getCodigo())
                                                    .mapToInt(it -> it.getCores().stream()
                                                            .filter(it2 -> checkRiscoAgrupado(corSelecionada.getCor())
                                                                    ? Arrays.asList(corSelecionada.getCor().replace("'", "").split(",")).contains(it2.getCor())
                                                                    : corSelecionada.getCor().equals(it2.getCor()))
                                                            .mapToInt(it2 -> it2.getGrade().stream()
                                                                    .filter(it3 -> it3.getTam().equals(grade.getTam()))
                                                                    .mapToInt(SdGradeRiscoCor::getRealizado)
                                                                    .sum())
                                                            .sum())
                                                    .sum()
                                                    : planejamentoAberto.getRiscos().stream()
                                                    .filter(it -> it.getAplicacaoft().getCodigo() == riscoPlanejamento.getAplicacaoft().getCodigo())
                                                    .mapToInt(it -> it.getCores().stream()
                                                            .filter(it2 -> checkRiscoAgrupado(it2.getCor()) ? it2.getRiscoCorpo().stream().anyMatch(it3 -> corSelecionada.getRiscoCorpo().stream().anyMatch(it4 -> it4.getId() == it3.getId()))
                                                            : it2.getCor().equals(corSelecionada.getCor()))
                                                            .mapToInt(it2 -> it2.getGrade().stream()
                                                                    .filter(it3 -> it3.getTam().equals(grade.getTam()))
                                                                    .mapToInt(SdGradeRiscoCor::getRealizado)
                                                                    .sum())
                                                            .sum())
                                                    .sum();
                                            grade.setSaldoOf(qtdeOf - qtdeRiscada);
                                        });
                                    if (newValue.intValue() != 0) {
                                        // calcula a grade de múltiplo da cor/tam
                                        // como está alterando o objeto, irá acontecer o listner do change do atributo da cortam
                                        if (fixarGrade.value.not().get()) {
                                            corSelecionada.getGrade().forEach(grade -> {
                                                grade.setGrade((int) (grade.getSaldoOf() / newValue.intValue()));
                                            }); // atribuição do valor do multiplo na grade (SALDO / QTDE_FOLHAS)
                                        } else {
                                            corSelecionada.getGrade().forEach(grade -> {
                                                grade.setRealizado(grade.getGrade() * newValue.intValue());
                                                grade.setSaldoOf(grade.getSaldoOf() - grade.getRealizado());
                                                grade.setParte((int) ((grade.getGrade() * riscoPlanejamento.getPartes()) / ((tecidoTubularField.value.get() || corTubular.value.get()) ? 2 : 1)));
                                            });
                                        }
                                        corSelecionada.setTotalGrade(corSelecionada.getGrade().stream().mapToInt(SdGradeRiscoCor::getGrade).sum());
                                        corSelecionada.setTotalRealizado(corSelecionada.getGrade().stream().mapToInt(SdGradeRiscoCor::getRealizado).sum());
                                        corSelecionada.setTotalParte(corSelecionada.getGrade().stream().mapToInt(SdGradeRiscoCor::getParte).sum());
                                        try {
                                            // calculo do consumo de material
                                            BigDecimal qtdeFolhas = new BigDecimal(corSelecionada.getFolhas());
                                            BigDecimal comprimentoComCabiceira = corSelecionada.getComprimento().add(new BigDecimal(.06)).setScale(4, RoundingMode.CEILING);
                                            BigDecimal consumoTotal = qtdeFolhas.multiply(comprimentoComCabiceira).setScale(4, RoundingMode.CEILING);
                                            BigDecimal consumoTotalArredondado = consumoTotal.setScale(4, RoundingMode.CEILING);
                                            corSelecionada.setConsumototal(consumoTotalArredondado);
                                            BigDecimal totalRealizado = new BigDecimal(corSelecionada.getTotalRealizado());
                                            BigDecimal consumoPorPeca = consumoTotalArredondado.divide(totalRealizado, BigDecimal.ROUND_UP).setScale(4, RoundingMode.CEILING);
                                            corSelecionada.setConsumopeca(consumoPorPeca);
                                            
                                            // verificação se grade atende consumos por peça
//                                            List<ConsumoTecido> consumosUnidade = getConsumosUnidade(planejamentoAberto.getNumero().getNumero(), checkRiscoAgrupado(corSelecionada.getCor()) ? stringCoresAgrupadas.get() : corSelecionada.getCor());
//                                            consumosUnidade.forEach(it -> {
//                                                BigDecimal consumoReal = new BigDecimal(planejamentoAberto.getRiscos().stream()
//                                                        .filter(it2 -> it2.getTiporisco().getCodigo() == riscoPlanejamento.getTiporisco().getCodigo())
//                                                        .mapToInt(it2 -> it2.getCores().stream()
//                                                                .filter(it3 -> it3.getCor() == null ? true : it3.getCor().equals(corSelecionada.getCor()))
//                                                                .mapToInt(it3 -> it3.getGrade().stream()
//                                                                        .filter(grade -> it.faixa.contains("|".concat(grade.getTam()).concat("|")))
//                                                                        .mapToInt(grade -> grade.getRealizado())
//                                                                        .sum())
//                                                                .sum())
//                                                        .sum() * it.consumo.doubleValue());
//
//                                                if (consumoReal.compareTo(it.consumoTotal) > 0) {
//                                                    MessageBox.create(message -> {
//                                                        message.message("Será necessário reservar mais material " + it.insumo + " unitário para a aplicação " + it.aplicacao);
//                                                        message.type(MessageBox.TypeMessageBox.WARNING);
//                                                        message.showAndWait();
//                                                    });
//                                                }
//                                            });
                                        } catch (ArithmeticException ex) {
                                            ex.printStackTrace();
                                            corSelecionada.setConsumopeca(BigDecimal.ZERO);
                                        } // calculo consumo por peça
                                    } else {
                                        if (field.value.isNotNull().and(field.value.isNotEmpty()).get()) {
                                            // zera os valores caso digitou 0 folhas
                                            corSelecionada.getGrade().forEach(grade -> {
                                                grade.setGrade(0);
                                            });
                                            corSelecionada.setTotalRealizado(0);
                                            corSelecionada.setTotalGrade(0);
                                            corSelecionada.setConsumototal(BigDecimal.ZERO);
                                            corSelecionada.setConsumopeca(BigDecimal.ZERO);
                                        }
                                    }
                                    atualizarSaldosPlanejamento(riscoPlanejamento);
                                    
                                    SysLogger.addSysDelizLog("Planejamento Encaixe", TipoAcao.EDITAR, planejamentoAberto.getNumero().getNumero(),
                                            "Adicionado folhas " + newValue.intValue() + " cor ID " + corSelecionada.getId() + " no risco ID " + riscoPlanejamento.getId()
                                                    + " (" + riscoPlanejamento.getTiporisco().getCodrisco() + riscoPlanejamento.getSeqTipo() + ") no planejamento ID " + planejamentoAberto.getId());
                                }
                            });
                        }).build()); // field com a qtde de folhas
                        boxSetInfosRisco.add(fixarGrade.build());
                        boxSetInfosRisco.add(corTubular.build());
//                        if (riscoPlanejamento.getTiporisco().getCodigo() != 1) {
//                            boxSetInfosRisco.add(FormFieldToggle.create(field -> {
//                                field.title("Agrupar Cor/Sub-Risco?");
//                                field.value.bindBidirectional(corSelecionada.corAgrupadaProperty());
//                            }).build()); // field para agrupar as cores em um risco somente
//                        } // teste para verificar se o risco não é do tipo corpo para aparecer o agrupardor
                    })); // formulário de informações do risco (COD_RISCO, COR, COMPRIMENTO, FOLHAS), com a opção de agrupar caso não é risco de corpo *** com botão de aplicar em todas as cores ***
                    boxDadosRiscoCor.add(FormBox.create(boxGradeCorSelecionada -> {
                        boxGradeCorSelecionada.horizontal();
                        boxGradeCorSelecionada.add(FormBox.create(boxDadosGrade -> {
                            boxDadosGrade.vertical();
                            boxDadosGrade.width(90.0);
                            boxDadosGrade.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.alignment(Pos.CENTER);
                                field.value.set("Tamanho");
                                field.addStyle("dark");
                                field.editable(false);
                            }).build()); // tamanho
                            boxDadosGrade.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.alignment(Pos.CENTER);
                                field.value.set("Saldo OF");
                                field.addStyle("amber");
                                field.editable(false);
                            }).build()); // saldo of
                            boxDadosGrade.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.alignment(Pos.CENTER);
                                field.value.set("Realizado");
                                field.addStyle("success");
                                field.editable(false);
                            }).build()); // realizado
                            boxDadosGrade.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.alignment(Pos.CENTER);
                                field.value.set("Grade");
                                field.editable(false);
                            }).build()); // grade
                            boxDadosGrade.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.alignment(Pos.CENTER);
                                field.value.set("Partes");
                                field.editable(false);
                            }).build()); // partes
                            if (riscoPlanejamento.getTiporisco().getCodigo() == 1) {
                                boxDadosGrade.add(new Separator(Orientation.HORIZONTAL));
                                boxDadosGrade.add(FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.alignment(Pos.CENTER);
                                    field.value.set("Som. Cor");
                                    field.addStyle("dark");
                                    field.editable(false);
                                }).build()); // som. cor
                            }
                        })); // boxes com a descrição das linhas (coluna descrição)
                        boxGradeCorSelecionada.add(FormBox.create(boxDadosGrade -> {
                            boxDadosGrade.vertical();
                            boxDadosGrade.add(FormBox.create(boxDadosGradeLinha -> {
                                boxDadosGradeLinha.horizontal();
                                faixaProduto.forEach(grade -> {
                                    // pega o objeto da grade/tam da cor selecionada
                                    Optional<SdGradeRiscoCor> optionalGrade = corSelecionada.getGrade().stream().filter(corRisco -> corRisco.getTam().equals(grade.getFaixaItemId().getTamanho())).findAny();
                                    if (optionalGrade.isPresent()) {
                                        SdGradeRiscoCor gradeRisco = optionalGrade.get();
                                        boxDadosGradeLinha.add(FormFieldText.create(field -> {
                                            field.withoutTitle();
                                            field.width(65.0);
                                            field.alignment(Pos.CENTER);
                                            field.value.set(grade.getFaixaItemId().getTamanho());
                                            field.addStyle("dark");
                                            field.editable(false);
                                        }).build()); // grade
                                    }
                                });
                            })); // boxes com os valores da grade da cor do risco linha tamanho
                            boxDadosGrade.add(FormBox.create(boxDadosGradeLinha -> {
                                boxDadosGradeLinha.horizontal();
                                faixaProduto.forEach(grade -> {
                                    // pega o objeto da grade/tam da cor selecionada
                                    Optional<SdGradeRiscoCor> optionalGrade = corSelecionada.getGrade().stream().filter(corRisco -> corRisco.getTam().equals(grade.getFaixaItemId().getTamanho())).findAny();
                                    if (optionalGrade.isPresent()) {
                                        SdGradeRiscoCor gradeRisco = optionalGrade.get();
                                        boxDadosGradeLinha.add(FormFieldText.create(field -> {
                                            field.withoutTitle();
                                            field.width(65.0);
                                            field.alignment(Pos.CENTER);
                                            field.addStyle("amber");
                                            field.editable(false);
                                            field.value.bind(gradeRisco.saldoOfProperty().asString());
                                        }).build()); // saldo_of
                                    }
                                });
                            })); // boxes com os valores da grade da cor do risco linha saldo of
                            boxDadosGrade.add(FormBox.create(boxDadosGradeLinha -> {
                                boxDadosGradeLinha.horizontal();
                                faixaProduto.forEach(grade -> {
                                    // pega o objeto da grade/tam da cor selecionada
                                    Optional<SdGradeRiscoCor> optionalGrade = corSelecionada.getGrade().stream().filter(corRisco -> corRisco.getTam().equals(grade.getFaixaItemId().getTamanho())).findAny();
                                    if (optionalGrade.isPresent()) {
                                        SdGradeRiscoCor gradeRisco = optionalGrade.get();
                                        boxDadosGradeLinha.add(FormFieldText.create(field -> {
                                            field.withoutTitle();
                                            field.width(65.0);
                                            field.alignment(Pos.CENTER);
                                            field.addStyle("success");
                                            field.editable(false);
                                            field.value.bind(gradeRisco.realizadoProperty().asString());
                                        }).build()); // realizado
                                    }
                                });
                            })); // boxes com os valores da grade da cor do risco linha realizado
                            boxDadosGrade.add(FormBox.create(boxDadosGradeLinha -> {
                                boxDadosGradeLinha.horizontal();
                                faixaProduto.forEach(grade -> {
                                    // pega o objeto da grade/tam da cor selecionada
                                    Optional<SdGradeRiscoCor> optionalGrade = corSelecionada.getGrade().stream().filter(corRisco -> corRisco.getTam().equals(grade.getFaixaItemId().getTamanho())).findAny();
                                    if (optionalGrade.isPresent()) {
                                        SdGradeRiscoCor gradeRisco = optionalGrade.get();
                                        boxDadosGradeLinha.add(FormFieldText.create(field -> {
                                            field.withoutTitle();
                                            field.width(65.0);
                                            field.alignment(Pos.CENTER);
                                            field.editable.bind(emEdicao);
                                            field.value.bindBidirectional(gradeRisco.gradeProperty(), new NumberStringConverter());
                                            field.value.addListener((observable, oldValue, newValue) -> {
                                                if (newValue != null) {
                                                    gradeRisco.setRealizado(0);
                                                    Integer qtdeOf = riscoPlanejamento.getTiporisco().getCodigo() == 1
                                                            ? planejamentoAberto.getNumero().getItens().stream()
                                                            .filter(it -> checkRiscoAgrupado(corSelecionada.getCor())
                                                                    ? Arrays.asList(corSelecionada.getCor().replace("'", "").split(",")).contains(it.getId().getCor().getCor())
                                                                    : corSelecionada.getCor().equals(it.getId().getCor().getCor()))
                                                            .mapToInt(it -> it.getTams().stream()
                                                                    .filter(it2 -> it2.getId().getTam().equals(gradeRisco.getTam()))
                                                                    .mapToInt(it2 -> it2.getQtdeof().intValue())
                                                                    .sum())
                                                            .sum()
                                                            : (riscoPlanejamento.getTiporisco().getCodigo() == 3 || riscoPlanejamento.getTiporisco().getCodigo() == 5)
                                                            ? corSelecionada.getRiscoCorpo().stream()
                                                            .filter(it -> checkRiscoAgrupado(it.getCor()) ? true : corSelecionada.isGrupoCor() ? it.getCor().equals(corSelecionada.getCor()) :
                                                                    getCorIds(corSelecionada.getCor()).stream().map(rCor -> String.valueOf(rCor.getId())).distinct().collect(Collectors.toList()).contains(String.valueOf(it.getId())))
                                                            .mapToInt(it -> it.getGrade().stream()
                                                                    .filter(it2 -> it2.getTam().equals(gradeRisco.getTam()))
                                                                    .mapToInt(SdGradeRiscoCor::getRealizado)
                                                                    .sum())
                                                            .sum()
                                                            : planejamentoAberto.getRiscos().stream()
                                                            .filter(it -> it.getTiporisco().getCodigo() == 1)
                                                            .mapToInt(it -> it.getCores().stream()
                                                                    .filter(it2 -> checkRiscoAgrupado(corSelecionada.getCor())
                                                                            ? Arrays.asList(corSelecionada.getCor().replace("'", "").split(",")).contains(it2.getCor())
                                                                            : corSelecionada.getCor().equals(it2.getCor()))
                                                                    .mapToInt(it2 -> it2.getGrade().stream()
                                                                            .filter(it3 -> it3.getTam().equals(gradeRisco.getTam()))
                                                                            .mapToInt(it3 -> it3.getRealizado())
                                                                            .sum())
                                                                    .sum())
                                                            .sum();
                                                    Integer qtdeRiscada = Arrays.asList(1, 2, 4).contains(riscoPlanejamento.getTiporisco().getCodigo())
                                                            ? planejamentoAberto.getRiscos().stream()
                                                            .filter(it -> it.getAplicacaoft().getCodigo() == riscoPlanejamento.getAplicacaoft().getCodigo())
                                                            .mapToInt(it -> it.getCores().stream()
                                                                    .filter(it2 -> checkRiscoAgrupado(corSelecionada.getCor())
                                                                            ? Arrays.asList(corSelecionada.getCor().replace("'", "").split(",")).contains(it2.getCor())
                                                                            : corSelecionada.getCor().equals(it2.getCor()))
                                                                    .mapToInt(it2 -> it2.getGrade().stream()
                                                                            .filter(it3 -> it3.getTam().equals(gradeRisco.getTam()))
                                                                            .mapToInt(SdGradeRiscoCor::getRealizado)
                                                                            .sum())
                                                                    .sum())
                                                            .sum()
                                                            : planejamentoAberto.getRiscos().stream()
                                                            .filter(it -> it.getAplicacaoft().getCodigo() == riscoPlanejamento.getAplicacaoft().getCodigo())
                                                            .mapToInt(it -> it.getCores().stream()
                                                                    .filter(it2 -> checkRiscoAgrupado(it2.getCor()) ? it2.getRiscoCorpo().stream().anyMatch(it3 -> corSelecionada.getRiscoCorpo().stream().anyMatch(it4 -> it4.getId() == it3.getId()))
                                                                            : it2.getCor().equals(corSelecionada.getCor()))
                                                                    .mapToInt(it2 -> it2.getGrade().stream()
                                                                            .filter(it3 -> it3.getTam().equals(gradeRisco.getTam()))
                                                                            .mapToInt(SdGradeRiscoCor::getRealizado)
                                                                            .sum())
                                                                    .sum())
                                                            .sum();
                                                    gradeRisco.setSaldoOf(qtdeOf - qtdeRiscada);
                                                    if (gradeRisco.getGrade() != 0) {
                                                        gradeRisco.setRealizado(gradeRisco.getGrade() * corSelecionada.getFolhas());
                                                        gradeRisco.setParte((int) ((gradeRisco.getGrade() * riscoPlanejamento.getPartes()) / ((tecidoTubularField.value.get() || corTubular.value.get()) ? 2 : 1)));
                                                        gradeRisco.setSaldoOf(gradeRisco.getSaldoOf() - gradeRisco.getRealizado());
                                                    } else {
                                                        gradeRisco.setRealizado(0);
                                                        gradeRisco.setGrade(0);
                                                        gradeRisco.setParte(0);
                                                    }
                                                    
                                                    // recalculando os totais da cor
                                                    corSelecionada.setTotalGrade(corSelecionada.getGrade().stream().mapToInt(tam -> tam.getGrade()).sum());
                                                    corSelecionada.setTotalParte(corSelecionada.getGrade().stream().mapToInt(tam -> tam.getParte()).sum());
                                                    corSelecionada.setTotalRealizado(corSelecionada.getGrade().stream().mapToInt(tam -> tam.getRealizado()).sum());
                                                    corSelecionada.setTotalSaldoOf(corSelecionada.getGrade().stream().mapToInt(tam -> tam.getSaldoOf()).sum());
                                                    
                                                    // verificação se grade atende consumos por peça
//                                            List<ConsumoTecido> consumosUnidade = getConsumosUnidade(planejamentoAberto.getNumero().getNumero(), checkRiscoAgrupado(corSelecionada.getCor()) ? stringCoresAgrupadas.get() : corSelecionada.getCor());
//                                            consumosUnidade.forEach(it -> {
//                                                BigDecimal consumoReal = new BigDecimal(planejamentoAberto.getRiscos().stream()
//                                                        .filter(it2 -> it2.getTiporisco().getCodigo() == riscoPlanejamento.getTiporisco().getCodigo())
//                                                        .mapToInt(it2 -> it2.getCores().stream()
//                                                                .filter(it3 -> it3.getCor() == null ? true : it3.getCor().equals(corSelecionada.getCor()))
//                                                                .mapToInt(it3 -> it3.getGrade().stream()
//                                                                        .filter(it4 -> it.faixa.contains("|".concat(grade.getFaixaItemId().getTamanho()).concat("|")))
//                                                                        .mapToInt(it4 -> it4.getRealizado())
//                                                                        .sum())
//                                                                .sum())
//                                                        .sum() * it.consumo.doubleValue());
//
//                                                if (consumoReal.compareTo(it.consumoTotal) > 0) {
//                                                    MessageBox.create(message -> {
//                                                        message.message("Será necessário reservar mais material " + it.insumo + " unitário para a aplicação " + it.aplicacao);
//                                                        message.type(MessageBox.TypeMessageBox.WARNING);
//                                                        message.showAndWait();
//                                                    });
//                                                }
//                                            });
                                                    
                                                    try {
                                                        // recalcula o consumo por peça
                                                        BigDecimal consumoPorPeca = corSelecionada.getConsumototal().divide(new BigDecimal(corSelecionada.getTotalRealizado()), BigDecimal.ROUND_UP).setScale(4, RoundingMode.CEILING);
                                                        corSelecionada.setConsumopeca(consumoPorPeca);
                                                        atualizarSaldosPlanejamento(riscoPlanejamento);
                                                    } catch (ArithmeticException ex) {
                                                        ex.printStackTrace();
                                                        corSelecionada.setConsumopeca(BigDecimal.ZERO);
                                                    } // cálculo do consumo por peça
                                                }
                                            });
                                        }).build()); // grade
                                    }
                                });
                            })); // boxes com os valores da grade da cor do risco linha grade
                            boxDadosGrade.add(FormBox.create(boxDadosGradeLinha -> {
                                boxDadosGradeLinha.horizontal();
                                faixaProduto.forEach(grade -> {
                                    // pega o objeto da grade/tam da cor selecionada
                                    Optional<SdGradeRiscoCor> optionalGrade = corSelecionada.getGrade().stream().filter(corRisco -> corRisco.getTam().equals(grade.getFaixaItemId().getTamanho())).findAny();
                                    if (optionalGrade.isPresent()) {
                                        SdGradeRiscoCor gradeRisco = optionalGrade.get();
                                        boxDadosGradeLinha.add(FormFieldText.create(field -> {
                                            field.withoutTitle();
                                            field.width(65.0);
                                            field.alignment(Pos.CENTER);
                                            field.mask(FormFieldText.Mask.INTEGER);
                                            field.editable.bind(corTubular.value);
                                            field.value.bindBidirectional(gradeRisco.parteProperty(), new NumberStringConverter());
                                            field.value.addListener((observable, oldValue, newValue) -> {
                                                corSelecionada.setTotalParte(corSelecionada.getGrade().stream().mapToInt(SdGradeRiscoCor::getParte).sum());
                                            });
                                        }).build()); // partes
                                    }
                                });
                            })); // boxes com os valores da grade da cor do risco linha partes
                            if (riscoPlanejamento.getTiporisco().getCodigo() == 1) {
                                boxDadosGrade.add(new Separator(Orientation.HORIZONTAL));
                                boxDadosGrade.add(FormBox.create(boxDadosGradeLinha -> {
                                    boxDadosGradeLinha.horizontal();
                                    faixaProduto.forEach(grade -> {
                                        // pega o objeto da grade/tam da cor selecionada
                                        Optional<SdGradeRiscoCor> optionalGrade = corSelecionada.getGrade().stream().filter(corRisco -> corRisco.getTam().equals(grade.getFaixaItemId().getTamanho())).findAny();
                                        if (optionalGrade.isPresent()) {
                                            SdGradeRiscoCor gradeRisco = optionalGrade.get();
                                            boxDadosGradeLinha.add(FormFieldText.create(field -> {
                                                field.withoutTitle();
                                                field.width(65.0);
                                                field.alignment(Pos.CENTER);
                                                field.editable(false);
                                                field.addStyle("dark");
                                                field.value.bind(gradeRisco.somatorioCorProperty().asString());
                                            }).build()); // somCor
                                        }
                                    });
                                })); // boxes com os valores da grade da cor do risco linha partes
                            }
                        }));  // boxes com os valores da grade
                        boxGradeCorSelecionada.add(FormBox.create(boxDadosGrade -> {
                            boxDadosGrade.vertical();
                            boxDadosGrade.width(65.0);
                            boxDadosGrade.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.alignment(Pos.CENTER);
                                field.value.set("Total");
                                field.addStyle("dark");
                                field.editable(false);
                            }).build());
                            boxDadosGrade.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.alignment(Pos.CENTER);
                                field.value.bind(corSelecionada.totalSaldoOfProperty().asString());
                                field.addStyle("amber");
                                field.editable(false);
                            }).build());
                            boxDadosGrade.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.alignment(Pos.CENTER);
                                field.value.bind(corSelecionada.totalRealizadoProperty().asString());
                                field.addStyle("success");
                                field.editable(false);
                            }).build());
                            boxDadosGrade.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.alignment(Pos.CENTER);
                                field.value.bind(corSelecionada.totalGradeProperty().asString());
                                field.editable(false);
                            }).build());
                            boxDadosGrade.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.alignment(Pos.CENTER);
                                field.value.bind(corSelecionada.totalParteProperty().asString());
                                field.editable(false);
                            }).build());
                            if (riscoPlanejamento.getTiporisco().getCodigo() == 1) {
                                boxDadosGrade.add(new Separator(Orientation.HORIZONTAL));
                                boxDadosGrade.add(FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.alignment(Pos.CENTER);
                                    field.value.bind(corSelecionada.totalSomCorProperty().asString());
                                    field.addStyle("dark");
                                    field.editable(false);
                                }).build());
                            }
                        })); // boxes com os totais dos valores da grade
                    })); // grade de tamanhos com a configuração do risco
                    boxDadosRiscoCor.add(FormBox.create(boxSetMateriais -> {
                        boxSetMateriais.vertical();
                        boxSetMateriais.width(170.0);
                        boxSetMateriais.add(FormButton.create(btnSelecionarMaterial -> {
                            btnSelecionarMaterial.title("Selecionar Rodos");
                            btnSelecionarMaterial.icon(ImageUtils.getIcon(ImageUtils.Icon.TECIDO, ImageUtils.IconSize._24));
                            btnSelecionarMaterial.addStyle("warning");
                            btnSelecionarMaterial.disable.bind(emEdicao.not());
                            btnSelecionarMaterial.setAction(evt3 -> {
                                selectMaterialRisco(riscoPlanejamento, corSelecionada, boxSetMateriais, fixarGrade);
                                //selectMaterialOld(riscoPlanejamento, corSelecionada, boxSetMateriais);
                            });
                        })); // button para seleção dos rolos para o risco
                        boxSetMateriais.add(FormFieldText.create(field -> {
                            field.withoutTitle();
                            field.label("Cons. Tt.:");
                            field.postLabel("mt");
                            field.editable(false);
                            field.alignment(Pos.CENTER);
                            field.value.bind(corSelecionada.consumototalProperty().asString("%.4f"));
                        }).build()); // field total de consumo do risco
                        boxSetMateriais.add(FormFieldText.create(field -> {
                            field.withoutTitle();
                            field.label("Cons. Pç.:");
                            field.postLabel("mt");
                            field.editable(false);
                            field.alignment(Pos.CENTER);
                            field.value.bind(corSelecionada.consumopecaProperty().asString("%.4f"));
                        }).build()); // field consumo por peça com o consumo do risco
                        if (corSelecionada.getConsumopecakg().compareTo(BigDecimal.ZERO) != 0) {
                            boxSetMateriais.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.label("Cons. Pç.:");
                                field.postLabel("kg");
                                field.editable(false);
                                field.alignment(Pos.CENTER);
                                field.value.bind(corSelecionada.consumopecakgProperty().asString("%.4f"));
                            }).build()); // field consumo por peça em kg
                        }
                    })); // formulário de seleção dos materiais
                }));
            }));
        });
    }
    
    private void selectMaterialOld(SdRiscoPlanejamentoEncaixe riscoPlanejamento, SdCorRisco corSelecionada, FormBox boxSetMateriais) {
        // validação se foi criado o consumo do risco (montagem da grade)
        if (corSelecionada.getConsumototal().compareTo(BigDecimal.ZERO) <= 0) {
            MessageBox.create(message -> {
                message.message("Você precisa primeiro montar a grade e o consumo para o risco antes de selecionar os rolos.");
                message.type(MessageBox.TypeMessageBox.IMPORTANT);
                message.showAndWait();
            });
            return;
        }
        
        // validação se o risco é detalhe e todos  os riscos de corpo estão com materiais
        if (!Arrays.asList(1, 2, 4).contains(riscoPlanejamento.getTiporisco().getCodigo())) {
            if (planejamentoAberto.getRiscos().stream()
                    .filter(it -> it.getTiporisco().getCodigo() == 1)
                    .anyMatch(it -> it.getCores().stream()
                            .anyMatch(it2 -> it2.getMateriais().size() == 0))) {
                MessageBox.create(message -> {
                    message.message("É necessário que você defina todos os materiais para os riscos do tipo GERAL.");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showAndWait();
                });
                return;
            }
        }
        
        // obtendo os materiais que consomem a aplicação e a cor do produto
        List<VSdConsumosOfCorApl> consumosDoRiscoCor = (List<VSdConsumosOfCorApl>) new FluentDao().selectFrom(VSdConsumosOfCorApl.class)
                .where(it -> it
                        .equal("numero", riscoPlanejamento.getIdplanejamento().getNumero().getNumero())
                        .isIn("cor", checkRiscoAgrupado(corSelecionada.getCor())
                                ? riscoPlanejamento.getStringCoresAgrupadas().replace("'", "").split(",")
                                : Arrays.asList(1, 2, 4).contains(riscoPlanejamento.getTiporisco().getCodigo())
                                ? corSelecionada.getCor().split(",")
                                : getCorIds(corSelecionada.getCor()).stream().map(it2 -> it2.getCor()).toArray())
                        .equal("aplicpcp.codigo", riscoPlanejamento.getAplicacaoft().getCodigo()))
                .resultList();
        
        // rolos reservados para a OF
        final List<VSdMateriaisCorte>[] rolosAplicacaoCor = new List[]{new ArrayList<>()};
        consumosDoRiscoCor.forEach(matConsumo -> {
            List<VSdMateriaisCorte> materiais = (List<VSdMateriaisCorte>) new FluentDao().selectFrom(VSdMateriaisCorte.class).where(it -> it
                    .equal("numero", matConsumo.getNumero())
                    .equal("codigo.codigo", matConsumo.getInsumo().getCodigo())
                    .equal("cori.cor", matConsumo.getCori().getCor())
            ).resultList();
            /** calculando a qtde disponível de cada rolo, é subtraido dos rolos o que já foi consumido no próprio risco
             o cálculo é feito rolo a rolo **/
            List<SdCorRisco> riscosPlanejamento = new ArrayList<>();
            planejamentoAberto.getRiscos().forEach(it -> riscosPlanejamento.addAll(it.getCores()));
            materiais.forEach(rolo -> {
                BigDecimal consumidoOutrosRiscos = new BigDecimal(riscosPlanejamento.stream()
                        .filter(it -> !it.equals(corSelecionada))
                        .mapToDouble(it -> it.getMateriais().stream()
                                .filter(it2 -> it2.equals(rolo))
                                .mapToDouble(it2 -> it2.getConsumido().doubleValue())
                                .sum())
                        .sum());
                BigDecimal consumidoNesteRisco = new BigDecimal(corSelecionada.getMateriais().stream()
                        .filter(it -> it.equals(rolo))
                        .mapToDouble(consumido -> consumido.getConsumido().doubleValue())
                        .sum());
                rolo.setResmtConsumir(rolo.getReservamt().subtract(consumidoOutrosRiscos).subtract(consumidoNesteRisco)); // definição da qtde disponível do rolo subtraindo o que já foi consumido no risco
                rolo.setQtdeConsumido(BigDecimal.ZERO); // Zerando a variável para uso na rotina
                rolo.setFolhas(rolo.getResmtConsumir().divide(corSelecionada.getComprimento().add(new BigDecimal(0.06)), RoundingMode.DOWN).intValue()); // calculo de folhas do rolo com base no risco
                rolo.setFolhasConsumidas(0);
                rolo.setSelected(false);
            });
            rolosAplicacaoCor[0].addAll(materiais);
            rolosAplicacaoCor[0] = rolosAplicacaoCor[0].stream().distinct().collect(Collectors.toList());
        }); // get dos rolos reservados a partir dos consumos da cor do produto
        
        // verificação para mensagem de sem materiais
        if (rolosAplicacaoCor[0].size() == 0) {
            MessageBox.create(message -> {
                message.message("Não foram encontrados materiais reservados para o risco, entre em contato com o PCP.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }
        
        // atributos para exibição de valores consumo em tela
        final DoubleProperty qtdeSelecionadaConsumo = new SimpleDoubleProperty(corSelecionada.getMateriais().stream().mapToDouble(consumido -> consumido.getConsumido().doubleValue()).sum());
        final DoubleProperty saldoConsumoSelecionado = new SimpleDoubleProperty(corSelecionada.getConsumototal().doubleValue() - qtdeSelecionadaConsumo.get());
        final StringProperty viewQtdeSelecionadaConsumo = new SimpleStringProperty();
        viewQtdeSelecionadaConsumo.bind(qtdeSelecionadaConsumo.asString("%.4f"));
        final StringProperty viewSaldoConsumoSelecionado = new SimpleStringProperty();
        viewSaldoConsumoSelecionado.bind(saldoConsumoSelecionado.asString("%.4f"));
        // atributos para exibição de valores de folhas em tela
        final IntegerProperty qtdeFolhasSelecionadaConsumo = new SimpleIntegerProperty(corSelecionada.getMateriais().stream().mapToInt(consumido -> consumido.getQtdefolhas()).sum());
        final IntegerProperty saldoFolhasConsumoSelecionado = new SimpleIntegerProperty(corSelecionada.getFolhas() - qtdeFolhasSelecionadaConsumo.get());
        final StringProperty viewQtdeFolhasSelecionadaConsumo = new SimpleStringProperty();
        viewQtdeFolhasSelecionadaConsumo.bind(qtdeFolhasSelecionadaConsumo.asString());
        final StringProperty viewSaldoFolhasConsumoSelecionado = new SimpleStringProperty();
        viewSaldoFolhasConsumoSelecionado.bind(saldoFolhasConsumoSelecionado.asString());
        
        // seleção material caso risco for corpo
        if (Arrays.asList(1, 2, 4).contains(riscoPlanejamento.getTiporisco().getCodigo())) {
            // table de exibição dos rolos reservados
            final FormTableView<VSdMateriaisCorte> tblMateriais = FormTableView.create(VSdMateriaisCorte.class, table -> {
                table.title("Materiais Cor: " + corSelecionada.getCor());
                table.items.set(FXCollections.observableList(rolosAplicacaoCor[0]));
                table.height(350.0);
                table.editable.set(true);
                table.columns(
                        FormTableColumn.create(cln -> {
                            cln.title("Insumo");
                            cln.width(300.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().toString()));
                        }).build() /*Insumo*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Cor");
                            cln.width(160.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCori().toString()));
                        }).build() /*Cor*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Lote");
                            cln.width(80.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLote()));
                            cln.alignment(Pos.CENTER);
                        }).build() /*Lote*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Ton.");
                            cln.width(80.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTonalidade()));
                            cln.alignment(Pos.CENTER);
                        }).build() /*Tonalidade*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Partida");
                            cln.width(55.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPartida()));
                            cln.alignment(Pos.CENTER);
                        }).build() /*Part.*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Largura");
                            cln.width(55.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLargura()));
                            cln.alignment(Pos.CENTER);
                        }).build() /*Larg.*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Gram.");
                            cln.width(45.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getGramatura()));
                            cln.alignment(Pos.CENTER);
                        }).build() /*Gramatura*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Seq.");
                            cln.width(35.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getSequencia()));
                            cln.alignment(Pos.CENTER);
                        }).build() /*Seq.*/,
                        FormTableColumn.create(cln -> {
                            cln.title("UM");
                            cln.width(30.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getUnidade().getUnidade()));
                            cln.alignment(Pos.CENTER);
                        }).build() /*UM*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Reservado");
                            cln.width(80.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getReservamt()));
                            cln.alignment(Pos.CENTER_RIGHT);
                            cln.format(param -> {
                                return new TableCell<VSdMateriaisCorte, BigDecimal>() {
                                    @Override
                                    protected void updateItem(BigDecimal item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        if (item != null && !empty) {
                                            setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                                        }
                                    }
                                };
                            });
                        }).build() /*Reservado*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Folhas Disp.");
                            cln.width(70.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getFolhas()));
                            cln.alignment(Pos.CENTER);
                            cln.format(param -> {
                                return new TableCell<VSdMateriaisCorte, Integer>() {
                                    @Override
                                    protected void updateItem(Integer item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        if (item != null && !empty) {
                                            setText(item > 0 ? StringUtils.toIntegerFormat(item) : "0");
                                        }
                                    }
                                };
                            });
                        }).build() /*Folhas Disp.*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Disp. Consumo");
                            cln.width(80.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getResmtConsumir()));
                            cln.alignment(Pos.CENTER_RIGHT);
                            cln.format(param -> {
                                return new TableCell<VSdMateriaisCorte, BigDecimal>() {
                                    @Override
                                    protected void updateItem(BigDecimal item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        if (item != null && !empty) {
                                            setText(item.compareTo(BigDecimal.ZERO) > 0 ? StringUtils.toDecimalFormat(item.doubleValue(), 4) : "0.0000");
                                        }
                                    }
                                };
                            });
                        }).build() /*Disp. Consumo*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Folhas Cons.");
                            cln.width(70.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                            cln.alignment(Pos.CENTER);
                            cln.editable.set(true);
                            cln.format(param -> {
                                return new TableCell<VSdMateriaisCorte, VSdMateriaisCorte>() {
                                    @Override
                                    protected void updateItem(VSdMateriaisCorte item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        setGraphic(null);
                                        if (item != null && !empty) {
                                            setGraphic(FormFieldText.create(field -> {
                                                field.withoutTitle();
                                                field.value.set(StringUtils.toIntegerFormat(item.getFolhasConsumidas()));
                                                field.mask(FormFieldText.Mask.INTEGER);
                                                field.alignment(Pos.CENTER);
                                                field.editable.bind(item.selectedProperty());
                                                field.value.addListener((observable, oldValue, newValue) -> {
                                                    if (newValue != null) {
                                                        try {
//                                                                                if (new BigDecimal(StringUtils.unformatDecimal(newValue)).compareTo(BigDecimal.ZERO) < 0 ||
//                                                                                        new BigDecimal(StringUtils.unformatDecimal(newValue)).compareTo(new BigDecimal(item.getFolhasConsumidasOrig())) > 0) {
//                                                                                    MessageBox.create(message -> {
//                                                                                        message.message("Você pode alterar a quantidade de reserva de um rolo, para utilizar no risco, porém ela deve ser maior que ZERO e menor ou igual a RESERVA.");
//                                                                                        message.type(MessageBox.TypeMessageBox.IMPORTANT);
//                                                                                        message.showAndWait();
//                                                                                    });
//                                                                                    item.setFolhasConsumidas(new BigDecimal(StringUtils.unformatDecimal(oldValue)).intValue());
//                                                                                    field.value.set(oldValue);
//                                                                                } else {
                                                            item.setFolhasConsumidas(new BigDecimal(StringUtils.unformatDecimal(newValue)).intValue());
                                                            item.setQtdeConsumido(new BigDecimal(item.getFolhasConsumidas()).multiply(corSelecionada.getComprimento().add(new BigDecimal(0.06))));
                                                            
                                                            BigDecimal difFolhas = new BigDecimal(StringUtils.unformatDecimal(newValue)).subtract(new BigDecimal(StringUtils.unformatDecimal(oldValue)));
                                                            item.setResmtConsumir(item.getResmtConsumir().subtract(difFolhas.multiply(corSelecionada.getComprimento().add(new BigDecimal(0.06)))));
                                                            
                                                            field.value.set(newValue);
                                                            qtdeSelecionadaConsumo.set(rolosAplicacaoCor[0].stream().filter(it -> it.isSelected()).mapToDouble(it -> it.getQtdeConsumido().doubleValue()).sum());
                                                            saldoConsumoSelecionado.set(saldoConsumoSelecionado.get() - qtdeSelecionadaConsumo.get());
                                                            qtdeFolhasSelecionadaConsumo.set(Integer.parseInt(newValue));
                                                            saldoFolhasConsumoSelecionado.set(saldoFolhasConsumoSelecionado.get() - qtdeFolhasSelecionadaConsumo.get());
                                                            table.refresh();
//                                                                                }
                                                        } catch (NumberFormatException exc) {
                                                            exc.printStackTrace();
                                                        }
                                                    }
                                                });
                                            }).build());
                                        }
                                    }
                                };
                            });
                        }).build() /*Folhas Cons.*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Consumido");
                            cln.width(80.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtdeConsumido()));
                            cln.alignment(Pos.CENTER_RIGHT);
                            cln.format(param -> {
                                return new TableCell<VSdMateriaisCorte, BigDecimal>() {
                                    @Override
                                    protected void updateItem(BigDecimal item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        if (item != null && !empty) {
                                            setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                                        }
                                    }
                                };
                            });
                        }).build() /*Consumido*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Baixado");
                            cln.width(80.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getBaixadomt()));
                            cln.alignment(Pos.CENTER_RIGHT);
                            cln.format(param -> {
                                return new TableCell<VSdMateriaisCorte, BigDecimal>() {
                                    @Override
                                    protected void updateItem(BigDecimal item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        if (item != null && !empty) {
                                            setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                                        }
                                    }
                                };
                            });
                        }).build() /*Baixado*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Sld. Estq");
                            cln.width(80.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getSaldomt()));
                            cln.alignment(Pos.CENTER_RIGHT);
                            cln.format(param -> {
                                return new TableCell<VSdMateriaisCorte, BigDecimal>() {
                                    @Override
                                    protected void updateItem(BigDecimal item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        if (item != null && !empty) {
                                            setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                                        }
                                    }
                                };
                            });
                        }).build() /*Saldo Estq*/
                );
                table.factoryRow(param -> {
                    return new TableRow<VSdMateriaisCorte>() {
                        @Override
                        protected void updateItem(VSdMateriaisCorte item, boolean empty) {
                            super.updateItem(item, empty);
                            clear();
                            if (item != null && !empty) {
                                if (item.isSelected())
                                    getStyleClass().add("table-row-success");
                                else if (item.getResmtConsumir().compareTo(BigDecimal.ZERO) <= 0)
                                    getStyleClass().add("table-row-danger");
                            }
                        }
                        
                        private void clear() {
                            getStyleClass().removeAll("table-row-success", "table-row-danger");
                        }
                    };
                });
                table.indices(
                        table.indice(Color.LIGHTGREEN, "Rolo selecionado", false),
                        table.indice(Color.LIGHTCORAL, "Rolos indisponível", false),
                        table.indice(Color.WHITESMOKE, "Rolos disponíveis", false)
                );
            });
            // obtendo as aplicações que consomem o tecido/cor em outras cores de produto diferente deste risco para baixa do rolo
            List<ConsumoTecido> consumosTecido = consumosDoRiscoCor.size() > 0 ? getConsumosTecido(
                    planejamentoAberto.getNumero().getNumero(),
                    (checkRiscoAgrupado(corSelecionada.getCor()) ? riscoPlanejamento.getStringCoresAgrupadas() : corSelecionada.getCor()),
                    consumosDoRiscoCor.get(0).getInsumo().getCodigo(),
                    consumosDoRiscoCor.get(0).getCori().getCor()) : new ArrayList<>();
            // aplicações desta cor do produto diferente da aplicação deste risco
            List<ConsumoTecido> consumosCorProduto = getConsumosCorProduto(
                    planejamentoAberto.getNumero().getNumero(),
                    (checkRiscoAgrupado(corSelecionada.getCor()) ? riscoPlanejamento.getStringCoresAgrupadas() : corSelecionada.getCor()),
                    riscoPlanejamento.getTiporisco().getAplicacao().getCodigo());
            
            // Ajuste do total consumido dos consumos da cor para a grade que está sendo realizada no risco
            consumosCorProduto.forEach(consumo -> {
                consumo.consumoTotal = new BigDecimal(corSelecionada.getGrade().stream().filter(grade -> consumo.faixa.contains("|".concat(grade.getTam()).concat("|"))).mapToDouble(grade -> grade.getRealizado()).sum() * consumo.consumo.doubleValue());
            });
            
            // menor largura e a gramatura correspondente dos rolos reservados para calculos de conversão
            BigDecimal menorLargura = rolosAplicacaoCor[0].stream().map(rolo -> rolo.getLargura()).distinct().sorted(Comparator.comparing(BigDecimal::doubleValue)).findFirst().get();
            OptionalDouble gramaturaRolos = rolosAplicacaoCor[0].stream()
                    .filter(rolo -> rolo.getLargura().compareTo(menorLargura) == 0 && rolo.getGramatura() != null)
                    .mapToDouble(rolo -> rolo.getGramatura().doubleValue())
                    .min();
            BigDecimal gramaturaMenorLargura = new BigDecimal(gramaturaRolos.isPresent() ? gramaturaRolos.getAsDouble() : 0);
            
            // ajustando o consumo das aplicações da cor e do tecido para metros
            consumosTecido.forEach(consumo -> {
                if (consumo.unidade.equals("KG")) {
                    consumo.consumoTotal = consumo.consumoTotal.multiply(new BigDecimal(1000).divide(menorLargura.multiply(gramaturaMenorLargura), RoundingMode.DOWN));
                    consumo.consumo = consumo.consumo.multiply(new BigDecimal(1000).divide(menorLargura.multiply(gramaturaMenorLargura), RoundingMode.DOWN));
                }
            });
            consumosCorProduto.forEach(consumo -> {
                if (consumo.unidade.equals("KG")) {
                    consumo.consumoTotal = consumo.consumoTotal.multiply(new BigDecimal(1000).divide(consumo.largura.multiply(consumo.gramatura), RoundingMode.DOWN));
                    consumo.consumo = consumo.consumo.multiply(new BigDecimal(1000).divide(consumo.largura.multiply(consumo.gramatura), RoundingMode.DOWN));
                }
            });
            
            List<ConsumoTecido> consumosTecidoCorNaCorProduto = consumosCorProduto.stream()
                    .filter(consumo -> consumo.insumo.equals(consumosDoRiscoCor.get(0).getInsumo()) && consumo.corI.equals(consumosDoRiscoCor.get(0).getCori()))
                    .collect(Collectors.toList()); // aplicações desta cor do produto diferente deste risco que usa o mesmo tecido do risco
            // remover da lista de consumosCorProduto as aplicações que utilizam o mesmo tecido
            consumosCorProduto.removeIf(it -> consumosTecidoCorNaCorProduto.contains(it));
            
            // get da quantidade de tonalidades de rolos reservados
//                                    Long qtdeTonsRolos = rolosAplicacaoCor[0].stream()
//                                            .map(rolo -> rolo.getLote().substring(0, 6).concat(rolo.getTonalidade()))
//                                            .distinct()
//                                            .count();
            
            // verificando se já teve um cálculo e remoção de tecido de um rolo para atender uma outra aplicação em outra cor
            if (rolosAplicacaoCor[0].stream().filter(it -> it.getConsumoOutraCor() != null && !it.getConsumoOutraCor().isEmpty()).count() > 0) {
                // remove dos rolos os consumos em outras cores já calculados.
                rolosAplicacaoCor[0].stream().filter(it -> it.getConsumoOutraCor() != null && !it.getConsumoOutraCor().isEmpty()).forEach(it -> {
                    it.setResmtConsumir(it.getResmtConsumir().subtract(new BigDecimal(it.getConsumoOutraCor().values().stream().mapToDouble(BigDecimal::doubleValue).sum())));
                });
            } else {
                // total da qtde consumida do tecido nas outras cores do produto
                final BigDecimal[] qtdeConsumidaOutraCor = {new BigDecimal(consumosTecido.stream()
                        .mapToDouble(consumo -> consumo.consumoTotal.doubleValue())
                        .sum())};
                final BigDecimal qtdeTotalConsumidaOutraCor = qtdeConsumidaOutraCor[0];
                
                // get dos rolos que atendem o total dos consumos do tecido nas outras cores de produto.
                List<VSdMateriaisCorte> rolosAtendemConsumoOutraCor = rolosAplicacaoCor[0].stream()
                        .filter(rolo -> rolo.getResmtConsumir().compareTo(qtdeConsumidaOutraCor[0]) >= 0)
                        .collect(Collectors.toList());
                // teste para verificar se tem UM rolo que atende totalmente o consumo nas outras cores de produto, senão deverá tirar o consumo de mais de um rolo.
                if (rolosAtendemConsumoOutraCor.size() > 0) {
                    rolosAtendemConsumoOutraCor.sort(Comparator.comparing(VSdMateriaisCorte::getResmtConsumir));
                    rolosAtendemConsumoOutraCor.get(0).setResmtConsumir(rolosAtendemConsumoOutraCor.get(0).getResmtConsumir().subtract(qtdeConsumidaOutraCor[0]));
                    Map<String, BigDecimal> dadosRoloOutraCor = new HashMap<>();
                    consumosTecido.stream().collect(Collectors.groupingBy(it -> it.cor, Collectors.summingDouble(it -> it.consumoTotal.doubleValue()))).forEach((key, value) -> {
                        dadosRoloOutraCor.put(key, new BigDecimal(value));
                    });
                    rolosAtendemConsumoOutraCor.get(0).setConsumoOutraCor(FXCollections.observableMap(dadosRoloOutraCor));
                } else {
                    // ordenando os rolos reservados por ordem de qtde disponível para consumir
                    rolosAplicacaoCor[0].sort(Comparator.comparing(VSdMateriaisCorte::getResmtConsumir));
                    rolosAplicacaoCor[0].forEach(rolo -> {
                        Map<String, BigDecimal> dadosRoloOutraCor = new HashMap<>();
                        if (qtdeConsumidaOutraCor[0].compareTo(rolo.getResmtConsumir()) > 0) {
                            // se o rolo reservado a qtde disponível é menor que o consumo, zera a qtde disponível do rolo para não ser utilizado neste risco
                            qtdeConsumidaOutraCor[0] = qtdeConsumidaOutraCor[0].subtract(rolo.getResmtConsumir());
                            rolo.setResmtConsumir(BigDecimal.ZERO);
                            
                            // calcula a porcentagem do consumo de cada cor para guardar o consumo outras cores
                            if (rolosAtendemConsumoOutraCor.size() > 0) {
                                BigDecimal percentConsumo = qtdeTotalConsumidaOutraCor.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : rolo.getResmtConsumir().divide(qtdeTotalConsumidaOutraCor, RoundingMode.DOWN);
                                consumosTecido.stream().collect(Collectors.groupingBy(it -> it.cor, Collectors.summingDouble(it -> it.consumoTotal.doubleValue()))).forEach((key, value) -> {
                                    dadosRoloOutraCor.put(key, new BigDecimal(value).multiply(percentConsumo).setScale(4, RoundingMode.DOWN));
                                });
                                rolosAtendemConsumoOutraCor.get(0).setConsumoOutraCor(FXCollections.observableMap(dadosRoloOutraCor));
                            }
                        } else {
                            // se o rolo atender a qtde, reserva o saldo do consumido em outra cor
                            rolo.setResmtConsumir(rolo.getResmtConsumir().subtract(qtdeConsumidaOutraCor[0]));
                            
                            // calcula a porcentagem do consumo de cada cor para guardar o consumo outras cores
                            BigDecimal percentConsumo = qtdeConsumidaOutraCor[0].divide(qtdeTotalConsumidaOutraCor, RoundingMode.DOWN);
                            consumosTecido.stream().collect(Collectors.groupingBy(it -> it.cor, Collectors.summingDouble(it -> it.consumoTotal.doubleValue()))).forEach((key, value) -> {
                                dadosRoloOutraCor.put(key, new BigDecimal(value).multiply(percentConsumo).setScale(4, RoundingMode.DOWN));
                            });
                            rolosAtendemConsumoOutraCor.get(0).setConsumoOutraCor(FXCollections.observableMap(dadosRoloOutraCor));
                        }
                    });
                }
            }
            
            
            // teste para remover consumo de outras aplicações que consomem o mesmo tecido/cor da cor do produto
            BigDecimal totalConsumoTecidoOutraAplicacao = new BigDecimal(consumosTecidoCorNaCorProduto.stream()
                    .mapToDouble(consumo -> consumo.consumoTotal.doubleValue())
                    .sum());
//                                    if (consumosTecidoCorNaCorProduto.size() > 0) {
//                                        if (qtdeTonsRolos == 1) {
//                                            // remove o consumo da outra aplicação desta cor de produto caso o material seja o mesmo do risco que está sendo realizado
//                                            List<VSdMateriaisCorte> rolosAtendemConsumoCor = rolosAplicacaoCor.stream().filter(rolo -> rolo.getResmtConsumir().compareTo(totalConsumoTecidoOutraAplicacao) > 0).collect(Collectors.toList());
//                                            if (rolosAtendemConsumoCor.size() > 0) {
//                                                rolosAtendemConsumoCor.sort(Comparator.comparing(VSdMateriaisCorte::getResmtConsumir));
//                                                rolosAtendemConsumoCor.get(0).setResmtConsumir(rolosAtendemConsumoCor.get(0).getResmtConsumir().subtract(totalConsumoTecidoOutraAplicacao));
//                                            } else {
//                                                rolosAplicacaoCor.sort(Comparator.comparing(VSdMateriaisCorte::getResmtConsumir));
//                                                final BigDecimal[] qtdeConsumoBaixar = {totalConsumoTecidoOutraAplicacao};
//                                                rolosAplicacaoCor.stream().filter(rolo -> rolo.getResmtConsumir().compareTo(BigDecimal.ZERO) > 0).forEach(rolo -> {
//                                                    if (qtdeConsumoBaixar[0].compareTo(rolo.getResmtConsumir()) > 0) {
//                                                        qtdeConsumoBaixar[0] = qtdeConsumoBaixar[0].subtract(rolo.getResmtConsumir());
//                                                        rolo.setResmtConsumir(BigDecimal.ZERO);
//                                                    } else {
//                                                        rolo.setResmtConsumir(rolo.getResmtConsumir().subtract(qtdeConsumoBaixar[0]));
//                                                    }
//                                                });
//                                            }
//                                        } else {
//
//                                        }
//                                    }
            rolosAplicacaoCor[0].forEach(rolo -> {
                rolo.setFolhas(rolo.getResmtConsumir().divide(corSelecionada.getComprimento().add(new BigDecimal(0.06)), RoundingMode.DOWN).intValue());
                rolo.setFolhasConsumidas(0);
                rolo.setQtdeConsumido(BigDecimal.ZERO);
            }); // re-calculo de folhas do rolo com base no risco
            
            tblMateriais.addColumn(FormTableColumn.create(cln -> {
                cln.title("");
                cln.width(40.0);
                cln.alignment(FormTableColumn.Alignment.CENTER);
                cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                cln.format(param -> {
                    return new TableCell<VSdMateriaisCorte, VSdMateriaisCorte>() {
                        final HBox boxButtonsRow = new HBox(3);
                        final Button btnSelectRolo = FormButton.create(btn -> {
                            btn.tooltip("Selecionar rolo para o risco");
                            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            btn.addStyle("default");
                        });
                        
                        @Override
                        protected void updateItem(VSdMateriaisCorte item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                
                                btnSelectRolo.setGraphic(ImageUtils.getIcon((item.isSelected() ? ImageUtils.Icon.CANCEL : ImageUtils.Icon.ADICIONAR), ImageUtils.IconSize.MANUAL.size(20.0)));
                                //btnSelectRolo.setDisable(item.getResmtConsumir().compareTo(BigDecimal.ZERO) <= 0);
                                btnSelectRolo.setOnAction(evt -> {
                                    Boolean newValue = !item.isSelected();
                                    
                                    BigDecimal consumoPecaCorRisco = new BigDecimal(consumosTecidoCorNaCorProduto.stream()
                                            .mapToDouble(consumo -> consumo.consumo.doubleValue())
                                            .sum()); // consumo por peça do tecido do risco em outra aplicação da mesma cor
                                    BigDecimal consumoTotalCorRisco = new BigDecimal(consumosTecidoCorNaCorProduto.stream()
                                            .mapToDouble(consumo -> consumo.consumoTotal.doubleValue())
                                            .sum()); // total consumo do tecido do risco em outra aplicação da mesma cor
                                    BigDecimal consumoJaSelecionado = new BigDecimal(qtdeSelecionadaConsumo.get()).setScale(4, RoundingMode.DOWN); // saldo do consumo do risco
                                    BigDecimal saldoConsumo = corSelecionada.getConsumototal().subtract(consumoJaSelecionado); // saldo do consumo do risco
                                    Integer folhasTecidoSelecionado = consumoJaSelecionado.divide(corSelecionada.getComprimento().add(new BigDecimal(0.06)), RoundingMode.DOWN).intValue(); // qtde folhas do tecido já reservado
                                    Integer saldoFolhas = corSelecionada.getFolhas() - folhasTecidoSelecionado.intValue(); // saldo das folhas que ainda precisa selecionar material
                                    Long qtdeTonsSelecionados = rolosAplicacaoCor[0].stream()
                                            .filter(it -> it.isSelected())
                                            .map(it -> it.getTonalidade())
                                            .distinct()
                                            .count(); // qtde de tons selecionados
                                    
                                    // exibição de mensagem caso o usuário seleciona um novo rolo porém o consumo já foi totalmente atendido
                                    if (saldoConsumo.compareTo(BigDecimal.ZERO) <= 0 && newValue) {
                                        MessageBox.create(message -> {
                                            message.message("Os rolos já selecionados atendem o consumo do risco, verifique se realmente é necessário a inclusão deste rolo.");
                                            message.type(MessageBox.TypeMessageBox.ALERT);
                                            message.showAndWait();
                                        });
                                    }
                                    
                                    Integer folhasAConsumir = item.getFolhas() > saldoFolhas ? saldoFolhas : item.getFolhas();
                                    BigDecimal necessidadeConsumo = saldoConsumo.add(new BigDecimal(saldoFolhas * corSelecionada.getTotalGrade()).multiply(consumoPecaCorRisco));
                                    
                                    AtomicReference<BigDecimal> consumoAplicacaoRolo = new AtomicReference<>(BigDecimal.ZERO);
                                    if (newValue) {
                                        item.setSelected(true);
                                        
                                        // adicionar ao consumo
                                        if (item.getResmtConsumir().compareTo(necessidadeConsumo) >= 0) {
                                            // caso o rolo atende o consumo do risco mais o tecido em outras aplicações nesta cor de produto
                                            // o consumo da aplicação será o que está selecionado no rolo
                                            consumoAplicacaoRolo.set(saldoConsumo);
                                            item.setQtdeConsumido(saldoConsumo);
                                            item.setFolhasConsumidas(saldoConsumo.divide(corSelecionada.getComprimento().add(new BigDecimal(0.06)), RoundingMode.DOWN).intValue());
                                            item.setFolhasConsumidasOrig(item.getFolhasConsumidas());
                                        } else {
                                            // caso o rolo não atende o consumo, será perguntado se quer selecionar mesmo assim esse rolo
                                            if ((Boolean) QuestionBox.build(Boolean.class, message -> {
                                                message.message("O rolo selecionado não atende o consumo para a grade gerada. Deseja realmente adicionar o rolo? Nesta rotina será removido o necessário deste rolo para consumir em outras aplicações que utilizam esse tecido e cor no produto.");
                                                message.showAndWait();
                                            }).value.get()) {
                                                BigDecimal disponivelUsoRolo = item.getResmtConsumir().subtract(new BigDecimal(folhasAConsumir * corSelecionada.getTotalGrade()).multiply(consumoPecaCorRisco));
                                                item.setFolhasConsumidas(disponivelUsoRolo.divide(corSelecionada.getComprimento().add(new BigDecimal(0.06)), RoundingMode.DOWN).intValue());
                                                item.setQtdeConsumido(new BigDecimal(item.getFolhasConsumidas()).multiply(corSelecionada.getComprimento().add(new BigDecimal(0.06))));
                                                item.setFolhasConsumidasOrig(item.getFolhasConsumidas());
                                                consumoAplicacaoRolo.set(item.getQtdeConsumido().setScale(4, RoundingMode.DOWN));
                                            } else {
                                                item.setSelected(false);
                                            }
                                        }
                                    } else {
                                        // remover do consumo
                                        consumoAplicacaoRolo.set(item.getQtdeConsumido().negate());
                                        item.setQtdeConsumido(BigDecimal.ZERO);
                                        item.setFolhasConsumidas(0);
                                        item.setSelected(false);
                                    }
                                    
                                    // saldo do rolo
                                    item.setResmtConsumir(item.getResmtConsumir().subtract(consumoAplicacaoRolo.get()));
                                    
                                    // atualização de valores para acumulo e exibição em tela
                                    qtdeSelecionadaConsumo.set(qtdeSelecionadaConsumo.add(consumoAplicacaoRolo.get().doubleValue()).doubleValue());
                                    saldoConsumoSelecionado.set(saldoConsumo.subtract(consumoAplicacaoRolo.get()).doubleValue());
                                    qtdeFolhasSelecionadaConsumo.set(item.getFolhas());
                                    saldoFolhasConsumoSelecionado.set(saldoFolhas - item.getFolhas());
                                    
                                    tblMateriais.refresh();
                                });
                                boxButtonsRow.getChildren().clear();
                                boxButtonsRow.getChildren().addAll(btnSelectRolo);
                                setGraphic(boxButtonsRow);
                            }
                        }
                    };
                });
            }).build() /*Ações*/);
            
            // preparação para exibição em tela
            tblMateriais.refresh();
            
            // criação do fragmento para seleção dos rolos
            new Fragment().show(fragment1 -> {
                fragment1.title("Seleção de Rolos para Risco");
                fragment1.size(1500.0, 700.0);
                fragment1.box.getChildren().add(tblMateriais.build());
                fragment1.box.getChildren().add(FormBox.create(boxConsumoReserva -> {
                    boxConsumoReserva.horizontal();
                    boxConsumoReserva.height(350.0);
                    boxConsumoReserva.add(FormBox.create(boxDadosConsumo -> {
                        boxDadosConsumo.vertical();
                        boxDadosConsumo.width(450.0);
                        boxDadosConsumo.add(FormBox.create(dadosReservaConsumo -> {
                            dadosReservaConsumo.horizontal();
                            dadosReservaConsumo.add(
                                    FormFieldText.create(field -> {
                                        field.title("Reserva Total");
                                        field.editable(false);
                                        field.expanded();
                                        field.addStyle("primary");
                                        field.value.set(StringUtils.toDecimalFormat(rolosAplicacaoCor[0].stream().mapToDouble(rolo -> rolo.getReservamt().doubleValue()).sum(), 4));
                                        field.alignment(Pos.CENTER_RIGHT);
                                    }).build(), // reserva total
                                    FormFieldText.create(field -> {
                                        field.title("Disponível Risco");
                                        field.editable(false);
                                        field.expanded();
                                        field.addStyle("success");
                                        field.value.set(StringUtils.toDecimalFormat(rolosAplicacaoCor[0].stream().mapToDouble(rolo -> rolo.getResmtConsumir().doubleValue()).sum(), 4));
                                        field.alignment(Pos.CENTER_RIGHT);
                                    }).build(), // reserva disponível para o risco
                                    FormFieldText.create(field -> {
                                        field.title("Cons. Mesma Cor");
                                        field.editable(false);
                                        field.expanded();
                                        field.addStyle("warning");
                                        field.value.set(StringUtils.toDecimalFormat(consumosTecidoCorNaCorProduto.stream()
                                                .mapToDouble(consumo -> consumo.consumoTotal.doubleValue())
                                                .sum(), 4));
                                        field.alignment(Pos.CENTER_RIGHT);
                                    }).build(), // consumos na mesma cor em outras aplicações
                                    FormFieldText.create(field -> {
                                        field.title("Cons. Outra Cor");
                                        field.editable(false);
                                        field.expanded();
                                        field.addStyle("danger");
                                        field.value.set(StringUtils.toDecimalFormat(consumosTecido.stream().mapToDouble(consumo -> consumo.consumoTotal.doubleValue()).sum(), 4));
                                        field.alignment(Pos.CENTER_RIGHT);
                                    }).build()  // consumos em outras cores
                            );
                        })); // dados de reservas e consumos aplicações
                        boxDadosConsumo.add(new Separator(Orientation.HORIZONTAL));
                        boxDadosConsumo.add(FormBox.create(dadosReservaConsumo -> {
                            dadosReservaConsumo.horizontal();
                            dadosReservaConsumo.add(
                                    FormFieldText.create(field -> {
                                        field.title("Consumo Risco");
                                        field.editable(false);
                                        field.expanded();
                                        field.addStyle("lg");
                                        field.value.set(StringUtils.toDecimalFormat(corSelecionada.getConsumototal().doubleValue(), 4));
                                        field.alignment(Pos.CENTER_RIGHT);
                                        if (new BigDecimal(rolosAplicacaoCor[0].stream().mapToDouble(rolo -> rolo.getResmtConsumir().doubleValue()).sum()).compareTo(corSelecionada.getConsumototal()) < 0) {
                                            field.addStyle("danger");
                                            field.tooltip("Não existe material reservado o suficiente para atender o risco.");
                                        }
                                    }).build(), // consumo do risco
                                    FormFieldText.create(field -> {
                                        field.title("Qtde. Alocado");
                                        field.editable(false);
                                        field.expanded();
                                        field.addStyle("lg").addStyle("success");
                                        field.value.bind(viewQtdeSelecionadaConsumo);
                                        field.alignment(Pos.CENTER_RIGHT);
                                    }).build(), // qtde alocado
                                    FormFieldText.create(field -> {
                                        field.title("Saldo");
                                        field.editable(false);
                                        field.expanded();
                                        field.addStyle("lg").addStyle("warning");
                                        field.value.bind(viewSaldoConsumoSelecionado);
                                        field.alignment(Pos.CENTER_RIGHT);
                                    }).build()  // saldo
                            );
                        })); // dados de seleção de rolos (metragem)
                        boxDadosConsumo.add(FormBox.create(dadosReservaConsumo -> {
                            dadosReservaConsumo.horizontal();
                            dadosReservaConsumo.add(
                                    FormFieldText.create(field -> {
                                        field.title("Consumo Risco");
                                        field.editable(false);
                                        field.expanded();
                                        field.addStyle("lg");
                                        field.value.set(StringUtils.toIntegerFormat(corSelecionada.getFolhas()));
                                        field.alignment(Pos.CENTER_RIGHT);
                                        if (new BigDecimal(rolosAplicacaoCor[0].stream().mapToDouble(rolo -> rolo.getResmtConsumir().doubleValue()).sum()).compareTo(corSelecionada.getConsumototal()) < 0) {
                                            field.addStyle("danger");
                                            field.tooltip("Não existe material reservado o suficiente para atender o risco.");
                                        }
                                    }).build(), // consumo do risco
                                    FormFieldText.create(field -> {
                                        field.title("Qtde. Alocado");
                                        field.editable(false);
                                        field.expanded();
                                        field.addStyle("lg").addStyle("success");
                                        field.value.bind(viewQtdeFolhasSelecionadaConsumo);
                                        field.alignment(Pos.CENTER_RIGHT);
                                    }).build(), // qtde alocado
                                    FormFieldText.create(field -> {
                                        field.title("Saldo");
                                        field.editable(false);
                                        field.expanded();
                                        field.addStyle("lg").addStyle("warning");
                                        field.value.bind(viewSaldoFolhasConsumoSelecionado);
                                        field.alignment(Pos.CENTER_RIGHT);
                                    }).build()  // saldo
                            );
                        })); // dados de seleção de rolos (folhas)
                        boxDadosConsumo.add(new Separator(Orientation.HORIZONTAL));
                        boxDadosConsumo.add(FormBox.create(boxInfosAdicionais -> {
                            boxInfosAdicionais.horizontal();
                            boxInfosAdicionais.add(FormFieldText.create(field -> {
                                field.title("Comp. Folha");
                                field.value.set(StringUtils.toDecimalFormat(corSelecionada.getComprimento().doubleValue(), 4));
                                field.addStyle("info");
                                field.editable(false);
                            }).build());
                            boxInfosAdicionais.add(FormFieldText.create(field -> {
                                field.title("Risco");
                                field.value.set(corSelecionada.getDescrisco());
                                field.editable(false);
                            }).build());
                            boxInfosAdicionais.add(FormFieldText.create(field -> {
                                field.title("Cor");
                                field.value.set(corSelecionada.getCor());
                                field.editable(false);
                            }).build());
                        }));
                        boxDadosConsumo.add(FormBox.create(boxInfosAdicionais -> {
                            boxInfosAdicionais.horizontal();
                            boxInfosAdicionais.add(FormFieldText.create(field -> {
                                field.title("Tot. Peças");
                                field.value.set(StringUtils.toIntegerFormat(corSelecionada.getTotalRealizado()));
                                field.editable(false);
                            }).build());
                            boxInfosAdicionais.add(FormFieldText.create(field -> {
                                field.title("Tot. Saldo");
                                field.value.set(StringUtils.toIntegerFormat(corSelecionada.getTotalSaldoOf()));
                                field.editable(false);
                            }).build());
                        }));
                    })); // box com os totais de consumo e selecionado
                    boxConsumoReserva.add(FormBox.create(boxConsumos -> {
                        boxConsumos.vertical();
                        boxConsumos.add(FormTableView.create(ConsumoTecido.class, table -> {
                            table.title("Consumos Material/Cor neste Produto/Cor");
                            table.items.set(FXCollections.observableList(consumosTecidoCorNaCorProduto));
                            table.columns(
                                    FormTableColumn.create(cln -> {
                                        cln.title("Cor Prod.");
                                        cln.width(60.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<ConsumoTecido, ConsumoTecido>, ObservableValue<ConsumoTecido>>) param -> new ReadOnlyObjectWrapper(param.getValue().cor));
                                        cln.alignment(Pos.CENTER);
                                    }).build() /*Cor Prod.*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Aplic.");
                                        cln.width(50.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<ConsumoTecido, ConsumoTecido>, ObservableValue<ConsumoTecido>>) param -> new ReadOnlyObjectWrapper(param.getValue().aplicacao));
                                        cln.alignment(Pos.CENTER);
                                    }).build() /*Aplicação*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Desc. Aplic.");
                                        cln.width(160.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<ConsumoTecido, ConsumoTecido>, ObservableValue<ConsumoTecido>>) param -> new ReadOnlyObjectWrapper(param.getValue().descAplicacao));
                                    }).build() /*Desc. Aplic.*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Insumo");
                                        cln.width(80.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<ConsumoTecido, ConsumoTecido>, ObservableValue<ConsumoTecido>>) param -> new ReadOnlyObjectWrapper(param.getValue().insumo));
                                        cln.alignment(Pos.CENTER);
                                    }).build() /*Insumo*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Cor Ins.");
                                        cln.width(60.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<ConsumoTecido, ConsumoTecido>, ObservableValue<ConsumoTecido>>) param -> new ReadOnlyObjectWrapper(param.getValue().corI));
                                        cln.alignment(Pos.CENTER);
                                    }).build() /*Cor Ins.*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Consumo");
                                        cln.width(70.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<ConsumoTecido, ConsumoTecido>, ObservableValue<ConsumoTecido>>) param -> new ReadOnlyObjectWrapper(StringUtils.toDecimalFormat(param.getValue().consumoTotal.doubleValue(), 4)));
                                        cln.alignment(Pos.CENTER_RIGHT);
                                    }).build() /*Consumo*/);
                        }).build()); // table consumos da cor do risco com o tecido do risco
                        boxConsumos.add(FormTableView.create(ConsumoTecido.class, table -> {
                            table.title("Consumos Material/Cor");
                            table.items.set(FXCollections.observableList(consumosTecido));
                            table.columns(
                                    FormTableColumn.create(cln -> {
                                        cln.title("Cor Prod.");
                                        cln.width(60.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<ConsumoTecido, ConsumoTecido>, ObservableValue<ConsumoTecido>>) param -> new ReadOnlyObjectWrapper(param.getValue().cor));
                                        cln.alignment(Pos.CENTER);
                                    }).build() /*Cor Prod.*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Aplic.");
                                        cln.width(50.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<ConsumoTecido, ConsumoTecido>, ObservableValue<ConsumoTecido>>) param -> new ReadOnlyObjectWrapper(param.getValue().aplicacao));
                                        cln.alignment(Pos.CENTER);
                                    }).build() /*Aplicação*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Desc. Aplic.");
                                        cln.width(160.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<ConsumoTecido, ConsumoTecido>, ObservableValue<ConsumoTecido>>) param -> new ReadOnlyObjectWrapper(param.getValue().descAplicacao));
                                    }).build() /*Desc. Aplic.*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Insumo");
                                        cln.width(80.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<ConsumoTecido, ConsumoTecido>, ObservableValue<ConsumoTecido>>) param -> new ReadOnlyObjectWrapper(param.getValue().insumo));
                                        cln.alignment(Pos.CENTER);
                                    }).build() /*Insumo*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Cor Ins.");
                                        cln.width(60.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<ConsumoTecido, ConsumoTecido>, ObservableValue<ConsumoTecido>>) param -> new ReadOnlyObjectWrapper(param.getValue().corI));
                                        cln.alignment(Pos.CENTER);
                                    }).build() /*Cor Ins.*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Consumo");
                                        cln.width(70.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<ConsumoTecido, ConsumoTecido>, ObservableValue<ConsumoTecido>>) param -> new ReadOnlyObjectWrapper(StringUtils.toDecimalFormat(param.getValue().consumoTotal.doubleValue(), 4)));
                                        cln.alignment(Pos.CENTER_RIGHT);
                                    }).build() /*Consumo*/);
                        }).build()); // table consumos de outras cores com o tecido do risco
                        boxConsumos.add(FormTableView.create(ConsumoTecido.class, table -> {
                            table.title("Outros Consumos neste Produto/Cor");
                            table.items.set(FXCollections.observableList(consumosCorProduto));
                            table.columns(
                                    FormTableColumn.create(cln -> {
                                        cln.title("Cor Prod.");
                                        cln.width(60.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<ConsumoTecido, ConsumoTecido>, ObservableValue<ConsumoTecido>>) param -> new ReadOnlyObjectWrapper(param.getValue().cor));
                                        cln.alignment(Pos.CENTER);
                                    }).build() /*Cor Prod.*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Aplic.");
                                        cln.width(50.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<ConsumoTecido, ConsumoTecido>, ObservableValue<ConsumoTecido>>) param -> new ReadOnlyObjectWrapper(param.getValue().aplicacao));
                                        cln.alignment(Pos.CENTER);
                                    }).build() /*Aplicação*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Desc. Aplic.");
                                        cln.width(160.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<ConsumoTecido, ConsumoTecido>, ObservableValue<ConsumoTecido>>) param -> new ReadOnlyObjectWrapper(param.getValue().descAplicacao));
                                    }).build() /*Desc. Aplic.*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Insumo");
                                        cln.width(80.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<ConsumoTecido, ConsumoTecido>, ObservableValue<ConsumoTecido>>) param -> new ReadOnlyObjectWrapper(param.getValue().insumo));
                                        cln.alignment(Pos.CENTER);
                                    }).build() /*Insumo*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Cor Ins.");
                                        cln.width(60.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<ConsumoTecido, ConsumoTecido>, ObservableValue<ConsumoTecido>>) param -> new ReadOnlyObjectWrapper(param.getValue().corI));
                                        cln.alignment(Pos.CENTER);
                                    }).build() /*Cor Ins.*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Consumo");
                                        cln.width(70.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<ConsumoTecido, ConsumoTecido>, ObservableValue<ConsumoTecido>>) param -> new ReadOnlyObjectWrapper(StringUtils.toDecimalFormat(param.getValue().consumoTotal.doubleValue(), 4)));
                                        cln.alignment(Pos.CENTER_RIGHT);
                                    }).build() /*Consumo*/);
                        }).build()); // table outros consumos da cor do risco
                    })); // box com as tabelas de aplicações/consumos
                    boxConsumoReserva.add(FormTableView.create(SdMateriaisRiscoCor.class, table -> {
                        table.title("Rolos Selecionados");
                        table.expanded();
                        table.items.bind(corSelecionada.materiaisObservableProperty());
                        table.editable.set(true);
                        table.columns(
                                FormTableColumn.create(cln -> {
                                    cln.title("Insumo");
                                    cln.width(170.0);
                                    cln.value((Callback<TableColumn.CellDataFeatures<SdMateriaisRiscoCor, SdMateriaisRiscoCor>, ObservableValue<SdMateriaisRiscoCor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getMaterial()));
                                }).build() /*Insumo*/,
                                FormTableColumn.create(cln -> {
                                    cln.title("Cor");
                                    cln.width(60.0);
                                    cln.value((Callback<TableColumn.CellDataFeatures<SdMateriaisRiscoCor, SdMateriaisRiscoCor>, ObservableValue<SdMateriaisRiscoCor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCori()));
                                }).build() /*Cor*/,
                                FormTableColumn.create(cln -> {
                                    cln.title("Lote");
                                    cln.width(75.0);
                                    cln.value((Callback<TableColumn.CellDataFeatures<SdMateriaisRiscoCor, SdMateriaisRiscoCor>, ObservableValue<SdMateriaisRiscoCor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLote()));
                                    cln.alignment(Pos.CENTER);
                                }).build() /*Lote*/,
                                FormTableColumn.create(cln -> {
                                    cln.title("Ton.");
                                    cln.width(60.0);
                                    cln.value((Callback<TableColumn.CellDataFeatures<SdMateriaisRiscoCor, SdMateriaisRiscoCor>, ObservableValue<SdMateriaisRiscoCor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTonalidade()));
                                    cln.alignment(Pos.CENTER);
                                }).build() /*Tonalidade*/,
                                FormTableColumn.create(cln -> {
                                    cln.title("Partida");
                                    cln.width(55.0);
                                    cln.value((Callback<TableColumn.CellDataFeatures<SdMateriaisRiscoCor, SdMateriaisRiscoCor>, ObservableValue<SdMateriaisRiscoCor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPartida()));
                                    cln.alignment(Pos.CENTER);
                                }).build() /*Part.*/,
                                FormTableColumn.create(cln -> {
                                    cln.title("Seq.");
                                    cln.width(35.0);
                                    cln.value((Callback<TableColumn.CellDataFeatures<SdMateriaisRiscoCor, SdMateriaisRiscoCor>, ObservableValue<SdMateriaisRiscoCor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getSequencia()));
                                    cln.alignment(Pos.CENTER);
                                }).build() /*Seq.*/,
                                FormTableColumn.create(cln -> {
                                    cln.title("Consumido");
                                    cln.width(70.0);
                                    cln.value((Callback<TableColumn.CellDataFeatures<SdMateriaisRiscoCor, SdMateriaisRiscoCor>, ObservableValue<SdMateriaisRiscoCor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getConsumido()));
                                    cln.alignment(Pos.CENTER_RIGHT);
                                    cln.format(param -> {
                                        return new TableCell<VSdMateriaisCorte, BigDecimal>() {
                                            @Override
                                            protected void updateItem(BigDecimal item, boolean empty) {
                                                super.updateItem(item, empty);
                                                setText(null);
                                                if (item != null && !empty) {
                                                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                                                }
                                            }
                                        };
                                    });
                                }).build() /*Consumido*/
                        );
                        table.contextMenu(FormContextMenu.create(menu -> {
                            menu.addItem(menuItem -> {
                                menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.TRASH, ImageUtils.IconSize._16));
                                menuItem.setText("Desmarcar Rolo");
                                menuItem.setOnAction(evt -> {
                                    SdMateriaisRiscoCor roloParaExcluir = (SdMateriaisRiscoCor) table.selectedItem();
                                    if (roloParaExcluir != null) {
                                        VSdMateriaisCorte roloReservado = rolosAplicacaoCor[0].stream()
                                                .filter(it -> it.getCodigo().getCodigo().equals(roloParaExcluir.getMaterial()) && it.getCori().getCor().equals(roloParaExcluir.getCori()) && it.getLote().equals(roloParaExcluir.getLote()) &&
                                                        it.getPartida().equals(roloParaExcluir.getPartida()) && it.getSequencia().equals(roloParaExcluir.getSequencia()) && it.getTonalidade().equals(roloParaExcluir.getTonalidade()))
                                                .findAny().get();
                                        
                                        roloReservado.setResmtConsumir(roloReservado.getResmtConsumir().add(roloParaExcluir.getConsumido()));
                                        roloReservado.setFolhas(roloReservado.getResmtConsumir().divide(corSelecionada.getComprimento().add(new BigDecimal(0.06)), RoundingMode.DOWN).intValue());
                                        roloReservado.setQtdeConsumido(BigDecimal.ZERO);
                                        roloReservado.setFolhasConsumidas(0);
                                        roloReservado.setSelected(false);
                                        qtdeSelecionadaConsumo.set(qtdeSelecionadaConsumo.subtract(roloParaExcluir.getConsumido().doubleValue()).doubleValue());
                                        saldoConsumoSelecionado.set(saldoConsumoSelecionado.add(roloParaExcluir.getConsumido().doubleValue()).doubleValue());
                                        
                                        qtdeFolhasSelecionadaConsumo.set(qtdeFolhasSelecionadaConsumo.get() - roloParaExcluir.getQtdefolhas());
                                        saldoFolhasConsumoSelecionado.set(saldoFolhasConsumoSelecionado.get() + roloParaExcluir.getQtdefolhas());
                                        
                                        //new FluentDao().delete(roloParaExcluir);
                                        try {
                                            new NativeDAO().runNativeQueryUpdate(String.format("delete from sd_materiais_risco_cor_001 where id = %s", roloParaExcluir.getId()));
                                            corSelecionada.getMateriais().remove(roloParaExcluir);
                                            
                                            SysLogger.addSysDelizLog("Planejamento Encaixe", TipoAcao.EDITAR, planejamentoAberto.getNumero().getNumero(),
                                                    "Excluído rolos na cor ID " + corSelecionada.getId() + " no risco ID " + riscoPlanejamento.getId()
                                                            + " (" + riscoPlanejamento.getTiporisco().getCodrisco() + riscoPlanejamento.getSeqTipo() + ") no planejamento ID " + planejamentoAberto.getId());
                                            
                                            table.refresh();
                                            tblMateriais.refresh();
                                        } catch (SQLException e) {
                                            e.printStackTrace();
                                            ExceptionBox.build(message -> {
                                                message.exception(e);
                                                message.showAndWait();
                                            });
                                        }
                                    }
                                });
                            });
                        }));
                    }).build()); // table dos rolos selecionados
                }));
                fragment1.buttonsBox.getChildren().add(FormButton.create(btnSelecionar -> {
                    btnSelecionar.title("Selecionar Rolos");
                    btnSelecionar.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                    btnSelecionar.addStyle("success");
                    btnSelecionar.setAction(event -> {
                        // validação de rolos selecionados
                        if (rolosAplicacaoCor[0].stream().filter(rolo -> rolo.isSelected()).count() == 0) {
                            MessageBox.create(message -> {
                                message.message("Você precisa selecionar ao menos um rolo para o risco.");
                                message.type(MessageBox.TypeMessageBox.IMPORTANT);
                                message.showAndWait();
                            });
                            return;
                        }
                        
                        BigDecimal qtdeTotalSelecionada = new BigDecimal(rolosAplicacaoCor[0].stream().filter(rolo -> rolo.isSelected()).mapToDouble(rolo -> rolo.getQtdeConsumido().doubleValue()).sum());
                        final BigDecimal[] qtdeConsumoRisco = {corSelecionada.getConsumototal().subtract(new BigDecimal(corSelecionada.getMateriais().stream().mapToDouble(materiais -> materiais.getConsumido().doubleValue()).sum()))};
                        // alerta se a qtde dos rolos é suficiente para a reserva com confirmação se que continuar
                        if (qtdeTotalSelecionada.setScale(4, RoundingMode.DOWN).compareTo(qtdeConsumoRisco[0].setScale(4, RoundingMode.DOWN)) < 0) {
                            if (!((Boolean) QuestionBox.build(Boolean.class, message -> {
                                message.message("A quantidade de reserva selecionada é menor que o risco está consumindo, estão faltando " +
                                        StringUtils.toDecimalFormat(qtdeTotalSelecionada.subtract(corSelecionada.getConsumototal()).doubleValue(), 4) +
                                        ". Deseja processeguir o risco com a falta do material?");
                                message.showAndWait();
                            }).value.get())) {
                                return;
                            }
                        }
                        
                        rolosAplicacaoCor[0].stream().filter(rolo -> rolo.isSelected()).forEach(rolo -> {
                            BigDecimal qtdeParaConsumir = new BigDecimal(rolo.getFolhasConsumidas()).multiply(corSelecionada.getComprimento().add(new BigDecimal(0.06)));
//                                                if (rolo.getResmtConsumir().compareTo(qtdeConsumoRisco[0]) > 0) {
//                                                    qtdeParaConsumir = qtdeConsumoRisco[0];
//                                                } else {
//                                                    qtdeConsumoRisco[0] = qtdeConsumoRisco[0].subtract(rolo.getResmtConsumir());
//                                                }
                            
                            SdMateriaisRiscoCor roloSelecionado = new SdMateriaisRiscoCor(corSelecionada.getId(), checkRiscoAgrupado(corSelecionada.getCor()) ? null : corSelecionada.getCor(), rolo.getCodigo().getCodigo(), rolo.getCori().getCor(),
                                    rolo.getLote(), rolo.getTonalidade(), rolo.getPartida(), rolo.getLargura(),
                                    rolo.getGramatura(), rolo.getSequencia(), qtdeParaConsumir, rolo.getFolhasConsumidas(),
                                    (rolo.getFolhasConsumidas() * corSelecionada.getTotalGrade()));
                            try {
                                roloSelecionado = new FluentDao().persist(roloSelecionado);
                                corSelecionada.getMateriais().add(roloSelecionado);
                            } catch (SQLException e) {
                                e.printStackTrace();
                                ExceptionBox.build(message -> {
                                    message.exception(e);
                                    message.showAndWait();
                                });
                            }
                        });
                        
                        if (rolosAplicacaoCor[0].stream().filter(it -> it.isSelected()).findAny().get().getCodigo().getUnidade().getUnidade().toUpperCase().equals("KG")) {
                            VSdMateriaisCorte rolo = rolosAplicacaoCor[0].stream().filter(it -> it.isSelected()).findAny().get();
                            if (corSelecionada.getConsumopecakg().compareTo(BigDecimal.ZERO) == 0) {
                                if (((VBox) boxSetMateriais.getChildren().get(0)).getChildren().size() > 3)
                                    ((VBox) boxSetMateriais.getChildren().get(0)).getChildren().remove(3);
                                
                                boxSetMateriais.add(FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.label("Cons. Pç.:");
                                    field.postLabel("kg");
                                    field.editable(false);
                                    field.alignment(Pos.CENTER);
                                    field.value.bind(corSelecionada.consumopecakgProperty().asString("%.4f"));
                                }).build());
                            }
                            
                            BigDecimal calc1 = rolo.getGramatura().multiply(rolo.getLargura());
                            BigDecimal calc2 = new BigDecimal(1000).divide(calc1, 15, RoundingMode.DOWN);
                            BigDecimal calc3 = corSelecionada.getConsumototal().divide(calc2, 15, RoundingMode.DOWN);
                            BigDecimal calc4 = calc3.divide(new BigDecimal(corSelecionada.getTotalRealizado()), 4, RoundingMode.DOWN);
                            
                            corSelecionada.setConsumopecakg(calc4);
                        }
                        fragment1.close();
                        
                        
                        SysLogger.addSysDelizLog("Planejamento Encaixe", TipoAcao.EDITAR, planejamentoAberto.getNumero().getNumero(),
                                "Selecionado rolos para a cor ID " + corSelecionada.getId() + " no risco ID " + riscoPlanejamento.getId()
                                        + " (" + riscoPlanejamento.getTiporisco().getCodrisco() + riscoPlanejamento.getSeqTipo() + ") no planejamento ID " + planejamentoAberto.getId());
                    });
                }));
            });
        } else {
            // verificação se o material do risco que está sendo feito não é contrastante com o corpo (mesmo tecido para o corpo e detalhe) *** precisa respeitar tonalidades ***
            boolean isMesmaCorCorpo = consumosDoRiscoCor.stream().map(cor -> cor.getCori().getCor()).anyMatch(cor -> corSelecionada.getRiscoCorpo().stream().anyMatch(it -> it.getMateriais().stream().anyMatch(rolo -> rolo.getCori().equals(cor))));
            if (isMesmaCorCorpo && consumosDoRiscoCor.stream().anyMatch(it -> corSelecionada.getRiscoCorpo().stream().anyMatch(it2 -> it2.getMateriais().stream().anyMatch(it3 -> it3.getMaterial().equals(it.getInsumo().getCodigo()))))) {
                rolosAplicacaoCor[0].removeIf(roloReserva -> corSelecionada.getRiscoCorpo().stream().noneMatch(it -> it.getMateriais().stream().noneMatch(roloCorpo -> roloCorpo.equals(roloReserva))));
                // remove dos rolos os consumos em outras cores já calculados.
                rolosAplicacaoCor[0].stream().filter(it -> it.getConsumoOutraCor() != null && !it.getConsumoOutraCor().isEmpty()).forEach(it -> {
                    it.setResmtConsumir(it.getResmtConsumir().subtract(new BigDecimal(it.getConsumoOutraCor().values().stream().mapToDouble(BigDecimal::doubleValue).sum())));
                });
            } else if (consumosDoRiscoCor.stream().anyMatch(it -> corSelecionada.getRiscoCorpo().stream().anyMatch(it2 -> it2.getMateriais().stream().anyMatch(it3 -> it3.getMaterial().equals(it.getInsumo().getCodigo()) && it3.getCori().equals(it.getCori().getCor()))))) {
                rolosAplicacaoCor[0].removeIf(roloReserva -> roloReserva.getConsumoOutraCor() == null ||
                        roloReserva.getConsumoOutraCor().isEmpty() ||
                        (checkRiscoAgrupado(corSelecionada.getCor())
                                ? !roloReserva.getConsumoOutraCor().containsKey(corSelecionada.getCor())
                                : Arrays.asList(riscoPlanejamento.getStringCoresAgrupadas().replace("'", "").split(",")).stream().anyMatch(corAgrupada -> !roloReserva.getConsumoOutraCor().containsKey(corAgrupada))));
            }
            
            // table de exibição dos rolos reservados
            final FormTableView<VSdMateriaisCorte> tblMateriais = FormTableView.create(VSdMateriaisCorte.class, table -> {
                table.title("Materiais Cor: " + corSelecionada.getCor());
                table.items.set(FXCollections.observableList(rolosAplicacaoCor[0]));
                table.height(350.0);
                table.editable.set(true);
                table.columns(
                        FormTableColumn.create(cln -> {
                            cln.title("Insumo");
                            cln.width(300.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().toString()));
                        }).build() /*Insumo*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Cor");
                            cln.width(160.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCori().toString()));
                        }).build() /*Cor*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Lote");
                            cln.width(80.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLote()));
                            cln.alignment(Pos.CENTER);
                        }).build() /*Lote*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Ton.");
                            cln.width(80.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTonalidade()));
                            cln.alignment(Pos.CENTER);
                        }).build() /*Tonalidade*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Partida");
                            cln.width(55.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPartida()));
                            cln.alignment(Pos.CENTER);
                        }).build() /*Part.*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Largura");
                            cln.width(55.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLargura()));
                            cln.alignment(Pos.CENTER);
                        }).build() /*Larg.*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Gram.");
                            cln.width(45.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getGramatura()));
                            cln.alignment(Pos.CENTER);
                        }).build() /*Gramatura*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Seq.");
                            cln.width(35.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getSequencia()));
                            cln.alignment(Pos.CENTER);
                        }).build() /*Seq.*/,
                        FormTableColumn.create(cln -> {
                            cln.title("UM");
                            cln.width(30.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getUnidade().getUnidade()));
                            cln.alignment(Pos.CENTER);
                        }).build() /*UM*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Reservado");
                            cln.width(80.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getReservamt()));
                            cln.alignment(Pos.CENTER_RIGHT);
                            cln.format(param -> {
                                return new TableCell<VSdMateriaisCorte, BigDecimal>() {
                                    @Override
                                    protected void updateItem(BigDecimal item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        if (item != null && !empty) {
                                            setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                                        }
                                    }
                                };
                            });
                        }).build() /*Reservado*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Folhas Disp.");
                            cln.width(70.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getFolhas()));
                            cln.alignment(Pos.CENTER);
                            cln.format(param -> {
                                return new TableCell<VSdMateriaisCorte, Integer>() {
                                    @Override
                                    protected void updateItem(Integer item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        if (item != null && !empty) {
                                            setText(item > 0 ? StringUtils.toIntegerFormat(item) : "0");
                                        }
                                    }
                                };
                            });
                        }).build() /*Folhas Disp.*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Disp. Consumo");
                            cln.width(80.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getResmtConsumir()));
                            cln.alignment(Pos.CENTER_RIGHT);
                            cln.format(param -> {
                                return new TableCell<VSdMateriaisCorte, BigDecimal>() {
                                    @Override
                                    protected void updateItem(BigDecimal item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        if (item != null && !empty) {
                                            setText(item.compareTo(BigDecimal.ZERO) > 0 ? StringUtils.toDecimalFormat(item.doubleValue(), 4) : "0,0000");
                                        }
                                    }
                                };
                            });
                        }).build() /*Disp. Consumo*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Folhas Cons.");
                            cln.width(70.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                            cln.alignment(Pos.CENTER);
                            cln.editable.set(true);
                            cln.format(param -> {
                                return new TableCell<VSdMateriaisCorte, VSdMateriaisCorte>() {
                                    @Override
                                    protected void updateItem(VSdMateriaisCorte item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        setGraphic(null);
                                        if (item != null && !empty) {
                                            setGraphic(FormFieldText.create(field -> {
                                                field.withoutTitle();
                                                field.value.set(StringUtils.toIntegerFormat(item.getFolhasConsumidas()));
                                                field.editable.set(true);
                                                field.mask(FormFieldText.Mask.INTEGER);
                                                field.alignment(Pos.CENTER);
                                                field.editable.bind(item.selectedProperty());
                                                field.value.addListener((observable, oldValue, newValue) -> {
                                                    if (newValue != null) {
//                                                                                if (new BigDecimal(StringUtils.unformatDecimal(newValue)).compareTo(BigDecimal.ZERO) <= 0 ||
//                                                                                        new BigDecimal(StringUtils.unformatDecimal(newValue)).compareTo(new BigDecimal(item.getFolhasConsumidasOrig())) > 0) {
//                                                                                    MessageBox.create(message -> {
//                                                                                        message.message("Você pode alterar a quantidade de reserva de um rolo, para utilizar no risco, porém ela deve ser maior que ZERO e menor ou igual a RESERVA.");
//                                                                                        message.type(MessageBox.TypeMessageBox.IMPORTANT);
//                                                                                        message.showAndWait();
//                                                                                    });
//                                                                                    item.setFolhasConsumidas(new BigDecimal(StringUtils.unformatDecimal(oldValue)).intValue());
//                                                                                    field.value.set(oldValue);
//                                                                                } else {
                                                        item.setFolhasConsumidas(new BigDecimal(StringUtils.unformatDecimal(newValue)).intValue());
                                                        item.setQtdeConsumido(new BigDecimal(item.getFolhasConsumidas()).multiply(corSelecionada.getComprimento().add(new BigDecimal(0.06))));
                                                        
                                                        BigDecimal difFolhas = new BigDecimal(StringUtils.unformatDecimal(newValue)).subtract(new BigDecimal(StringUtils.unformatDecimal(oldValue)));
                                                        item.setResmtConsumir(item.getResmtConsumir().subtract(difFolhas.multiply(corSelecionada.getComprimento().add(new BigDecimal(0.06)))));
                                                        
                                                        field.value.set(newValue);
                                                        qtdeSelecionadaConsumo.set(rolosAplicacaoCor[0].stream().filter(it -> it.isSelected()).mapToDouble(it -> it.getQtdeConsumido().doubleValue()).sum());
                                                        table.refresh();
//                                                                                }
                                                    }
                                                });
                                            }).build());
                                        }
                                    }
                                };
                            });
                        }).build() /*Folhas Cons.*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Consumido");
                            cln.width(80.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtdeConsumido()));
                            cln.alignment(Pos.CENTER_RIGHT);
                            cln.format(param -> {
                                return new TableCell<VSdMateriaisCorte, BigDecimal>() {
                                    @Override
                                    protected void updateItem(BigDecimal item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        if (item != null && !empty) {
                                            setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                                        }
                                    }
                                };
                            });
                        }).build() /*Consumido*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Baixado");
                            cln.width(80.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getBaixadomt()));
                            cln.alignment(Pos.CENTER_RIGHT);
                            cln.format(param -> {
                                return new TableCell<VSdMateriaisCorte, BigDecimal>() {
                                    @Override
                                    protected void updateItem(BigDecimal item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        if (item != null && !empty) {
                                            setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                                        }
                                    }
                                };
                            });
                        }).build() /*Baixado*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Sld Estq");
                            cln.width(80.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getSaldomt()));
                            cln.alignment(Pos.CENTER_RIGHT);
                            cln.format(param -> {
                                return new TableCell<VSdMateriaisCorte, BigDecimal>() {
                                    @Override
                                    protected void updateItem(BigDecimal item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        if (item != null && !empty) {
                                            setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                                        }
                                    }
                                };
                            });
                        }).build() /*Sld Estq*/
                );
                table.factoryRow(param -> {
                    return new TableRow<VSdMateriaisCorte>() {
                        @Override
                        protected void updateItem(VSdMateriaisCorte item, boolean empty) {
                            super.updateItem(item, empty);
                            clear();
                            if (item != null && !empty) {
                                if (item.isSelected())
                                    getStyleClass().add("table-row-success");
                                else if (item.getResmtConsumir().compareTo(BigDecimal.ZERO) <= 0)
                                    getStyleClass().add("table-row-danger");
                            }
                        }
                        
                        private void clear() {
                            getStyleClass().removeAll("table-row-success", "table-row-danger");
                        }
                    };
                });
                table.indices(
                        table.indice(Color.LIGHTGREEN, "Rolo selecionado", false),
                        table.indice(Color.LIGHTCORAL, "Rolos indisponível", false),
                        table.indice(Color.WHITESMOKE, "Rolos disponíveis", false)
                );
            });
            
            // obtendo os consumos do tecido que não sejam a aplicação que está sendo feita e nem corpo
            List<ConsumoTecido> consumosTecido = consumosDoRiscoCor.size() > 0 ? getConsumosTecidoAplicacao(
                    planejamentoAberto.getNumero().getNumero(),
                    (checkRiscoAgrupado(corSelecionada.getCor()) ? riscoPlanejamento.getStringCoresAgrupadas() : corSelecionada.getCor()),
                    riscoPlanejamento.getTiporisco().getAplicacao().getSdriscoapl(),
                    consumosDoRiscoCor.get(0).getInsumo().getCodigo(),
                    consumosDoRiscoCor.get(0).getCori().getCor()) : new ArrayList<>();
            
            // get nas aplicações que utilizam o tecido/cor na mesma cor do risco
            List<ConsumoTecido> consumosTecidoMesmaCor = consumosTecido.stream()
                    .filter(it -> !checkRiscoAgrupado(corSelecionada.getCor())
                            ? it.cor.equals(corSelecionada.getCor())
                            : Arrays.asList(riscoPlanejamento.getStringCoresAgrupadas().replace("'", "").split(",")).stream().anyMatch(it2 -> it2.equals(it.cor)))
                    .collect(Collectors.toList());
            
            // Ajuste do total consumido dos consumos da cor para a grade que está sendo realizada no risco
            consumosTecidoMesmaCor.forEach(consumo -> {
                consumo.consumoTotal = new BigDecimal(corSelecionada.getGrade().stream().filter(grade -> consumo.faixa.contains("|".concat(grade.getTam()).concat("|"))).mapToDouble(grade -> grade.getRealizado()).sum() * consumo.consumo.doubleValue());
            });
            // get nas aplicações que utilizam o tecido/cor em outras cores
            List<ConsumoTecido> consumosTecidoOutraCor = consumosTecido.stream()
                    .filter(it -> !checkRiscoAgrupado(corSelecionada.getCor())
                            ? !it.cor.equals(corSelecionada.getCor())
                            : Arrays.asList(riscoPlanejamento.getStringCoresAgrupadas().replace("'", "").split(",")).stream().anyMatch(it2 -> !it2.equals(it.cor)))
                    .collect(Collectors.toList());
            
            // get debrums que utilizam o tecido/cor em na mesma cor do risco
            List<ConsumoTecido> debrumTecidoMesmaCor = consumosTecidoMesmaCor.stream()
                    .filter(it -> it.aplicacao.equals("001"))
                    .collect(Collectors.toList());
            // get debrums que utilizam o tecido/cor em outras cores
            List<ConsumoTecido> debrumTecidoOutraCor = consumosTecidoOutraCor.stream()
                    .filter(it -> it.aplicacao.equals("001"))
                    .collect(Collectors.toList());
            
            final int totalPecasRiscoCorpo = corSelecionada.getRiscoCorpo().stream().mapToInt(it -> it.getTotalRealizado()).sum();
            
            tblMateriais.addColumn(FormTableColumn.create(cln -> {
                cln.title("");
                cln.width(40.0);
                cln.alignment(FormTableColumn.Alignment.CENTER);
                cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                cln.format(param -> {
                    return new TableCell<VSdMateriaisCorte, VSdMateriaisCorte>() {
                        final HBox boxButtonsRow = new HBox(3);
                        final Button btnSelectRolo = FormButton.create(btn -> {
                            btn.tooltip("Selecionar rolo para o risco");
                            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            btn.addStyle("default");
                        });
                        
                        @Override
                        protected void updateItem(VSdMateriaisCorte item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                
                                btnSelectRolo.setGraphic(ImageUtils.getIcon((item.isSelected() ? ImageUtils.Icon.CANCEL : ImageUtils.Icon.ADICIONAR), ImageUtils.IconSize.MANUAL.size(20.0)));
                                btnSelectRolo.setOnAction(evt -> {
                                    Boolean newValue = !item.isSelected();
                                    
                                    BigDecimal consumoPecaCorRisco = new BigDecimal(consumosTecidoMesmaCor.stream()
                                            .mapToDouble(consumo -> consumo.consumo.doubleValue())
                                            .sum()); // consumo por peça do tecido do risco em outra aplicação da mesma cor
                                    BigDecimal consumoTotalCorRisco = new BigDecimal(consumosTecidoMesmaCor.stream()
                                            .mapToDouble(consumo -> consumo.consumoTotal.doubleValue())
                                            .sum()); // total consumo do tecido do risco em outra aplicação da mesma cor
                                    BigDecimal consumoJaSelecionado = new BigDecimal(qtdeSelecionadaConsumo.get()).setScale(4, RoundingMode.DOWN); // saldo do consumo do risco
                                    BigDecimal saldoConsumo = corSelecionada.getConsumototal().subtract(consumoJaSelecionado); // saldo do consumo do risco
                                    Integer folhasTecidoSelecionado = consumoJaSelecionado.divide(corSelecionada.getComprimento().add(new BigDecimal(0.06)), RoundingMode.DOWN).intValue(); // qtde folhas do tecido já reservado
                                    Integer saldoFolhas = corSelecionada.getFolhas() - folhasTecidoSelecionado.intValue(); // saldo das folhas que ainda precisa selecionar material
                                    Long qtdeTonsSelecionados = rolosAplicacaoCor[0].stream().filter(it -> it.isSelected()).map(it -> it.getTonalidade()).distinct().count(); // qtde de tons selecionados
                                    
                                    // exibição de mensagem caso o usuário seleciona um novo rolo porém o consumo já foi todo atendido
                                    if (saldoConsumo.compareTo(BigDecimal.ZERO) <= 0 && newValue) {
                                        MessageBox.create(message -> {
                                            message.message("Os rolos já selecionados atendem o consumo do risco, verifique se realmente é necessário a inclusão deste rolo.");
                                            message.type(MessageBox.TypeMessageBox.ALERT);
                                            message.showAndWait();
                                        });
                                        item.setSelected(false);
                                    }
                                    
                                    Integer folhasAConsumir = item.getFolhas() > saldoFolhas ? saldoFolhas : item.getFolhas();
                                    BigDecimal necessidadeConsumo = saldoConsumo.add(new BigDecimal(saldoFolhas * corSelecionada.getTotalGrade()).multiply(consumoPecaCorRisco));
                                    
                                    AtomicReference<BigDecimal> consumoAplicacaoRolo = new AtomicReference<>(BigDecimal.ZERO);
                                    if (newValue) {
                                        if (isMesmaCorCorpo && consumosDoRiscoCor.stream().anyMatch(it -> corSelecionada.getRiscoCorpo().stream().anyMatch(it2 -> it2.getMateriais().stream().anyMatch(it3 -> it3.getMaterial().equals(it.getInsumo()))))) {
                                            int qtdePecasCorpo = corSelecionada.getRiscoCorpo().stream().mapToInt(it -> it.getMateriais().stream().filter(it2 -> it2.equals(item)).mapToInt(SdMateriaisRiscoCor::getQtdepecas).sum()).sum();
                                            double percentDouble = (double) qtdePecasCorpo / (double) totalPecasRiscoCorpo;
                                            BigDecimal percentPecasRisco = new BigDecimal(percentDouble).setScale(4, RoundingMode.DOWN);
                                            item.setQtdeConsumido(corSelecionada.getConsumototal().multiply(percentPecasRisco).setScale(4, RoundingMode.DOWN));
                                            item.setFolhasConsumidas(item.getQtdeConsumido().divide(corSelecionada.getComprimento().add(new BigDecimal(0.06)), RoundingMode.DOWN).intValue());
                                        } else {
                                            // adicionar ao consumo
                                            if (item.getResmtConsumir().compareTo(necessidadeConsumo) >= 0) {
                                                // caso o rolo atende o consumo do risco mais o tecido em outras aplicações nesta cor de produto
                                                // o consumo da aplicação será o que está selecionado no rolo
                                                consumoAplicacaoRolo.set(saldoConsumo);
                                                item.setQtdeConsumido(saldoConsumo);
                                                item.setFolhasConsumidas(saldoConsumo.divide(corSelecionada.getComprimento().add(new BigDecimal(0.06)), RoundingMode.DOWN).intValue());
                                            } else {
                                                // caso o rolo não atende o consumo, será perguntado se quer selecionar mesmo assim esse rolo
                                                if ((Boolean) QuestionBox.build(Boolean.class, message -> {
                                                    message.message("O rolo selecionado não atende o consumo para a grade gerada. Deseja realmente adicionar o rolo? Nesta rotina será removido o necessário deste rolo para consumir em outras aplicações que utilizam esse tecido e cor no produto.");
                                                    message.showAndWait();
                                                }).value.get()) {
                                                    BigDecimal disponivelUsoRolo = item.getResmtConsumir().subtract(new BigDecimal(folhasAConsumir * corSelecionada.getTotalGrade()).multiply(consumoPecaCorRisco));
                                                    item.setFolhasConsumidas(disponivelUsoRolo.divide(corSelecionada.getComprimento().add(new BigDecimal(0.06)), RoundingMode.DOWN).intValue());
                                                    item.setQtdeConsumido(new BigDecimal(item.getFolhasConsumidas()).multiply(corSelecionada.getComprimento().add(new BigDecimal(0.06))));
                                                    consumoAplicacaoRolo.set(item.getQtdeConsumido());
                                                } else {
                                                    item.setSelected(false);
                                                }
                                            }
                                        }
                                        item.setSelected(true);
                                    } else {
                                        // remover do consumo
                                        consumoAplicacaoRolo.set(item.getQtdeConsumido().negate());
                                        item.setQtdeConsumido(BigDecimal.ZERO);
                                        item.setFolhasConsumidas(0);
                                        item.setSelected(false);
                                    }
                                    
                                    // saldo do rolo
                                    item.setResmtConsumir(item.getResmtConsumir().subtract(consumoAplicacaoRolo.get()));
                                    
                                    // atualização de valores para acumulo e exibição em tela
                                    qtdeSelecionadaConsumo.set(qtdeSelecionadaConsumo.add(consumoAplicacaoRolo.get().setScale(4, RoundingMode.DOWN).doubleValue()).doubleValue());
                                    saldoConsumoSelecionado.set(saldoConsumo.subtract(consumoAplicacaoRolo.get()).doubleValue());
                                    
                                    tblMateriais.refresh();
                                });
                                boxButtonsRow.getChildren().clear();
                                boxButtonsRow.getChildren().addAll(btnSelectRolo);
                                setGraphic(boxButtonsRow);
                            }
                        }
                    };
                });
            }).build() /*Ações*/);
            
            // criação do fragmento para seleção dos rolos
            new Fragment().show(fragment1 -> {
                fragment1.title("Seleção de Rolos para Risco");
                fragment1.size(1500.0, 700.0);
                fragment1.box.getChildren().add(tblMateriais.build());
                fragment1.box.getChildren().add(FormBox.create(boxConsumoReserva -> {
                    boxConsumoReserva.horizontal();
                    boxConsumoReserva.height(350.0);
                    boxConsumoReserva.add(FormBox.create(boxDadosConsumo -> {
                        boxDadosConsumo.vertical();
                        boxDadosConsumo.width(450.0);
                        boxDadosConsumo.add(FormBox.create(dadosReservaConsumo -> {
                            dadosReservaConsumo.horizontal();
                            dadosReservaConsumo.add(
                                    FormFieldText.create(field -> {
                                        field.title("Reserva Total");
                                        field.editable(false);
                                        field.expanded();
                                        field.addStyle("primary");
                                        field.value.set(StringUtils.toDecimalFormat(rolosAplicacaoCor[0].stream().mapToDouble(rolo -> rolo.getReservamt().doubleValue()).sum(), 4));
                                        field.alignment(Pos.CENTER_RIGHT);
                                    }).build(), // reserva total
                                    FormFieldText.create(field -> {
                                        field.title("Disponível Risco");
                                        field.editable(false);
                                        field.expanded();
                                        field.addStyle("success");
                                        field.value.set(StringUtils.toDecimalFormat(rolosAplicacaoCor[0].stream().mapToDouble(rolo -> rolo.getResmtConsumir().doubleValue()).sum(), 4));
                                        field.alignment(Pos.CENTER_RIGHT);
                                    }).build(), // reserva disponível para o risco
                                    FormFieldText.create(field -> {
                                        field.title("Cons. Mesma Cor");
                                        field.editable(false);
                                        field.expanded();
                                        field.addStyle("warning");
                                        field.value.set(StringUtils.toDecimalFormat(new BigDecimal(consumosTecidoMesmaCor.stream().mapToDouble(it -> it.consumoTotal.doubleValue()).sum()).doubleValue(), 4));
                                        field.alignment(Pos.CENTER_RIGHT);
                                    }).build(), // consumos na mesma cor em outras aplicações
                                    FormFieldText.create(field -> {
                                        field.title("Cons. Outra Cor");
                                        field.editable(false);
                                        field.expanded();
                                        field.addStyle("danger");
                                        field.value.set(StringUtils.toDecimalFormat(new BigDecimal(consumosTecidoOutraCor.stream().mapToDouble(it -> it.consumoTotal.doubleValue()).sum()).doubleValue(), 4));
                                        field.alignment(Pos.CENTER_RIGHT);
                                    }).build()  // consumos em outras cores
                            );
                        })); // dados de reservas e consumos aplicações
                        boxDadosConsumo.add(FormBox.create(dadosReservaConsumo -> {
                            dadosReservaConsumo.horizontal();
                            dadosReservaConsumo.add(
                                    FormFieldText.create(field -> {
                                        field.title("Consumo Risco");
                                        field.editable(false);
                                        field.expanded();
                                        field.addStyle("lg");
                                        field.value.set(StringUtils.toDecimalFormat(corSelecionada.getConsumototal().doubleValue(), 4));
                                        field.alignment(Pos.CENTER_RIGHT);
                                        if (new BigDecimal(rolosAplicacaoCor[0].stream().mapToDouble(rolo -> rolo.getResmtConsumir().doubleValue()).sum()).compareTo(corSelecionada.getConsumototal()) < 0) {
                                            field.addStyle("danger");
                                            field.tooltip("Não existe material reservado o suficiente para atender o risco.");
                                        }
                                    }).build(), // consumo do risco
                                    FormFieldText.create(field -> {
                                        field.title("Qtde. Alocado");
                                        field.editable(false);
                                        field.expanded();
                                        field.addStyle("lg").addStyle("success");
                                        field.value.bind(viewQtdeSelecionadaConsumo);
                                        field.alignment(Pos.CENTER_RIGHT);
                                    }).build(), // qtde alocado
                                    FormFieldText.create(field -> {
                                        field.title("Saldo");
                                        field.editable(false);
                                        field.expanded();
                                        field.addStyle("lg").addStyle("warning");
                                        field.value.bind(viewSaldoConsumoSelecionado);
                                        field.alignment(Pos.CENTER_RIGHT);
                                    }).build()  // saldo
                            );
                        })); // dados de seleção de rolos
                    })); // box com os totais de consumo e selecionado
                    boxConsumoReserva.add(FormBox.create(boxConsumos -> {
                        boxConsumos.vertical();
                        boxConsumos.add(FormTableView.create(ConsumoTecido.class, table -> {
                            table.title("Outros Consumos neste Produto/Cor");
                            table.setItems(FXCollections.observableList(consumosTecidoMesmaCor));
                            table.columns(
                                    FormTableColumn.create(cln -> {
                                        cln.title("Cor Prod.");
                                        cln.width(60.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<ConsumoTecido, ConsumoTecido>, ObservableValue<ConsumoTecido>>) param -> new ReadOnlyObjectWrapper(param.getValue().cor));
                                        cln.alignment(Pos.CENTER);
                                    }).build() /*Cor Prod.*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Aplic.");
                                        cln.width(50.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<ConsumoTecido, ConsumoTecido>, ObservableValue<ConsumoTecido>>) param -> new ReadOnlyObjectWrapper(param.getValue().aplicacao));
                                        cln.alignment(Pos.CENTER);
                                    }).build() /*Aplicação*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Desc. Aplic.");
                                        cln.width(160.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<ConsumoTecido, ConsumoTecido>, ObservableValue<ConsumoTecido>>) param -> new ReadOnlyObjectWrapper(param.getValue().descAplicacao));
                                    }).build() /*Desc. Aplic.*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Insumo");
                                        cln.width(80.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<ConsumoTecido, ConsumoTecido>, ObservableValue<ConsumoTecido>>) param -> new ReadOnlyObjectWrapper(param.getValue().insumo));
                                        cln.alignment(Pos.CENTER);
                                    }).build() /*Insumo*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Cor Ins.");
                                        cln.width(60.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<ConsumoTecido, ConsumoTecido>, ObservableValue<ConsumoTecido>>) param -> new ReadOnlyObjectWrapper(param.getValue().corI));
                                        cln.alignment(Pos.CENTER);
                                    }).build() /*Cor Ins.*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Consumo");
                                        cln.width(70.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<ConsumoTecido, ConsumoTecido>, ObservableValue<ConsumoTecido>>) param -> new ReadOnlyObjectWrapper(StringUtils.toDecimalFormat(param.getValue().consumoTotal.doubleValue(), 4)));
                                        cln.alignment(Pos.CENTER_RIGHT);
                                    }).build() /*Consumo*/);
                        }).build()); // table consumos da cor do risco com o tecido do risco
                        boxConsumos.add(FormTableView.create(ConsumoTecido.class, table -> {
                            table.title("Consumos Material/Cor");
                            table.setItems(FXCollections.observableList(consumosTecidoOutraCor));
                            table.columns(
                                    FormTableColumn.create(cln -> {
                                        cln.title("Cor Prod.");
                                        cln.width(60.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<ConsumoTecido, ConsumoTecido>, ObservableValue<ConsumoTecido>>) param -> new ReadOnlyObjectWrapper(param.getValue().cor));
                                        cln.alignment(Pos.CENTER);
                                    }).build() /*Cor Prod.*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Aplic.");
                                        cln.width(50.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<ConsumoTecido, ConsumoTecido>, ObservableValue<ConsumoTecido>>) param -> new ReadOnlyObjectWrapper(param.getValue().aplicacao));
                                        cln.alignment(Pos.CENTER);
                                    }).build() /*Aplicação*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Desc. Aplic.");
                                        cln.width(160.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<ConsumoTecido, ConsumoTecido>, ObservableValue<ConsumoTecido>>) param -> new ReadOnlyObjectWrapper(param.getValue().descAplicacao));
                                    }).build() /*Desc. Aplic.*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Insumo");
                                        cln.width(80.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<ConsumoTecido, ConsumoTecido>, ObservableValue<ConsumoTecido>>) param -> new ReadOnlyObjectWrapper(param.getValue().insumo));
                                        cln.alignment(Pos.CENTER);
                                    }).build() /*Insumo*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Cor Ins.");
                                        cln.width(60.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<ConsumoTecido, ConsumoTecido>, ObservableValue<ConsumoTecido>>) param -> new ReadOnlyObjectWrapper(param.getValue().corI));
                                        cln.alignment(Pos.CENTER);
                                    }).build() /*Cor Ins.*/,
                                    FormTableColumn.create(cln -> {
                                        cln.title("Consumo");
                                        cln.width(70.0);
                                        cln.value((Callback<TableColumn.CellDataFeatures<ConsumoTecido, ConsumoTecido>, ObservableValue<ConsumoTecido>>) param -> new ReadOnlyObjectWrapper(StringUtils.toDecimalFormat(param.getValue().consumoTotal.doubleValue(), 4)));
                                        cln.alignment(Pos.CENTER_RIGHT);
                                    }).build() /*Consumo*/);
                        }).build()); // table consumos de outras cores com o tecido do risco
                    })); // box com as tabelas de aplicações/consumos
                    boxConsumoReserva.add(FormTableView.create(SdMateriaisRiscoCor.class, table -> {
                        table.title("Rolos Selecionados");
                        table.expanded();
                        table.items.bind(corSelecionada.materiaisObservableProperty());
                        table.editable.set(true);
                        table.columns(
                                FormTableColumn.create(cln -> {
                                    cln.title("Insumo");
                                    cln.width(170.0);
                                    cln.value((Callback<TableColumn.CellDataFeatures<SdMateriaisRiscoCor, SdMateriaisRiscoCor>, ObservableValue<SdMateriaisRiscoCor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getMaterial()));
                                }).build() /*Insumo*/,
                                FormTableColumn.create(cln -> {
                                    cln.title("Cor");
                                    cln.width(60.0);
                                    cln.value((Callback<TableColumn.CellDataFeatures<SdMateriaisRiscoCor, SdMateriaisRiscoCor>, ObservableValue<SdMateriaisRiscoCor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCori()));
                                }).build() /*Cor*/,
                                FormTableColumn.create(cln -> {
                                    cln.title("Lote");
                                    cln.width(75.0);
                                    cln.value((Callback<TableColumn.CellDataFeatures<SdMateriaisRiscoCor, SdMateriaisRiscoCor>, ObservableValue<SdMateriaisRiscoCor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLote()));
                                    cln.alignment(Pos.CENTER);
                                }).build() /*Lote*/,
                                FormTableColumn.create(cln -> {
                                    cln.title("Ton.");
                                    cln.width(60.0);
                                    cln.value((Callback<TableColumn.CellDataFeatures<SdMateriaisRiscoCor, SdMateriaisRiscoCor>, ObservableValue<SdMateriaisRiscoCor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTonalidade()));
                                    cln.alignment(Pos.CENTER);
                                }).build() /*Tonalidade*/,
                                FormTableColumn.create(cln -> {
                                    cln.title("Partida");
                                    cln.width(55.0);
                                    cln.value((Callback<TableColumn.CellDataFeatures<SdMateriaisRiscoCor, SdMateriaisRiscoCor>, ObservableValue<SdMateriaisRiscoCor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPartida()));
                                    cln.alignment(Pos.CENTER);
                                }).build() /*Part.*/,
                                FormTableColumn.create(cln -> {
                                    cln.title("Seq.");
                                    cln.width(35.0);
                                    cln.value((Callback<TableColumn.CellDataFeatures<SdMateriaisRiscoCor, SdMateriaisRiscoCor>, ObservableValue<SdMateriaisRiscoCor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getSequencia()));
                                    cln.alignment(Pos.CENTER);
                                }).build() /*Seq.*/,
                                FormTableColumn.create(cln -> {
                                    cln.title("Consumido");
                                    cln.width(70.0);
                                    cln.value((Callback<TableColumn.CellDataFeatures<SdMateriaisRiscoCor, SdMateriaisRiscoCor>, ObservableValue<SdMateriaisRiscoCor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getConsumido()));
                                    cln.alignment(Pos.CENTER_RIGHT);
                                    cln.format(param -> {
                                        return new TableCell<VSdMateriaisCorte, BigDecimal>() {
                                            @Override
                                            protected void updateItem(BigDecimal item, boolean empty) {
                                                super.updateItem(item, empty);
                                                setText(null);
                                                if (item != null && !empty) {
                                                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                                                }
                                            }
                                        };
                                    });
                                }).build() /*Consumido*/
                        );
                        table.contextMenu(FormContextMenu.create(menu -> {
                            menu.addItem(menuItem -> {
                                menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.TRASH, ImageUtils.IconSize._16));
                                menuItem.setText("Desmarcar Rolo");
                                menuItem.setOnAction(evt -> {
                                    SdMateriaisRiscoCor roloParaExcluir = (SdMateriaisRiscoCor) table.selectedItem();
                                    if (roloParaExcluir != null) {
                                        VSdMateriaisCorte roloReservado = rolosAplicacaoCor[0].stream()
                                                .filter(it -> it.getCodigo().getCodigo().equals(roloParaExcluir.getMaterial()) && it.getCori().getCor().equals(roloParaExcluir.getCori()) && it.getLote().equals(roloParaExcluir.getLote()) &&
                                                        it.getPartida().equals(roloParaExcluir.getPartida()) && it.getSequencia().equals(roloParaExcluir.getSequencia()) && it.getTonalidade().equals(roloParaExcluir.getTonalidade()))
                                                .findAny().get();
                                        
                                        roloReservado.setResmtConsumir(roloReservado.getResmtConsumir().add(roloParaExcluir.getConsumido()));
                                        roloReservado.setFolhas(roloReservado.getResmtConsumir().divide(corSelecionada.getComprimento().add(new BigDecimal(0.06)), RoundingMode.DOWN).intValue());
                                        roloReservado.setQtdeConsumido(BigDecimal.ZERO);
                                        roloReservado.setFolhasConsumidas(0);
                                        roloReservado.setSelected(false);
                                        qtdeSelecionadaConsumo.set(qtdeSelecionadaConsumo.subtract(roloParaExcluir.getConsumido().doubleValue()).doubleValue());
                                        saldoConsumoSelecionado.set(saldoConsumoSelecionado.add(roloParaExcluir.getConsumido().doubleValue()).doubleValue());
                                        
                                        new FluentDao().delete(roloParaExcluir);
                                        corSelecionada.getMateriais().remove(roloParaExcluir);
                                        
                                        SysLogger.addSysDelizLog("Planejamento Encaixe", TipoAcao.EDITAR, planejamentoAberto.getNumero().getNumero(),
                                                "Excluido rolos na cor ID " + corSelecionada.getId() + " no risco ID " + riscoPlanejamento.getId()
                                                        + " (" + riscoPlanejamento.getTiporisco().getCodrisco() + riscoPlanejamento.getSeqTipo() + ") no planejamento ID " + planejamentoAberto.getId());
                                        table.refresh();
                                        tblMateriais.refresh();
                                    }
                                });
                            });
                        }));
                    }).build()); // table dos rolos selecionados
                }));
                fragment1.buttonsBox.getChildren().add(FormButton.create(btnSelecionar -> {
                    btnSelecionar.title("Selecionar Rolos");
                    btnSelecionar.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                    btnSelecionar.addStyle("success");
                    btnSelecionar.setAction(event -> {
                        // validação de rolos selecionados
                        if (rolosAplicacaoCor[0].stream().filter(rolo -> rolo.isSelected()).count() == 0) {
                            MessageBox.create(message -> {
                                message.message("Você precisa selecionar ao menos um rolo para o risco.");
                                message.type(MessageBox.TypeMessageBox.IMPORTANT);
                                message.showAndWait();
                            });
                            return;
                        }
                        
                        BigDecimal qtdeTotalSelecionada = new BigDecimal(rolosAplicacaoCor[0].stream().filter(rolo -> rolo.isSelected()).mapToDouble(rolo -> rolo.getQtdeConsumido().doubleValue()).sum());
                        final BigDecimal[] qtdeConsumoRisco = {corSelecionada.getConsumototal().subtract(new BigDecimal(corSelecionada.getMateriais().stream().mapToDouble(materiais -> materiais.getConsumido().doubleValue()).sum()))};
                        // alerta se a qtde dos rolos é suficiente para a reserva com confirmação se que continuar
                        if (qtdeTotalSelecionada.setScale(4, RoundingMode.DOWN).compareTo(qtdeConsumoRisco[0].setScale(4, RoundingMode.DOWN)) < 0) {
                            if (!((Boolean) QuestionBox.build(Boolean.class, message -> {
                                message.message("A quantidade de reserva selecionada é menor que o risco está consumindo, estão faltando " +
                                        StringUtils.toDecimalFormat(qtdeTotalSelecionada.subtract(corSelecionada.getConsumototal()).doubleValue(), 4) +
                                        ". Deseja processeguir o risco com a falta do material?");
                                message.showAndWait();
                            }).value.get())) {
                                return;
                            }
                        }
                        
                        rolosAplicacaoCor[0].stream().filter(rolo -> rolo.isSelected()).forEach(rolo -> {
                            BigDecimal qtdeParaConsumir = rolo.getQtdeConsumido();
//                                                if (rolo.getResmtConsumir().compareTo(qtdeConsumoRisco[0]) > 0) {
//                                                    qtdeParaConsumir = qtdeConsumoRisco[0];
//                                                } else {
//                                                    qtdeConsumoRisco[0] = qtdeConsumoRisco[0].subtract(rolo.getResmtConsumir());
//                                                }
                            SdMateriaisRiscoCor roloSelecionado = new SdMateriaisRiscoCor(corSelecionada.getId(), checkRiscoAgrupado(corSelecionada.getCor()) ? null : corSelecionada.getCor(), rolo.getCodigo().getCodigo(), rolo.getCori().getCor(),
                                    rolo.getLote(), rolo.getTonalidade(), rolo.getPartida(), rolo.getLargura(),
                                    rolo.getGramatura(), rolo.getSequencia(), qtdeParaConsumir, rolo.getFolhasConsumidas(),
                                    (rolo.getFolhasConsumidas() * corSelecionada.getTotalGrade()));
                            try {
                                roloSelecionado = new FluentDao().persist(roloSelecionado);
                                corSelecionada.getMateriais().add(roloSelecionado);
                            } catch (SQLException e) {
                                e.printStackTrace();
                                ExceptionBox.build(message -> {
                                    message.exception(e);
                                    message.showAndWait();
                                });
                            }
                            
                            // verifica se o risco é contrastante, caso true, irá remover, da cor que corresponde a qtde alocada do consumoOutraCor
                            if (!isMesmaCorCorpo) {
                                if (!checkRiscoAgrupado(corSelecionada.getCor())) {
                                    BigDecimal qtdePreReservada = rolo.getConsumoOutraCor().get(corSelecionada.getCor());
                                    rolo.getConsumoOutraCor().replace(corSelecionada.getCor(), qtdePreReservada.subtract(qtdeParaConsumir));
                                } else {
                                    if (rolo.getConsumoOutraCor() != null && rolo.getConsumoOutraCor().size() > 0) {
                                        Arrays.asList(riscoPlanejamento.getIdsCoresAgrupadas().replace("'", "").split(",")).forEach(cores -> {
                                            SdCorRisco corCorpo = planejamentoAberto.getRiscos().stream().map(it -> it.getCores().stream().filter(it2 -> it2.getId() == Integer.parseInt(cores)).findAny().get()).findAny().get();
                                            BigDecimal qtdePreReservada = rolo.getConsumoOutraCor().get(corCorpo.getCor());
                                            rolo.getConsumoOutraCor().replace(cores, qtdePreReservada.subtract(qtdeParaConsumir));
                                        });
                                    }
                                }
                            }
                        });
                        
                        if (rolosAplicacaoCor[0].stream().filter(it -> it.isSelected()).findAny().get().getCodigo().getUnidade().getUnidade().toUpperCase().equals("KG")) {
                            VSdMateriaisCorte rolo = rolosAplicacaoCor[0].stream().filter(it -> it.isSelected()).findAny().get();
                            if (corSelecionada.getConsumopecakg().compareTo(BigDecimal.ZERO) == 0) {
                                if (((VBox) boxSetMateriais.getChildren().get(0)).getChildren().size() > 3)
                                    ((VBox) boxSetMateriais.getChildren().get(0)).getChildren().remove(3);
                                
                                boxSetMateriais.add(FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.label("Cons. Pç.:");
                                    field.postLabel("kg");
                                    field.editable(false);
                                    field.alignment(Pos.CENTER);
                                    field.value.bind(corSelecionada.consumopecakgProperty().asString("%.4f"));
                                }).build());
                            }
                            
                            BigDecimal calc1 = rolo.getGramatura().multiply(rolo.getLargura());
                            BigDecimal calc2 = new BigDecimal(1000).divide(calc1, 15, RoundingMode.DOWN);
                            BigDecimal calc3 = corSelecionada.getConsumototal().divide(calc2, 15, RoundingMode.DOWN);
                            BigDecimal calc4 = calc3.divide(new BigDecimal(corSelecionada.getTotalRealizado()), 4, RoundingMode.DOWN);
                            
                            corSelecionada.setConsumopecakg(calc4);
                        }
                        fragment1.close();
                        
                        SysLogger.addSysDelizLog("Planejamento Encaixe", TipoAcao.EDITAR, planejamentoAberto.getNumero().getNumero(),
                                "Selecionado rolos para a cor ID " + corSelecionada.getId() + " no risco ID " + riscoPlanejamento.getId()
                                        + " (" + riscoPlanejamento.getTiporisco().getCodrisco() + riscoPlanejamento.getSeqTipo() + ") no planejamento ID " + planejamentoAberto.getId());
                    });
                }));
            });
        }
    }
    
    private void selectMaterialRisco(SdRiscoPlanejamentoEncaixe risco, SdCorRisco corRisco, FormBox boxSetMateriais, FormFieldToggleSingle fixarGrade) {
        // <editor-fold defaultstate="collapsed" desc="GET das informações base para seleção dos rolos">
        SdTipoRisco tipoRisco = risco.getTiporisco();
        Pcpapl aplicacaoRisco = risco.getAplicacaoft();
        Boolean riscoAgrupado = checkRiscoAgrupado(corRisco.getCor());
        String[] coresRisco = (tipoRisco.isUsacor()
                ? corRisco.getCor()
                : checkRiscoAgrupado(corRisco.getCor())
                    ? getCorIds(corRisco.getCor()).stream().map(SdCorRisco::getCor).distinct().collect(Collectors.joining(","))
                    : corRisco.isGrupoCor() ? corRisco.getCor() : getCorIds(corRisco.getCor()).stream().map(SdCorRisco::getCor).distinct().collect(Collectors.joining(","))).replace("'", "").split(",");
        BigDecimal comprimentoRisco = corRisco.getComprimento().add(new BigDecimal(0.06)).setScale(5, RoundingMode.UP);
        corRisco.getMateriaisObservable().clear();
        corRisco.getMateriaisObservable().addAll(corRisco.getMateriais());
        // </editor-fold>

        if (planejamentoAberto == null || planejamentoAberto.getNumero() == null) {
            MessageBox.create(message -> {
                message.message("Aconteceu um erro durante o carregamento da OF. Verifique o cadastro da OF com os materiais.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }

        if (aplicacaoRisco == null) {
            MessageBox.create(message -> {
                message.message("Erro durante a carga das aplicações da ficha técnica. Verifique o cadastro das Aplicações e seu grupos.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }

        // <editor-fold defaultstate="collapsed" desc="GET das listas de materiais para o risco">
        // <editor-fold defaultstate="collapsed" desc="materiais consumidos pelo risco na cor do produto">
        List<VSdConsumosOfCorApl> materiaisAplicacaoCorRisco = (List<VSdConsumosOfCorApl>) new FluentDao().selectFrom(VSdConsumosOfCorApl.class)
                .where(it -> it
                        .equal("numero", planejamentoAberto.getNumero().getNumero())
                        .isIn("cor", coresRisco)
                        .equal("aplicpcp.codigo", aplicacaoRisco.getCodigo()))
                .resultList();
        if (materiaisAplicacaoCorRisco.size() == 0) {
            MessageBox.create(message -> {
                message.message("Não foi possível carregar os materiais para a aplicação: " + aplicacaoRisco.toString() + " na(s) cor(es): " + Arrays.asList(coresRisco).stream().collect(Collectors.joining(",")));
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showAndWait();
            });
            return;
        }
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="consumos do material e risco">
        // Aplicações que utilizam o material/cor da cor do risco
        List<VSdConsumosOfCorApl> aplicacoesMaterialCorRisco = (List<VSdConsumosOfCorApl>) new FluentDao().selectFrom(VSdConsumosOfCorApl.class)
                .where(it -> it
                        .equal("numero", planejamentoAberto.getNumero().getNumero())
                        .isIn("cor", coresRisco)
                        .notEqual("aplicpcp.codigo", aplicacaoRisco.getCodigo())
                        .isIn("insumo.codigo", materiaisAplicacaoCorRisco.stream().map(VSdConsumosOfCorApl::getInsumo).map(Material::getCodigo).distinct().toArray())
                        .isIn("cori.cor", materiaisAplicacaoCorRisco.stream().map(VSdConsumosOfCorApl::getCori).map(Cor::getCor).distinct().toArray()))
                .resultList();
        // Outras aplicações e materiais da cor do risco
        List<VSdConsumosOfCorApl> aplicacoesMateriaisRiscoCor = (List<VSdConsumosOfCorApl>) new FluentDao().selectFrom(VSdConsumosOfCorApl.class)
                .where(it -> it
                        .equal("numero", planejamentoAberto.getNumero().getNumero())
                        .isIn("cor", coresRisco)
                        .notEqual("aplicpcp.codigo", aplicacaoRisco.getCodigo()))
                .resultList();
        aplicacoesMateriaisRiscoCor.removeIf(it -> aplicacoesMaterialCorRisco.contains(it)); // remove os materiais que são da mesma cor/material do risco
        // Aplicações do material/cor em outras cores risco
        List<VSdConsumosOfCorApl> aplicacoesMaterialOutraCorRisco = (List<VSdConsumosOfCorApl>) new FluentDao().selectFrom(VSdConsumosOfCorApl.class)
                .where(it -> it
                        .equal("numero", planejamentoAberto.getNumero().getNumero())
                        .notIn("cor", coresRisco)
                        .notEqual("aplicpcp.codigo", aplicacaoRisco.getCodigo())
                        .isIn("insumo.codigo", materiaisAplicacaoCorRisco.stream().map(VSdConsumosOfCorApl::getInsumo).map(Material::getCodigo).distinct().toArray())
                        .isIn("cori.cor", materiaisAplicacaoCorRisco.stream().map(VSdConsumosOfCorApl::getCori).map(Cor::getCor).distinct().toArray()))
                .resultList();
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="rolos reservados aplicacao cor risco">
        List<VSdMateriaisCorte> rolosReservados = (List<VSdMateriaisCorte>) new FluentDao().selectFrom(VSdMateriaisCorte.class).where(it -> it
                .equal("numero", planejamentoAberto.getNumero().getNumero())
                .isIn("codigo.codigo", materiaisAplicacaoCorRisco.stream().map(VSdConsumosOfCorApl::getInsumo).map(Material::getCodigo).distinct().toArray())
                .isIn("cori.cor", materiaisAplicacaoCorRisco.stream().map(VSdConsumosOfCorApl::getCori).map(Cor::getCor).distinct().toArray())
        ).resultList();
        if (rolosReservados.size() == 0) {
            MessageBox.create(message -> {
                message.message(String.format("Não foi encontrado nenhum rolo reservado nesta OF para o material: %s e cor: %s",
                        materiaisAplicacaoCorRisco.stream().map(VSdConsumosOfCorApl::getInsumo).map(Material::getCodigo).distinct().collect(Collectors.joining(",")),
                        materiaisAplicacaoCorRisco.stream().map(VSdConsumosOfCorApl::getCori).map(Cor::getCor).distinct().collect(Collectors.joining(","))));
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showAndWait();
            });
            return;
        }
        // atualização das disponibilidades dos rolos com o que já foi consumido em outros riscos mais debrum
        rolosReservados.forEach(rolo -> {
            BigDecimal consumidoOutrosRiscos = new BigDecimal(planejamentoAberto.getRiscos().stream()
                    .mapToDouble(it -> it.getCores().stream()
                            .filter(it2 -> !it2.equals(corRisco))
                            .mapToDouble(it2 -> it2.getMateriais().stream()
                                    .filter(it3 -> it3.equals(rolo))
                                    .mapToDouble(it3 -> it3.getConsumido().add(it3.getConsumidoDebrum()).doubleValue())
                                    .sum())
                            .sum())
                    .sum());
            BigDecimal consumidoNesteRisco = new BigDecimal(corRisco.getMateriais().stream()
                    .filter(it -> it.equals(rolo))
                    .mapToDouble(consumido -> consumido.getConsumido().add(consumido.getConsumidoDebrum()).doubleValue())
                    .sum());
            rolo.setResmtConsumir(rolo.getReservamt().subtract(consumidoOutrosRiscos).subtract(consumidoNesteRisco)); // definição da qtde disponível do rolo subtraindo o que já foi consumido nos riscos (disponível)
            rolo.setFolhas(rolo.getResmtConsumir().divide(comprimentoRisco, RoundingMode.DOWN).intValue()); // calculo de folhas do rolo com base no risco (disponível)
            rolo.setQtdeConsumido(BigDecimal.ZERO); // Zerando a variável para uso na rotina
            rolo.setFolhasConsumidas(0);
            rolo.setSelected(false);
        });
        // remoção de qtde disponível do menor rolo que atende os consumos contrastantes
        // verificando se já teve um cálculo e remoção de tecido de um rolo para atender uma outra aplicação em outra cor
        if (rolosReservados.stream().filter(it -> it.getConsumoOutraCor() != null && !it.getConsumoOutraCor().isEmpty()).count() > 0) {
            // remove dos rolos os consumos em outras cores já calculados.
            rolosReservados.stream().filter(it -> it.getConsumoOutraCor() != null && !it.getConsumoOutraCor().isEmpty()).forEach(it -> {
                it.setResmtConsumir(it.getResmtConsumir().subtract(new BigDecimal(it.getConsumoOutraCor().values().stream().mapToDouble(BigDecimal::doubleValue).sum())));
            });
        } else {
            // total da qtde consumida do tecido nas outras cores do produto
            final AtomicReference<BigDecimal> qtdeConsumidaOutraCor = new AtomicReference<>(new BigDecimal(aplicacoesMaterialOutraCorRisco.stream()
                    .mapToDouble(consumo -> consumo.getConsumototal().doubleValue())
                    .sum()));
            final BigDecimal qtdeTotalConsumidaOutraCor = qtdeConsumidaOutraCor.get();
            
            // get dos rolos que atendem o total dos consumos do tecido nas outras cores de produto.
            List<VSdMateriaisCorte> rolosAtendemConsumoOutraCor = rolosReservados.stream()
                    .filter(rolo -> rolo.getResmtConsumir().compareTo(qtdeConsumidaOutraCor.get()) >= 0)
                    .collect(Collectors.toList());
            // teste para verificar se tem UM rolo que atende totalmente o consumo nas outras cores de produto, senão deverá tirar o consumo de mais de um rolo.
            if (rolosAtendemConsumoOutraCor.size() > 0) {
                rolosAtendemConsumoOutraCor.sort(Comparator.comparing(VSdMateriaisCorte::getResmtConsumir));
                rolosAtendemConsumoOutraCor.get(0).setResmtConsumir(rolosAtendemConsumoOutraCor.get(0).getResmtConsumir().subtract(qtdeConsumidaOutraCor.get()));
                Map<String, BigDecimal> dadosRoloOutraCor = new HashMap<>();
                aplicacoesMaterialOutraCorRisco.stream().collect(Collectors.groupingBy(it -> it.getCor(), Collectors.summingDouble(it -> it.getConsumototal().doubleValue()))).forEach((key, value) -> {
                    dadosRoloOutraCor.put(key, new BigDecimal(value));
                });
                rolosAtendemConsumoOutraCor.get(0).setConsumoOutraCor(FXCollections.observableMap(dadosRoloOutraCor));
            } else {
                // ordenando os rolos reservados por ordem de qtde disponível para consumir
                rolosReservados.sort(Comparator.comparing(VSdMateriaisCorte::getResmtConsumir));
                rolosReservados.forEach(rolo -> {
                    Map<String, BigDecimal> dadosRoloOutraCor = new HashMap<>();
                    if (qtdeConsumidaOutraCor.get().compareTo(rolo.getResmtConsumir()) > 0) {
                        // se o rolo reservado a qtde disponível é menor que o consumo, zera a qtde disponível do rolo para não ser utilizado neste risco
                        qtdeConsumidaOutraCor.set(qtdeConsumidaOutraCor.get().subtract(rolo.getResmtConsumir()));
                        rolo.setResmtConsumir(BigDecimal.ZERO);
                        
                        // calcula a porcentagem do consumo de cada cor para guardar o consumo outras cores
                        if (rolosAtendemConsumoOutraCor.size() > 0) {
                            BigDecimal percentConsumo = qtdeTotalConsumidaOutraCor.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : rolo.getResmtConsumir().divide(qtdeTotalConsumidaOutraCor, RoundingMode.DOWN);
                            aplicacoesMaterialOutraCorRisco.stream().collect(Collectors.groupingBy(it -> it.getCor(), Collectors.summingDouble(it -> it.getConsumototal().doubleValue()))).forEach((key, value) -> {
                                dadosRoloOutraCor.put(key, new BigDecimal(value).multiply(percentConsumo).setScale(4, RoundingMode.DOWN));
                            });
                            rolosAtendemConsumoOutraCor.get(0).setConsumoOutraCor(FXCollections.observableMap(dadosRoloOutraCor));
                        }
                    } else {
                        // se o rolo atender a qtde, reserva o saldo do consumido em outra cor
                        rolo.setResmtConsumir(rolo.getResmtConsumir().subtract(qtdeConsumidaOutraCor.get()));
                        
                        // calcula a porcentagem do consumo de cada cor para guardar o consumo outras cores
                        BigDecimal percentConsumo = qtdeConsumidaOutraCor.get().divide(qtdeTotalConsumidaOutraCor, RoundingMode.DOWN);
                        aplicacoesMaterialOutraCorRisco.stream().collect(Collectors.groupingBy(it -> it.getCor(), Collectors.summingDouble(it -> it.getConsumototal().doubleValue()))).forEach((key, value) -> {
                            dadosRoloOutraCor.put(key, new BigDecimal(value).multiply(percentConsumo).setScale(4, RoundingMode.DOWN));
                        });
                        rolosAtendemConsumoOutraCor.get(0).setConsumoOutraCor(FXCollections.observableMap(dadosRoloOutraCor));
                    }
                });
            }
        }
        // removendo da lista de rolos os rolos que não foram utilizados no corpo, caso o risco seja um detalhe ou ribana não contrastante
        if (!tipoRisco.isUsacor()) {
            // verifica se o detalhe/ribana é contrastante, remove somente os que não são contrastantes
            boolean isMesmaCorCorpo = materiaisAplicacaoCorRisco.stream()
                    .anyMatch(cor -> corRisco.getRiscoCorpo().stream().anyMatch(it -> it.getMateriais().stream().anyMatch(rolo -> rolo.getMaterial().equals(cor.getInsumo().getCodigo()) && rolo.getCori().equals(cor.getCori().getCor()))));
            if (isMesmaCorCorpo) {
                rolosReservados.removeIf(rolo -> corRisco.getRiscoCorpo().stream().noneMatch(corpo -> corpo.getMateriais().stream().anyMatch(roloCorpo -> roloCorpo.equals(rolo))));
                rolosReservados.forEach(rolo -> {
                    corRisco.getRiscoCorpo().forEach(corpo -> corpo.getMateriais().stream().filter(roloCorpo -> roloCorpo.equals(rolo)).forEach(roloCorpo -> {
                        rolo.setFolhasCorpo(rolo.getFolhasCorpo() + roloCorpo.getQtdefolhas());
                        rolo.setPecasCorpo(rolo.getPecasCorpo() + roloCorpo.getQtdepecas());
                    }));
                });
            }
        }
        // </editor-fold>
        // </editor-fold>
        
        // <editor-fold defaultstate="collapsed" desc="GET/SET das informações para exibição em tela">
        final DoubleProperty qtdeSelecionadaConsumo = new SimpleDoubleProperty(corRisco.getMateriais().stream().mapToDouble(consumido -> consumido.getConsumido().doubleValue()).sum());
        final DoubleProperty saldoConsumoSelecionado = new SimpleDoubleProperty(corRisco.getConsumototal().doubleValue() - qtdeSelecionadaConsumo.get());
        final IntegerProperty qtdeFolhasSelecionadaConsumo = new SimpleIntegerProperty(corRisco.getMateriais().stream().mapToInt(consumido -> consumido.getQtdefolhas()).sum());
        final IntegerProperty saldoFolhasConsumoSelecionado = new SimpleIntegerProperty(corRisco.getFolhas() - qtdeFolhasSelecionadaConsumo.get());
        final StringProperty viewQtdeSelecionadaConsumo = new SimpleStringProperty();
        final StringProperty viewSaldoConsumoSelecionado = new SimpleStringProperty();
        final StringProperty viewQtdeFolhasSelecionadaConsumo = new SimpleStringProperty();
        final StringProperty viewSaldoFolhasConsumoSelecionado = new SimpleStringProperty();
        viewQtdeSelecionadaConsumo.bind(qtdeSelecionadaConsumo.asString("%.4f"));
        viewSaldoConsumoSelecionado.bind(saldoConsumoSelecionado.asString("%.4f"));
        viewQtdeFolhasSelecionadaConsumo.bind(qtdeFolhasSelecionadaConsumo.asString());
        viewSaldoFolhasConsumoSelecionado.bind(saldoFolhasConsumoSelecionado.asString());
        // </editor-fold>
        // <editor-fold defaultstate="collapsed" desc="Fields do fragment para seleção dos rolos">
        final FormTableView<VSdMateriaisCorte> tblRolosDisponiveis = FormTableView.create(VSdMateriaisCorte.class, table -> {
            table.title("Rolos Disponíveis");
            table.editable.set(true);
            table.height(350.0);
            table.items.set(FXCollections.observableList(rolosReservados));
            table.columns(
                    FormTableColumn.create(cln -> {
                        cln.title("Material");
                        cln.width(270.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().toString()));
                    }).build() /*Material*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Cor Mat.");
                        cln.width(150.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCori().toString()));
                    }).build() /*Cor Mat.*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Lote");
                        cln.width(90.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLote()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*Lote*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Ton.");
                        cln.width(100.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTonalidade()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*Tonalidade*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Part.");
                        cln.width(55.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPartida()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*Part.*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Larg.");
                        cln.width(45.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLargura()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*Larg.*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Gram.");
                        cln.width(45.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getGramatura()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*Gramatura*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Seq.");
                        cln.width(35.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getSequencia()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*Seq.*/,
                    FormTableColumn.create(cln -> {
                        cln.title("UM");
                        cln.width(30.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getUnidade().getUnidade()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*UM*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Reservado");
                        cln.width(75.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getReservamt()));
                        cln.alignment(Pos.CENTER_RIGHT);
                        cln.format(param -> {
                            return new TableCell<VSdMateriaisCorte, BigDecimal>() {
                                @Override
                                protected void updateItem(BigDecimal item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    if (item != null && !empty) {
                                        setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                                    }
                                }
                            };
                        });
                    }).build() /*Reservado*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Folhas Disp.");
                        cln.width(70.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getFolhas()));
                        cln.alignment(Pos.CENTER);
                        cln.format(param -> {
                            return new TableCell<VSdMateriaisCorte, Integer>() {
                                @Override
                                protected void updateItem(Integer item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    if (item != null && !empty) {
                                        setText(item > 0 ? StringUtils.toIntegerFormat(item) : "0");
                                    }
                                }
                            };
                        });
                    }).build() /*Folhas Disp.*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Disp. Consumo");
                        cln.width(80.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getResmtConsumir()));
                        cln.alignment(Pos.CENTER_RIGHT);
                        cln.format(param -> {
                            return new TableCell<VSdMateriaisCorte, BigDecimal>() {
                                @Override
                                protected void updateItem(BigDecimal item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    if (item != null && !empty) {
                                        setText(item.compareTo(BigDecimal.ZERO) > 0 ? StringUtils.toDecimalFormat(item.doubleValue(), 4) : "0.0000");
                                    }
                                }
                            };
                        });
                    }).build() /*Disp. Consumo*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Folhas Cons.");
                        cln.width(70.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getFolhasConsumidas()));
                        cln.alignment(Pos.CENTER);
//                        cln.editable.set(true);
//                        cln.format(param -> {
//                            return new TableCell<VSdMateriaisCorte, VSdMateriaisCorte>() {
//                                @Override
//                                protected void updateItem(VSdMateriaisCorte item, boolean empty) {
//                                    super.updateItem(item, empty);
//                                    setText(null);
//                                    setGraphic(null);
//                                    if (item != null && !empty) {
//                                        setGraphic(FormFieldText.create(field -> {
//                                            field.withoutTitle();
//                                            field.value.set(StringUtils.toIntegerFormat(item.getFolhasConsumidas()));
//                                            field.mask(FormFieldText.Mask.INTEGER);
//                                            field.alignment(Pos.CENTER);
//                                            field.editable.bind(item.selectedProperty());
//                                            field.value.addListener((observable, oldValue, newValue) -> {
//                                                if (newValue != null) {
//                                                    try {
////                                                                                if (new BigDecimal(StringUtils.unformatDecimal(newValue)).compareTo(BigDecimal.ZERO) < 0 ||
////                                                                                        new BigDecimal(StringUtils.unformatDecimal(newValue)).compareTo(new BigDecimal(item.getFolhasConsumidasOrig())) > 0) {
////                                                                                    MessageBox.create(message -> {
////                                                                                        message.message("Você pode alterar a quantidade de reserva de um rolo, para utilizar no risco, porém ela deve ser maior que ZERO e menor ou igual a RESERVA.");
////                                                                                        message.type(MessageBox.TypeMessageBox.IMPORTANT);
////                                                                                        message.showAndWait();
////                                                                                    });
////                                                                                    item.setFolhasConsumidas(new BigDecimal(StringUtils.unformatDecimal(oldValue)).intValue());
////                                                                                    field.value.set(oldValue);
////                                                                                } else {
//                                                        item.setFolhasConsumidas(new BigDecimal(StringUtils.unformatDecimal(newValue)).intValue());
//                                                        item.setQtdeConsumido(new BigDecimal(item.getFolhasConsumidas()).multiply(comprimentoRisco));
//
//                                                        BigDecimal difFolhas = new BigDecimal(StringUtils.unformatDecimal(newValue)).subtract(new BigDecimal(StringUtils.unformatDecimal(oldValue)));
//                                                        item.setResmtConsumir(item.getResmtConsumir().subtract(difFolhas.multiply(comprimentoRisco)));
//                                                        field.value.set(newValue);
//
//                                                        // cálculo do consumo a partir da qtde de folhas digitada ou calculada
//                                                        qtdeSelecionadaConsumo.set(rolosReservados.stream().filter(it -> it.isSelected()).mapToDouble(it -> it.getQtdeConsumido().doubleValue()).sum());
//                                                        saldoConsumoSelecionado.set(saldoConsumoSelecionado.get() - qtdeSelecionadaConsumo.get());
//                                                        // cálculo das folhas utilizadas para o risco
//                                                        qtdeFolhasSelecionadaConsumo.set(rolosReservados.stream().filter(it -> it.isSelected()).mapToInt(it -> it.getFolhasConsumidas()).sum());
//                                                        saldoFolhasConsumoSelecionado.set(saldoFolhasConsumoSelecionado.get() - qtdeFolhasSelecionadaConsumo.get());
//
//                                                        table.refresh();
////                                                                                }
//                                                    } catch (NumberFormatException exc) {
//                                                        exc.printStackTrace();
//                                                    }
//                                                }
//                                            });
//                                        }).build());
//                                    }
//                                }
//                            };
//                        });
                    }).build() /*Folhas Cons.*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Consumido");
                        cln.width(75.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtdeConsumido()));
                        cln.alignment(Pos.CENTER_RIGHT);
                        cln.format(param -> {
                            return new TableCell<VSdMateriaisCorte, BigDecimal>() {
                                @Override
                                protected void updateItem(BigDecimal item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    if (item != null && !empty) {
                                        setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                                    }
                                }
                            };
                        });
                    }).build() /*Consumido*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Baixado");
                        cln.width(70.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getBaixadomt()));
                        cln.alignment(Pos.CENTER_RIGHT);
                        cln.format(param -> {
                            return new TableCell<VSdMateriaisCorte, BigDecimal>() {
                                @Override
                                protected void updateItem(BigDecimal item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    if (item != null && !empty) {
                                        setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                                    }
                                }
                            };
                        });
                    }).build() /*Baixado*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Sld. Estq");
                        cln.width(70.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getSaldomt()));
                        cln.alignment(Pos.CENTER_RIGHT);
                        cln.format(param -> {
                            return new TableCell<VSdMateriaisCorte, BigDecimal>() {
                                @Override
                                protected void updateItem(BigDecimal item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    if (item != null && !empty) {
                                        setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                                    }
                                }
                            };
                        });
                    }).build() /*Saldo Estq*/);
            if (!tipoRisco.isUsacor()) {
                table.addColumn(FormTableColumnGroup.createGroup(group -> {
                    group.title("Corpo");
                    group.addColumn(FormTableColumn.create(cln -> {
                        cln.title("Folhas");
                        cln.width(50.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getFolhasCorpo()));
                        cln.alignment(Pos.CENTER);
                    }).build());
                    group.addColumn(FormTableColumn.create(cln -> {
                        cln.title("Peças");
                        cln.width(50.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPecasCorpo()));
                        cln.alignment(Pos.CENTER);
                    }).build());
                }).build() /*Corpo*/);
            }
            table.addColumn(FormTableColumn.create(cln -> {
                cln.title("");
                cln.width(40.0);
                cln.alignment(FormTableColumn.Alignment.CENTER);
                cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                cln.format(param -> {
                    return new TableCell<VSdMateriaisCorte, VSdMateriaisCorte>() {
                        final HBox boxButtonsRow = new HBox(3);
                        final Button btnSelectRolo = FormButton.create(btn -> {
                            btn.tooltip("Selecionar rolo para o risco");
                            btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                            btn.addStyle("default");
                        });
                        
                        @Override
                        protected void updateItem(VSdMateriaisCorte item, boolean empty) {
                            super.updateItem(item, empty);
                            setText(null);
                            setGraphic(null);
                            if (item != null && !empty) {
                                
                                btnSelectRolo.setGraphic(ImageUtils.getIcon((item.isSelected() ? ImageUtils.Icon.CANCEL : ImageUtils.Icon.ADICIONAR), ImageUtils.IconSize.MANUAL.size(20.0)));
                                //btnSelectRolo.setDisable(item.getResmtConsumir().compareTo(BigDecimal.ZERO) <= 0);
                                btnSelectRolo.setOnAction(evt -> {
                                    Boolean newValue = !item.isSelected(); // inverte a seleção da linha
                                    
                                    // adicionando ao consumo as aplicações que utilizam o mesmo material/cor do risco
                                    List<VSdConsumosOfCorApl> aplicacoesConsumoPeca = new ArrayList<>();
                                    aplicacoesConsumoPeca.addAll(aplicacoesMaterialCorRisco);
                                    // remove a aplicação do cálculo caso seja uma aplicação que tenha sido usada em um risco
                                    aplicacoesConsumoPeca.removeIf(consumo -> {
                                        // não remove caso a aplicação não foi riscada
                                        if (planejamentoAberto.getRiscos().stream()
                                                .filter(risco -> risco.getAplicacaoft().getCodigo().equals(consumo.getAplicpcp().getCodigo())).count() == 0)
                                            return false;
                                        // não remove caso tenha um risco cadastrado com a aplicação, porém sem nenhum material
                                        for (SdRiscoPlanejamentoEncaixe risco : planejamentoAberto.getRiscos().stream()
                                                .filter(risco -> risco.getAplicacaoft().getCodigo().equals(consumo.getAplicpcp().getCodigo())).collect(Collectors.toList())) {
                                            for (SdCorRisco cor : risco.getCores()) {
                                                String corProdutoConsumo = risco.getTiporisco().isUsacor()
                                                        ? cor.getCor()
                                                        : checkRiscoAgrupado(cor.getCor()) ? getCorIds(cor.getCor()).stream().map(SdCorRisco::getCor).distinct().collect(Collectors.joining(",")) : cor.getCor();
                                                if (Arrays.asList(corProdutoConsumo.replace("'", "").split(",")).contains(consumo.getCor())
                                                && cor.getMateriais().size() == 0) {
                                                    return false;
                                                }
                                            }
//
//
//                                            if (risco.getCores().stream().filter(cor -> (Arrays.asList((risco.getTiporisco().isUsacor()
//                                                    ? cor.getCor()
//                                                    : getCorIds(cor.getCor()).stream().map(SdCorRisco::getCor).distinct().collect(Collectors.joining(",")))
//                                                    .replace("'", "").split(",")).contains(consumo.getCor())) && cor.getMateriais().size() == 0).count() == 0)
//                                                return false;
                                        }
                                        
                                        return true;
                                    });
                                    BigDecimal consumoOutrasAplicacoesCorRisco = new BigDecimal((isPt.not().get() ? aplicacoesConsumoPeca :
                                            aplicacoesConsumoPeca.stream().filter(aplic -> aplic.getCor().equals(aplicacoesConsumoPeca.get(0).getCor())).collect(Collectors.toList())).stream()
                                            .mapToDouble(consumo -> consumo.getInsumo().getUnidade().getUnidade().equals("KG")
                                                    ? consumo.getConsumo().doubleValue() * (1000.0 / (item.getLargura().doubleValue() * item.getGramatura().doubleValue()))
                                                    : consumo.getConsumo().doubleValue())
                                            .sum()); // consumo por peça do tecido do risco em outra aplicação da mesma cor
                                    BigDecimal consumoDebrumCorRisco = new BigDecimal((isPt.not().get() ? aplicacoesMaterialCorRisco :
                                            aplicacoesMaterialCorRisco.stream().filter(aplic -> aplic.getCor().equals(aplicacoesMaterialCorRisco.get(0).getCor())).collect(Collectors.toList())).stream()
                                            .filter(consumo -> consumo.getAplicacao().equals("001"))
                                            .mapToDouble(consumo -> consumo.getInsumo().getUnidade().getUnidade().equals("KG")
                                                    ? consumo.getConsumo().doubleValue() * (1000.0 / (item.getLargura().doubleValue() * item.getGramatura().doubleValue()))
                                                    : consumo.getConsumo().doubleValue())
                                            .sum()); // consumo de debrum do risco
                                    BigDecimal consumoJaSelecionado = new BigDecimal(qtdeSelecionadaConsumo.get()).setScale(4, RoundingMode.DOWN); // saldo do consumo do risco
                                    BigDecimal saldoConsumo = corRisco.getConsumototal().subtract(consumoJaSelecionado); // saldo do consumo do risco
                                    Integer folhasTecidoSelecionado = consumoJaSelecionado.divide(comprimentoRisco, RoundingMode.DOWN).intValue(); // qtde folhas do tecido já reservado
                                    Integer saldoFolhas = corRisco.getFolhas() - folhasTecidoSelecionado.intValue(); // saldo das folhas que ainda precisa selecionar material
                                    
                                    // exibição de mensagem caso o usuário seleciona um novo rolo porém o consumo já foi totalmente atendido
                                    if (saldoConsumo.compareTo(BigDecimal.ZERO) <= 0 && newValue) {
                                        MessageBox.create(message -> {
                                            message.message("Os rolos já selecionados atendem o consumo do risco, verifique se realmente é necessário a inclusão deste rolo.");
                                            message.type(MessageBox.TypeMessageBox.ALERT);
                                            message.showAndWait();
                                        });
                                    }
                                    
                                    Integer folhasAConsumir = item.getFolhas() > saldoFolhas ? saldoFolhas : item.getFolhas();
                                    BigDecimal necessidadeConsumo = saldoConsumo.add(new BigDecimal(saldoFolhas * corRisco.getTotalGrade()).multiply(consumoOutrasAplicacoesCorRisco));
                                    
                                    AtomicReference<BigDecimal> consumoAplicacaoRolo = new AtomicReference<>(BigDecimal.ZERO);
                                    if (newValue) {
                                        item.setSelected(true);
                                        
                                        // sequencia dos rolos para enfesto
                                        AtomicReference<Integer> seqEnfesto = new AtomicReference<>(0);
                                        do {
                                            seqEnfesto.set(seqEnfesto.get() + 1);
                                        } while (corRisco.getMateriais().stream().anyMatch(mat -> mat.getSeqEnfesto() == seqEnfesto.get()));
                                        // adicionar ao consumo
                                        if (item.getResmtConsumir().compareTo(necessidadeConsumo) >= 0) {
                                            // caso o rolo atende o consumo do risco mais o tecido em outras aplicações nesta cor de produto
                                            // o consumo da aplicação será o que está selecionado no rolo
                                            consumoAplicacaoRolo.set(saldoConsumo);
                                            item.setQtdeConsumido(saldoConsumo);
                                            item.setFolhasConsumidas(saldoConsumo.divide(comprimentoRisco, RoundingMode.DOWN).intValue());
                                            item.setFolhasConsumidasOrig(item.getFolhasConsumidas());
                                            
                                            // adicionando item na tabela de itens selecionados
                                            SdMateriaisRiscoCor roloSelecionado = new SdMateriaisRiscoCor(corRisco.getId(), checkRiscoAgrupado(corRisco.getCor()) ? null : corRisco.getCor(), item.getCodigo().getCodigo(), item.getCori().getCor(),
                                                    item.getLote(), item.getTonalidade(), item.getPartida(), item.getLargura(),
                                                    item.getGramatura(), item.getSequencia(), item.getQtdeConsumido(), item.getFolhasConsumidas(),
                                                    (item.getFolhasConsumidas() * corRisco.getTotalGrade()), consumoDebrumCorRisco.multiply(new BigDecimal(item.getFolhasConsumidas() * corRisco.getTotalGrade())), seqEnfesto.get());
                                            corRisco.getMateriais().add(roloSelecionado);
                                            corRisco.getMateriaisObservable().add(roloSelecionado);
                                        } else {
                                            // caso o rolo não atende o consumo, será perguntado se quer selecionar mesmo assim esse rolo
//                                            if ((Boolean) QuestionBox.build(Boolean.class, message -> {
//                                                message.message("O rolo selecionado não atende o consumo para a grade gerada. Deseja realmente adicionar o rolo? Nesta rotina será removido o necessário deste rolo para consumir em outras aplicações que utilizam esse tecido e cor no produto.");
//                                                message.showAndWait();
//                                            }).value.get()) {

                                            //pensar aqui em um abatimento fazendo proporção entre outros consumos com o consumo do tecido adicionado
                                            //encontrar um indice para o cálculo.
                                            BigDecimal disponivelUsoRolo = item.getResmtConsumir().subtract(new BigDecimal(folhasAConsumir * corRisco.getTotalGrade()).multiply(consumoOutrasAplicacoesCorRisco));
                                            item.setFolhasConsumidas(disponivelUsoRolo.divide(comprimentoRisco, RoundingMode.DOWN).intValue());
                                            item.setQtdeConsumido(new BigDecimal(item.getFolhasConsumidas()).multiply(comprimentoRisco));
                                            item.setFolhasConsumidasOrig(item.getFolhasConsumidas());
                                            consumoAplicacaoRolo.set(item.getQtdeConsumido().setScale(4, RoundingMode.DOWN));
                                            
                                            // adicionando item na tabela de itens selecionados
                                            SdMateriaisRiscoCor roloSelecionado = new SdMateriaisRiscoCor(corRisco.getId(), checkRiscoAgrupado(corRisco.getCor()) ? null : corRisco.getCor(), item.getCodigo().getCodigo(), item.getCori().getCor(),
                                                    item.getLote(), item.getTonalidade(), item.getPartida(), item.getLargura(),
                                                    item.getGramatura(), item.getSequencia(), item.getQtdeConsumido(), item.getFolhasConsumidas(),
                                                    (item.getFolhasConsumidas() * corRisco.getTotalGrade()), consumoDebrumCorRisco.multiply(new BigDecimal(item.getFolhasConsumidas() * corRisco.getTotalGrade())), seqEnfesto.get());
                                            corRisco.getMateriais().add(roloSelecionado);
                                            corRisco.getMateriaisObservable().add(roloSelecionado);
//                                            } else {
//                                                item.setSelected(false);
//                                            }
                                        }
                                    } else {
                                        // remover do consumo
                                        consumoAplicacaoRolo.set(item.getQtdeConsumido().negate());
                                        item.setQtdeConsumido(BigDecimal.ZERO);
                                        item.setFolhasConsumidas(0);
                                        item.setSelected(false);
                                        corRisco.getMateriais().removeIf(mat -> mat.equals(item) && (mat.getId() == 0 || mat.getId() == null));
                                        corRisco.getMateriaisObservable().removeIf(mat -> mat.equals(item) && (mat.getId() == 0 || mat.getId() == null));
                                    }
                                    
                                    // saldo do rolo
                                    item.setResmtConsumir(item.getResmtConsumir().subtract(consumoAplicacaoRolo.get()));
                                    
                                    // atualização de valores para acumulo e exibição em tela
                                    // cálculo do consumo a partir da qtde de folhas digitada ou calculada
                                    qtdeSelecionadaConsumo.set(corRisco.getMateriais().stream().mapToDouble(consumido -> consumido.getConsumido().doubleValue()).sum());
                                    saldoConsumoSelecionado.set(corRisco.getConsumototal().doubleValue() - qtdeSelecionadaConsumo.get());
                                    // cálculo das folhas utilizadas para o risco
                                    qtdeFolhasSelecionadaConsumo.set(corRisco.getMateriais().stream().mapToInt(consumido -> consumido.getQtdefolhas()).sum());
                                    saldoFolhasConsumoSelecionado.set(corRisco.getFolhas() - qtdeFolhasSelecionadaConsumo.get());
                                    
                                    table.refresh();
                                });
                                boxButtonsRow.getChildren().clear();
                                boxButtonsRow.getChildren().addAll(btnSelectRolo);
                                setGraphic(boxButtonsRow);
                            }
                        }
                    };
                });
            }).build() /*Selecionar*/);
            table.factoryRow(param -> {
                return new TableRow<VSdMateriaisCorte>() {
                    @Override
                    protected void updateItem(VSdMateriaisCorte item, boolean empty) {
                        super.updateItem(item, empty);
                        clear();
                        if (item != null && !empty) {
                            if (item.isSelected())
                                getStyleClass().add("table-row-success");
                            else if (item.getResmtConsumir().compareTo(BigDecimal.ZERO) <= 0)
                                getStyleClass().add("table-row-danger");
                        }
                    }
                    
                    private void clear() {
                        getStyleClass().removeAll("table-row-success", "table-row-danger");
                    }
                };
            });
            table.indices(
                    table.indice(Color.LIGHTGREEN, "Rolo selecionado", false),
                    table.indice(Color.LIGHTCORAL, "Rolos indisponível", false),
                    table.indice(Color.WHITESMOKE, "Rolos disponíveis", false)
            );
        });
        final FormTableView<VSdConsumosOfCorApl> tblAplicacoesMaterialCorRisco = FormTableView.create(VSdConsumosOfCorApl.class, table -> {
            table.title("Consumos Material/Cor na Cor do Risco");
            table.items.set(FXCollections.observableList(aplicacoesMaterialCorRisco));
            table.columns(
                    FormTableColumn.create(cln -> {
                        cln.title("Cor");
                        cln.width(40.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdConsumosOfCorApl, VSdConsumosOfCorApl>, ObservableValue<VSdConsumosOfCorApl>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCor()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*Cor*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Aplicação");
                        cln.width(180.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdConsumosOfCorApl, VSdConsumosOfCorApl>, ObservableValue<VSdConsumosOfCorApl>>) param -> new ReadOnlyObjectWrapper(param.getValue().getAplicpcp().toString()));
                    }).build() /*Aplic.*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Material");
                        cln.width(90.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdConsumosOfCorApl, VSdConsumosOfCorApl>, ObservableValue<VSdConsumosOfCorApl>>) param -> new ReadOnlyObjectWrapper(param.getValue().getInsumo().getCodigo()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*Material*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Cor Mat.");
                        cln.width(55.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdConsumosOfCorApl, VSdConsumosOfCorApl>, ObservableValue<VSdConsumosOfCorApl>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCori().getCor()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*Cor Mat.*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Cons. Risco");
                        cln.width(70.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdConsumosOfCorApl, VSdConsumosOfCorApl>, ObservableValue<VSdConsumosOfCorApl>>) param ->
                                new ReadOnlyObjectWrapper(StringUtils.toDecimalFormat(param.getValue().getConsumoMt().doubleValue() * corRisco.getTotalRealizado(), 4)));
                        cln.alignment(Pos.CENTER_RIGHT);
                    }).build() /*Consumo*/);
        });
        final FormTableView<VSdConsumosOfCorApl> tblAplicacoesMaterialOutraCor = FormTableView.create(VSdConsumosOfCorApl.class, table -> {
            table.title("Consumos Material/Cor em Outras Cores");
            table.items.set(FXCollections.observableList(aplicacoesMaterialOutraCorRisco));
            table.columns(
                    FormTableColumn.create(cln -> {
                        cln.title("Cor");
                        cln.width(40.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdConsumosOfCorApl, VSdConsumosOfCorApl>, ObservableValue<VSdConsumosOfCorApl>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCor()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*Cor*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Aplicação");
                        cln.width(180.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdConsumosOfCorApl, VSdConsumosOfCorApl>, ObservableValue<VSdConsumosOfCorApl>>) param -> new ReadOnlyObjectWrapper(param.getValue().getAplicpcp().toString()));
                    }).build() /*Aplic.*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Material");
                        cln.width(90.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdConsumosOfCorApl, VSdConsumosOfCorApl>, ObservableValue<VSdConsumosOfCorApl>>) param -> new ReadOnlyObjectWrapper(param.getValue().getInsumo().getCodigo()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*Material*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Cor Mat.");
                        cln.width(55.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdConsumosOfCorApl, VSdConsumosOfCorApl>, ObservableValue<VSdConsumosOfCorApl>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCori().getCor()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*Cor Mat.*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Cons. Tot.");
                        cln.width(70.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdConsumosOfCorApl, VSdConsumosOfCorApl>, ObservableValue<VSdConsumosOfCorApl>>) param ->
                                new ReadOnlyObjectWrapper(StringUtils.toDecimalFormat(param.getValue().getConsumoTotalMt().doubleValue(), 4)));
                        cln.alignment(Pos.CENTER_RIGHT);
                    }).build() /*Consumo*/);
        });
        final FormTableView<VSdConsumosOfCorApl> tblAplicacoesMateriaisCorRisco = FormTableView.create(VSdConsumosOfCorApl.class, table -> {
            table.title("Outros Consumos deste Risco");
            table.items.set(FXCollections.observableList(aplicacoesMateriaisRiscoCor));
            table.columns(
                    FormTableColumn.create(cln -> {
                        cln.title("Cor");
                        cln.width(40.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdConsumosOfCorApl, VSdConsumosOfCorApl>, ObservableValue<VSdConsumosOfCorApl>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCor()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*Cor*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Aplicação");
                        cln.width(180.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdConsumosOfCorApl, VSdConsumosOfCorApl>, ObservableValue<VSdConsumosOfCorApl>>) param -> new ReadOnlyObjectWrapper(param.getValue().getAplicpcp().toString()));
                    }).build() /*Aplic.*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Material");
                        cln.width(90.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdConsumosOfCorApl, VSdConsumosOfCorApl>, ObservableValue<VSdConsumosOfCorApl>>) param -> new ReadOnlyObjectWrapper(param.getValue().getInsumo().getCodigo()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*Material*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Cor Mat.");
                        cln.width(55.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdConsumosOfCorApl, VSdConsumosOfCorApl>, ObservableValue<VSdConsumosOfCorApl>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCori().getCor()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*Cor Mat.*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Cons. Risco");
                        cln.width(70.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdConsumosOfCorApl, VSdConsumosOfCorApl>, ObservableValue<VSdConsumosOfCorApl>>) param ->
                                new ReadOnlyObjectWrapper(StringUtils.toDecimalFormat(param.getValue().getConsumoMt().doubleValue() * corRisco.getTotalRealizado(), 4)));
                        cln.alignment(Pos.CENTER_RIGHT);
                    }).build() /*Consumo*/);
        });
        final FormTableView<SdMateriaisRiscoCor> tblRolosSelecionados = FormTableView.create(SdMateriaisRiscoCor.class, table -> {
            table.title("Rolos Selecionados");
            table.expanded();
            table.items.bind(corRisco.materiaisObservableProperty());
            table.editable.set(true);
            table.columns(
                    FormTableColumn.create(cln -> {
                        cln.title("Material");
                        cln.width(95.0);
                        cln.alignment(Pos.CENTER);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdMateriaisRiscoCor, SdMateriaisRiscoCor>, ObservableValue<SdMateriaisRiscoCor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getMaterial()));
                    }).build() /*Material*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Cor");
                        cln.width(50.0);
                        cln.alignment(Pos.CENTER);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdMateriaisRiscoCor, SdMateriaisRiscoCor>, ObservableValue<SdMateriaisRiscoCor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCori()));
                    }).build() /*Cor*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Lote");
                        cln.width(90.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdMateriaisRiscoCor, SdMateriaisRiscoCor>, ObservableValue<SdMateriaisRiscoCor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLote()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*Lote*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Tonalidade");
                        cln.width(120.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdMateriaisRiscoCor, SdMateriaisRiscoCor>, ObservableValue<SdMateriaisRiscoCor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTonalidade()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*Tonalidade*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Partida");
                        cln.width(55.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdMateriaisRiscoCor, SdMateriaisRiscoCor>, ObservableValue<SdMateriaisRiscoCor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPartida()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*Part.*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Seq.");
                        cln.width(35.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdMateriaisRiscoCor, SdMateriaisRiscoCor>, ObservableValue<SdMateriaisRiscoCor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getSequencia()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*Seq.*/,
//                    FormTableColumn.create(cln -> {
//                        cln.title("Folhas");
//                        cln.width(50.0);
//                        cln.value((Callback<TableColumn.CellDataFeatures<SdMateriaisRiscoCor, SdMateriaisRiscoCor>, ObservableValue<SdMateriaisRiscoCor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtdefolhas()));
//                        cln.alignment(Pos.CENTER);
//                    }).build() /*Folhas*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Folhas");
                        cln.width(65.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdMateriaisRiscoCor, SdMateriaisRiscoCor>, ObservableValue<SdMateriaisRiscoCor>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                        cln.alignment(Pos.CENTER);
                        cln.editable.set(true);
                        cln.format(param -> {
                            return new TableCell<SdMateriaisRiscoCor, SdMateriaisRiscoCor>() {
                                @Override
                                protected void updateItem(SdMateriaisRiscoCor item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    setGraphic(null);
                                    if (item != null && !empty) {
                                        setGraphic(FormFieldText.create(field -> {
                                            field.withoutTitle();
//                                            field.value.set(StringUtils.toIntegerFormat(item.getFolhasConsumidas()));
                                            field.value.set(StringUtils.toIntegerFormat(item.getQtdefolhas()));
                                            field.mask(FormFieldText.Mask.INTEGER);
                                            field.alignment(Pos.CENTER);
                                            field.addStyle("xs").addStyle("info");
                                            field.value.addListener((observable, oldValue, newValue) -> {
                                                if (newValue != null) {
                                                    try {
                                                        BigDecimal consumoDebrumCorRisco = new BigDecimal((isPt.not().get() ? aplicacoesMaterialCorRisco :
                                                                aplicacoesMaterialCorRisco.stream().filter(aplic -> aplic.getCor().equals(aplicacoesMaterialCorRisco.get(0).getCor())).collect(Collectors.toList())).stream()
                                                                .filter(consumo -> consumo.getAplicacao().equals("001"))
                                                                .mapToDouble(consumo -> consumo.getInsumo().getUnidade().getUnidade().equals("KG")
                                                                        ? consumo.getConsumo().doubleValue() * (1000.0 / (item.getLargura().doubleValue() * item.getGramatura().doubleValue()))
                                                                        : consumo.getConsumo().doubleValue())
                                                                .sum()); // consumo de debrum do risco

                                                        // atualizando dados do rolo selecionado
                                                        item.setQtdefolhas(new BigDecimal(StringUtils.unformatDecimal(newValue)).intValue());
                                                        item.setConsumido(new BigDecimal(item.getQtdefolhas()).multiply(comprimentoRisco));
                                                        item.setQtdepecas(item.getQtdefolhas() * corRisco.getTotalGrade());
                                                        item.setConsumidoDebrum(consumoDebrumCorRisco.multiply(new BigDecimal(item.getQtdepecas())));
                                                        field.value.set(newValue);
                                                        
                                                        // atualizando dados do rolo reservado
                                                        VSdMateriaisCorte roloReservado = rolosReservados.stream()
                                                                .filter(it -> it.getCodigo().getCodigo().equals(item.getMaterial()) && it.getCori().getCor().equals(item.getCori()) && it.getLote().equals(item.getLote()) &&
                                                                        it.getPartida().equals(item.getPartida()) && it.getSequencia().equals(item.getSequencia()) && it.getTonalidade().equals(item.getTonalidade()))
                                                                .findAny().get();
                                                        BigDecimal difFolhas = new BigDecimal(StringUtils.unformatDecimal(newValue)).subtract(new BigDecimal(StringUtils.unformatDecimal(oldValue)));
                                                        roloReservado.setResmtConsumir(roloReservado.getResmtConsumir().subtract(difFolhas.multiply(comprimentoRisco)));
                                                        roloReservado.setFolhas(roloReservado.getResmtConsumir().divide(comprimentoRisco, RoundingMode.DOWN).intValue());
                                                        roloReservado.setFolhasConsumidas(new BigDecimal(StringUtils.unformatDecimal(newValue)).intValue());
                                                        roloReservado.setQtdeConsumido(new BigDecimal(roloReservado.getFolhasConsumidas()).multiply(comprimentoRisco));
                                                        
                                                        // cálculo do consumo a partir da qtde de folhas digitada ou calculada
                                                        qtdeSelecionadaConsumo.set(corRisco.getMateriais().stream().mapToDouble(consumido -> consumido.getConsumido().doubleValue()).sum());
                                                        saldoConsumoSelecionado.set(corRisco.getConsumototal().doubleValue() - qtdeSelecionadaConsumo.get());
                                                        // cálculo das folhas utilizadas para o risco
                                                        qtdeFolhasSelecionadaConsumo.set(corRisco.getMateriais().stream().mapToInt(consumido -> consumido.getQtdefolhas()).sum());
                                                        saldoFolhasConsumoSelecionado.set(corRisco.getFolhas() - qtdeFolhasSelecionadaConsumo.get());
                                                        
                                                        table.refresh();
                                                        tblRolosDisponiveis.refresh();
                                                    } catch (NumberFormatException exc) {
                                                        exc.printStackTrace();
                                                    }
                                                }
                                            });
                                        }).build());
                                    }
                                }
                            };
                        });
                    }).build() /*Folhas Cons.*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Consumido");
                        cln.width(80.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdMateriaisRiscoCor, SdMateriaisRiscoCor>, ObservableValue<SdMateriaisRiscoCor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getConsumido()));
                        cln.alignment(Pos.CENTER_RIGHT);
                        cln.format(param -> {
                            return new TableCell<VSdMateriaisCorte, BigDecimal>() {
                                @Override
                                protected void updateItem(BigDecimal item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    if (item != null && !empty) {
                                        setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                                    }
                                }
                            };
                        });
                    }).build() /*Consumido*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Enfesto");
                        cln.width(50.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdMateriaisRiscoCor, SdMateriaisRiscoCor>, ObservableValue<SdMateriaisRiscoCor>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                        cln.format(param -> {
                            return new TableCell<SdMateriaisRiscoCor, SdMateriaisRiscoCor>() {
                                @Override
                                protected void updateItem(SdMateriaisRiscoCor item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    setGraphic(null);
                                    if (item != null && !empty) {
                                        setGraphic(FormFieldText.create(field -> {
                                            field.withoutTitle();
                                            field.editable(true);
                                            field.addStyle("xs").addStyle("success");
                                            field.alignment(Pos.CENTER);
                                            field.value.bindBidirectional(item.seqEnfestoProperty(), new NumberStringConverter());
                                        }).build());
                                    }
                                }
                            };
                        });
                        cln.order(Comparator.comparing(o -> ((SdMateriaisRiscoCor) o).getSeqEnfesto()));
                    }).build() /*Enfesto*/
            );
            table.factoryRow(param -> {
                return new TableRow<SdMateriaisRiscoCor>() {
                    @Override
                    protected void updateItem(SdMateriaisRiscoCor item, boolean empty) {
                        super.updateItem(item, empty);
                        setContextMenu(null);
                        if (item != null && !empty)
                            setContextMenu(FormContextMenu.create(menu -> {
                                menu.addItem(menuItem -> {
                                    menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.TRASH, ImageUtils.IconSize._16));
                                    menuItem.setText("Desmarcar Rolo");
                                    menuItem.setOnAction(evt -> {
                                        SdMateriaisRiscoCor roloParaExcluir = item;
                                        if (roloParaExcluir != null) {
                                            VSdMateriaisCorte roloReservado = rolosReservados.stream()
                                                    .filter(it -> it.getCodigo().getCodigo().equals(roloParaExcluir.getMaterial()) && it.getCori().getCor().equals(roloParaExcluir.getCori()) && it.getLote().equals(roloParaExcluir.getLote()) &&
                                                            it.getPartida().equals(roloParaExcluir.getPartida()) && it.getSequencia().equals(roloParaExcluir.getSequencia()) && it.getTonalidade().equals(roloParaExcluir.getTonalidade()))
                                                    .findAny().get();
                                            
                                            roloReservado.setResmtConsumir(roloReservado.getResmtConsumir().add(roloParaExcluir.getConsumido()));
                                            roloReservado.setFolhas(roloReservado.getResmtConsumir().divide(comprimentoRisco, RoundingMode.DOWN).intValue());
                                            roloReservado.setQtdeConsumido(BigDecimal.ZERO);
                                            roloReservado.setFolhasConsumidas(0);
                                            roloReservado.setSelected(false);
                                            
                                            //new FluentDao().delete(roloParaExcluir);
                                            try {
                                                new NativeDAO().runNativeQueryUpdate(String.format("delete from sd_materiais_risco_cor_001 where id = %s", roloParaExcluir.getId()));
                                                corRisco.getMateriais().remove(roloParaExcluir);
                                                corRisco.getMateriaisObservable().remove(roloParaExcluir);
                                                
                                                // cálculo do consumo a partir da qtde de folhas digitada ou calculada
                                                qtdeSelecionadaConsumo.set(corRisco.getMateriais().stream().mapToDouble(consumido -> consumido.getConsumido().doubleValue()).sum());
                                                saldoConsumoSelecionado.set(corRisco.getConsumototal().doubleValue() - qtdeSelecionadaConsumo.get());
                                                // cálculo das folhas utilizadas para o risco
                                                qtdeFolhasSelecionadaConsumo.set(corRisco.getMateriais().stream().mapToInt(consumido -> consumido.getQtdefolhas()).sum());
                                                saldoFolhasConsumoSelecionado.set(corRisco.getFolhas() - qtdeFolhasSelecionadaConsumo.get());
                                                
                                                SysLogger.addSysDelizLog("Planejamento Encaixe", TipoAcao.EDITAR, planejamentoAberto.getNumero().getNumero(),
                                                        "Excluído rolos na cor ID " + corRisco.getId() + " no risco ID " + risco.getId()
                                                                + " (" + risco.getTiporisco().getCodrisco() + risco.getSeqTipo() + ") no planejamento ID " + planejamentoAberto.getId());
                                                
                                                table.refresh();
                                                tblRolosDisponiveis.refresh();
                                            } catch (SQLException e) {
                                                e.printStackTrace();
                                                ExceptionBox.build(message -> {
                                                    message.exception(e);
                                                    message.showAndWait();
                                                });
                                            }
                                        }
                                    });
                                });
                            }));
                    }
                };
            });
        });
        // dados material
        final FormFieldText reservaTotalField = FormFieldText.create(field -> {
            field.title("Res. Total");
            field.editable(false);
            field.expanded();
            field.addStyle("primary");
            field.value.set(StringUtils.toDecimalFormat(rolosReservados.stream().mapToDouble(rolo -> rolo.getReservamt().doubleValue()).sum(), 4));
            field.alignment(Pos.CENTER_RIGHT);
        });
        final FormFieldText reservaDisponivelField = FormFieldText.create(field -> {
            field.title("Res. Disponível");
            field.editable(false);
            field.expanded();
            field.addStyle("success");
            field.value.set(StringUtils.toDecimalFormat(rolosReservados.stream().mapToDouble(rolo -> rolo.getResmtConsumir().doubleValue()).sum(), 4));
            field.alignment(Pos.CENTER_RIGHT);
        });
        final FormFieldText totalConsumoMaterialMesmaCorField = FormFieldText.create(field -> {
            field.title("Mesma Cor");
            field.editable(false);
            field.expanded();
            field.addStyle("warning");
            field.value.set(StringUtils.toDecimalFormat(aplicacoesMaterialCorRisco.stream()
                    .mapToDouble(consumo -> consumo.getConsumototal().doubleValue())
                    .sum(), 4));
            field.alignment(Pos.CENTER_RIGHT);
        });
        final FormFieldText totalConsumoMaterialOutrasCoresField = FormFieldText.create(field -> {
            field.title("Outra Cor");
            field.editable(false);
            field.expanded();
            field.addStyle("danger");
            field.value.set(StringUtils.toDecimalFormat(aplicacoesMaterialOutraCorRisco.stream().mapToDouble(consumo -> consumo.getConsumototal().doubleValue()).sum(), 4));
            field.alignment(Pos.CENTER_RIGHT);
        });
        // consumo risco
        final FormFieldText consumoDoRiscoField = FormFieldText.create(field -> {
            field.title("Consumo Risco");
            field.editable(false);
            field.expanded();
            field.addStyle("sm");
            field.value.set(StringUtils.toDecimalFormat(corRisco.getConsumototal().doubleValue(), 4));
            field.alignment(Pos.CENTER_RIGHT);
            if (new BigDecimal(rolosReservados.stream().mapToDouble(rolo -> rolo.getResmtConsumir().doubleValue()).sum()).compareTo(corRisco.getConsumototal()) < 0) {
                field.addStyle("danger");
                field.tooltip("Não existe material reservado o suficiente para atender o risco.");
            }
        });
        final FormFieldText consumoAlocadoRiscoField = FormFieldText.create(field -> {
            field.title("Qtde. Alocado");
            field.editable(false);
            field.expanded();
            field.addStyle("sm").addStyle("success");
            field.value.bind(viewQtdeSelecionadaConsumo);
            field.alignment(Pos.CENTER_RIGHT);
        });
        final FormFieldText saldoConsumoField = FormFieldText.create(field -> {
            field.title("Saldo");
            field.editable(false);
            field.expanded();
            field.addStyle("sm").addStyle("warning");
            field.value.bind(viewSaldoConsumoSelecionado);
            field.alignment(Pos.CENTER_RIGHT);
        });
        final FormFieldText folhasDoRiscoField = FormFieldText.create(field -> {
            field.title("Consumo Risco");
            field.editable(false);
            field.expanded();
            field.addStyle("lg");
            field.value.set(StringUtils.toIntegerFormat(corRisco.getFolhas()));
            field.alignment(Pos.CENTER_RIGHT);
            if (new BigDecimal(rolosReservados.stream().mapToDouble(rolo -> rolo.getResmtConsumir().doubleValue()).sum()).compareTo(corRisco.getConsumototal()) < 0) {
                field.addStyle("danger");
                field.tooltip("Não existe material reservado o suficiente para atender o risco.");
            }
        });
        final FormFieldText folhasAlocadasRiscoField = FormFieldText.create(field -> {
            field.title("Qtde. Alocado");
            field.editable(false);
            field.expanded();
            field.addStyle("lg").addStyle("success");
            field.value.bind(viewQtdeFolhasSelecionadaConsumo);
            field.alignment(Pos.CENTER_RIGHT);
        });
        final FormFieldText saldoFolhasField = FormFieldText.create(field -> {
            field.title("Saldo");
            field.editable(false);
            field.expanded();
            field.addStyle("lg").addStyle("warning");
            field.value.bind(viewSaldoFolhasConsumoSelecionado);
            field.alignment(Pos.CENTER_RIGHT);
        });
        // dados adicionais risco
        final FormFieldText comprimentoRiscoField = FormFieldText.create(field -> {
            field.title("Comp. Folha");
            field.value.set(StringUtils.toDecimalFormat(comprimentoRisco.doubleValue(), 4));
            field.addStyle("info");
            field.editable(false);
        });
        final FormFieldText descricaoRiscoField = FormFieldText.create(field -> {
            field.title("Risco");
            field.value.set(risco.getTiporisco().getCodrisco() + risco.getSeqTipo() + "." + corRisco.getSeq());
            field.editable(false);
            field.addStyle("lg").addStyle("warning");
        });
        final FormFieldText corRiscoField = FormFieldText.create(field -> {
            field.title("Cor");
            field.value.set(Arrays.asList(coresRisco).stream().collect(Collectors.joining(",")));
            field.editable(false);
        });
        final FormFieldText pecasRiscoField = FormFieldText.create(field -> {
            field.title("Tot. Peças Risco");
            field.value.set(StringUtils.toIntegerFormat(corRisco.getTotalRealizado()));
            field.editable(false);
        });
        final FormFieldText saldoPlanejamentoField = FormFieldText.create(field -> {
            field.title("Saldo Plan. Cor");
            field.value.set(StringUtils.toIntegerFormat(corRisco.getTotalSaldoOf()));
            field.editable(false);
        });
        // </editor-fold>
        
        // criação do fragmento para seleção dos rolos
        new Fragment().show(fragment1 -> {
            fragment1.title("Seleção de Rolos para Risco");
            fragment1.size(1500.0, 700.0);
            fragment1.box.getChildren().add(tblRolosDisponiveis.build());
            fragment1.box.getChildren().add(FormBox.create(boxConsumoReserva -> {
                boxConsumoReserva.horizontal();
                boxConsumoReserva.height(350.0);
                boxConsumoReserva.add(FormBox.create(boxDadosConsumo -> {
                    boxDadosConsumo.vertical();
                    boxDadosConsumo.width(380.0);
                    boxDadosConsumo.add(FormBox.create(dadosReservaConsumo -> {
                        dadosReservaConsumo.horizontal();
                        dadosReservaConsumo.add(
                                reservaTotalField.build(), // reserva total
                                reservaDisponivelField.build(), // reserva disponível para o risco
                                totalConsumoMaterialMesmaCorField.build(), // consumos na mesma cor em outras aplicações
                                totalConsumoMaterialOutrasCoresField.build()  // consumos em outras cores
                        );
                    })); // dados de reservas e consumos aplicações
                    boxDadosConsumo.add(new Separator(Orientation.HORIZONTAL));
                    boxDadosConsumo.add(FormBox.create(dadosReservaConsumo -> {
                        dadosReservaConsumo.horizontal();
                        dadosReservaConsumo.add(
                                consumoDoRiscoField.build(), // consumo do risco
                                consumoAlocadoRiscoField.build(), // qtde alocado
                                saldoConsumoField.build()  // saldo
                        );
                    })); // dados de seleção de rolos (metragem)
                    boxDadosConsumo.add(FormBox.create(dadosReservaConsumo -> {
                        dadosReservaConsumo.horizontal();
                        dadosReservaConsumo.add(
                                folhasDoRiscoField.build(), // consumo do risco
                                folhasAlocadasRiscoField.build(), // qtde alocado
                                saldoFolhasField.build()  // saldo
                        );
                    })); // dados de seleção de rolos (folhas)
                    boxDadosConsumo.add(new Separator(Orientation.HORIZONTAL));
                    boxDadosConsumo.add(FormBox.create(boxInfosAdicionais -> {
                        boxInfosAdicionais.horizontal();
                        boxInfosAdicionais.add(
                                descricaoRiscoField.build(),
                                corRiscoField.build(),
                                comprimentoRiscoField.build()
                        );
                    }));
                    boxDadosConsumo.add(FormBox.create(boxInfosAdicionais -> {
                        boxInfosAdicionais.horizontal();
                        boxInfosAdicionais.add(
                                pecasRiscoField.build(),
                                saldoPlanejamentoField.build()
                        );
                    }));
                })); // box com os totais de consumo e selecionado
                boxConsumoReserva.add(FormBox.create(boxConsumos -> {
                    boxConsumos.vertical();
                    boxConsumos.width(450.0);
                    boxConsumos.add(tblAplicacoesMaterialCorRisco.build()); // table consumos da cor do risco com o tecido do risco
                    boxConsumos.add(tblAplicacoesMaterialOutraCor.build()); // table consumos de outras cores com o tecido do risco
                    boxConsumos.add(tblAplicacoesMateriaisCorRisco.build()); // table outros consumos da cor do risco
                })); // box com as tabelas de aplicações/consumos
                boxConsumoReserva.add(tblRolosSelecionados.build()); // table dos rolos selecionados
            }));
            fragment1.buttonsBox.getChildren().add(FormButton.create(btnSelecionar -> {
                btnSelecionar.title("Selecionar Rolos");
                btnSelecionar.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                btnSelecionar.addStyle("success");
                btnSelecionar.setAction(event -> {
                    // validação de rolos selecionados
//                    if (rolosReservados.stream().filter(rolo -> rolo.isSelected()).count() == 0) {
//                        MessageBox.create(message -> {
//                            message.message("Você precisa selecionar ao menos um rolo para o risco.");
//                            message.type(MessageBox.TypeMessageBox.IMPORTANT);
//                            message.showAndWait();
//                        });
//                        return;
//                    }
                    
                    // validação de atendimento de consumo do risco
                    try {
                        BigDecimal qtdeTotalSelecionada = new BigDecimal(rolosReservados.stream().filter(rolo -> rolo.isSelected()).mapToDouble(rolo -> rolo.getQtdeConsumido().doubleValue()).sum());
                        final BigDecimal[] qtdeConsumoRisco = {corRisco.getConsumototal().subtract(new BigDecimal(corRisco.getMateriais().stream().mapToDouble(materiais -> materiais.getConsumido().doubleValue()).sum()))};
                        // alerta se a qtde dos rolos é suficiente para a reserva com confirmação se que continuar
                        if (qtdeTotalSelecionada.setScale(4, RoundingMode.DOWN).compareTo(qtdeConsumoRisco[0].setScale(4, RoundingMode.DOWN)) < 0) {
                            if (!((Boolean) QuestionBox.build(Boolean.class, message -> {
                                message.message("A quantidade de reserva selecionada é menor que o risco está consumindo, estão faltando " +
                                        StringUtils.toDecimalFormat(qtdeTotalSelecionada.subtract(corRisco.getConsumototal()).doubleValue(), 4) +
                                        ". Deseja processeguir o risco com a falta do material?");
                                message.showAndWait();
                            }).value.get())) {
                                return;
                            }
                        }
                        
                        new FluentDao().merge(corRisco);
                        corRisco.getMateriais().forEach(roloRisco -> roloRisco = new FluentDao().merge(roloRisco));
//                    rolosReservados.stream().filter(rolo -> rolo.isSelected()).forEach(rolo -> {
//                        BigDecimal qtdeParaConsumir = new BigDecimal(rolo.getFolhasConsumidas()).multiply(corRisco.getComprimento().add(new BigDecimal(0.06)));
////                                                if (rolo.getResmtConsumir().compareTo(qtdeConsumoRisco[0]) > 0) {
////                                                    qtdeParaConsumir = qtdeConsumoRisco[0];
////                                                } else {
////                                                    qtdeConsumoRisco[0] = qtdeConsumoRisco[0].subtract(rolo.getResmtConsumir());
////                                                }
//
//                        SdMateriaisRiscoCor roloSelecionado = new SdMateriaisRiscoCor(corRisco.getId(), checkRiscoAgrupado(corRisco.getCor()) ? null : corRisco.getCor(), rolo.getCodigo().getCodigo(), rolo.getCori().getCor(),
//                                rolo.getLote(), rolo.getTonalidade(), rolo.getPartida(), rolo.getLargura(),
//                                rolo.getGramatura(), rolo.getSequencia(), qtdeParaConsumir, rolo.getFolhasConsumidas(),
//                                (rolo.getFolhasConsumidas() * corRisco.getTotalGrade()));
//                        try {
//                            roloSelecionado = new FluentDao().persist(roloSelecionado);
//                            corRisco.getMateriais().add(roloSelecionado);
//                        } catch (SQLException e) {
//                            e.printStackTrace();
//                            ExceptionBox.build(message -> {
//                                message.exception(e);
//                                message.showAndWait();
//                            });
//                        }
//                    });
                        
                        if (rolosReservados.stream().findAny().get().getCodigo().getUnidade().getUnidade().toUpperCase().equals("KG")) {
                            SdMateriaisRiscoCor rolo = corRisco.getMateriais().stream().findAny().get();
                            if (corRisco.getConsumopecakg().compareTo(BigDecimal.ZERO) == 0) {
                                if (((VBox) boxSetMateriais.getChildren().get(0)).getChildren().size() > 3)
                                    ((VBox) boxSetMateriais.getChildren().get(0)).getChildren().remove(3);
                                
                                boxSetMateriais.add(FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.label("Cons. Pç.:");
                                    field.postLabel("kg");
                                    field.editable(false);
                                    field.alignment(Pos.CENTER);
                                    field.value.bind(corRisco.consumopecakgProperty().asString("%.4f"));
                                }).build());
                            }
                            
                            BigDecimal calc1 = rolo.getGramatura().multiply(rolo.getLargura());
                            BigDecimal calc2 = new BigDecimal(1000).divide(calc1, 15, RoundingMode.DOWN);
                            BigDecimal calc3 = corRisco.getConsumototal().divide(calc2, 15, RoundingMode.DOWN);
                            BigDecimal calc4 = calc3.divide(new BigDecimal(corRisco.getTotalRealizado()), 4, RoundingMode.DOWN);
                            
                            corRisco.setConsumopecakg(calc4);
                        }
                        
                        if (corRisco.getMateriais().stream().mapToInt(SdMateriaisRiscoCor::getQtdefolhas).sum() != corRisco.getFolhas()) {
                            if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                                message.message("O somatório das folhas selecionadas está diferentes do risco planejado. Deseja atualizar as folhas do planejamento?");
                                message.showAndWait();
                            }).value.get())) {
                                fixarGrade.value.set(true);
                                corRisco.setFolhas(corRisco.getMateriais().stream().mapToInt(SdMateriaisRiscoCor::getQtdefolhas).sum());
                            }
                        }
                        
                        fragment1.close();
                        
                        SysLogger.addSysDelizLog("Planejamento Encaixe", TipoAcao.EDITAR, planejamentoAberto.getNumero().getNumero(),
                                "Selecionado rolos para a cor ID " + corRisco.getId() + " no risco ID " + risco.getId()
                                        + " (" + risco.getTiporisco().getCodrisco() + risco.getSeqTipo() + ") no planejamento ID " + planejamentoAberto.getId());
                    } catch (Exception e) {
                        e.printStackTrace();
                        ExceptionBox.build(message -> {
                            message.exception(e);
                            message.showAndWait();
                        });
                    }
                });
            }));
        });
        
    }
    
    /**
     * function para salvar o objeto no banco e colocar no array do objeto do planejamento
     *
     * @param riscoPlanejamento
     */
    private void salvarRisco(SdRiscoPlanejamentoEncaixe riscoPlanejamento) {
        // validação arquivo diamino
        if (riscoPlanejamento.getArqdiamino() == null) {
            MessageBox.create(message -> {
                message.message("Você deve preencher o campo com o nome do arquivo diamino para leitura no corte.");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showAndWait();
            });
            return;
        }
        
        // validação risco não criado
        if (riscoPlanejamento.getCores().size() == 0 || riscoPlanejamento.getCores().stream().anyMatch(it -> it.getTotalRealizado() == 0)) {
            MessageBox.create(message -> {
                message.message("Você não finalizou as grades para este risco, primeiramente você deve definir as grades e após salvar o risco.");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showAndWait();
            });
            return;
        }
        
        // confirmação de finalização sem tecidos selecionados
        if (riscoPlanejamento.getCores().stream().anyMatch(it -> it.getMateriais().size() == 0)) {
            if (!((Boolean) QuestionBox.build(Boolean.class, message -> {
                message.message("Você não selecionou rolos para este risco, deseja salvar o risco e depois selecionar os rolos?");
                message.showAndWait();
            }).value.get())) {
                return;
            }
        }
        
        // atualização das partes do risco, para preenchimento no próximo risco.
        // aqui vamos ter que verificar como fazer para as partes serem divididas por aplicação
        if (riscoPlanejamento.getPartes() != planejamentoAberto.getNumero().getProduto().getPartesMolde()) {
            new FluentDao().runNativeQueryUpdate(String.format("update sd_produto_001 set partes_modelagem = %d where codigo = '%s'", riscoPlanejamento.getPartes(), planejamentoAberto.getNumero().getProduto().getCodigo()));
            planejamentoAberto.getNumero().getProduto().setPartesMolde(riscoPlanejamento.getPartes());
        }
        // persistencia do risco
        new FluentDao().merge(planejamentoAberto);
        
        SysLogger.addSysDelizLog("Planejamento Encaixe", TipoAcao.EDITAR, planejamentoAberto.getNumero().getNumero(),
                "Salvando risco ID " + riscoPlanejamento.getId()
                        + " (" + riscoPlanejamento.getTiporisco().getCodrisco() + riscoPlanejamento.getSeqTipo() + ") no planejamento ID " + planejamentoAberto.getId());
        criandoRisco.set(false);
        MessageBox.create(message -> {
            message.message("Risco gravado com sucesso!");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }
    
    private void cancelarRisco(SdRiscoPlanejamentoEncaixe riscoPlanejamento) {
        riscoPlanejamento.getCores().forEach(cor -> {
            cor.getGrade().forEach(grade -> new FluentDao().delete(grade));
            cor.getMateriais().forEach(material -> new FluentDao().delete(material));
            new FluentDao().delete(cor);
        });
        new FluentDao().delete(riscoPlanejamento);
        
        SysLogger.addSysDelizLog("Planejamento Encaixe", TipoAcao.EXCLUIR, planejamentoAberto.getNumero().getNumero(),
                "Excluindo risco ID " + riscoPlanejamento.getId()
                        + " (" + riscoPlanejamento.getTiporisco().getCodrisco() + riscoPlanejamento.getSeqTipo() + ") do planejamento ID " + planejamentoAberto.getId());
        
        planejamentoAberto.getRiscos().remove(riscoPlanejamento);
        criandoRisco.set(false);
        MessageBox.create(message -> {
            message.message("Risco excluído com sucesso!");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }
    
    private void cleanFieldsCargaOf() {
        //fields
        referenciaFild.clear();
        descricaoProdutoField.clear();
        linhaProdutoField.clear();
        gradeProdutoField.clear();
        familiaProdutoField.clear();
        marcaProdutoField.clear();
        periodoOfField.clear();
        periodoOfField.removeStyle("mostruario");
        observacaoOfField.clear();
        numeroOfField.clear();
        //objects
        beanOfCarregada.set(null);
        dhEncaixeField.value.unbindBidirectional(planejamentoAberto.dtplanoProperty());
        usuarioEncaixeField.value.unbindBidirectional(planejamentoAberto.usuarioProperty());
        encaixeComGabaritoField.value.unbindBidirectional(planejamentoAberto.gabaritoProperty());
        tecidoTubularField.value.unbindBidirectional(planejamentoAberto.tubularProperty());
        planejamentoAberto = null;
        //lists
        beanGradeOf.clear();
        beanAplicacoesOf.clear();
        beanMateriaisOf.clear();
        saldoGrade.clear();
        
        emEdicao.set(false);
        tabRiscos.getTabs().clear();
    }
    
    private void explodirInfosGrade() {
        new Fragment().show(fragment -> {
            fragment.title("Informações Grade Produto");
            fragment.size(1280.0, 800.0);
            FormTableView<VSdCorOf> tblInfosGradeOf = FormTableView.create(VSdCorOf.class, table -> {
                table.expanded();
                table.title("Infos Grade OF");
                table.items.setValue(beanOfCarregada.get().getItensObservable());
                table.columns(
                        FormTableColumn.create(cln -> {
                            cln.title("Código");
                            cln.width(50.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdCorOf, VSdCorOf>, ObservableValue<VSdCorOf>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCor().getCor()));
                            cln.alignment(FormTableColumn.Alignment.CENTER);
                        }) /*Código*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Descrição");
                            cln.width(150.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdCorOf, VSdCorOf>, ObservableValue<VSdCorOf>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getCor().getDescricao()));
                        }) /*Descrição*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Info");
                            cln.width(60.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdCorOf, VSdCorOf>, ObservableValue<VSdCorOf>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                            cln.format(param -> {
                                return new TableCell<VSdCorOf, VSdCorOf>() {
                                    @Override
                                    protected void updateItem(VSdCorOf item, boolean empty) {
                                        super.updateItem(item, empty);
                                        setText(null);
                                        setGraphic(null);
                                        if (item != null && !empty) {
                                            setGraphic(FormBox.create(boxDescHeader -> {
                                                boxDescHeader.vertical();
                                                boxDescHeader.add(FormFieldText.create(field -> {
                                                    field.withoutTitle();
                                                    field.alignment(Pos.CENTER);
                                                    field.editable(false);
                                                    field.addStyle("xs");
                                                    field.value.set("OF");
                                                }).build());
                                                boxDescHeader.add(FormFieldText.create(field -> {
                                                    field.withoutTitle();
                                                    field.alignment(Pos.CENTER);
                                                    field.editable(false);
                                                    field.addStyle("xs").addStyle("success");
                                                    field.value.set("Vend.");
                                                }).build());
                                                boxDescHeader.add(FormFieldText.create(field -> {
                                                    field.withoutTitle();
                                                    field.alignment(Pos.CENTER);
                                                    field.editable(false);
                                                    field.addStyle("xs").addStyle("warning");
                                                    field.value.set("Prod.");
                                                }).build());
                                                boxDescHeader.add(FormFieldText.create(field -> {
                                                    field.withoutTitle();
                                                    field.alignment(Pos.CENTER);
                                                    field.editable(false);
                                                    field.addStyle("xs").addStyle("info");
                                                    field.value.set("Plan.");
                                                }).build());
                                            }));
                                        }
                                    }
                                };
                            });
                        }) /*Info*/
                );
            });
            for (FaixaItem faixa : super.faixaProduto) {
                tblInfosGradeOf.addColumn(FormTableColumn.create(cln -> {
                    cln.title(faixa.getFaixaItemId().getTamanho());
                    cln.width(60.0);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdCorOf, VSdCorOf>, ObservableValue<VSdCorOf>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<VSdCorOf, VSdCorOf>() {
                            @Override
                            protected void updateItem(VSdCorOf item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    VSdCortamOf tam = item.getTams().stream().filter(itemCor -> itemCor.getId().getTam().equals(faixa.getFaixaItemId().getTamanho())).findFirst().get();
                                    setGraphic(FormBox.create(dadosTamanho -> {
                                        dadosTamanho.vertical();
                                        dadosTamanho.add(
                                                FormFieldText.create(field -> {
                                                    field.withoutTitle();
                                                    field.alignment(Pos.CENTER);
                                                    field.editable(false);
                                                    field.addStyle("xs");
                                                    field.value.set(StringUtils.toIntegerFormat(tam.getQtdeof().intValue()));
                                                }).build(),
                                                FormFieldText.create(field -> {
                                                    field.withoutTitle();
                                                    field.alignment(Pos.CENTER);
                                                    field.editable(false);
                                                    field.addStyle("xs").addStyle("success");
                                                    field.value.set(StringUtils.toIntegerFormat(tam.getVendpend().intValue()));
                                                }).build(),
                                                FormFieldText.create(field -> {
                                                    field.withoutTitle();
                                                    field.alignment(Pos.CENTER);
                                                    field.editable(false);
                                                    field.addStyle("xs").addStyle("warning");
                                                    field.value.set(StringUtils.toIntegerFormat(tam.getEmprod().intValue() - tam.getQtdeof().intValue()));
                                                }).build(),
                                                FormFieldText.create(field -> {
                                                    field.withoutTitle();
                                                    field.alignment(Pos.CENTER);
                                                    field.editable(false);
                                                    field.addStyle("xs").addStyle("info");
                                                    field.value.set(StringUtils.toIntegerFormat(tam.getPlanejado().intValue()));
                                                }).build()
                                        );
                                    }));
                                }
                            }
                        };
                    });
                }));
            }
            fragment.box.getChildren().add(tblInfosGradeOf.build());
        });
    }
    
    private void exibirMateriaisAplicacaoOf() {
        new Fragment().show(fragment -> {
            fragment.title("Materias e Aplicações da OF");
            fragment.size(1200.0, 800.0);
            FormTableView<VSdMateriaisCorte> tblMateriais = FormTableView.create(VSdMateriaisCorte.class, table -> {
                table.title("Materiais");
                table.expanded();
                table.items.bind(beanMateriaisOf);
                table.columns(
                        FormTableColumn.create(cln -> {
                            cln.title("Insumo");
                            cln.width(400.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().toString()));
                        }).build() /*Insumo*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Cor");
                            cln.width(180.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCori().toString()));
                        }).build() /*Cor*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Lote");
                            cln.width(85.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLote()));
                            cln.alignment(Pos.CENTER);
                        }).build() /*Lote*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Tonalidade");
                            cln.width(70.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTonalidade()));
                            cln.alignment(Pos.CENTER);
                        }).build() /*Tonalidade*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Partida");
                            cln.width(55.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPartida()));
                            cln.alignment(Pos.CENTER);
                        }).build() /*Part.*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Largura");
                            cln.width(55.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLargura()));
                            cln.alignment(Pos.CENTER);
                        }).build() /*Larg.*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Gramat.");
                            cln.width(55.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getGramatura()));
                            cln.alignment(Pos.CENTER);
                        }).build() /*Gramatura*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Seq.");
                            cln.width(35.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getSequencia()));
                            cln.alignment(Pos.CENTER);
                        }).build() /*Seq.*/,
                        FormTableColumn.create(cln -> {
                            cln.title("UM");
                            cln.width(30.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getUnidade().getUnidade()));
                            cln.alignment(Pos.CENTER);
                        }).build() /*UM*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Consumo");
                            cln.width(60.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getConsumo()));
                            cln.alignment(Pos.CENTER_RIGHT);
                        }).build() /*Consumo*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Reserva");
                            cln.width(60.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getReserva()));
                            cln.alignment(Pos.CENTER_RIGHT);
                        }).build() /*Reserva*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Estoque");
                            cln.width(60.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMateriaisCorte, VSdMateriaisCorte>, ObservableValue<VSdMateriaisCorte>>) param -> new ReadOnlyObjectWrapper(param.getValue().getEstoque()));
                            cln.alignment(Pos.CENTER_RIGHT);
                        }).build() /*Estoque*/
                );
            });
            FormTableView<VSdMatAplicacaoOf> tblAplicacoes = FormTableView.create(VSdMatAplicacaoOf.class, table -> {
                table.title("Aplicações");
                table.expanded();
                table.items.bind(beanAplicacoesOf);
                table.columns(
                        FormTableColumn.create(cln -> {
                            cln.title("Aplicação");
                            cln.width(180.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMatAplicacaoOf, VSdMatAplicacaoOf>, ObservableValue<VSdMatAplicacaoOf>>) param -> new ReadOnlyObjectWrapper(param.getValue().getAplicacaoft().toString()));
                        }).build() /*Aplicação*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Grupo Aplicação");
                            cln.width(140.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMatAplicacaoOf, VSdMatAplicacaoOf>, ObservableValue<VSdMatAplicacaoOf>>) param -> new ReadOnlyObjectWrapper(param.getValue().getAplicacao().toString()));
                        }).build() /*Grp. Aplic.*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Material");
                            cln.width(420.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMatAplicacaoOf, VSdMatAplicacaoOf>, ObservableValue<VSdMatAplicacaoOf>>) param -> new ReadOnlyObjectWrapper(param.getValue().getInsumo().toString()));
                        }).build() /*Material*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Cor");
                            cln.width(200.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMatAplicacaoOf, VSdMatAplicacaoOf>, ObservableValue<VSdMatAplicacaoOf>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCori().toString()));
                        }).build() /*Cor*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Consumo Un.");
                            cln.width(80.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMatAplicacaoOf, VSdMatAplicacaoOf>, ObservableValue<VSdMatAplicacaoOf>>) param -> new ReadOnlyObjectWrapper(param.getValue().getConsumopc()));
                            cln.alignment(Pos.CENTER_RIGHT);
                        }).build() /*Consumo Un.*/,
                        FormTableColumn.create(cln -> {
                            cln.title("Consumo Tot.");
                            cln.width(80.0);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdMatAplicacaoOf, VSdMatAplicacaoOf>, ObservableValue<VSdMatAplicacaoOf>>) param -> new ReadOnlyObjectWrapper(param.getValue().getConsumototal()));
                            cln.alignment(Pos.CENTER_RIGHT);
                        }).build() /*Consumo Tot.*/
                );
            });
            
            fragment.box.getChildren().add(tblMateriais.build());
            fragment.box.getChildren().add(tblAplicacoes.build());
        });
    }
    
    private void cancelarPlano() {
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Deseja realmente excluir este planejamento?");
            message.showAndWait();
        }).value.get())) {
            planejamentoAberto.getRiscos().forEach(it -> {
                it.getCores().forEach(it2 -> {
                    it2.getGrade().forEach(it3 -> new FluentDao().delete(it3));
                    it2.getMateriais().forEach(it3 -> new FluentDao().delete(it3));
                    new FluentDao().delete(it2);
                });
                new FluentDao().delete(it);
            });
            new FluentDao().delete(planejamentoAberto);
            
            SysLogger.addSysDelizLog("Planejamento Encaixe", TipoAcao.EXCLUIR, planejamentoAberto.getNumero().getNumero(), "Excluido planejamento de encaixe ID " + planejamentoAberto.getId());
            cleanFieldsCargaOf();
            MessageBox.create(message -> {
                message.message("Cancelado planejamento!");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
        }
    }
    
    private void finalizarPlano() {
        BigDecimal totalOf = new BigDecimal(planejamentoAberto.getNumero().getItens().stream().mapToInt(it -> it.getQtde().intValue()).sum());
        BigDecimal totalPlanejado = new BigDecimal(planejamentoAberto.getRiscos().stream().mapToInt(it -> it.getCores().stream().mapToInt(it2 -> it2.getTotalRealizado()).sum()).sum());
        
        // validação de quantidade planejada
        if (totalOf.compareTo(totalPlanejado) > 0) {
            if (!((Boolean) QuestionBox.build(Boolean.class, message -> {
                message.message("Faltam peça para serem planejadas.\nTotal OF: " + totalOf.intValue() + "\nTotal Planejado: " + totalPlanejado.intValue() + "\nDeseja mesmo assim finalizar a OF?");
                message.showAndWait();
            }).value.get())) {
                return;
            }
        }
        // validação de materiais alocados
        if (planejamentoAberto.getRiscos().stream().anyMatch(it -> it.getCores().stream().anyMatch(it2 -> it2.getMateriais().size() == 0))) {
            MessageBox.create(message -> {
                message.message("Existem riscos sem materiais selecionados, verifique os risco para definir os materiais consumidos.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }
        
        ObjectProperty<Exception> exceptionRotina = new SimpleObjectProperty<>();
        AtomicBoolean isFinalizado = new AtomicBoolean(false);
        new RunAsyncWithOverlay(this).exec(task -> {
            new FluentDao().merge(planejamentoAberto);
            try {
                /** Início do cálculo de consumo de debrum */
                List<SdMateriaisDebrum> debrumsPlanejamento = new ArrayList<>();
                // Get da lista de debrums e consumos na OF
                List<VSdConsumosOfCorApl> consumosDebrum = (List<VSdConsumosOfCorApl>) new FluentDao().selectFrom(VSdConsumosOfCorApl.class)
                        .where(it -> it
                                .equal("numero", planejamentoAberto.getNumero().getNumero())
                                .equal("aplicacao", "001"))
                        .resultList();
                
                // Faz os testes para saber se é PT
                if (((List<VSdConsumosOfCorApl>) new FluentDao().selectFrom(VSdConsumosOfCorApl.class)
                        .where(eb -> eb
                                .equal("numero", planejamentoAberto.getNumero().getNumero())
                                .equal("aplicacao", "717")
                                .equal("aplicpcp.codigo","520"))
                        .resultList()).size() > 0 ||
                        consumosDebrum.stream().allMatch(consumo -> consumo.getCori().getCor().equals("PT"))) {
                    List<VSdConsumosOfCorApl> consumosPtDebrum = new ArrayList<>();
                    Object[] aplicacoes = consumosDebrum.stream().map(VSdConsumosOfCorApl::getAplicpcp).distinct().toArray();
                    for (Object aplicacoe : aplicacoes) {
                        consumosDebrum.stream()
                                .filter(consumo -> consumo.getAplicpcp().getCodigo().equals(((Pcpapl) aplicacoe).getCodigo()))
                                .findFirst()
                                .ifPresent(consumo -> consumosPtDebrum.add(consumo));
                    }
                    consumosDebrum = consumosPtDebrum;
                }
                
                // Inicia a verificação dos consumos de debrum
                consumosDebrum.forEach(debrum -> {
                    // get dos riscos de corpo para a cor do debrum
                    List<SdMateriaisRiscoCor> materiaisRiscosCor = new ArrayList<>();
                    planejamentoAberto.getRiscos().stream().filter(it -> it.getTiporisco().getCodigo() == 1).forEach(risco -> {
//                        risco.getCores().stream().filter(it -> Arrays.asList(it.getCor().replace("'", "").split(",")).contains(debrum.getCor())).forEach(cor -> {
                        risco.getCores().stream().forEach(cor -> {
                            cor.getMateriais().stream().filter(it -> Boolean.logicalAnd(it.getMaterial().equals(debrum.getInsumo().getCodigo()), it.getCori().equals(debrum.getCori().getCor()))).forEach(material -> {
                                materiaisRiscosCor.add(material);
                            });
                        });
                    });
                    // caso não existe riscos de corpo com a cor do debrum, verifica se existem detalhes ou ribana com a cor
                    if (materiaisRiscosCor.size() == 0) {
                        planejamentoAberto.getRiscos().stream().filter(it -> Arrays.asList(3, 5).contains(it.getTiporisco().getCodigo())).forEach(risco -> {
                            risco.getCores().stream().forEach(corDetalhe -> {
                                List<SdCorRisco> coresDetalhe = getCorIds(corDetalhe.getCor());
                                if (coresDetalhe.stream().map(SdCorRisco::getCor).anyMatch(cor -> cor.equals(debrum.getCor()))) {
                                    corDetalhe.getMateriais().stream()
                                            .filter(it -> Boolean.logicalAnd(it.getMaterial().equals(debrum.getInsumo().getCodigo()), it.getCori().equals(debrum.getCori().getCor())))
                                            .forEach(material -> materiaisRiscosCor.add(material));
                                }
//                                coresDetalhe.stream().filter(it -> it.getCor().equals(debrum.getCor())).forEach(cor -> {
//                                    cor.getMateriais().stream().filter(it -> Boolean.logicalAnd(it.getMaterial().equals(debrum.getInsumo().getCodigo()), it.getCori().equals(debrum.getCori().getCor()))).forEach(material -> {
//                                        ;
//                                    });
//                                });
                            });
                        });
                    }
                    
                    // verificação se encontrou algum material planejado para o material/cor do debrum
                    if (materiaisRiscosCor.size() == 0) {
                        // caso não encontrou nenhum rolo alocado nos riscos
                        // calculo do total de debrum que será consumido
                        Integer qtdePlanejadoCorProduto = planejamentoAberto.getRiscos().stream()
                                .filter(it -> it.getTiporisco().getCodigo() == 1)
                                .mapToInt(it -> it.getCores().stream()
                                        .filter(it2 -> Arrays.asList(it2.getCor().replace("'", "").split(",")).contains(debrum.getCor()))
                                        .mapToInt(SdCorRisco::getTotalRealizado)
                                        .sum())
                                .sum();
                        BigDecimal totalConsumoDebrum = new BigDecimal(qtdePlanejadoCorProduto).multiply(debrum.getConsumo());
                        // get materiais reservados
                        List<VSdMateriaisCorte> reservasMaterialDebrum = (List<VSdMateriaisCorte>) new FluentDao().selectFrom(VSdMateriaisCorte.class)
                                .where(it -> it
                                        .equal("numero", debrum.getNumero())
                                        .equal("codigo.codigo", debrum.getInsumo().getCodigo())
                                        .equal("cori.cor", debrum.getCori().getCor()))
                                .resultList();
                        // get das quantidades disponíveis dos rolos
                        reservasMaterialDebrum.forEach(rolo -> {
                            BigDecimal consumoRiscos = new BigDecimal(planejamentoAberto.getRiscos().stream()
                                    .mapToDouble(it -> it.getCores().stream()
                                            .mapToDouble(it2 -> it2.getMateriais().stream()
                                                    .filter(it3 -> it3.equals(rolo))
                                                    .mapToDouble(it3 -> it3.getConsumido().doubleValue() /
                                                            (rolo.getCodigo().getUnidade().getUnidade().equals("KG") ? (1000.0 / (it3.getGramatura().doubleValue() * it3.getLargura().doubleValue())) : 1))
                                                    .sum())
                                            .sum())
                                    .sum());
                            BigDecimal consumoDebrums = new BigDecimal(debrumsPlanejamento.stream()
                                    .filter(it -> it.getMaterial().equals(rolo.getCodigo().getCodigo())
                                            && it.getCori().equals(rolo.getCori().getCor())
                                            && it.getLote().equals(rolo.getLote())
                                            && it.getTonalidade().equals(rolo.getTonalidade())
                                            && it.getPartida().equals(rolo.getPartida())
                                            && it.getSequencia().equals(rolo.getSequencia()))
                                    .mapToDouble(it -> it.getConsumido().doubleValue())
                                    .sum());
                            rolo.setResmtConsumir(rolo.getReserva().subtract(consumoRiscos).subtract(consumoDebrums));
                        });
                        reservasMaterialDebrum.sort(Comparator.comparingDouble(value -> ((VSdMateriaisCorte) value).getResmtConsumir().doubleValue()).reversed());
                        for (VSdMateriaisCorte rolo : reservasMaterialDebrum) {
                            BigDecimal qtdeAConsumir = rolo.getResmtConsumir().compareTo(totalConsumoDebrum) <= 0 ? rolo.getResmtConsumir() : totalConsumoDebrum;
                            SdMateriaisDebrum materialDebrum = new SdMateriaisDebrum();
                            materialDebrum.setIdplano(planejamentoAberto.getId());
                            materialDebrum.setAplicacao(debrum.getAplicpcp().getCodigo());
                            materialDebrum.setCor(debrum.getCor());
                            materialDebrum.setMaterial(rolo.getCodigo().getCodigo());
                            materialDebrum.setCori(rolo.getCori().getCor());
                            materialDebrum.setLote(rolo.getLote());
                            materialDebrum.setTonalidade(rolo.getTonalidade());
                            materialDebrum.setPartida(rolo.getPartida());
                            materialDebrum.setSequencia(rolo.getSequencia());
                            materialDebrum.setLargura(rolo.getLargura());
                            materialDebrum.setGramatura(rolo.getGramatura());
                            materialDebrum.setConsumopc(debrum.getConsumo());
                            materialDebrum.setConsumido(qtdeAConsumir.setScale(4, RoundingMode.HALF_UP));
                            materialDebrum.setQtdepecas(qtdePlanejadoCorProduto);
                            debrumsPlanejamento.add(materialDebrum);
                            totalConsumoDebrum = totalConsumoDebrum.subtract(qtdeAConsumir);
                            if (totalConsumoDebrum.compareTo(BigDecimal.ZERO) <= 0)
                                break;
                        }
                    } else {
                        // caso encontrou rolos alocado nos riscos
                        for (SdMateriaisRiscoCor rolo : materiaisRiscosCor) {
                            BigDecimal qtdeAConsumir = debrum.getConsumo().multiply(new BigDecimal(rolo.getQtdepecas()));
                            SdMateriaisDebrum materialDebrum = new SdMateriaisDebrum();
                            materialDebrum.setIdplano(planejamentoAberto.getId());
                            materialDebrum.setIdrisco(rolo.getIdrisco());
                            materialDebrum.setAplicacao(debrum.getAplicpcp().getCodigo());
                            materialDebrum.setCor(debrum.getCor());
                            materialDebrum.setMaterial(rolo.getMaterial());
                            materialDebrum.setCori(rolo.getCori());
                            materialDebrum.setLote(rolo.getLote());
                            materialDebrum.setTonalidade(rolo.getTonalidade());
                            materialDebrum.setPartida(rolo.getPartida());
                            materialDebrum.setSequencia(rolo.getSequencia());
                            materialDebrum.setLargura(rolo.getLargura());
                            materialDebrum.setGramatura(rolo.getGramatura());
                            materialDebrum.setConsumopc(debrum.getConsumo());
                            materialDebrum.setConsumido(qtdeAConsumir);
                            materialDebrum.setQtdepecas(rolo.getQtdepecas());
                            debrumsPlanejamento.add(materialDebrum);
                        }
                    }
                    
                });
                
                new FluentDao().runNativeQueryUpdate(String.format("delete from sd_materiais_debrum_001 where id_plano = %d", planejamentoAberto.getId()));
                for (SdMateriaisDebrum debrum : debrumsPlanejamento) {
                    new FluentDao().persist(debrum);
                }
            } catch (SQLException e) {
                e.printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(e);
                    message.showAndWait();
                });
            }
            
            try {
                if (!planejamentoAberto.isFinalizado()) {
                    new ReportUtils().config()
                            .addReport(ReportUtils.ReportFile.PLANEJAMENTO_ENCAIXE,
                                    new ReportUtils.ParameterReport("id_planejamento", planejamentoAberto.getId()))
                            .view().print();
                    new ReportUtils().config()
                            .addReport(ReportUtils.ReportFile.ROMANEIO_MATERIAIS_ENCAIXE,
                                    new ReportUtils.ParameterReport("id_planejamento", String.valueOf(planejamentoAberto.getId())),
                                    new ReportUtils.ParameterReport("numero", planejamentoAberto.getNumero().getNumero()))
                            .view().print();
                    new ReportUtils().config()
                            .addReport(ReportUtils.ReportFile.PLANEJAMENTO_ENCAIXE_CONFERENCIA,
                                    new ReportUtils.ParameterReport("id_planejamento", planejamentoAberto.getId()))
                            .view().print();
                    
                    planejamentoAberto.setFinalizado(true);
                    new FluentDao().merge(planejamentoAberto);
                } else {
                    isFinalizado.set(true);
                }
            } catch (JRException | SQLException | IOException e) {
                e.printStackTrace();
                exceptionRotina.set(e);
                return ReturnAsync.EXCEPTION.value;
            }
            
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                if (isFinalizado.get())
                    new Fragment().show(fragment -> {
                        fragment.title("Imprimir Planejamento");
                        fragment.size(350.0, 200.0);
                        final FormFieldToggleSingle printPlano = FormFieldToggleSingle.create(field -> {
                            field.title("Imprimir Planejamento");
                            field.value.set(true);
                        });
                        final FormFieldToggleSingle printMateriais = FormFieldToggleSingle.create(field -> {
                            field.title("Imprimir Lista de Materiais");
                            field.value.set(true);
                        });
                        final FormFieldToggleSingle printConferencia = FormFieldToggleSingle.create(field -> {
                            field.title("Imprimir Folha de Conferência");
                            field.value.set(true);
                        });
                        
                        fragment.box.getChildren().addAll(printPlano.build(), printMateriais.build(), printConferencia.build());
                        fragment.buttonsBox.getChildren().add(FormButton.create(btnImprimir -> {
                            btnImprimir.title("Imprimir");
                            btnImprimir.icon(ImageUtils.getIcon(ImageUtils.Icon.IMPRIMIR, ImageUtils.IconSize._24));
                            btnImprimir.addStyle("success");
                            btnImprimir.setAction(evtPrint -> {
                                /**
                                 * task: print * return:
                                 */
                                ObjectProperty<Exception> excPrint = new SimpleObjectProperty<>();
                                new RunAsyncWithOverlay(this).exec(task -> {
                                    try {
                                        if (printPlano.value.get())
                                            new ReportUtils().config()
                                                    .addReport(ReportUtils.ReportFile.PLANEJAMENTO_ENCAIXE,
                                                            new ReportUtils.ParameterReport("id_planejamento", planejamentoAberto.getId()))
                                                    .view().print();
                                        if (printMateriais.value.get())
                                            new ReportUtils().config()
                                                    .addReport(ReportUtils.ReportFile.ROMANEIO_MATERIAIS_ENCAIXE,
                                                            new ReportUtils.ParameterReport("id_planejamento", String.valueOf(planejamentoAberto.getId())),
                                                            new ReportUtils.ParameterReport("numero", planejamentoAberto.getNumero().getNumero()))
                                                    .view().print();
                                        if (printConferencia.value.get())
                                            new ReportUtils().config()
                                                    .addReport(ReportUtils.ReportFile.PLANEJAMENTO_ENCAIXE_CONFERENCIA,
                                                            new ReportUtils.ParameterReport("id_planejamento", planejamentoAberto.getId()))
                                                    .view().print();
                                    } catch (JRException | SQLException | IOException e) {
                                        e.printStackTrace();
                                        excPrint.set(e);
                                        return ReturnAsync.EXCEPTION.value;
                                    }
                                    return ReturnAsync.OK.value;
                                }).addTaskEndNotification(taskSubReturn -> {
                                    if (taskSubReturn.equals(ReturnAsync.EXCEPTION.value)) {
                                        ExceptionBox.build(box -> {
                                            box.exception(excPrint.get());
                                            box.showAndWait();
                                        });
                                    } else if (taskSubReturn.equals(ReturnAsync.OK.value)) {
                                        fragment.close();
                                    }
                                });
                            });
                        }));
                    });
                
                SysLogger.addSysDelizLog("Planejamento Encaixe", TipoAcao.EDITAR, planejamentoAberto.getNumero().getNumero(), "Salvando e imprimindo planejamento de encaixe ID " + planejamentoAberto.getId());
                
                cleanFieldsCargaOf();
                MessageBox.create(message -> {
                    message.message("Planjemaneto salvo com sucesso.");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            } else if (taskReturn.equals(ReturnAsync.EXCEPTION.value)) {
                ExceptionBox.build(message -> {
                    message.exception(exceptionRotina.get());
                    message.showAndWait();
                });
            }
        });
        
    }
}