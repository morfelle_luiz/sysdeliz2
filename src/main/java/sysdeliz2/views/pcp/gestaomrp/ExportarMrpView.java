package sysdeliz2.views.pcp.gestaomrp;

import javafx.geometry.Pos;
import javafx.stage.FileChooser;
import sysdeliz2.controllers.views.pcp.gestaomrp.ExportarMrpController;
import sysdeliz2.models.sysdeliz.SdMrpProduto;
import sysdeliz2.utils.GUIUtils;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.gui.components.FormBox;
import sysdeliz2.utils.gui.components.FormButton;
import sysdeliz2.utils.gui.components.FormFieldSegmentedButton;
import sysdeliz2.utils.gui.components.FormFieldText;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.sys.ExportUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

public class ExportarMrpView extends ExportarMrpController {
    
    // <editor-fold defaultstate="collapsed" desc="Fields">
    private final FormFieldSegmentedButton<String> paisMrpField = FormFieldSegmentedButton.create(field -> {
        field.title("País");
        field.options(
                field.option("Brasil", "BR", FormFieldSegmentedButton.Style.SUCCESS),
                field.option("Paraguai", "PY", FormFieldSegmentedButton.Style.PRIMARY)
        );
        field.select(0);
    });
    private final FormFieldSegmentedButton<String> comResumoField = FormFieldSegmentedButton.create(field -> {
        field.title("Com Resumo");
        field.options(
                field.option("Sim", "S", FormFieldSegmentedButton.Style.SUCCESS),
                field.option("Não", "N", FormFieldSegmentedButton.Style.DANGER)
        );
        field.select(1);
    });
    private final FormFieldText dataUltimaRodadaField = FormFieldText.create(field -> {
        field.title("Data Última Rodada MRP");
        field.editable(false);
        field.value.bind(super.dataUltimoLog);
    });
    private final FormFieldText infoUltimaRodadaField = FormFieldText.create(field -> {
        field.title("Info. Última Rodada MRP");
        field.editable(false);
        field.value.bind(super.infoUltimoLog);
    });
    private final FormFieldText inicioUltimaRodadaField = FormFieldText.create(field -> {
        field.title("Início Última Rodada MRP");
        field.editable(false);
        field.value.bind(super.inicioUltimoLog);
    });
    private final FormFieldText tempoUltimaRodadaField = FormFieldText.create(field -> {
        field.title("Tempo Última Rodada MRP");
        field.editable(false);
        field.value.bind(super.tempoUltimoLog);
    });
    private final FormFieldText estadoJobField = FormFieldText.create(field -> {
        field.title("Estado MRP");
        field.editable(false);
        field.value.bind(super.statusJobLog);
        field.value.addListener((observable, oldValue, newValue) -> {
            if (newValue != null){
                if (newValue.equals("EM EXECUÇÃO")){
                    field.addStyle("danger");
                } else {
                    field.removeStyle("danger");
                }
            }
        });
    });
    private final FormFieldText dataProximaRodadaField = FormFieldText.create(field -> {
        field.title("Próxima Rodada MRP");
        field.editable(false);
        field.value.bind(super.dataProximoLog);
    });
    private final FormFieldText inicioUltimaRodadaProducaoField = FormFieldText.create(field -> {
        field.title("Início Última Rodada MRP");
        field.editable(false);
        field.value.bind(super.inicioUltimoProducaoLog);
    });
    private final FormFieldText tempoUltimaRodadaProducaoField = FormFieldText.create(field -> {
        field.title("Tempo Última Rodada MRP");
        field.editable(false);
        field.value.bind(super.tempoUltimoProducaoLog);
    });
    private final FormFieldText estadoJobProducaoField = FormFieldText.create(field -> {
        field.title("Estado MRP");
        field.editable(false);
        field.value.bind(super.statusJobProducaoLog);
        field.value.addListener((observable, oldValue, newValue) -> {
            if (newValue != null){
                if (newValue.equals("EM EXECUÇÃO")){
                    field.addStyle("danger");
                } else {
                    field.removeStyle("danger");
                }
            }
        });
    });
    private final FormFieldText dataProximaRodadaProducaoField = FormFieldText.create(field -> {
        field.title("Próxima Rodada MRP");
        field.editable(false);
        field.value.bind(super.dataProximoProducaoLog);
    });
    // </editor-fold>
    
    public ExportarMrpView() {
        super("Exportação de Dados MRP", ImageUtils.getImage(ImageUtils.Icon.GESTAO_MATERIAIS));
        init();
    }
    
    private void init() {
        getLogJob();
        try {
            getStatusJob();
            getStatusJobProducao();
        } catch (SQLException e) {
            e.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(e);
                message.showAndWait();
            });
        }
        super.box.getChildren().add(FormBox.create(boxPrincipal -> {
            boxPrincipal.horizontal();
            boxPrincipal.add(FormBox.create(boxExportacao -> {
                boxExportacao.vertical();
                boxExportacao.title("Exportação Excel");
                boxExportacao.add(FormBox.create(lo1 -> {
                    lo1.horizontal();
                    lo1.add(dataUltimaRodadaField.build());
                    lo1.add(infoUltimaRodadaField.build());
                }));
                boxExportacao.add(FormBox.create(boxMrpMateriais -> {
                    boxMrpMateriais.vertical();
                    boxMrpMateriais.title("MRP Materiais");
                    boxMrpMateriais.add(paisMrpField.build());
                    boxMrpMateriais.add(comResumoField.build());
                    boxMrpMateriais.add(FormButton.create(btn -> {
                        btn.title("Exportar MRP");
                        btn.addStyle("lg").addStyle("success");
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EXCEL, ImageUtils.IconSize._32));
                        btn.setAction(event -> {
                            exportExcelMrp();
                        });
                    }));
                }));
                boxExportacao.add(FormBox.create(boxMrpMateriais -> {
                    boxMrpMateriais.vertical();
                    boxMrpMateriais.title("MRP Produção");
                    boxMrpMateriais.add(FormButton.create(btn -> {
                        btn.title("Exportar MRP");
                        btn.addStyle("lg").addStyle("success");
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EXCEL, ImageUtils.IconSize._32));
                        btn.setAction(event -> {
                            exportExcelMrpProducao();
                        });
                    }));
                }));
            }));
            boxPrincipal.add(FormBox.create(boxJob -> {
                boxJob.vertical();
                boxJob.title("Job MRP Materiais");
                boxJob.add(estadoJobField.build());
                boxJob.add(inicioUltimaRodadaField.build());
                boxJob.add(tempoUltimaRodadaField.build());
                boxJob.add(dataProximaRodadaField.build());
                boxJob.add(FormBox.create(lo1 -> {
                    lo1.horizontal();
                    lo1.add(FormButton.create(btn -> {
                        btn.title("Rodar MRP");
                        btn.addStyle("lg").addStyle("success");
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._32));
                        btn.setAction(event -> {
                            try {
                                runJob();
                            } catch (SQLException e) {
                                e.printStackTrace();
                                ExceptionBox.build(message -> {
                                    message.exception(e);
                                    message.showAndWait();
                                });
                            }
                        });
                    }));
                    lo1.add(FormButton.create(btn -> {
                        btn.title("Atualizar status MRP");
                        btn.addStyle("sm").addStyle("primary");
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ATUALIZAR, ImageUtils.IconSize._24));
                        btn.setAction(event -> {
                            try {
                                getStatusJob();
                            } catch (SQLException e) {
                                e.printStackTrace();
                                ExceptionBox.build(message -> {
                                    message.exception(e);
                                    message.showAndWait();
                                });
                            }
                        });
                    }));
                }));
            }));
            boxPrincipal.add(FormBox.create(boxJob -> {
                boxJob.vertical();
                boxJob.title("Job MRP Produção");
                boxJob.add(estadoJobProducaoField.build());
                boxJob.add(inicioUltimaRodadaProducaoField.build());
                boxJob.add(tempoUltimaRodadaProducaoField.build());
                boxJob.add(dataProximaRodadaProducaoField.build());
                boxJob.add(FormBox.create(lo1 -> {
                    lo1.horizontal();
                    lo1.add(FormButton.create(btn -> {
                        btn.title("Rodar MRP");
                        btn.addStyle("lg").addStyle("success");
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._32));
                        btn.setAction(event -> {
                            try {
                                runJobProducao();
                            } catch (SQLException e) {
                                e.printStackTrace();
                                ExceptionBox.build(message -> {
                                    message.exception(e);
                                    message.showAndWait();
                                });
                            }
                        });
                    }));
                    lo1.add(FormButton.create(btn -> {
                        btn.title("Atualizar status MRP");
                        btn.addStyle("sm").addStyle("primary");
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ATUALIZAR, ImageUtils.IconSize._24));
                        btn.setAction(event -> {
                            try {
                                getStatusJobProducao();
                            } catch (SQLException e) {
                                e.printStackTrace();
                                ExceptionBox.build(message -> {
                                    message.exception(e);
                                    message.showAndWait();
                                });
                            }
                        });
                    }));
                }));
            }));
        }));
    }
    
    private void exportExcelMrp() {
        getLogJob();
        try {
            getStatusJob();
        } catch (SQLException e) {
            e.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(e);
                message.showAndWait();
            });
        }
        String filePath = GUIUtils.showSaveFileDialog(new ArrayList<>(Collections.singleton(new FileChooser.ExtensionFilter("Arquivos do Excel (.xlsx)", "*.xlsx"))));
        if (filePath != null)
            new RunAsyncWithOverlay(this).addTaskEndNotification(taskReturn -> {
                if (taskReturn.equals(ReturnAsync.OK.value)) {
                    MessageBox.create(message -> {
                        message.message("Expotação de dados concluída com sucesso.");
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.position(Pos.TOP_RIGHT);
                        message.notification();
                    });
                }
            }).exec(task -> {
                getMrp(paisMrpField.value.get());
                criarExcelOrigemTab(filePath, comResumoField.value.get(), paisMrpField.value.get());
                return ReturnAsync.OK.value;
            });
    }
    
    private void exportExcelMrpProducao() {
        try {
            getStatusJobProducao();
        } catch (SQLException e) {
            e.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(e);
                message.showAndWait();
            });
        }
        String filePath = GUIUtils.showSaveFileDialog(new ArrayList<>(Collections.singleton(new FileChooser.ExtensionFilter("Arquivos do Excel (.xlsx)", "*.xlsx"))));
        if (filePath != null)
            new RunAsyncWithOverlay(this).addTaskEndNotification(taskReturn -> {
                if (taskReturn.equals(ReturnAsync.OK.value)) {
                    MessageBox.create(message -> {
                        message.message("Expotação de dados concluída com sucesso.");
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.position(Pos.TOP_RIGHT);
                        message.notification();
                    });
                }
            }).exec(task -> {
                getMrpProducao();
                try {
                    new ExportUtils().excel(filePath)
                            .<SdMrpProduto>addSheet("ORIGEM", SdMrpProduto.class, mrpProducao)
                            .export();
                } catch (IOException | IllegalAccessException e) {
                    e.printStackTrace();
                    ExceptionBox.build(message -> {
                        message.exception(e);
                        message.showAndWait();
                    });
                }
                return ReturnAsync.OK.value;
            });
    }
}
