package sysdeliz2.views.pcp.gestaomrp;

import javafx.scene.layout.VBox;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.gui.window.base.WindowBase;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.views.pcp.gestaomrp.mrptinturaria.*;

public class MrpTinturariaTecelagemView extends WindowBase {

    private MrpTinturariaJobTab viewTabJob = new MrpTinturariaJobTab(this);
    private MrpTinturariaFioTab viewTabFio = new MrpTinturariaFioTab(this);
    private MrpTinturariaTecelagemTab viewTabTecelagem = new MrpTinturariaTecelagemTab(this);
    private MrpTinturariaTinturariaTab viewTabTinturaria = new MrpTinturariaTinturariaTab(this);
    private MrpTinturariaKanbanTab viewTabKanban = new MrpTinturariaKanbanTab(this);
    private MrpTinturariaExplosaoTab viewTabExplosao = new MrpTinturariaExplosaoTab(this);

    public MrpTinturariaTecelagemView() {
        super("Demandas Tinturaria/Tecelagem",
                ImageUtils.getImage(ImageUtils.Icon.LAVANDERIA),
                new String[]{"Job","Fio","Tecelagem","Tinturaria","Kanban","Explosão"});
        init();
    }
    
    private void init() {
        new RunAsyncWithOverlay(this).exec(task -> {
            viewTabFio.loadLotes();
            viewTabTecelagem.loadLotes();
            viewTabTinturaria.loadLotes();
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                viewTabFio.initLotes();
                viewTabTecelagem.initLotes();
                viewTabTinturaria.initLotes();

                initJobTab();
                initFioTab();
                initTecelagemTab();
                initTinturariaTab();
                initKanbanTab();
                initExplosaoTab();
            }
        });
    }

    // init das abas com os componentes de cada uma
    private void initJobTab() {
        ((VBox) super.tabs.getTabs().get(0).getContent()).getChildren().add(viewTabJob.init());
    }
    private void initFioTab() {
        ((VBox) super.tabs.getTabs().get(1).getContent()).getChildren().add(viewTabFio.init());
    }
    private void initTecelagemTab() {
        ((VBox) super.tabs.getTabs().get(2).getContent()).getChildren().add(viewTabTecelagem.init());
    }
    private void initTinturariaTab() {
        ((VBox) super.tabs.getTabs().get(3).getContent()).getChildren().add(viewTabTinturaria.init());
    }
    private void initKanbanTab() {
        ((VBox) super.tabs.getTabs().get(4).getContent()).getChildren().add(viewTabKanban.init());
    }
    private void initExplosaoTab() {
        ((VBox) super.tabs.getTabs().get(5).getContent()).getChildren().add(viewTabExplosao.init());
    }
    
    @Override
    public void closeWindow() {
    
    }
}