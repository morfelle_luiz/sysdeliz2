package sysdeliz2.views.pcp.gestaomrp.mrptinturaria;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import org.controlsfx.control.SegmentedBar;
import sysdeliz2.controllers.views.pcp.gestaomrp.MrpTinturariaTecelagemController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.sysdeliz.pcp.SdBarcaFornecedor;
import sysdeliz2.models.sysdeliz.pcp.SdBarcaProducao;
import sysdeliz2.models.sysdeliz.pcp.SdItensBarca;
import sysdeliz2.models.ti.Cor;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.DateUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.views.pcp.gestaomrp.MrpTinturariaTecelagemView;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class MrpTinturariaExplosaoTab extends MrpTinturariaTecelagemController {

    private MrpTinturariaTecelagemView view = null;

    private final FormFieldSegmentedButton<String> fieldStatusBarca = FormFieldSegmentedButton.create(field -> {
        field.title("Status Barcas");
        field.options(
                field.option("Todas", "T", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("Pendentes", "F", FormFieldSegmentedButton.Style.WARNING),
                field.option("Firmadas", "A", FormFieldSegmentedButton.Style.SUCCESS)
        );
        field.select(0);
    });
    private final FormBox boxGantt = FormBox.create(gantt -> {
        gantt.horizontal();
        gantt.expanded();
        gantt.horizontalScroll();
    });
    private List<SdBarcaProducao> barcas = new ArrayList<>();

    public MrpTinturariaExplosaoTab(MrpTinturariaTecelagemView view) {
        this.view = view;
    }

    private void carregarGantt() {
        boxGantt.clear();
        barcas = (List<SdBarcaProducao>) new FluentDao().selectFrom(SdBarcaProducao.class)
                .where(eb -> eb
                        .isIn("status", new String[]{"F", "A"}, TipoExpressao.AND, b -> fieldStatusBarca.value.get().equals("T"))
                        .equal("status", "F", TipoExpressao.AND, b -> fieldStatusBarca.value.get().equals("F"))
                        .equal("status", "A", TipoExpressao.AND, b -> fieldStatusBarca.value.get().equals("A")))
                .resultList();

        barcas.forEach(barca -> {
//            JPAUtils.getEntityManager().refresh(barca);
            barca.getItens().forEach(item -> {
                JPAUtils.getEntityManager().refresh(item);
            });
        });

        LocalDate inicioPeriodo = barcas.stream()
                .filter(barca -> barca.getDtenvio() != null)
                .map(SdBarcaProducao::getDtenvio)
                .min((o1, o2) -> o1.compareTo(o2))
                .orElse(LocalDate.now());
        LocalDate fimPeriodo = barcas.stream()
                .filter(barca -> barca.getDtretorno() != null)
                .map(SdBarcaProducao::getDtretorno)
                .max((o1, o2) -> o1.compareTo(o2))
                .orElse(LocalDate.now());

        Long qtdeDiasPeriodo = ChronoUnit.DAYS.between(inicioPeriodo, fimPeriodo) + 1;

        Map<SdBarcaFornecedor, List<SdBarcaProducao>> maquinasFornecedor = barcas.stream()
                .collect(Collectors.groupingBy(SdBarcaProducao::getMaquina));
        maquinasFornecedor.forEach((maquina, barcasMaquina) -> {
            barcasMaquina.stream()
                    .filter(barca -> barca.getStatus().matches("(F|A)"))
                    .sorted((o1, o2) -> o1.getDtenvio().compareTo(o2.getDtenvio()));
            barcasMaquina.stream()
                    .filter(barca -> barca.getStatus().matches("(F|A)"))
                    .forEach(barca -> {
                        List<SdBarcaProducao> barcasConcorrente = barcasMaquina.stream()
                                .filter(concorrente -> !concorrente.equals(barca) && concorrente.getStatus().matches("(F|A)")
                                        && DateUtils.between(barca.getDtenvio(), concorrente.getDtenvio(), concorrente.getDtretorno()))
                                .collect(Collectors.toList());
                        if (barcasConcorrente.size() == 0) {
                            barca.setAndar(1);
                        } else {
                            AtomicInteger andar = new AtomicInteger(0);
                            do {
                                andar.incrementAndGet();
                            } while (barcasConcorrente.stream()
                                    .mapToInt(SdBarcaProducao::getAndar)
                                    .anyMatch(value -> andar.get() == value));
                            barca.setAndar(andar.get());
                        }
                    });
        });

        boxGantt.add(FormBox.create(ganttContainer -> {
            ganttContainer.vertical();
            ganttContainer.width(200 + (qtdeDiasPeriodo * 40));
            ganttContainer.add(FormBox.create(header -> {
                header.horizontal();
                header.addStyle("secundary");
                header.add(FormBox.create(dadosFornecedor -> {
                    dadosFornecedor.horizontal();
                    dadosFornecedor.width(200.0);
                    dadosFornecedor.add(FormLabel.create(label -> {
                        label.value.set("Período");
                        label.fontColor("#FFF");
                        label.boldText();
                    }).build());
                    dadosFornecedor.alignment(Pos.CENTER_RIGHT);
                }));
                for (int i = 0; i < qtdeDiasPeriodo; i++) {
                    int finalI = i;
                    header.add(FormBox.create(boxDia -> {
                        boxDia.horizontal();
                        boxDia.width(35.0);
                        boxDia.add(FormLabel.create(label -> {
                            label.value.set(inicioPeriodo.plusDays(finalI).format(DateTimeFormatter.ofPattern("dd/MM"))
                                    + "\n " + inicioPeriodo.plusDays(finalI).format(DateTimeFormatter.ofPattern("yyyy")));
                            label.sizeText(10);
                            label.alignment(Pos.CENTER);
                            label.fontColor("#FFF");
                        }).build());
                    }));
                }
            }));
            ganttContainer.add(new Separator(Orientation.HORIZONTAL));
            ganttContainer.add(FormBox.create(ganttContent -> {
                ganttContent.vertical();
                ganttContent.expanded();
                ganttContent.verticalScroll();
                maquinasFornecedor.forEach((maquina, barcasFornecedor) -> {
                    ganttContent.add(FormBox.create(kanbanMaquina -> {
                        kanbanMaquina.horizontal();
                        kanbanMaquina.height(160.0);
                        //kanbanMaquina.verticalScroll();
                        kanbanMaquina.add(FormBox.create(dadosMaquina -> {
                            dadosMaquina.vertical();
                            dadosMaquina.width(200.0);
                            dadosMaquina.add(FormBox.create(boxNomeMaquina -> {
                                boxNomeMaquina.horizontal();
                                boxNomeMaquina.addStyle("info");
                                boxNomeMaquina.add(FormLabel.create(label -> {
                                    label.value.set(maquina.getFornecedor().toString());
                                    label.width(190.0);
                                    label.fontColor("#000");
                                }).build());
                            }));
                            dadosMaquina.add(FormBox.create(boxNomeMaquina -> {
                                boxNomeMaquina.horizontal();
                                boxNomeMaquina.addStyle("primary");
                                boxNomeMaquina.add(FormLabel.create(label -> {
                                    label.value.set(maquina.getMaquina());
                                    label.boldText();
                                    label.width(190.0);
                                    label.fontColor("#FFF");
                                }).build());
                            }));
                        }));
                        kanbanMaquina.add(FormBox.create(boxBarcas -> {
                            boxBarcas.vertical();
                            boxBarcas.expanded();
                            boxBarcas.verticalScroll();
                            barcasFornecedor.stream()
                                    .filter(barca -> !barca.getStatus().matches("P"))
                                    .sorted(Comparator.comparing(SdBarcaProducao::getDtenvio))
                                    .mapToInt(SdBarcaProducao::getAndar)
                                    .distinct()
                                    .sorted()
                                    .forEach(andar -> {
                                        Pane paneBarcas = new Pane();
                                        barcasFornecedor.stream()
                                                .filter(barca -> barca.getStatus().matches("(A|F)")
                                                        && barca.getAndar() == andar)
                                                .forEach(barca -> {
                                                    paneBarcas.getChildren().add(FormBox.create(boxBarca -> {
                                                        boxBarca.horizontal();
                                                        Long diasBarca = ChronoUnit.DAYS.between(barca.getDtenvio(), barca.getDtretorno()) + 1;
                                                        boxBarca.width(diasBarca * 40);
                                                        boxBarca.height(32.0);
                                                        boxBarca.alignment(Pos.CENTER_LEFT);
                                                        boxBarca.setLayoutX(ChronoUnit.DAYS.between(inicioPeriodo, barca.getDtenvio()) * 40);
                                                        boxBarca.add(FormBox.create(boxDadosBarca -> {
                                                            boxDadosBarca.horizontal();
                                                            boxDadosBarca.expanded();
                                                            boxDadosBarca.alignment(Pos.CENTER_LEFT);
                                                            boxDadosBarca.border();
                                                            Map<String, Long> attEstoque = barca.getItens().stream()
                                                                    .sorted((o1, o2) -> o1.getFirmadoEm() == null ? -1
                                                                            : o2.getFirmadoEm() == null ? 1
                                                                            : o2.getFirmadoEm().compareTo(o1.getFirmadoEm()))
                                                                    .collect(Collectors
                                                                            .groupingBy(it -> it.getFirmadoEm() == null ? "X" : it.getFirmadoEm(),
                                                                                    Collectors.counting()));
                                                            List<String> sortFirmados = new ArrayList<>(attEstoque.keySet());
                                                            sortFirmados.sort(Comparator.reverseOrder());

                                                            SegmentedBar<TypeSegment> bar = new SegmentedBar();
                                                            bar.setOrientation(Orientation.HORIZONTAL);
                                                            bar.setSegmentViewFactory(TypeSegmentView::new);
                                                            sortFirmados.forEach(it -> {
                                                                bar.getSegments().add(new TypeSegment(attEstoque.get(it).doubleValue() / barca.getItens().stream().count(), it));
                                                            });
                                                            boxDadosBarca.add(FormOverlap.create(formOverlap -> {
                                                                formOverlap.add(bar);
                                                                formOverlap.add(FormButton.create(btnInfosBarca -> {
                                                                    btnInfosBarca.title("Prog: " + barca.getProgramacao().getProgramacao() +
                                                                            " #" + barca.getOrdem() +
                                                                            " Cor: " + barca.getProgramacao().getCorbarca());
                                                                    btnInfosBarca.icon(ImageUtils.getIcon(ImageUtils.Icon.ALERT_INFO, ImageUtils.IconSize._16));
                                                                    btnInfosBarca.tooltip("Exibir informações da barca");
                                                                    btnInfosBarca.addStyle("xs").addStyle(barca.getStatus().equals("A")
                                                                            ? "primary"
                                                                            : barca.getStatus().equals("C")
                                                                            ? "success"
                                                                            : "warning");
                                                                    btnInfosBarca.setAction(evt -> exibirInformacoesBarca(barca));
                                                                }));
                                                            }));

                                                            //boxDadosBarca.add(FormButton.create(btnInfosBarca -> {
                                                            //    btnInfosBarca.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                                            //   btnInfosBarca.icon(ImageUtils.getIcon(ImageUtils.Icon.EDIT, ImageUtils.IconSize._16));
                                                            //    btnInfosBarca.tooltip("Alterar configurações da barca");
                                                            //    btnInfosBarca.addStyle("xs").addStyle("secundary");
                                                            //    btnInfosBarca.setAction(evt -> editarBarca(barca));
                                                            //}));
                                                        }));
                                                    }));
                                                });
                                        boxBarcas.add(paneBarcas);
                                    });
                        }));
                    }));
                    ganttContent.add(new Separator(Orientation.HORIZONTAL));
                });
            }));
        }));
    }

    public VBox init() {
        return FormBox.create(containerTab -> {
            containerTab.vertical();
            containerTab.expanded();
            containerTab.add(FormBox.create(boxFilter -> {
                boxFilter.horizontal();
                boxFilter.add(FormTitledPane.create(filter -> {
                    filter.filter();
                    filter.add(FormBox.create(boxFieldsFilter -> {
                        boxFieldsFilter.vertical();
                        boxFieldsFilter.add(fieldStatusBarca.build());
                    }));
                    filter.find.setOnAction(evt -> {
                        carregarGantt();
                    });
                    filter.clean.setOnAction(evt -> {
                        boxGantt.clear();
                        fieldStatusBarca.select(0);
                    });
                }));
            }));
            containerTab.add(boxGantt);
            containerTab.add(FormBox.create(boxTools -> {
                boxTools.horizontal();
                boxTools.add(FormButton.create(btnRodarExplosao -> {
                    btnRodarExplosao.title("Explodir Materiais");
                    btnRodarExplosao.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._32));
                    btnRodarExplosao.addStyle("primary").addStyle("lg");
                    btnRodarExplosao.setAction(evt -> explosaoBarcas());
                }));
//                boxTools.add(FormButton.create(btnRodarExplosao -> {
//                    btnRodarExplosao.title("Firmar Explosão");
//                    btnRodarExplosao.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._32));
//                    btnRodarExplosao.addStyle("success").addStyle("lg");
//                    btnRodarExplosao.setAction(evt -> firmarBarcas());
//                }));
            }));
        });
    }

    private void explosaoBarcas() {
        AtomicReference<Exception> excRun = new AtomicReference<>();
        new RunAsyncWithOverlay(view).exec(task -> {
            try {
                new NativeDAO().runNativeQueryProcedure("pkg_sd_mrp_explosao.p_sd_explodir_mrp");
            } catch (SQLException throwables) {
                excRun.set(throwables);
            }
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                MessageBox.create(message -> {
                    message.message("Explosão de materiais finalizada com sucesso.\nVerifique nas barcas a distribução dos materiais.");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
                carregarGantt();
            } else if (taskReturn.equals(ReturnAsync.EXCEPTION.value)) {
                excRun.get().printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(excRun.get());
                    message.showAndWait();
                });
            }
        });
    }

    private void firmarBarcas() {

    }

    private void exibirInformacoesBarca(SdBarcaProducao barca) {
        Fragment fragmentInfos = new Fragment();
        FormBox fieldStatusBarca = FormBox.create(boxField -> {
            boxField.vertical();
            boxField.title("Status");
            boxField.width(120.0);
            boxField.addStyle(barca.getStatus().equals("P")
                    ? "danger"
                    : barca.getStatus().equals("F") ? "warning"
                    : barca.getStatus().equals("A") ? "primary"
                    : "success");
            boxField.add(FormLabel.create(label -> {
                label.width(120.0);
                label.alignment(Pos.CENTER);
                label.value.set(barca.getStatus().equals("P")
                        ? "P/ ANALISAR"
                        : barca.getStatus().equals("F") ? "PENDENTE"
                        : barca.getStatus().equals("A") ? "FIRMADA"
                        : "CONCLUÍDA");
            }).build());
            boxField.setOnMouseClicked(evt -> {
                if (evt.getClickCount() > 1)
                    if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                            message.message("Deseja realmente marcar a barca como concluída?");
                            message.showAndWait();
                        }).value.get())) {
                        marcarBarcaConcluida(barca);
                        MessageBox.create(message -> {
                            message.message("Barca marcada como concluída com sucesso!");
                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                            message.position(Pos.TOP_RIGHT);
                            message.notification();
                        });
                        carregarGantt();
                        fragmentInfos.close();
                    }
            });
            boxField.tooltip("Clique duas vezes para marcar como concluída a barca.");
        });
        FormBox fieldCorBarca = FormBox.create(boxField -> {
            boxField.vertical();
            boxField.expanded();
            boxField.title("Cor Barca");
            boxField.add(FormLabel.create(label -> {
                label.value.set(((Cor) new FluentDao().selectFrom(Cor.class)
                        .where(eb -> eb.equal("cor", barca.getProgramacao().getCorbarca()))
                        .singleResult()).toString());
            }).build());
        });
        FormBox fieldDataProgramacao = FormBox.create(boxField -> {
            boxField.vertical();
            boxField.title("Dt. Prog.");
            boxField.add(FormLabel.create(label -> {
                label.alignment(Pos.CENTER);
                label.value.set(StringUtils.toDateFormat(barca.getProgramacao().getDtprogramacao()));
            }).build());
        });
        FormBox fieldProgramador = FormBox.create(boxField -> {
            boxField.vertical();
            boxField.addStyle("warning");
            boxField.title("Prog. Por");
            boxField.add(FormLabel.create(label -> {
                label.width(180.0);
                label.value.set(barca.getProgramacao().getUsuario());
            }).build());
        });
        FormBox fieldLoteProgramacao = FormBox.create(boxField -> {
            boxField.vertical();
            boxField.title("Lote Prog.");
            boxField.add(FormLabel.create(label -> {
                label.width(70.0);
                label.alignment(Pos.CENTER);
                label.value.set(barca.getProgramacao().getLote());
            }).build());
        });
        FormBox fieldMaquinaBarca = FormBox.create(boxField -> {
            boxField.vertical();
            boxField.title("Máquina");
            boxField.addStyle("info");
            boxField.add(FormLabel.create(label -> {
                label.width(180.0);
                label.value.set(barca.getMaquina().getMaquina());
            }).build());
        });
        FormBox fieldMaximoMaquinaBarca = FormBox.create(boxField -> {
            boxField.vertical();
            boxField.title("Máx.");
            boxField.add(FormLabel.create(label -> {
                label.width(50.0);
                label.alignment(Pos.CENTER);
                label.value.set(String.valueOf(barca.getMaquina().getMaximo()));
            }).build());
        });
        FormBox fieldMinimoMaquinaBarca = FormBox.create(boxField -> {
            boxField.vertical();
            boxField.title("Min.");
            boxField.add(FormLabel.create(label -> {
                label.width(50.0);
                label.alignment(Pos.CENTER);
                label.value.set(String.valueOf(barca.getMaquina().getMinimo()));
            }).build());
        });
        FormBox fieldQtdeBarca = FormBox.create(boxField -> {
            boxField.vertical();
            boxField.title("Qtde.");
            boxField.add(FormLabel.create(label -> {
                label.width(90.0);
                label.alignment(Pos.CENTER_RIGHT);
                label.value.set(String.valueOf(StringUtils.toDecimalFormat(barca.getQtde().doubleValue(), 3)));
            }).build());
        });
        FormBox fieldDataEnvio = FormBox.create(boxField -> {
            boxField.vertical();
            boxField.title("Dt. Env.");
            boxField.add(FormLabel.create(label -> {
                label.alignment(Pos.CENTER);
                label.width(80.0);
                label.value.set(barca.getDtenvio() != null ? StringUtils.toDateFormat(barca.getDtenvio()) : "-");
            }).build());
        });
        FormBox fieldDataRetorno = FormBox.create(boxField -> {
            boxField.vertical();
            boxField.title("Dt. Ret.");
            boxField.add(FormLabel.create(label -> {
                label.alignment(Pos.CENTER);
                label.width(80.0);
                label.value.set(barca.getDtretorno() != null ? StringUtils.toDateFormat(barca.getDtretorno()) : "-");
            }).build());
        });
        FormBox fieldNumeroOtn = FormBox.create(boxField -> {
            boxField.vertical();
            boxField.title("OTN");
            boxField.addStyle("warning");
            boxField.setOnMouseClicked(evt -> {
                if (evt.getClickCount() > 1 && barca.getNumero() != null && !barca.getStatus().equals("C")) {
                    if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                            message.message("Deseja realmente desvincular a OTN desta barca?");
                            message.showAndWait();
                        }).value.get())) {
                        removerOtnBarca(barca);
                        MessageBox.create(message -> {
                            message.message("OTN desvinculada da barca com sucesso!");
                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                            message.position(Pos.TOP_RIGHT);
                            message.notification();
                        });
                        carregarGantt();
                        fragmentInfos.close();
                    }
                }
            });
            boxField.add(FormLabel.create(label -> {
                label.alignment(Pos.CENTER);
                label.width(70.0);
                label.value.set(barca.getNumero());
            }).build());
            boxField.tooltip("Clique duas vezes desvincular a barca da OTN.");
        });
        FormTableView<SdItensBarca> tblItensBarca = FormTableView.create(SdItensBarca.class, table -> {
            table.title("Itens Barca");
            table.expanded();
            table.items.set(FXCollections.observableList(barca.getItens()));
            table.columns(
                    FormTableColumn.create(cln -> {
                        cln.title("O.F.");
                        cln.width(55.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdItensBarca, SdItensBarca>, ObservableValue<SdItensBarca>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumeroof()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*O.F.*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Lote");
                        cln.width(35.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdItensBarca, SdItensBarca>, ObservableValue<SdItensBarca>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLoteof()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*Lote*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Material");
                        cln.width(180.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdItensBarca, SdItensBarca>, ObservableValue<SdItensBarca>>) param -> new ReadOnlyObjectWrapper(param.getValue().getInsumo()));
                    }).build() /*Material*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Depósito");
                        cln.width(50.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdItensBarca, SdItensBarca>, ObservableValue<SdItensBarca>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDeposito()));
                    }).build() /*Material*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Qtde Estq");
                        cln.width(80.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdItensBarca, SdItensBarca>, ObservableValue<SdItensBarca>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtdeEstoque()));
                        cln.alignment(Pos.CENTER_RIGHT);
                        cln.format(param -> {
                            return new TableCell<SdItensBarca, BigDecimal>() {
                                @Override
                                protected void updateItem(BigDecimal item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    if (item != null && !empty) {
                                        setText(StringUtils.toDecimalFormat(item.doubleValue(), 3));
                                    }
                                }
                            };
                        });
                    }).build() /*Qtde Est.*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Qtde");
                        cln.width(75.0);
                        cln.hide();
                        cln.value((Callback<TableColumn.CellDataFeatures<SdItensBarca, SdItensBarca>, ObservableValue<SdItensBarca>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                        cln.alignment(Pos.CENTER_RIGHT);
                        cln.format(param -> {
                            return new TableCell<SdItensBarca, BigDecimal>() {
                                @Override
                                protected void updateItem(BigDecimal item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    if (item != null && !empty) {
                                        setText(StringUtils.toDecimalFormat(item.doubleValue(), 3));
                                    }
                                }
                            };
                        });
                    }).build() /*Qtde*/
            );
            table.factoryRow(param -> {
                return new TableRow<SdItensBarca>() {
                    @Override
                    protected void updateItem(SdItensBarca item, boolean empty) {
                        super.updateItem(item, empty);
                        getStyleClass().removeAll("table-row-success", "table-row-warning", "table-row-danger");
                        if (item != null && !empty && item.getFirmadoEm() != null) {
                            getStyleClass().add(item.getFirmadoEm().equals("C") ? "table-row-success"
                                    : item.getFirmadoEm().equals("B") ? "table-row-warning"
                                    : "table-row-danger");
                        }
                    }
                };
            });
        });
        fragmentInfos.show(fragment -> {
            fragment.title("Informações Barca");
            fragment.size(450.0, 600.0);
            fragment.box.getChildren().add(FormBox.create(containerFragment -> {
                containerFragment.vertical();
                containerFragment.expanded();
                containerFragment.add(FormBox.create(box -> {
                    box.horizontal();
                    box.add(fieldStatusBarca);
                    box.add(fieldCorBarca);
                }));
                containerFragment.add(FormBox.create(boxDadosProg -> {
                    boxDadosProg.vertical();
                    boxDadosProg.title("Programação");
                    boxDadosProg.add(FormBox.create(box -> {
                        box.horizontal();
                        box.add(fieldProgramador);
                        box.add(fieldLoteProgramacao, fieldDataProgramacao);
                    }));
                }));
                containerFragment.add(FormBox.create(boxDadosBarca -> {
                    boxDadosBarca.vertical();
                    boxDadosBarca.title("Barca");
                    boxDadosBarca.add(FormBox.create(box -> {
                        box.horizontal();
                        box.add(fieldMaquinaBarca);
                        box.add(fieldMinimoMaquinaBarca, fieldMaximoMaquinaBarca);
                    }));
                    boxDadosBarca.add(FormBox.create(box -> {
                        box.horizontal();
                        box.add(fieldQtdeBarca, fieldNumeroOtn);
                        box.add(fieldDataEnvio, fieldDataRetorno);
                    }));
                }));
                containerFragment.add(tblItensBarca.build());
            }));
            if (barca.getNumero() == null)
                fragment.buttonsBox.getChildren().add(FormButton.create(btn -> {
                    btn.title("Firmar Barca");
                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._24));
                    btn.addStyle("success");
                    btn.setAction(evt -> {
                        try {
                            if (barcas.stream()
                                    .anyMatch(barc -> barc.getItens().stream()
                                            .anyMatch(ite -> ite.getFirmadoEm() == null))) {
                                MessageBox.create(message -> {
                                    message.message("Existem barcas pendentes que não foram explodido os materiais, " +
                                            "faça uma nova explosão de materiais ou remova as barcas pendentes da programação.");
                                    message.type(MessageBox.TypeMessageBox.ALERT);
                                    message.showAndWait();
                                });
                                return;
                            }

                            criarOrdemTinturaria(barca);
                            fragment.close();
                            MessageBox.create(message -> {
                                message.message("Ordem de tinturaria criada com sucesso.");
                                message.type(MessageBox.TypeMessageBox.CONFIRM);
                                message.position(Pos.TOP_RIGHT);
                                message.notification();
                            });
                            carregarGantt();
                        } catch (SQLException throwables) {
                            throwables.printStackTrace();
                            ExceptionBox.build(message -> {
                                message.exception(throwables);
                                message.showAndWait();
                            });
                        }
                    });
                }));
        });
    }

    public class TypeSegmentView extends StackPane {

        private Label label;

        public TypeSegmentView(TypeSegment segment) {
            label = new Label();
            label.setStyle("-fx-font-weight: bold; -fx-text-fill: white; -fx-font-size: 1.2em;");
            label.setTextOverrun(OverrunStyle.CLIP);
            label.textProperty().bind(segment.textProperty());
            StackPane.setAlignment(label, Pos.CENTER_LEFT);

            getChildren().add(label);
            switch (segment.getType()) {
                case "X":
                    setStyle("-fx-background-color: #a7cfdb;");
                    break;
                case "C":
                    setStyle("-fx-background-color: #5cb85c;");
                    break;
                case "B":
                    setStyle("-fx-background-color: #fce903;");
                    break;
                case "A":
                case "3":
                    setStyle("-fx-background-color: #d9534f;");
                    break;
                case "2":
                    getStyleClass().add("back_red_yellow");
                    break;
                case "1":
                    getStyleClass().add("back_red_green");
                    break;
            }
            setPadding(new Insets(5));
            setPrefHeight(32);
        }

        @Override
        protected void layoutChildren() {
            super.layoutChildren();
            label.setVisible(label.prefWidth(-1) < getWidth() - getPadding().getLeft() - getPadding().getRight());
        }
    }

    public static class TypeSegment extends SegmentedBar.Segment {
        String type;

        public TypeSegment(double value, String type) {
            super(value);
            this.type = type;
            setText(type);
        }

        public String getType() {
            return type;
        }
    }
}
