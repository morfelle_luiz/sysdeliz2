package sysdeliz2.views.pcp.gestaomrp.mrptinturaria;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Separator;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import sysdeliz2.controllers.views.pcp.gestaomrp.MrpTinturariaTecelagemController;
import sysdeliz2.models.view.mrptinturaria.VSdMrpTinturariaH;
import sysdeliz2.models.view.mrptinturaria.VSdMrpTinturariaV;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.views.pcp.gestaomrp.MrpTinturariaTecelagemView;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class MrpTinturariaFioTab extends MrpTinturariaTecelagemController {
    
    private MrpTinturariaTecelagemView view = null;
    
    // <editor-fold defaultstate="collapsed" desc="bean">
    private final ObjectProperty<BigDecimal> calculadoraSomaSelecao = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> calculadoraSomaLotes = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> calculadoraSomaOutrosLotes = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final StringProperty unidadeCalculadoraSoma = new SimpleStringProperty("");
    private final StringProperty unidadeCalculadoraSomaSelecao = new SimpleStringProperty("");
    private final ObjectProperty<BigDecimal> calculadoraSomaSobrasSelecao = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> acumuladoSobrasEstoque = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> acumuladoSobrasCompra = new SimpleObjectProperty<>(BigDecimal.ZERO);
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fields">
    private final FormFieldToggleSingle fieldDadosExplosao = FormFieldToggleSingle.create(field -> {
        field.title("Dados explosão:");
        field.value.set(false);
        field.value.addListener((observable, oldValue, newValue) -> {
            clearDados();
            getLotesFioMrp(this.fieldSomenteFalta.value.get(), newValue);
            this.fieldLotes.items.set(FXCollections.observableList(super.lotesFioMrp));
        });
    });
    private final FormFieldToggleSingle fieldSomenteFalta = FormFieldToggleSingle.create(field -> {
        field.title("Somente a comprar:");
        field.value.set(true);
        field.value.addListener((observable, oldValue, newValue) -> {
            clearDados();
            getLotesFioMrp(newValue, this.fieldDadosExplosao.value.get());
            this.fieldLotes.items.set(FXCollections.observableList(super.lotesFioMrp));
        });
    });
    private final FormListView<String> fieldLotes = FormListView.create(field -> {
        field.title("Lotes");
        field.multipleSelection();
        field.withoutCounter();
        field.expanded();
        field.width(100.0);
        field.height(180.0);
        field.listProperties().getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                selecionarLoteMrp((List<String>) field.selectedRegisters(),
                        this.fieldMaterial.value.get() == null ? new String[]{} : this.fieldMaterial.value.get().split(","),
                        this.fieldCor.value.get() == null ? new String[]{} : this.fieldCor.value.get().split(","));
            }
        });
    });
    private final FormFieldText fieldMaterial = FormFieldText.create(field -> {
        field.title("Material");
        field.toUpper();
        field.width(300.0);
        field.tooltip("Utilize vírgula(,) para separar os materiais para filtro.");
    });
    private final FormFieldText fieldCor = FormFieldText.create(field -> {
        field.title("Cor");
        field.toUpper();
        field.width(150.0);
        field.tooltip("Utilize vírgula(,) para separar as cores para filtro.");
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="tables">
    private final FormTableView<VSdMrpTinturariaV> tblMateriaisMrp = FormTableView.create(VSdMrpTinturariaV.class, table -> {
        table.withoutTitle();
        table.expanded();
        table.multipleSelection();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("");
                    cln.width(20.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().isSelected()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<VSdMrpTinturariaV, Boolean>(){
                            @Override
                            protected void updateItem(Boolean item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    if (item) {
                                        setGraphic(ImageUtils.getIcon(ImageUtils.Icon.ALERT_INFO, ImageUtils.IconSize._16));
                                    }
                                }
                            }
                        };
                    });
                }).build() /**/,
                FormTableColumn.create(cln -> {
                    cln.title("Material");
                    cln.width(270.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getInsumo()));
                }).build() /*Material*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cor Mat.");
                    cln.width(160.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCorinsumo()));
                }).build() /*Cor Mat.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Dep.");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDeposito()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Dep.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Consumo OF");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getConsumoof()));
                    cln.alignment(Pos.CENTER_RIGHT);
                    cln.hide();
                    cln.format(param -> {
                        return new TableCell<VSdMrpTinturariaV, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 3));
                                }
                            }
                        };
                    });
                }).build() /*Consumo OF*/,
                FormTableColumn.create(cln -> {
                    cln.title("Consumido");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getConsumido()));
                    cln.alignment(Pos.CENTER_RIGHT);
                    cln.format(param -> {
                        return new TableCell<VSdMrpTinturariaV, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 3));
                                }
                            }
                        };
                    });
                }).build() /*Consumido*/,
                FormTableColumn.create(cln -> {
                    cln.title("Un");
                    cln.width(25.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getUnidade()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Un*/,
                FormTableColumn.create(cln -> {
                    cln.title("Fornec.");
                    cln.width(180.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getForinsumo()));
                }).build() /*Fornec.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cons. Estq.");
                    cln.width(90.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getConsumoestq()));
                    cln.alignment(Pos.CENTER_RIGHT);
                    cln.format(param -> {
                        return new TableCell<VSdMrpTinturariaV, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 3));
                                }
                            }
                        };
                    });
                }).build() /*Cons. Estq.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Un");
                    cln.width(25.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getUnidadeestq()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Un*/,
                FormTableColumn.create(cln -> {
                    cln.title("Ord. Comp.");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getOrdcompra()));
                    cln.alignment(Pos.CENTER);
                    cln.hide();
                }).build() /*Ord. Comp.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Entrega");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDtentrega()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<VSdMrpTinturariaV, LocalDate>() {
                            @Override
                            protected void updateItem(LocalDate item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDateFormat(item));
                                }
                            }
                        };
                    });
                    cln.hide();
                }).build() /*Entrega*/
        );
        table.factoryRow(param -> {
            return new TableRow<VSdMrpTinturariaV>() {
                @Override
                protected void updateItem(VSdMrpTinturariaV item, boolean empty) {
                    super.updateItem(item, empty);
                    getStyleClass().removeAll("table-row-warning", "table-row-success", "table-row-danger");
                    if (item != null && !empty) {
                        getStyleClass().add(item.getConsumidode().equals("A") ? "table-row-danger" : item.getConsumidode().equals("B") ? "table-row-warning" : "table-row-success");
                    }
                }
            };
        });
        table.indices(
                table.indice("Estoque", "success", ""),
                table.indice("Compra", "warning", ""),
                table.indice("A Comprar", "danger", "")
        );
        table.selectionModelItem((observable, oldValue, newValue) -> {
            if (newValue != null) {
                carregaDadosMaterialSelecionado((VSdMrpTinturariaV) table.selectedItem());
                calculaMateriasSelecionados((List<VSdMrpTinturariaV>) table.selectedRegisters());
            }
        });
    });
    private final FormTableView<VSdMrpTinturariaH> tblOrigemMateriaisMrp = FormTableView.create(VSdMrpTinturariaH.class, table -> {
        table.withoutTitle();
        table.expanded();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Lote");
                    cln.width(40.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaH, VSdMrpTinturariaH>, ObservableValue<VSdMrpTinturariaH>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLoteof()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Lote*/,
                FormTableColumn.create(cln -> {
                    cln.title("Material");
                    cln.width(230.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaH, VSdMrpTinturariaH>, ObservableValue<VSdMrpTinturariaH>>) param -> new ReadOnlyObjectWrapper(param.getValue().getInsumo()));
                }).build() /*Material*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cor");
                    cln.width(150.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaH, VSdMrpTinturariaH>, ObservableValue<VSdMrpTinturariaH>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCorinsumo()));
                }).build() /*Cor*/,
                FormTableColumn.create(cln -> {
                    cln.title("Fornecedor");
                    cln.width(200.0);
                    cln.hide();
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaH, VSdMrpTinturariaH>, ObservableValue<VSdMrpTinturariaH>>) param -> new ReadOnlyObjectWrapper(param.getValue().getForinsumo()));
                }).build() /*Fornecedor*/,
                FormTableColumn.create(cln -> {
                    cln.title("Un.");
                    cln.width(30.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaH, VSdMrpTinturariaH>, ObservableValue<VSdMrpTinturariaH>>) param -> new ReadOnlyObjectWrapper(param.getValue().getUnidadeestq()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Unidade*/,
                FormTableColumn.create(cln -> {
                    cln.title("Estoque");
                    cln.getStyleClass().add("header-success");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaH, VSdMrpTinturariaH>, ObservableValue<VSdMrpTinturariaH>>) param -> new ReadOnlyObjectWrapper(param.getValue().getConsestqestoque()));
                    cln.alignment(Pos.CENTER_RIGHT);
                    cln.format(param -> {
                        return new TableCell<VSdMrpTinturariaH, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                getStyleClass().removeAll("table-row-success", "border-cell");
                                if (item != null && !empty) {
                                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 3));
                                    getStyleClass().addAll("table-row-success", "border-cell");
                                }
                            }
                        };
                    });
                }).build() /*Estoque*/,
                FormTableColumn.create(cln -> {
                    cln.title("Compra");
                    cln.getStyleClass().add("header-warning");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaH, VSdMrpTinturariaH>, ObservableValue<VSdMrpTinturariaH>>) param -> new ReadOnlyObjectWrapper(param.getValue().getConsestqcompra()));
                    cln.alignment(Pos.CENTER_RIGHT);
                    cln.format(param -> {
                        return new TableCell<VSdMrpTinturariaH, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                getStyleClass().removeAll("table-row-warning", "border-cell");
                                if (item != null && !empty) {
                                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 3));
                                    getStyleClass().addAll("table-row-warning", "border-cell");
                                }
                            }
                        };
                    });
                }).build() /*Compra*/,
                FormTableColumn.create(cln -> {
                    cln.title("A Comprar");
                    cln.getStyleClass().add("header-danger");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaH, VSdMrpTinturariaH>, ObservableValue<VSdMrpTinturariaH>>) param -> new ReadOnlyObjectWrapper(param.getValue().getConsestqfalta()));
                    cln.alignment(Pos.CENTER_RIGHT);
                    cln.format(param -> {
                        return new TableCell<VSdMrpTinturariaH, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                getStyleClass().removeAll("table-row-danger", "border-cell");
                                if (item != null && !empty) {
                                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 3));
                                    getStyleClass().addAll("table-row-danger", "border-cell");
                                }
                            }
                        };
                    });
                }).build() /*A Comprar*/
        );
        table.factoryRow(param -> {
            return new TableRow<VSdMrpTinturariaH>() {
                @Override
                protected void updateItem(VSdMrpTinturariaH item, boolean empty) {
                    super.updateItem(item, empty);
                    getStyleClass().removeAll("table-row-danger", "table-row-warning", "table-row-success");
                    if (item != null && !empty) {
                        getStyleClass().add(item.getConsestqfalta().compareTo(BigDecimal.ZERO) > 0 ? "table-row-danger" : item.getConsestqcompra().compareTo(BigDecimal.ZERO) > 0 ? "table-row-warning" : "table-row-success");
                    }
                }
            };
        });
        table.selectionModelItem((observable, oldValue, newValue) -> {
            if (newValue != null) {
                List<VSdMrpTinturariaV> materiaisFilho = getMateriaisFilho(fieldDadosExplosao.value.get(),
                        ((VSdMrpTinturariaH) newValue).getInsumo().getCodigo(),
                        ((VSdMrpTinturariaH) newValue).getCorinsumo().getCor());
                this.tblMateriaisMrp.items.forEach(item -> {
                    item.setSelected(false);
                    if (materiaisFilho.stream().map(mat -> mat.getInsumo().getCodigo()).anyMatch(insumo -> insumo.equals(item.getInsumo().getCodigo())) &&
                            materiaisFilho.stream().map(mat -> mat.getCorinsumo().getCor()).anyMatch(insumo -> insumo.equals(item.getCorinsumo().getCor())))
                        item.setSelected(true);
                });
                this.tblMateriaisMrp.refresh();
            }
        });
    });
    private final FormTableView<VSdMrpTinturariaV> tblSobrasMateriaisMrp = FormTableView.create(VSdMrpTinturariaV.class, table -> {
        table.withoutTitle();
        table.expanded();
        table.multipleSelection();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Material");
                    cln.width(270.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getInsumo()));
                }).build() /*Material*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cor Mat.");
                    cln.width(160.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCorinsumo()));
                }).build() /*Cor Mat.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Dep.");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDeposito()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Dep.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Qtde");
                    cln.width(80.0);
                    cln.hide();
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getConsumido()));
                    cln.alignment(Pos.CENTER_RIGHT);
                    cln.format(param -> {
                        return new TableCell<VSdMrpTinturariaV, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 3));
                                }
                            }
                        };
                    });
                }).build() /*Qtde*/,
                FormTableColumn.create(cln -> {
                    cln.title("Un");
                    cln.width(25.0);
                    cln.hide();
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getUnidade()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Un*/,
                FormTableColumn.create(cln -> {
                    cln.title("Fornec.");
                    cln.width(180.0);
                    cln.hide();
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getForinsumo()));
                }).build() /*Fornec.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Qtde Estq.");
                    cln.width(90.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getConsumoestq()));
                    cln.alignment(Pos.CENTER_RIGHT);
                    cln.format(param -> {
                        return new TableCell<VSdMrpTinturariaV, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 3));
                                }
                            }
                        };
                    });
                }).build() /*Qtde Estq.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Un");
                    cln.width(25.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getUnidadeestq()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Un*/,
                FormTableColumn.create(cln -> {
                    cln.title("Ord. Comp.");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getOrdcompra()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Ord. Comp.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Entrega");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDtentrega()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<VSdMrpTinturariaV, LocalDate>() {
                            @Override
                            protected void updateItem(LocalDate item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDateFormat(item));
                                }
                            }
                        };
                    });
                }).build() /*Entrega*/
        );
        table.factoryRow(param -> {
            return new TableRow<VSdMrpTinturariaV>() {
                @Override
                protected void updateItem(VSdMrpTinturariaV item, boolean empty) {
                    super.updateItem(item, empty);
                    getStyleClass().removeAll("table-row-warning", "table-row-success", "table-row-danger");
                    if (item != null && !empty) {
                        getStyleClass().add(item.getConsumidode().equals("A") ? "table-row-danger" : item.getConsumidode().equals("B") ? "table-row-warning" : "table-row-success");
                    }
                }
            };
        });
        table.selectionModelItem((observable, oldValue, newValue) -> {
            if (newValue != null) {
                calculaSobrasMateriasSelecionados((List<VSdMrpTinturariaV>) table.selectedRegisters());
            }
        });
    });
    // </editor-fold>
    
    public MrpTinturariaFioTab(MrpTinturariaTecelagemView view) {
        this.view = view;
    }

    public void loadLotes() {
        getLotesFioMrp(true, false);
    }

    public void initLotes() {
        fieldLotes.items.set(FXCollections.observableList(super.lotesFioMrp));
    }
    
    public VBox init() {
       return FormBox.create(container -> {
            container.horizontal();
            container.expanded();
            container.add(FormBox.create(boxFiltros -> {
                boxFiltros.vertical();
                boxFiltros.title("Filtros");
                boxFiltros.addStyle("light-primary");
                //boxFiltros.add(fieldDadosExplosao.build());
                boxFiltros.add(fieldSomenteFalta.build());
                boxFiltros.add(fieldLotes.build());
            }));
            container.add(FormBox.create(boxDadosMrp -> {
                boxDadosMrp.vertical();
                boxDadosMrp.expanded();
                boxDadosMrp.add(FormBox.create(boxDadosMateriais -> {
                    boxDadosMateriais.horizontal();
                    boxDadosMateriais.expanded();
                    boxDadosMateriais.add(FormBox.create(boxDadosOrigem -> {
                        boxDadosOrigem.vertical();
                        boxDadosOrigem.expanded();
                        boxDadosOrigem.title("Fio");
                        boxDadosOrigem.addStyle("light-info");
                        boxDadosOrigem.add(FormBox.create(boxFilter -> {
                            boxFilter.horizontal();
                            boxFilter.add(FormTitledPane.create(filter -> {
                                filter.filter();
                                filter.collapse(false);
                                filter.add(FormBox.create(boxFieldsFilter -> {
                                    boxFieldsFilter.horizontal();
                                    boxFieldsFilter.add(fieldMaterial.build(), fieldCor.build());
                                }));
                                filter.find.setOnAction(evt -> {
                                    if (fieldLotes.selectedRegisters().size() == 0) {
                                        MessageBox.create(message -> {
                                            message.message("É necessário selecionar um ou mais lotes para consulta.");
                                            message.type(MessageBox.TypeMessageBox.IMPORTANT);
                                            message.showAndWait();
                                        });
                                    } else {
                                        selecionarLoteMrp(fieldLotes.selectedRegisters(),
                                                fieldMaterial.value.get() == null ? new String[]{} : fieldMaterial.value.get().split(","),
                                                fieldCor.value.get() == null ? new String[]{} : fieldCor.value.get().split(","));
                                    }
                                });
                                filter.clean.setOnAction(evt -> {
                                    fieldMaterial.clear();
                                    fieldCor.clear();
                                    if (fieldLotes.selectedRegisters().size() > 0) {
                                        selecionarLoteMrp(fieldLotes.selectedRegisters(),
                                                fieldMaterial.value.get() == null ? new String[]{} : fieldMaterial.value.get().split(","),
                                                fieldCor.value.get() == null ? new String[]{} : fieldCor.value.get().split(","));
                                    }
                                });
                            }));
                        }));
                        boxDadosOrigem.add(tblMateriaisMrp.build());
                        boxDadosOrigem.add(FormBox.create(boxTotais -> {
                            boxTotais.horizontal();
                            boxTotais.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.label("Somatório\nSeleção");
                                field.value.bind(calculadoraSomaSelecao.asString("%.3f").concat(" ").concat(unidadeCalculadoraSomaSelecao));
                                field.width(230.0);
                                field.addStyle("lg");
                                field.alignment(Pos.CENTER_RIGHT);
                            }).build());
                            boxTotais.add(new Separator(Orientation.VERTICAL));
                            boxTotais.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.label("Consumo\nLotes");
                                field.value.bind(calculadoraSomaLotes.asString("%.3f").concat(" ").concat(unidadeCalculadoraSoma));
                                field.width(230.0);
                                field.addStyle("lg");
                                field.alignment(Pos.CENTER_RIGHT);
                            }).build());
                            boxTotais.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.label("Saldo\nLotes");
                                field.value.bind(calculadoraSomaOutrosLotes.asString("%.3f").concat(" ").concat(unidadeCalculadoraSoma));
                                field.width(230.0);
                                field.addStyle("lg");
                                field.alignment(Pos.CENTER_RIGHT);
                            }).build());
                        }));
                    }));
                    boxDadosMateriais.add(FormBox.create(boxDadosSubniveis -> {
                        boxDadosSubniveis.vertical();
                        boxDadosSubniveis.width(700.0);
                        boxDadosSubniveis.add(FormBox.create(boxDadosOrigemMaterial -> {
                            boxDadosOrigemMaterial.vertical();
                            boxDadosOrigemMaterial.title("Origem Material");
                            boxDadosOrigemMaterial.addStyle("light-amber");
                            boxDadosOrigemMaterial.expanded();
                            boxDadosOrigemMaterial.add(tblOrigemMateriaisMrp.build());
                        }));
                        boxDadosSubniveis.add(FormBox.create(boxDadosEstoque -> {
                            boxDadosEstoque.vertical();
                            boxDadosEstoque.title("Sobras");
                            boxDadosEstoque.addStyle("light-dark");
                            boxDadosEstoque.expanded();
                            boxDadosEstoque.add(tblSobrasMateriaisMrp.build());
                            boxDadosEstoque.add(FormBox.create(boxTotais -> {
                                boxTotais.horizontal();
                                boxTotais.alignment(Pos.TOP_RIGHT);
                                boxTotais.add(FormBox.create(boxAcumulado -> {
                                    boxAcumulado.horizontal();
                                    boxAcumulado.expanded();
                                    boxAcumulado.alignment(Pos.TOP_LEFT);
                                    boxAcumulado.add(FormFieldText.create(field -> {
                                        field.withoutTitle();
                                        field.label("Estoque");
                                        field.value.bind(acumuladoSobrasEstoque.asString("%.3f"));
                                        field.addStyle("success");
                                        field.width(130.0);
                                        field.alignment(Pos.CENTER_RIGHT);
                                    }).build());
                                    boxAcumulado.add(FormFieldText.create(field -> {
                                        field.withoutTitle();
                                        field.label("Compra");
                                        field.value.bind(acumuladoSobrasCompra.asString("%.3f"));
                                        field.addStyle("warning");
                                        field.width(130.0);
                                        field.alignment(Pos.CENTER_RIGHT);
                                    }).build());
                                }));
                                boxTotais.add(FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.label("Somatório\nSeleção");
                                    field.value.bind(calculadoraSomaSobrasSelecao.asString("%.3f"));
                                    field.width(210.0);
                                    field.addStyle("lg");
                                    field.alignment(Pos.CENTER_RIGHT);
                                }).build());
                            }));
                        }));
                    }));
                }));
            }));
        });
    }
    /**
     * Limpeza dos dados
     * Limpa as tabelas e os beans que exibem os totais selecionados
     */
    private void clearDados() {
        tblMateriaisMrp.items.clear();
        tblOrigemMateriaisMrp.items.clear();
        tblSobrasMateriaisMrp.items.clear();
        calculadoraSomaSelecao.set(BigDecimal.ZERO);
        calculadoraSomaLotes.set(BigDecimal.ZERO);
        calculadoraSomaOutrosLotes.set(BigDecimal.ZERO);
        calculadoraSomaSobrasSelecao.set(BigDecimal.ZERO);
        unidadeCalculadoraSomaSelecao.set("");
        acumuladoSobrasEstoque.set(BigDecimal.ZERO);
        acumuladoSobrasCompra.set(BigDecimal.ZERO);
    }
    
    /**
     * Action SELECT_MODEL do ListView de lotes
     * Get dos materiais a partir dos lotes selecionados
     *
     * @param lotes
     * @param materiais
     * @param cores
     */
    private void selecionarLoteMrp(List<String> lotes, Object[] materiais, Object[] cores) {
        clearDados();
        AtomicReference<List<VSdMrpTinturariaV>> materiaisMrp = new AtomicReference<>(new ArrayList<>());
        AtomicReference<List<VSdMrpTinturariaV>> outrosLotesMaterialSelecionado = new AtomicReference<>(new ArrayList<>());
        new RunAsyncWithOverlay(view).exec(task -> {
            materiaisMrp.set(getMateriaisGrupo10(fieldDadosExplosao.value.get(), fieldSomenteFalta.value.get(),
                    lotes.toArray(), materiais, cores));
            outrosLotesMaterialSelecionado.set(getMateriaisGrupo10(fieldDadosExplosao.value.get(), fieldSomenteFalta.value.get(),
                    fieldLotes.notSelectedRegisters().toArray(), materiais, cores));
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                tblMateriaisMrp.items.set(FXCollections.observableList(materiaisMrp.get()));
    
                unidadeCalculadoraSoma.set(materiaisMrp.get().stream().map(VSdMrpTinturariaV::getUnidadeestq).max(Comparator.naturalOrder()).get());
                calculadoraSomaLotes.set(new BigDecimal(materiaisMrp.get().stream().mapToDouble(material -> material.getConsumoestq().doubleValue()).sum()));
                calculadoraSomaOutrosLotes.set(new BigDecimal(outrosLotesMaterialSelecionado.get().stream().mapToDouble(material -> material.getConsumoestq().doubleValue()).sum()));
            }
        });
    }
    /**
     * Action SELECT_MODEL do TableView materiais
     * Get dos dados de OFs e sobras do material selecionado
     *
     * @param materialSelecionado
     */
    private void carregaDadosMaterialSelecionado(VSdMrpTinturariaV materialSelecionado) {
        // limpeza da marcação de materiais para formação da origem
        tblMateriaisMrp.items.forEach(item -> item.setSelected(false));
        tblMateriaisMrp.refresh();
        // atomic para carga dos dados para as tabelas e calculadores
        AtomicReference<List<VSdMrpTinturariaV>> outrosLotesMaterialSelecionado = new AtomicReference<>(new ArrayList<>());
        AtomicReference<List<VSdMrpTinturariaH>> materiaisPaiSelecionados = new AtomicReference<>(new ArrayList<>());
        AtomicReference<List<VSdMrpTinturariaV>> sobrasMateriaisSelecionados = new AtomicReference<>(new ArrayList<>());
        // async
        new RunAsyncWithOverlay(this.view).exec(task -> {
            // get das sobras do material selecionado
            sobrasMateriaisSelecionados.set(getSobrasMaterialSelecionado(
                    fieldDadosExplosao.value.get(),
                    materialSelecionado.getInsumo().getCodigo(),
                    materialSelecionado.getCorinsumo().getCor()));
            // get do mrp do material selecionado nos lotes não selecionados
            outrosLotesMaterialSelecionado.set(getOutrosLotesMaterialSelecionado(
                    fieldDadosExplosao.value.get(),
                    fieldSomenteFalta.value.get(),
                    materialSelecionado.getInsumo().getCodigo(),
                    materialSelecionado.getCorinsumo().getCor(),
                    fieldLotes.notSelectedRegisters().toArray()
            ));
            // get dos materiais que originam o material selecionado nos lotes selecionados
            List<VSdMrpTinturariaV> materiaisPai = getMateriaisPai(
                    fieldDadosExplosao.value.getValue(),
                    materialSelecionado.getInsumo().getCodigo(), materialSelecionado.getCorinsumo().getCor());
            materiaisPaiSelecionados.set(getOrigemMaterialSelecionado(
                    fieldDadosExplosao.value.get(),
                    materiaisPai.stream().map(VSdMrpTinturariaV::getMaterialpai).distinct().toArray(),
                    materiaisPai.stream().map(VSdMrpTinturariaV::getCormaterialpai).distinct().toArray(),
                    fieldLotes.selectedRegisters().toArray()));
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                // somatório do material seleciona e seus outros lotes
                unidadeCalculadoraSoma.set(materialSelecionado.getUnidadeestq());
                calculadoraSomaLotes.set(materialSelecionado.getConsumoestq());
                calculadoraSomaOutrosLotes.set(new BigDecimal(outrosLotesMaterialSelecionado.get().stream().mapToDouble(material -> material.getConsumoestq().doubleValue()).sum()));
                // alimentando dados dos materiais de origem do material selecionado
                tblOrigemMateriaisMrp.items.set(FXCollections.observableList(materiaisPaiSelecionados.get()));
                // alimentando dados da tabela de sobras e os acumuladores por estoque
                tblSobrasMateriaisMrp.items.set(FXCollections.observableList(sobrasMateriaisSelecionados.get()));
                acumuladoSobrasEstoque.set(new BigDecimal(sobrasMateriaisSelecionados.get().stream()
                        .filter(material -> material.getConsumidode().equals("C"))
                        .mapToDouble(material -> material.getConsumoestq().doubleValue()).sum()).setScale(3, RoundingMode.HALF_UP));
                acumuladoSobrasCompra.set(new BigDecimal(sobrasMateriaisSelecionados.get().stream()
                        .filter(material -> material.getConsumidode().equals("B"))
                        .mapToDouble(material -> material.getConsumoestq().doubleValue()).sum()).setScale(3, RoundingMode.HALF_UP));
            }
        });
    }
    /**
     * Action SELECT_MODEL da TableView materiais para o cálculo do consumo de estoque dos materiais selecionados
     *
     * @param materiaisSelecionados
     */
    private void calculaMateriasSelecionados(List<VSdMrpTinturariaV> materiaisSelecionados) {
        calculadoraSomaSelecao.set(new BigDecimal(materiaisSelecionados.stream().mapToDouble(material -> material.getConsumoestq().doubleValue()).sum()).setScale(3, RoundingMode.HALF_UP));
        unidadeCalculadoraSomaSelecao.set(materiaisSelecionados.stream().map(VSdMrpTinturariaV::getUnidadeestq).max(Comparator.naturalOrder()).get());
    }
    /**
     * Action SELECT_MODEL da TableView de sobras para o cálculo do consumo de estoque dos materiais selecionados
     *
     * @param materiaisSelecionados
     */
    private void calculaSobrasMateriasSelecionados(List<VSdMrpTinturariaV> materiaisSelecionados) {
        calculadoraSomaSobrasSelecao.set(new BigDecimal(materiaisSelecionados.stream().mapToDouble(material -> material.getConsumoestq().doubleValue()).sum()).setScale(3, RoundingMode.HALF_UP));
    }
    /**
     * Fragment para abertura de tabelas de totais
     * Caso necessário implementar as tabelas de totais
     */
    @Deprecated
    private void abrirTotaisFioTab() {
        new Fragment().showAndContinue(fragment -> {
            fragment.title("Consumo Acumulado por Material");
            fragment.size(300.0, 500.0);
        });
    }
    
}
