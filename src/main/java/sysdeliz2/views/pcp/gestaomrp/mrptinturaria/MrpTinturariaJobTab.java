package sysdeliz2.views.pcp.gestaomrp.mrptinturaria;

import javafx.scene.layout.VBox;
import sysdeliz2.controllers.views.pcp.gestaomrp.MrpTinturariaTecelagemController;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.gui.components.FormBox;
import sysdeliz2.utils.gui.components.FormButton;
import sysdeliz2.utils.gui.components.FormFieldText;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.views.pcp.gestaomrp.MrpTinturariaTecelagemView;

import java.sql.SQLException;

public class MrpTinturariaJobTab extends MrpTinturariaTecelagemController {
    
    // <editor-fold defaultstate="collapsed" desc="fields job tab">
    private final FormFieldText inicioUltimaRodadaField = FormFieldText.create(field -> {
        field.title("Início Última Rodada MRP");
        field.editable(false);
        field.value.bind(super.inicioUltimoLog);
    });
    private final FormFieldText tempoUltimaRodadaField = FormFieldText.create(field -> {
        field.title("Tempo Última Rodada MRP");
        field.editable(false);
        field.value.bind(super.tempoUltimoLog);
    });
    private final FormFieldText estadoJobField = FormFieldText.create(field -> {
        field.title("Estado MRP");
        field.editable(false);
        field.value.bind(super.statusJobLog);
        field.value.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                if (newValue.equals("EM EXECUÇÃO")) {
                    field.addStyle("danger");
                } else {
                    field.removeStyle("danger");
                }
            }
        });
    });
    private final FormFieldText dataProximaRodadaField = FormFieldText.create(field -> {
        field.title("Próxima Rodada MRP");
        field.editable(false);
        field.value.bind(super.dataProximoLog);
    });
    // </editor-fold>
    private MrpTinturariaTecelagemView view = null;
    
    public MrpTinturariaJobTab(MrpTinturariaTecelagemView view) {
        this.view = view;
        new RunAsyncWithOverlay(view).exec(task -> {
            try {
                getStatusJob();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
            }
        });
    }
    
    public VBox init() {
        return FormBox.create(container -> {
            container.vertical();
            container.expanded();
            container.add(FormBox.create(boxPrincipal -> {
                boxPrincipal.horizontal();
                boxPrincipal.add(FormBox.create(boxJob -> {
                    boxJob.vertical();
                    boxJob.title("Job MRP");
                    boxJob.add(estadoJobField.build());
                    boxJob.add(inicioUltimaRodadaField.build());
                    boxJob.add(tempoUltimaRodadaField.build());
                    boxJob.add(dataProximaRodadaField.build());
                    boxJob.add(FormBox.create(lo1 -> {
                        lo1.horizontal();
                        lo1.add(FormButton.create(btn -> {
                            btn.title("Rodar MRP");
                            btn.addStyle("lg").addStyle("success");
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._32));
                            btn.setAction(event -> {
                                try {
                                    runJob();
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    ExceptionBox.build(message -> {
                                        message.exception(e);
                                        message.showAndWait();
                                    });
                                }
                            });
                        }));
                        lo1.add(FormButton.create(btn -> {
                            btn.title("Atualizar status MRP");
                            btn.addStyle("sm").addStyle("primary");
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ATUALIZAR, ImageUtils.IconSize._24));
                            btn.setAction(event -> {
                                try {
                                    getStatusJob();
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                    ExceptionBox.build(message -> {
                                        message.exception(e);
                                        message.showAndWait();
                                    });
                                }
                            });
                        }));
                    }));
                }));
            }));
        });
    }
}
