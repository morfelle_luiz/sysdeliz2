package sysdeliz2.views.pcp.gestaomrp.mrptinturaria;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import sysdeliz2.controllers.views.pcp.gestaomrp.MrpTinturariaTecelagemController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.sysdeliz.pcp.SdBarcaFornecedor;
import sysdeliz2.models.sysdeliz.pcp.SdBarcaProducao;
import sysdeliz2.models.sysdeliz.pcp.SdItensBarca;
import sysdeliz2.models.ti.Cor;
import sysdeliz2.models.ti.Entidade;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.InputBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.DateUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.views.pcp.gestaomrp.MrpTinturariaTecelagemView;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class MrpTinturariaKanbanTab extends MrpTinturariaTecelagemController {

    private MrpTinturariaTecelagemView view = null;

    private final FormFieldSegmentedButton<String> fieldStatusBarca = FormFieldSegmentedButton.create(field -> {
        field.title("Status Barcas");
        field.options(
                field.option("Pendentes, Firmadas e Analisadas", "PFA", FormFieldSegmentedButton.Style.DARK),
                field.option("Pendentes", "P", FormFieldSegmentedButton.Style.WARNING),
                field.option("Firmadas", "F", FormFieldSegmentedButton.Style.DANGER),
                field.option("Analisada", "A", FormFieldSegmentedButton.Style.DEFAULT),
                field.option("Concluídas", "C", FormFieldSegmentedButton.Style.SUCCESS),
                field.option("Pendentes e Firmadas", "PF", FormFieldSegmentedButton.Style.SECUNDARY),
                field.option("Firmadas e Analisadas", "FA", FormFieldSegmentedButton.Style.INFO),
                field.option("Todas", "T", FormFieldSegmentedButton.Style.PRIMARY)
        );
        field.select(0);
    });
    private final FormFieldSingleFind<Entidade> fieldFornecedor = FormFieldSingleFind.create(Entidade.class, field -> {
        field.title("Fornecedor");
        field.width(300.0);
    });
    private final FormBox boxGantt = FormBox.create(gantt -> {
        gantt.horizontal();
        gantt.expanded();
        gantt.horizontalScroll();
    });

    public MrpTinturariaKanbanTab(MrpTinturariaTecelagemView view) {
        this.view = view;
    }

    private void carregarGantt() {
        if (fieldFornecedor.value.get() == null) {
            MessageBox.create(message -> {
                message.message("Selecione o fornecedor para carregar as barcas com os status selecionados.");
                message.type(MessageBox.TypeMessageBox.IMPORTANT);
                message.showAndWait();
            });
            return;
        }

        boxGantt.clear();
        List<SdBarcaProducao> barcas = (List<SdBarcaProducao>) new FluentDao().selectFrom(SdBarcaProducao.class)
                .where(eb -> eb
                        .equal("status", "P", TipoExpressao.AND, b -> fieldStatusBarca.value.get().equals("P"))
                        .equal("status", "F", TipoExpressao.AND, b -> fieldStatusBarca.value.get().equals("F"))
                        .equal("status", "A", TipoExpressao.AND, b -> fieldStatusBarca.value.get().equals("A"))
                        .equal("status", "C", TipoExpressao.AND, b -> fieldStatusBarca.value.get().equals("C"))
                        .isIn("status", new String[]{"P", "F"}, TipoExpressao.AND, b -> fieldStatusBarca.value.get().equals("PF"))
                        .isIn("status", new String[]{"F", "A"}, TipoExpressao.AND, b -> fieldStatusBarca.value.get().equals("FA"))
                        .isIn("status", new String[]{"P", "F", "A"}, TipoExpressao.AND, b -> fieldStatusBarca.value.get().equals("PFA"))
                        .equal("programacao.fornecedor", fieldFornecedor.value.get().getCodcli()))
                .resultList();

        barcas.forEach(barca -> {
//            JPAUtils.getEntityManager().refresh(barca);
            barca.getItens().forEach(item -> {
                JPAUtils.getEntityManager().refresh(item);
            });
        });
        AtomicReference<String> fornecedor = new AtomicReference<>();
        barcas.stream().findAny().ifPresent(forn -> fornecedor.set(String.valueOf(forn.getProgramacao().getFornecedor())));

        LocalDate inicioPeriodo = barcas.stream()
                .filter(barca -> barca.getDtenvio() != null)
                .map(SdBarcaProducao::getDtenvio)
                .min((o1, o2) -> o1.compareTo(o2))
                .orElse(LocalDate.now());
        LocalDate fimPeriodo = barcas.stream()
                .filter(barca -> barca.getDtretorno() != null)
                .map(SdBarcaProducao::getDtretorno)
                .max((o1, o2) -> o1.compareTo(o2))
                .orElse(LocalDate.now());

        Long qtdeDiasPeriodo = ChronoUnit.DAYS.between(inicioPeriodo, fimPeriodo) + 1;

        Map<SdBarcaFornecedor, List<SdBarcaProducao>> maquinasFornecedor = barcas.stream()
                .collect(Collectors.groupingBy(SdBarcaProducao::getMaquina));
        maquinasFornecedor.forEach((maquina, barcasMaquina) -> {
            barcasMaquina.stream()
                    .filter(barca -> barca.getStatus().matches("(F|A)"))
                    .sorted((o1, o2) -> o1.getDtenvio().compareTo(o2.getDtenvio()));
            barcasMaquina.stream()
                    .filter(barca -> barca.getStatus().matches("(F|A)"))
                    .forEach(barca -> {
                        List<SdBarcaProducao> barcasConcorrente = barcasMaquina.stream()
                                .filter(concorrente -> !concorrente.equals(barca) && concorrente.getStatus().matches("(F|A)")
                                        && DateUtils.between(barca.getDtenvio(), concorrente.getDtenvio(), concorrente.getDtretorno()))
                                .collect(Collectors.toList());
                        if (barcasConcorrente.size() == 0) {
                            barca.setAndar(1);
                        } else {
                            AtomicInteger andar = new AtomicInteger(0);
                            do {
                                andar.incrementAndGet();
                            } while (barcasConcorrente.stream()
                                    .mapToInt(SdBarcaProducao::getAndar)
                                    .anyMatch(value -> andar.get() == value));
                            barca.setAndar(andar.get());
                        }
                    });
        });

        boxGantt.add(FormBox.create(ganttContainer -> {
            ganttContainer.vertical();
            ganttContainer.width(200 + (qtdeDiasPeriodo * 40));
            ganttContainer.add(FormBox.create(header -> {
                header.horizontal();
                header.addStyle("secundary");
                header.add(FormBox.create(dadosFornecedor -> {
                    dadosFornecedor.horizontal();
                    dadosFornecedor.width(200.0);
                    dadosFornecedor.add(FormLabel.create(label -> {
                        label.value.set("Período");
                        label.fontColor("#FFF");
                        label.boldText();
                    }).build());
                    dadosFornecedor.alignment(Pos.CENTER_RIGHT);
                }));
                for (int i = 0; i < qtdeDiasPeriodo; i++) {
                    int finalI = i;
                    header.add(FormBox.create(boxDia -> {
                        boxDia.horizontal();
                        boxDia.width(35.0);
                        boxDia.add(FormLabel.create(label -> {
                            label.value.set(inicioPeriodo.plusDays(finalI).format(DateTimeFormatter.ofPattern("dd/MM"))
                                    + "\n " + inicioPeriodo.plusDays(finalI).format(DateTimeFormatter.ofPattern("yyyy")));
                            label.sizeText(10);
                            label.alignment(Pos.CENTER);
                            label.fontColor("#FFF");
                        }).build());
                    }));
                }
            }));
            ganttContainer.add(new Separator(Orientation.HORIZONTAL));
            ganttContainer.add(FormBox.create(ganttContent -> {
                ganttContent.vertical();
                ganttContent.expanded();
                maquinasFornecedor.forEach((maquina, barcasFornecedor) -> {
                    ganttContent.add(FormBox.create(kanbanMaquina -> {
                        kanbanMaquina.horizontal();
                        kanbanMaquina.height(200.0);
                        kanbanMaquina.verticalScroll();
                        kanbanMaquina.add(FormBox.create(dadosMaquina -> {
                            dadosMaquina.vertical();
                            dadosMaquina.width(200.0);
                            dadosMaquina.add(FormBox.create(boxNomeMaquina -> {
                                boxNomeMaquina.horizontal();
                                boxNomeMaquina.addStyle("primary");
                                boxNomeMaquina.add(FormLabel.create(label -> {
                                    label.value.set(maquina.getMaquina());
                                    label.boldText();
                                    label.fontColor("#FFF");
                                }).build());
                            }));
                            dadosMaquina.add(FormTableView.create(SdBarcaProducao.class, table -> {
                                table.title("Pendentes");
                                table.withoutHeader();
                                table.expanded();
                                table.height(150.0);
                                table.items.set(FXCollections.observableList(barcasFornecedor.stream()
                                        .filter(barca -> barca.getStatus().matches("P"))
                                        .collect(Collectors.toList())));
                                table.columns(
                                        FormTableColumn.create(cln -> {
                                            cln.title("Prog.");
                                            cln.width(40.0);
                                            cln.value((Callback<TableColumn.CellDataFeatures<SdBarcaProducao, SdBarcaProducao>, ObservableValue<SdBarcaProducao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getProgramacao().getProgramacao()));
                                            cln.alignment(Pos.CENTER);
                                        }).build() /*Prog.*/,
                                        FormTableColumn.create(cln -> {
                                            cln.title("Cor");
                                            cln.width(50.0);
                                            cln.value((Callback<TableColumn.CellDataFeatures<SdBarcaProducao, SdBarcaProducao>, ObservableValue<SdBarcaProducao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getProgramacao().getCorbarca()));
                                            cln.alignment(Pos.CENTER);
                                        }).build() /*Cor*/,
                                        FormTableColumn.create(cln -> {
                                            cln.title("Ações");
                                            cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                                            cln.width(80.0);
                                            cln.alignment(FormTableColumn.Alignment.CENTER);
                                            cln.value((Callback<TableColumn.CellDataFeatures<SdBarcaProducao, SdBarcaProducao>, ObservableValue<SdBarcaProducao>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                            cln.format(param -> {
                                                return new TableCell<SdBarcaProducao, SdBarcaProducao>() {
                                                    final HBox boxButtonsRow = new HBox(3);
                                                    final Button btnInfoBarca = FormButton.create(btn -> {
                                                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ALERT_INFO, ImageUtils.IconSize._16));
                                                        btn.tooltip("Informações da barca");
                                                        btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                                        btn.addStyle("xs").addStyle("info");
                                                    });
                                                    final Button btnFirmarBarca = FormButton.create(btn -> {
                                                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._16));
                                                        btn.tooltip("Enviar barca para planejamento");
                                                        btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                                        btn.addStyle("xs").addStyle("success");
                                                    });

                                                    @Override
                                                    protected void updateItem(SdBarcaProducao item, boolean empty) {
                                                        super.updateItem(item, empty);
                                                        setText(null);
                                                        setGraphic(null);
                                                        if (item != null && !empty) {

                                                            btnInfoBarca.setOnAction(evt -> {
                                                                exibirInformacoesBarca(item);
                                                            });
                                                            btnFirmarBarca.setOnAction(evt -> {
                                                                firmarBarca(item);
                                                                carregarGantt();
                                                            });
                                                            boxButtonsRow.getChildren().clear();
                                                            boxButtonsRow.getChildren().addAll(btnInfoBarca, btnFirmarBarca);
                                                            setGraphic(boxButtonsRow);
                                                        }
                                                    }
                                                };
                                            });
                                        }).build()
                                );
                            }).build());
                        }));
                        kanbanMaquina.add(FormBox.create(boxBarcas -> {
                            boxBarcas.vertical();
                            boxBarcas.expanded();
                            barcasFornecedor.stream()
                                    .filter(barca -> !barca.getStatus().matches("P"))
                                    .sorted((o1, o2) -> o1.getDtenvio().compareTo(o2.getDtenvio()))
                                    .mapToInt(SdBarcaProducao::getAndar)
                                    .distinct()
                                    .sorted()
                                    .forEach(andar -> {
                                        Pane paneBarcas = new Pane();
                                        barcasFornecedor.stream()
                                                .filter(barca -> barca.getStatus().matches("(A|F)")
                                                        && barca.getAndar() == andar)
                                                .forEach(barca -> {
                                                    paneBarcas.getChildren().add(FormBox.create(boxBarca -> {
                                                        boxBarca.horizontal();
                                                        Long diasBarca = ChronoUnit.DAYS.between(barca.getDtenvio(), barca.getDtretorno()) + 1;
                                                        boxBarca.width(diasBarca * 40);
                                                        boxBarca.height(32.0);
                                                        boxBarca.alignment(Pos.CENTER_LEFT);
                                                        boxBarca.setLayoutX(ChronoUnit.DAYS.between(inicioPeriodo, barca.getDtenvio()) * 40);
                                                        boxBarca.addStyle(barca.getStatus().equals("A")
                                                                ? "primary"
                                                                : barca.getStatus().equals("C")
                                                                ? "success"
                                                                : "warning");
                                                        boxBarca.add(FormBox.create(boxDadosBarca -> {
                                                            boxDadosBarca.horizontal();
                                                            boxDadosBarca.expanded();
                                                            boxDadosBarca.alignment(Pos.CENTER_LEFT);
                                                            boxDadosBarca.add(FormLabel.create(label -> {
                                                                label.alignment(Pos.CENTER_LEFT);
                                                                label.boldText();
                                                                label.value.set("Prog: " + barca.getProgramacao().getProgramacao() +
                                                                        " #" + barca.getOrdem() +
                                                                        " Cor: " + barca.getProgramacao().getCorbarca());
                                                            }).build());
                                                            boxDadosBarca.add(FormButton.create(btnInfosBarca -> {
                                                                btnInfosBarca.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                                                btnInfosBarca.icon(ImageUtils.getIcon(ImageUtils.Icon.ALERT_INFO, ImageUtils.IconSize._16));
                                                                btnInfosBarca.tooltip("Exibir informações da barca");
                                                                btnInfosBarca.addStyle("xs").addStyle("info");
                                                                btnInfosBarca.setAction(evt -> exibirInformacoesBarca(barca));
                                                            }));
                                                            //boxDadosBarca.add(FormButton.create(btnInfosBarca -> {
                                                            //    btnInfosBarca.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                                            //   btnInfosBarca.icon(ImageUtils.getIcon(ImageUtils.Icon.EDIT, ImageUtils.IconSize._16));
                                                            //    btnInfosBarca.tooltip("Alterar configurações da barca");
                                                            //    btnInfosBarca.addStyle("xs").addStyle("secundary");
                                                            //    btnInfosBarca.setAction(evt -> editarBarca(barca));
                                                            //}));
                                                        }));
                                                    }));
                                                });
                                        boxBarcas.add(paneBarcas);
                                    });
                        }));
                    }));
                    ganttContent.add(new Separator(Orientation.HORIZONTAL));
                });
            }));
        }));
    }

    private void firmarBarca(SdBarcaProducao barca) {
        InputBox<LocalDate> dtEnvioBarca = null;
        do {
            dtEnvioBarca = InputBox.build(LocalDate.class, boxInput -> {
                boxInput.message("Informe a data de envio da barca para produção:");
                boxInput.showAndWait();
            });
        } while (dtEnvioBarca.value.get() == null);
        firmarBarca(barca, dtEnvioBarca.value.get());
        MessageBox.create(message -> {
            message.message("Barca firmada para produção.");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }

    public VBox init() {
        return FormBox.create(containerTab -> {
            containerTab.vertical();
            containerTab.expanded();
            containerTab.add(FormBox.create(boxFilter -> {
                boxFilter.horizontal();
                boxFilter.add(FormTitledPane.create(filter -> {
                    filter.filter();
                    filter.add(FormBox.create(boxFieldsFilter -> {
                        boxFieldsFilter.vertical();
                        boxFieldsFilter.add(fieldStatusBarca.build());
                        boxFieldsFilter.add(fieldFornecedor.build());
                    }));
                    filter.find.setOnAction(evt -> {
                        carregarGantt();
                    });
                    filter.clean.setOnAction(evt -> {
                        boxGantt.clear();
                        fieldFornecedor.clear();
                        fieldStatusBarca.select(0);
                    });
                }));
            }));
            containerTab.add(boxGantt);
        });
    }

    private void exibirInformacoesBarca(SdBarcaProducao barca) {
        FormBox fieldStatusBarca = FormBox.create(boxField -> {
            boxField.vertical();
            boxField.title("Status");
            boxField.width(120.0);
            boxField.addStyle(barca.getStatus().equals("P")
                    ? "danger"
                    : barca.getStatus().equals("F") ? "warning"
                    : barca.getStatus().equals("A") ? "primary"
                    : "success");
            boxField.add(FormLabel.create(label -> {
                label.width(120.0);
                label.alignment(Pos.CENTER);
                label.value.set(barca.getStatus().equals("P")
                        ? "Pendente"
                        : barca.getStatus().equals("F") ? "Firmada"
                        : barca.getStatus().equals("A") ? "Analisada"
                        : "Concluída");
            }).build());
        });
        FormBox fieldCorBarca = FormBox.create(boxField -> {
            boxField.vertical();
            boxField.expanded();
            boxField.title("Cor Barca");
            boxField.add(FormLabel.create(label -> {
                label.value.set(((Cor) new FluentDao().selectFrom(Cor.class)
                        .where(eb -> eb.equal("cor", barca.getProgramacao().getCorbarca()))
                        .singleResult()).toString());
            }).build());
        });
        FormBox fieldDataProgramacao = FormBox.create(boxField -> {
            boxField.vertical();
            boxField.title("Dt. Prog.");
            boxField.add(FormLabel.create(label -> {
                label.alignment(Pos.CENTER);
                label.value.set(StringUtils.toDateFormat(barca.getProgramacao().getDtprogramacao()));
            }).build());
        });
        FormBox fieldProgramador = FormBox.create(boxField -> {
            boxField.vertical();
            boxField.addStyle("warning");
            boxField.title("Prog. Por");
            boxField.add(FormLabel.create(label -> {
                label.width(180.0);
                label.value.set(barca.getProgramacao().getUsuario());
            }).build());
        });
        FormBox fieldLoteProgramacao = FormBox.create(boxField -> {
            boxField.vertical();
            boxField.title("Lote Prog.");
            boxField.add(FormLabel.create(label -> {
                label.width(70.0);
                label.alignment(Pos.CENTER);
                label.value.set(barca.getProgramacao().getLote());
            }).build());
        });
        FormBox fieldMaquinaBarca = FormBox.create(boxField -> {
            boxField.vertical();
            boxField.title("Máquina");
            boxField.addStyle("info");
            boxField.add(FormLabel.create(label -> {
                label.width(180.0);
                label.value.set(barca.getMaquina().getMaquina());
            }).build());
        });
        FormBox fieldMaximoMaquinaBarca = FormBox.create(boxField -> {
            boxField.vertical();
            boxField.title("Máx.");
            boxField.add(FormLabel.create(label -> {
                label.width(50.0);
                label.alignment(Pos.CENTER);
                label.value.set(String.valueOf(barca.getMaquina().getMaximo()));
            }).build());
        });
        FormBox fieldMinimoMaquinaBarca = FormBox.create(boxField -> {
            boxField.vertical();
            boxField.title("Min.");
            boxField.add(FormLabel.create(label -> {
                label.width(50.0);
                label.alignment(Pos.CENTER);
                label.value.set(String.valueOf(barca.getMaquina().getMinimo()));
            }).build());
        });
        FormBox fieldPrazoMaquinaBarca = FormBox.create(boxField -> {
            boxField.vertical();
            boxField.title("Prazo.");
            boxField.add(FormLabel.create(label -> {
                label.width(50.0);
                label.alignment(Pos.CENTER);
                label.value.set(String.valueOf(barca.getMaquina().getPrazo()));
            }).build());
        });
        FormBox fieldQtdeBarca = FormBox.create(boxField -> {
            boxField.vertical();
            boxField.title("Qtde.");
            boxField.add(FormLabel.create(label -> {
                label.width(90.0);
                label.alignment(Pos.CENTER_RIGHT);
                label.value.set(String.valueOf(StringUtils.toDecimalFormat(barca.getQtde().doubleValue(), 3)));
            }).build());
        });
        FormBox fieldDataEnvio = FormBox.create(boxField -> {
            boxField.vertical();
            boxField.title("Dt. Env.");
            boxField.add(FormLabel.create(label -> {
                label.alignment(Pos.CENTER);
                label.width(80.0);
                label.value.set(barca.getDtenvio() != null ? StringUtils.toDateFormat(barca.getDtenvio()) : "-");
            }).build());
        });
        FormBox fieldDataRetorno = FormBox.create(boxField -> {
            boxField.vertical();
            boxField.title("Dt. Ret.");
            boxField.add(FormLabel.create(label -> {
                label.alignment(Pos.CENTER);
                label.width(80.0);
                label.value.set(barca.getDtretorno() != null ? StringUtils.toDateFormat(barca.getDtretorno()) : "-");
            }).build());
        });
        FormBox fieldNumeroOtn = FormBox.create(boxField -> {
            boxField.vertical();
            boxField.title("OTN");
            boxField.addStyle("warning");
            boxField.add(FormLabel.create(label -> {
                label.alignment(Pos.CENTER);
                label.width(70.0);
                label.value.set(barca.getNumero());
            }).build());
        });
        FormTableView<SdItensBarca> tblItensBarca = FormTableView.create(SdItensBarca.class, table -> {
            table.title("Itens Barca");
            table.expanded();
            table.items.set(FXCollections.observableList(barca.getItens()));
            table.columns(
                    FormTableColumn.create(cln -> {
                        cln.title("O.F.");
                        cln.width(55.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdItensBarca, SdItensBarca>, ObservableValue<SdItensBarca>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumeroof()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*O.F.*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Lote");
                        cln.width(35.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdItensBarca, SdItensBarca>, ObservableValue<SdItensBarca>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLoteof()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*Lote*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Material");
                        cln.width(180.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdItensBarca, SdItensBarca>, ObservableValue<SdItensBarca>>) param -> new ReadOnlyObjectWrapper(param.getValue().getInsumo()));
                    }).build() /*Material*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Depósito");
                        cln.width(50.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdItensBarca, SdItensBarca>, ObservableValue<SdItensBarca>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDeposito()));
                    }).build() /*Material*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Qtde Estq");
                        cln.width(80.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdItensBarca, SdItensBarca>, ObservableValue<SdItensBarca>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                        cln.alignment(Pos.CENTER_RIGHT);
                        cln.format(param -> {
                            return new TableCell<SdItensBarca, BigDecimal>() {
                                @Override
                                protected void updateItem(BigDecimal item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    if (item != null && !empty) {
                                        setText(StringUtils.toDecimalFormat(item.doubleValue(), 3));
                                    }
                                }
                            };
                        });
                    }).build() /*Qtde Est.*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Qtde");
                        cln.width(75.0);
                        cln.hide();
                        cln.value((Callback<TableColumn.CellDataFeatures<SdItensBarca, SdItensBarca>, ObservableValue<SdItensBarca>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                        cln.alignment(Pos.CENTER_RIGHT);
                        cln.format(param -> {
                            return new TableCell<SdItensBarca, BigDecimal>() {
                                @Override
                                protected void updateItem(BigDecimal item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    if (item != null && !empty) {
                                        setText(StringUtils.toDecimalFormat(item.doubleValue(), 3));
                                    }
                                }
                            };
                        });
                    }).build() /*Qtde*/
            );
            table.factoryRow(param -> {
                return new TableRow<SdItensBarca>() {
                    @Override
                    protected void updateItem(SdItensBarca item, boolean empty) {
                        super.updateItem(item, empty);
                        getStyleClass().remove("table-row-success");
                        if (item != null && !empty && item.isPrincipal()) {
                            getStyleClass().add("table-row-success");
                        }
                    }
                };
            });
        });
        new Fragment().show(fragment -> {
            fragment.title("Informações Barca");
            fragment.size(450.0, 600.0);
            fragment.box.getChildren().add(FormBox.create(containerFragment -> {
                containerFragment.vertical();
                containerFragment.expanded();
                containerFragment.add(FormBox.create(box -> {
                    box.horizontal();
                    box.add(fieldStatusBarca);
                    box.add(fieldCorBarca);
                }));
                containerFragment.add(FormBox.create(boxDadosProg -> {
                    boxDadosProg.vertical();
                    boxDadosProg.title("Programação");
                    boxDadosProg.add(FormBox.create(box -> {
                        box.horizontal();
                        box.add(fieldProgramador);
                        box.add(fieldLoteProgramacao, fieldDataProgramacao);
                    }));
                }));
                containerFragment.add(FormBox.create(boxDadosBarca -> {
                    boxDadosBarca.vertical();
                    boxDadosBarca.title("Barca");
                    boxDadosBarca.add(FormBox.create(box -> {
                        box.horizontal();
                        box.add(fieldMaquinaBarca);
                        box.add(fieldMinimoMaquinaBarca, fieldMaximoMaquinaBarca, fieldPrazoMaquinaBarca);
                    }));
                    boxDadosBarca.add(FormBox.create(box -> {
                        box.horizontal();
                        box.add(fieldQtdeBarca, fieldNumeroOtn);
                        box.add(fieldDataEnvio, fieldDataRetorno);
                    }));
                }));
                containerFragment.add(FormBox.create(box -> {
                    box.horizontal();
                    if (barca.getStatus().equals("F")) {
                        box.add(FormButton.create(btn -> {
                            btn.title("Desprogramar Barca");
                            btn.addStyle("warning");
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.LIMPAR, ImageUtils.IconSize._24));
                            btn.setAction(evt -> {
                                cancelarProgramacaoBarca(barca);
                                MessageBox.create(message -> {
                                    message.message("Barca desprogramada com sucesso");
                                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                                    message.position(Pos.TOP_RIGHT);
                                    message.notification();
                                });
                                fragment.close();
                                carregarGantt();
                            });
                        }));
                        box.add(FormButton.create(btn -> {
                            btn.title("Alterar Envio");
                            btn.addStyle("info");
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ENVIAR, ImageUtils.IconSize._24));
                            btn.setAction(evt -> {
                                InputBox<LocalDate> dtEnvioBarca = null;
                                do {
                                    dtEnvioBarca = InputBox.build(LocalDate.class, boxInput -> {
                                        boxInput.message("Informe a data de envio da barca para produção:");
                                        boxInput.showAndWait();
                                    });
                                } while (dtEnvioBarca.value.get() == null);
                                firmarBarca(barca, dtEnvioBarca.value.get());
                                MessageBox.create(message -> {
                                    message.message("Alterada a data de envio da barca com sucesso.");
                                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                                    message.position(Pos.TOP_RIGHT);
                                    message.notification();
                                });
                                fragment.close();
                                carregarGantt();
                            });
                        }));
                    }
                    if (barca.getStatus().matches("(P|F)"))
                        box.add(FormButton.create(btn -> {
                            btn.title("Remover Barca");
                            btn.addStyle("danger");
                            btn.icon(ImageUtils.getIcon(ImageUtils.Icon.TRASH, ImageUtils.IconSize._24));
                            btn.setAction(evt -> {
                                if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                                        message.message("Deseja realmente excluir a barca selecionada?\n" +
                                                "Esse processo irá excluir a programação da barca e é irreverssível.\n" +
                                                "Será necessário retornar a tela de tinturaria para uma nova programação, " +
                                                "porém não será descontado as barcas já programadas.");
                                        message.showAndWait();
                                    }).value.get())) {
                                    try {
                                        excluirBarca(barca);
                                        MessageBox.create(message -> {
                                            message.message("Barca excluída com sucesso");
                                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                                            message.position(Pos.TOP_RIGHT);
                                            message.notification();
                                        });
                                        carregarGantt();
                                    } catch (SQLException throwables) {
                                        throwables.printStackTrace();
                                        ExceptionBox.build(message -> {
                                            message.exception(throwables);
                                            message.showAndWait();
                                        });
                                    }
                                }
                                fragment.close();
                            });
                        }));
                }));
                containerFragment.add(tblItensBarca.build());
            }));
        });
    }

    private void editarBarca(SdBarcaProducao barca) {

    }

}
