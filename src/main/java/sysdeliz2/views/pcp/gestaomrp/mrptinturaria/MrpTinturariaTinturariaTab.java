package sysdeliz2.views.pcp.gestaomrp.mrptinturaria;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import sysdeliz2.controllers.views.pcp.gestaomrp.MrpTinturariaTecelagemController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.sysdeliz.pcp.SdBarcaFornecedor;
import sysdeliz2.models.sysdeliz.pcp.SdBarcaProducao;
import sysdeliz2.models.sysdeliz.pcp.SdItensBarca;
import sysdeliz2.models.sysdeliz.pcp.SdProgramacaoBarca;
import sysdeliz2.models.ti.*;
import sysdeliz2.models.ti.materiais.Cadcorti;
import sysdeliz2.models.ti.materiais.Cadmatti;
import sysdeliz2.models.view.VSdConsumosTinturaria;
import sysdeliz2.models.view.VSdOfsMrpTinturaria;
import sysdeliz2.models.view.mrptinturaria.VSdMrpTinturariaV;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.exceptions.BreakException;
import sysdeliz2.utils.exceptions.ContinueException;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.InputBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.gui.window.GenericSelect;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;
import sysdeliz2.views.pcp.gestaomrp.MrpTinturariaTecelagemView;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MrpTinturariaTinturariaTab extends MrpTinturariaTecelagemController {

    private MrpTinturariaTecelagemView view = null;

    // <editor-fold defaultstate="collapsed" desc="bean">
    private final ObjectProperty<BigDecimal> calculadoraSomaLotes = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> calculadoraSomaCores = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> calculadoraSomaSelecao = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> calculadoraSomaOutrosLotes = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> calculadoraSomaCoresOutrosLotes = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> calculadoraSomaSelecaoOutrosLotes = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final StringProperty unidadeCalculadoraSomaLote = new SimpleStringProperty("");
    private final StringProperty unidadeCalculadoraSomaCor = new SimpleStringProperty("");
    private final StringProperty unidadeCalculadoraSomaSelecao = new SimpleStringProperty("");
    private final ObjectProperty<BigDecimal> calculadoraSomaSobrasSelecao = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> acumuladoSobrasEstoque = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> acumuladoSobrasCompra = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> acumuladoNecessidadesEstoque = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> acumuladoNecessidadesCompra = new SimpleObjectProperty<>(BigDecimal.ZERO);
    private final ObjectProperty<BigDecimal> acumuladoNecessidadesFalta = new SimpleObjectProperty<>(BigDecimal.ZERO);
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="fields">
    private final FormFieldToggleSingle fieldSomenteFalta = FormFieldToggleSingle.create(field -> {
        field.title("Somente a comprar:");
        field.value.set(true);
        field.value.addListener((observable, oldValue, newValue) -> {
            clearDados();
            getLotesTinturariaMrp(newValue, false,
                    this.fieldMaterialPeriodoCor.objectValues.stream().map(Material::getCodigo).toArray());
            this.fieldLotes.items.set(FXCollections.observableList(super.lotesTinturariaMrp));
        });
    });
    private final FormListView<String> fieldLotes = FormListView.create(field -> {
        field.title("Lotes");
        field.multipleSelection();
        field.withoutCounter();
        field.expanded();
        field.width(100.0);
        field.height(180.0);
        field.listProperties().getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                selecionarLoteMrp(field.selectedRegisters().toArray());
            }
        });
    });
    private final FormListView<Cor> fieldCoresMaterial = FormListView.create(field -> {
        field.title("Cores");
        field.multipleSelection();
        field.withoutCounter();
        field.width(200.0);
        field.height(180.0);
        field.listProperties().getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                selecionarCorMrp((List<Cor>) field.selectedRegisters(),
                        this.fieldMaterial.objectValues.size() == 0
                                ? new String[]{}
                                : this.fieldMaterial.objectValues.get().stream().map(Material::getCodigo).toArray(),
                        this.fieldFornecedor.objectValues.size() == 0
                                ? new String[]{}
                                : this.fieldFornecedor.objectValues.get().stream().map(Entidade::getCodcli).toArray(),
                        this.fieldDeposito.objectValues.size() == 0
                                ? new String[]{}
                                : this.fieldDeposito.objectValues.stream().map(Deposito::getCodigo).toArray());
            }
        });
    });
    private final FormFieldMultipleFind<Material> fieldMaterialPeriodoCor = FormFieldMultipleFind.create(Material.class, field -> {
        field.title("Material");
        field.width(250.0);
        field.postSelected.addListener((observable, oldValue, newValue) -> {
            clearDados();
            fieldCoresMaterial.clear();
            getLotesTinturariaMrp(fieldSomenteFalta.value.get(), false,
                    this.fieldMaterialPeriodoCor.objectValues.stream().map(Material::getCodigo).toArray());
            this.fieldLotes.items.set(FXCollections.observableList(super.lotesTinturariaMrp));
            this.fieldMaterial.objectValues.set(field.objectValues);
        });
    });
    private final FormFieldMultipleFind<Material> fieldMaterial = FormFieldMultipleFind.create(Material.class, field -> {
        field.title("Material");
        field.width(300.0);
    });
    private final FormFieldMultipleFind<Entidade> fieldFornecedor = FormFieldMultipleFind.create(Entidade.class, field -> {
        field.title("Fornecedor");
        field.width(200.0);
    });
    private final FormFieldMultipleFind<Deposito> fieldDeposito = FormFieldMultipleFind.create(Deposito.class, field -> {
        field.title("Depósito");
        field.width(150.0);
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="tables">
    private final FormTableView<VSdMrpTinturariaV> tblMateriaisMrp = FormTableView.create(VSdMrpTinturariaV.class, table -> {
        table.withoutTitle();
        table.expanded();
        table.multipleSelection();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("");
                    cln.width(20.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().isSelected()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<VSdMrpTinturariaV, Boolean>() {
                            @Override
                            protected void updateItem(Boolean item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    if (item) {
                                        setGraphic(ImageUtils.getIcon(ImageUtils.Icon.ALERT_INFO, ImageUtils.IconSize._16));
                                    }
                                }
                            }
                        };
                    });
                }).build() /**/,
                FormTableColumn.create(cln -> {
                    cln.title("Material");
                    cln.width(270.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<VSdMrpTinturariaV, VSdMrpTinturariaV>() {
                            @Override
                            protected void updateItem(VSdMrpTinturariaV item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setText(item.getInsumo().toString());
                                    HBox indicadoresMaterial = new HBox();
                                    if (item.isPrincipal())
                                        indicadoresMaterial.getChildren().add(ImageUtils.getIcon(ImageUtils.Icon.ALERT_SUCCESS, ImageUtils.IconSize._16));
                                    if (item.isContrastante())
                                        indicadoresMaterial.getChildren().add(ImageUtils.getIcon(ImageUtils.Icon.COR, ImageUtils.IconSize._16));
                                    if (item.isEmBarca())
                                        indicadoresMaterial.getChildren().add(ImageUtils.getIcon(ImageUtils.Icon.LAVANDERIA, ImageUtils.IconSize._16));

                                    if (indicadoresMaterial.getChildren().size() > 0)
                                        setGraphic(indicadoresMaterial);
                                }
                            }
                        };
                    });
                }).build() /*Material*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cor Mat.");
                    cln.width(160.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCorinsumo()));
                }).build() /*Cor Mat.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Dep.");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDeposito()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Dep.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Consumo OF");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getConsumoof()));
                    cln.alignment(Pos.CENTER_RIGHT);
                    cln.hide();
                    cln.format(param -> {
                        return new TableCell<VSdMrpTinturariaV, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 3));
                                }
                            }
                        };
                    });
                }).build() /*Consumo OF*/,
                FormTableColumn.create(cln -> {
                    cln.title("Consumido");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getConsumido()));
                    cln.alignment(Pos.CENTER_RIGHT);
                    cln.format(param -> {
                        return new TableCell<VSdMrpTinturariaV, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 3));
                                }
                            }
                        };
                    });
                }).build() /*Consumido*/,
                FormTableColumn.create(cln -> {
                    cln.title("Un");
                    cln.width(25.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getUnidade()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Un*/,
                FormTableColumn.create(cln -> {
                    cln.title("Fornec.");
                    cln.width(180.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getForinsumo()));
                }).build() /*Fornec.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cons. Estq.");
                    cln.width(90.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getConsumoestq()));
                    cln.alignment(Pos.CENTER_RIGHT);
                    cln.format(param -> {
                        return new TableCell<VSdMrpTinturariaV, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 3));
                                }
                            }
                        };
                    });
                }).build() /*Cons. Estq.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Un");
                    cln.width(25.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getUnidadeestq()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Un*/,
                FormTableColumn.create(cln -> {
                    cln.title("Ord. Comp.");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getOrdcompra()));
                    cln.alignment(Pos.CENTER);
                    cln.hide();
                }).build() /*Ord. Comp.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Entrega");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDtentrega()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<VSdMrpTinturariaV, LocalDate>() {
                            @Override
                            protected void updateItem(LocalDate item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDateFormat(item));
                                }
                            }
                        };
                    });
                    cln.hide();
                }).build() /*Entrega*/
        );
        table.factoryRow(param -> {
            return new TableRow<VSdMrpTinturariaV>() {
                @Override
                protected void updateItem(VSdMrpTinturariaV item, boolean empty) {
                    super.updateItem(item, empty);
                    getStyleClass().removeAll("table-row-warning", "table-row-success", "table-row-danger");
                    if (item != null && !empty) {
                        getStyleClass().add(item.getConsumidode().equals("A")
                                ? "table-row-danger"
                                : item.getConsumidode().equals("B") ? "table-row-warning" : "table-row-success");
                        setOnMouseClicked(event -> {
                            if (event.getClickCount() > 1) {
                                if (fieldMaterial.objectValues.get().contains(item.getInsumo())) {
                                    fieldMaterial.objectValues.get().remove(item.getInsumo());
                                } else {
                                    fieldMaterial.objectValues.get().add(item.getInsumo());
                                }
                                selecionarCorMrp(fieldCoresMaterial.selectedRegisters(),
                                        fieldMaterial.objectValues.size() == 0
                                                ? new String[]{}
                                                : fieldMaterial.objectValues.get().stream().map(Material::getCodigo).toArray(),
                                        fieldFornecedor.objectValues.size() == 0
                                                ? new String[]{}
                                                : fieldFornecedor.objectValues.get().stream().map(Entidade::getCodcli).toArray(),
                                        fieldDeposito.objectValues.size() == 0
                                                ? new String[]{}
                                                : fieldDeposito.objectValues.stream().map(Deposito::getCodigo).toArray());
                            }
                        });
                    }
                }
            };
        });
        table.indices(
                table.indice("Estoque", "success", ""),
                table.indice("Compra", "warning", ""),
                table.indice("A Comprar", "danger", "")
        );
        table.selectionModelItem((observable, oldValue, newValue) -> {
            if (newValue != null) {
                carregaDadosMaterialSelecionado((VSdMrpTinturariaV) table.selectedItem());
                //calculaMateriasSelecionados((List<VSdMrpTinturariaV>) table.selectedRegisters());
            }
        });
    });
    private final FormTableView<VSdMrpTinturariaV> tblNecessidadesMaterialMrp = FormTableView.create(VSdMrpTinturariaV.class, table -> {
        table.withoutTitle();
        table.expanded();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Material");
                    cln.width(270.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getInsumo()));
                }).build() /*Material*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cor Mat.");
                    cln.width(160.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCorinsumo()));
                }).build() /*Cor Mat.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Dep.");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDeposito()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Dep.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Consumo OF");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getConsumoof()));
                    cln.alignment(Pos.CENTER_RIGHT);
                    cln.hide();
                    cln.format(param -> {
                        return new TableCell<VSdMrpTinturariaV, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 3));
                                }
                            }
                        };
                    });
                }).build() /*Consumo OF*/,
                FormTableColumn.create(cln -> {
                    cln.title("Consumido");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getConsumido()));
                    cln.alignment(Pos.CENTER_RIGHT);
                    cln.format(param -> {
                        return new TableCell<VSdMrpTinturariaV, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 3));
                                }
                            }
                        };
                    });
                    cln.hide();
                }).build() /*Consumido*/,
                FormTableColumn.create(cln -> {
                    cln.title("Un");
                    cln.width(25.0);
                    cln.hide();
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getUnidade()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Un*/,
                FormTableColumn.create(cln -> {
                    cln.title("Fornec.");
                    cln.width(180.0);
                    cln.hide();
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getForinsumo()));
                }).build() /*Fornec.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cons. Estq.");
                    cln.width(90.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getConsumoestq()));
                    cln.alignment(Pos.CENTER_RIGHT);
                    cln.format(param -> {
                        return new TableCell<VSdMrpTinturariaV, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 3));
                                }
                            }
                        };
                    });
                }).build() /*Cons. Estq.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Un");
                    cln.width(25.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getUnidadeestq()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Un*/,
                FormTableColumn.create(cln -> {
                    cln.title("Ord. Comp.");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getOrdcompra()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Ord. Comp.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Entrega");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDtentrega()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<VSdMrpTinturariaV, LocalDate>() {
                            @Override
                            protected void updateItem(LocalDate item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDateFormat(item));
                                }
                            }
                        };
                    });
                }).build() /*Entrega*/
        );
        table.factoryRow(param -> {
            return new TableRow<VSdMrpTinturariaV>() {
                @Override
                protected void updateItem(VSdMrpTinturariaV item, boolean empty) {
                    super.updateItem(item, empty);
                    getStyleClass().removeAll("table-row-warning", "table-row-success", "table-row-danger");
                    if (item != null && !empty) {
                        getStyleClass().add(item.getConsumidode().equals("A") ? "table-row-danger" : item.getConsumidode().equals("B") ? "table-row-warning" : "table-row-success");
                    }
                }
            };
        });
        table.selectionModelItem((observable, oldValue, newValue) -> {
            if (newValue != null) {
                List<VSdMrpTinturariaV> materiaisPai = getMateriaisPai(false,
                        ((VSdMrpTinturariaV) newValue).getInsumo().getCodigo(),
                        ((VSdMrpTinturariaV) newValue).getCorinsumo().getCor());
                this.tblMateriaisMrp.items.forEach(item -> {
                    item.setSelected(false);
                    if (materiaisPai.stream().map(VSdMrpTinturariaV::getMaterialpai).distinct().anyMatch(insumo -> insumo.equals(item.getInsumo().getCodigo())) &&
                            materiaisPai.stream().map(VSdMrpTinturariaV::getCormaterialpai).distinct().anyMatch(insumo -> insumo.equals(item.getCorinsumo().getCor())))
                        item.setSelected(true);
                });
                this.tblMateriaisMrp.refresh();
            }
        });
    });
    private final FormTableView<VSdOfsMrpTinturaria> tblOrigemMateriaisMrp = FormTableView.create(VSdOfsMrpTinturaria.class, table -> {
        table.withoutTitle();
        table.expanded();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Lote");
                    cln.width(40.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdOfsMrpTinturaria, VSdOfsMrpTinturaria>, ObservableValue<VSdOfsMrpTinturaria>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLoteOf()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Lote*/,
                FormTableColumn.create(cln -> {
                    cln.title("OF");
                    cln.width(55.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdOfsMrpTinturaria, VSdOfsMrpTinturaria>, ObservableValue<VSdOfsMrpTinturaria>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumeroof().getNumero()));
                    cln.alignment(Pos.CENTER);
                }).build() /*OF*/,
                FormTableColumn.create(cln -> {
                    cln.title("Qtde");
                    cln.width(40.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdOfsMrpTinturaria, VSdOfsMrpTinturaria>, ObservableValue<VSdOfsMrpTinturaria>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<VSdOfsMrpTinturaria, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toIntegerFormat(item.intValue()));
                                }
                            }
                        };
                    });
                }).build() /*Qtde*/,
                FormTableColumn.create(cln -> {
                    cln.title("Produto");
                    cln.width(210.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdOfsMrpTinturaria, VSdOfsMrpTinturaria>, ObservableValue<VSdOfsMrpTinturaria>>) param -> new ReadOnlyObjectWrapper(param.getValue().getProduto()));
                }).build() /*Produto*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cor");
                    cln.width(120.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdOfsMrpTinturaria, VSdOfsMrpTinturaria>, ObservableValue<VSdOfsMrpTinturaria>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCorproduto()));
                }).build() /*Cor*/,
                FormTableColumn.create(cln -> {
                    cln.title("Estoque");
                    cln.getStyleClass().add("header-success");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdOfsMrpTinturaria, VSdOfsMrpTinturaria>, ObservableValue<VSdOfsMrpTinturaria>>) param -> new ReadOnlyObjectWrapper(param.getValue().getEstoque()));
                    cln.alignment(Pos.CENTER_RIGHT);
                    cln.format(param -> {
                        return new TableCell<VSdOfsMrpTinturaria, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                getStyleClass().removeAll("table-row-success", "border-cell");
                                if (item != null && !empty) {
                                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 3));
                                    getStyleClass().addAll("table-row-success", "border-cell");
                                }
                            }
                        };
                    });
                }).build() /*Estoque*/,
                FormTableColumn.create(cln -> {
                    cln.title("Compra");
                    cln.getStyleClass().add("header-warning");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdOfsMrpTinturaria, VSdOfsMrpTinturaria>, ObservableValue<VSdOfsMrpTinturaria>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCompra()));
                    cln.alignment(Pos.CENTER_RIGHT);
                    cln.format(param -> {
                        return new TableCell<VSdOfsMrpTinturaria, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                getStyleClass().removeAll("table-row-warning", "border-cell");
                                if (item != null && !empty) {
                                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 3));
                                    getStyleClass().addAll("table-row-warning", "border-cell");
                                }
                            }
                        };
                    });
                }).build() /*Compra*/,
                FormTableColumn.create(cln -> {
                    cln.title("A Comprar");
                    cln.getStyleClass().add("header-danger");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdOfsMrpTinturaria, VSdOfsMrpTinturaria>, ObservableValue<VSdOfsMrpTinturaria>>) param -> new ReadOnlyObjectWrapper(param.getValue().getAcomprar()));
                    cln.alignment(Pos.CENTER_RIGHT);
                    cln.format(param -> {
                        return new TableCell<VSdOfsMrpTinturaria, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                getStyleClass().removeAll("table-row-danger", "border-cell");
                                if (item != null && !empty) {
                                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 3));
                                    getStyleClass().addAll("table-row-danger", "border-cell");
                                }
                            }
                        };
                    });
                }).build() /*A Comprar*/
        );
        table.factoryRow(param -> {
            return new TableRow<VSdOfsMrpTinturaria>() {
                @Override
                protected void updateItem(VSdOfsMrpTinturaria item, boolean empty) {
                    super.updateItem(item, empty);
                    getStyleClass().removeAll("table-row-danger", "table-row-warning", "table-row-success");
                    if (item != null && !empty) {
                        getStyleClass().add(item.getAcomprar().compareTo(BigDecimal.ZERO) > 0 ? "table-row-danger" : item.getCompra().compareTo(BigDecimal.ZERO) > 0 ? "table-row-warning" : "table-row-success");
                    }
                }
            };
        });
        table.selectionModelItem((observable, oldValue, newValue) -> {
            if (newValue != null) {
                List<VSdMrpTinturariaV> materiaisFilho = getMateriaisFilho(false,
                        ((VSdOfsMrpTinturaria) newValue).getInsumo(),
                        ((VSdOfsMrpTinturaria) newValue).getCorinsumo());
                List<Object> materiaisOfs = (List<Object>) new FluentDao().selectFrom(VSdMrpTinturariaV.class)
                        .distinctColumn("insumo.codigo")
                        .where(eb -> eb
                                .isNotNull("insumo")
                                .equal("principal", ((VSdOfsMrpTinturaria) newValue).isPrincipal())
                                .equal("numeroof", ((VSdOfsMrpTinturaria) newValue).getNumeroof().getNumero())
                                .equal("cormaterialpai", ((VSdOfsMrpTinturaria) newValue).getCorproduto().getCor()))
                        .resultList();
                this.tblMateriaisMrp.items.forEach(item -> {
                    item.setSelected(false);
                    if (materiaisOfs.stream().anyMatch(insumo -> insumo.equals(item.getInsumo().getCodigo())
                            && item.isPrincipal() == ((VSdOfsMrpTinturaria) newValue).isPrincipal()))
                        item.setSelected(true);
                });
                this.tblMateriaisMrp.refresh();
            }
        });
    });
    private final FormTableView<VSdMrpTinturariaV> tblSobrasMateriaisMrp = FormTableView.create(VSdMrpTinturariaV.class, table -> {
        table.withoutTitle();
        table.expanded();
        table.multipleSelection();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Material");
                    cln.width(270.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getInsumo()));
                }).build() /*Material*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cor Mat.");
                    cln.width(160.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCorinsumo()));
                }).build() /*Cor Mat.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Dep.");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDeposito()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Dep.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Qtde");
                    cln.width(80.0);
                    cln.hide();
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getConsumido()));
                    cln.alignment(Pos.CENTER_RIGHT);
                    cln.format(param -> {
                        return new TableCell<VSdMrpTinturariaV, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 3));
                                }
                            }
                        };
                    });
                }).build() /*Qtde*/,
                FormTableColumn.create(cln -> {
                    cln.title("Un");
                    cln.width(25.0);
                    cln.hide();
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getUnidade()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Un*/,
                FormTableColumn.create(cln -> {
                    cln.title("Fornec.");
                    cln.width(180.0);
                    cln.hide();
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getForinsumo()));
                }).build() /*Fornec.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Qtde Estq.");
                    cln.width(90.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getConsumoestq()));
                    cln.alignment(Pos.CENTER_RIGHT);
                    cln.format(param -> {
                        return new TableCell<VSdMrpTinturariaV, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 3));
                                }
                            }
                        };
                    });
                }).build() /*Qtde Estq.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Un");
                    cln.width(25.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getUnidadeestq()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Un*/,
                FormTableColumn.create(cln -> {
                    cln.title("Ord. Comp.");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getOrdcompra()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Ord. Comp.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Entrega");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDtentrega()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<VSdMrpTinturariaV, LocalDate>() {
                            @Override
                            protected void updateItem(LocalDate item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDateFormat(item));
                                }
                            }
                        };
                    });
                }).build() /*Entrega*/
        );
        table.factoryRow(param -> {
            return new TableRow<VSdMrpTinturariaV>() {
                @Override
                protected void updateItem(VSdMrpTinturariaV item, boolean empty) {
                    super.updateItem(item, empty);
                    getStyleClass().removeAll("table-row-warning", "table-row-success", "table-row-danger");
                    if (item != null && !empty) {
                        getStyleClass().add(item.getConsumidode().equals("A") ? "table-row-danger" : item.getConsumidode().equals("B") ? "table-row-warning" : "table-row-success");
                    }
                }
            };
        });
        table.selectionModelItem((observable, oldValue, newValue) -> {
            if (newValue != null) {
                calculaSobrasMateriasSelecionados((List<VSdMrpTinturariaV>) table.selectedRegisters());
            }
        });
    });
    // </editor-fold>

    public MrpTinturariaTinturariaTab(MrpTinturariaTecelagemView view) {
        this.view = view;
    }

    public void loadLotes() {
        getLotesTinturariaMrp(true, false,  new Object[]{});
    }

    public void initLotes() {
        fieldLotes.items.set(FXCollections.observableList(super.lotesTinturariaMrp));
    }

    public VBox init() {
        return FormBox.create(container -> {
            container.horizontal();
            container.expanded();
            container.add(FormBox.create(boxFiltros -> {
                boxFiltros.vertical();
                boxFiltros.title("Filtros");
                boxFiltros.addStyle("light-primary");
                boxFiltros.add(fieldMaterialPeriodoCor.build());
                boxFiltros.add(FormBox.create(boxFieldsFiltros -> {
                    boxFieldsFiltros.horizontal();
                    boxFieldsFiltros.add(fieldSomenteFalta.build());
                }));
                boxFiltros.add(FormBox.create(boxFieldsFiltros -> {
                    boxFieldsFiltros.horizontal();
                    boxFieldsFiltros.expanded();
                    boxFieldsFiltros.add(fieldLotes.build());
                    boxFieldsFiltros.add(fieldCoresMaterial.build());
                }));
            }));
            container.add(FormBox.create(boxDadosMrp -> {
                boxDadosMrp.vertical();
                boxDadosMrp.expanded();
                boxDadosMrp.add(FormBox.create(boxDadosMateriais -> {
                    boxDadosMateriais.horizontal();
                    boxDadosMateriais.expanded();
                    boxDadosMateriais.add(FormBox.create(boxDadosOrigem -> {
                        boxDadosOrigem.vertical();
                        boxDadosOrigem.expanded();
                        boxDadosOrigem.title("Tinturaria");
                        boxDadosOrigem.addStyle("light-info");
                        boxDadosOrigem.add(FormBox.create(boxFilter -> {
                            boxFilter.horizontal();
                            boxFilter.add(FormTitledPane.create(filter -> {
                                filter.filter();
                                filter.collapse(false);
                                filter.add(FormBox.create(boxFieldsFilter -> {
                                    boxFieldsFilter.horizontal();
                                    boxFieldsFilter.add(fieldMaterial.build());
                                    boxFieldsFilter.add(fieldFornecedor.build());
                                    boxFieldsFilter.add(fieldDeposito.build());
                                }));
                                filter.find.setOnAction(evt -> {
                                    if (fieldCoresMaterial.selectedRegisters().size() == 0) {
                                        MessageBox.create(message -> {
                                            message.message("É necessário selecionar ao menos uma Cor na lista de cores.");
                                            message.type(MessageBox.TypeMessageBox.IMPORTANT);
                                            message.showAndWait();
                                        });
                                    } else {
                                        selecionarCorMrp(fieldCoresMaterial.selectedRegisters(),
                                                this.fieldMaterial.objectValues.size() == 0
                                                        ? new String[]{}
                                                        : this.fieldMaterial.objectValues.get().stream().map(Material::getCodigo).toArray(),
                                                this.fieldFornecedor.objectValues.size() == 0
                                                        ? new String[]{}
                                                        : this.fieldFornecedor.objectValues.stream().map(Entidade::getCodcli).toArray(),
                                                this.fieldDeposito.objectValues.size() == 0
                                                        ? new String[]{}
                                                        : this.fieldDeposito.objectValues.stream().map(Deposito::getCodigo).toArray());
                                    }
                                });
                                filter.clean.setOnAction(evt -> {
                                    fieldMaterial.clear();
                                    fieldFornecedor.clear();
                                    fieldDeposito.clear();
                                    if (fieldCoresMaterial.selectedRegisters().size() > 0) {
                                        selecionarCorMrp(fieldCoresMaterial.selectedRegisters(),
                                                this.fieldMaterial.objectValues.size() == 0
                                                        ? new String[]{}
                                                        : this.fieldMaterial.objectValues.get().stream().map(Material::getCodigo).toArray(),
                                                this.fieldFornecedor.objectValues.size() == 0
                                                        ? new String[]{}
                                                        : this.fieldFornecedor.objectValues.stream().map(Entidade::getCodcli).toArray(),
                                                this.fieldDeposito.objectValues.size() == 0
                                                        ? new String[]{}
                                                        : this.fieldDeposito.objectValues.stream().map(Deposito::getCodigo).toArray());
                                    }
                                });
                            }));
                        }));
                        boxDadosOrigem.add(tblMateriaisMrp.build());
                        boxDadosOrigem.add(FormBox.create(boxTotais -> {
                            boxTotais.horizontal();
                            boxTotais.add(FormBox.create(boxTotaisLote -> {
                                boxTotaisLote.vertical();
                                boxTotaisLote.title("Todas as cores");
                                boxTotaisLote.addStyle("default");
                                boxTotaisLote.add(FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.label("Total");
                                    field.value.bind(calculadoraSomaLotes.asString("%.3f").concat(" ")
                                            .concat(unidadeCalculadoraSomaLote));
                                    field.tooltip("Somatório dos consumos das cores dos lotes selecionados");
                                    field.width(180.0);
                                    field.alignment(Pos.CENTER_LEFT);
                                }).build());
                                boxTotaisLote.add(FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.label("Saldo");
                                    field.value.bind(calculadoraSomaOutrosLotes.asString("%.3f").concat(" ")
                                            .concat(unidadeCalculadoraSomaLote));
                                    field.tooltip("Somatório dos consumos das cores dos lotes não selecionados");
                                    field.width(180.0);
                                    field.alignment(Pos.CENTER_LEFT);
                                }).build());
                            }));
                            boxTotais.add(new Separator(Orientation.VERTICAL));
                            boxTotais.add(FormBox.create(boxTotaisCores -> {
                                boxTotaisCores.vertical();
                                boxTotaisCores.title("Cores selecionadas");
                                boxTotaisCores.addStyle("default");
                                boxTotaisCores.add(FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.label("Total");
                                    field.value.bind(calculadoraSomaCores.asString("%.3f").concat(" ").concat(unidadeCalculadoraSomaCor));
                                    field.tooltip("Somatório dos consumos das cores selecionadas nos lotes selecionados");
                                    field.width(180.0);
                                    field.alignment(Pos.CENTER_LEFT);
                                }).build());
                                boxTotaisCores.add(FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.label("Saldo");
                                    field.value.bind(calculadoraSomaCoresOutrosLotes.asString("%.3f").concat(" ").concat(unidadeCalculadoraSomaCor));
                                    field.tooltip("Somatório dos consumos das cores selecionadas nos lotes não selecionados");
                                    field.width(180.0);
                                    field.alignment(Pos.CENTER_LEFT);
                                }).build());
                            }));
                            boxTotais.add(FormBox.create(boxFooterMrp -> {
                                boxFooterMrp.vertical();
                                boxFooterMrp.expanded();
                                boxFooterMrp.alignment(Pos.TOP_RIGHT);
                                boxFooterMrp.add(FormButton.create(btnCriarBarcas -> {
                                    btnCriarBarcas.title("Criar Barcas");
                                    btnCriarBarcas.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._32));
                                    btnCriarBarcas.addStyle("success").addStyle("lg");
                                    btnCriarBarcas.setOnMouseClicked(event -> criarBarcas(
                                            Stream.concat(tblMateriaisMrp.selectedRegisters().stream()
                                                            .filter(VSdMrpTinturariaV::isPrincipal),
                                                    tblMateriaisMrp.items.stream()
                                                            .filter(VSdMrpTinturariaV::isContrastante))
                                                    .collect(Collectors.toList()),
                                            event.isControlDown(),
                                            this.fieldDeposito.objectValues.size() == 0
                                                    ? new String[]{}
                                                    : this.fieldDeposito.objectValues.stream().map(Deposito::getCodigo).toArray()));
                                    //btnCriarBarcas.setAction(evt -> );
                                }));
                            }));
                        }));
                    }));
                    boxDadosMateriais.add(FormBox.create(boxDadosSubniveis -> {
                        boxDadosSubniveis.vertical();
                        boxDadosSubniveis.width(700.0);
                        boxDadosSubniveis.add(FormBox.create(boxDadosNecessidades -> {
                            boxDadosNecessidades.vertical();
                            boxDadosNecessidades.title("Necessidades Material");
                            boxDadosNecessidades.addStyle("light-amber");
                            boxDadosNecessidades.expanded();
                            boxDadosNecessidades.add(tblNecessidadesMaterialMrp.build());
                            boxDadosNecessidades.add(FormBox.create(boxTotais -> {
                                boxTotais.horizontal();
                                boxTotais.add(FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.label("Estoque");
                                    field.value.bind(acumuladoNecessidadesEstoque.asString("%.3f"));
                                    field.addStyle("success");
                                    field.width(130.0);
                                    field.alignment(Pos.CENTER_RIGHT);
                                }).build());
                                boxTotais.add(FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.label("Compra");
                                    field.value.bind(acumuladoNecessidadesCompra.asString("%.3f"));
                                    field.addStyle("warning");
                                    field.width(130.0);
                                    field.alignment(Pos.CENTER_RIGHT);
                                }).build());
                                boxTotais.add(FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.label("A Comprar");
                                    field.value.bind(acumuladoNecessidadesFalta.asString("%.3f"));
                                    field.addStyle("danger");
                                    field.width(130.0);
                                    field.alignment(Pos.CENTER_RIGHT);
                                }).build());
                            }));
                        }));
                        boxDadosSubniveis.add(FormBox.create(boxDadosOrigemMaterial -> {
                            boxDadosOrigemMaterial.vertical();
                            boxDadosOrigemMaterial.title("Origem Material");
                            boxDadosOrigemMaterial.addStyle("light-amber");
                            boxDadosOrigemMaterial.expanded();
                            boxDadosOrigemMaterial.add(tblOrigemMateriaisMrp.build());
                        }));
                        boxDadosSubniveis.add(FormBox.create(boxDadosSobrasMaterial -> {
                            boxDadosSobrasMaterial.vertical();
                            boxDadosSobrasMaterial.title("Sobras");
                            boxDadosSobrasMaterial.addStyle("light-dark");
                            boxDadosSobrasMaterial.expanded();
                            boxDadosSobrasMaterial.add(tblSobrasMateriaisMrp.build());
                            boxDadosSobrasMaterial.add(FormBox.create(boxTotais -> {
                                boxTotais.horizontal();
                                boxTotais.alignment(Pos.TOP_RIGHT);
                                boxTotais.add(FormBox.create(boxAcumulado -> {
                                    boxAcumulado.horizontal();
                                    boxAcumulado.expanded();
                                    boxAcumulado.alignment(Pos.TOP_LEFT);
                                    boxAcumulado.add(FormFieldText.create(field -> {
                                        field.withoutTitle();
                                        field.label("Estoque");
                                        field.value.bind(acumuladoSobrasEstoque.asString("%.3f"));
                                        field.addStyle("success");
                                        field.width(130.0);
                                        field.alignment(Pos.CENTER_RIGHT);
                                    }).build());
                                    boxAcumulado.add(FormFieldText.create(field -> {
                                        field.withoutTitle();
                                        field.label("Compra");
                                        field.value.bind(acumuladoSobrasCompra.asString("%.3f"));
                                        field.addStyle("warning");
                                        field.width(130.0);
                                        field.alignment(Pos.CENTER_RIGHT);
                                    }).build());
                                }));
                                boxTotais.add(FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.label("Somatório\nSeleção");
                                    field.value.bind(calculadoraSomaSobrasSelecao.asString("%.3f"));
                                    field.width(210.0);
                                    field.addStyle("lg");
                                    field.alignment(Pos.CENTER_RIGHT);
                                }).build());
                            }));
                        }));
                    }));
                }));
            }));
        });
    }

    /**
     * Limpeza dos dados
     * Limpa as tabelas e os beans que exibem os totais selecionados
     */
    private void clearDados() {
        tblMateriaisMrp.items.clear();
        tblOrigemMateriaisMrp.items.clear();
        tblSobrasMateriaisMrp.items.clear();
        tblNecessidadesMaterialMrp.items.clear();
        calculadoraSomaLotes.set(BigDecimal.ZERO);
        calculadoraSomaOutrosLotes.set(BigDecimal.ZERO);
        calculadoraSomaCores.set(BigDecimal.ZERO);
        calculadoraSomaCoresOutrosLotes.set(BigDecimal.ZERO);
        calculadoraSomaSelecao.set(BigDecimal.ZERO);
        calculadoraSomaSelecaoOutrosLotes.set(BigDecimal.ZERO);
        calculadoraSomaSobrasSelecao.set(BigDecimal.ZERO);
        unidadeCalculadoraSomaSelecao.set("");
        unidadeCalculadoraSomaLote.set("");
        unidadeCalculadoraSomaCor.set("");
        acumuladoSobrasEstoque.set(BigDecimal.ZERO);
        acumuladoSobrasCompra.set(BigDecimal.ZERO);
        acumuladoNecessidadesEstoque.set(BigDecimal.ZERO);
        acumuladoNecessidadesCompra.set(BigDecimal.ZERO);
        acumuladoNecessidadesFalta.set(BigDecimal.ZERO);
    }

    /**
     * Set dos somatórios dos dados de materiais selecionados em tela
     * para exibição nos totalizadores
     */
    private void calculadoraSomatoriosDados() {
        try {
            Object[] materiaisSelecionados = tblMateriaisMrp.selectedRegisters().size() > 0
                    ? tblMateriaisMrp.selectedRegisters().stream().map(mat -> mat.getInsumo().getCodigo()).distinct().toArray()
                    : new Object[]{};

            // get do somatório da qtde dos lotes de todas as cores
            unidadeCalculadoraSomaLote.set(getUnidadeEstqConsumo(false, fieldSomenteFalta.value.getValue(),
                    fieldLotes.selectedRegisters().toArray(),
                    super.coresMateriaisTinturariaMrp.stream().map(Cor::getCor).distinct().toArray(),
                    materiaisSelecionados.length > 0 ? materiaisSelecionados : fieldMaterial.objectValues.get().stream().map(Material::getCodigo).toArray()));
            calculadoraSomaLotes.set(getSumQtdeConsumo(false, fieldSomenteFalta.value.getValue(),
                    fieldLotes.selectedRegisters().toArray(),
                    super.coresMateriaisTinturariaMrp.stream().map(Cor::getCor).distinct().toArray(),
                    materiaisSelecionados.length > 0 ? materiaisSelecionados : fieldMaterial.objectValues.get().stream().map(Material::getCodigo).toArray(),
                    fieldFornecedor.objectValues.get().stream().map(Entidade::getCodcli).toArray()));
            calculadoraSomaOutrosLotes.set(getSumQtdeConsumo(false, fieldSomenteFalta.value.getValue(),
                    fieldLotes.notSelectedRegisters().toArray(),
                    super.coresMateriaisTinturariaMrp.stream().map(Cor::getCor).distinct().toArray(),
                    materiaisSelecionados.length > 0 ? materiaisSelecionados : fieldMaterial.objectValues.get().stream().map(Material::getCodigo).toArray(),
                    fieldFornecedor.objectValues.get().stream().map(Entidade::getCodcli).toArray()));

            // get do somatório da qtde dos lotes de todas as cores
            unidadeCalculadoraSomaCor.set(getUnidadeEstqConsumo(false, fieldSomenteFalta.value.getValue(),
                    fieldLotes.selectedRegisters().toArray(),
                    fieldCoresMaterial.selectedRegisters().stream().map(Cor::getCor).toArray(),
                    materiaisSelecionados));
            calculadoraSomaCores.set(getSumQtdeConsumo(false, fieldSomenteFalta.value.getValue(),
                    fieldLotes.selectedRegisters().toArray(),
                    fieldCoresMaterial.selectedRegisters().stream().map(Cor::getCor).toArray(),
                    materiaisSelecionados.length > 0 ? materiaisSelecionados : fieldMaterial.objectValues.stream().map(Material::getCodigo).toArray(),
                    fieldFornecedor.objectValues.stream().map(Entidade::getCodcli).toArray()));
            calculadoraSomaCoresOutrosLotes.set(getSumQtdeConsumo(false, fieldSomenteFalta.value.getValue(),
                    fieldLotes.notSelectedRegisters().toArray(),
                    fieldCoresMaterial.selectedRegisters().stream().map(Cor::getCor).toArray(),
                    materiaisSelecionados.length > 0 ? materiaisSelecionados : fieldMaterial.objectValues.stream().map(Material::getCodigo).toArray(),
                    fieldFornecedor.objectValues.stream().map(Entidade::getCodcli).toArray()));
        } catch (StackOverflowError ex) { }
    }

    /**
     * Action SELECT_MODEL do ListView de lotes
     * Get dos materiais a partir dos lotes selecionados
     *
     * @param lotes
     */
    private void selecionarLoteMrp(Object[] lotes) {
        clearDados();
        new RunAsyncWithOverlay(view).exec(task -> {
            // get das cores dos lotes selecionados
            getCoresMateriaisTinturariaMrp(false, fieldSomenteFalta.value.get(), lotes,
                    fieldMaterialPeriodoCor.objectValues.stream().map(Material::getCodigo).toArray());

            Object[] materiaisSelecionados = tblMateriaisMrp.selectedRegisters().size() > 0
                    ? tblMateriaisMrp.selectedRegisters().stream().map(mat -> mat.getInsumo().getCodigo()).distinct().toArray()
                    : new Object[]{};
            // get do somatório da qtde dos lotes de todas as cores
            unidadeCalculadoraSomaLote.set(getUnidadeEstqConsumo(false, fieldSomenteFalta.value.getValue(),
                    lotes,
                    super.coresMateriaisTinturariaMrp.stream().map(Cor::getCor).distinct().toArray(),
                    materiaisSelecionados.length > 0 ? materiaisSelecionados : fieldMaterial.objectValues.get().stream().map(Material::getCodigo).toArray()));
            calculadoraSomaLotes.set(getSumQtdeConsumo(false, fieldSomenteFalta.value.getValue(),
                    lotes,
                    super.coresMateriaisTinturariaMrp.stream().map(Cor::getCor).distinct().toArray(),
                    materiaisSelecionados.length > 0 ? materiaisSelecionados : fieldMaterial.objectValues.get().stream().map(Material::getCodigo).toArray(),
                    fieldFornecedor.objectValues.get().stream().map(Entidade::getCodcli).toArray()));
            calculadoraSomaOutrosLotes.set(getSumQtdeConsumo(false, fieldSomenteFalta.value.getValue(),
                    fieldLotes.notSelectedRegisters().toArray(),
                    super.coresMateriaisTinturariaMrp.stream().map(Cor::getCor).distinct().toArray(),
                    materiaisSelecionados.length > 0 ? materiaisSelecionados : fieldMaterial.objectValues.get().stream().map(Material::getCodigo).toArray(),
                    fieldFornecedor.objectValues.get().stream().map(Entidade::getCodcli).toArray()));
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                fieldCoresMaterial.items.set(FXCollections.observableList(super.coresMateriaisTinturariaMrp));
            }
        });
    }

    /**
     * Action SELECT_MODEL do ListView
     * > get dos materiais de tinturaria com os lotes e cores selecionados
     *
     * @param cores
     * @param materiais
     */
    private void selecionarCorMrp(List<Cor> cores, Object[] materiais, Object[] fornecedor, Object[] deposito) {
        clearDados();
        AtomicReference<List<VSdMrpTinturariaV>> materiaisMrp = new AtomicReference<>(new ArrayList<>());
        new RunAsyncWithOverlay(view).exec(task -> {
            // get das cores dos lotes selecionados
            materiaisMrp.set(getMateriaisGrupo40(false, fieldSomenteFalta.value.get(),
                    cores.stream().map(Cor::getCor).toArray(),
                    fieldLotes.selectedRegisters().toArray(),
                    materiais,
                    fornecedor,
                    deposito));

            calculadoraSomatoriosDados();
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                tblMateriaisMrp.items.set(FXCollections.observableList(materiaisMrp.get()));
            }
        });
    }

    /**
     * Action SELECT_MODEL do TableView materiais
     * Get dos dados de OFs e sobras do material selecionado
     *
     * @param materialSelecionado
     */
    private void carregaDadosMaterialSelecionado(VSdMrpTinturariaV materialSelecionado) {
        // limpeza da marcação de materiais para formação da origem
        tblMateriaisMrp.items.forEach(item -> item.setSelected(false));
        tblMateriaisMrp.refresh();
        // atomic para carga dos dados para as tabelas e calculadores
        AtomicReference<List<VSdOfsMrpTinturaria>> materiaisPaiSelecionados = new AtomicReference<>(new ArrayList<>());
        AtomicReference<List<VSdMrpTinturariaV>> sobrasMateriaisSelecionados = new AtomicReference<>(new ArrayList<>());
        AtomicReference<List<VSdMrpTinturariaV>> necessidadesMateriaisSelecionados = new AtomicReference<>(new ArrayList<>());
        // async
        new RunAsyncWithOverlay(this.view).exec(task -> {
            // somatório do material seleciona e seus outros lotes
            calculadoraSomatoriosDados();
            // get das sobras do material selecionado
            sobrasMateriaisSelecionados.set(getSobrasMaterialSelecionado(false,
                    materialSelecionado.getInsumo().getCodigo(),
                    materialSelecionado.getCorinsumo().getCor()));
            // get dos materiais que originam o material selecionado nos lotes selecionados
            materiaisPaiSelecionados.set(getOfsMaterialSelecionado(false, fieldSomenteFalta.value.getValue(),
                    fieldMaterial.objectValues.size() == 1 ? fieldMaterial.objectValues.get(0).getCodigo() : materialSelecionado.getInsumo().getCodigo(),
                    fieldMaterial.objectValues.size() == 1 ? fieldCoresMaterial.selectedItem().getCor() : materialSelecionado.getCorinsumo().getCor(),
                    materialSelecionado.isPrincipal(),
                    fieldLotes.selectedRegisters().toArray()));
            // get materiais de composição do material selecionado
            List<VSdConsumosTinturaria> composicaoMaterialSelecionado = (List<VSdConsumosTinturaria>) new FluentDao().selectFrom(VSdConsumosTinturaria.class)
                    .where(eb -> eb
                            .equal("codigo", materialSelecionado.getInsumo().getCodigo())
                            .equal("cor", materialSelecionado.getCorinsumo().getCor()))
                    .resultList();
            necessidadesMateriaisSelecionados.set(getNecessidadesMaterialSelecionado(false, fieldLotes.selectedRegisters().toArray(),
                    composicaoMaterialSelecionado.stream().map(VSdConsumosTinturaria::getInsumo).distinct().toArray(),
                    composicaoMaterialSelecionado.stream().map(VSdConsumosTinturaria::getCorinsumo).distinct().toArray()));
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                // alimentando dados dos materiais de origem e consumo do material selecionado
                tblOrigemMateriaisMrp.items.set(FXCollections.observableList(materiaisPaiSelecionados.get()));
                tblNecessidadesMaterialMrp.items.set(FXCollections.observableList(necessidadesMateriaisSelecionados.get()));
                acumuladoNecessidadesEstoque.set(new BigDecimal(necessidadesMateriaisSelecionados.get().stream()
                        .filter(material -> material.getConsumidode().equals("C"))
                        .mapToDouble(material -> material.getConsumoestq().doubleValue()).sum()).setScale(3, RoundingMode.HALF_UP));
                acumuladoNecessidadesCompra.set(new BigDecimal(necessidadesMateriaisSelecionados.get().stream()
                        .filter(material -> material.getConsumidode().equals("B"))
                        .mapToDouble(material -> material.getConsumoestq().doubleValue()).sum()).setScale(3, RoundingMode.HALF_UP));
                acumuladoNecessidadesFalta.set(new BigDecimal(necessidadesMateriaisSelecionados.get().stream()
                        .filter(material -> material.getConsumidode().equals("A"))
                        .mapToDouble(material -> material.getConsumoestq().doubleValue()).sum()).setScale(3, RoundingMode.HALF_UP));
                // alimentando dados da tabela de sobras e os acumuladores por estoque
                tblSobrasMateriaisMrp.items.set(FXCollections.observableList(sobrasMateriaisSelecionados.get()));
                acumuladoSobrasEstoque.set(new BigDecimal(sobrasMateriaisSelecionados.get().stream()
                        .filter(material -> material.getConsumidode().equals("C"))
                        .mapToDouble(material -> material.getConsumoestq().doubleValue()).sum()).setScale(3, RoundingMode.HALF_UP));
                acumuladoSobrasCompra.set(new BigDecimal(sobrasMateriaisSelecionados.get().stream()
                        .filter(material -> material.getConsumidode().equals("B"))
                        .mapToDouble(material -> material.getConsumoestq().doubleValue()).sum()).setScale(3, RoundingMode.HALF_UP));
            }
        });
    }

    /**
     * Action SELECT_MODEL da TableView materiais para o cálculo do consumo de estoque dos materiais selecionados
     *
     * @param materiaisSelecionados
     */
    private void calculaMateriasSelecionados(List<VSdMrpTinturariaV> materiaisSelecionados) {
        calculadoraSomaSelecao.set(new BigDecimal(materiaisSelecionados.stream().mapToDouble(material -> material.getConsumoestq().doubleValue()).sum()).setScale(3, RoundingMode.HALF_UP));
        unidadeCalculadoraSomaSelecao.set(materiaisSelecionados.stream().map(VSdMrpTinturariaV::getUnidadeestq).max(Comparator.naturalOrder()).get());
    }

    /**
     * Action SELECT_MODEL da TableView de sobras para o cálculo do consumo de estoque dos materiais selecionados
     *
     * @param materiaisSelecionados
     */
    private void calculaSobrasMateriasSelecionados(List<VSdMrpTinturariaV> materiaisSelecionados) {
        calculadoraSomaSobrasSelecao.set(new BigDecimal(materiaisSelecionados.stream().mapToDouble(material -> material.getConsumoestq().doubleValue()).sum()).setScale(3, RoundingMode.HALF_UP));
    }

    /**
     * Fragment para abertura de tabelas de totais
     * Caso necessário implementar as tabelas de totais
     */
    @Deprecated
    private void abrirTotaisFioTab() {
        new Fragment().showAndContinue(fragment -> {
            fragment.title("Consumo Acumulado por Material");
            fragment.size(300.0, 500.0);
        });
    }

    /**
     * Fragment com a configuração das barcas e a criação das ordens de tinturaria
     *
     * @param materiais
     */
    private void criarBarcas(List<VSdMrpTinturariaV> materiais, boolean ctrlIsDown, Object[] depositos) {
        // validação de materiais principal selecionado
        if (materiais.size() == 0) {
            MessageBox.create(message -> {
                message.message("Você deve selecionar ao menos um material principal (indicador verde) para a criação das barcas.");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showAndWait();
            });
            return;
        }
        // validação de cor única selecionada ou atribuição da cor da barca
        if (materiais.stream().map(VSdMrpTinturariaV::getCorinsumo).distinct().count() > 1) {
            MessageBox.create(message -> {
                message.message("Você selecionou mais de uma cor de material para a barca. Verifique os materiais selecionados.");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showAndWait();
            });
            return;
        }
        final Cor corBarca = materiais.stream().map(VSdMrpTinturariaV::getCorinsumo).distinct().findAny().get();

        // get dos materiais e dos combinados, fornecedor e dados mais barcas cadastradas para o fornecedor
        AtomicReference<Entidade> fornecedor = new AtomicReference<>(null);
        AtomicReference<List<SdBarcaFornecedor>> barcasFornecedor = new AtomicReference<>(Collections.emptyList());
        AtomicReference<List<VSdMrpTinturariaV>> materiaisBarca = new AtomicReference<>(Collections.emptyList());
        AtomicReference<List<VSdMrpTinturariaV>> materiaisOfBarca = new AtomicReference<>();
        AtomicReference<List<VSdMrpTinturariaV>> materiaisOfBarcaOutrosLotes = new AtomicReference<>();
        AtomicReference<Map<String, List<VSdMrpTinturariaV>>> materiaisMapOfBarca = new AtomicReference<>();
        AtomicReference<Map<String, List<VSdMrpTinturariaV>>> materiaisMapOfBarcaOutrosLotes = new AtomicReference<>();
        AtomicReference<Exception> exceptionAsync = new AtomicReference<>();

        AtomicReference<List<Of1>> ofsPeriodosSelecionados = new AtomicReference<>();
        AtomicReference<List<Of1>> ofsPeriodosNaoSelecionados = new AtomicReference<>();

        new RunAsyncWithOverlay(view).exec(task -> {
            try {
                // get das OFs dos materiais/cor/lotes selecionados
                Set<String> ofsMateriaisSelecionados = getOFsMateriaisBarca(false,
                        materiais.stream().map(mat -> mat.getInsumo().getCodigo()).distinct().toArray(),
                        corBarca.getCor(),
                        fieldLotes.selectedRegisters().toArray(),
                        depositos);
                ofsPeriodosSelecionados.set((List<Of1>) new FluentDao().selectFrom(Of1.class)
                        .where(eb -> eb
                                .isIn("numero", ofsMateriaisSelecionados.toArray()))
                        .resultList());
                Set<String> ofsOutrosLotesMateriaisSelecionados = getOFsMateriaisBarca(false,
                        materiais.stream().map(mat -> mat.getInsumo().getCodigo()).distinct().toArray(),
                        corBarca.getCor(),
                        fieldLotes.notSelectedRegisters().toArray(),
                        depositos);
                ofsPeriodosNaoSelecionados.set((List<Of1>) new FluentDao().selectFrom(Of1.class)
                        .where(eb -> eb
                                .isIn("numero", ofsOutrosLotesMateriaisSelecionados.toArray()))
                        .resultList());

                // get dos materiais a partir das OFs dos períodos
                materiaisBarca.set(getMateriaisGrupo40SemDeposito(false, true, new String[]{corBarca.getCor()},
                        ofsMateriaisSelecionados.toArray()));
                // get materiais barca aberto por OF
                materiaisOfBarca.set(getMateriaisMrpGrupo40(false, true, new String[]{corBarca.getCor()},
                        ofsMateriaisSelecionados.toArray()));
                materiaisOfBarca.get().forEach(VSdMrpTinturariaV::refresh);
                materiaisOfBarcaOutrosLotes.set(getMateriaisMrpGrupo40(false, true, new String[]{corBarca.getCor()},
                        ofsOutrosLotesMateriaisSelecionados.toArray()));
                materiaisOfBarcaOutrosLotes.get().forEach(VSdMrpTinturariaV::refresh);

            } catch (Exception e) {
                exceptionAsync.set(e);
                return ReturnAsync.EXCEPTION.value();
            }
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                // validação dos fornecedores dos materiais
                try {
                    if (ctrlIsDown) {
                        do {
                            fornecedor.set((Entidade) InputBox.build(Entidade.class, boxInput -> {
                                boxInput.message("Selecione a tinturaria que deseja criar as barcas:");
                                boxInput.showAndWait();
                            }).value.getValue());
                        } while (fornecedor.get() == null);
                    } else if (materiaisBarca.get().stream().map(VSdMrpTinturariaV::getForinsumo).distinct().count() > 1) {
                        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                            message.message("Existe mais de um fornecedor cadastrado para os materias selecionados, " +
                                    "deseja criar a barca com os materiais selecionados?");
                            message.showAndWait();
                        }).value.get())) {
                            do {
                                fornecedor.set(new GenericSelect<Entidade>(Entidade.class,
                                        FXCollections.observableList(materiaisBarca.get().stream()
                                                .map(VSdMrpTinturariaV::getForinsumo)
                                                .distinct()
                                                .collect(Collectors.toList()))).selectedReturn);
                            } while (fornecedor.get() == null);
                        } else {
                            return;
                        }
                    } else {
                        fornecedor.set(materiaisBarca.get().stream()
                                .filter(VSdMrpTinturariaV::isPrincipal)
                                .map(VSdMrpTinturariaV::getForinsumo)
                                .distinct()
                                .findAny()
                                .get());
                    }
                    materiaisBarca.get().removeIf(mat -> !mat.getForinsumo().getCodcli().equals(fornecedor.get().getCodcli()) && !ctrlIsDown);
                    materiaisOfBarca.get().removeIf(mat -> !mat.getForinsumo().getCodcli().equals(fornecedor.get().getCodcli()) && !ctrlIsDown);
                    materiaisOfBarcaOutrosLotes.get().removeIf(mat -> !mat.getForinsumo().getCodcli().equals(fornecedor.get().getCodcli()) && !ctrlIsDown);
                } catch (Exception exc) {
                    exc.printStackTrace();
                    ExceptionBox.build(ebox -> {
                        ebox.exception(exc);
                        ebox.showAndWait();
                    });
                    return;
                }

                // complemento da carga dos materiais
                new RunAsyncWithOverlay(view).exec(task -> {
                    try {
                        // get dados fornecedor
                        barcasFornecedor.set((List<SdBarcaFornecedor>) new FluentDao().selectFrom(SdBarcaFornecedor.class)
                                .where(eb -> eb
                                        .equal("id.fornecedor.codcli", fornecedor.get().getCodcli())
                                        .equal("ativo", true))
                                .resultList());

                        // get necessidade materiais barca e consumo outros lotes
                        materiaisBarca.get().forEach(material -> {
                            // limpeza dos dados para criação das barcas
                            material.setConsumidoBarca(BigDecimal.ZERO);
                            material.setConsumidoEstqBarca(BigDecimal.ZERO);
                            // get do materia cru
                            List<Cadmatti> materiaisCru = (List<Cadmatti>) new FluentDao().selectFrom(Cadmatti.class).where(eb -> eb.equal("codigo2", material.getInsumo().getCodigo())).resultList();
                            if (materiaisCru.size() == 1) {
                                JPAUtils.getEntityManager().refresh(materiaisCru.stream().findFirst().get());
                                material.setMaterialNecessidade(materiaisCru.stream().findFirst().get().getCodigo());
                                material.setCorMaterialNecessidade(materiaisCru.stream().findFirst().get().getCors());
                                material.setPesoRolo(materiaisCru.stream().findFirst().get().getKgrolo());
                                material.setRolosPar(StringUtils.nvl(materiaisCru.stream().findFirst().get().getEstampa(), "N"));
                                material.setSaldoLotes(getSaldoLotesMaterialCor(false, true, new String[]{corBarca.getCor()}, fieldLotes.notSelectedRegisters().toArray(), new String[]{material.getInsumo().getCodigo()}));
                                // get dados cor do fornecedor
                                if (fornecedor.get() != null) {
                                    List<Cadcorti> corFornecedor = (List<Cadcorti>) new FluentDao().selectFrom(Cadcorti.class)
                                            .where(eb -> eb
                                                    .equal("codigo", material.getMaterialNecessidade())
                                                    .equal("cor", corBarca.getCor())
                                                    .equal("codcli", fornecedor.get().getCodcli()))
                                            .resultList();
                                    if (corFornecedor.size() == 1) {
                                        material.setCorTinturaria(corFornecedor.stream().findFirst().get().getCor1());
                                        material.setCustoTinturaria(corFornecedor.stream().findFirst().get().getCusto());
                                    }
                                }
                            }
                        });
                        // get complementos para materiais abertos por OF
                        materiaisOfBarca.get().forEach(material -> {
                            // limpeza dos dados para criação das barcas
                            material.setConsumidoBarca(BigDecimal.ZERO);
                            material.setConsumidoEstqBarca(BigDecimal.ZERO);

                            BigDecimal consumoUnitario = new FluentDao().selectFrom(PcpFtOf.class).sum("consumo")
                                    .where(eb -> eb
                                            .equal("pcpFtOfID.numero", material.getNumeroof())
                                            .equal("pcpFtOfID.cor", material.getCormaterialpai())
                                            .equal("pcpFtOfID.insumo", material.getInsumo().getCodigo())
                                            .equal("pcpFtOfID.corI", material.getCorinsumo().getCor())
                                            .isNull("deposito", TipoExpressao.AND, b -> material.getDeposito().equals("****"))
                                            .equal("deposito", material.getDeposito(), TipoExpressao.AND, b -> !material.getDeposito().equals("****")))
                                    .singleResult();
                            if (consumoUnitario == null)
                                throw new BreakException("Não foi possível obter o consumo unitário do material na OF " + material.getNumeroof());
                            material.setConsumoUnitario(consumoUnitario);
                            materiaisBarca.get().stream().filter(mat -> mat.getInsumo().getCodigo().equals(material.getInsumo().getCodigo()))
                                    .findAny().ifPresent(mat -> {
                                material.setPesoRolo(mat.getPesoRolo());
                                material.setMaterialNecessidade(mat.getMaterialNecessidade());
                                material.setCorMaterialNecessidade(mat.getCorMaterialNecessidade());
                                material.setCorTinturaria(mat.getCorTinturaria());
                                material.setCustoTinturaria(mat.getCustoTinturaria());
                            });
                        });
                        materiaisOfBarcaOutrosLotes.get().forEach(material -> {
                            // limpeza dos dados para criação das barcas
                            material.setConsumidoBarca(BigDecimal.ZERO);
                            material.setConsumidoEstqBarca(BigDecimal.ZERO);

                            BigDecimal consumoUnitario = new FluentDao().selectFrom(PcpFtOf.class).sum("consumo")
                                    .where(eb -> eb
                                            .equal("pcpFtOfID.numero", material.getNumeroof())
                                            .equal("pcpFtOfID.cor", material.getCormaterialpai())
                                            .equal("pcpFtOfID.insumo", material.getInsumo().getCodigo())
                                            .equal("pcpFtOfID.corI", material.getCorinsumo().getCor())
                                            .isNull("deposito", TipoExpressao.AND, b -> material.getDeposito().equals("****"))
                                            .equal("deposito", material.getDeposito(), TipoExpressao.AND, b -> !material.getDeposito().equals("****")))
                                    .singleResult();
                            if (consumoUnitario == null)
                                throw new BreakException("Não foi possível obter o consumo unitário do material na OF " + material.getNumeroof());
                            material.setConsumoUnitario(consumoUnitario);
                            materiaisBarca.get().stream().filter(mat -> mat.getInsumo().getCodigo().equals(material.getInsumo().getCodigo()))
                                    .findAny().ifPresent(mat -> {
                                material.setPesoRolo(mat.getPesoRolo());
                                material.setMaterialNecessidade(mat.getMaterialNecessidade());
                                material.setCorMaterialNecessidade(mat.getCorMaterialNecessidade());
                                material.setCorTinturaria(mat.getCorTinturaria());
                                material.setCustoTinturaria(mat.getCustoTinturaria());
                            });
                        });
                        // get dos materiais da barca separados por OF
                        materiaisMapOfBarca.set(materiaisOfBarca.get().stream().collect(Collectors.groupingBy(VSdMrpTinturariaV::getNumeroof)));
                        materiaisMapOfBarcaOutrosLotes.set(materiaisOfBarcaOutrosLotes.get().stream().collect(Collectors.groupingBy(VSdMrpTinturariaV::getNumeroof)));
                    } catch (Exception excpt) {
                        excpt.printStackTrace();
                        exceptionAsync.set(excpt);
                        return ReturnAsync.EXCEPTION.value;
                    }
                    return ReturnAsync.OK.value;
                }).addTaskEndNotification(taskReturnComplemento -> {
                    if (taskReturnComplemento.equals(ReturnAsync.OK.value)) {
                        createFragmentBarca(materiais, corBarca, fornecedor, barcasFornecedor,
                                materiaisBarca, materiaisOfBarca, materiaisOfBarcaOutrosLotes,
                                materiaisMapOfBarca, materiaisMapOfBarcaOutrosLotes, ofsPeriodosSelecionados,
                                ofsPeriodosNaoSelecionados);
                    } else if (taskReturn.equals(ReturnAsync.EXCEPTION.value)) {
                        ExceptionBox.build(box -> {
                            box.exception(exceptionAsync.get());
                            box.showAndWait();
                        });
                    }
                });
            } else if (taskReturn.equals(ReturnAsync.EXCEPTION.value)) {
                ExceptionBox.build(box -> {
                    box.exception(exceptionAsync.get());
                    box.showAndWait();
                });
            }
        });
    }

    /**
     * criação do fragment para configuração da barca para os materiais selecionados
     *
     * @param materiais
     * @param corBarca
     * @param fornecedor
     * @param barcasFornecedor
     * @param materiaisBarca
     * @param materiaisOfBarca
     * @param materiaisOfBarcaOutrosLotes
     * @param materiaisMapOfBarca
     * @param materiaisMapOfBarcaOutrosLotes
     */
    private void createFragmentBarca(List<VSdMrpTinturariaV> materiais,
                                     Cor corBarca,
                                     AtomicReference<Entidade> fornecedor,
                                     AtomicReference<List<SdBarcaFornecedor>> barcasFornecedor,
                                     AtomicReference<List<VSdMrpTinturariaV>> materiaisBarca,
                                     AtomicReference<List<VSdMrpTinturariaV>> materiaisOfBarca,
                                     AtomicReference<List<VSdMrpTinturariaV>> materiaisOfBarcaOutrosLotes,
                                     AtomicReference<Map<String, List<VSdMrpTinturariaV>>> materiaisMapOfBarca,
                                     AtomicReference<Map<String, List<VSdMrpTinturariaV>>> materiaisMapOfBarcaOutrosLotes,
                                     AtomicReference<List<Of1>> ofsPeriodosSelecionados,
                                     AtomicReference<List<Of1>> ofsPeriodosNaoSelecionados) {
        // get do lote de programação pendente
        String loteProgramacao = null;
        try {
            loteProgramacao = getLoteProgramacaoPendente();
            if (loteProgramacao != null) {
                String finalLoteProgramacao = loteProgramacao;
                if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                        message.message("Lote "+ finalLoteProgramacao + " pendente.\n\n" +
                                "Deseja criar um novo lote?\n" +
                                "A criação de um novo lote irá excluir as barcas pendentes de OTN.");
                        message.showAndWait();
                    }).value.get())) {
                    deleteProgramacaoPendente();
                    loteProgramacao = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd", new Locale("pt", "BR")));
                    SysLogger.addSysDelizLog("MRP Tinturaria", TipoAcao.CADASTRAR, String.valueOf(fornecedor.get().getCodcli()),
                            "Atualizando o lote de programação de barcas. Excluindo todas as programações pendentes para a criação do novo número.");
                }
            } else {
                loteProgramacao = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd", new Locale("pt", "BR")));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(throwables);
                message.showAndWait();
            });
            return;
        }

        SysLogger.addSysDelizLog("MRP Tinturaria", TipoAcao.CADASTRAR, String.valueOf(fornecedor.get().getCodcli()),
                "Iniciando programação de barcas na cor "+corBarca.getCor()+" para o lote "+loteProgramacao);

        // controle
        AtomicReference<SdProgramacaoBarca> programacaoBarca = new AtomicReference<>(
                new SdProgramacaoBarca(fornecedor.get().getCodcli(), corBarca.getCor(), loteProgramacao));
        AtomicReference<BigDecimal> saldoProgramacao = new AtomicReference<>(
                new BigDecimal(materiaisBarca.get().stream()
                        .mapToDouble(mat -> mat.getConsumoestq().doubleValue())
                        .sum()));

        // variaveis para exibição
        final Fragment fragmentBarcas = new Fragment();

        // fields totais
        final FormFieldText fieldQtdeTotalBarca = FormFieldText.create(field -> {
            field.title("Qtde Total");
            field.addStyle("lg");
            field.editable.set(false);
            field.width(150.0);
            field.alignment(Pos.CENTER_RIGHT);
            field.value.set(StringUtils.toDecimalFormat(materiaisBarca.get().stream().mapToDouble(mat -> mat.getConsumoestq().doubleValue()).sum(), 3));
        });
        final FormFieldText fieldTotalBarca = FormFieldText.create(field -> {
            field.title("Total");
            field.addStyle("lg");
            field.editable.set(false);
            field.width(200.0);
            field.alignment(Pos.CENTER_RIGHT);
            field.value.set(StringUtils.toMonetaryFormat(materiaisBarca.get().stream().mapToDouble(mat -> mat.getCustoTinturaria().multiply(mat.getConsumoestq()).doubleValue()).sum(), 2));
        });
        final FormFieldText fieldQtdeMateriaisBarca = FormFieldText.create(field -> {
            field.title("Qtde Mats.");
            field.addStyle("lg");
            field.editable.set(false);
            field.width(70.0);
            field.alignment(Pos.CENTER);
            field.value.set(String.valueOf(materiaisBarca.get().stream().map(VSdMrpTinturariaV::getInsumo).distinct().count()));
        });
        final FormFieldText fieldQtdeSaldoMateriaisBarca = FormFieldText.create(field -> {
            field.title("Saldo Mats.");
            field.addStyle("lg");
            field.editable.set(false);
            field.width(120.0);
            field.alignment(Pos.CENTER_RIGHT);
            field.value.set(StringUtils.toDecimalFormat(materiaisBarca.get().stream().mapToDouble(mats -> mats.getSaldoLotes().doubleValue()).sum(), 3));
        });
        // fields/tables materiais e maquinas
        final FormFieldTextArea fieldObservacaoBarca = FormFieldTextArea.create(field -> {
            field.title("Infos. Barca");
            field.height(200.0);
        });
        final FormTableView<SdBarcaFornecedor> tblBarcasFornecedor = FormTableView.create(SdBarcaFornecedor.class, table -> {
            table.title("Barcas Fornecedor");
            table.expanded();
            table.setItems(FXCollections.observableList(barcasFornecedor.get()));
            table.columns(
                    FormTableColumn.create(cln -> {
                        cln.title("Barca");
                        cln.width(100.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdBarcaFornecedor, SdBarcaFornecedor>, ObservableValue<SdBarcaFornecedor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getMaquina()));
                    }).build() /*Barca*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Min");
                        cln.width(50.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdBarcaFornecedor, SdBarcaFornecedor>, ObservableValue<SdBarcaFornecedor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getMinimo()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*Min*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Max");
                        cln.width(50.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdBarcaFornecedor, SdBarcaFornecedor>, ObservableValue<SdBarcaFornecedor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getMaximo()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*Max*/,
                    FormTableColumn.create(cln -> {
                        cln.title("DD");
                        cln.width(40.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdBarcaFornecedor, SdBarcaFornecedor>, ObservableValue<SdBarcaFornecedor>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPrazo()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*DD*/
            );
            table.selectionModelItem((observable, oldValue, newValue) -> {
                fieldObservacaoBarca.value.set(((SdBarcaFornecedor) newValue).getObs());
            });
        });
        final FormTableView<VSdMrpTinturariaV> tblMateriaisBarca = FormTableView.create(VSdMrpTinturariaV.class, table -> {
            table.title("Materiais Barca");
            table.expanded();
            table.items.set(FXCollections.observableList(materiaisBarca.get()));
            table.columns(
                    FormTableColumn.create(cln -> {
                        cln.title("Retorno");
                        cln.width(320.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getInsumo()));
                    }).build() /*Retorno*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Gram.");
                        cln.width(60.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getInsumo().getGramatura()));
                        cln.alignment(Pos.CENTER_RIGHT);
                        cln.format(param -> {
                            return new TableCell<VSdMrpTinturariaV, BigDecimal>() {
                                @Override
                                protected void updateItem(BigDecimal item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    if (item != null && !empty) {
                                        setText(StringUtils.toDecimalFormat(item.doubleValue(), 3));
                                    }
                                }
                            };
                        });
                    }).build() /*Gram.*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Larg.");
                        cln.width(50.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getInsumo().getLargura()));
                        cln.alignment(Pos.CENTER);
                        cln.format(param -> {
                            return new TableCell<VSdMrpTinturariaV, BigDecimal>() {
                                @Override
                                protected void updateItem(BigDecimal item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    if (item != null && !empty) {
                                        setText(StringUtils.toDecimalFormat(item.doubleValue(), 2));
                                    }
                                }
                            };
                        });
                    }).build() /*Larg.*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Material");
                        cln.width(80.0);
                        cln.hide();
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getMaterialNecessidade()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*Material*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Qtde");
                        cln.width(70.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getConsumoestq()));
                        cln.alignment(Pos.CENTER_RIGHT);
                        cln.format(param -> {
                            return new TableCell<VSdMrpTinturariaV, BigDecimal>() {
                                @Override
                                protected void updateItem(BigDecimal item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    if (item != null && !empty) {
                                        setText(StringUtils.toDecimalFormat(item.doubleValue(), 3));
                                    }
                                }
                            };
                        });
                    }).build() /*Qtde*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Sld Lotes");
                        cln.width(70.0);
                        cln.hide();
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getSaldoLotes()));
                        cln.alignment(Pos.CENTER_RIGHT);
                        cln.format(param -> {
                            return new TableCell<VSdMrpTinturariaV, BigDecimal>() {
                                @Override
                                protected void updateItem(BigDecimal item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    if (item != null && !empty) {
                                        setText(StringUtils.toDecimalFormat(item.doubleValue(), 3));
                                    }
                                }
                            };
                        });
                    }).build() /*Sld Lotes*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Cor F.");
                        cln.width(180.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                        cln.format(param -> {
                            return new TableCell<VSdMrpTinturariaV, VSdMrpTinturariaV>() {
                                @Override
                                protected void updateItem(VSdMrpTinturariaV item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    setGraphic(null);
                                    if (item != null && !empty) {
                                        if (item.getCorTinturaria() == null) {
                                            setGraphic(FormButton.create(btnSelecionarCor -> {
                                                btnSelecionarCor.title("Selecionar Cor");
                                                btnSelecionarCor.icon(ImageUtils.getIcon(ImageUtils.Icon.COR, ImageUtils.IconSize._16));
                                                btnSelecionarCor.addStyle("xs").addStyle("info");
                                                btnSelecionarCor.setAction(event -> {
                                                    atualizarCorFornecedorMaterial(item);
                                                });
                                            }));
                                        } else {
                                            setText(item.getCorTinturaria());
                                        }
                                    }
                                }

                                private void atualizarCorFornecedorMaterial(VSdMrpTinturariaV item) {
                                    AtomicReference<List<Cadcorti>> coresFornecedor = new AtomicReference<>(new ArrayList<>());
                                    new RunAsyncWithOverlay(fragmentBarcas).exec(task -> {
                                        coresFornecedor.set((List<Cadcorti>) new FluentDao().selectFrom(Cadcorti.class)
                                                .where(eb -> eb
                                                        .equal("codcli", fornecedor.get().getCodcli())
                                                        .equal("cor", item.getCorinsumo().getCor())
                                                        .equal("codigo", item.getMaterialNecessidade()))
                                                .resultList());
                                        return ReturnAsync.OK.value;
                                    }).addTaskEndNotification(taskReturn -> {
                                        if (taskReturn.equals(ReturnAsync.OK.value)) {
                                            if (coresFornecedor.get().size() == 0) {
                                                MessageBox.create(message -> {
                                                    message.message("Não existem cores cadastradas para esse fornecedor nesta material e cor de barca.");
                                                    message.type(MessageBox.TypeMessageBox.ALERT);
                                                    message.showAndWait();
                                                });
                                            } else {
                                                new Fragment().show(fragment -> {
                                                    fragment.title("Selecione uma cor");
                                                    fragment.size(300.0, 400.0);
                                                    final FormTableView<Cadcorti> tblCoresFornecedor = FormTableView.create(Cadcorti.class, tableInt -> {
                                                        tableInt.title("Cores");
                                                        tableInt.expanded();
                                                        tableInt.items.set(FXCollections.observableList(coresFornecedor.get()));
                                                        tableInt.columns(
                                                                FormTableColumn.create(cln -> {
                                                                    cln.title("Cor F.");
                                                                    cln.width(150.0);
                                                                    cln.value((Callback<TableColumn.CellDataFeatures<Cadcorti, Cadcorti>, ObservableValue<Cadcorti>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCor1()));
                                                                }).build() /*Cor F.*/,
                                                                FormTableColumn.create(cln -> {
                                                                    cln.title("Custo");
                                                                    cln.width(50.0);
                                                                    cln.value((Callback<TableColumn.CellDataFeatures<Cadcorti, Cadcorti>, ObservableValue<Cadcorti>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCusto()));
                                                                    cln.alignment(Pos.CENTER_RIGHT);
                                                                    cln.format(param -> {
                                                                        return new TableCell<Cadcorti, BigDecimal>() {
                                                                            @Override
                                                                            protected void updateItem(BigDecimal item, boolean empty) {
                                                                                super.updateItem(item, empty);
                                                                                setText(null);
                                                                                if (item != null && !empty) {
                                                                                    setText(StringUtils.toMonetaryFormat(item.doubleValue(), 2));
                                                                                }
                                                                            }
                                                                        };
                                                                    });
                                                                }).build() /*Custo*/,
                                                                FormTableColumn.create(cln -> {
                                                                    cln.title("Ativo");
                                                                    cln.width(40.0);
                                                                    cln.value((Callback<TableColumn.CellDataFeatures<Cadcorti, Cadcorti>, ObservableValue<Cadcorti>>) param -> new ReadOnlyObjectWrapper(param.getValue().getAtivo()));
                                                                    cln.alignment(Pos.CENTER);
                                                                    cln.format(param -> {
                                                                        return new TableCell<Cadcorti, Boolean>() {
                                                                            @Override
                                                                            protected void updateItem(Boolean item, boolean empty) {
                                                                                super.updateItem(item, empty);
                                                                                setText(null);
                                                                                setGraphic(null);
                                                                                if (item != null && !empty) {
                                                                                    setGraphic(item ? ImageUtils.getIcon(ImageUtils.Icon.STATUS_VERDE, ImageUtils.IconSize._16) : ImageUtils.getIcon(ImageUtils.Icon.STATUS_VERMELHO, ImageUtils.IconSize._16));
                                                                                }
                                                                            }
                                                                        };
                                                                    });
                                                                }).build() /*Ativo*/
                                                        );
                                                    });
                                                    fragment.box.getChildren().add(tblCoresFornecedor.build());
                                                    fragment.buttonsBox.getChildren().add(FormButton.create(btnSelecionarCorFornecedor -> {
                                                        btnSelecionarCorFornecedor.title("Selecionar");
                                                        btnSelecionarCorFornecedor.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                                                        btnSelecionarCorFornecedor.addStyle("success");
                                                        btnSelecionarCorFornecedor.setAction(evt -> {
                                                            if (tblCoresFornecedor.selectedItem() != null) {
                                                                item.setCorTinturaria(tblCoresFornecedor.selectedItem().getCor1());
                                                                item.setCustoTinturaria(tblCoresFornecedor.selectedItem().getCusto());
                                                                materiaisBarca.get().stream()
                                                                        .filter(mat -> mat.getInsumo().getCodigo().equals(item.getInsumo().getCodigo()))
                                                                        .forEach(mat -> mat.setCorTinturaria(tblCoresFornecedor.selectedItem().getCor1()));
                                                                materiaisOfBarca.get().stream()
                                                                        .filter(mat -> mat.getInsumo().getCodigo().equals(item.getInsumo().getCodigo()))
                                                                        .forEach(mat -> mat.setCorTinturaria(tblCoresFornecedor.selectedItem().getCor1()));
                                                                materiaisOfBarcaOutrosLotes.get().stream()
                                                                        .filter(mat -> mat.getInsumo().getCodigo().equals(item.getInsumo().getCodigo()))
                                                                        .forEach(mat -> mat.setCorTinturaria(tblCoresFornecedor.selectedItem().getCor1()));
                                                                materiaisMapOfBarca.get().forEach((numero, materiaisOf) -> {
                                                                    materiaisOf.stream()
                                                                            .filter(mat -> mat.getInsumo().getCodigo().equals(item.getInsumo().getCodigo()))
                                                                            .forEach(mat -> mat.setCorTinturaria(tblCoresFornecedor.selectedItem().getCor1()));
                                                                });
                                                                materiaisMapOfBarcaOutrosLotes.get().forEach((numero, materiaisOf) -> {
                                                                    materiaisOf.stream()
                                                                            .filter(mat -> mat.getInsumo().getCodigo().equals(item.getInsumo().getCodigo()))
                                                                            .forEach(mat -> mat.setCorTinturaria(tblCoresFornecedor.selectedItem().getCor1()));
                                                                });

                                                                fieldTotalBarca.value.set(StringUtils.toMonetaryFormat(materiaisBarca.get().stream()
                                                                        .mapToDouble(mat -> mat.getCustoTinturaria().multiply(mat.getConsumoestq()).doubleValue()).sum(), 2));
                                                                fragment.close();
                                                                table.refresh();
                                                            } else {
                                                                MessageBox.create(message -> {
                                                                    message.message("Você deve selecionar uma cor do fornecedor para o material.");
                                                                    message.type(MessageBox.TypeMessageBox.ALERT);
                                                                    message.showAndWait();
                                                                });
                                                            }
                                                        });
                                                    }));
                                                });
                                            }
                                        }
                                    });
                                }
                            };
                        });
                    }).build() /*COR F.*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Custo");
                        cln.width(50.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCustoTinturaria()));
                        cln.alignment(Pos.CENTER_RIGHT);
                        cln.format(param -> {
                            return new TableCell<VSdMrpTinturariaV, BigDecimal>() {
                                @Override
                                protected void updateItem(BigDecimal item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    if (item != null && !empty) {
                                        setText(StringUtils.toMonetaryFormat(item.doubleValue(), 2));
                                    }
                                }
                            };
                        });
                    }).build() /*Custo*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Total");
                        cln.width(80.0);
                        cln.hide();
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                        cln.alignment(Pos.CENTER_RIGHT);
                        cln.format(param -> {
                            return new TableCell<VSdMrpTinturariaV, VSdMrpTinturariaV>() {
                                @Override
                                protected void updateItem(VSdMrpTinturariaV item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    if (item != null && !empty) {
                                        setText(StringUtils.toMonetaryFormat(item.getCustoTinturaria().multiply(item.getConsumoestq()).doubleValue(), 2));
                                    }
                                }
                            };
                        });
                    }).build() /*Total*/,
                    FormTableColumn.create(cln -> {
                        cln.title("");
                        cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                        cln.width(50.0);
                        cln.alignment(FormTableColumn.Alignment.CENTER);
                        cln.value((Callback<TableColumn.CellDataFeatures<VSdMrpTinturariaV, VSdMrpTinturariaV>, ObservableValue<VSdMrpTinturariaV>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                        cln.format(param -> {
                            return new TableCell<VSdMrpTinturariaV, VSdMrpTinturariaV>() {
                                final HBox boxButtonsRow = new HBox(3);
                                final Button btnExcluirMaterial = FormButton.create(btn -> {
                                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                    btn.tooltip("Excluir material da barca");
                                    btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                    btn.addStyle("xs").addStyle("danger");
                                });

                                @Override
                                protected void updateItem(VSdMrpTinturariaV item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    setGraphic(null);
                                    if (item != null && !empty) {
                                        btnExcluirMaterial.setOnAction(evt -> {
                                            if (programacaoBarca.get().getBarcas().size() > 0) {
                                                MessageBox.create(message -> {
                                                    message.message("Existem barcas criadas para essa programação, você não pode excluir materiais." +
                                                            " Caso seja necessário a remoção do material, exclua as barcas primeiramente.");
                                                    message.type(MessageBox.TypeMessageBox.ALERT);
                                                    message.showAndWait();
                                                });
                                                return;
                                            }
                                            materiaisBarca.get().removeIf(mat -> mat.getInsumo().getCodigo().equals(item.getInsumo().getCodigo())
                                                    && mat.isContrastante() == item.isContrastante()
                                                    && mat.isPrincipal() == item.isPrincipal());
                                            materiaisOfBarca.get().removeIf(mat -> mat.getInsumo().getCodigo().equals(item.getInsumo().getCodigo())
                                                    && mat.isContrastante() == item.isContrastante()
                                                    && mat.isPrincipal() == item.isPrincipal());
                                            materiaisOfBarcaOutrosLotes.get().removeIf(mat -> mat.getInsumo().getCodigo().equals(item.getInsumo().getCodigo())
                                                    && mat.isContrastante() == item.isContrastante()
                                                    && mat.isPrincipal() == item.isPrincipal());

                                            materiaisMapOfBarca.get().forEach((numeroOf, mats) -> {
                                                mats.removeIf(mat -> mat.getInsumo().getCodigo().equals(item.getInsumo().getCodigo())
                                                        && mat.isContrastante() == item.isContrastante()
                                                        && mat.isPrincipal() == item.isPrincipal());
                                            });
                                            materiaisMapOfBarcaOutrosLotes.get().forEach((numeroOf, mats) -> {
                                                mats.removeIf(mat -> mat.getInsumo().getCodigo().equals(item.getInsumo().getCodigo())
                                                        && mat.isContrastante() == item.isContrastante()
                                                        && mat.isPrincipal() == item.isPrincipal());
                                            });
                                            table.refresh();
                                        });
                                        boxButtonsRow.getChildren().clear();
                                        boxButtonsRow.getChildren().addAll(btnExcluirMaterial);
                                        setGraphic(boxButtonsRow);
                                    }
                                }
                            };
                        });
                    }).build()
            );
            table.factoryRow(param -> {
                return new TableRow<VSdMrpTinturariaV>() {
                    @Override
                    protected void updateItem(VSdMrpTinturariaV item, boolean empty) {
                        super.updateItem(item, empty);
                        getStyleClass().removeAll("table-row-success", "table-row-warning");
                        if (item != null && !empty) {
                            getStyleClass().add(item.isContrastante() ? "table-row-warning" : item.isPrincipal() ? "table-row-success" : "");
                        }
                    }
                };
            });
            table.indices(
                    table.indice("Principal", "success", ""),
                    table.indice("Contrastante", "warning", "")
            );
        });
        final FormTableView<Of1> tblOfsPeriodosSelecionados = FormTableView.create(Of1.class, table -> {
            table.title("OFs Períodos");
            table.expanded();
            table.items.set(FXCollections.observableList(ofsPeriodosSelecionados.get()));
            table.selectColumn();
            table.editable.set(true);
            table.columns(
                    FormTableColumn.create(cln -> {
                        cln.title("OF");
                        cln.width(60.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<Of1, Of1>, ObservableValue<Of1>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumero()));
                        cln.alignment(Pos.CENTER);
                        cln.format(param -> {
                            return new TableCell<Of1, String>() {
                                @Override
                                protected void updateItem(String item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    setGraphic(null);
                                    if (item != null && !empty) {
                                        setText(item);
                                        if (materiaisOfBarca.get().stream()
                                                .filter(mat -> mat.isContrastante() && mat.getNumeroof().equals(item))
                                                .count() > 0) {
                                            setGraphic(ImageUtils.getIcon(ImageUtils.Icon.COR, ImageUtils.IconSize._16));
                                        }
                                    }
                                }
                            };
                        });
                    }).build() /*OF*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Lote");
                        cln.width(40.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<Of1, Of1>, ObservableValue<Of1>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPeriodo()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*Lote*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Produto");
                        cln.width(150.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<Of1, Of1>, ObservableValue<Of1>>) param -> new ReadOnlyObjectWrapper(param.getValue().getProduto().toString()));
                    }).build() /*Produto*/,
                    FormTableColumn.create(cln -> {
                        cln.title("");
                        cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                        cln.width(40.0);
                        cln.alignment(FormTableColumn.Alignment.CENTER);
                        cln.value((Callback<TableColumn.CellDataFeatures<Of1, Of1>, ObservableValue<Of1>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                        cln.format(param -> {
                            return new TableCell<Of1, Of1>() {
                                final HBox boxButtonsRow = new HBox(3);
                                final Button btnExcluirOf = FormButton.create(btn -> {
                                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                    btn.tooltip("Excluir OF da programação");
                                    btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                    btn.addStyle("xs").addStyle("danger");
                                });

                                @Override
                                protected void updateItem(Of1 item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    setGraphic(null);
                                    if (item != null && !empty) {
                                        btnExcluirOf.setOnAction(evt -> {
                                            if (programacaoBarca.get().getBarcas().size() > 0) {
                                                MessageBox.create(message -> {
                                                    message.message("Existem barcas criadas para essa programação, você não pode excluir OFs." +
                                                            " Caso seja necessário a remoção, exclua as barcas primeiramente.");
                                                    message.type(MessageBox.TypeMessageBox.ALERT);
                                                    message.showAndWait();
                                                });
                                                return;
                                            }
                                            materiaisMapOfBarca.get().get(item.getNumero()).forEach(mat -> {

                                                materiaisBarca.get().stream()
                                                        .filter(mat2 -> mat2.getInsumo().getCodigo().equals(mat.getInsumo().getCodigo())
                                                                && mat2.isContrastante() == mat.isContrastante()
                                                                && mat2.isPrincipal() == mat.isPrincipal())
                                                        .forEach(mat2 -> {
                                                            mat2.setConsumido(mat2.getConsumido()
                                                                    .subtract(mat.getConsumido()));
                                                            mat2.setConsumoof(mat2.getConsumoof()
                                                                    .subtract(mat.getConsumoof()));
                                                            mat2.setConsumoestq(mat2.getConsumoestq()
                                                                    .subtract(mat.getConsumoestq()));
                                                        });
                                                materiaisOfBarca.get().removeIf(mat2 -> mat2.getNumeroof().equals(item.getNumero()));

                                            });

                                            materiaisBarca.get().removeIf(mat2 -> mat2.getConsumoestq().compareTo(BigDecimal.ZERO) <= 0);
                                            materiaisMapOfBarca.get().remove(item.getNumero());
                                            ofsPeriodosSelecionados.get().removeIf(numOf -> numOf.getNumero().equals(item.getNumero()));

                                            table.refresh();
                                            tblMateriaisBarca.refresh();
                                        });
                                        boxButtonsRow.getChildren().clear();
                                        boxButtonsRow.getChildren().addAll(btnExcluirOf);
                                        setGraphic(boxButtonsRow);
                                    }
                                }
                            };
                        });
                    }).build()
            );
        });
        final FormTableView<Of1> tblOfsPeriodosNaoSelecionados = FormTableView.create(Of1.class, table -> {
            table.title("OFs Futuras");
            table.expanded();
            table.items.set(FXCollections.observableList(ofsPeriodosNaoSelecionados.get()));
            table.selectColumn();
            table.editable.set(true);
            table.columns(
                    FormTableColumn.create(cln -> {
                        cln.title("OF");
                        cln.width(50.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<Of1, Of1>, ObservableValue<Of1>>) param -> new ReadOnlyObjectWrapper(param.getValue().getNumero()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*OF*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Lote");
                        cln.width(40.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<Of1, Of1>, ObservableValue<Of1>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPeriodo()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*Lote*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Produto");
                        cln.width(150.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<Of1, Of1>, ObservableValue<Of1>>) param -> new ReadOnlyObjectWrapper(param.getValue().getProduto().toString()));
                    }).build() /*Produto*/,
                    FormTableColumn.create(cln -> {
                        cln.title("");
                        cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                        cln.width(40.0);
                        cln.alignment(FormTableColumn.Alignment.CENTER);
                        cln.value((Callback<TableColumn.CellDataFeatures<Of1, Of1>, ObservableValue<Of1>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                        cln.format(param -> {
                            return new TableCell<Of1, Of1>() {
                                final HBox boxButtonsRow = new HBox(3);
                                final Button btnExcluirOf = FormButton.create(btn -> {
                                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                    btn.tooltip("Excluir OF da programação");
                                    btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                    btn.addStyle("xs").addStyle("danger");
                                });

                                @Override
                                protected void updateItem(Of1 item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    setGraphic(null);
                                    if (item != null && !empty) {
                                        btnExcluirOf.setOnAction(evt -> {
                                            if (programacaoBarca.get().getBarcas().size() > 0) {
                                                MessageBox.create(message -> {
                                                    message.message("Existem barcas criadas para essa programação, você não pode excluir OFs." +
                                                            " Caso seja necessário a remoção, exclua as barcas primeiramente.");
                                                    message.type(MessageBox.TypeMessageBox.ALERT);
                                                    message.showAndWait();
                                                });
                                                return;
                                            }
                                            materiaisMapOfBarcaOutrosLotes.get().get(item.getNumero()).forEach(mat -> {
                                                materiaisOfBarcaOutrosLotes.get().removeIf(mat2 -> mat2.getNumeroof().equals(item.getNumero()));
                                            });
                                            materiaisMapOfBarcaOutrosLotes.get().remove(item.getNumero());
                                            ofsPeriodosNaoSelecionados.get().removeIf(numOf -> numOf.getNumero().equals(item.getNumero()));
                                            table.refresh();
                                        });
                                        boxButtonsRow.getChildren().clear();
                                        boxButtonsRow.getChildren().addAll(btnExcluirOf);
                                        setGraphic(boxButtonsRow);
                                    }
                                }
                            };
                        });
                    }).build()
            );
        });
        //fields fornecedor
        final FormFieldText fieldCidadeFornecedor = FormFieldText.create(field -> {
            field.title("Cidade");
            field.editable.set(false);
            field.width(250.0);
            if (fornecedor.get() != null)
                field.value.set(fornecedor.get().getCep().getCidade().getNomeCid());

        });
        final FormFieldText fieldContatoFornecedor = FormFieldText.create(field -> {
            field.title("Contato");
            field.editable.set(false);
            field.width(120.0);
            if (fornecedor.get() != null)
                field.value.set(fornecedor.get().getTelefone());

        });
        final FormFieldSingleFind<Entidade> fieldFornecedor = FormFieldSingleFind.create(Entidade.class, field -> {
            field.title("Fornecedor");
            field.width(300.0);
            field.disable.set(fornecedor.get() != null);
            if (fornecedor.get() != null)
                field.setDefaultCode(String.valueOf(fornecedor.get().getCodcli()));
            else
                field.postSelected((observable, oldValue, newValue) -> {

                });
        });
        final FormFieldText fieldQtdeBarca = FormFieldText.create(field -> {
            field.withoutTitle();
            field.label("QTDE");
            field.decimalField(3);
            field.alignment(Pos.CENTER);
            field.width(150.0);
            field.value.set(StringUtils.toDecimalFormat(saldoProgramacao.get().doubleValue(), 3));
        });
        // table barcas
        final FormTableView<SdBarcaProducao> tblBarcas = FormTableView.create(SdBarcaProducao.class, table -> {
            table.withoutHeader();
            table.expanded();
            table.items.set(FXCollections.observableArrayList());
            table.columns(
                    FormTableColumn.create(cln -> {
                        cln.title("Ordem");
                        cln.width(40.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdBarcaProducao, SdBarcaProducao>, ObservableValue<SdBarcaProducao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getOrdem()));
                        cln.alignment(Pos.CENTER);
                    }).build() /*Ordem*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Máquina");
                        cln.width(70.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdBarcaProducao, SdBarcaProducao>, ObservableValue<SdBarcaProducao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getMaquina().getId().getMaquina()));
                    }).build() /*Máquina*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Qtde");
                        cln.width(90.0);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdBarcaProducao, SdBarcaProducao>, ObservableValue<SdBarcaProducao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                        cln.alignment(Pos.CENTER_RIGHT);
                        cln.format(param -> {
                            return new TableCell<SdBarcaProducao, BigDecimal>() {
                                @Override
                                protected void updateItem(BigDecimal item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    if (item != null && !empty) {
                                        setText(StringUtils.toDecimalFormat(item.doubleValue(), 3));
                                    }
                                }
                            };
                        });
                    }).build() /*Qtde*/,
                    FormTableColumn.create(cln -> {
                        cln.title("Ações");
                        cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                        cln.width(80.0);
                        cln.alignment(FormTableColumn.Alignment.CENTER);
                        cln.value((Callback<TableColumn.CellDataFeatures<SdBarcaProducao, SdBarcaProducao>, ObservableValue<SdBarcaProducao>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                        cln.format(param -> {
                            return new TableCell<SdBarcaProducao, SdBarcaProducao>() {
                                final HBox boxButtonsRow = new HBox(3);
                                final Button btnVisualizarBarca = FormButton.create(btn -> {
                                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ABRIR, ImageUtils.IconSize._16));
                                    btn.tooltip("Visualizar materiais/ofs da barca.");
                                    btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                    btn.addStyle("xs").addStyle("info");
                                });
                                final Button btnExcluirBarca = FormButton.create(btn -> {
                                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                    btn.tooltip("Excluir a barca criada.");
                                    btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                    btn.addStyle("xs").addStyle("danger");
                                });

                                @Override
                                protected void updateItem(SdBarcaProducao item, boolean empty) {
                                    super.updateItem(item, empty);
                                    setText(null);
                                    setGraphic(null);
                                    if (item != null && !empty) {
                                        btnVisualizarBarca.setOnAction(evt -> {
                                            new Fragment().show(fragment -> {
                                                fragment.title("Visualizar Materias/OFs Barca");
                                                fragment.size(300.0, 400.0);
                                                Map<String, List<SdItensBarca>> ofsBarca = item.getItens().stream().collect(Collectors.groupingBy(SdItensBarca::getNumeroof));
                                                FormTabPane tabPaneOfs = FormTabPane.create(tabs -> {
                                                    tabs.expanded();
                                                    ofsBarca.forEach((of, itens) -> {
                                                        tabs.addTab(FormTab.create(tab -> {
                                                            tab.title(of);
                                                            tab.add(FormTableView.create(SdItensBarca.class, table2 -> {
                                                                table2.withoutHeader();
                                                                table2.items.set(FXCollections.observableList(itens));
                                                                table2.columns(
                                                                        FormTableColumn.create(cln -> {
                                                                            cln.title("Insumo");
                                                                            cln.width(120.0);
                                                                            cln.value((Callback<TableColumn.CellDataFeatures<SdItensBarca, SdItensBarca>, ObservableValue<SdItensBarca>>) param -> new ReadOnlyObjectWrapper(param.getValue().getInsumo()));
                                                                        }).build() /*Insumo*/,
                                                                        FormTableColumn.create(cln -> {
                                                                            cln.title("Depósito");
                                                                            cln.width(60.0);
                                                                            cln.value((Callback<TableColumn.CellDataFeatures<SdItensBarca, SdItensBarca>, ObservableValue<SdItensBarca>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDeposito()));
                                                                            cln.alignment(Pos.CENTER);
                                                                        }).build() /*Depósito*/,
                                                                        FormTableColumn.create(cln -> {
                                                                            cln.title("Qtde");
                                                                            cln.width(80.0);
                                                                            cln.value((Callback<TableColumn.CellDataFeatures<SdItensBarca, SdItensBarca>, ObservableValue<SdItensBarca>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtdeEstoque()));
                                                                            cln.alignment(Pos.CENTER_RIGHT);
                                                                            cln.format(param -> {
                                                                                return new TableCell<SdItensBarca, BigDecimal>() {
                                                                                    @Override
                                                                                    protected void updateItem(BigDecimal item, boolean empty) {
                                                                                        super.updateItem(item, empty);
                                                                                        setText(null);
                                                                                        if (item != null && !empty) {
                                                                                            setText(StringUtils.toDecimalFormat(item.doubleValue(), 3));
                                                                                        }
                                                                                    }
                                                                                };
                                                                            });
                                                                        }).build() /*Qtde*/
                                                                );
                                                            }).build());
                                                        }));
                                                    });
                                                });
                                                fragment.box.getChildren().add(tabPaneOfs);
                                            });
                                        });
                                        btnExcluirBarca.setOnAction(evt -> {
                                            // removendo qtde da barca em exclusão dos objetos já carregado
                                            item.getItens().forEach(itemBarca -> {
                                                // abatimento da carga da OF no MRP para próximas barcas
                                                materiaisOfBarca.get().stream()
                                                        .filter(mat -> mat.getInsumo().getCodigo().equals(itemBarca.getInsumo())
                                                                && mat.getCorinsumo().getCor().equals(itemBarca.getCorinsumo())
                                                                && mat.getNumeroof().equals(itemBarca.getNumeroof())
                                                                && mat.getDeposito().equals(itemBarca.getDeposito())
                                                                && mat.getCormaterialpai().equals(itemBarca.getCorProduto()))
                                                        .findAny()
                                                        .ifPresent(mat -> {
                                                            mat.setConsumidoBarca(mat.getConsumidoBarca().subtract(itemBarca.getQtde()));
                                                            mat.setConsumidoEstqBarca(mat.getConsumidoEstqBarca().subtract(itemBarca.getQtdeEstoque()));
                                                        });
                                            });
                                            saldoProgramacao.set(new BigDecimal(materiaisOfBarca.get().stream()
                                                    .mapToDouble(mat -> mat.getConsumoestq().subtract(mat.getConsumidoEstqBarca()).doubleValue())
                                                    .sum()));
                                            fieldQtdeBarca.value.set(StringUtils.toDecimalFormat(saldoProgramacao.get().doubleValue(), 3));
                                            programacaoBarca.get().getBarcas().remove(item);
                                            table.items.remove(item);
                                            table.refresh();
                                        });
                                        boxButtonsRow.getChildren().clear();
                                        boxButtonsRow.getChildren().addAll(btnVisualizarBarca);
                                        boxButtonsRow.getChildren().addAll(btnExcluirBarca);
                                        setGraphic(boxButtonsRow);
                                    }
                                }
                            };
                        });
                    }).build()
            );
        });

        // construção tela
        fragmentBarcas.show(fragment -> {
            fragment.title("Criar Barcas - Cor: " + corBarca.toString());
            fragment.size(1400.0, 750.0);
            fragment.box.getChildren().add(FormBox.create(container -> {
                container.horizontal();
                container.expanded();
                container.add(FormBox.create(boxBarcas -> {
                    boxBarcas.vertical();
                    boxBarcas.add(tblBarcasFornecedor.build());
                    boxBarcas.add(fieldObservacaoBarca.build());
                }));
                container.add(FormBox.create(boxFornecedorBarca -> {
                    boxFornecedorBarca.vertical();
                    boxFornecedorBarca.expanded();
                    boxFornecedorBarca.add(FormBox.create(boxDadosMateriaisBarca -> {
                        boxDadosMateriaisBarca.vertical();
                        boxDadosMateriaisBarca.add(FormBox.create(boxMateriaisOfsBarca -> {
                            boxMateriaisOfsBarca.horizontal();
                            boxMateriaisOfsBarca.expanded();
                            boxMateriaisOfsBarca.add(tblMateriaisBarca.build());
                        }));
                    }));
                    boxFornecedorBarca.add(FormBox.create(boxBarcasDadosFornecedor -> {
                        boxBarcasDadosFornecedor.horizontal();
                        boxBarcasDadosFornecedor.expanded();
                        boxBarcasDadosFornecedor.add(FormBox.create(boxDadosFornecedorTotais -> {
                            boxDadosFornecedorTotais.vertical();
                            boxDadosFornecedorTotais.add(FormBox.create(boxDadosFornecedor -> {
                                boxDadosFornecedor.vertical();
                                boxDadosFornecedor.title("Fornecedor");
                                boxDadosFornecedor.add(fieldFornecedor.build());
                                boxDadosFornecedor.add(FormBox.create(boxDadosFornecedorExtra -> {
                                    boxDadosFornecedorExtra.horizontal();
                                    boxDadosFornecedorExtra.add(fieldCidadeFornecedor.build());
                                    boxDadosFornecedorExtra.add(fieldContatoFornecedor.build());
                                }));
                            }));
                            boxDadosFornecedorTotais.add(FormBox.create(boxDadosTotais -> {
                                boxDadosTotais.vertical();
                                boxDadosTotais.expanded();
                                boxDadosTotais.title("Totais Programação");
                                boxDadosTotais.add(FormBox.create(boxFields -> {
                                    boxFields.flutuante();
                                    boxFields.expanded();
                                    boxFields.add(fieldQtdeMateriaisBarca.build());
                                    boxFields.add(fieldQtdeTotalBarca.build());
                                    boxFields.add(fieldQtdeSaldoMateriaisBarca.build());
                                    boxFields.add(fieldTotalBarca.build());
                                }));
                            }));
                        }));
                        boxBarcasDadosFornecedor.add(FormBox.create(boxBarcas -> {
                            boxBarcas.vertical();
                            boxBarcas.expanded();
                            boxBarcas.add(FormBox.create(boxDadosCriacaoBarca -> {
                                boxDadosCriacaoBarca.horizontal();
                                boxDadosCriacaoBarca.add(fieldQtdeBarca.build());
                                boxDadosCriacaoBarca.add(FormButton.create(btnCriarBarca -> {
                                    btnCriarBarca.title("Criar Barca");
                                    btnCriarBarca.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                                    btnCriarBarca.addStyle("success");
                                    btnCriarBarca.setAction(evt -> {
                                        // validação de peso padrão dos rolos do mat principal
                                        if (materiaisBarca.get().stream().filter(VSdMrpTinturariaV::isPrincipal).anyMatch(mat -> mat.getPesoRolo().compareTo(BigDecimal.ZERO) == 0)) {
                                            MessageBox.create(message -> {
                                                message.message("Existem materiais principais sem o peso padrão do rolo. Verifique o cadastro do material.");
                                                message.type(MessageBox.TypeMessageBox.IMPORTANT);
                                                message.showAndWait();
                                            });
                                            return;
                                        }
                                        // validação se selecionou a maquina do fornecedor
                                        if (tblBarcasFornecedor.selectedItem() == null) {
                                            MessageBox.create(message -> {
                                                message.message("Você precisa selecionar a barca do fornecedor.");
                                                message.type(MessageBox.TypeMessageBox.IMPORTANT);
                                                message.showAndWait();
                                            });
                                            return;
                                        }
                                        // validação se todos os materiais estão com a cor do fornecedor cadastrada
                                        if (materiaisBarca.get().stream().anyMatch(mat -> mat.getCorTinturaria() == null)) {
                                            MessageBox.create(message -> {
                                                message.message("Existem materiais sem a cor do fornecedor configurada, é necessário selecionar as cores dos materiais para iniciar as barcas.");
                                                message.type(MessageBox.TypeMessageBox.IMPORTANT);
                                                message.showAndWait();
                                            });
                                            return;
                                        }
                                        // variaveis de controle
                                        BigDecimal qtdeBarca = new BigDecimal(fieldQtdeBarca.value.getValue().replace(",", "."));

                                        // validação se a qtde da barca é maior que a qtde necessário do MRP
                                        Boolean usaOutrosLotes = false;
                                        if (materiaisBarca.get().stream().mapToDouble(mat -> mat.getSaldoLotes().doubleValue()).sum() > 0) {
                                            if (qtdeBarca.compareTo(saldoProgramacao.get()) > 0) {
                                                if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                                                    message.message("A qtde digitada para a barca é maior que a necessidade da cor. Deseja utilizar o saldo de outros lotes de produção?");
                                                    message.showAndWait();
                                                }).value.get())) {
                                                    usaOutrosLotes = true;
                                                }
                                            }
                                        }
                                        // adicionando a nova barca na programação
                                        SdBarcaProducao barca = new SdBarcaProducao(programacaoBarca.get(),
                                                tblBarcasFornecedor.selectedItem(),
                                                programacaoBarca.get().getBarcas().size() + 1,
                                                qtdeBarca, "P");
                                        programacaoBarca.get().getBarcas().add(barca); // add da barca a programação

                                        // get da qtde da barca sem os materiais não principal
                                        BigDecimal qtdeBarcaMatsPrincipal = qtdeBarca.multiply(
                                                new BigDecimal(materiaisBarca.get().stream()
                                                        .filter(matMrp -> matMrp.isPrincipal())
                                                        .mapToDouble(matMrp -> matMrp.getConsumoestq().doubleValue())
                                                        .sum())
                                                        .divide(new BigDecimal(materiaisBarca.get().stream()
                                                                .mapToDouble(matMrp -> matMrp.getConsumoestq().doubleValue())
                                                                .sum()), 9, RoundingMode.HALF_UP));

                                        // get de rolos mínimos para tinturaria
                                        Integer qtdeRolosBarca = qtdeBarcaMatsPrincipal
                                                .divide(materiaisBarca.get().stream()
                                                        .filter(VSdMrpTinturariaV::isPrincipal)
                                                        .findAny()
                                                        .get().getPesoRolo(), 0, RoundingMode.CEILING).intValue();
                                        // verifica se os rolos devem ser par e incrementa +1 rolo caso não tenha dado número par de rolos
                                        if (materiaisBarca.get().stream()
                                                .filter(VSdMrpTinturariaV::isPrincipal)
                                                .map(VSdMrpTinturariaV::getRolosPar)
                                                .distinct()
                                                .findFirst().equals("S")
                                                && qtdeRolosBarca % 2 > 0) {
                                            qtdeRolosBarca = qtdeRolosBarca + 1;
                                        }
                                        AtomicReference<BigDecimal> qtdeBarcaAtendeRolos = new AtomicReference<>(
                                                new BigDecimal(qtdeRolosBarca.doubleValue() * materiaisBarca.get().stream()
                                                        .filter(VSdMrpTinturariaV::isPrincipal)
                                                        .findAny().get().getPesoRolo().doubleValue()));

                                        SysLogger.addSysDelizLog("MRP Tinturaria", TipoAcao.CADASTRAR, String.valueOf(fornecedor.get().getCodcli()),
                                                "Adicionando barca #"+barca.getOrdem()+" a programação com qtde rolos " + qtdeRolosBarca + " utilizando excedente " + (usaOutrosLotes ? "nos próximos lotes" : " proporcional nos lotes selecionados"));

                                        // abatimento condição: não utiliza saldo lotes e qtde da barca é menor que a demanda do MRP na cor
                                        if (qtdeBarca.compareTo(saldoProgramacao.get()) <= 0 && !usaOutrosLotes) {
                                            AtomicReference<BigDecimal> qtdeParaAbatimento = new AtomicReference<>(qtdeBarcaAtendeRolos.get());

                                            try {
                                                // verificando materiais contrastantes principal
                                                List<VSdMrpTinturariaV> materiaisContrastantesPrincipal = materiaisOfBarca.get().stream()
                                                        .filter(mat -> mat.isContrastante() && mat.isPrincipal())
                                                        .collect(Collectors.toList());
                                                materiaisContrastantesPrincipal.forEach(matComb -> {
                                                    try {
                                                        List<Object> consumosProgramadoContrastante =
                                                                getProgramadoContrastante(programacaoBarca.get().getLote(), matComb.getNumeroof());
                                                        Integer qtdeProgramado = 0;
                                                        if (consumosProgramadoContrastante.size() > 0) {
                                                            qtdeProgramado = consumosProgramadoContrastante.stream()
                                                                    .mapToInt(matProg -> (Integer) ((Map<String, Object>) matProg).get("QTDE"))
                                                                    .min()
                                                                    .getAsInt();
                                                        }
                                                        BigDecimal consumoQtdeBarca = BigDecimal.ZERO;
                                                        BigDecimal consumoEstoqueQtdeBarca = BigDecimal.ZERO;
                                                        // consumo para barca
                                                        if (qtdeProgramado > 0) {
                                                            // definindo o consumo para abatimento do material
                                                            BigDecimal consumoFinal = new BigDecimal(qtdeProgramado)
                                                                    .multiply(matComb.getConsumoUnitario())
                                                                    .subtract(matComb.getConsumidoBarca());
                                                            if (consumoFinal.compareTo(BigDecimal.ZERO) > 0) {
                                                                // verificação se o consumo do estoque do material é maior que o consumo da barca, para utilizar o da barca
                                                                if (consumoFinal.multiply(matComb.getInsumo().getPeso().compareTo(BigDecimal.ZERO) > 0
                                                                        ? matComb.getInsumo().getPeso()
                                                                        : BigDecimal.ONE).compareTo(qtdeParaAbatimento.get()) > 0)
                                                                    consumoFinal = qtdeParaAbatimento.get();
                                                                // difine as qtde de consumos
                                                                consumoQtdeBarca = consumoFinal
                                                                        .divide(new BigDecimal(1 - (matComb.getInsumo().getIndtin()
                                                                                .divide(new BigDecimal(100), 9, RoundingMode.HALF_UP).doubleValue())));
                                                                consumoEstoqueQtdeBarca = consumoFinal
                                                                        .multiply((matComb.getInsumo().getPeso() != null
                                                                                && matComb.getInsumo().getPeso().compareTo(BigDecimal.ZERO) > 0)
                                                                                ? matComb.getInsumo().getPeso()
                                                                                : BigDecimal.ONE)
                                                                        .divide(new BigDecimal(1 - (matComb.getInsumo().getIndtin()
                                                                                .divide(new BigDecimal(100), 9, RoundingMode.HALF_UP).doubleValue())));
                                                                // criando item da barca
                                                                barca.getItens().add(new SdItensBarca(barca, matComb.getNumeroof(), matComb.getCormaterialpai(),
                                                                        matComb.getLoteof(), matComb.getCorTinturaria(), matComb.getCustoTinturaria(), matComb.getInsumo().getCodigo(),
                                                                        matComb.getMaterialNecessidade(), matComb.getCorinsumo().getCor(), matComb.getCorMaterialNecessidade(),
                                                                        matComb.getDeposito(), consumoQtdeBarca, consumoEstoqueQtdeBarca, matComb.isPrincipal(), matComb.isContrastante(),
                                                                        matComb.getInsumo().getLinha()));
                                                                qtdeParaAbatimento.set(qtdeParaAbatimento.get().subtract(consumoEstoqueQtdeBarca));
                                                            }
                                                        } else {
                                                            BigDecimal consumoFinal = matComb.getConsumoestq().subtract(matComb.getConsumidoEstqBarca());
                                                            if (consumoFinal.compareTo(BigDecimal.ZERO) > 0) {
                                                                if (consumoFinal.compareTo(qtdeParaAbatimento.get()) > 0)
                                                                    consumoFinal = qtdeParaAbatimento.get();
                                                                consumoQtdeBarca = consumoFinal.divide(matComb.getInsumo().getPeso().compareTo(BigDecimal.ZERO) > 0
                                                                        ? matComb.getInsumo().getPeso()
                                                                        : BigDecimal.ONE, 3, RoundingMode.CEILING);
                                                                consumoEstoqueQtdeBarca = consumoFinal;
                                                                barca.getItens().add(new SdItensBarca(barca, matComb.getNumeroof(), matComb.getCormaterialpai(),
                                                                        matComb.getLoteof(), matComb.getCorTinturaria(), matComb.getCustoTinturaria(), matComb.getInsumo().getCodigo(),
                                                                        matComb.getMaterialNecessidade(), matComb.getCorinsumo().getCor(), matComb.getCorMaterialNecessidade(),
                                                                        matComb.getDeposito(), consumoQtdeBarca, consumoEstoqueQtdeBarca, matComb.isPrincipal(), matComb.isContrastante(),
                                                                        matComb.getInsumo().getLinha()));
                                                                qtdeParaAbatimento.set(qtdeParaAbatimento.get().subtract(consumoEstoqueQtdeBarca));
                                                            }
                                                        }

                                                        // abatimento da carga da OF no MRP para próximas barcas
                                                        BigDecimal finalConsumoQtdeBarca = consumoQtdeBarca;
                                                        BigDecimal finalConsumoEstoqueQtdeBarca = consumoEstoqueQtdeBarca;
                                                        materiaisOfBarca.get().stream()
                                                                .filter(mat -> mat.getInsumo().getCodigo().equals(matComb.getInsumo().getCodigo())
                                                                        && mat.getCorinsumo().getCor().equals(matComb.getCorinsumo().getCor())
                                                                        && mat.getNumeroof().equals(matComb.getNumeroof())
                                                                        && mat.getDeposito().equals(matComb.getDeposito())
                                                                        && mat.getCormaterialpai().equals(matComb.getCormaterialpai()))
                                                                .findAny()
                                                                .ifPresent(mat -> {
                                                                    mat.setConsumidoBarca(mat.getConsumidoBarca().add(finalConsumoQtdeBarca));
                                                                    mat.setConsumidoEstqBarca(mat.getConsumidoEstqBarca().add(finalConsumoEstoqueQtdeBarca));
                                                                });

                                                        if (qtdeParaAbatimento.get().compareTo(BigDecimal.ZERO) <= 0)
                                                            throw new BreakException();
                                                    } catch (SQLException throwables) {
                                                        throwables.printStackTrace();
                                                        ExceptionBox.build(message -> {
                                                            message.exception(throwables);
                                                            message.showAndWait();
                                                        });
                                                        throw new BreakException();
                                                    }
                                                });

                                                // teste para consumo da barca
                                                if (qtdeParaAbatimento.get().compareTo(BigDecimal.ZERO) <= 0)
                                                    throw new BreakException();

                                                // verificando materiais constrastantes combinado
                                                List<VSdMrpTinturariaV> materiaisContrastantesCombinado = materiaisOfBarca.get().stream()
                                                        .filter(mat -> mat.isContrastante() && !mat.isPrincipal())
                                                        .collect(Collectors.toList());
                                                materiaisContrastantesCombinado.forEach(matComb -> {
                                                    try {
                                                        List<Object> consumosProgramadoContrastante =
                                                                getProgramadoContrastante(programacaoBarca.get().getLote(), matComb.getNumeroof());
                                                        Integer qtdeProgramado = 0;
                                                        if (consumosProgramadoContrastante.size() > 0) {
                                                            qtdeProgramado = consumosProgramadoContrastante.stream()
                                                                    .mapToInt(matProg -> (Integer) ((Map<String, Object>) matProg).get("QTDE"))
                                                                    .min()
                                                                    .getAsInt();
                                                        }
                                                        BigDecimal consumoQtdeBarca = BigDecimal.ZERO;
                                                        BigDecimal consumoEstoqueQtdeBarca = BigDecimal.ZERO;
                                                        // consumo para barca
                                                        if (qtdeProgramado > 0) {
                                                            // definindo o consumo para abatimento do material
                                                            BigDecimal consumoFinal = new BigDecimal(qtdeProgramado)
                                                                    .multiply(matComb.getConsumoUnitario())
                                                                    .subtract(matComb.getConsumidoBarca());
                                                            if (consumoFinal.compareTo(BigDecimal.ZERO) > 0) {
                                                                // verificação se o consumo do estoque do material é maior que o consumo da barca, para utilizar o da barca
                                                                if (consumoFinal.multiply(matComb.getInsumo().getPeso().compareTo(BigDecimal.ZERO) > 0
                                                                        ? matComb.getInsumo().getPeso()
                                                                        : BigDecimal.ONE).compareTo(qtdeParaAbatimento.get()) > 0)
                                                                    consumoFinal = qtdeParaAbatimento.get();
                                                                // difine as qtde de consumos
                                                                consumoQtdeBarca = consumoFinal
                                                                        .divide(new BigDecimal(1 - (matComb.getInsumo().getIndtin()
                                                                                .divide(new BigDecimal(100), 9, RoundingMode.HALF_UP).doubleValue())));
                                                                consumoEstoqueQtdeBarca = consumoFinal
                                                                        .multiply((matComb.getInsumo().getPeso() != null
                                                                                && matComb.getInsumo().getPeso().compareTo(BigDecimal.ZERO) > 0)
                                                                                ? matComb.getInsumo().getPeso()
                                                                                : BigDecimal.ONE)
                                                                        .divide(new BigDecimal(1 - (matComb.getInsumo().getIndtin()
                                                                                .divide(new BigDecimal(100), 9, RoundingMode.HALF_UP).doubleValue())));
                                                                // criando item da barca
                                                                barca.getItens().add(new SdItensBarca(barca, matComb.getNumeroof(), matComb.getCormaterialpai(),
                                                                        matComb.getLoteof(), matComb.getCorTinturaria(), matComb.getCustoTinturaria(), matComb.getInsumo().getCodigo(),
                                                                        matComb.getMaterialNecessidade(), matComb.getCorinsumo().getCor(), matComb.getCorMaterialNecessidade(),
                                                                        matComb.getDeposito(), consumoQtdeBarca, consumoEstoqueQtdeBarca, matComb.isPrincipal(), matComb.isContrastante(),
                                                                        matComb.getInsumo().getLinha()));
                                                                qtdeParaAbatimento.set(qtdeParaAbatimento.get().subtract(consumoEstoqueQtdeBarca));
                                                            }
                                                        } else {
                                                            BigDecimal consumoFinal = matComb.getConsumoestq().subtract(matComb.getConsumidoEstqBarca());
                                                            if (consumoFinal.compareTo(BigDecimal.ZERO) > 0) {
                                                                if (consumoFinal.compareTo(qtdeParaAbatimento.get()) > 0)
                                                                    consumoFinal = qtdeParaAbatimento.get();
                                                                consumoQtdeBarca = consumoFinal.divide((matComb.getInsumo().getPeso() != null
                                                                        && matComb.getInsumo().getPeso().compareTo(BigDecimal.ZERO) > 0)
                                                                        ? matComb.getInsumo().getPeso()
                                                                        : BigDecimal.ONE, 3, RoundingMode.CEILING);
                                                                consumoEstoqueQtdeBarca = consumoFinal;
                                                                barca.getItens().add(new SdItensBarca(barca, matComb.getNumeroof(), matComb.getCormaterialpai(),
                                                                        matComb.getLoteof(), matComb.getCorTinturaria(), matComb.getCustoTinturaria(), matComb.getInsumo().getCodigo(),
                                                                        matComb.getMaterialNecessidade(), matComb.getCorinsumo().getCor(), matComb.getCorMaterialNecessidade(),
                                                                        matComb.getDeposito(), consumoQtdeBarca, consumoEstoqueQtdeBarca, matComb.isPrincipal(), matComb.isContrastante(),
                                                                        matComb.getInsumo().getLinha()));
                                                                qtdeParaAbatimento.set(qtdeParaAbatimento.get().subtract(consumoEstoqueQtdeBarca));
                                                            }
                                                        }

                                                        // abatimento da carga da OF no MRP para próximas barcas
                                                        BigDecimal finalConsumoQtdeBarca = consumoQtdeBarca;
                                                        BigDecimal finalConsumoEstoqueQtdeBarca = consumoEstoqueQtdeBarca;
                                                        materiaisOfBarca.get().stream()
                                                                .filter(mat -> mat.getInsumo().getCodigo().equals(matComb.getInsumo().getCodigo())
                                                                        && mat.getCorinsumo().getCor().equals(matComb.getCorinsumo().getCor())
                                                                        && mat.getNumeroof().equals(matComb.getNumeroof())
                                                                        && mat.getDeposito().equals(matComb.getDeposito())
                                                                        && mat.getCormaterialpai().equals(matComb.getCormaterialpai()))
                                                                .findAny()
                                                                .ifPresent(mat -> {
                                                                    mat.setConsumidoBarca(mat.getConsumidoBarca().add(finalConsumoQtdeBarca));
                                                                    mat.setConsumidoEstqBarca(mat.getConsumidoEstqBarca().add(finalConsumoEstoqueQtdeBarca));
                                                                });

                                                        if (qtdeParaAbatimento.get().compareTo(BigDecimal.ZERO) <= 0)
                                                            throw new BreakException();
                                                    } catch (SQLException throwables) {
                                                        throwables.printStackTrace();
                                                        ExceptionBox.build(message -> {
                                                            message.exception(throwables);
                                                            message.showAndWait();
                                                        });
                                                        throw new BreakException();
                                                    }
                                                });

                                                // teste para consumo da barca
                                                if (qtdeParaAbatimento.get().compareTo(BigDecimal.ZERO) <= 0)
                                                    throw new BreakException();

                                                // definindo o rank dos pedidos com mais peças na caixa
                                                Map<String, Double> rankOf = materiaisOfBarca.get().stream()
                                                        .filter(mat -> !Stream.concat(materiaisContrastantesCombinado.stream(), materiaisContrastantesPrincipal.stream())
                                                                .map(VSdMrpTinturariaV::getNumeroof)
                                                                .distinct()
                                                                .collect(Collectors.toList())
                                                                .contains(mat.getNumeroof()))
                                                        .collect(Collectors.groupingBy(it -> it.getNumeroof(),
                                                                Collectors.summingDouble(mapper -> mapper.getConsumoestq().setScale(3, RoundingMode.HALF_UP)
                                                                        .subtract(mapper.getConsumidoEstqBarca().setScale(3, RoundingMode.HALF_UP)).doubleValue())));
                                                //Sort map rankOf e forEach para abatimento
                                                rankOf.entrySet().stream()
                                                        .sorted(Map.Entry.<String, Double>comparingByValue())
                                                        .filter(mapOf -> mapOf.getValue() > 0.0)
                                                        .forEachOrdered(numeroOf -> {
                                                            List<VSdMrpTinturariaV> materiaisOf = materiaisMapOfBarca.get().get(numeroOf.getKey());
                                                            AtomicReference<Integer> qtdePecasMatPrincipal = new AtomicReference<>(0);
                                                            AtomicReference<BigDecimal> qtdeAbatimento = new AtomicReference<>(BigDecimal.ZERO);
                                                            AtomicReference<BigDecimal> qtdeAbatida = new AtomicReference<>(BigDecimal.ZERO);
                                                            try {
                                                                materiaisOf.stream().filter(VSdMrpTinturariaV::isPrincipal).forEach(material -> {
                                                                    // set da qtde de consumo para a barca do material
                                                                    qtdeAbatimento.set(material.getConsumoestq()
                                                                            .subtract(material.getConsumidoEstqBarca())
                                                                            .compareTo(qtdeParaAbatimento.get()) > 0
                                                                            ? qtdeParaAbatimento.get().subtract(qtdeAbatida.get())
                                                                            : material.getConsumoestq().subtract(material.getConsumidoEstqBarca()));
                                                                    qtdePecasMatPrincipal.set(
                                                                            qtdeAbatimento.get().multiply(new BigDecimal(1.0 - (material.getInsumo().getIndtin().divide(new BigDecimal(100), 9, RoundingMode.HALF_UP).doubleValue())))
                                                                                    .divide(material.getConsumoUnitario(), 0, RoundingMode.CEILING)
                                                                                    .intValue());
                                                                    qtdeAbatida.set(qtdeAbatida.get().add(qtdeAbatimento.get()));
                                                                    // cálculo do total de consumo para a barca (unidade material)
                                                                    BigDecimal consumoQtdeBarca = new BigDecimal(qtdePecasMatPrincipal.get())
                                                                            .multiply(material.getConsumoUnitario())
                                                                            .divide(new BigDecimal(1.0 - (material.getInsumo().getIndtin().divide(new BigDecimal(100), 9, RoundingMode.HALF_UP).doubleValue())), 3, RoundingMode.HALF_UP);
                                                                    // cálculo do total de consumo para a barca (unidade estoque)
                                                                    BigDecimal consumoEstoqueQtdeBarca = new BigDecimal(qtdePecasMatPrincipal.get())
                                                                            .multiply(material.getConsumoUnitario())
                                                                            .divide(new BigDecimal(1.0 - (material.getInsumo().getIndtin().divide(new BigDecimal(100), 9, RoundingMode.HALF_UP).doubleValue())), 3, RoundingMode.HALF_UP)
                                                                            .multiply(material.getUnidade().equals(material.getUnidadeestq()) ? BigDecimal.ONE : material.getInsumo().getPeso());
                                                                    // criação dos itens da barca
                                                                    SdItensBarca itemBarca = new SdItensBarca(barca, material.getNumeroof(), material.getCormaterialpai(), material.getLoteof(),
                                                                            material.getCorTinturaria(), material.getCustoTinturaria(), material.getInsumo().getCodigo(), material.getMaterialNecessidade(),
                                                                            material.getCorinsumo().getCor(), material.getCorMaterialNecessidade(), material.getDeposito(), consumoQtdeBarca, consumoEstoqueQtdeBarca,
                                                                            material.isPrincipal(), material.isContrastante(), material.getInsumo().getLinha());
                                                                    barca.getItens().add(itemBarca);

                                                                    // abatimento da carga da OF no MRP para próximas barcas
                                                                    materiaisOfBarca.get().stream()
                                                                            .filter(mat -> mat.getInsumo().getCodigo().equals(material.getInsumo().getCodigo())
                                                                                    && mat.getCorinsumo().getCor().equals(material.getCorinsumo().getCor())
                                                                                    && mat.getNumeroof().equals(material.getNumeroof())
                                                                                    && mat.getDeposito().equals(material.getDeposito())
                                                                                    && mat.getCormaterialpai().equals(material.getCormaterialpai()))
                                                                            .findAny()
                                                                            .ifPresent(mat -> {
                                                                                mat.setConsumidoBarca(mat.getConsumidoBarca().add(consumoQtdeBarca));
                                                                                mat.setConsumidoEstqBarca(mat.getConsumidoEstqBarca().add(consumoEstoqueQtdeBarca));
                                                                            });
                                                                    if (qtdeParaAbatimento.get().subtract(qtdeAbatida.get()).compareTo(BigDecimal.ZERO) <= 0)
                                                                        throw new ContinueException();
                                                                });
                                                            } catch (ContinueException ce) {}
                                                            materiaisOf.stream().filter(material -> !material.isPrincipal()).forEach(material -> {
                                                                // cálculo do total de consumo para a barca (unidade material)
                                                                BigDecimal consumoQtdeBarca = new BigDecimal(qtdePecasMatPrincipal.get().doubleValue() *
                                                                        (material.getConsumido().divide(
                                                                                new BigDecimal(materiaisOf.stream().filter(mat -> !mat.isPrincipal())
                                                                                        .mapToDouble(mat -> mat.getConsumido().doubleValue()).sum()), 9, RoundingMode.HALF_UP)).doubleValue()).setScale(0, RoundingMode.HALF_UP)
                                                                        .multiply(material.getConsumoUnitario());
                                                                // cálculo do total de consumo para a barca (unidade estoque)
                                                                BigDecimal consumoEstoqueQtdeBarca = new BigDecimal(qtdePecasMatPrincipal.get().doubleValue() *
                                                                        (material.getConsumido().divide(new BigDecimal(materiaisOf.stream()
                                                                                .filter(mat -> !mat.isPrincipal())
                                                                                .mapToDouble(mat -> mat.getConsumido().doubleValue()).sum()), 9, RoundingMode.HALF_UP)).doubleValue()).setScale(0, RoundingMode.HALF_UP)
                                                                        .multiply(material.getConsumoUnitario()).multiply(!material.getUnidade().equals("KG") ? material.getInsumo().getPeso() : BigDecimal.ONE);
                                                                // criação dos itens da barca
                                                                SdItensBarca itemBarca = new SdItensBarca(barca, material.getNumeroof(), material.getCormaterialpai(), material.getLoteof(),
                                                                        material.getCorTinturaria(), material.getCustoTinturaria(), material.getInsumo().getCodigo(), material.getMaterialNecessidade(),
                                                                        material.getCorinsumo().getCor(), material.getCorMaterialNecessidade(), material.getDeposito(), consumoQtdeBarca, consumoEstoqueQtdeBarca,
                                                                        material.isPrincipal(), material.isContrastante(), material.getInsumo().getLinha());
                                                                barca.getItens().add(itemBarca);

                                                                // abatimento da carga da OF no MRP para próximas barcas
                                                                materiaisOfBarca.get().stream()
                                                                        .filter(mat -> mat.getInsumo().getCodigo().equals(material.getInsumo().getCodigo())
                                                                                && mat.getCorinsumo().getCor().equals(material.getCorinsumo().getCor())
                                                                                && mat.getNumeroof().equals(material.getNumeroof())
                                                                                && mat.getDeposito().equals(material.getDeposito())
                                                                                && mat.getCormaterialpai().equals(material.getCormaterialpai()))
                                                                        .findAny()
                                                                        .ifPresent(mat -> {
                                                                            mat.setConsumidoBarca(mat.getConsumidoBarca().add(consumoQtdeBarca));
                                                                            mat.setConsumidoEstqBarca(mat.getConsumidoEstqBarca().add(consumoEstoqueQtdeBarca));
                                                                        });
                                                            });

                                                            qtdeParaAbatimento.set(qtdeParaAbatimento.get().subtract(qtdeAbatida.get()));
                                                            if (qtdeParaAbatimento.get().compareTo(BigDecimal.ZERO) <= 0)
                                                                throw new BreakException();
                                                        });
                                            } catch (BreakException e) {
                                            }
                                        }
                                        else if (!usaOutrosLotes) {
                                            AtomicReference<BigDecimal> qtdeParaAbatimento = new AtomicReference<>(qtdeBarcaAtendeRolos.get());
                                            AtomicReference<BigDecimal> qtdeConsumosMatPrincipal = new AtomicReference<>(
                                                    new BigDecimal(materiaisOfBarca.get().stream()
                                                            .filter(VSdMrpTinturariaV::isPrincipal)
                                                            .mapToDouble(mat -> mat.getConsumoestq().subtract(mat.getConsumidoEstqBarca()).doubleValue())
                                                            .sum()));

                                            // definindo o rank das OFs com maior consumo de materiais contrastantes
                                            Map<String, Double> rankOfContrastante = materiaisOfBarca.get().stream()
                                                    .filter(VSdMrpTinturariaV::isContrastante)
                                                    .collect(Collectors.groupingBy(it -> it.getNumeroof(),
                                                            Collectors.summingDouble(mapper -> mapper.getConsumoestq().setScale(3, RoundingMode.HALF_UP)
                                                                    .subtract(mapper.getConsumidoEstqBarca().setScale(3, RoundingMode.HALF_UP)).doubleValue())));
                                            rankOfContrastante.entrySet().stream()
                                                    .sorted(Map.Entry.<String, Double>comparingByValue())
                                                    .filter(mapOf -> mapOf.getValue() > 0.0)
                                                    .forEachOrdered(numeroOf -> {
                                                        List<VSdMrpTinturariaV> materiaisOf = materiaisMapOfBarca.get().get(numeroOf.getKey());
                                                        AtomicReference<BigDecimal> qtdeAbatimento = new AtomicReference<>(BigDecimal.ZERO);
                                                        materiaisOf.stream()
                                                                .filter(material -> material.isPrincipal() && material.isContrastante())
                                                                .forEach(material -> {
                                                                    try {
                                                                        List<Object> consumosProgramadoContrastante =
                                                                                getProgramadoContrastante(programacaoBarca.get().getLote(), numeroOf.getKey());

                                                                        if (consumosProgramadoContrastante.size() > 0) {
                                                                            Integer qtdeProgramado = consumosProgramadoContrastante.stream()
                                                                                    .mapToInt(matProg -> (Integer) ((Map<String, Object>) matProg).get("QTDE"))
                                                                                    .min()
                                                                                    .getAsInt();
                                                                            BigDecimal consumoContrastante = new BigDecimal(qtdeProgramado).multiply(material.getConsumoUnitario())
                                                                                    .divide(BigDecimal.ONE
                                                                                                    .subtract(material.getInsumo().getIndtin()
                                                                                                            .divide(new BigDecimal(100), 9, RoundingMode.HALF_UP)),
                                                                                            3, RoundingMode.HALF_UP);
                                                                            BigDecimal consumoEstqContrastante = new BigDecimal(qtdeProgramado).multiply(material.getConsumoUnitario())
                                                                                    .multiply((material.getInsumo().getPeso() != null
                                                                                            && material.getInsumo().getPeso().compareTo(BigDecimal.ZERO) > 0)
                                                                                            ? material.getInsumo().getPeso()
                                                                                            : BigDecimal.ONE)
                                                                                    .divide(BigDecimal.ONE
                                                                                                    .subtract(material.getInsumo().getIndtin()
                                                                                                            .divide(new BigDecimal(100), 9, RoundingMode.HALF_UP)),
                                                                                            3, RoundingMode.HALF_UP);
                                                                            // criando item da barca
                                                                            barca.getItens().add(new SdItensBarca(barca, material.getNumeroof(), material.getCormaterialpai(),
                                                                                    material.getLoteof(), material.getCorTinturaria(), material.getCustoTinturaria(), material.getInsumo().getCodigo(),
                                                                                    material.getMaterialNecessidade(), material.getCorinsumo().getCor(), material.getCorMaterialNecessidade(), material.getDeposito(),
                                                                                    consumoContrastante, consumoEstqContrastante, material.isPrincipal(), material.isContrastante(),
                                                                                    material.getInsumo().getLinha()));
                                                                            qtdeBarcaAtendeRolos.set(qtdeBarcaAtendeRolos.get().subtract(consumoEstqContrastante));
                                                                            qtdeConsumosMatPrincipal.set(qtdeConsumosMatPrincipal.get().subtract(material.getConsumoestq()));

                                                                            qtdeParaAbatimento.set(qtdeParaAbatimento.get().subtract(consumoEstqContrastante));
                                                                        } else {
                                                                            BigDecimal consumoContrastante = qtdeBarcaAtendeRolos.get().multiply(material.getConsumoestq()
                                                                                    .subtract(material.getConsumidoEstqBarca())
                                                                                    .divide(qtdeConsumosMatPrincipal.get(), 9, RoundingMode.HALF_UP))
                                                                                    .divide((material.getInsumo().getPeso() != null
                                                                                            && material.getInsumo().getPeso().compareTo(BigDecimal.ZERO) > 0)
                                                                                            ? material.getInsumo().getPeso()
                                                                                            : BigDecimal.ONE, 3, RoundingMode.CEILING);
                                                                            BigDecimal consumoEstqContrastante = qtdeBarcaAtendeRolos.get().multiply(material.getConsumoestq()
                                                                                    .subtract(material.getConsumidoEstqBarca())
                                                                                    .divide(qtdeConsumosMatPrincipal.get(), 9, RoundingMode.HALF_UP));
                                                                            // criando item da barca
                                                                            barca.getItens().add(new SdItensBarca(barca, material.getNumeroof(), material.getCormaterialpai(),
                                                                                    material.getLoteof(), material.getCorTinturaria(), material.getCustoTinturaria(), material.getInsumo().getCodigo(),
                                                                                    material.getMaterialNecessidade(), material.getCorinsumo().getCor(), material.getCorMaterialNecessidade(),
                                                                                    material.getDeposito(), consumoContrastante, consumoEstqContrastante, material.isPrincipal(), material.isContrastante(),
                                                                                    material.getInsumo().getLinha()));

                                                                            qtdeParaAbatimento.set(qtdeParaAbatimento.get().subtract(consumoEstqContrastante));
                                                                        }
                                                                    } catch (SQLException exc) {
                                                                        exc.printStackTrace();
                                                                        ExceptionBox.build(ebox -> {
                                                                            ebox.exception(exc);
                                                                            ebox.showAndWait();
                                                                        });
                                                                    }
                                                                });
                                                        materiaisOf.stream()
                                                                .filter(material -> !material.isPrincipal() && material.isContrastante())
                                                                .forEach(material -> {
                                                                    try {
                                                                        List<Object> consumosProgramadoContrastante =
                                                                                getProgramadoContrastante(programacaoBarca.get().getLote(), numeroOf.getKey());

                                                                        if (consumosProgramadoContrastante.size() > 0) {
                                                                            Integer qtdeProgramado = consumosProgramadoContrastante.stream()
                                                                                    .mapToInt(matProg -> (Integer) ((Map<String, Object>) matProg).get("QTDE"))
                                                                                    .min()
                                                                                    .getAsInt();
                                                                            BigDecimal consumoContrastante = new BigDecimal(qtdeProgramado).multiply(material.getConsumoUnitario())
                                                                                    .divide(BigDecimal.ONE
                                                                                                    .subtract(material.getInsumo().getIndtin()
                                                                                                            .divide(new BigDecimal(100), 9, RoundingMode.HALF_UP)),
                                                                                            3, RoundingMode.HALF_UP);
                                                                            BigDecimal consumoEstqContrastante = new BigDecimal(qtdeProgramado).multiply(material.getConsumoUnitario())
                                                                                    .multiply((material.getInsumo().getPeso() != null
                                                                                            && material.getInsumo().getPeso().compareTo(BigDecimal.ZERO) > 0)
                                                                                            ? material.getInsumo().getPeso()
                                                                                            : BigDecimal.ONE)
                                                                                    .divide(BigDecimal.ONE
                                                                                                    .subtract(material.getInsumo().getIndtin()
                                                                                                            .divide(new BigDecimal(100), 9, RoundingMode.HALF_UP)),
                                                                                            3, RoundingMode.HALF_UP);
                                                                            // criando item da barca
                                                                            barca.getItens().add(new SdItensBarca(barca, material.getNumeroof(), material.getCormaterialpai(),
                                                                                    material.getLoteof(), material.getCorTinturaria(), material.getCustoTinturaria(), material.getInsumo().getCodigo(),
                                                                                    material.getMaterialNecessidade(), material.getCorinsumo().getCor(), material.getCorMaterialNecessidade(),
                                                                                    material.getDeposito(), consumoContrastante, consumoEstqContrastante, material.isPrincipal(), material.isContrastante(),
                                                                                    material.getInsumo().getLinha()));
                                                                        } else {
                                                                            BigDecimal consumoContrastante = qtdeBarcaAtendeRolos.get().multiply(material.getConsumoestq()
                                                                                    .subtract(material.getConsumidoEstqBarca())
                                                                                    .divide(qtdeConsumosMatPrincipal.get()
                                                                                                    .add(new BigDecimal(materiaisOfBarca.get().stream()
                                                                                                            .filter(mat -> mat.isContrastante() && !mat.isPrincipal())
                                                                                                            .mapToDouble(mat -> mat.getConsumoestq().doubleValue())
                                                                                                            .sum())),
                                                                                            9, RoundingMode.HALF_UP))
                                                                                    .divide((material.getInsumo().getPeso() != null
                                                                                            && material.getInsumo().getPeso().compareTo(BigDecimal.ZERO) > 0)
                                                                                            ? material.getInsumo().getPeso()
                                                                                            : BigDecimal.ONE, 3, RoundingMode.CEILING);
                                                                            BigDecimal consumoEstqContrastante = qtdeBarcaAtendeRolos.get().multiply(material.getConsumoestq()
                                                                                    .subtract(material.getConsumidoEstqBarca())
                                                                                    .divide(qtdeConsumosMatPrincipal.get()
                                                                                            .add(new BigDecimal(materiaisOfBarca.get().stream()
                                                                                                    .filter(mat -> mat.isContrastante() && !mat.isPrincipal())
                                                                                                    .mapToDouble(mat -> mat.getConsumoestq().doubleValue())
                                                                                                    .sum())), 9, RoundingMode.HALF_UP));
                                                                            // criando item da barca
                                                                            barca.getItens().add(new SdItensBarca(barca, material.getNumeroof(), material.getCormaterialpai(),
                                                                                    material.getLoteof(), material.getCorTinturaria(), material.getCustoTinturaria(), material.getInsumo().getCodigo(),
                                                                                    material.getMaterialNecessidade(), material.getCorinsumo().getCor(), material.getCorMaterialNecessidade(),
                                                                                    material.getDeposito(), consumoContrastante, consumoEstqContrastante, material.isPrincipal(), material.isContrastante(),
                                                                                    material.getInsumo().getLinha()));
                                                                        }
                                                                    } catch (SQLException exc) {
                                                                        exc.printStackTrace();
                                                                        ExceptionBox.build(ebox -> {
                                                                            ebox.exception(exc);
                                                                            ebox.showAndWait();
                                                                        });
                                                                    }
                                                                });
                                                    });

                                            // definindo o rank dos pedidos com mais peças na caixa
                                            Map<String, Double> rankOf = materiaisOfBarca.get().stream()
                                                    .collect(Collectors.groupingBy(it -> it.getNumeroof(),
                                                            Collectors.summingDouble(mapper -> mapper.getConsumoestq().setScale(3, RoundingMode.HALF_UP)
                                                                    .subtract(mapper.getConsumidoEstqBarca().setScale(3, RoundingMode.HALF_UP)).doubleValue())));
                                            //Sort map rankOf e forEach para abatimento
                                            rankOf.entrySet().stream()
                                                    .sorted(Map.Entry.<String, Double>comparingByValue())
                                                    .filter(mapOf -> mapOf.getValue() > 0.0)
                                                    .forEachOrdered(numeroOf -> {
                                                        List<VSdMrpTinturariaV> materiaisOf = materiaisMapOfBarca.get().get(numeroOf.getKey());
                                                        AtomicReference<Integer> qtdePecasMatPrincipal = new AtomicReference<>(0);
                                                        AtomicReference<BigDecimal> qtdeAbatimento = new AtomicReference<>(BigDecimal.ZERO);
                                                        materiaisOf.stream().filter(material -> material.isPrincipal() && !material.isContrastante())
                                                                .forEach(material -> {
                                                                    qtdeAbatimento.set(qtdeBarcaAtendeRolos.get().multiply(material.getConsumoestq()
                                                                            .subtract(material.getConsumidoEstqBarca())
                                                                            .divide(qtdeConsumosMatPrincipal.get(), 9, RoundingMode.HALF_UP))); // << aqui é o problema (tem que ser pelo saldo e não pelo total
                                                                    qtdePecasMatPrincipal.set(
                                                                            qtdeAbatimento.get().multiply(new BigDecimal(1.0 - (material.getInsumo().getIndtin()
                                                                                    .divide(new BigDecimal(100), 9, RoundingMode.HALF_UP).doubleValue())))
                                                                                    .divide(material.getConsumoUnitario(), 0, RoundingMode.CEILING)
                                                                                    .intValue());
                                                                    // cálculo do total de consumo para a barca (unidade material)
                                                                    BigDecimal consumoQtdeBarca = new BigDecimal(qtdePecasMatPrincipal.get())
                                                                            .multiply(material.getConsumoUnitario())
                                                                            .divide(new BigDecimal(1.0 - (material.getInsumo().getIndtin()
                                                                                    .divide(new BigDecimal(100), 9, RoundingMode.HALF_UP).doubleValue())), 3, RoundingMode.HALF_UP);
                                                                    // cálculo do total de consumo para a barca (unidade estoque)
                                                                    BigDecimal consumoEstoqueQtdeBarca = new BigDecimal(qtdePecasMatPrincipal.get())
                                                                            .multiply(material.getConsumoUnitario())
                                                                            .divide(new BigDecimal(1.0 - (material.getInsumo().getIndtin()
                                                                                    .divide(new BigDecimal(100), 9, RoundingMode.HALF_UP).doubleValue())), 3, RoundingMode.HALF_UP)
                                                                            .multiply(material.getUnidade().equals(material.getUnidadeestq()) ? BigDecimal.ONE : material.getInsumo().getPeso());
                                                                    // criação dos itens da barca
                                                                    SdItensBarca itemBarca = new SdItensBarca(barca, material.getNumeroof(), material.getCormaterialpai(), material.getLoteof(),
                                                                            material.getCorTinturaria(), material.getCustoTinturaria(), material.getInsumo().getCodigo(), material.getMaterialNecessidade(),
                                                                            material.getCorinsumo().getCor(), material.getCorMaterialNecessidade(),
                                                                            material.getDeposito(), consumoQtdeBarca, consumoEstoqueQtdeBarca,
                                                                            material.isPrincipal(), material.isContrastante(), material.getInsumo().getLinha());
                                                                    barca.getItens().add(itemBarca);

                                                                    // abatimento da carga da OF no MRP para próximas barcas
                                                                    materiaisOfBarca.get().stream()
                                                                            .filter(mat -> mat.getInsumo().getCodigo().equals(material.getInsumo().getCodigo())
                                                                                    && mat.getCorinsumo().getCor().equals(material.getCorinsumo().getCor())
                                                                                    && mat.getNumeroof().equals(material.getNumeroof())
                                                                                    && mat.getDeposito().equals(material.getDeposito())
                                                                                    && mat.getCormaterialpai().equals(material.getCormaterialpai()))
                                                                            .findAny()
                                                                            .ifPresent(mat -> {
                                                                                mat.setConsumidoBarca(mat.getConsumidoBarca().add(consumoQtdeBarca));
                                                                                mat.setConsumidoEstqBarca(mat.getConsumidoEstqBarca().add(consumoEstoqueQtdeBarca));
                                                                            });
                                                                });
                                                        materiaisOf.stream().filter(material -> !material.isPrincipal() && !material.isContrastante())
                                                                .forEach(material -> {
                                                                    // cálculo do total de consumo para a barca (unidade material)
                                                                    BigDecimal consumoQtdeBarca = new BigDecimal(qtdePecasMatPrincipal.get().doubleValue() *
                                                                            (material.getConsumido().divide(
                                                                                    new BigDecimal(materiaisOf.stream().filter(mat -> !mat.isPrincipal())
                                                                                            .mapToDouble(mat -> mat.getConsumido().doubleValue()).sum()), 9, RoundingMode.HALF_UP)).doubleValue()).setScale(0, RoundingMode.HALF_UP)
                                                                            .multiply(material.getConsumoUnitario());
                                                                    // cálculo do total de consumo para a barca (unidade estoque)
                                                                    BigDecimal consumoEstoqueQtdeBarca = new BigDecimal(qtdePecasMatPrincipal.get().doubleValue() *
                                                                            (material.getConsumido().divide(new BigDecimal(materiaisOf.stream()
                                                                                    .filter(mat -> !mat.isPrincipal())
                                                                                    .mapToDouble(mat -> mat.getConsumido().doubleValue()).sum()), 9, RoundingMode.HALF_UP)).doubleValue()).setScale(0, RoundingMode.HALF_UP)
                                                                            .multiply(material.getConsumoUnitario()).multiply(!material.getUnidade().equals("KG") ? material.getInsumo().getPeso() : BigDecimal.ONE);
                                                                    // criação dos itens da barca
                                                                    SdItensBarca itemBarca = new SdItensBarca(barca, material.getNumeroof(), material.getCormaterialpai(), material.getLoteof(),
                                                                            material.getCorTinturaria(), material.getCustoTinturaria(), material.getInsumo().getCodigo(), material.getMaterialNecessidade(),
                                                                            material.getCorinsumo().getCor(), material.getCorMaterialNecessidade(),
                                                                            material.getDeposito(), consumoQtdeBarca, consumoEstoqueQtdeBarca,
                                                                            material.isPrincipal(), material.isContrastante(), material.getInsumo().getLinha());
                                                                    barca.getItens().add(itemBarca);

                                                                    // abatimento da carga da OF no MRP para próximas barcas
                                                                    materiaisOfBarca.get().stream()
                                                                            .filter(mat -> mat.getInsumo().getCodigo().equals(material.getInsumo().getCodigo())
                                                                                    && mat.getCorinsumo().getCor().equals(material.getCorinsumo().getCor())
                                                                                    && mat.getNumeroof().equals(material.getNumeroof())
                                                                                    && mat.getDeposito().equals(material.getDeposito())
                                                                                    && mat.getCormaterialpai().equals(material.getCormaterialpai()))
                                                                            .findAny()
                                                                            .ifPresent(mat -> {
                                                                                mat.setConsumidoBarca(mat.getConsumidoBarca().add(consumoQtdeBarca));
                                                                                mat.setConsumidoEstqBarca(mat.getConsumidoEstqBarca().add(consumoEstoqueQtdeBarca));
                                                                            });
                                                                });

                                                        qtdeParaAbatimento.set(qtdeParaAbatimento.get().subtract(qtdeAbatimento.get()));
                                                    });
                                        }
                                        else {
                                            AtomicReference<BigDecimal> qtdeParaAbatimento = new AtomicReference<>(qtdeBarcaAtendeRolos.get());

                                            try {
                                                // verificando materiais contrastantes principal
                                                List<VSdMrpTinturariaV> materiaisContrastantesPrincipal = materiaisOfBarca.get().stream()
                                                        .filter(mat -> mat.isContrastante() && mat.isPrincipal())
                                                        .collect(Collectors.toList());
                                                materiaisContrastantesPrincipal.forEach(matComb -> {
                                                    try {
                                                        List<Object> consumosProgramadoContrastante =
                                                                getProgramadoContrastante(programacaoBarca.get().getLote(), matComb.getNumeroof());
                                                        Integer qtdeProgramado = 0;
                                                        if (consumosProgramadoContrastante.size() > 0) {
                                                            qtdeProgramado = consumosProgramadoContrastante.stream()
                                                                    .mapToInt(matProg -> (Integer) ((Map<String, Object>) matProg).get("QTDE"))
                                                                    .min()
                                                                    .getAsInt();
                                                        }
                                                        BigDecimal consumoQtdeBarca = BigDecimal.ZERO;
                                                        BigDecimal consumoEstoqueQtdeBarca = BigDecimal.ZERO;
                                                        // consumo para barca
                                                        if (qtdeProgramado > 0) {
                                                            // definindo o consumo para abatimento do material
                                                            BigDecimal consumoFinal = new BigDecimal(qtdeProgramado)
                                                                    .multiply(matComb.getConsumoUnitario())
                                                                    .subtract(matComb.getConsumidoBarca());
                                                            if (consumoFinal.compareTo(BigDecimal.ZERO) > 0) {
                                                                // verificação se o consumo do estoque do material é maior que o consumo da barca, para utilizar o da barca
                                                                if (consumoFinal.multiply(matComb.getInsumo().getPeso().compareTo(BigDecimal.ZERO) > 0
                                                                        ? matComb.getInsumo().getPeso()
                                                                        : BigDecimal.ONE).compareTo(qtdeParaAbatimento.get()) > 0)
                                                                    consumoFinal = qtdeParaAbatimento.get();
                                                                // difine as qtde de consumos
                                                                consumoQtdeBarca = consumoFinal
                                                                        .divide(new BigDecimal(1 - (matComb.getInsumo().getIndtin()
                                                                                .divide(new BigDecimal(100), 9, RoundingMode.HALF_UP).doubleValue())));
                                                                consumoEstoqueQtdeBarca = consumoFinal
                                                                        .multiply((matComb.getInsumo().getPeso() != null
                                                                                && matComb.getInsumo().getPeso().compareTo(BigDecimal.ZERO) > 0)
                                                                                ? matComb.getInsumo().getPeso()
                                                                                : BigDecimal.ONE)
                                                                        .divide(new BigDecimal(1 - (matComb.getInsumo().getIndtin()
                                                                                .divide(new BigDecimal(100), 9, RoundingMode.HALF_UP).doubleValue())));
                                                                // criando item da barca
                                                                barca.getItens().add(new SdItensBarca(barca, matComb.getNumeroof(), matComb.getCormaterialpai(),
                                                                        matComb.getLoteof(), matComb.getCorTinturaria(), matComb.getCustoTinturaria(), matComb.getInsumo().getCodigo(),
                                                                        matComb.getMaterialNecessidade(), matComb.getCorinsumo().getCor(), matComb.getCorMaterialNecessidade(),
                                                                        matComb.getDeposito(), consumoQtdeBarca, consumoEstoqueQtdeBarca, matComb.isPrincipal(), matComb.isContrastante(),
                                                                        matComb.getInsumo().getLinha()));
                                                                qtdeParaAbatimento.set(qtdeParaAbatimento.get().subtract(consumoEstoqueQtdeBarca));
                                                            }
                                                        } else {
                                                            BigDecimal consumoFinal = matComb.getConsumoestq().subtract(matComb.getConsumidoEstqBarca());
                                                            if (consumoFinal.compareTo(BigDecimal.ZERO) > 0) {
                                                                if (consumoFinal.compareTo(qtdeParaAbatimento.get()) > 0)
                                                                    consumoFinal = qtdeParaAbatimento.get();
                                                                consumoQtdeBarca = consumoFinal.divide((matComb.getInsumo().getPeso() != null
                                                                        && matComb.getInsumo().getPeso().compareTo(BigDecimal.ZERO) > 0)
                                                                        ? matComb.getInsumo().getPeso()
                                                                        : BigDecimal.ONE, 3, RoundingMode.CEILING);
                                                                consumoEstoqueQtdeBarca = consumoFinal;
                                                                barca.getItens().add(new SdItensBarca(barca, matComb.getNumeroof(), matComb.getCormaterialpai(),
                                                                        matComb.getLoteof(), matComb.getCorTinturaria(), matComb.getCustoTinturaria(), matComb.getInsumo().getCodigo(),
                                                                        matComb.getMaterialNecessidade(), matComb.getCorinsumo().getCor(), matComb.getCorMaterialNecessidade(),
                                                                        matComb.getDeposito(), consumoQtdeBarca, consumoEstoqueQtdeBarca, matComb.isPrincipal(), matComb.isContrastante(),
                                                                        matComb.getInsumo().getLinha()));
                                                                qtdeParaAbatimento.set(qtdeParaAbatimento.get().subtract(consumoEstoqueQtdeBarca));
                                                            }
                                                        }

                                                        // abatimento da carga da OF no MRP para próximas barcas
                                                        BigDecimal finalConsumoQtdeBarca = consumoQtdeBarca;
                                                        BigDecimal finalConsumoEstoqueQtdeBarca = consumoEstoqueQtdeBarca;
                                                        materiaisOfBarca.get().stream()
                                                                .filter(mat -> mat.getInsumo().getCodigo().equals(matComb.getInsumo().getCodigo())
                                                                        && mat.getCorinsumo().getCor().equals(matComb.getCorinsumo().getCor())
                                                                        && mat.getNumeroof().equals(matComb.getNumeroof())
                                                                        && mat.getDeposito().equals(matComb.getDeposito())
                                                                        && mat.getCormaterialpai().equals(matComb.getCormaterialpai()))
                                                                .findAny()
                                                                .ifPresent(mat -> {
                                                                    mat.setConsumidoBarca(mat.getConsumidoBarca().add(finalConsumoQtdeBarca));
                                                                    mat.setConsumidoEstqBarca(mat.getConsumidoEstqBarca().add(finalConsumoEstoqueQtdeBarca));
                                                                });

                                                        if (qtdeParaAbatimento.get().compareTo(BigDecimal.ZERO) <= 0)
                                                            throw new BreakException();
                                                    } catch (SQLException throwables) {
                                                        throwables.printStackTrace();
                                                        ExceptionBox.build(message -> {
                                                            message.exception(throwables);
                                                            message.showAndWait();
                                                        });
                                                        throw new BreakException();
                                                    }
                                                });

                                                // teste para consumo da barca
                                                if (qtdeParaAbatimento.get().compareTo(BigDecimal.ZERO) <= 0)
                                                    throw new BreakException();

                                                // verificando materiais constrastantes combinado
                                                List<VSdMrpTinturariaV> materiaisContrastantesCombinado = materiaisOfBarca.get().stream()
                                                        .filter(mat -> mat.isContrastante() && !mat.isPrincipal())
                                                        .collect(Collectors.toList());
                                                materiaisContrastantesCombinado.forEach(matComb -> {
                                                    try {
                                                        List<Object> consumosProgramadoContrastante =
                                                                getProgramadoContrastante(programacaoBarca.get().getLote(), matComb.getNumeroof());
                                                        Integer qtdeProgramado = 0;
                                                        if (consumosProgramadoContrastante.size() > 0) {
                                                            qtdeProgramado = consumosProgramadoContrastante.stream()
                                                                    .mapToInt(matProg -> (Integer) ((Map<String, Object>) matProg).get("QTDE"))
                                                                    .min()
                                                                    .getAsInt();
                                                        }
                                                        BigDecimal consumoQtdeBarca = BigDecimal.ZERO;
                                                        BigDecimal consumoEstoqueQtdeBarca = BigDecimal.ZERO;
                                                        // consumo para barca
                                                        if (qtdeProgramado > 0) {
                                                            // definindo o consumo para abatimento do material
                                                            BigDecimal consumoFinal = new BigDecimal(qtdeProgramado)
                                                                    .multiply(matComb.getConsumoUnitario())
                                                                    .subtract(matComb.getConsumidoBarca());
                                                            if (consumoFinal.compareTo(BigDecimal.ZERO) > 0) {
                                                                // verificação se o consumo do estoque do material é maior que o consumo da barca, para utilizar o da barca
                                                                if (consumoFinal.multiply(matComb.getInsumo().getPeso().compareTo(BigDecimal.ZERO) > 0
                                                                        ? matComb.getInsumo().getPeso()
                                                                        : BigDecimal.ONE).compareTo(qtdeParaAbatimento.get()) > 0)
                                                                    consumoFinal = qtdeParaAbatimento.get();
                                                                // difine as qtde de consumos
                                                                consumoQtdeBarca = consumoFinal
                                                                        .divide(new BigDecimal(1 - (matComb.getInsumo().getIndtin()
                                                                                .divide(new BigDecimal(100), 9, RoundingMode.HALF_UP).doubleValue())));
                                                                consumoEstoqueQtdeBarca = consumoFinal
                                                                        .multiply((matComb.getInsumo().getPeso() != null
                                                                                && matComb.getInsumo().getPeso().compareTo(BigDecimal.ZERO) > 0)
                                                                                ? matComb.getInsumo().getPeso()
                                                                                : BigDecimal.ONE)
                                                                        .divide(new BigDecimal(1 - (matComb.getInsumo().getIndtin()
                                                                                .divide(new BigDecimal(100), 9, RoundingMode.HALF_UP).doubleValue())));
                                                                // criando item da barca
                                                                barca.getItens().add(new SdItensBarca(barca, matComb.getNumeroof(), matComb.getCormaterialpai(),
                                                                        matComb.getLoteof(), matComb.getCorTinturaria(), matComb.getCustoTinturaria(), matComb.getInsumo().getCodigo(),
                                                                        matComb.getMaterialNecessidade(), matComb.getCorinsumo().getCor(), matComb.getCorMaterialNecessidade(),
                                                                        matComb.getDeposito(), consumoQtdeBarca, consumoEstoqueQtdeBarca, matComb.isPrincipal(), matComb.isContrastante(),
                                                                        matComb.getInsumo().getLinha()));
                                                                qtdeParaAbatimento.set(qtdeParaAbatimento.get().subtract(consumoEstoqueQtdeBarca));
                                                            }
                                                        } else {
                                                            BigDecimal consumoFinal = matComb.getConsumoestq().subtract(matComb.getConsumidoEstqBarca());
                                                            if (consumoFinal.compareTo(BigDecimal.ZERO) > 0) {
                                                                if (consumoFinal.compareTo(qtdeParaAbatimento.get()) > 0)
                                                                    consumoFinal = qtdeParaAbatimento.get();
                                                                consumoQtdeBarca = consumoFinal.divide((matComb.getInsumo().getPeso() != null
                                                                        && matComb.getInsumo().getPeso().compareTo(BigDecimal.ZERO) > 0)
                                                                        ? matComb.getInsumo().getPeso()
                                                                        : BigDecimal.ONE, 3, RoundingMode.CEILING);
                                                                consumoEstoqueQtdeBarca = consumoFinal;
                                                                barca.getItens().add(new SdItensBarca(barca, matComb.getNumeroof(), matComb.getCormaterialpai(),
                                                                        matComb.getLoteof(), matComb.getCorTinturaria(), matComb.getCustoTinturaria(), matComb.getInsumo().getCodigo(),
                                                                        matComb.getMaterialNecessidade(), matComb.getCorinsumo().getCor(), matComb.getCorMaterialNecessidade(),
                                                                        matComb.getDeposito(), consumoQtdeBarca, consumoEstoqueQtdeBarca, matComb.isPrincipal(), matComb.isContrastante(),
                                                                        matComb.getInsumo().getLinha()));
                                                                qtdeParaAbatimento.set(qtdeParaAbatimento.get().subtract(consumoEstoqueQtdeBarca));
                                                            }
                                                        }

                                                        // abatimento da carga da OF no MRP para próximas barcas
                                                        BigDecimal finalConsumoQtdeBarca = consumoQtdeBarca;
                                                        BigDecimal finalConsumoEstoqueQtdeBarca = consumoEstoqueQtdeBarca;
                                                        materiaisOfBarca.get().stream()
                                                                .filter(mat -> mat.getInsumo().getCodigo().equals(matComb.getInsumo().getCodigo())
                                                                        && mat.getCorinsumo().getCor().equals(matComb.getCorinsumo().getCor())
                                                                        && mat.getNumeroof().equals(matComb.getNumeroof())
                                                                        && mat.getDeposito().equals(matComb.getDeposito())
                                                                        && mat.getCormaterialpai().equals(matComb.getCormaterialpai()))
                                                                .findAny()
                                                                .ifPresent(mat -> {
                                                                    mat.setConsumidoBarca(mat.getConsumidoBarca().add(finalConsumoQtdeBarca));
                                                                    mat.setConsumidoEstqBarca(mat.getConsumidoEstqBarca().add(finalConsumoEstoqueQtdeBarca));
                                                                });

                                                        if (qtdeParaAbatimento.get().compareTo(BigDecimal.ZERO) <= 0)
                                                            throw new BreakException();
                                                    } catch (SQLException throwables) {
                                                        throwables.printStackTrace();
                                                        ExceptionBox.build(message -> {
                                                            message.exception(throwables);
                                                            message.showAndWait();
                                                        });
                                                        throw new BreakException();
                                                    }
                                                });

                                                // teste para consumo da barca
                                                if (qtdeParaAbatimento.get().compareTo(BigDecimal.ZERO) <= 0)
                                                    throw new BreakException();

                                                // definindo o rank das ofs com os primeiros periodos
                                                Map<String, Double> rankOf = materiaisOfBarca.get().stream()
                                                        .filter(mat -> !Stream.concat(materiaisContrastantesCombinado.stream(), materiaisContrastantesPrincipal.stream())
                                                                .map(VSdMrpTinturariaV::getNumeroof)
                                                                .distinct()
                                                                .collect(Collectors.toList())
                                                                .contains(mat.getNumeroof()))
                                                        .collect(Collectors.groupingBy(it -> it.getNumeroof(),
                                                                Collectors.summingDouble(mapper -> mapper.getConsumoestq().setScale(3, RoundingMode.HALF_UP)
                                                                        .subtract(mapper.getConsumidoEstqBarca().setScale(3, RoundingMode.HALF_UP)).doubleValue())));
                                                Map<String, String> rankOfSaldo = materiaisOfBarcaOutrosLotes.get().stream()
                                                        .collect(Collectors.groupingBy(VSdMrpTinturariaV::getNumeroof,
                                                                Collectors.mapping(VSdMrpTinturariaV::getLoteof, Collectors.joining(","))));

                                                //Sort map rankOf e forEach para abatimento
                                                rankOf.entrySet().stream()
                                                        .sorted(Map.Entry.<String, Double>comparingByValue())
                                                        .filter(mapOf -> mapOf.getValue() > 0.0)
                                                        .forEachOrdered(numeroOf -> {
                                                            List<VSdMrpTinturariaV> materiaisOf = materiaisMapOfBarca.get().get(numeroOf.getKey());
                                                            AtomicReference<Integer> qtdePecasMatPrincipal = new AtomicReference<>(0);
                                                            AtomicReference<BigDecimal> qtdeAbatimento = new AtomicReference<>(BigDecimal.ZERO);
                                                            AtomicReference<BigDecimal> qtdeAbatida = new AtomicReference<>(BigDecimal.ZERO);
                                                            try {
                                                                materiaisOf.stream().filter(VSdMrpTinturariaV::isPrincipal).forEach(material -> {
                                                                    // set da qtde de consumo para a barca do material
                                                                    qtdeAbatimento.set(material.getConsumoestq()
                                                                            .subtract(material.getConsumidoEstqBarca())
                                                                            .compareTo(qtdeParaAbatimento.get()) > 0
                                                                            ? qtdeParaAbatimento.get().subtract(qtdeAbatida.get())
                                                                            : material.getConsumoestq().subtract(material.getConsumidoEstqBarca()));
                                                                    qtdePecasMatPrincipal.set(
                                                                            qtdeAbatimento.get().multiply(new BigDecimal(1.0 - (material.getInsumo().getIndtin().divide(new BigDecimal(100), 9, RoundingMode.HALF_UP).doubleValue())))
                                                                                    .divide(material.getConsumoUnitario(), 0, RoundingMode.CEILING)
                                                                                    .intValue());
                                                                    qtdeAbatida.set(qtdeAbatida.get().add(qtdeAbatimento.get()));
                                                                    // cálculo do total de consumo para a barca (unidade material)
                                                                    BigDecimal consumoQtdeBarca = new BigDecimal(qtdePecasMatPrincipal.get())
                                                                            .multiply(material.getConsumoUnitario())
                                                                            .divide(new BigDecimal(1.0 - (material.getInsumo().getIndtin().divide(new BigDecimal(100), 9, RoundingMode.HALF_UP).doubleValue())), 3, RoundingMode.HALF_UP);
                                                                    // cálculo do total de consumo para a barca (unidade estoque)
                                                                    BigDecimal consumoEstoqueQtdeBarca = new BigDecimal(qtdePecasMatPrincipal.get())
                                                                            .multiply(material.getConsumoUnitario())
                                                                            .divide(new BigDecimal(1.0 - (material.getInsumo().getIndtin().divide(new BigDecimal(100), 9, RoundingMode.HALF_UP).doubleValue())), 3, RoundingMode.HALF_UP)
                                                                            .multiply(material.getUnidade().equals(material.getUnidadeestq()) ? BigDecimal.ONE : material.getInsumo().getPeso());
                                                                    // criação dos itens da barca
                                                                    SdItensBarca itemBarca = new SdItensBarca(barca, material.getNumeroof(), material.getCormaterialpai(), material.getLoteof(),
                                                                            material.getCorTinturaria(), material.getCustoTinturaria(), material.getInsumo().getCodigo(), material.getMaterialNecessidade(),
                                                                            material.getCorinsumo().getCor(), material.getCorMaterialNecessidade(), material.getDeposito(), consumoQtdeBarca, consumoEstoqueQtdeBarca,
                                                                            material.isPrincipal(), material.isContrastante(), material.getInsumo().getLinha());
                                                                    barca.getItens().add(itemBarca);

                                                                    // abatimento da carga da OF no MRP para próximas barcas
                                                                    materiaisOfBarca.get().stream()
                                                                            .filter(mat -> mat.getInsumo().getCodigo().equals(material.getInsumo().getCodigo())
                                                                                    && mat.getCorinsumo().getCor().equals(material.getCorinsumo().getCor())
                                                                                    && mat.getNumeroof().equals(material.getNumeroof())
                                                                                    && mat.getDeposito().equals(material.getDeposito())
                                                                                    && mat.getCormaterialpai().equals(material.getCormaterialpai()))
                                                                            .findAny()
                                                                            .ifPresent(mat -> {
                                                                                mat.setConsumidoBarca(mat.getConsumidoBarca().add(consumoQtdeBarca));
                                                                                mat.setConsumidoEstqBarca(mat.getConsumidoEstqBarca().add(consumoEstoqueQtdeBarca));
                                                                            });
                                                                    if (qtdeParaAbatimento.get().subtract(qtdeAbatida.get()).compareTo(BigDecimal.ZERO) <= 0)
                                                                        throw new ContinueException();
                                                                });
                                                            } catch (ContinueException ce) {}
                                                            materiaisOf.stream().filter(material -> !material.isPrincipal()).forEach(material -> {
                                                                // cálculo do total de consumo para a barca (unidade material)
                                                                BigDecimal consumoQtdeBarca = new BigDecimal(qtdePecasMatPrincipal.get().doubleValue() *
                                                                        (material.getConsumido().divide(
                                                                                new BigDecimal(materiaisOf.stream().filter(mat -> !mat.isPrincipal())
                                                                                        .mapToDouble(mat -> mat.getConsumido().doubleValue()).sum()), 9, RoundingMode.HALF_UP)).doubleValue()).setScale(0, RoundingMode.HALF_UP)
                                                                        .multiply(material.getConsumoUnitario());
                                                                // cálculo do total de consumo para a barca (unidade estoque)
                                                                BigDecimal consumoEstoqueQtdeBarca = new BigDecimal(qtdePecasMatPrincipal.get().doubleValue() *
                                                                        (material.getConsumido().divide(new BigDecimal(materiaisOf.stream()
                                                                                .filter(mat -> !mat.isPrincipal())
                                                                                .mapToDouble(mat -> mat.getConsumido().doubleValue()).sum()), 9, RoundingMode.HALF_UP)).doubleValue()).setScale(0, RoundingMode.HALF_UP)
                                                                        .multiply(material.getConsumoUnitario()).multiply(!material.getUnidade().equals("KG") ? material.getInsumo().getPeso() : BigDecimal.ONE);
                                                                // criação dos itens da barca
                                                                SdItensBarca itemBarca = new SdItensBarca(barca, material.getNumeroof(), material.getCormaterialpai(), material.getLoteof(),
                                                                        material.getCorTinturaria(), material.getCustoTinturaria(), material.getInsumo().getCodigo(), material.getMaterialNecessidade(),
                                                                        material.getCorinsumo().getCor(), material.getCorMaterialNecessidade(), material.getDeposito(), consumoQtdeBarca, consumoEstoqueQtdeBarca,
                                                                        material.isPrincipal(), material.isContrastante(), material.getInsumo().getLinha());
                                                                barca.getItens().add(itemBarca);

                                                                // abatimento da carga da OF no MRP para próximas barcas
                                                                materiaisOfBarca.get().stream()
                                                                        .filter(mat -> mat.getInsumo().getCodigo().equals(material.getInsumo().getCodigo())
                                                                                && mat.getCorinsumo().getCor().equals(material.getCorinsumo().getCor())
                                                                                && mat.getNumeroof().equals(material.getNumeroof())
                                                                                && mat.getDeposito().equals(material.getDeposito())
                                                                                && mat.getCormaterialpai().equals(material.getCormaterialpai()))
                                                                        .findAny()
                                                                        .ifPresent(mat -> {
                                                                            mat.setConsumidoBarca(mat.getConsumidoBarca().add(consumoQtdeBarca));
                                                                            mat.setConsumidoEstqBarca(mat.getConsumidoEstqBarca().add(consumoEstoqueQtdeBarca));
                                                                        });
                                                            });

                                                            qtdeParaAbatimento.set(qtdeParaAbatimento.get().subtract(qtdeAbatida.get()));
                                                            if (qtdeParaAbatimento.get().compareTo(BigDecimal.ZERO) <= 0)
                                                                throw new BreakException();
                                                        });

                                                rankOfSaldo.entrySet().stream()
                                                        .sorted(Map.Entry.<String, String>comparingByValue())
                                                        .forEachOrdered(numeroOf -> {
                                                            List<VSdMrpTinturariaV> materiaisOf = materiaisMapOfBarcaOutrosLotes.get().get(numeroOf.getKey());
                                                            if (materiais.stream().mapToDouble(mat -> mat.getConsumoestq()
                                                                    .subtract(mat.getConsumidoEstqBarca()).doubleValue()).sum() > 0) {
                                                                AtomicReference<Integer> qtdePecasMatPrincipal = new AtomicReference<>(0);
                                                                AtomicReference<BigDecimal> qtdeAbatimento = new AtomicReference<>(BigDecimal.ZERO);
                                                                AtomicReference<BigDecimal> qtdeAbatida = new AtomicReference<>(BigDecimal.ZERO);
                                                                try {
                                                                    materiaisOf.stream().filter(VSdMrpTinturariaV::isPrincipal).forEach(material -> {
                                                                        // set da qtde de consumo para a barca do material
                                                                        qtdeAbatimento.set(material.getConsumoestq()
                                                                                .subtract(material.getConsumidoEstqBarca())
                                                                                .compareTo(qtdeParaAbatimento.get()) > 0
                                                                                ? qtdeParaAbatimento.get().subtract(qtdeAbatida.get())
                                                                                : material.getConsumoestq().subtract(material.getConsumidoEstqBarca()));
                                                                        qtdePecasMatPrincipal.set(
                                                                                qtdeAbatimento.get().multiply(new BigDecimal(1.0 - (material.getInsumo().getIndtin().divide(new BigDecimal(100), 9, RoundingMode.HALF_UP).doubleValue())))
                                                                                        .divide(material.getConsumoUnitario(), 0, RoundingMode.CEILING)
                                                                                        .intValue());
                                                                        qtdeAbatida.set(qtdeAbatida.get().add(qtdeAbatimento.get()));
                                                                        // cálculo do total de consumo para a barca (unidade material)
                                                                        BigDecimal consumoQtdeBarca = new BigDecimal(qtdePecasMatPrincipal.get())
                                                                                .multiply(material.getConsumoUnitario())
                                                                                .divide(new BigDecimal(1.0 - (material.getInsumo().getIndtin().divide(new BigDecimal(100), 9, RoundingMode.HALF_UP).doubleValue())), 3, RoundingMode.HALF_UP);
                                                                        // cálculo do total de consumo para a barca (unidade estoque)
                                                                        BigDecimal consumoEstoqueQtdeBarca = new BigDecimal(qtdePecasMatPrincipal.get())
                                                                                .multiply(material.getConsumoUnitario())
                                                                                .divide(new BigDecimal(1.0 - (material.getInsumo().getIndtin().divide(new BigDecimal(100), 9, RoundingMode.HALF_UP).doubleValue())), 3, RoundingMode.HALF_UP)
                                                                                .multiply(material.getUnidade().equals(material.getUnidadeestq()) ? BigDecimal.ONE : material.getInsumo().getPeso());
                                                                        // criação dos itens da barca
                                                                        SdItensBarca itemBarca = new SdItensBarca(barca, material.getNumeroof(), material.getCormaterialpai(), material.getLoteof(),
                                                                                material.getCorTinturaria(), material.getCustoTinturaria(), material.getInsumo().getCodigo(), material.getMaterialNecessidade(),
                                                                                material.getCorinsumo().getCor(), material.getCorMaterialNecessidade(), material.getDeposito(), consumoQtdeBarca, consumoEstoqueQtdeBarca,
                                                                                material.isPrincipal(), material.isContrastante(), material.getInsumo().getLinha());
                                                                        barca.getItens().add(itemBarca);

                                                                        // abatimento da carga da OF no MRP para próximas barcas
                                                                        materiaisOfBarca.get().stream()
                                                                                .filter(mat -> mat.getInsumo().getCodigo().equals(material.getInsumo().getCodigo())
                                                                                        && mat.getCorinsumo().getCor().equals(material.getCorinsumo().getCor())
                                                                                        && mat.getNumeroof().equals(material.getNumeroof())
                                                                                        && mat.getDeposito().equals(material.getDeposito())
                                                                                        && mat.getCormaterialpai().equals(material.getCormaterialpai()))
                                                                                .findAny()
                                                                                .ifPresent(mat -> {
                                                                                    mat.setConsumidoBarca(mat.getConsumidoBarca().add(consumoQtdeBarca));
                                                                                    mat.setConsumidoEstqBarca(mat.getConsumidoEstqBarca().add(consumoEstoqueQtdeBarca));
                                                                                });
                                                                        if (qtdeParaAbatimento.get().subtract(qtdeAbatida.get()).compareTo(BigDecimal.ZERO) <= 0)
                                                                            throw new ContinueException();
                                                                    });
                                                                } catch (ContinueException ce) {}
                                                                materiaisOf.stream().filter(material -> !material.isPrincipal()).forEach(material -> {
                                                                    // cálculo do total de consumo para a barca (unidade material)
                                                                    BigDecimal consumoQtdeBarca = new BigDecimal(qtdePecasMatPrincipal.get().doubleValue() *
                                                                            (material.getConsumido().divide(
                                                                                    new BigDecimal(materiaisOf.stream().filter(mat -> !mat.isPrincipal())
                                                                                            .mapToDouble(mat -> mat.getConsumido().doubleValue()).sum()), 9, RoundingMode.HALF_UP)).doubleValue()).setScale(0, RoundingMode.HALF_UP)
                                                                            .multiply(material.getConsumoUnitario());
                                                                    // cálculo do total de consumo para a barca (unidade estoque)
                                                                    BigDecimal consumoEstoqueQtdeBarca = new BigDecimal(qtdePecasMatPrincipal.get().doubleValue() *
                                                                            (material.getConsumido().divide(new BigDecimal(materiaisOf.stream()
                                                                                    .filter(mat -> !mat.isPrincipal())
                                                                                    .mapToDouble(mat -> mat.getConsumido().doubleValue()).sum()), 9, RoundingMode.HALF_UP)).doubleValue()).setScale(0, RoundingMode.HALF_UP)
                                                                            .multiply(material.getConsumoUnitario()).multiply(!material.getUnidade().equals("KG") ? material.getInsumo().getPeso() : BigDecimal.ONE);
                                                                    // criação dos itens da barca
                                                                    SdItensBarca itemBarca = new SdItensBarca(barca, material.getNumeroof(), material.getCormaterialpai(), material.getLoteof(),
                                                                            material.getCorTinturaria(), material.getCustoTinturaria(), material.getInsumo().getCodigo(), material.getMaterialNecessidade(),
                                                                            material.getCorinsumo().getCor(), material.getCorMaterialNecessidade(), material.getDeposito(), consumoQtdeBarca, consumoEstoqueQtdeBarca,
                                                                            material.isPrincipal(), material.isContrastante(), material.getInsumo().getLinha());
                                                                    barca.getItens().add(itemBarca);

                                                                    // abatimento da carga da OF no MRP para próximas barcas
                                                                    materiaisOfBarca.get().stream()
                                                                            .filter(mat -> mat.getInsumo().getCodigo().equals(material.getInsumo().getCodigo())
                                                                                    && mat.getCorinsumo().getCor().equals(material.getCorinsumo().getCor())
                                                                                    && mat.getNumeroof().equals(material.getNumeroof())
                                                                                    && mat.getDeposito().equals(material.getDeposito())
                                                                                    && mat.getCormaterialpai().equals(material.getCormaterialpai()))
                                                                            .findAny()
                                                                            .ifPresent(mat -> {
                                                                                mat.setConsumidoBarca(mat.getConsumidoBarca().add(consumoQtdeBarca));
                                                                                mat.setConsumidoEstqBarca(mat.getConsumidoEstqBarca().add(consumoEstoqueQtdeBarca));
                                                                            });
                                                                });

                                                                qtdeParaAbatimento.set(qtdeParaAbatimento.get().subtract(qtdeAbatida.get()));
                                                                if (qtdeParaAbatimento.get().compareTo(BigDecimal.ZERO) <= 0)
                                                                    throw new BreakException();
                                                            }
                                                        });
                                            } catch (BreakException e) {
                                            }
                                        }

                                        barca.setQtde(new BigDecimal(barca.getItens().stream().mapToDouble(item -> item.getQtdeEstoque().doubleValue()).sum()));
                                        saldoProgramacao.set(saldoProgramacao.get().subtract(barca.getQtde()));
                                        tblBarcas.items.get().add(barca);
                                        fieldQtdeBarca.value.set(StringUtils.toDecimalFormat(saldoProgramacao.get().doubleValue(), 3));
                                    });
                                }));
                            }));
                            boxBarcas.add(tblBarcas.build());
                        }));
                    }));
                }));
                container.add(FormBox.create(boxOfsBarca -> {
                    boxOfsBarca.vertical();
                    boxOfsBarca.title("Congelar OFs");
                    boxOfsBarca.add(tblOfsPeriodosSelecionados.build());
                    boxOfsBarca.add(tblOfsPeriodosNaoSelecionados.build());
                }));
            }));
            fragment.buttonsBox.getChildren().add(FormButton.create(btnSalvarProgramacao -> {
                btnSalvarProgramacao.title("Salvar Programação");
                btnSalvarProgramacao.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                btnSalvarProgramacao.addStyle("success");
                btnSalvarProgramacao.setAction(evt -> {
                    if (programacaoBarca.get().getBarcas().size() == 0) {
                        MessageBox.create(message -> {
                            message.message("Você precisa programar as barcas e cores do fornecedor.");
                            message.type(MessageBox.TypeMessageBox.IMPORTANT);
                            message.showAndWait();
                        });
                        return;
                    }
                    try {
                        persistirProgramacaoBarcas(programacaoBarca.get());
                        SysLogger.addSysDelizLog("MRP Tinturaria", TipoAcao.CADASTRAR, String.valueOf(programacaoBarca.get().getProgramacao()),
                                "Programação cadastrado com " + programacaoBarca.get().getBarcas().size() + " barcas");
                        MessageBox.create(message -> {
                            message.message("Programação salva com sucesso");
                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                            message.position(Pos.TOP_RIGHT);
                            message.notification();
                        });
                        fragmentBarcas.close();
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                        ExceptionBox.build(ebox -> {
                            ebox.exception(ex);
                            ebox.showAndWait();
                        });
                    }
                });
            }));
        });
    }
}