package sysdeliz2.views.pcp.impressoes;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import sysdeliz2.controllers.views.pcp.impressoes.EtiquetaMostruarioController;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Linha;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.models.ti.Produto;
import sysdeliz2.models.view.VSdDadosProduto;
import sysdeliz2.utils.ReportUtils;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.dialog.DirectoryChoice;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.SysLogger;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class EtiquetaMostruarioView extends EtiquetaMostruarioController {

    // <editor-fold defaultstate="collapsed" desc="fields filter">
    private final FormFieldMultipleFind<Produto> fieldCodigo = FormFieldMultipleFind.create(Produto.class, field -> {
        field.title("Produto");
        field.toUpper();
        field.width(350.0);
    });
    private final FormFieldMultipleFind<Colecao> fieldColecao = FormFieldMultipleFind.create(Colecao.class, field -> {
        field.title("Coleção");
        field.toUpper();
        field.width(350.0);
    });
    private final FormFieldMultipleFind<Marca> fieldMarca = FormFieldMultipleFind.create(Marca.class, field -> {
        field.title("Marca");
        field.toUpper();
        field.width(350.0);
    });
    private final FormFieldMultipleFind<Linha> fieldLinha = FormFieldMultipleFind.create(Linha.class, field -> {
        field.title("Linha");
        field.toUpper();
        field.width(350.0);
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="bean components">
    private final ListProperty<VSdDadosProduto> beanFilteredProdutos = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<SelectedProduto> beanSelectProdutos = new SimpleListProperty<>(FXCollections.observableArrayList());
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="tables">
    private final FormTableView<VSdDadosProduto> tblFilteredProdutos = FormTableView.create(VSdDadosProduto.class, table -> {
        table.title("Produtos Filtrados");
        table.expanded();
        table.items.bind(beanFilteredProdutos);
        table.editable.bind(beanFilteredProdutos.emptyProperty().not());
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(50.0);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosProduto, VSdDadosProduto>, ObservableValue<VSdDadosProduto>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<VSdDadosProduto, VSdDadosProduto>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnEnviarImpressao = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.IMPRIMIR, ImageUtils.IconSize._16));
                                btn.tooltip("Enviar para impressão");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("info");
                            });

                            @Override
                            protected void updateItem(VSdDadosProduto item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    btnEnviarImpressao.setOnAction(evt -> {
                                        selecionarProduto(item);
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnEnviarImpressao);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build() /*Ações*/,
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosProduto, VSdDadosProduto>, ObservableValue<VSdDadosProduto>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo()));
                }).build() /*Código*/,
                FormTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(260.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosProduto, VSdDadosProduto>, ObservableValue<VSdDadosProduto>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDescricao()));
                }).build() /*Descrição*/,
                FormTableColumn.create(cln -> {
                    cln.title("Marca");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosProduto, VSdDadosProduto>, ObservableValue<VSdDadosProduto>>) param -> new ReadOnlyObjectWrapper(param.getValue().getMarca()));

                }).build() /*Marca*/,
                FormTableColumn.create(cln -> {
                    cln.title("Coleção");
                    cln.width(120.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosProduto, VSdDadosProduto>, ObservableValue<VSdDadosProduto>>) param -> new ReadOnlyObjectWrapper(param.getValue().getColecao()));

                }).build() /*Coleção*/,
                FormTableColumn.create(cln -> {
                    cln.title("Linha");
                    cln.width(120.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosProduto, VSdDadosProduto>, ObservableValue<VSdDadosProduto>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLinha()));

                }).build() /*Linha*/
        );
    });
    private final FormTableView<SelectedCores> tblCoresProduto = FormTableView.create(SelectedCores.class, table -> {
        table.title("Cores Produto");
        table.editable.bind(beanSelectProdutos.emptyProperty().not());
        table.selectColumn();
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Seq.");
                    cln.width(40.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SelectedCores, SelectedCores>, ObservableValue<SelectedCores>>) param -> new ReadOnlyObjectWrapper(param.getValue().sequencial.get()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Seq.*/,
                FormTableColumn.create(cln -> {
                    cln.title("Cor");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SelectedCores, SelectedCores>, ObservableValue<SelectedCores>>) param -> new ReadOnlyObjectWrapper(param.getValue().cor.get()));
                }).build() /*Cor*/,
                FormTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(180.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SelectedCores, SelectedCores>, ObservableValue<SelectedCores>>) param -> new ReadOnlyObjectWrapper(param.getValue().descricao.get()));
                }).build() /*Descrição*/,
                FormTableColumn.create(cln -> {
                    cln.title("Versão");
                    cln.width(60.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SelectedCores, SelectedCores>, ObservableValue<SelectedCores>>) param -> new ReadOnlyObjectWrapper(param.getValue().versao.get()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Versão*/
        );
    });
    private final FormTableView<SelectedProduto> tblSelectedProdutos = FormTableView.create(SelectedProduto.class, table -> {
        table.title("Produtos Selecionados");
        table.expanded();
        table.items.bind(beanSelectProdutos);
        table.editable.bind(beanSelectProdutos.emptyProperty().not());
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(50.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SelectedProduto, SelectedProduto>, ObservableValue<SelectedProduto>>) param -> new ReadOnlyObjectWrapper(param.getValue().produto.get().getCodigo()));
                }).build() /*Código*/,
                FormTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(180.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SelectedProduto, SelectedProduto>, ObservableValue<SelectedProduto>>) param -> new ReadOnlyObjectWrapper(param.getValue().produto.get().getDescricao()));
                }).build() /*Descrição*/,
                FormTableColumn.create(cln -> {
                    cln.title("Versão");
                    cln.width(280.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SelectedProduto, SelectedProduto>, ObservableValue<SelectedProduto>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SelectedProduto, SelectedProduto>() {
                            @Override
                            protected void updateItem(SelectedProduto item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(FormFieldSingleFind.create(Produto.class, field -> {
                                        field.withoutTitle();
                                        field.value.bindBidirectional(item.produtoVersao);
                                    }).build());
                                }
                            }
                        };
                    });
                }).build() /*Versão*/,
                FormTableColumn.create(cln -> {
                    cln.title("Qtde Cores");
                    cln.width(80.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SelectedProduto, SelectedProduto>, ObservableValue<SelectedProduto>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<SelectedProduto, SelectedProduto>() {
                            @Override
                            protected void updateItem(SelectedProduto item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(String.valueOf(item.cores.get().size()));
                                }
                            }
                        };
                    });
                }).build() /*Qtde Cores*/
        );
        table.selectionModelItem((observable, oldValue, newValue) -> {
            if (newValue != null) {
                AtomicReference<Exception> error = new AtomicReference<>();
                SelectedProduto rowItem = (SelectedProduto) newValue;
                String codigoSelectCores = rowItem.produtoVersao.isNull().get()
                        ? rowItem.produto.get().getCodigo()
                        : rowItem.produto.get().getCodigo().concat(",").concat(rowItem.produtoVersao.get().getCodigo());
                AtomicReference<List<SelectedCores>> cores = new AtomicReference<>(new ArrayList<>());
                new RunAsyncWithOverlay(this).exec(task -> {
                    try {
                        cores.set(getCoresFilteredProduto(codigoSelectCores));
                        return ReturnAsync.OK.value;
                    } catch (SQLException e) {
                        e.printStackTrace();
                        error.set(e);
                        return ReturnAsync.EXCEPTION.value;
                    }
                }).addTaskEndNotification(taskReturn -> {
                    if (taskReturn.equals(ReturnAsync.OK.value)) {
                        cores.get().forEach(cor -> {
                            cor.selectedProperty().addListener((observable1, oldValue1, newValue1) -> {
                                if (newValue1 != null) {
                                    SelectedProduto produtoSelecionado = (SelectedProduto) table.selectedItem();
                                    if (newValue1) {
                                        produtoSelecionado.cores.add(cor);
                                    } else {
                                        produtoSelecionado.cores.remove(cor);
                                    }
                                    table.refresh();
                                }
                            });
                        });
                        tblCoresProduto.setItems(FXCollections.observableList(cores.get()));
                        tblCoresProduto.refresh();
                    } else if (taskReturn.equals(ReturnAsync.EXCEPTION.value)) {
                        ExceptionBox.build(message -> {
                            message.exception(error.get());
                            message.showAndWait();
                        });
                    }
                });
            }
        });
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Switch">
    private FormFieldToggleSingle btnQrCode = FormFieldToggleSingle.create(it -> {
        it.title("QR Code");
    });
    // </editor-fold>

    public EtiquetaMostruarioView() {
        super("Impressão Etiqueta Mostruário", ImageUtils.getImage(ImageUtils.Icon.IMPRIMIR_TAG));
        init();
    }

    private void init() {
        super.box.getChildren().add(FormBox.create(principal -> {
            principal.vertical();
            principal.expanded();
            principal.add(FormBox.create(boxFilter -> {
                boxFilter.horizontal();
                boxFilter.add(FormTitledPane.create(filter -> {
                    filter.filter();
                    filter.add(FormBox.create(boxRow -> {
                        boxRow.horizontal();
                        boxRow.add(fieldCodigo.build(), fieldColecao.build());
                    }));
                    filter.add(FormBox.create(boxRow -> {
                        boxRow.horizontal();
                        boxRow.add(fieldLinha.build(), fieldMarca.build());
                    }));
                    filter.find.setOnAction(evt -> {
                        new RunAsyncWithOverlay(this).exec(task -> {
                            getFilteredProdutos(fieldCodigo.objectValues.get(), fieldColecao.objectValues.get(), fieldMarca.objectValues.get(), fieldLinha.objectValues.get());
                            return ReturnAsync.OK.value;
                        }).addTaskEndNotification(taskReturn -> {
                            if (taskReturn.equals(ReturnAsync.OK.value)) {
                                beanFilteredProdutos.set(FXCollections.observableList(super.filteredProdutos));
                            }
                        });
                    });
                    filter.clean.setOnAction(evt -> {
                        fieldMarca.clear();
                        fieldLinha.clear();
                        fieldCodigo.clear();
                        fieldColecao.clear();
                        beanFilteredProdutos.clear();
                    });
                }));
            }));
            principal.add(FormBox.create(boxProdutos -> {
                boxProdutos.horizontal();
                boxProdutos.expanded();
                boxProdutos.add(FormBox.create(boxFilteredProdutos -> {
                    boxFilteredProdutos.vertical();
                    boxFilteredProdutos.width(700.0);
                    boxFilteredProdutos.add(tblFilteredProdutos.build());
                }));
                boxProdutos.add(FormBox.create(boxSelectedProdutos -> {
                    boxSelectedProdutos.horizontal();
                    boxSelectedProdutos.expanded();
                    boxSelectedProdutos.add(tblSelectedProdutos.build());
                    boxSelectedProdutos.add(tblCoresProduto.build());
                }));
            }));
            principal.add(FormBox.create(boxToolbar -> {
                boxToolbar.horizontal();
                boxToolbar.alignment(Pos.CENTER_RIGHT);
                boxToolbar.add(FormBox.create(boxEtq -> {
                    boxEtq.vertical();
                    boxEtq.add(btnQrCode.build());
                    boxEtq.add(FormButton.create(btnLimparSelecao -> {
                        btnLimparSelecao.title("Limpar Seleção");
                        btnLimparSelecao.icon(ImageUtils.getIcon(ImageUtils.Icon.LIMPAR, ImageUtils.IconSize._24));
                        btnLimparSelecao.addStyle("sm").addStyle("warning");
                        btnLimparSelecao.disable.bind(beanSelectProdutos.emptyProperty());
                        btnLimparSelecao.setAction(evt -> {
                            limparSelecao();
                        });
                    }));
                }));

//                boxToolbar.add(FormButton.create(btnSalvarSelecao -> {
//                    btnSalvarSelecao.title("Salvar Seleção");
//                    btnSalvarSelecao.icon(ImageUtils.getIcon(ImageUtils.Icon.SALVAR, ImageUtils.IconSize._24));
//                    btnSalvarSelecao.addStyle("sm").addStyle("primary");
//                    btnSalvarSelecao.disable.bind(beanSelectProdutos.emptyProperty());
//                }));
                boxToolbar.add(FormButton.create(btnGerarEtiqueta -> {
                    btnGerarEtiqueta.title("Gerar Etiqueta");
                    btnGerarEtiqueta.icon(ImageUtils.getIcon(ImageUtils.Icon.IMPRIMIR_TAG, ImageUtils.IconSize._32));
                    btnGerarEtiqueta.addStyle("lg").addStyle("success");
                    btnGerarEtiqueta.disable.bind(beanSelectProdutos.emptyProperty());
                    btnGerarEtiqueta.setAction(evt -> gerarEtiqueta());
                }));
            }));
        }));
    }

    private void gerarEtiqueta() {
        if (beanSelectProdutos.stream().anyMatch(it -> it.cores.size() == 0)) {
            MessageBox.create(message -> {
                message.message("Existem produtos sem as cores selecionadas, você deve primeiro selecionar as cores da etiqueta de mostruário para gerar carteiras.");
                message.type(MessageBox.TypeMessageBox.IMPORTANT);
                message.showAndWait();
            });
            return;
        }

        String pathPastaDestino = DirectoryChoice.show().stringPath();
        AtomicReference<Exception> error = null;
        new RunAsyncWithOverlay(this).exec(task -> {
            try {
                for (SelectedProduto produto : beanSelectProdutos) {
                    String finalPathCarteira = pathPastaDestino.concat("\\").concat(produto.produto.get().getCodigo()).concat(".pdf");
                    String codigoPrincipal = produto.produto.get().getCodigo();
                    String sufixoPrincipal = codigoPrincipal.substring(codigoPrincipal.length() - 1, codigoPrincipal.length());
                    String abreviacaoPrincipal = codigoPrincipal.substring(0, codigoPrincipal.length() - 1);
                    String codigoVersao = produto.produtoVersao.get() == null ? null : produto.produtoVersao.get().getCodigo();
                    String sufixoVersao = codigoVersao == null ? null : codigoVersao.substring(codigoVersao.length() - 1, codigoVersao.length());
                    Map<String, Object> dadosPrincipal = (Map<String, Object>) getDadosCodigo(codigoPrincipal);
                    Map<String, Object> dadosVersao = codigoVersao == null ? null : (Map<String, Object>) getDadosCodigo(codigoVersao);

                    String grade = (codigoVersao != null ? "(" + sufixoPrincipal + ") " : "") +
                            ((String) dadosPrincipal.get("GRADE")) +
                            (codigoVersao != null ? " (" + sufixoVersao + ") " + ((String) dadosVersao.get("GRADE")) : "");
                    String gancho = (codigoVersao != null ? "(" + sufixoPrincipal + ") " : "") +
                            (dadosPrincipal.get("GANCHO") != null ? (String) dadosPrincipal.get("GANCHO") : "") +
                            (codigoVersao != null ? " (" + sufixoVersao + ") " + ((String) dadosVersao.get("GANCHO")) : "");
                    String tecido = (String) dadosPrincipal.get("COMPOSICAO");
                    String descricao = produto.produto.get().getDescricao();
                    String apelo = (String) dadosPrincipal.get("APELO");
                    String codigo = codigoVersao == null ? codigoPrincipal : abreviacaoPrincipal + "(" + sufixoPrincipal + sufixoVersao + ")";

                    String codigosConsulta = codigoPrincipal + (codigoVersao == null ? "" : "," + codigoVersao);
                    String coresConsulta = produto.cores.stream().map(it -> it.cor.get().replace(" [M]", "")).collect(Collectors.joining(","));

                    int limitePorPagina = btnQrCode.value.get() ? 9 : 13;

                    if (produto.cores.stream().map(it -> it.cor.get().replace(" [M]", "")).count() <= limitePorPagina) {
                        criaReport(finalPathCarteira, codigoPrincipal, codigoVersao, grade, gancho, tecido, descricao, apelo, codigo, codigosConsulta, coresConsulta);
                    } else {
                        int contador = 0;
                        while (contador <= produto.cores.getSize()) {
                            String coresConsulta1;

                            if ((contador + limitePorPagina) < produto.cores.getSize())
                                coresConsulta1 = produto.cores.subList(contador, contador + limitePorPagina).stream().map(it -> it.cor.get().replace(" [M]", "")).collect(Collectors.joining(","));
                            else
                                coresConsulta1 = produto.cores.subList(contador, produto.cores.getSize()).stream().map(it -> it.cor.get().replace(" [M]", "")).collect(Collectors.joining(","));

                            contador += limitePorPagina;
                            criaReport(finalPathCarteira.replace(".pdf", "-" + contador / limitePorPagina + ".pdf"), codigoPrincipal, codigoVersao, grade, gancho, tecido, descricao, apelo, codigo, codigosConsulta, coresConsulta1);
                        }
//                        String coresConsulta1 = produto.cores.subList(0, limitePorPagina).stream().map(it -> it.cor.get().replace(" [M]", "")).collect(Collectors.joining(","));
//                        String coresConsulta2 = produto.cores.subList(limitePorPagina, 2 * limitePorPagina).stream().map(it -> it.cor.get().replace(" [M]", "")).collect(Collectors.joining(","));
//                        String coresConsulta3 = produto.cores.subList(2 * limitePorPagina, produto.cores.getSize()).stream().map(it -> it.cor.get().replace(" [M]", "")).collect(Collectors.joining(","));
//                        criaReport(finalPathCarteira.replace(".pdf", "-1.pdf"), codigoPrincipal, codigoVersao, grade, gancho, tecido, descricao, apelo, codigo, codigosConsulta, coresConsulta1);
//                        criaReport(finalPathCarteira.replace(".pdf", "-2.pdf"), codigoPrincipal, codigoVersao, grade, gancho, tecido, descricao, apelo, codigo, codigosConsulta, coresConsulta2);
//                        criaReport(finalPathCarteira.replace(".pdf", "-3.pdf"), codigoPrincipal, codigoVersao, grade, gancho, tecido, descricao, apelo, codigo, codigosConsulta, coresConsulta3);
                    }

                    SysLogger.addSysDelizLog("Carteira Mostruario", TipoAcao.CADASTRAR, codigoPrincipal, "Criado carteirinha mostruário em: " + finalPathCarteira);
                }
                return ReturnAsync.OK.value;
            } catch (SQLException | JRException | IOException e) {
                e.printStackTrace();
                error.set(e);
                return ReturnAsync.EXCEPTION.value;
            }
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                MessageBox.create(message -> {
                    message.message("Carteiras geradas com sucesso!");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            } else if (taskReturn.equals(ReturnAsync.EXCEPTION.value)) {
                ExceptionBox.build(message -> {
                    message.exception(error.get());
                    message.showAndWait();
                });
            }
        });
    }

    private void criaReport(String finalPathCarteira, String codigoPrincipal, String codigoVersao, String grade, String gancho, String tecido, String descricao, String apelo, String codigo, String codigosConsulta, String coresConsulta) throws IOException, JRException, SQLException {
        ReportUtils.ReportFile reportFile = coresConsulta.split(",").length > 1 ? ReportUtils.ReportFile.CARTEIRA_MOSTRUARIO : ReportUtils.ReportFile.CARTEIRA_MOSTRUARIO_COR_UNICA;

        new ReportUtils().config()
                .addReport(reportFile,
                        new ReportUtils.ParameterReport[]{
                                new ReportUtils.ParameterReport("codigoPrincipal", codigoPrincipal),
                                new ReportUtils.ParameterReport("codigoVersao", codigoVersao),
                                new ReportUtils.ParameterReport("codigoExibicao", codigo),
                                new ReportUtils.ParameterReport("grade", grade),
                                new ReportUtils.ParameterReport("gancho", gancho),
                                new ReportUtils.ParameterReport("tecido", tecido),
                                new ReportUtils.ParameterReport("apelo", apelo),
                                new ReportUtils.ParameterReport("codigos", codigosConsulta),
                                new ReportUtils.ParameterReport("cores", coresConsulta),
                                new ReportUtils.ParameterReport("descricao", descricao),
                                new ReportUtils.ParameterReport("qrCode", btnQrCode.value.get())
                        })
                .view().toPdf(finalPathCarteira);
    }

    private void limparSelecao() {
        beanSelectProdutos.clear();
        tblCoresProduto.clear();
    }

    private void selecionarProduto(VSdDadosProduto item) {
        if (beanSelectProdutos.stream().noneMatch(it -> it.produto.get().equals(item)))
            beanSelectProdutos.add(new SelectedProduto(item));
    }

}
