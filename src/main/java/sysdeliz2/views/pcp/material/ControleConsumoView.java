package sysdeliz2.views.pcp.material;

import javafx.application.Platform;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.util.Callback;
import org.jetbrains.annotations.NotNull;
import sysdeliz2.controllers.views.pcp.material.ControleConsumoController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.sysdeliz.SdConsumoProd;
import sysdeliz2.models.ti.*;
import sysdeliz2.models.view.b2c.VB2cProdutosEnvio;
import sysdeliz2.utils.SortUtils;
import sysdeliz2.utils.TableUtils;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class ControleConsumoView extends ControleConsumoController {

    final double WIDHTFIELDS = 90;
    final BigDecimal BIGDECIMAL_100 = new BigDecimal("100");

    //<editor-fold desc="Component">
    private final FormFieldText codigoField = FormFieldText.create(field -> {
        field.title("Produto");
        field.width(150);
        field.toUpper();
        field.textField.setOnKeyReleased(evt -> {
            if (evt.getCode().equals(KeyCode.ENTER) && !field.value.getValueSafe().equals("")) {
                buscaProduto(field.value.getValue());
                field.value.setValue("");
            }
        });
    });
    private final FormBox root = FormBox.create(box -> {
        box.expanded();
        box.vertical();
        box.verticalScroll();
    });
    // Filter

    private final FormFieldMultipleFind<Produto> produtoFilter = FormFieldMultipleFind.create(Produto.class, field -> {
        field.title("Código");
        field.width(200);
        field.toUpper();
    });

    private final FormFieldMultipleFind<Marca> marcaFilter = FormFieldMultipleFind.create(Marca.class, field -> {
        field.title("Marca");
        field.width(200);
        field.toUpper();
    });

    private final FormFieldMultipleFind<Colecao> colecaoFilter = FormFieldMultipleFind.create(Colecao.class, field -> {
        field.title("Coleção");
        field.width(200);
        field.toUpper();
    });
    // Table


    private final FormTreeTableView<SdConsumoProd> tblConsumo = FormTreeTableView.create(SdConsumoProd.class, table -> {
        table.title("Produtos");
        table.expanded();
        table.columns(
                FormTreeTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(100);
                    cln.value((Callback<TreeTableColumn.CellDataFeatures<SdConsumoProd, SdConsumoProd>, ObservableValue<SdConsumoProd>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValue()));
                    cln.format(param -> {
                        return new TreeTableCell<SdConsumoProd, SdConsumoProd>() {
                            @Override
                            protected void updateItem(SdConsumoProd item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    if (item.isLinhaItem() && item.getMaterial() != null) {
                                        setGraphic(createTextFieldToSelectOnTable(item.getMaterial().getCodigo()).build());
                                    } else if (!item.isLinhaItem() && item.getCodigo() != null) {
                                        setText(item.getCodigo().getCodigo());
                                    }
                                    if (!item.isLinhaItem()) {
                                        setFont(Font.font("System", FontWeight.BOLD, 12));
                                    } else {
                                        setFont(Font.font(11));
                                    }
                                }
                            }
                        };
                    });
                }).build(),
                FormTreeTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(350);
                    cln.value((Callback<TreeTableColumn.CellDataFeatures<SdConsumoProd, SdConsumoProd>, ObservableValue<SdConsumoProd>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValue()));
                    cln.format(param -> {
                        return new TreeTableCell<SdConsumoProd, SdConsumoProd>() {
                            @Override
                            protected void updateItem(SdConsumoProd item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    if (item.isLinhaItem() && item.getMaterial() != null) {
                                        setGraphic(createTextFieldToSelectOnTable(item.getMaterial().getDescricao()).build());
                                    } else if (!item.isLinhaItem() && item.getCodigo() != null) {
                                        setText(item.getCodigo().getDescricao());
                                    }
                                    if (!item.isLinhaItem()) {
                                        setFont(Font.font("System", FontWeight.BOLD, 12));
                                    } else {
                                        setFont(Font.font(11));
                                    }
                                }
                            }
                        };
                    });
                }).build(),
                FormTreeTableColumn.create(cln -> {
                    cln.title("Marca");
                    cln.width(100);
                    cln.value((Callback<TreeTableColumn.CellDataFeatures<SdConsumoProd, SdConsumoProd>, ObservableValue<SdConsumoProd>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValue()));
                    cln.format(param -> {
                        return new TreeTableCell<SdConsumoProd, SdConsumoProd>() {
                            @Override
                            protected void updateItem(SdConsumoProd item, boolean empty) {
                                super.updateItem(item, empty);
                                setText("");
                                if (item != null && !empty) {
                                    if (item.getCodigo() != null && item.getCodigo().getMarca() != null) {
                                        setText(item.getCodigo().getMarca().getDescricao());
                                    }
                                    if (!item.isLinhaItem()) {
                                        setFont(Font.font("System", FontWeight.BOLD, 12));
                                    } else {
                                        setFont(Font.font(11));
                                    }
                                }
                            }
                        };
                    });
                }).build(),
                FormTreeTableColumn.create(cln -> {
                    cln.title("Coleção");
                    cln.width(150);
                    cln.value((Callback<TreeTableColumn.CellDataFeatures<SdConsumoProd, SdConsumoProd>, ObservableValue<SdConsumoProd>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValue()));
                    cln.format(param -> {
                        return new TreeTableCell<SdConsumoProd, SdConsumoProd>() {
                            @Override
                            protected void updateItem(SdConsumoProd item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    if (item.getCodigo() != null && item.getCodigo().getColecao() != null) {
                                        setText(item.getCodigo().getColecao().getDescricao());
                                    }
                                    if (!item.isLinhaItem()) {
                                        setFont(Font.font("System", FontWeight.BOLD, 12));
                                    } else {
                                        setFont(Font.font(11));
                                    }
                                }
                            }
                        };
                    });
                }).build(),
                FormTreeTableColumnGroup.createGroup(aplicacao -> {
                    aplicacao.title("Aplicação");
                    aplicacao.addColumn(
                            FormTreeTableColumn.create(cln -> {
                                cln.title("Código");
                                cln.width(80);
                                cln.value((Callback<TreeTableColumn.CellDataFeatures<SdConsumoProd, SdConsumoProd>, ObservableValue<SdConsumoProd>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValue()));
                                cln.format(param -> {
                                    return new TreeTableCell<SdConsumoProd, SdConsumoProd>() {
                                        @Override
                                        protected void updateItem(SdConsumoProd item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            setGraphic(null);
                                            if (item != null && !empty) {
                                                if (item.isLinhaItem() && item.getMaterial() != null) {
                                                    setGraphic(createTextFieldToSelectOnTable(item.getAplicacao().getCodigo()).build());
                                                }
                                            }
                                        }
                                    };
                                });
                            }).build()
                    );
                    aplicacao.addColumn(
                            FormTreeTableColumn.create(cln -> {
                                cln.title("Descrição");
                                cln.width(140);
                                cln.value((Callback<TreeTableColumn.CellDataFeatures<SdConsumoProd, SdConsumoProd>, ObservableValue<SdConsumoProd>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValue()));
                                cln.format(param -> {
                                    return new TreeTableCell<SdConsumoProd, SdConsumoProd>() {
                                        @Override
                                        protected void updateItem(SdConsumoProd item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            setGraphic(null);
                                            if (item != null && !empty) {
                                                if (item.isLinhaItem() && item.getMaterial() != null) {
                                                    setGraphic(createTextFieldToSelectOnTable(item.getAplicacao().getDescricao()).build());
                                                }
                                            }
                                        }
                                    };
                                });
                            }).build()
                    );
                }),
                FormTreeTableColumnGroup.createGroup(modelagem -> {
                    modelagem.title("Modelagem");
                    modelagem.addColumn(
                            FormTreeTableColumn.create(cln -> {
                                cln.title("Consumo metro");
                                cln.width(100);
                                cln.alignment(Pos.CENTER);
                                cln.value((Callback<TreeTableColumn.CellDataFeatures<SdConsumoProd, SdConsumoProd>, ObservableValue<SdConsumoProd>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValue()));
                                cln.format(param -> {
                                    return new TreeTableCell<SdConsumoProd, SdConsumoProd>() {
                                        @Override
                                        protected void updateItem(SdConsumoProd item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            setGraphic(null);
                                            if (item != null && !empty) {
                                                if (item.isLinhaItem() && item.getMaterial() != null) {
                                                    setGraphic(createTextFieldToSelectOnTable(item.getConsuMetroMod().toString()).build());
                                                }
                                            }
                                        }
                                    };
                                });
                            }).build());
                    modelagem.addColumn(
                            FormTreeTableColumn.create(cln -> {
                                cln.title("Largura");
                                cln.width(100);
                                cln.alignment(Pos.CENTER);
                                cln.value((Callback<TreeTableColumn.CellDataFeatures<SdConsumoProd, SdConsumoProd>, ObservableValue<SdConsumoProd>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValue()));
                                cln.format(param -> {
                                    return new TreeTableCell<SdConsumoProd, SdConsumoProd>() {
                                        @Override
                                        protected void updateItem(SdConsumoProd item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            setGraphic(null);
                                            if (item != null && !empty) {
                                                if (item.isLinhaItem() && item.getMaterial() != null) {
                                                    setGraphic(createTextFieldToSelectOnTable(item.getLargmod().toString()).build());
                                                }
                                            }
                                        }
                                    };
                                });
                            }).build());
                    modelagem.addColumn(
                            FormTreeTableColumn.create(cln -> {
                                cln.title("Gramatura");
                                cln.width(100);
                                cln.alignment(Pos.CENTER);
                                cln.value((Callback<TreeTableColumn.CellDataFeatures<SdConsumoProd, SdConsumoProd>, ObservableValue<SdConsumoProd>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValue()));
                                cln.format(param -> {
                                    return new TreeTableCell<SdConsumoProd, SdConsumoProd>() {
                                        @Override
                                        protected void updateItem(SdConsumoProd item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            setGraphic(null);
                                            if (item != null && !empty) {
                                                if (item.isLinhaItem() && item.getMaterial() != null) {
                                                    setGraphic(createTextFieldToSelectOnTable(item.getGrammod().toString()).build());
                                                }
                                            }
                                        }
                                    };
                                });                            }).build());
                    modelagem.addColumn(
                            FormTreeTableColumn.create(cln -> {
                                cln.title("Consumo Kilo");
                                cln.width(100);
                                cln.alignment(Pos.CENTER);
                                cln.value((Callback<TreeTableColumn.CellDataFeatures<SdConsumoProd, SdConsumoProd>, ObservableValue<SdConsumoProd>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValue()));
                                cln.format(param -> {
                                    return new TreeTableCell<SdConsumoProd, SdConsumoProd>() {
                                        @Override
                                        protected void updateItem(SdConsumoProd item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            setGraphic(null);
                                            if (item != null && !empty) {
                                                if (item.isLinhaItem() && item.getMaterial() != null) {
                                                    setGraphic(createTextFieldToSelectOnTable(item.getConsuKiloMod().toString()).build());
                                                }
                                            }
                                        }
                                    };
                                });                            }).build());
                    modelagem.addColumn(
                            FormTreeTableColumn.create(cln -> {
                                cln.title("%");
                                cln.width(100);
                                cln.value((Callback<TreeTableColumn.CellDataFeatures<SdConsumoProd, SdConsumoProd>, ObservableValue<SdConsumoProd>>) param -> new ReadOnlyObjectWrapper(param.getValue().getValue()));
                                cln.format(param -> {
                                    return new TreeTableCell<SdConsumoProd, SdConsumoProd>() {
                                        @Override
                                        protected void updateItem(SdConsumoProd item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            setGraphic(null);
                                            if (item != null && !empty) {
                                                if (item.isLinhaItem()) {
                                                    List<SdConsumoProd> collect = consumos.stream().filter(it -> it.getCodigo().getCodigo().equals(item.getCodigo().getCodigo())).collect(Collectors.toList());
                                                    setGraphic(createTextFieldToSelectOnTable(calculaPorcentagem(collect, item) + "%").build());
                                                }
                                            }
                                        }
                                    };
                                });
                            }).build());
                }),
                FormTreeTableColumnGroup.createGroup(mostruario -> {
                    mostruario.title("Mostruário");
                    mostruario.addColumn(
                            FormTreeTableColumn.create(cln -> {
                                cln.title("Largura");
                                cln.width(100);
                                cln.alignment(Pos.CENTER);
                                cln.value((Callback<TreeTableColumn.CellDataFeatures<SdConsumoProd, SdConsumoProd>, ObservableValue<SdConsumoProd>>) param -> new ReadOnlyObjectWrapper(!param.getValue().getValue().isLinhaItem() ? "" : param.getValue().getValue().getLargmost().toString()));
                            }).build());
                    mostruario.addColumn(
                            FormTreeTableColumn.create(cln -> {
                                cln.title("Gramatura");
                                cln.width(100);
                                cln.alignment(Pos.CENTER);
                                cln.value((Callback<TreeTableColumn.CellDataFeatures<SdConsumoProd, SdConsumoProd>, ObservableValue<SdConsumoProd>>) param -> new ReadOnlyObjectWrapper(!param.getValue().getValue().isLinhaItem() ? "" : param.getValue().getValue().getGrammost().toString()));
                            }).build());
                    mostruario.addColumn(
                            FormTreeTableColumn.create(cln -> {
                                cln.title("Consumo");
                                cln.width(100);
                                cln.alignment(Pos.CENTER);
                                cln.value((Callback<TreeTableColumn.CellDataFeatures<SdConsumoProd, SdConsumoProd>, ObservableValue<SdConsumoProd>>) param -> new ReadOnlyObjectWrapper(!param.getValue().getValue().isLinhaItem() ? "" : param.getValue().getValue().getConsumost().toString()));
                            }).build()
                    );
                }),
                FormTreeTableColumnGroup.createGroup(producao -> {
                    producao.title("Produção");
                    producao.addColumn(
                            FormTreeTableColumn.create(cln -> {
                                cln.title("Consumo");
                                cln.width(100);
                                cln.alignment(Pos.CENTER);
                                cln.value((Callback<TreeTableColumn.CellDataFeatures<SdConsumoProd, SdConsumoProd>, ObservableValue<SdConsumoProd>>) param -> new ReadOnlyObjectWrapper(!param.getValue().getValue().isLinhaItem() ? "" : param.getValue().getValue().getConsuprod().toString()));
                            }).build());
                })
        );
        table.factoryRow(param -> new TreeTableRow<SdConsumoProd>() {
            @Override
            protected void updateItem(SdConsumoProd item, boolean empty) {
                super.updateItem(item, empty);
                clearStyle();
                if (item != null && !empty) {
                    setContextMenu(FormContextMenu.create(menu -> {
                        menu.addItem(menuItem -> {
                            menuItem.setText("Expandir Tudo");
                            menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.EXPANDIR, ImageUtils.IconSize._16));
                            menuItem.setOnAction(evt -> {
                                table.properties().getRoot().getChildren().forEach(node -> ((TreeItem<SdConsumoProd>) node).setExpanded(true));
                            });
                        });
                        menu.addItem(menuItem -> {
                            menuItem.setText("Recolher Tudo");
                            menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.RECOLHER, ImageUtils.IconSize._16));
                            menuItem.setOnAction(evt -> {
                                table.properties().getRoot().getChildren().forEach(node -> ((TreeItem<SdConsumoProd>) node).setExpanded(false));
                            });
                        });
                        menu.addSeparator();
                        menu.addItem(menuItem -> {
                            menuItem.setText("Editar");
                            menuItem.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.EDIT, ImageUtils.IconSize._16));
                            menuItem.setOnAction((evt -> editarProduto(item)));
                        });
                    }));
                    if (item.isLinhaItem()) {
                        if (item.getConsuprod().compareTo(BigDecimal.ZERO) > 0)
                            getStyleClass().add("tree-table-row-info");
                        else if (item.getConsumost().compareTo(BigDecimal.ZERO) > 0 || item.getGrammost().compareTo(BigDecimal.ZERO) > 0 || item.getLargmost().compareTo(BigDecimal.ZERO) > 0)
                            getStyleClass().add("tree-table-row-warning");
                        else getStyleClass().add("tree-table-row-primary");
                    }
                }
            }

            void clearStyle() {
                getStyleClass().removeAll("tree-table-row-danger", "tree-table-row-success",
                        "tree-table-row-warning", "tree-table-row-info", "tree-table-row-primary",
                        "tree-table-row-secundary", "tree-table-row-dark", "tree-table-row-amber");
            }
        });
    });
    //</editor-fold>

    private final ListProperty<Produto> produtosBean = new SimpleListProperty<>(FXCollections.observableArrayList());

    private final VBox tabListagem = (VBox) super.tabs.getTabs().get(0).getContent();
    private final VBox tabManutencao = (VBox) super.tabs.getTabs().get(1).getContent();

    public ControleConsumoView() {
        super("Controle de Consumo", ImageUtils.getImage(ImageUtils.Icon.GESTAO_PRODUCAO), new String[]{"Listagem", "Manutenção"});
        initListagem();
        initManutencao();
        super.tabs.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.intValue() == 0) {
                if (consumos != null) {
                    criaItensTreeTableView();
                    tblConsumo.refresh();
                }
            }
        });
    }

    private void initListagem() {
        tabListagem.getChildren().add(FormBox.create(root -> {
            root.vertical();
            root.expanded();
            root.add(header -> {
                header.horizontal();
                header.add(FormTitledPane.create(filter -> {
                    filter.filter();
                    filter.add(FormBox.create(boxFilter -> {
                        boxFilter.horizontal();
                        boxFilter.add(produtoFilter.build(), marcaFilter.build(), colecaoFilter.build());
                    }));
                    filter.find.setOnAction(evt -> {
                        new RunAsyncWithOverlay(this).exec(task -> {
                            buscarProdutos(
                                    produtoFilter.objectValues.stream().map(Produto::getCodigo).toArray(),
                                    marcaFilter.objectValues.stream().map(Marca::getCodigo).toArray(),
                                    colecaoFilter.objectValues.stream().map(Colecao::getCodigo).toArray()
                            );
                            return ReturnAsync.OK.value;
                        }).addTaskEndNotification(taskReturn -> {
                            if (taskReturn.equals(ReturnAsync.OK.value)) {
                                criaItensTreeTableView();
                                tblConsumo.refresh();
                            }
                        });
                    });
                    filter.clean.setOnAction(evt -> {
                        produtoFilter.clear();
                        marcaFilter.clear();
                        colecaoFilter.clear();
                    });
                }));
            });
            root.add(content -> {
                content.vertical();
                content.expanded();
                content.add(tblConsumo.build());
            });
        }));
    }

    private void initManutencao() {
        tabManutencao.getChildren().add(FormBox.create(principal -> {
            principal.vertical();
            principal.expanded();
            principal.add(FormBox.create(boxBtn -> {
                boxBtn.horizontal();
                boxBtn.add(codigoField.build());
                boxBtn.add(boxB -> {
                    boxB.vertical();
                    boxB.alignment(Pos.BOTTOM_LEFT);
                    boxB.add(FormButton.create(btn -> {
                        btn.addStyle("success");
                        btn.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._16));
                        btn.setOnAction(evt -> {
                            if (!codigoField.value.getValueSafe().equals("")) {
                                buscaProduto(codigoField.value.getValue());
                                codigoField.clear();
                            }
                        });
                    }));
                });
            }));
            principal.add(root);
        }));
    }

    private void addReferencia(Produto produto) {
        produtosBean.add(produto);
        root.add(root.getChildrenSize(),
                FormTitledPane.create(titledPane -> {
                    ListProperty<SdConsumoProd> listConsumoProd = new SimpleListProperty<>(FXCollections.observableArrayList());
                    titledPane.title(produto.getCodigo() + " - " + produto.getDescricao());
                    titledPane.setFont(Font.font("System", FontWeight.BOLD, 15));
                    titledPane.setGraphic(FormBox.create(boxIcon -> {
                        boxIcon.horizontal();
                        ImageView iconClose = new ImageView(ImageUtils.getImage(ImageUtils.Icon.CANCEL));
                        iconClose.setFitHeight(20);
                        iconClose.setFitWidth(20);
                        iconClose.setOnMouseClicked(evt -> {
                            root.remove(titledPane);
                            produtosBean.remove(produto);
                        });

                        ImageView iconProduto = new ImageView(ImageUtils.getImage(ImageUtils.Icon.PRODUTO));
                        iconProduto.setFitHeight(20);
                        iconProduto.setFitWidth(20);
                        boxIcon.add(iconClose);
                        boxIcon.add(iconProduto);
                    }));
                    titledPane.setPadding(Insets.EMPTY);
                    titledPane.add(FormBox.create(principal -> {
                        principal.horizontal();
                        principal.add(FormBox.create(boxProd -> {
                            boxProd.vertical();
                            boxProd.add(FormBox.create(header -> {
                                header.horizontal();
                                header.add(
                                        FormFieldText.create(field -> {
                                            field.withoutTitle();
                                            field.width((3 * WIDHTFIELDS) + 30);
                                            field.label("Estilista");
                                            field.clickable(false);
                                            field.editable.set(false);
                                            field.value.setValue(produto.getEstilista());
                                        }).build()
                                );
                                header.add(
                                        FormFieldText.create(field -> {
                                            field.withoutTitle();
                                            field.width((3 * WIDHTFIELDS) + 30);
                                            field.label("Modelista");
                                            field.clickable(false);
                                            field.editable.set(false);
                                            field.value.setValue(produto.getModelista());
                                        }).build()
                                );
                                header.add(
                                        FormFieldText.create(field -> {
                                            field.withoutTitle();
                                            field.width((4 * WIDHTFIELDS) + 15);
                                            field.editable.set(false);
                                            field.clickable(false);
                                            field.addStyle("primary");
                                            field.value.setValue("Modelagem");
                                            field.alignment(Pos.CENTER);
                                        }).build()
                                );
                                header.add(
                                        FormFieldText.create(field -> {
                                            field.withoutTitle();
                                            field.width(WIDHTFIELDS);
                                            field.editable.set(false);
                                            field.clickable(false);
                                            field.addStyle("dark");
                                            field.value.setValue("%");
                                            field.alignment(Pos.CENTER);
                                        }).build()
                                );
                                header.add(
                                        FormFieldText.create(field -> {
                                            field.withoutTitle();
                                            field.width((3 * WIDHTFIELDS) + 10);
                                            field.editable.set(false);
                                            field.clickable(false);
                                            field.addStyle("warning");
                                            field.value.setValue("Mostruário");
                                            field.alignment(Pos.CENTER);
                                        }).build()
                                );
                                header.add(
                                        FormFieldText.create(field -> {
                                            field.withoutTitle();
                                            field.width(WIDHTFIELDS);
                                            field.editable.set(false);
                                            field.clickable(false);
                                            field.addStyle("danger");
                                            field.value.setValue("% Variação");
                                            field.alignment(Pos.CENTER);
                                        }).build()
                                );
                                header.add(
                                        FormFieldText.create(field -> {
                                            field.withoutTitle();
                                            field.width(WIDHTFIELDS * 2);
                                            field.editable.set(false);
                                            field.clickable(false);
                                            field.addStyle("info");
                                            field.value.setValue("Produção");
                                            field.alignment(Pos.CENTER);
                                        }).build()
                                );
                            }));
                            FormBox content = FormBox.create(bContent -> {
                                bContent.vertical();
                                listConsumoProd.sizeProperty().addListener((observable, oldValue, newValue) -> {
                                    bContent.clear();
                                    listConsumoProd.stream().sorted(Comparator.comparing(SdConsumoProd::getOrdem)).forEach(it -> bContent.add(addLinhaProduto(it, listConsumoProd)));
                                });
                                List<SdConsumoProd> consumos = (List<SdConsumoProd>) new FluentDao().selectFrom(SdConsumoProd.class).where(it -> it.equal("codigo.codigo", produto.getCodigo())).resultList();
                                if (consumos != null && consumos.size() > 0) {
                                    consumos.sort(Comparator.comparing(SdConsumoProd::getOrdem));
                                    listConsumoProd.set(FXCollections.observableArrayList(consumos));
                                }
                            });
                            boxProd.add(content);
                            boxProd.add(FormBox.create(footer -> {
                                footer.horizontal();
                                footer.add(FormBox.create(boxButton -> {
                                    boxButton.vertical();
                                    boxButton.add(FormButton.create(btn -> {
                                        btn.addStyle("success");
                                        btn.setText("Adicionar");
                                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ADICIONAR, ImageUtils.IconSize._24));
                                        btn.setOnAction(evt -> {
                                            if (listConsumoProd.stream().anyMatch(it -> it.getAplicacao() == null && it.getMaterial() == null)) {
                                                MessageBox.create(message -> {
                                                    message.message("Você já possui um item vazio adicionado");
                                                    message.type(MessageBox.TypeMessageBox.WARNING);
                                                    message.position(Pos.TOP_RIGHT);
                                                    message.notification();
                                                });
                                                return;
                                            }
                                            SdConsumoProd consumoProd = new SdConsumoProd(produto);
                                            consumoProd.setOrdem(listConsumoProd.size());
                                            listConsumoProd.add(consumoProd);
                                        });
                                    }));
                                }));
                                footer.add(FormBox.create(boxTotal -> {
                                    boxTotal.horizontal();
                                    boxTotal.setPadding(new Insets(0, 0, 0, (2 * WIDHTFIELDS) + 60));
                                    boxTotal.add(
                                            FormFieldText.create(field -> {
                                                field.addStyle("amber");
                                                field.alignment(Pos.CENTER);
                                                field.editable.set(false);
                                                field.clickable(false);
                                                field.withoutTitle();
                                                field.width(3 * WIDHTFIELDS);
                                                field.value.setValue("TOTAIS");
                                            }).build()
                                    );
                                    boxTotal.add(
                                            FormFieldText.create(field -> {
                                                field.width(WIDHTFIELDS);
                                                field.addStyle("amber");
                                                field.alignment(Pos.CENTER);
                                                field.editable.set(false);
                                                field.clickable(false);
                                                field.withoutTitle();
                                                field.postLabel("m");
                                                field.value.setValue(listConsumoProd.stream().map(SdConsumoProd::getConsuMetroMod).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(4, RoundingMode.HALF_EVEN).toString());
                                                addListenersTotal(listConsumoProd, field);
                                                listConsumoProd.sizeProperty().addListener((observable, oldValue, newValue) -> {
                                                    addListenersTotal(listConsumoProd, field);
                                                    field.value.setValue(listConsumoProd.stream().filter(it -> it.getMaterial() != null && it.getAplicacao() != null).map(SdConsumoProd::getConsuMetroMod).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(4, RoundingMode.HALF_EVEN).toString());
                                                });
                                            }).build()
                                    );
                                    boxTotal.add(boxEspaco -> {
                                        boxEspaco.vertical();
                                        boxEspaco.width((3 * WIDHTFIELDS) + 10);
                                    });
                                    boxTotal.add(
                                            FormFieldText.create(field -> {
                                                field.width(WIDHTFIELDS);
                                                field.addStyle("amber");
                                                field.alignment(Pos.CENTER);
                                                field.editable.set(false);
                                                field.clickable(false);
                                                field.withoutTitle();
                                                field.postLabel("%");
                                                field.value.setValue("100");
                                            }).build()
                                    );
                                }));
                            }));
                        }));
                        principal.add(new Separator(Orientation.VERTICAL));
                        principal.add(FormBox.create(boxImg -> {
                            boxImg.vertical();
                            boxImg.alignment(Pos.CENTER);
                            boxImg.add(
                                    FormFieldImage.create(img -> {
                                        VB2cProdutosEnvio foto = new FluentDao().selectFrom(VB2cProdutosEnvio.class).where(it -> it.equal("referencia", produto.getCodigo())).singleResult();
                                        img.title("Imagem");
                                        try {
                                            img.image(new Image(new FileInputStream(foto.getImagem()
                                                    .replace("https://imagens.deliz.com.br/produtos/",
                                                            "K:\\Loja Virtual\\imagens\\produtos\\")
                                                    .replace("flordelis", "Flor De Lis")), 180, 280, true, false)
                                            );
                                        } catch (FileNotFoundException | NullPointerException e) {
                                            try {
                                                img.image(new Image(new FileInputStream("K:\\Loja Virtual\\imagens\\produtos\\semimg.jpg"), 180, 280, true, false));
                                            } catch (FileNotFoundException ex) {
                                                ex.printStackTrace();
                                            }
                                        }
                                    }).build()
                            );
                            boxImg.add(boxGrade -> {
                                boxGrade.vertical();
                                boxGrade.add(
                                        FormFieldText.create(field -> {
                                            field.withoutTitle();
                                            field.editable.set(false);
                                            field.alignment(Pos.CENTER);
                                            List<PaIten> paItens = (List<PaIten>) new FluentDao().selectFrom(PaIten.class).where(it -> it
                                                            .equal("id.deposito", "0001")
                                                            .equal("id.codigo", produto.getCodigo()))
                                                    .resultList();
                                            String grade = "";
                                            if (paItens != null && paItens.size() > 0) {
                                                grade = paItens.stream().map(eb -> eb.getId().getTam()).distinct().sorted(SortUtils::sortTamanhos).reduce("", (x1, x2) -> x2 + "-");
                                            }
                                            field.value.setValue(grade.endsWith("-") ? new StringBuffer(grade).deleteCharAt(grade.length() - 1).toString() : grade);
                                        }).build()
                                );
                            });
                        }));
                    }));
                    if (listConsumoProd.size() == 0) {
                        SdConsumoProd consumoProd = new SdConsumoProd(produto);
                        consumoProd.setOrdem(listConsumoProd.size());
                        listConsumoProd.add(consumoProd);
                    }
                })
        );
    }

    private void addListenersTotal(ListProperty<SdConsumoProd> listConsumoProd, FormFieldText field) {
        listConsumoProd.forEach(consumo -> {
            consumo.consuMetroModProperty().addListener((observable1, oldValue1, newValue1) -> {
                if (newValue1 != null)
                    field.value.setValue(listConsumoProd.stream().filter(it -> it.getMaterial() != null && it.getAplicacao() != null).map(SdConsumoProd::getConsuMetroMod).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(4, RoundingMode.HALF_EVEN).toString());
            });
//            consumo.materialProperty().addListener((observable1, oldValue1, newValue1) -> {
//                field.value.setValue(listConsumoProd.stream().filter(it -> it.getMaterial() != null && it.getAplicacao() != null).map(SdConsumoProd::getConsuMetroMod).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(4, RoundingMode.HALF_EVEN).toString());
//            });
//            consumo.aplicacaoProperty().addListener((observable1, oldValue1, newValue1) -> {
//                field.value.setValue(listConsumoProd.stream().filter(it -> it.getMaterial() != null && it.getAplicacao() != null).map(SdConsumoProd::getConsuMetroMod).reduce(BigDecimal.ZERO, BigDecimal::add).setScale(4, RoundingMode.HALF_EVEN).toString());
//            });
        });
    }

    private Node addLinhaProduto(SdConsumoProd prod, ListProperty<SdConsumoProd> listConsumoProd) {
        if (prod == null) return FormBox.create();
        return FormBox.create(boxGeral -> {
            boxGeral.vertical();
            boxGeral.add(FormBox.create(box -> {
                box.horizontal();
                box.setPadding(Insets.EMPTY);
                box.alignment(Pos.BOTTOM_LEFT);
                box.add(FormBox.create(boxButton -> {
                    boxButton.vertical();
                    boxButton.alignment(Pos.BOTTOM_LEFT);
                    boxButton.add(FormButton.create(btn -> {
                        btn.addStyle("danger");
                        btn.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
                        btn.setOnAction(evt -> {
                            removeLinhaProduto(prod, listConsumoProd, boxGeral);
                        });
                    }));
                }));
                box.add(FormBox.create(boxInfos -> {
                    boxInfos.vertical();
                    boxInfos.alignment(Pos.BOTTOM_LEFT);
                    boxInfos.add(FormBox.create(boxInfosProd -> {
                        boxInfosProd.horizontal();
                        boxInfosProd.setPadding(Insets.EMPTY);
                        boxInfosProd.add(FormFieldText.create(field -> {
                            field.width(WIDHTFIELDS * 2);
                            field.title("Tecido");
                            field.alignment(Pos.CENTER);
                            field.setPadding(Insets.EMPTY);
                            field.value.setValue(prod.getMaterial() == null ? "" : prod.getMaterial().getCodigo());
                            field.textField.focusedProperty().addListener((observable, oldValue, newValue) -> {
                                if (!newValue) {
                                    definirMaterialConsumo(prod, field, listConsumoProd);
                                }
                            });
                            field.textField.setOnKeyReleased(evt -> {
                                if (evt.getCode().equals(KeyCode.ENTER) && !field.value.getValueSafe().equals("")) {
                                    definirMaterialConsumo(prod, field, listConsumoProd);
                                }
                            });
                            Platform.runLater(field::requestFocus);
                        }).build());
                        boxInfosProd.add(FormFieldSingleFind.create(Pcpapl.class, field -> {
                            field.dividedWidth((4 * WIDHTFIELDS) + 15);
                            field.title("Aplicação");
                            field.value.setValue(prod.getAplicacao() == null ? null : prod.getAplicacao());
                            field.value.addListener((observable, oldValue, newValue) -> {
                                definirAplicacaoConsumo(prod, field, listConsumoProd);
                            });
                        }).build());
                    }));
                }));
                box.add(FormBox.create(boxDadosConsumo -> {
                    boxDadosConsumo.horizontal();
                    boxDadosConsumo.alignment(Pos.BOTTOM_RIGHT);
                    boxDadosConsumo.add(createValueField(prod, "primary", "Consumo", prod.consuMetroModProperty(), "m", true).build());
                    boxDadosConsumo.add(createValueField(prod, "primary", "Largura", prod.largmodProperty(), "m", true).build());
                    boxDadosConsumo.add(createValueField(prod, "primary", "Gramatura", prod.grammodProperty(), "g/m²", false).build());
                    boxDadosConsumo.add(createValueField(prod, "primary", "Consumo", prod.consuKiloModProperty(), "Kg", false).build());
                    boxDadosConsumo.add(createPorcentagemField(prod, "dark", prod.porcentagemProperty()).build());
                    //<editor-fold desc="Listener">
                    configureListenersConsumos(listConsumoProd, prod);
                    prod.largmodProperty().addListener((observable, oldValue, newValue) -> {
                        if (newValue != null)
                            prod.setConsuKiloMod(prod.getConsuMetroMod().multiply(prod.getLargmod()).multiply(prod.getGrammod()).multiply(new BigDecimal("1.08")).setScale(4, RoundingMode.HALF_EVEN));
                    });
                    prod.grammodProperty().addListener((observable, oldValue, newValue) -> {
                        if (newValue != null)
                            prod.setConsuKiloMod(prod.getConsuMetroMod().multiply(prod.getLargmod()).multiply(prod.getGrammod()).multiply(new BigDecimal("1.08")).setScale(4, RoundingMode.HALF_EVEN));
                    });
                    //</editor-fold>
                }));
                box.add(FormBox.create(boxMostruario -> {
                    boxMostruario.horizontal();
                    boxMostruario.add(createValueField(prod, "warning", "Largura", prod.largmostProperty(), "m", true).build());
                    boxMostruario.add(createValueField(prod, "warning", "Gramatura", prod.grammostProperty(), "g/m²", false).build());
                    boxMostruario.add(createValueField(prod, "warning", "Consumo", prod.consumostProperty(), "", true).build());
                    boxMostruario.add(createPorcentagemField(prod, "danger", prod.porcentagemVariacaoProperty()).build());
                }));
                box.add(FormBox.create(boxProducao -> {
                    boxProducao.horizontal();
                    boxProducao.add(createValueField(prod, "info", "Largura", prod.largprodProperty(), "m", true).build());
                    boxProducao.add(createValueField(prod, "info", "Consumo", prod.consuprodProperty(), "", true).build());
                }));
            }));
            Separator separator = new Separator(Orientation.HORIZONTAL);
            separator.setPadding(new Insets(0, 5, 1, 5));
            boxGeral.add(separator);
            prod.setPorcentagem(calculaPorcentagem(listConsumoProd, prod));
            prod.setPorcentagemVariacao(calculaPorcentagemVariacao(prod));
        });
    }

    private void removeLinhaProduto(SdConsumoProd prod, ListProperty<SdConsumoProd> listConsumoProd, FormBox boxGeral) {
        VBox parent = (VBox) boxGeral.getParent();
        parent.getChildren().remove(boxGeral);
        if (prod.getMaterial() != null && prod.getAplicacao() != null) {
            SdConsumoProd consumoSalvo = new FluentDao().selectFrom(SdConsumoProd.class).where(it -> it
                            .equal("codigo.codigo", prod.getCodigo().getCodigo())
                            .equal("material.codigo", prod.getMaterial().getCodigo())
                            .equal("aplicacao.codigo", prod.getAplicacao().getCodigo())
                    )
                    .singleResult();
            if (consumoSalvo != null)
                new FluentDao().delete(consumoSalvo);
        }
        listConsumoProd.remove(prod);
        AtomicInteger contador = new AtomicInteger(0);
        listConsumoProd.stream().sorted(Comparator.comparing(SdConsumoProd::getOrdem)).forEachOrdered(item -> {
            item.setOrdem(contador.getAndIncrement());
        });
        new FluentDao().mergeAll(listConsumoProd);
        JPAUtils.clearEntitys(listConsumoProd);
        listConsumoProd.clear();
        listConsumoProd.set(FXCollections.observableArrayList(((List<SdConsumoProd>) new FluentDao().selectFrom(SdConsumoProd.class).where(it -> it.equal("codigo.codigo", prod.getCodigo().getCodigo())).resultList())));

        MessageBox.create(message -> {
            message.message("Material removido do produto");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }

    private void configureListenersConsumos(ListProperty<SdConsumoProd> listConsumoProd, SdConsumoProd consumoProd) {
        consumoProd.consuMetroModProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                consumoProd.setConsuKiloMod(consumoProd.getConsuMetroMod().multiply(consumoProd.getLargmod()).multiply(consumoProd.getGrammod()).multiply(new BigDecimal("1.08")).setScale(4, RoundingMode.HALF_EVEN));
                listConsumoProd.forEach(cons -> {
                    cons.setPorcentagem(calculaPorcentagem(listConsumoProd, cons));
                    if (consumoProd.getUnidade().getUnidade().equals("M") || consumoProd.getUnidade().getUnidade().equals("MT")) {
                        consumoProd.setPorcentagemVariacao(consumoProd.getConsumost()
                                .multiply(new BigDecimal("100"))
                                .divide(consumoProd.getConsuMetroMod(), 2, RoundingMode.HALF_EVEN)
                                .subtract(new BigDecimal("100"))
                        );
                    }
                });
            }
        });
        consumoProd.consuKiloModProperty().addListener((observable, oldValue, newValue) -> {
            if ((!consumoProd.getUnidade().getUnidade().equals("M") && !consumoProd.getUnidade().getUnidade().equals("MT")) && newValue.intValue() != 0) {
                consumoProd.setPorcentagemVariacao(consumoProd.getConsumost()
                        .multiply(new BigDecimal("100"))
                        .divide(newValue, 2, RoundingMode.HALF_EVEN)
                        .subtract(new BigDecimal("100"))
                );
            }
        });
        consumoProd.consumostProperty().addListener((observable, oldValue, newValue) -> {
            consumoProd.setPorcentagemVariacao(
                    calculaPorcentagemVariacao(consumoProd)
            );
        });
    }

    @NotNull
    private BigDecimal calculaPorcentagemVariacao(SdConsumoProd consumoProd) {
        try {
            return consumoProd.getConsumost()
                    .multiply(BIGDECIMAL_100)
                    .divide(consumoProd.getUnidade().getUnidade().equals("M") || consumoProd.getUnidade().getUnidade().equals("MT") ? consumoProd.getConsuMetroMod() : consumoProd.getConsuKiloMod(), 4, RoundingMode.HALF_EVEN)
                    .subtract(BIGDECIMAL_100);
        } catch (NullPointerException | ArithmeticException e) {
            e.printStackTrace();
            return BigDecimal.ZERO;
        }
    }

    @NotNull
    private BigDecimal calculaPorcentagem(List<SdConsumoProd> listConsumoProd, SdConsumoProd cons) {
        try {
            return cons.getConsuMetroMod()
                    .divide(
                            listConsumoProd.stream().allMatch(it -> it.getConsuMetroMod().equals(BigDecimal.ZERO)) ?
                                    BigDecimal.ONE : listConsumoProd.stream().map(SdConsumoProd::getConsuMetroMod).reduce(BigDecimal.ZERO, BigDecimal::add), 4, RoundingMode.HALF_EVEN)
                    .multiply(new BigDecimal("100"));
        } catch (Exception e) {
            e.printStackTrace();
            return BigDecimal.ZERO;
        }
    }

    //<editor-fold desc="Create Fields">
    private FormFieldText createPorcentagemField(SdConsumoProd consumoProd, String style, ObjectProperty<BigDecimal> property) {
        return FormFieldText.create(field -> {
            field.width(WIDHTFIELDS);
            field.addStyle(style);
            field.alignment(Pos.CENTER);
            field.editable.set(false);
            field.invisibleTitle();
            field.clickable(false);
            field.postLabel("%");
            field.setPadding(Insets.EMPTY);
            field.setVisible(consumoProd.getMaterial() != null && consumoProd.getAplicacao() != null);
            field.value.setValue(property.getValue().toString());
            property.addListener((observable, oldValue, newValue) -> {
                field.value.setValue(property.getValue().toString());
            });
            consumoProd.materialProperty().addListener((observable, oldValue, newValue) -> {
                field.setVisible(newValue != null && consumoProd.getAplicacao() != null);
            });
            consumoProd.aplicacaoProperty().addListener((observable, oldValue, newValue) -> {
                field.setVisible(newValue != null && consumoProd.getMaterial() != null);
            });
        });
    }

    private FormFieldText createValueField(SdConsumoProd prod, String style, String title, ObjectProperty<BigDecimal> property, String label, boolean porMetro) {
        return FormFieldText.create(field -> {
            AtomicReference<String> valorAntigo = new AtomicReference<>();
            field.width(WIDHTFIELDS);
            field.addStyle(style);
            field.alignment(Pos.CENTER);
            field.title(title);
            field.setPadding(Insets.EMPTY);
            field.value.setValue(property.getValue().setScale(4, RoundingMode.HALF_EVEN).toString());
            if (!label.equals("")) field.postLabel(label);
            else if (prod.getUnidade() != null) field.postLabel(prod.getUnidade().getAbreviacao());
            else field.postLabel(" ");

            if (property.equals(prod.consuKiloModProperty())) field.clickable(false);

            field.setVisible(prod.getMaterial() != null && prod.getAplicacao() != null && (porMetro || (!prod.getUnidade().getUnidade().equals("M") && !prod.getUnidade().getUnidade().equals("MT"))));
            prod.unidadeProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue != null && label.equals("")) {
                    field.getPostLabelField().setText(newValue.getAbreviacao());
                }
            });
            prod.materialProperty().addListener((observable, oldValue, newValue) -> {
                if (prod.getUnidade() == null && prod.getMaterial() != null)
                    prod.setUnidade(prod.getMaterial().getUnicom());
                field.setVisible(prod.getMaterial() != null && prod.getAplicacao() != null && (porMetro || (!prod.getUnidade().getUnidade().equals("M") && !prod.getUnidade().getUnidade().equals("MT"))));
            });
            prod.aplicacaoProperty().addListener((observable, oldValue, newValue) -> {
                if (prod.getUnidade() != null) {
                    field.setVisible(prod.getMaterial() != null && prod.getAplicacao() != null && (porMetro || (!prod.getUnidade().getUnidade().equals("M") && !prod.getUnidade().getUnidade().equals("MT"))));
                }
            });
            property.addListener((observable, oldValue, newValue) -> {
                if (newValue != null) {
                    field.value.setValue(property.getValue().setScale(4, RoundingMode.HALF_EVEN).toString());
                    if (prod.getMaterial() != null && prod.getAplicacao() != null) new FluentDao().merge(prod);
                }
            });
            field.focusedListener((observable, oldValue, newValue) -> {
                try {
                    if (newValue) valorAntigo.set(field.value.getValueSafe());
                    else if (field.value.getValueSafe().equals(""))
                        property.setValue(new BigDecimal(valorAntigo.get()).setScale(4, RoundingMode.HALF_EVEN));
                    else property.setValue(new BigDecimal(field.value.getValueSafe().replace(",", ".")));
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    field.value.setValue(valorAntigo.get());
                }
            });
            field.textField.setOnMouseClicked(event -> {
                if (event.getButton().equals(MouseButton.PRIMARY)) {
                    field.textField.selectAll();
                }
            });
        });
    }
    //</editor-fold>

    protected void buscaProduto(String value) {
        if (produtosBean.stream().noneMatch(it -> it.getCodigo().equals(value))) {
            Produto codigo = new FluentDao().selectFrom(Produto.class).where(it -> it.equal("codigo", value)).singleResult();
            if (codigo == null) {
                MessageBox.create(message -> {
                    message.message("Produto não encontrado");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
                return;
            }
            addReferencia(codigo);
        }
    }

    private void criaItensTreeTableView() {
        TreeItem<SdConsumoProd> rootNode = new TreeItem<>(new SdConsumoProd());
        Map<Produto, List<SdConsumoProd>> mapMateriasProduto = consumos.stream().collect(Collectors.groupingBy(SdConsumoProd::getCodigo));
        mapMateriasProduto.forEach((produto, itens) -> {
            TreeItem<SdConsumoProd> itemCodigo = new TreeItem<>(new SdConsumoProd(produto));
            itens.stream().sorted(Comparator.comparing(SdConsumoProd::getOrdem)).forEach(item -> itemCodigo.getChildren().add(new TreeItem<>(item)));
            rootNode.getChildren().add(itemCodigo);
        });
        tblConsumo.properties().setRoot(rootNode);
    }

    private void editarProduto(SdConsumoProd item) {
        buscaProduto(item.getCodigo().getCodigo());
        super.tabs.getSelectionModel().select(1);
    }

    private FormFieldText createTextFieldToSelectOnTable(String value) {
        return FormFieldText.create(field -> {
            field.editable.set(false);
            field.withoutTitle();
            field.width(200);
            field.textField.setOnMouseClicked(evt -> {
                if (evt.getButton().equals(MouseButton.PRIMARY)) {
                    field.textField.selectAll();
                }
            });
            field.value.setValue(value);
        });
    }
}
