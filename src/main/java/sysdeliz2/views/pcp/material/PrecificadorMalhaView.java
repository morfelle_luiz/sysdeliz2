package sysdeliz2.views.pcp.material;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import sysdeliz2.controllers.views.pcp.material.PrecificadorMalhaController;
import sysdeliz2.daos.NativeDAO;
import sysdeliz2.models.ti.Entidade;
import sysdeliz2.models.ti.Material;
import sysdeliz2.models.view.VSdMateriaisMalha;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.FormBox;
import sysdeliz2.utils.gui.components.FormButton;
import sysdeliz2.utils.gui.components.FormFieldSingleFind;
import sysdeliz2.utils.gui.components.FormFieldText;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.CustomSelect;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PrecificadorMalhaView extends PrecificadorMalhaController {

    // <editor-fold defaultstate="collapsed" desc="Bean">
    protected final ListProperty<VSdMateriaisMalha> materiaisListagem = new SimpleListProperty<>(FXCollections.observableArrayList());
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Filter">
    private final FormFieldSingleFind<Material> filterCodMaterialField = FormFieldSingleFind.create(Material.class, field -> {
        field.title("Código Material");
        field.value.setValue(null);
        field.width(300);
        field.widthCode(100.0);
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Field">
    private final FormFieldText precoParcialTecelagem = FormFieldText.create(field -> {
        field.title("Preço Parcial");
        field.width(100);
        field.editable.set(false);
    });

    private final FormFieldText precoParcialTinturaria = FormFieldText.create(field -> {
        field.title("Preço Parcial");
        field.width(100);
        field.editable.set(false);
    });
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Button">
    private final Button buscarMaterialButton = FormButton.create(formButton -> {
        formButton.title("Buscar");
        formButton.addStyle("success");
        formButton.setAlignment(Pos.BOTTOM_LEFT);
        formButton.addStyle("sm");
        formButton.setAction(event -> {
            try {
                buscarMalhaPrincipal(filterCodMaterialField.value.getValue());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    });
    private final Button salvarPrecoButton = FormButton.create(formButton -> {
        formButton.title("Salvar Preço");
        formButton.addStyle("danger");
        formButton.setAlignment(Pos.BOTTOM_LEFT);
        formButton.addStyle("sm");
        formButton.setAction(event -> {
            try {
                salvarPreco();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    });

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Dados">
    private VSdMateriaisMalha malhaPrincipal = new VSdMateriaisMalha();
    private BigDecimal precoTotalMat = BigDecimal.ZERO;
    private BigDecimal custoTec = BigDecimal.ZERO;
    private BigDecimal peso = BigDecimal.ZERO;
    private BigDecimal ppTin = BigDecimal.ZERO;
    private BigDecimal ppTec = BigDecimal.ZERO;
    private List<VSdMateriaisMalha> faseTinturaria = new ArrayList<>();
    private List<VSdMateriaisMalha> faseTecelagem = new ArrayList<>();
    private List<VSdMateriaisMalha> malhasCruas = new ArrayList<>();
    private List<VSdMateriaisMalha> faseFios = new ArrayList<>();
    private FormBox boxDados = new FormBox();
    private String unidade;

    // </editor-fold>

    public PrecificadorMalhaView() {
        super("Precificador de Malha", ImageUtils.getImage(ImageUtils.Icon.COMERCIAL));
        init();
    }

    private void init() {
        super.box.getChildren().add(FormBox.create(principal -> {
            principal.horizontal();
            principal.add(FormBox.create(header -> {
                header.vertical();
                header.add(FormBox.create(filterBox -> {
                    filterBox.horizontal();
                    filterBox.alignment(Pos.BOTTOM_LEFT);
                    filterBox.add(filterCodMaterialField.build());
                    filterBox.add(buscarMaterialButton);
                }));
            }));
        }));
    }

    private void buscarMalhaPrincipal(Material material) throws Exception {
        createSection();
        consultarMaterial(material);
        materiaisListagem.set(FXCollections.observableList(materiais));
        if (materiaisListagem.get(0).getFio() != null || materiaisListagem.get(0).getCodigo() != null) {
            if (materiaisListagem.size() >= 1 && materiaisListagem.get(0).getFio() != null) trataMalhaCrua();

            if (materiaisListagem.get(0).getFio() == null) {
                malhaPrincipal = materiaisListagem.size() > 1 ? telaSelecao(materiaisListagem) : materiaisListagem.get(0);
                unidade = malhaPrincipal.getUnidade();
                peso = malhaPrincipal.getPeso();
                trataMalhaTingida(malhaPrincipal);
            }
        }
    }

    private void createSection() {
        malhaPrincipal = new VSdMateriaisMalha();
        precoTotalMat = BigDecimal.ZERO;
        custoTec = BigDecimal.ZERO;
        peso = BigDecimal.ZERO;
        faseFios.clear();
        faseTecelagem.clear();
        faseTinturaria.clear();
        filhos.clear();
        fios.clear();
        materiais.clear();
        malhasCruas.clear();

        if (super.box.getChildren().size() > 1) super.box.getChildren().remove(1);
        boxDados = new FormBox();
        boxDados.vertical();
        super.box.getChildren().add(boxDados.build());
    }

    private void trataMalhaCrua() throws Exception {
        ObservableList<Entidade> fornecedores = FXCollections.observableList(materiaisListagem.stream().map(VSdMateriaisMalha::getCodfor).distinct().collect(Collectors.toList()));
        Entidade ent = fornecedores.size() > 1 ? telaSelecaoFornecedor(fornecedores) : fornecedores.get(0);
        materiaisListagem.removeIf(b -> !b.getCodfor().getCodcli().equals(ent.getCodcli()));

        if (malhaPrincipal.getCodigoretorno() == null) malhaPrincipal = materiaisListagem.get(0);
        malhasCruas = materiaisListagem;

        unidade = malhaPrincipal.getUnidade();
        peso = malhaPrincipal.getPeso();

        for (VSdMateriaisMalha tec :
                materiaisListagem) {
            custoTec = custoTec.add(tec.getCustotecel());
        }
        if (malhaPrincipal.getFio() != null) {
            malhaPrincipal.setCustototalTemp(custoTec);
        } else {
            malhasCruas.get(0).setCustototalTemp(custoTec);
            faseTecelagem.add(malhasCruas.get(0));
        }
        pegarFios();
    }

    private void trataMalhaTingida(VSdMateriaisMalha malha) throws Exception {
        getFilho(malha);
        if (filhos.get(0).getFio() == null) {
            if (filhos.size() > 1) {
                malha = telaSelecao(filhosBean);
            } else {
                malha = filhos.get(0);
            }
            if (hasTinturaria(malha)) {
                faseTinturaria.add(malha);
                trataMalhaTingida(malha);
            } else {
                faseTecelagem.add(malha);
                createFields();
                gerarFieldPreco();
            }
        } else {
            consultarMaterial(malha.getCodigo());
            materiaisListagem.set(FXCollections.observableList(materiais));
            trataMalhaCrua();
        }
    }

    private void pegarFios() throws Exception {
        VSdMateriaisMalha filho = malhaPrincipal;
        if (!hasTecelagem(filho) && notFio(filho)) {
            do {
                getFilho(filho);
                filho = filhos.get(0);
            } while (!hasTecelagem(filho) && (filho.getCodigo() != null && filho.getFio() != null) );
        }
        if (notFio(filho)) {
            for (VSdMateriaisMalha malhaCrua :
                    malhasCruas) {
                BigDecimal custoFioTemp = malhaCrua.getCustofio();
                consultaFios(malhaCrua);
                fiosBean.set(FXCollections.observableList(fios));
                if (fios.size() > 1) {
                    VSdMateriaisMalha fioSelecionado = telaSelecao(fiosBean);
                    fioSelecionado.setCustototalTemp(custoFioTemp.add(fioSelecionado.getCustototalTemp()));
                    faseFios.add(fioSelecionado);
                } else {
                    fios.get(0).setCustototalTemp(custoFioTemp);
                    faseFios.addAll(fios);
                }
            }
        }
        createFields();
        gerarFieldPreco();
    }

    private void createFields() {
        if (malhaPrincipal.getCodigoretorno() != null) {
            ArrayList<VSdMateriaisMalha> array = new ArrayList<VSdMateriaisMalha>();
            array.add(malhaPrincipal);
            feedFields(array, "Malha");
        }
        if (faseTinturaria.size() > 0) feedFields(faseTinturaria, "Tinturaria");
        if (faseTecelagem.size() > 0) feedFields(faseTecelagem, "Tecelagem");
        if (faseFios.size() > 0) feedFields(faseFios, "Fios");

        ppTin = precoTotalMat.subtract(malhaPrincipal.getCustototalTemp());
        ppTec = ppTin.subtract(faseTinturaria.size() > 0 ? faseTinturaria.get(0).getCustototalTemp() : BigDecimal.ZERO);
        precoParcialTecelagem.value.setValue(StringUtils.toMonetaryFormat(ppTec, 3));
        precoParcialTinturaria.value.setValue(StringUtils.toMonetaryFormat(ppTin, 3));
    }

    private void gerarFieldPreco() {
        FormBox boxPreco = new FormBox();
        boxPreco.horizontal();
        boxPreco.maxWidthSize(400);
        boxDados.add(boxPreco.build());

        boxPreco.add(FormBox.create(box -> {
            box.horizontal();
            box.alignment(Pos.BOTTOM_LEFT);
            if (!unidade.equals("PC")) {
                box.add(FormFieldText.create(field -> {
                    field.title("Custo Final");
                    field.value.setValue(StringUtils.toMonetaryFormat(Double.parseDouble(precoTotalMat.toString()), 3));
                    field.editable.set(false);
                    field.width(200);
                    field.postLabel(unidade);
                }).build());
            } else {
                precoTotalMat = precoTotalMat.multiply(peso);
                box.add(FormFieldText.create(field -> {
                    field.title("Preço por Peça");
                    field.value.setValue(StringUtils.toMonetaryFormat(Double.parseDouble(precoTotalMat.toString()), 3));
                    field.editable.set(false);
                    field.width(200);
                    field.postLabel(unidade);
                }).build());
            }
            box.add(salvarPrecoButton);
        }));
    }

    private VSdMateriaisMalha telaSelecao(ListProperty<VSdMateriaisMalha> lista) throws Exception {
        CustomSelect<VSdMateriaisMalha> selectMaterial = CustomSelect.create(VSdMateriaisMalha.class, lista, "Selecione a Cor do Material");
        return selectMaterial.selectedReturn;
    }

    private Entidade telaSelecaoFornecedor(ObservableList<Entidade> fornecedores) throws Exception {
        CustomSelect<Entidade> selectEntidade = CustomSelect.create(Entidade.class, fornecedores, "Selecione o Fornecedor da Malha");
        return selectEntidade.selectedReturn;
    }

    private void feedFields(List<VSdMateriaisMalha> lista, String nomeBox) {

        FormBox boxFase = new FormBox();
        boxFase.vertical();
        boxFase.visible.set(true);
        boxFase.alignment(Pos.CENTER_LEFT);
        boxFase.maxWidthSize(800);
        boxFase.title(nomeBox);
        boxFase.border();

        boxDados.add(boxFase.build());

        for (VSdMateriaisMalha selecionado : lista) {
            String codigoRetorno = selecionado.getCodigoretorno().getCodigo();
            BigDecimal custoTotal = selecionado.getCustotecel().compareTo(BigDecimal.ZERO) > 0 ? custoTec : selecionado.getCustototalTemp();
            String descricao = selecionado.getCodigoretorno().getDescricao();

            precoTotalMat = precoTotalMat.add(custoTotal);
            FormBox boxMaterial = new FormBox();
            boxMaterial.horizontal();

            boxFase.add(boxMaterial.build());

            boxMaterial.add(FormFieldText.create(field -> {
                field.title("Código");
                field.value.setValue(codigoRetorno);
                field.editable.set(false);
                field.width(200);
                field.maxWidth(200);
            }).build(), FormFieldText.create(field -> {
                field.title("Descrição");
                field.value.setValue(descricao);
                field.editable.set(false);
                field.width(300);
                field.maxWidth(300);
            }).build(), FormFieldText.create(field -> {
                field.title("Valor");
                field.value.setValue(StringUtils.toMonetaryFormat(Double.parseDouble(custoTotal.toString()), 3));
                field.width(100);
                field.maxWidth(100);
                field.editable.set(false);
            }).build());
            if (nomeBox.equals("Tinturaria")) {
                boxMaterial.add(precoParcialTinturaria.build());
            }
            if (nomeBox.equals("Tecelagem")) {
                boxMaterial.add(precoParcialTecelagem.build());
            }

        }
    }

    private void salvarPreco() throws SQLException {

        if (!precoTotalMat.equals(BigDecimal.ZERO) && malhaPrincipal.getCodigoretorno() != null) {
            if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                message.message("Deseja atualizar o preço do material?");
                message.showAndWait();
            }).value.get())) {
                BigDecimal precoAntigo = getPrecoAntigo(malhaPrincipal.getCodigoretorno().getCodigo());
                new NativeDAO().runNativeQueryUpdate(String.format(
                        "update material_001" +
                                " set preco = '%s'" +
                                " where codigo = '%s'"
                        , precoTotalMat.toString().replace('.', ','), malhaPrincipal.getCodigoretorno().getCodigo()
                ));
                MessageBox.create(message -> {
                    message.message("Preço atualizado com sucesso!");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
                SysLogger.addSysDelizLog("Precificação de Malha", TipoAcao.EDITAR, malhaPrincipal.getCodigoretorno().getCodigo(),
                        "Malha: " + malhaPrincipal.getCodigoretorno().getCodigo() +
                                "\n Preço atualizado de: " + precoAntigo.toString().replace('.', ',') + " para " + precoTotalMat.toString().replace('.', ','));
            }

        }

    }
}

