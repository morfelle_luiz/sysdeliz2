package sysdeliz2.views.pcp.producao;

import com.calendarfx.model.Calendar;
import com.calendarfx.model.CalendarSource;
import com.calendarfx.model.Entry;
import com.calendarfx.view.CalendarView;
import com.calendarfx.view.popover.EntryPopOverContentPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import sysdeliz2.controllers.views.pcp.producao.ControleProducaoController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.fluentDao.TipoExpressao;
import sysdeliz2.models.sysdeliz.cadastros.SdFamiliasRecurso;
import sysdeliz2.models.sysdeliz.pcp.SdDemandaFaccao;
import sysdeliz2.models.sysdeliz.pcp.SdFluxoAtualOf;
import sysdeliz2.models.ti.CadFluxo;
import sysdeliz2.models.ti.Familia;
import sysdeliz2.models.view.VSdDadosEntidade;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.gui.components.FormBox;
import sysdeliz2.utils.gui.components.FormBoxPane;
import sysdeliz2.utils.gui.components.FormFieldMultipleFind;
import sysdeliz2.utils.gui.components.FormTitledPane;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class AgendaProducaoView extends ControleProducaoController {

    // <editor-fold defaultstate="collapsed" desc="tabs">
    private final VBox tabAgenda = (VBox) super.box;
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="agenda">
    private final FormFieldMultipleFind<Familia> fieldFilterFamiliaAgenda = FormFieldMultipleFind.create(Familia.class, field -> {
        field.title("Famílias");
        field.toUpper().width(120);
    });
    private final FormFieldMultipleFind<CadFluxo> fieldFilterSetorAgenda = FormFieldMultipleFind.create(CadFluxo.class, field -> {
        field.title("Setores");
        field.toUpper().width(120);
    });
    private final FormFieldMultipleFind<VSdDadosEntidade> fieldFilterFaccoesAgenda = FormFieldMultipleFind.create(VSdDadosEntidade.class, field -> {
        field.title("Facções");
        field.toUpper().width(120);
    });

    private Calendar ofsSetorAtual = new Calendar("Demandas Facções");
    private Calendar ofsProximosSetores = new Calendar("Próximas Demandas");
    private Calendar ofsPlanejamentoSetores = new Calendar("Demandas Planejadas");
    private CalendarSource myCalendarSource = new CalendarSource("Agenda das Facções");
    private CalendarView calendarView = new CalendarView();
    // </editor-fold>

    public AgendaProducaoView() {
        super("Agenda de Produção (Setor/Recursos)", ImageUtils.getImage(ImageUtils.Icon.CALENDARIO_PRODUTO));
        initAgenda();
    }

    // ------------------- AGENDA --------------------------
    private void initAgenda() {
        ofsSetorAtual.setStyle(Calendar.Style.STYLE1);
        ofsProximosSetores.setStyle(Calendar.Style.STYLE3);
        ofsPlanejamentoSetores.setStyle(Calendar.Style.STYLE2);
        myCalendarSource.getCalendars().addAll(ofsSetorAtual, ofsProximosSetores, ofsPlanejamentoSetores);

        calendarView.showDeveloperConsoleProperty().set(false);
        calendarView.setShowSourceTrayButton(true);
        calendarView.setShowSourceTray(false);
        calendarView.setShowSearchField(true);
        calendarView.setShowToolBar(true);
        calendarView.setShowSearchResultsTray(true);
        calendarView.setShowAddCalendarButton(false);
        calendarView.setShowPageToolBarControls(true);
        calendarView.setShowPrintButton(false);
        calendarView.setShowPageSwitcher(true);
        calendarView.setShowToday(true);
        calendarView.setTraysAnimated(true);
        calendarView.setEntryDetailsPopOverContentCallback(param -> {
            return new EntryPopOverContentPane(param.getPopOver(), param.getDateControl(), param.getEntry());
        });

        calendarView.showMonthPage();
        calendarView.setRequestedTime(LocalTime.now());
        calendarView.setToday(LocalDate.now());
        calendarView.setTime(LocalTime.now());
        calendarView.getCalendarSources().addAll(myCalendarSource);

        tabAgenda.getChildren().add(FormBoxPane.create(content -> {
            content.expanded();
            content.top(FormBox.create(container -> {
                container.horizontal();
                container.add(FormTitledPane.create(filter -> {
                    filter.filter();
                    filter.add(fields -> fields.addHorizontal(fieldFilterFamiliaAgenda.build(), fieldFilterFaccoesAgenda.build(), fieldFilterSetorAgenda.build()));
                    filter.find.setOnAction(evt -> {
                        Object[] setores = fieldFilterSetorAgenda.objectValues.stream().map(set -> set.getCodigo()).toArray();
                        Object[] faccoes = fieldFilterFaccoesAgenda.objectValues.stream().map(set -> set.getCodcli()).toArray();
                        Object[] familias = fieldFilterFamiliaAgenda.objectValues.stream().map(fam -> fam.getCodigo()).toArray();

                        ofsSetorAtual.clear();
                        ofsProximosSetores.clear();
                        ofsPlanejamentoSetores.clear();

                        AtomicReference<Exception> exRwo = new AtomicReference<>();
                        AtomicReference<List<SdFluxoAtualOf>> ofSetorAtual = new AtomicReference<>(new ArrayList<>());
                        AtomicReference<List<SdFluxoAtualOf>> ofProximoSetor = new AtomicReference<>(new ArrayList<>());
                        AtomicReference<List<SdFluxoAtualOf>> ofPlanejamentoSetor = new AtomicReference<>(new ArrayList<>());
                        new RunAsyncWithOverlay(this).exec(task -> {

                            Object[] faccoesFamilia = new Object[]{};
                            if (familias.length > 0) {
                                List<SdFamiliasRecurso> recursosFamilia = (List<SdFamiliasRecurso>) new FluentDao().selectFrom(SdFamiliasRecurso.class).where(eb -> eb.isIn("id.familia.codigo", familias)).resultList();
                                faccoesFamilia = recursosFamilia.stream().map(rec -> rec.getId().getRecurso().getFornecedor().getCodcli()).distinct().toArray();
                            }

                            List<Object> concatArrays = new ArrayList<>();
                            concatArrays.addAll(Arrays.asList(faccoes));
                            concatArrays.addAll(Arrays.asList(faccoesFamilia));
                            Object[] finalFaccoesFamilia = concatArrays.stream().filter(fac -> fac != null).distinct().toArray();

                            List<SdFluxoAtualOf> ofsProducao = (List<SdFluxoAtualOf>) new FluentDao().selectFrom(SdFluxoAtualOf.class)
                                    .where(eb -> eb
                                            .equal("emProducao", true)
                                            .isIn("id.setor.codigo", setores, TipoExpressao.AND, b -> setores.length > 0)
                                            .isIn("id.faccao.codcli", finalFaccoesFamilia, TipoExpressao.AND, b -> finalFaccoesFamilia.length > 0))
                                    .resultList();
                            ofSetorAtual.set(ofsProducao.stream().filter(of -> of.getId().isSetorAtual()).collect(Collectors.toList()));
                            ofProximoSetor.set(ofsProducao.stream().filter(of -> !of.getId().isSetorAtual() && of.isEmProducao()).collect(Collectors.toList()));
                            ofPlanejamentoSetor.set(ofsProducao.stream().filter(of -> !of.getId().isSetorAtual() && !of.isEmProducao()).collect(Collectors.toList()));

                            for (SdFluxoAtualOf vSdFluxoOf : ofSetorAtual.get()) {
                                Entry<SdFluxoAtualOf> ofEntry = new Entry<>();
                                ofEntry.setCalendar(ofsSetorAtual);
                                ofEntry.setUserObject(vSdFluxoOf);
                                ofEntry.setTitle(vSdFluxoOf.getId().getFaccao().toString() + " / " + vSdFluxoOf.getId().getSetor().getCodigo() + " (" + vSdFluxoOf.getId().getNumero().getNumero() +")");
                                ofEntry.setLocation("Qtde: " + vSdFluxoOf.getQtdeOf() + (vSdFluxoOf.isAtrasado() ? " - OF em Atraso! Dias: " + vSdFluxoOf.getDiasAtraso() : ""));
                                ofEntry.setInterval(vSdFluxoOf.getId().getDtInicio(), vSdFluxoOf.getId().getDtRetorno());
                                ofEntry.setFullDay(true);
                            }
                            for (SdFluxoAtualOf vSdFluxoOf : ofProximoSetor.get()) {
                                Entry<SdFluxoAtualOf> ofEntry = new Entry<>();
                                ofEntry.setCalendar(ofsProximosSetores);
                                ofEntry.setUserObject(vSdFluxoOf);
                                ofEntry.setTitle(vSdFluxoOf.getId().getFaccao().toString() + " / " + vSdFluxoOf.getId().getSetor().getCodigo() + " (" + vSdFluxoOf.getId().getNumero().getNumero() +")");
                                ofEntry.setLocation("Qtde: " + vSdFluxoOf.getQtdeOf() + (vSdFluxoOf.isAtrasado() ? " - OF em Atraso! Dias: " + vSdFluxoOf.getDiasAtraso() : ""));
                                ofEntry.setInterval(vSdFluxoOf.getId().getDtInicio(), vSdFluxoOf.getId().getDtRetorno());
                                ofEntry.setFullDay(true);
                            }
                            for (SdFluxoAtualOf vSdFluxoOf : ofPlanejamentoSetor.get()) {
                                Entry<SdFluxoAtualOf> ofEntry = new Entry<>();
                                ofEntry.setCalendar(ofsPlanejamentoSetores);
                                ofEntry.setUserObject(vSdFluxoOf);
                                ofEntry.setTitle(vSdFluxoOf.getId().getFaccao().toString() + " / " + vSdFluxoOf.getId().getSetor().getCodigo() + " (" + vSdFluxoOf.getId().getNumero().getNumero() +")");
                                ofEntry.setLocation("Qtde: " + vSdFluxoOf.getQtdeOf() + (vSdFluxoOf.isAtrasado() ? " - OF em Atraso! Dias: " + vSdFluxoOf.getDiasAtraso() : ""));
                                ofEntry.setInterval(vSdFluxoOf.getId().getDtInicio(), vSdFluxoOf.getId().getDtRetorno());
                                ofEntry.setFullDay(true);
                            }

                            return ReturnAsync.OK.value;
                        }).addTaskEndNotification(taskReturn -> {
                            if (taskReturn.equals(ReturnAsync.OK.value)) {
                                filter.collapse(false);
                            }
                        });

                    });
                    filter.clean.setOnAction(evt -> {
                        fieldFilterFaccoesAgenda.clear();
                        fieldFilterSetorAgenda.clear();
                        fieldFilterFamiliaAgenda.clear();
                    });
                }));
            }));
            content.center(FormBox.create(container -> {
                container.vertical();
                container.expanded();

                VBox.setVgrow(calendarView, Priority.ALWAYS);
                container.add(calendarView);
            }));
        }));
    }

}
