package sysdeliz2.views.pcp.producao;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.geometry.Side;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import org.controlsfx.control.PopOver;
import sysdeliz2.controllers.views.pcp.producao.ControleProducaoController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.daos.generics.helpers.Ordenacao;
import sysdeliz2.models.sysdeliz.SdColaborador;
import sysdeliz2.models.sysdeliz.SdPesosCamposOrd;
import sysdeliz2.models.sysdeliz.cadastros.SdRecursoProducao;
import sysdeliz2.models.sysdeliz.pcp.SdDemandaFaccao;
import sysdeliz2.models.sysdeliz.pcp.SdDemandaOf;
import sysdeliz2.models.sysdeliz.pcp.SdFluxoAtualOf;
import sysdeliz2.models.sysdeliz.pcp.SdProgFaccao;
import sysdeliz2.models.ti.Ano;
import sysdeliz2.models.ti.CadFluxo;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Familia;
import sysdeliz2.models.view.VSdDadosEntidade;
import sysdeliz2.models.view.VSdDadosOfPendente;
import sysdeliz2.models.view.VSdDadosProduto;
import sysdeliz2.models.view.VSdGrupoCadFluxo;
import sysdeliz2.models.view.pcp.VSdDemandaFaccao;
import sysdeliz2.utils.Globals;
import sysdeliz2.utils.ReportUtils;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.exceptions.BreakException;
import sysdeliz2.utils.exceptions.ContinueException;
import sysdeliz2.utils.exceptions.FormValidationException;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.hibernate.DefaultFilter;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class ControleProducaoView extends ControleProducaoController {

    // <editor-fold defaultstate="collapsed" desc="bean">
    private final BooleanProperty colapseDiario = new SimpleBooleanProperty(false);
    private SdColaborador controladorLogado = Globals.getColaboradorSistema();
    private AtomicReference<List<SdDemandaFaccao>> diarioRecurso = new AtomicReference<>(new ArrayList<>());
    private AtomicReference<List<Diario>> diario = new AtomicReference<>(new ArrayList<>());
    private final List<SdPesosCamposOrd> pesosDistDemanda = (List<SdPesosCamposOrd>) new FluentDao().selectFrom(SdPesosCamposOrd.class)
            .where(eb -> eb.equal("codigo", 1)).resultList();
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="tabs">
    private final VBox tabProgramacao = (VBox) super.box;
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="programacao">
    private final FormFieldMultipleFind<VSdDadosEntidade> fieldFilterFaccoes = FormFieldMultipleFind.create(VSdDadosEntidade.class, field -> {
        field.title("Facções");
        field.toUpper();
        field.width(130.0);
    });
    private final FormFieldSingleFind<VSdGrupoCadFluxo> fieldFilterSetor = FormFieldSingleFind.create(VSdGrupoCadFluxo.class, field -> {
        field.title("Setor");
        field.toUpper();
        field.width(260.0);
    });
    private final FormFieldMultipleFind<Familia> fieldFilterFamilia = FormFieldMultipleFind.create(Familia.class, field -> {
        field.title("Familias");
        field.toUpper();
        field.width(110.0);
    });
    private final FormFieldMultipleFind<Colecao> fieldFilterColecao = FormFieldMultipleFind.create(Colecao.class, field -> {
        field.title("Coleção");
        field.toUpper();
        field.width(110.0);
    });
    private final FormFieldMultipleFind<SdColaborador> fieldFilterControlador = FormFieldMultipleFind.create(SdColaborador.class, field -> {
        field.title("Controlador");
        field.defaults.add(new DefaultFilter("121","codigoFuncao"));
        field.toUpper().width(120);
    });
    private final FormFieldMultipleFind<VSdDadosProduto> fieldFilterProduto = FormFieldMultipleFind.create(VSdDadosProduto.class, field -> {
        field.title("Produto");
        field.toUpper().width(120);
    });
    private final FormFieldDate fieldFilterDataEntrega = FormFieldDate.create(field -> {
        field.title("Dt. Entrega");
        field.width(120);
    });
    private final FormFieldToggle fieldFilterVerPlanejamento = FormFieldToggle.create(field -> {
        field.title("Ver Planejamento");
        field.value.set(false);
    });
    private final FormFieldToggle fieldFilterProximoSetor = FormFieldToggle.create(field -> {
        field.title("Próx. Setor");
        field.value.set(false);
    });
    private final FormFieldSegmentedButton<String> fieldFilterTipoVisao = FormFieldSegmentedButton.create(field -> {
        field.title("Visão");
        field.options(
                field.option("Diária", "D", FormFieldSegmentedButton.Style.PRIMARY),
                field.option("Semanal", "S", FormFieldSegmentedButton.Style.DANGER)
        );
        field.select(0);
    });
    private final FormTableView<VSdDemandaFaccao> tblDemandasFaccao = FormTableView.create(VSdDemandaFaccao.class, table -> {
        table.withoutHeader();
        table.expanded();
        table.tableProperties().minHeight(180.0);
        table.columns(
                FormTableColumnGroup.createGroup(group -> {
                    group.title("Recurso");
                    group.addColumn(
                            FormTableColumn.create(cln -> {
                                cln.title("Código");
                                cln.width(50);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdDemandaFaccao, VSdDemandaFaccao>, ObservableValue<VSdDemandaFaccao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getFaccao() == null ? "-" : param.getValue().getId().getFaccao().getCodcli()));
                                cln.alignment(Pos.CENTER);
                            }).build() /*Código*/,
                            FormTableColumn.create(cln -> {
                                cln.title("Facção");
                                cln.width(270.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdDemandaFaccao, VSdDemandaFaccao>, ObservableValue<VSdDemandaFaccao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getFaccao() == null ? "Sem Facção" : param.getValue().getId().getFaccao().getRazaosocial()));
                            }).build() /*Facção*/,
                            FormTableColumn.create(cln -> {
                                cln.title("Localização");
                                cln.width(160.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdDemandaFaccao, VSdDemandaFaccao>, ObservableValue<VSdDemandaFaccao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getFaccao().getCodcli().equals("SFAC") ? "" :
                                        param.getValue().getId().getFaccao().getCidade().concat("/").concat(param.getValue().getId().getFaccao().getUf())));
                            }).build() /*Localização*/,
                            FormTableColumn.create(cln -> {
                                cln.title("Cód.");
                                cln.width(50);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdDemandaFaccao, VSdDemandaFaccao>, ObservableValue<VSdDemandaFaccao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getSetor().getCodigo()));
                                cln.alignment(Pos.CENTER);
                            }).build() /*C. Setor*/,
                            FormTableColumn.create(cln -> {
                                cln.title("Setor");
                                cln.width(130);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdDemandaFaccao, VSdDemandaFaccao>, ObservableValue<VSdDemandaFaccao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getSetor().getDescricao()));
                            }).build() /*Setor*/,
                            FormTableColumn.create(cln -> {
                                cln.title("Capac.");
                                cln.width(70.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdDemandaFaccao, VSdDemandaFaccao>, ObservableValue<VSdDemandaFaccao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCapacidadeRecurso()));
                                cln.alignment(Pos.CENTER);
                                cln.format(param -> {
                                    return new TableCell<VSdDemandaFaccao, BigDecimal>() {
                                        @Override
                                        protected void updateItem(BigDecimal item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            if (item != null && !empty) {
                                                setText(StringUtils.toDecimalFormat(item));
                                            }
                                        }
                                    };
                                });
                            }).build() /*Capacidade*/);
                }).build() /*Recurso*/,
                FormTableColumnGroup.createGroup(group -> {
                    group.title("Status");
                    group.addColumn(
                            FormTableColumn.create(cln -> {
                                cln.title("Orig.");
                                cln.width(45);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdDemandaFaccao, VSdDemandaFaccao>, ObservableValue<VSdDemandaFaccao>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                cln.alignment(Pos.CENTER);
                                cln.format(param -> {
                                    return new TableCell<VSdDemandaFaccao, VSdDemandaFaccao>() {
                                        @Override
                                        protected void updateItem(VSdDemandaFaccao item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            setGraphic(null);
                                            if (item != null && !empty) {
                                                setGraphic(item.isAtrasadoOrig() ? ImageUtils.getIcon(ImageUtils.Icon.ALERT_ERROR, ImageUtils.IconSize._16) : null);
                                            }
                                        }
                                    };
                                });
                            }).build() /*Orig.*/,
                            FormTableColumn.create(cln -> {
                                cln.title("Atraso (dd)");
                                cln.width(65);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdDemandaFaccao, VSdDemandaFaccao>, ObservableValue<VSdDemandaFaccao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDiasAtraso()));
                                cln.alignment(Pos.CENTER);
                            }).build() /*Atraso (dd)*/);
                }).build() /*Status*/,
                FormTableColumnGroup.createGroup(group -> {
                    group.title("Demanda");
                    group.addColumn(
                            FormTableColumn.create(cln -> {
                                cln.title("Qtde");
                                cln.width(60);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdDemandaFaccao, VSdDemandaFaccao>, ObservableValue<VSdDemandaFaccao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtdePecas()));
                                cln.alignment(Pos.CENTER);
                                cln.format(param -> {
                                    return new TableCell<VSdDemandaFaccao, Integer>() {
                                        @Override
                                        protected void updateItem(Integer item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            if (item != null && !empty) {
                                                setText(String.valueOf(item));
                                            }
                                        }
                                    };
                                });
                            }).build() /*Qtde*/,
                            FormTableColumn.create(cln -> {
                                cln.title("Dias");
                                cln.width(50.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdDemandaFaccao, VSdDemandaFaccao>, ObservableValue<VSdDemandaFaccao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLeadDemanda()));
                                cln.alignment(Pos.CENTER);
                                cln.format(param -> {
                                    return new TableCell<VSdDemandaFaccao, BigDecimal>() {
                                        @Override
                                        protected void updateItem(BigDecimal item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            if (item != null && !empty) {
                                                setText(String.valueOf(item.setScale(0, RoundingMode.CEILING)));
                                            }
                                        }
                                    };
                                });
                            }).build() /*Dias*/,
                            FormTableColumn.create(cln -> {
                                cln.title("Minutos");
                                cln.width(80);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdDemandaFaccao, VSdDemandaFaccao>, ObservableValue<VSdDemandaFaccao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTempoDemanda()));
                                cln.alignment(Pos.CENTER);
                                cln.format(param -> {
                                    return new TableCell<VSdDemandaFaccao, BigDecimal>() {
                                        @Override
                                        protected void updateItem(BigDecimal item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            if (item != null && !empty) {
                                                setText(StringUtils.toDecimalFormat(item.doubleValue(), 1));
                                            }
                                        }
                                    };
                                });
                            }).build() /*Minutos*/,
                            FormTableColumn.create(cln -> {
                                cln.title("Pç/dia");
                                cln.width(50);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdDemandaFaccao, VSdDemandaFaccao>, ObservableValue<VSdDemandaFaccao>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                cln.alignment(Pos.CENTER);
                                cln.format(param -> {
                                    return new TableCell<VSdDemandaFaccao, VSdDemandaFaccao>() {
                                        @Override
                                        protected void updateItem(VSdDemandaFaccao item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            if (item != null && !empty) {
                                                setText(StringUtils.toDecimalFormat(new BigDecimal(item.getQtdePecas()).divide(item.getLeadDemanda(), 0, RoundingMode.CEILING).doubleValue(), 0));
                                            }
                                        }
                                    };
                                });
                            }).build() /*Pç/dia*/,
                            FormTableColumn.create(cln -> {
                                cln.title("Ult. Retorno");
                                cln.width(80.0);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdDemandaFaccao, VSdDemandaFaccao>, ObservableValue<VSdDemandaFaccao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getUltimoRetorno()));
                                cln.alignment(Pos.CENTER);
                                cln.format(param -> {
                                    return new TableCell<VSdDemandaFaccao, LocalDate>() {
                                        @Override
                                        protected void updateItem(LocalDate item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            if (item != null && !empty) {
                                                setText(StringUtils.toDateFormat(item));
                                            }
                                        }
                                    };
                                });
                            }).build() /*Ult. Retorno*/);
                }).build() /*Demanda*/,
                FormTableColumnGroup.createGroup(group -> {
                    group.title("Próximas");
                    group.addColumn(
                            FormTableColumn.create(cln -> {
                                cln.title("Qtde");
                                cln.width(70);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdDemandaFaccao, VSdDemandaFaccao>, ObservableValue<VSdDemandaFaccao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDemandaProxima()));
                                cln.alignment(Pos.CENTER);
                                cln.format(param -> {
                                    return new TableCell<VSdDemandaFaccao, VSdDemandaFaccao>() {
                                        @Override
                                        protected void updateItem(VSdDemandaFaccao item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            if (item != null && !empty) {
                                                setText(String.valueOf(item.getQtdePecas()));
                                            }
                                        }
                                    };
                                });
                            }).build() /*Qtde*/,
                            FormTableColumn.create(cln -> {
                                cln.title("Dias");
                                cln.width(50);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdDemandaFaccao, VSdDemandaFaccao>, ObservableValue<VSdDemandaFaccao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDemandaProxima()));
                                cln.alignment(Pos.CENTER);
                                cln.format(param -> {
                                    return new TableCell<VSdDemandaFaccao, VSdDemandaFaccao>() {
                                        @Override
                                        protected void updateItem(VSdDemandaFaccao item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            if (item != null && !empty) {
                                                setText(String.valueOf(item.getLeadDemanda().setScale(0, RoundingMode.CEILING)));
                                            }
                                        }
                                    };
                                });
                            }).build() /*Dias*/,
                            FormTableColumn.create(cln -> {
                                cln.title("Minutos");
                                cln.width(60);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdDemandaFaccao, VSdDemandaFaccao>, ObservableValue<VSdDemandaFaccao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDemandaProxima()));
                                cln.alignment(Pos.CENTER);
                                cln.format(param -> {
                                    return new TableCell<VSdDemandaFaccao, VSdDemandaFaccao>() {
                                        @Override
                                        protected void updateItem(VSdDemandaFaccao item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            if (item != null && !empty) {
                                                setText(StringUtils.toDecimalFormat(item.getTempoDemanda().doubleValue(), 1));
                                            }
                                        }
                                    };
                                });
                            }).build() /*Minutos*/,
                            FormTableColumn.create(cln -> {
                                cln.title("Pç/dia");
                                cln.width(50);
                                cln.value((Callback<TableColumn.CellDataFeatures<VSdDemandaFaccao, VSdDemandaFaccao>, ObservableValue<VSdDemandaFaccao>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDemandaProxima()));
                                cln.alignment(Pos.CENTER);
                                cln.format(param -> {
                                    return new TableCell<VSdDemandaFaccao, VSdDemandaFaccao>() {
                                        @Override
                                        protected void updateItem(VSdDemandaFaccao item, boolean empty) {
                                            super.updateItem(item, empty);
                                            setText(null);
                                            if (item != null && !empty) {
                                                setText(StringUtils.toDecimalFormat(new BigDecimal(item.getQtdePecas()).divide(item.getLeadDemanda(), 0, RoundingMode.CEILING).doubleValue(), 0));
                                            }
                                        }
                                    };
                                });
                            }).build() /*Pç/dia*/);
                }).build() /*Proxima*/
        );
        table.factoryRow(param -> {
            return new TableRow<VSdDemandaFaccao>() {
                @Override
                protected void updateItem(VSdDemandaFaccao item, boolean empty) {
                    super.updateItem(item, empty);
                    getStyleClass().removeAll("table-row-danger", "table-row-success");
                    if (item != null && !empty) {
                        if (item.isAtrasado())
                            getStyleClass().add("table-row-danger");
                    }
                }
            };
        });
        table.indices(
                table.indice("Em Atrasdo", "danger", "")
        );
        table.selectionModelItem((observable, oldValue, newValue) -> {
            if (newValue != null) {
                carregarCalendarioFaccoes((VSdDemandaFaccao) table.selectedItem());
            }
        });
    });
    private final FormTableView<SdFluxoAtualOf> tblOfsNaoIniciadas = FormTableView.create(SdFluxoAtualOf.class, table -> {
        table.withoutHeader();
        table.expanded();
        table.fontSize(11);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(40);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdFluxoAtualOf, SdFluxoAtualOf>, ObservableValue<SdFluxoAtualOf>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdFluxoAtualOf, SdFluxoAtualOf>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnProgramarOf = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CELULA, ImageUtils.IconSize._16));
                                btn.tooltip("Programar Fornecedor");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("primary");
                            });

                            @Override
                            protected void updateItem(SdFluxoAtualOf item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {

                                    btnProgramarOf.setOnAction(evt -> {
                                        programarOfFornecedor(item);
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnProgramarOf);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build() /*Ações*/,
                FormTableColumn.create(cln -> {
                    cln.title("OF");
                    cln.width(55);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdFluxoAtualOf, SdFluxoAtualOf>, ObservableValue<SdFluxoAtualOf>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getNumero().getNumero()));
                    cln.alignment(Pos.CENTER);
                }).build() /*OF*/,
                FormTableColumn.create(cln -> {
                    cln.title("Lote");
                    cln.width(45);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdFluxoAtualOf, SdFluxoAtualOf>, ObservableValue<SdFluxoAtualOf>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getNumero().getPeriodo()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Lote*/,
                FormTableColumn.create(cln -> {
                    cln.title("Produto");
                    cln.width(240);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdFluxoAtualOf, SdFluxoAtualOf>, ObservableValue<SdFluxoAtualOf>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.order((o1, o2) -> ((SdFluxoAtualOf) o1).getId().getNumero().getProduto().getCodigo().compareTo(((SdFluxoAtualOf) o2).getId().getNumero().getProduto().getCodigo()));
                    cln.format(param -> {
                        return new TableCell<SdFluxoAtualOf, SdFluxoAtualOf>() {
                            @Override
                            protected void updateItem(SdFluxoAtualOf item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(item.getId().getNumero().getProduto().toString());
                                    setContextMenu(FormContextMenu.create(menu -> {
                                        menu.addItem(itemMenu -> {
                                            itemMenu.setText("Ver Entregas Produto");
                                            itemMenu.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.CALENDARIO_PRODUTO, ImageUtils.IconSize._16));
                                            itemMenu.setOnAction(evt -> {
                                                carregarDatasEntregaProduto(item);
                                            });
                                        });
                                        menu.addItem(itemMenu -> {
                                            itemMenu.setText("Ver Entregas OF");
                                            itemMenu.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.GESTAO_ORDEM_COMPRA, ImageUtils.IconSize._16));
                                            itemMenu.setOnAction(evt -> {
                                                carregarDatasEntregaOf(item);
                                            });
                                        });
                                    }));
                                }
                            }
                        };
                    });
                }).build() /*Produto*/,
                FormTableColumn.create(cln -> {
                    cln.title("Coleção");
                    cln.width(160);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdFluxoAtualOf, SdFluxoAtualOf>, ObservableValue<SdFluxoAtualOf>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getNumero().getProduto().getColecao()));
                    cln.order((o1, o2) -> ((SdFluxoAtualOf) o1).getId().getNumero().getProduto().getColecao().getCodigo().compareTo(((SdFluxoAtualOf) o2).getId().getNumero().getProduto().getColecao().getCodigo()));
                }).build() /*Coleção*/,
                FormTableColumn.create(cln -> {
                    cln.title("Família");
                    cln.width(130);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdFluxoAtualOf, SdFluxoAtualOf>, ObservableValue<SdFluxoAtualOf>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getNumero().getProduto().getFamilia()));
                    cln.order((o1, o2) -> ((Familia) o1).getDescricao().compareTo(((Familia) o2).getDescricao()));
                }).build() /*Família*/,
                FormTableColumn.create(cln -> {
                    cln.title("Qtde");
                    cln.width(50);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdFluxoAtualOf, SdFluxoAtualOf>, ObservableValue<SdFluxoAtualOf>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtdeOf()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Qtde*/,
                FormTableColumn.create(cln -> {
                    cln.title("Dt. Envio");
                    cln.width(75);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdFluxoAtualOf, SdFluxoAtualOf>, ObservableValue<SdFluxoAtualOf>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getDtEnvio()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<SdFluxoAtualOf, LocalDate>() {
                            @Override
                            protected void updateItem(LocalDate item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDateFormat(item));
                                }
                            }
                        };
                    });
                }).build() /*Dt. Envio*/,
                FormTableColumn.create(cln -> {
                    cln.title("Dt. Início");
                    cln.width(75);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdFluxoAtualOf, SdFluxoAtualOf>, ObservableValue<SdFluxoAtualOf>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getDtInicio()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<SdFluxoAtualOf, LocalDate>() {
                            @Override
                            protected void updateItem(LocalDate item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDateFormat(item));
                                }
                            }
                        };
                    });
                }).build() /*Dt. Início*/,
                FormTableColumn.create(cln -> {
                    cln.title("Dias");
                    cln.width(40);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdFluxoAtualOf, SdFluxoAtualOf>, ObservableValue<SdFluxoAtualOf>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLeadSetor()));
                    cln.alignment(Pos.CENTER);
                }).build() /*Dias*/,
                FormTableColumn.create(cln -> {
                    cln.title("Tempo");
                    cln.width(70);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdFluxoAtualOf, SdFluxoAtualOf>, ObservableValue<SdFluxoAtualOf>>) param -> new ReadOnlyObjectWrapper(param.getValue().getTempoOf()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<SdFluxoAtualOf, BigDecimal>() {
                            @Override
                            protected void updateItem(BigDecimal item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDecimalFormat(item.doubleValue(), 4));
                                }
                            }
                        };
                    });
                }).build() /*Tempo*/,
                FormTableColumn.create(cln -> {
                    cln.title("Facção Programada");
                    cln.width(220);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdFluxoAtualOf, SdFluxoAtualOf>, ObservableValue<SdFluxoAtualOf>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getFaccao()));
                }).build() /*Facção Programada*/,
                FormTableColumn.create(cln -> {
                    cln.title("Setor Atual");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdFluxoAtualOf, SdFluxoAtualOf>, ObservableValue<SdFluxoAtualOf>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.order(Comparator.comparing(o -> ((SdFluxoAtualOf) o).getSetorEmProducao().getDescricao()));
                    cln.format(param -> {
                        return new TableCell<SdFluxoAtualOf, SdFluxoAtualOf>() {
                            @Override
                            protected void updateItem(SdFluxoAtualOf item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    if (item.getSetorEmProducao() != null) {
                                        setText(item.getSetorEmProducao().toString());
                                        setContextMenu(FormContextMenu.create(menu -> {
                                            menu.addItem(itemMenu -> {
                                                itemMenu.setText("Abrir Fornecedor");
                                                itemMenu.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.ENTIDADE, ImageUtils.IconSize._16));
                                                itemMenu.setOnAction(evt -> fragmentFornecedor(item));
                                            });
                                        }));
                                    }
                                }
                            }
                        };
                    });
                }).build() /*Setor Atual*/,
                FormTableColumn.create(cln -> {
                    cln.title("Dt. Ret. Prod.");
                    cln.width(85);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdFluxoAtualOf, SdFluxoAtualOf>, ObservableValue<SdFluxoAtualOf>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDtRetornoProducao()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<SdFluxoAtualOf, LocalDate>() {
                            @Override
                            protected void updateItem(LocalDate item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                if (item != null && !empty) {
                                    setText(StringUtils.toDateFormat(item));
                                }
                            }
                        };
                    });
                }).build() /*Dt. Ret. Prod.*/,
                FormTableColumn.create(cln -> {
                    cln.title("PS");
                    cln.width(35);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdFluxoAtualOf, SdFluxoAtualOf>, ObservableValue<SdFluxoAtualOf>>) param -> new ReadOnlyObjectWrapper(param.getValue().getProximoSetor()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<SdFluxoAtualOf, Integer>() {
                            @Override
                            protected void updateItem(Integer item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    if (item <= Integer.parseInt(indiceProximoSetor.getValor())) {
                                        setGraphic(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._16));
                                        setTooltip(new Tooltip("Próximo Setor"));
                                    }
                                }
                            }
                        };
                    });
                }).build() /*PS*/
        );
        table.factoryRow(param -> {
            return new TableRow<SdFluxoAtualOf>() {
                @Override
                protected void updateItem(SdFluxoAtualOf item, boolean empty) {
                    super.updateItem(item, empty);
                    getStyleClass().removeAll("table-row-warning");
                    if (item != null && !empty)
                        if (item.isPrioridade())
                            getStyleClass().add("table-row-warning");

                }
            };
        });
        table.indices(
                table.indice("Com Prioridade", "warning", "")
        );
    });
    private final FormBox boxCalendarioOfs = FormBox.create(boxCalendario -> {
        boxCalendario.horizontal();
        boxCalendario.expanded();
    });
    private final FormBox boxCalendarioEntregaOfs = FormBox.create(boxCalendario -> {
        boxCalendario.horizontal();
        boxCalendario.expanded();
    });
    // </editor-fold>

    public ControleProducaoView() {
        super("Controle de Produção", ImageUtils.getImage(ImageUtils.Icon.CONTROLE_PRODUCAO));
        initProgramacao();

        if (controladorLogado != null) {
            controladorLogado.refresh();
            if (controladorLogado.getCodigoFuncao() == 121)
                super.title.setText("Controle de Produção - Controlador: " + controladorLogado.toString());
        }
    }

    // ------------------- PROGRAMAÇÃO ---------------------
    private void initProgramacao() {
        tabProgramacao.getChildren().add(FormBoxPane.create(content -> {
            content.expanded();
            content.center(FormBox.create(center -> {
                center.horizontal();
                center.expanded();
                center.add(container -> {
                    container.vertical();;
                    container.add(FormTitledPane.create(filter -> {
                        filter.filter();
                        filter.width(380.0);
                        filter.add(filterContent -> {
                            filterContent.vertical();
                            filterContent.add(fields -> fields.addHorizontal(fieldFilterSetor.build()));
                            filterContent.add(fields -> fields.addHorizontal(fieldFilterFaccoes.build(), fieldFilterFamilia.build()));
                            filterContent.add(fields -> fields.addHorizontal(fieldFilterControlador.build(), fieldFilterProduto.build()));
                            filterContent.add(fields -> fields.addHorizontal(fieldFilterColecao.build(), fieldFilterVerPlanejamento.build()));
                            filterContent.add(fields -> fields.addHorizontal(fieldFilterDataEntrega.build(), fieldFilterProximoSetor.build()));
                            filterContent.add(fields -> fields.addHorizontal(fieldFilterTipoVisao.build()));
                        });
                        filter.find.setOnAction(evt -> {
                            filterProgramacao();
                        });
                        filter.clean.setOnAction(evt -> {
                            clearFilterProgramacao();
                        });
                    }));
                    container.add(FormTitledPane.create(pane -> {
                        pane.title("Ações da Programação");
                        pane.addStyle("primary");
                        pane.collapse(true);
                        pane.setCollapsible(false);
                        pane.add(FormBox.create(boxTools -> {
                            boxTools.flutuante();
                            boxTools.expanded();
                            boxTools.add(FormButton.create(btn -> {
                                btn.title("Ajustes");
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.AJUSTES, ImageUtils.IconSize._32));
                                btn.addStyle("info");
                                btn.contentDisplay(ContentDisplay.TOP);
                                btn.setAction(evt -> ajustesDistribuicaoDemanda());
                            }));
                            boxTools.add(FormButton.create(btn -> {
                                btn.title("Distribuir Demanda");
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.RODAR_ACAO, ImageUtils.IconSize._32));
                                btn.addStyle("primary");
                                btn.contentDisplay(ContentDisplay.TOP);
                                btn.setAction(evt -> recalcularDemandaOfs());
                            }));
                            boxTools.add(new Separator(Orientation.VERTICAL));
                            boxTools.add(FormButton.create(btn -> {
                                btn.title("Firmar Demanda");
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._32));
                                btn.addStyle("success");
                                btn.contentDisplay(ContentDisplay.TOP);
                                btn.setAction(event -> firmarProgramacao());
                            }));
                            boxTools.add(new Separator(Orientation.VERTICAL));
                            boxTools.add(FormButton.create(btn -> {
                                btn.title("Imprimir");
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.IMPRIMIR, ImageUtils.IconSize._32));
                                btn.addStyle("info");
                                btn.contentDisplay(ContentDisplay.TOP);
                                btn.setAction(event -> imprimirProgramacao());
                            }));
                        }));
                    }));
                });
                center.add(container -> {
                    container.vertical();
                    container.expanded();
                    container.add(FormTitledPane.create(pane -> {
                        pane.title("Demandas dos Recursos");
                        pane.addStyle("info");
                        pane.expanded();
                        pane.setMinHeight(180.0);
                        pane.collapse(true);
                        pane.setCollapsible(false);
                        pane.add(tblDemandasFaccao.build());
                    }));
                    container.add(FormTitledPane.create(pane -> {
                        pane.title("OFs em Produção");
                        pane.addStyle("success");
                        pane.expanded();
                        pane.collapse(false);
                        pane.add(tblOfsNaoIniciadas.build());
                    }));
                });
            }));
            content.bottom(FormTitledPane.create(boxCalendario -> {
                boxCalendario.title("Calendário");
                boxCalendario.collapsabled.bindBidirectional(colapseDiario);
                boxCalendario.add(FormTabPane.create(tabsCalendarios -> {
                    tabsCalendarios.side(Side.BOTTOM);
                    tabsCalendarios.setPrefHeight(420.0);
                    tabsCalendarios.addTab(FormTab.create(tabCalendario -> {
                        tabCalendario.title("Retornos");
                        tabCalendario.add(FormBox.create(boxTab -> {
                            boxTab.vertical();
                            boxTab.horizontalScroll();
                            boxTab.expanded();
                            boxTab.add(boxCalendarioEntregaOfs);
                        }));
                    }));
                    tabsCalendarios.addTab(FormTab.create(tabCalendario -> {
                        tabCalendario.title("Diário de Produção");
                        tabCalendario.add(FormBox.create(boxTab -> {
                            boxTab.vertical();
                            boxTab.horizontalScroll();
                            boxTab.expanded();
                            boxTab.add(boxCalendarioOfs);
                        }));
                    }));
                }));
            }));
        }));
    }

    private void filterProgramacao() {
        try {
            fieldFilterSetor.validate();
        } catch (FormValidationException ex) {
            MessageBox.create(message -> {
                message.message("Você precisa selecionar o setor em análise.");
                message.type(MessageBox.TypeMessageBox.FORM_VALIDATION);
                message.showAndWait();
            });
            return;
        }

        //--------- Limpeza das Listas -----------
        JPAUtils.clearEntitys(tblOfsNaoIniciadas.items.get());
        tblOfsNaoIniciadas.clear();
        JPAUtils.clearEntitys(tblDemandasFaccao.items.get());
        tblDemandasFaccao.clear();
        // -------- Limpeza Calendários ----------
        boxCalendarioOfs.clear();
        boxCalendarioEntregaOfs.clear();

        String grupoSetor = fieldFilterSetor.value.get().getCodigo();
        Object[] faccoes = fieldFilterFaccoes.objectValues.stream().map(ent -> ent.getCodcli()).toArray();
        Object[] familias = fieldFilterFamilia.objectValues.stream().map(fam -> fam.getCodigo()).toArray();
        Object[] controladores = fieldFilterControlador.objectValues.stream().map(con -> con.getCodigo()).toArray();
        Object[] produtos = fieldFilterProduto.objectValues.stream().map(prd -> prd.getCodigo()).toArray();
        Object[] colecoes = fieldFilterColecao.objectValues.stream().map(col -> col.getCodigo()).toArray();
        Boolean verPlanejamento = fieldFilterVerPlanejamento.value.getValue();
        Boolean proximoSetor = fieldFilterProximoSetor.value.getValue();
        LocalDate dataEntrega = fieldFilterDataEntrega.value.get();
        procurarDemandas(faccoes, grupoSetor, familias, controladores, verPlanejamento, proximoSetor, produtos, dataEntrega, colecoes);
    }

    private void clearFilterProgramacao() {
        // -------- Limpeza dos Fields ----------
        fieldFilterFaccoes.clear();
        fieldFilterSetor.clear();
        fieldFilterFamilia.clear();
        fieldFilterControlador.clear();
        fieldFilterVerPlanejamento.value.set(false);
        fieldFilterProximoSetor.value.set(false);
        fieldFilterProduto.clear();
        fieldFilterColecao.clear();
        fieldFilterDataEntrega.value.set(null);
        //--------- Limpeza das Listas -----------
        JPAUtils.clearEntitys(tblOfsNaoIniciadas.items.get());
        tblOfsNaoIniciadas.clear();
        JPAUtils.clearEntitys(tblDemandasFaccao.items.get());
        tblDemandasFaccao.clear();
        // -------- Limpeza Calendários ----------
        boxCalendarioOfs.clear();
        boxCalendarioEntregaOfs.clear();
    }

    private void criarCalendarioRecurso(List<Diario> diarioRecurso) {
        // limpeza dos dados em tela do diário do recurso
        boxCalendarioOfs.clear();
        boxCalendarioEntregaOfs.clear();
        AtomicReference<String> tipoVisao = new AtomicReference<>(fieldFilterTipoVisao.value.get());

        // for no diário do recurso para exibição dos dados
        // ordenando o diário pelo dia
        diarioRecurso.stream().filter(dia -> dia.getData().isDiaUtil())
                .sorted((o1, o2) -> o1.getData().getData().compareTo(o2.getData().getData()))
                .forEachOrdered(dia -> {
                    final FormTableView<SdFluxoAtualOf> tblCalendario = FormTableView.create(SdFluxoAtualOf.class, table -> {
                        table.withoutHeader();
                        table.height(190.0);
                        table.items.set(FXCollections.observableList(dia.getOfsDia()));
                        table.columns(
                                FormTableColumn.create(cln -> {
                                    cln.title("OF");
                                    cln.width(65);
                                    cln.value((Callback<TableColumn.CellDataFeatures<SdFluxoAtualOf, SdFluxoAtualOf>, ObservableValue<SdFluxoAtualOf>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                    cln.alignment(Pos.CENTER);
                                    cln.format(param -> {
                                        return new TableCell<SdFluxoAtualOf, SdFluxoAtualOf>() {
                                            @Override
                                            protected void updateItem(SdFluxoAtualOf item, boolean empty) {
                                                super.updateItem(item, empty);
                                                setText(null);
                                                setGraphic(null);
                                                if (item != null && !empty) {
                                                    if (item.isAtrasado()) {
                                                        setGraphic(ImageUtils.getIcon(ImageUtils.Icon.ALERT_ERROR, ImageUtils.IconSize._16));
                                                    } else if (item.isPrioridade()) {
                                                        setGraphic(ImageUtils.getIcon(ImageUtils.Icon.FAVORITO, ImageUtils.IconSize._16));
                                                    }
                                                    setText(item.getId().getNumero().getNumero());

                                                    PopOver pop = new PopOver(FormFieldText.create(field -> {
                                                        field.title("Prod");
                                                        field.editable.set(false);
                                                        field.value.set(item.getId().getNumero().getProduto().toString());
                                                        field.addStyle("xs");
                                                        field.width(150.0);
                                                    }).build());
                                                    pop.setArrowLocation(PopOver.ArrowLocation.BOTTOM_LEFT);
                                                    pop.setHeight(30.0);
                                                    setOnMouseEntered(evt -> pop.show(this));
                                                    setOnMouseExited(evt -> pop.hide());
                                                }
                                            }
                                        };
                                    });
                                }).build() /*OF*/,
                                FormTableColumn.create(cln -> {
                                    cln.title("Lote");
                                    cln.width(40);
                                    cln.value((Callback<TableColumn.CellDataFeatures<SdFluxoAtualOf, SdFluxoAtualOf>, ObservableValue<SdFluxoAtualOf>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getNumero().getPeriodo()));
                                    cln.alignment(Pos.CENTER);
                                }).build() /*Lote*/,
                                FormTableColumn.create(cln -> {
                                    cln.title("Qtde");
                                    cln.width(45);
                                    cln.value((Callback<TableColumn.CellDataFeatures<SdFluxoAtualOf, SdFluxoAtualOf>, ObservableValue<SdFluxoAtualOf>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                    cln.alignment(Pos.CENTER);
                                    cln.format(param -> {
                                        return new TableCell<SdFluxoAtualOf, SdFluxoAtualOf>() {
                                            @Override
                                            protected void updateItem(SdFluxoAtualOf item, boolean empty) {
                                                super.updateItem(item, empty);
                                                setText(null);
                                                if (item != null && !empty) {
                                                    item.getOcupacaoOf().stream().filter(dem -> dem.getId().getDtDemanda().isEqual(dia.getData().getData()))
                                                            .findFirst().ifPresent(dem -> {
                                                                setText(StringUtils.toIntegerFormat(dem.getQtde().intValue()));
                                                            });
                                                }
                                            }
                                        };
                                    });
                                }).build() /*Qtde*/,
                                FormTableColumn.create(cln -> {
                                    cln.title("Tempo");
                                    cln.width(60);
                                    cln.value((Callback<TableColumn.CellDataFeatures<SdFluxoAtualOf, SdFluxoAtualOf>, ObservableValue<SdFluxoAtualOf>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                    cln.alignment(Pos.CENTER);
                                    cln.format(param -> {
                                        return new TableCell<SdFluxoAtualOf, SdFluxoAtualOf>() {
                                            @Override
                                            protected void updateItem(SdFluxoAtualOf item, boolean empty) {
                                                super.updateItem(item, empty);
                                                setText(null);
                                                if (item != null && !empty) {
                                                    item.getOcupacaoOf().stream().filter(dem -> dem.getId().getDtDemanda().isEqual(dia.getData().getData()))
                                                            .findFirst().ifPresent(dem -> {
                                                                setText(StringUtils.toDecimalFormat(dem.getTempo()));
                                                            });
                                                }
                                            }
                                        };
                                    });
                                }).build() /*Tempo*/
                        );
                        table.factoryRow(param -> {
                            return new TableRow<SdFluxoAtualOf>() {
                                @Override
                                protected void updateItem(SdFluxoAtualOf item, boolean empty) {
                                    super.updateItem(item, empty);
                                    getStyleClass().removeAll("table-row-danger", "table-row-success", "table-row-warning", "table-row-primary");
                                    if (item != null && !empty) {
                                        setOnMouseClicked(evt -> {
                                            if (evt.getClickCount() > 1)
                                                fragmentDiarioOfs(item, this);
                                        });
                                        if (item.isFinalizado())
                                            getStyleClass().add("table-row-success");
                                        else if (item.getDtRetornoProg().isBefore(LocalDate.now()))
                                            getStyleClass().add("table-row-danger");
                                        else if (item.isComProgramacaoRetorno() && !item.getId().isSetorAtual())
                                            getStyleClass().add("table-row-primary");
                                    }
                                }
                            };
                        });
                        table.indices(
                                table.indice("Atrasado", "danger", ""),
                                table.indice("Com Prog.", "primary", ""),
                                table.indice("Finalizado", "success", "")
                        );
                    });
                    final FormTableView<SdFluxoAtualOf> tblEntregasDoDia = FormTableView.create(SdFluxoAtualOf.class, table -> {
                        table.withoutHeader();
                        table.height(240.0);
                        table.items.set(FXCollections.observableList(dia.getOfsEntrega()));
                        table.columns(
                                FormTableColumn.create(cln -> {
                                    cln.title("OF");
                                    cln.width(65);
                                    cln.value((Callback<TableColumn.CellDataFeatures<SdFluxoAtualOf, SdFluxoAtualOf>, ObservableValue<SdFluxoAtualOf>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                                    cln.alignment(Pos.CENTER);
                                    cln.format(param -> {
                                        return new TableCell<SdFluxoAtualOf, SdFluxoAtualOf>() {
                                            @Override
                                            protected void updateItem(SdFluxoAtualOf item, boolean empty) {
                                                super.updateItem(item, empty);
                                                setText(null);
                                                setGraphic(null);
                                                if (item != null && !empty) {
                                                    if (item.isAtrasado()) {
                                                        setGraphic(ImageUtils.getIcon(ImageUtils.Icon.ALERT_ERROR, ImageUtils.IconSize._16));
                                                    } else if (item.isPrioridade()) {
                                                        setGraphic(ImageUtils.getIcon(ImageUtils.Icon.FAVORITO, ImageUtils.IconSize._16));
                                                    }
                                                    setText(item.getId().getNumero().getNumero());

                                                    PopOver pop = new PopOver(FormFieldText.create(field -> {
                                                        field.title("Prod");
                                                        field.editable.set(false);
                                                        field.value.set(item.getId().getNumero().getProduto().toString());
                                                        field.addStyle("xs");
                                                        field.width(150.0);
                                                    }).build());
                                                    pop.setArrowLocation(PopOver.ArrowLocation.BOTTOM_LEFT);
                                                    pop.setHeight(30.0);
                                                    setOnMouseEntered(evt -> pop.show(this));
                                                    setOnMouseExited(evt -> pop.hide());
                                                }
                                            }
                                        };
                                    });
                                }).build() /*OF*/,
                                FormTableColumn.create(cln -> {
                                    cln.title("Lote");
                                    cln.width(40);
                                    cln.value((Callback<TableColumn.CellDataFeatures<SdFluxoAtualOf, SdFluxoAtualOf>, ObservableValue<SdFluxoAtualOf>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getNumero().getPeriodo()));
                                    cln.alignment(Pos.CENTER);
                                }).build() /*Lote*/,
                                FormTableColumn.create(cln -> {
                                    cln.title("Qtde");
                                    cln.width(45);
                                    cln.value((Callback<TableColumn.CellDataFeatures<SdFluxoAtualOf, SdFluxoAtualOf>, ObservableValue<SdFluxoAtualOf>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtdeOf()));
                                    cln.alignment(Pos.CENTER);
                                }).build() /*Qtde*/,
                                FormTableColumn.create(cln -> {
                                    cln.title("Envio");
                                    cln.width(60);
                                    cln.value((Callback<TableColumn.CellDataFeatures<SdFluxoAtualOf, SdFluxoAtualOf>, ObservableValue<SdFluxoAtualOf>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getDtEnvio()));
                                    cln.alignment(Pos.CENTER);
                                    cln.format(param -> {
                                        return new TableCell<SdFluxoAtualOf, LocalDate>() {
                                            @Override
                                            protected void updateItem(LocalDate item, boolean empty) {
                                                super.updateItem(item, empty);
                                                setText(null);
                                                if (item != null && !empty) {
                                                    setText(StringUtils.toShortDateFormat(item));
                                                }
                                            }
                                        };
                                    });
                                }).build() /*Envio*/
                        );
                        table.factoryRow(param -> {
                            return new TableRow<SdFluxoAtualOf>() {
                                @Override
                                protected void updateItem(SdFluxoAtualOf item, boolean empty) {
                                    super.updateItem(item, empty);
                                    getStyleClass().removeAll("table-row-danger", "table-row-success", "table-row-primary");
                                    if (item != null && !empty) {
                                        setOnMouseClicked(evt -> {
                                            if (evt.getClickCount() > 1)
                                                fragmentEntregaOfs(item, this);
                                        });
                                        setContextMenu(menuTblEntrega(item, table));
                                        if (item.isMarcadoFinalizado())
                                            getStyleClass().add("table-row-success");
                                        else if (item.getDtRetornoProg().isBefore(LocalDate.now()))
                                            getStyleClass().add("table-row-danger");
                                        else if (item.isComProgramacaoRetorno() && !item.getId().isSetorAtual())
                                            getStyleClass().add("table-row-primary");
                                    }
                                }
                            };
                        });
                        table.indices(
                                table.indice("Atrasado", "danger", ""),
                                table.indice("Com Prog.", "primary", ""),
                                table.indice("Finalizado", "success", "")
                        );
                    });
                    boxCalendarioOfs.add(FormBox.create(boxDia -> {
                        boxDia.vertical();
                        boxDia.width(240.0);
                        boxDia.title(StringUtils.toShortDateFormat(dia.getData().getData()) + (tipoVisao.get().equals("D") ? "" : " - " + StringUtils.toShortDateFormat(dia.getData().getData().plusDays(4))));
                        if (dia.getData().getData().isEqual(LocalDate.now()))
                            boxDia.addStyle("info");
                        boxDia.add(fields -> fields.addHorizontal(
                                FormFieldText.create(field -> field.withoutTitle().addStyle("xs").addStyle("primary").expanded().editable(false).label("Capac.:")
                                        .value.set(StringUtils.toDecimalFormat(dia.getCapacidade()))).build(),
                                FormFieldText.create(field -> field.withoutTitle().addStyle("xs").addStyle("success").expanded().editable(false).label("Ocupa.:")
                                        .value.set(StringUtils.toDecimalFormat(dia.getOcupacao()))).build()
                        ));
                        boxDia.add(FormFieldText.create(field -> field.withoutTitle().addStyle("xs").addStyle("warning").editable(false).label("Percentual Ocupação:")
                                .value.set(StringUtils.toPercentualFormat(dia.getOcupacao().divide(dia.getCapacidade(), 5, RoundingMode.HALF_UP).doubleValue(), 2))).build());
                        boxDia.add(tblCalendario.build());
                        boxDia.add(fields -> fields.addHorizontal(FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.label(ImageUtils.getIcon(ImageUtils.Icon.ALERT_ERROR, ImageUtils.IconSize._16));
                                    field.value.set("Prog. Original Atrasada");
                                    field.editable.set(false);
                                    field.addStyle("xs");
                                    field.expanded();
                                }).build(),
                                FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.label(ImageUtils.getIcon(ImageUtils.Icon.FAVORITO, ImageUtils.IconSize._16));
                                    field.value.set("OF Prioridade");
                                    field.editable.set(false);
                                    field.addStyle("xs");
                                    field.width(100.0);
                                }).build()));
                    }));
                    if (dia.getOfsEntrega().size() > 0)
                        boxCalendarioEntregaOfs.add(FormBox.create(boxDia -> {
                            boxDia.vertical();
                            boxDia.width(250.0);
                            boxDia.title(StringUtils.toShortDateFormat(dia.getData().getData()) + (tipoVisao.get().equals("D") ? "" : " - " + StringUtils.toShortDateFormat(dia.getData().getData().plusDays(4))));
                            if (dia.getData().getData().isEqual(LocalDate.now()))
                                boxDia.addStyle("info");
                            boxDia.add(tblEntregasDoDia.build());
                            boxDia.add(fields -> fields.addHorizontal(FormFieldText.create(field -> {
                                        field.withoutTitle();
                                        field.label(ImageUtils.getIcon(ImageUtils.Icon.ALERT_ERROR, ImageUtils.IconSize._16));
                                        field.value.set("Prog. Original Atrasada");
                                        field.editable.set(false);
                                        field.addStyle("xs");
                                        field.expanded();
                                    }).build(),
                                    FormFieldText.create(field -> {
                                        field.withoutTitle();
                                        field.label(ImageUtils.getIcon(ImageUtils.Icon.FAVORITO, ImageUtils.IconSize._16));
                                        field.value.set("OF Prioridade");
                                        field.editable.set(false);
                                        field.addStyle("xs");
                                        field.width(100.0);
                                    }).build()));
                        }));
                });
        colapseDiario.set(true);
    }

    private void carregarDatasEntregaProduto(SdFluxoAtualOf fluxoOf) {
        try {
            List<String> datasEntrega = getDatasEntrega("", fluxoOf.getId().getNumero().getProduto().getCodigo());
            new Fragment().show(fragment -> {
                fragment.title("Entregas Produto");
                fragment.size(200.0, 300.0);
                fragment.withoutCancelButton();
                fragment.content(FormBox.create(content -> {
                    content.vertical();
                    content.expanded();
                    content.add(FormListView.create(list -> {
                        list.withoutHeader();
                        list.expanded();
                        list.setItems(FXCollections.observableList(datasEntrega));
                    }).build());
                }));
            });
        } catch (SQLException e) {
            e.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(e);
                message.showAndWait();
            });
        }
    }

    private void carregarDatasEntregaOf(SdFluxoAtualOf fluxoOf) {
        try {
            List<String> datasEntrega = getDatasEntrega(fluxoOf.getId().getNumero().getNumero(), "");
            new Fragment().show(fragment -> {
                fragment.title("Entregas OF");
                fragment.size(200.0, 300.0);
                fragment.withoutCancelButton();
                fragment.content(FormBox.create(content -> {
                    content.vertical();
                    content.expanded();
                    content.add(FormListView.create(list -> {
                        list.withoutHeader();
                        list.expanded();
                        list.setItems(FXCollections.observableList(datasEntrega));
                    }).build());
                }));
            });
        } catch (SQLException e) {
            e.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(e);
                message.showAndWait();
            });
        }
    }

    // ----------------- EVENTOS ---------------------------
    private void firmarProgramacao() {
        VSdDemandaFaccao demandaFaccao = tblDemandasFaccao.selectedItem();
        if (demandaFaccao == null) {
            MessageBox.create(message -> {
                message.message("Você precisa primeiro selecionar uma facção/setor para firmar a programação.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }

        AtomicReference<Exception> exRwo = new AtomicReference<>();
        new RunAsyncWithOverlay(this).exec(task -> {
            try {
                diario.get().stream().filter(dia -> dia.getOfsDia().size() > 0)
                        .forEach(dia -> {
                            dia.getOfsDia().forEach(of -> {
                                try {
                                    SdProgFaccao progFaccao = getProgramacaoFaccao(of.getId().getNumero().getNumero(), dia.getFaccao().getCodcli(), dia.getSetor().getCodigo());

                                    if (progFaccao == null) {
                                        progFaccao = new SdProgFaccao(dia.getFaccao(), dia.getSetor(), of.getId().getNumero().getNumero());
                                        progFaccao.setDtProgramacaoOriginal(of.getDtRetornoProg());
                                    }
                                    progFaccao.setTipo("A");
                                    progFaccao.setDtInicioProd(of.getDtInicioProg());
                                    progFaccao.setDtRetornoProg(of.getDtRetornoProg());

                                    // persistencias de banco
                                    progFaccao = saveProgFaccaoOf(progFaccao);
                                    SysLogger.addSysDelizLog("Controle Produção", TipoAcao.EDITAR, progFaccao.getId().getNumero(), "Firmando programação: " + progFaccao.toLog());

                                    of.getOcupacaoOf().stream().filter(demanda -> demanda.getId().getDtDemanda().isEqual(dia.getData().getData())).findFirst().ifPresent(this::saveDemandaOf);
                                    updateMovimentoFaccao(progFaccao.getDtRetornoProg(), progFaccao.getId().getNumero(), progFaccao.getId().getFaccao().getCodcli(), progFaccao.getId().getSetor().getCodigo());
                                    SysLogger.addSysDelizLog("Controle Produção", TipoAcao.EDITAR, progFaccao.getId().getNumero(),
                                            "Alterado data de retorno para " + progFaccao.getDtRetornoProg() + " no setor " + progFaccao.getId().getSetor().getCodigo() + " facção " + progFaccao.getId().getFaccao().getCodcli());
                                } catch (SQLException exp) {
                                    throw new BreakException(exp);
                                }
                            });
                        });
            } catch (BreakException exp) {
                exRwo.set(exp);
                return ReturnAsync.EXCEPTION.value;
            }
            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                MessageBox.create(message -> {
                    message.message("Firmado calendário da facção/setor.");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            } else if (taskReturn.equals(ReturnAsync.EXCEPTION.value)) {
                ExceptionBox.build(m -> {
                    m.exception(exRwo.get());
                    m.showAndWait();
                });
            }
        });

    }

    private void ajustesDistribuicaoDemanda() {
        new Fragment().show(fragment -> {
            fragment.title("Ajustes");
            fragment.size(620.0, 320.0);
            fragment.box.getChildren().add(FormBox.create(content -> {
                content.horizontal();
                content.height(250.0);
                content.horizontalScroll();
                pesosDistDemanda.forEach(attr -> {
                    content.add(FormBox.create(boxField -> {
                        boxField.vertical();
                        boxField.alignment(Pos.CENTER);
                        boxField.add(FormFieldSlider.create(slider -> {
                            slider.title(attr.getDescricao());
                            slider.setPrefHeight(200.0);
                            slider.value.bindBidirectional(attr.pesoProperty());
                            slider.setOrientation(Orientation.VERTICAL);
                            slider.showTickLabels(true);
                            slider.tickUnit(50.);
                            slider.integerIncrement(1);
                        }));
                        boxField.add(FormFieldComboBox.create(String.class, field -> {
                            field.withoutTitle();
                            field.items(FXCollections.observableList(Arrays.asList("asc", "desc")));
                            field.value.bindBidirectional(attr.tipoProperty());
                            field.width(60.0);
                            field.addStyle("xs");
                        }).build());
                    }));
                });
            }));
            fragment.buttonsBox.getChildren().add(FormButton.create(btn -> {
                btn.title("Salvar");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                btn.addStyle("success");
                btn.setAction(evt -> {
                    pesosDistDemanda.forEach(this::saveAjuste);
                    fragment.close();
                    MessageBox.create(message -> {
                        message.message("Ajustes salvos com sucesso.");
                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                        message.position(Pos.TOP_RIGHT);
                        message.notification();
                    });
                });
            }));
        });
    }

    private void recalcularDemandaOfs() {
        // get da demanda da facção seleciona
        VSdDemandaFaccao recursoSelecionado = tblDemandasFaccao.selectedItem();
        if (recursoSelecionado == null) {
            MessageBox.create(message -> {
                message.message("É necessário você selecionar uma facção/setor ao lado para executar a análise.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return;
        }

        AtomicReference<Exception> exRwo = new AtomicReference<>();
        new RunAsyncWithOverlay(this).exec(task -> {
            // transformando a demanda seleciona em recurso
            List<SdRecursoProducao> recursoFaccaoSetor = (List<SdRecursoProducao>) new FluentDao().selectFrom(SdRecursoProducao.class)
                    .where(eb -> eb.equal("setorExcia.codigo", recursoSelecionado.getId().getSetor().getSdgrupo())
                            .equal("fornecedor.codcli", recursoSelecionado.getId().getFaccao().getCodcli()))
                    .resultList();
            recursoFaccaoSetor.forEach(JPAUtils::refresh);

            // get da capacidade média (coleção e turnos) diária do recurso
            BigDecimal capacidadeRecurso = new BigDecimal(recursoFaccaoSetor.stream()
                    .mapToDouble(rec -> new BigDecimal(rec.getEquipamentos()).multiply(rec.getTempoEquipamento().compareTo(BigDecimal.ZERO) == 0 ? new BigDecimal(1440) : rec.getTempoEquipamento()).multiply(rec.getEficiencia()).multiply(rec.getMargem().add(BigDecimal.ONE)).doubleValue())
                    .sum())
                    .divide(new BigDecimal(recursoFaccaoSetor.stream().map(rec -> rec.getColecao().getCodigo()).distinct().count()), 4, RoundingMode.HALF_UP);
            SdRecursoProducao.TipoCapacidade tipoCapacidadeRecurso = recursoFaccaoSetor.stream().map(rec -> rec.getTipoCapacidade()).distinct().findFirst().get();

            // verificando os parâmetros de configuração para ordenação das OFs para redistribuição das OFs
            List<Ordenacao> pesosAtributos = new ArrayList<>();
            pesosDistDemanda.stream().filter(peso -> peso.getPeso() > 0).sorted(Comparator.comparingInt(SdPesosCamposOrd::getPeso).reversed()).forEachOrdered(peso -> {
                pesosAtributos.add(new Ordenacao(peso.getTipo(), peso.getAtributo()));
            });
            // get das OFs do recurso com a ordenação configurada
            List<SdFluxoAtualOf> ofsRecurso = getOfsDemanda(recursoSelecionado.getId().getFaccao().getCodcli(), recursoSelecionado.getId().getSetor().getCodigo(), true, true, pesosAtributos);
            ofsRecurso.forEach(JPAUtils::refresh);

            // inicialização dos dados das OFs para cálculo
            ofsRecurso.forEach(of -> {
                of.setSaldoTempoOf(tipoCapacidadeRecurso.equals(SdRecursoProducao.TipoCapacidade.M) ? of.getTempoOf() : new BigDecimal(of.getQtdeOf()));
                of.getOcupacaoOf().clear();
            });
            // get da data mínima das OFs para início do cálculo do diário do recurso
            LocalDate minDate = ofsRecurso.stream().map(of -> of.getDtInicioProg()).distinct().min((o1, o2) -> o1.compareTo(o2)).orElse(LocalDate.now());

            diario.get().clear();
            LocalDate data = minDate;
            while (ofsRecurso.stream().anyMatch(of -> of.getSaldoTempoOf().compareTo(BigDecimal.ZERO) > 0)) {
                // get do objeto Ano da data do diário
                LocalDate finalData = data;
                Ano dia = new FluentDao().selectFrom(Ano.class).where(eb -> eb.equal("data", finalData).equal("diaUtil", true)).singleResult();
                if (dia != null) {
                    // criação do objeto do dia do diário
                    Diario diaRecurso = new Diario();
                    diaRecurso.setCapacidade(capacidadeRecurso);
                    diaRecurso.setFaccao(recursoSelecionado.getId().getFaccao());
                    diaRecurso.setSetor(recursoSelecionado.getId().getSetor());
                    diaRecurso.setData(dia);
                    diaRecurso.setOcupacao(BigDecimal.ZERO);

                    AtomicReference<BigDecimal> tempoOcupadoDia = new AtomicReference<>(diaRecurso.getCapacidade());
                    // get das OFs na ordem configurada onde o saldo de distribuição (SaldoTempoOf) é maior que 0 (zero)
                    // retorna somente as OFs que o envio é menor que a data do diário
                    // primeiro get das OFs que já tenham iniciado, senão busca novas OFs atendento a regra
                    Set<SdFluxoAtualOf> ofsDemandas = new HashSet<>(ofsRecurso.stream()
                            .filter(of -> of.getSaldoTempoOf().compareTo(tipoCapacidadeRecurso.equals(SdRecursoProducao.TipoCapacidade.M) ? of.getTempoOf() : new BigDecimal(of.getQtdeOf())) < 0
                                    && of.getSaldoTempoOf().compareTo(BigDecimal.ZERO) > 0
                                    && (of.getDtInicioProg().isBefore(dia.getData()) || of.getDtInicioProg().isEqual(dia.getData())))
                            .collect(Collectors.toList()));
                    if (ofsDemandas.size() == 0 || new BigDecimal(ofsDemandas.stream().mapToDouble(of -> of.getSaldoTempoOf().doubleValue()).sum()).compareTo(capacidadeRecurso) < 0) {
                        ofsDemandas.addAll(ofsRecurso.stream()
                                .filter(of -> of.getSaldoTempoOf().compareTo(BigDecimal.ZERO) > 0
                                        && (of.getDtInicioProg().isBefore(dia.getData()) || of.getDtInicioProg().isEqual(dia.getData())))
                                .collect(Collectors.toList()));
                    }
                    try {
                        // for nas OFs para obter o abatimento do dia do recurso
                        ofsDemandas.forEach(of -> {
                            // teste se o dia foi todo ocupado para o recurso
                            if (tempoOcupadoDia.get().compareTo(BigDecimal.ZERO) > 0) {
                                // verificação se a OF ainda não foi calculada para o dia de início assumir o dia em análise
                                if (of.getSaldoTempoOf().compareTo(tipoCapacidadeRecurso.equals(SdRecursoProducao.TipoCapacidade.M) ? of.getTempoOf() : new BigDecimal(of.getQtdeOf())) == 0)
                                    of.setDtInicioProg(diaRecurso.getData().getData());
                                if (of.getSaldoTempoOf().compareTo(tempoOcupadoDia.get()) > 0) {
                                    // o tempo de saldo da OF é maior que a capacidade do  recurso
                                    // assume o dia a capacidade do recurso
                                    diaRecurso.setOcupacao(diaRecurso.getOcupacao().add(tempoOcupadoDia.get()));
                                    of.getOcupacaoOf().add(new SdDemandaOf(diaRecurso.getData().getData(), of.getId().getNumero().getNumero(), of.getId().getFaccao().getCodcli(), of.getId().getSetor().getCodigo(),
                                            tipoCapacidadeRecurso.equals(SdRecursoProducao.TipoCapacidade.M) ?
                                                    tempoOcupadoDia.get().divide(of.getTempoOf().divide(new BigDecimal(of.getQtdeOf()), 4, RoundingMode.HALF_UP), 0, RoundingMode.DOWN) :
                                                    tempoOcupadoDia.get(),
                                            tipoCapacidadeRecurso.equals(SdRecursoProducao.TipoCapacidade.M) ?
                                                    tempoOcupadoDia.get() :
                                                    tempoOcupadoDia.get().multiply(of.getTempoOf().divide(new BigDecimal(of.getQtdeOf()), 4, RoundingMode.HALF_UP))));
                                    of.setSaldoTempoOf(of.getSaldoTempoOf().subtract(tempoOcupadoDia.get()));
                                    tempoOcupadoDia.set(tempoOcupadoDia.get().subtract(tempoOcupadoDia.get()));
                                } else {
                                    // o tempo de saldo da OF é menor que a capacidade do  recurso
                                    // assume o dia o saldo da OF
                                    diaRecurso.setOcupacao(diaRecurso.getOcupacao().add(of.getSaldoTempoOf()));
                                    of.getOcupacaoOf().add(new SdDemandaOf(diaRecurso.getData().getData(), of.getId().getNumero().getNumero(), of.getId().getFaccao().getCodcli(), of.getId().getSetor().getCodigo(),
                                            tipoCapacidadeRecurso.equals(SdRecursoProducao.TipoCapacidade.M) ?
                                                    of.getSaldoTempoOf().divide(of.getTempoOf().divide(new BigDecimal(of.getQtdeOf()), 4, RoundingMode.HALF_UP), 0, RoundingMode.DOWN) :
                                                    of.getSaldoTempoOf(),
                                            tipoCapacidadeRecurso.equals(SdRecursoProducao.TipoCapacidade.M) ?
                                                    of.getSaldoTempoOf() :
                                                    of.getSaldoTempoOf().multiply(of.getTempoOf().divide(new BigDecimal(of.getQtdeOf()), 4, RoundingMode.HALF_UP))));
                                    tempoOcupadoDia.set(tempoOcupadoDia.get().subtract(of.getSaldoTempoOf()));
                                    of.setSaldoTempoOf(BigDecimal.ZERO);
                                }
                                diaRecurso.getOfsDia().add(of);
//                                of.getId().setDtRetorno(diaRecurso.getData().getData());
//                                of.setDtRetornoProgamado(diaRecurso.getData().getData());
                                of.setDtRetornoProg(diaRecurso.getData().getData());
                            } else {
                                throw new ContinueException();
                            }
                        });
                    } catch (ContinueException ex) {
                    }
                    diario.get().add(diaRecurso);
                }
                data = data.plusDays(1);
            }
            diario.get().forEach(dia -> {
                dia.setOfsEntrega(ofsRecurso.stream().filter(of -> of.getDtRetornoProg().isEqual(dia.getData().getData())).collect(Collectors.toList()));
            });

            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                MessageBox.create(message -> {
                    message.message("Finalizado análise de demanda da facção.");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });

                criarCalendarioRecurso(diario.get());
            }
        });

    }

    private void procurarDemandas(Object[] faccoes, String grupoSetor, Object[] familias, Object[] controladores, Boolean verPlanejamento,
                                  Boolean proximoSetor, Object[] produtos, LocalDate dataEntrega, Object[] colecoes) {

        List<Object> controlador = new ArrayList<>(Arrays.asList(controladores));
        if (controladorLogado != null && controladorLogado.getCodigoFuncao() == 121)
            controlador.add(controladorLogado.getCodigo());

        JPAUtils.clearEntitys(tblDemandasFaccao.items.get());
        JPAUtils.clearEntitys(tblOfsNaoIniciadas.items.get());

        AtomicReference<Exception> exRwo = new AtomicReference<>();
        AtomicReference<List<VSdDemandaFaccao>> demandas = new AtomicReference<>(new ArrayList<>());
        AtomicReference<List<SdFluxoAtualOf>> ofsEmProducao = new AtomicReference<>(new ArrayList<>());
        new RunAsyncWithOverlay(this).exec(task -> {
            JPAUtils.clearEntitys(demandas.get());
            JPAUtils.clearEntitys(ofsEmProducao.get());
            demandas.set(getDemandasFaccao(faccoes, grupoSetor, familias, controlador.toArray(), produtos, dataEntrega));
            ofsEmProducao.set(carregandoOfsEmProducao(familias, grupoSetor, faccoes, controladores, verPlanejamento, proximoSetor, produtos, dataEntrega, colecoes));

            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                tblDemandasFaccao.items.set(FXCollections.observableList(demandas.get()));
                tblOfsNaoIniciadas.items.set(FXCollections.observableList(ofsEmProducao.get()));
            }
        });
    }

    private List<SdFluxoAtualOf> carregandoOfsEmProducao(Object[] familias, String setor, Object[] faccoes, Object[] controladores, Boolean emPlanejamento,
                                                         Boolean proximoSetor, Object[] produtos, LocalDate dataEntrega, Object[] colecoes) {
        // transformando o filtro família em SET para filtro das OFs em produção
        Set<Object> setFamilias = new HashSet<>(Arrays.asList(familias));
        // get das familias dos recursos caso não exista filtro de familia
        if (setFamilias.isEmpty()) {
            // get dos recursos do filtro
            getRecursos(faccoes, new String[]{setor}, familias, controladores, produtos, dataEntrega).forEach(recurso -> {
                recurso.getFamilias().forEach(familia -> {
                    setFamilias.add(familia.getId().getFamilia().getCodigo());
                });
            });
        }

        // get das OFs das famílias do filtro ou recursos e com os setores antes dos setores do filtro
        List<SdFluxoAtualOf> ofsEmProducao = getOfsProximoSetor(setor, setFamilias.toArray(), emPlanejamento, proximoSetor, colecoes);
        return ofsEmProducao;
    }

    private void carregarCalendarioFaccoes(VSdDemandaFaccao recurso) {
        AtomicReference<String> tipoVisao = new AtomicReference<>(fieldFilterTipoVisao.value.get());
        AtomicReference<Exception> exRwo = new AtomicReference<>();
        new RunAsyncWithOverlay(this).exec(task -> {

            // transformando a demanda da facção selecionada em um recurso para validação se existe
            List<SdRecursoProducao> recursoFaccaoSetor = (List<SdRecursoProducao>) new FluentDao().selectFrom(SdRecursoProducao.class)
                    .where(eb -> eb.equal("setorExcia.codigo", recurso.getId().getSetor().getSdgrupo())
                            .equal("fornecedor.codcli", recurso.getId().getFaccao().getCodcli()))
                    .resultList();
            recursoFaccaoSetor.forEach(JPAUtils::refresh);

            // get da capacidade dia do recurso
            // feito uma média entre as coleções e os turnos
            BigDecimal capacidadeRecurso = new BigDecimal(recursoFaccaoSetor.stream()
                    .mapToDouble(rec -> new BigDecimal(rec.getEquipamentos()).multiply(rec.getTempoEquipamento().compareTo(BigDecimal.ZERO) == 0 ? new BigDecimal(1440) : rec.getTempoEquipamento()).multiply(rec.getEficiencia()).multiply(rec.getMargem().add(BigDecimal.ONE)).doubleValue())
                    .sum())
                    .divide(new BigDecimal(recursoFaccaoSetor.stream().map(rec -> rec.getColecao().getCodigo()).distinct().count()), 4, RoundingMode.HALF_UP);
            SdRecursoProducao.TipoCapacidade tipoCapacidadeRecurso = recursoFaccaoSetor.stream().map(rec -> rec.getTipoCapacidade()).distinct().findFirst().get();

            // get das OFs programadas para o recurso (FACCAO/SETOR)
            // somente as OFs com o setor atual do recurso
            List<SdFluxoAtualOf> ofsRecurso = getOfsDemanda(recurso.getId().getFaccao().getCodcli(), recurso.getId().getSetor().getCodigo(), true, true, null);
            ofsRecurso.forEach(JPAUtils::refresh);
            // get da data mínima de início das ofs do recurso
            LocalDate minDate = ofsRecurso.stream().map(of -> of.getId().getDtInicio()).distinct().min((o1, o2) -> o1.compareTo(o2)).orElse(LocalDate.now());
            // get da data máxima de retorno das ofs do recurso
            LocalDate maxDate = ofsRecurso.stream().map(of -> of.getId().getDtRetorno()).distinct().max((o1, o2) -> o1.compareTo(o2)).orElse(LocalDate.now());
            // get do período das datas de início e fim das OFs do recurso
            // get somente dos dias úteis
            List<Ano> periodo = (List<Ano>) new FluentDao().selectFrom(Ano.class).where(eb -> eb.greaterThanOrEqualTo("data", minDate).lessThanOrEqualTo("data", maxDate).equal("diaUtil", true)).resultList();
            if (tipoVisao.get().equals("S"))
                periodo = periodo.stream().filter(it -> it.getData().getDayOfWeek().equals(DayOfWeek.MONDAY)).collect(Collectors.toList());

            // iniciando o for no período para carga dos dados de exibição
            diario.get().clear();
            for (Ano dia : periodo) {
                // criando o objeto do diário do recurso
                Diario diaRecurso = new Diario();
                diaRecurso.setData(dia);
                diaRecurso.setFaccao(recurso.getId().getFaccao());
                diaRecurso.setSetor(recurso.getId().getSetor());
                diaRecurso.setCapacidade(capacidadeRecurso.multiply(tipoVisao.get().equals("D") ? BigDecimal.ONE : new BigDecimal(5)));

                // get das OFs que tem o dia do diário do recurso entre o inínio e fim da OF
                // somente OFs no setor atual do recurso
                List<SdFluxoAtualOf> ofsDoDia = getOfsDoDia(recurso, dia.getData(), tipoVisao.get().equals("D") ? dia.getData() : dia.getData().plusDays(5), false, true);
                ofsDoDia.forEach(JPAUtils::refresh);
                // for nas OFs e get das demandas firmadas para a OF
                ofsDoDia.forEach(of -> {
                    // get das demandas firmadas na OF
                    SdDemandaOf demandaOf = getDemandaOfDia(recurso, dia, of);
                    // caso não tenha demanda firmada na OF
                    // cria uma demanda para o dia com o tempo dia da OF e a qtde com base no período da OF
                    if (demandaOf == null)
                        demandaOf = new SdDemandaOf(dia.getData(), of.getId().getNumero().getNumero(), of.getId().getFaccao().getCodcli(), of.getId().getSetor().getCodigo(),
                                of.getTempoDia().divide(of.getTempoOf().divide(new BigDecimal(of.getQtdeOf()), 4, RoundingMode.HALF_UP), 0, RoundingMode.CEILING), of.getTempoDia());

                    of.getOcupacaoOf().add(demandaOf);
                });
                diaRecurso.setOfsDia(ofsDoDia);
                // get da ocupação do dia do recurso
                // somatório das demandas firmadas para a OF
                diaRecurso.setOcupacao(new BigDecimal(ofsDoDia.stream()
                        .mapToDouble(of -> of.getOcupacaoOf().stream()
                                .filter(demOf -> !demOf.getId().getDtDemanda().isBefore(dia.getData())
                                        && !demOf.getId().getDtDemanda().isAfter(tipoVisao.get().equals("D") ? dia.getData() : dia.getData().plusDays(5)))
                                .mapToDouble(demOf -> tipoCapacidadeRecurso.equals(SdRecursoProducao.TipoCapacidade.M)
                                        ? demOf.getTempo().doubleValue()
                                        : demOf.getQtde().doubleValue()).sum()).sum()));
                // get das entregas de ofs do dia
                diaRecurso.setOfsEntrega(ofsRecurso.stream()
                        .filter(of -> !of.getId().getDtRetorno().isBefore(dia.getData())
                                && !of.getId().getDtRetorno().isAfter(tipoVisao.get().equals("D") ? dia.getData() : dia.getData().plusDays(5)))
                        .collect(Collectors.toList()));

                diario.get().add(diaRecurso);
            }

            return ReturnAsync.OK.value;
        }).addTaskEndNotification(taskReturn -> {
            if (taskReturn.equals(ReturnAsync.OK.value)) {
                criarCalendarioRecurso(diario.get());
            }
        });
    }

    private void programarOfFornecedor(SdFluxoAtualOf fluxoOf) {
        AtomicReference<VSdDadosOfPendente> dadosOf = new AtomicReference<>(getDadosOfPendente(fluxoOf));
        new Fragment().show(fragment -> {
            fragment.title("Programação de OF");
            fragment.size(350.0, 300.0);

            final FormFieldSingleFind<VSdDadosEntidade> fieldFornecedor = FormFieldSingleFind.create(VSdDadosEntidade.class, field -> {
                field.title("Fornecedor");
                field.toUpper();
                field.value.set(fluxoOf.getId().getFaccao());
            });
            final FormFieldDate fieldDataEnvio = FormFieldDate.create(field -> {
                field.title("Dt. Envio");
                field.width(170.0);
                field.value.set(fluxoOf.getId().getDtEnvio());
            });
            final FormFieldDate fieldDataInicio = FormFieldDate.create(field -> {
                field.title("Dt. Início");
                field.width(170.0);
                field.value.set(fluxoOf.getId().getDtInicio());
            });
            final FormFieldDate fieldDataRetorno = FormFieldDate.create(field -> {
                field.title("Dt. Retorno");
                field.width(170.0);
                field.value.set(fluxoOf.getId().getDtRetorno());
            });
            final FormFieldToggle fieldPrioridade = FormFieldToggle.create(field -> {
                field.title("Prioridade");
                field.value.set(fluxoOf.isPrioridade());
            });

            fieldFornecedor.postSelected((observable, oldValue, newValue) -> {
                if (newValue != null) {
                    fieldDataRetorno.value.set(getDtRetornoFaccao(fieldFornecedor.value.get().getCodcli(), fluxoOf, fieldDataInicio.value.get()));
                }
            });
            fieldDataInicio.value.addListener((observable, oldValue, newValue) -> {
                if (newValue != null) {
                    fieldDataRetorno.value.set(getDtRetornoFaccao(fieldFornecedor.value.get().getCodcli(), fluxoOf, fieldDataInicio.value.get()));
                }
            });
            fragment.box.getChildren().add(FormBox.create(content -> {
                content.vertical();
                content.expanded();
                content.add(fieldFornecedor.build());
                content.add(fields -> fields.addHorizontal(fieldDataEnvio.build(), fieldDataInicio.build()));
                content.add(fields -> fields.addHorizontal(fieldDataRetorno.build(), fieldPrioridade.build()));
                if (dadosOf.get() != null)
                    content.add(fields -> fields.addHorizontal(FormFieldText.create(field -> {
                        field.title("Corte");
                        field.expanded();
                        field.editable.set(false);
                        field.alignment(Pos.CENTER);
                        field.value.set(StringUtils.capitalize(dadosOf.get().getCort()));
                        field.addStyle(dadosOf.get().getCort().equals("NAO INICIADO") ? "danger" : dadosOf.get().getCort().equals("PARCIAL") ? "warning" : dadosOf.get().getCort().equals("BAIXADO") ? "success" : "info");
                    }).build(), FormFieldText.create(field -> {
                        field.title("Aplicações");
                        field.expanded();
                        field.editable.set(false);
                        field.alignment(Pos.CENTER);
                        field.value.set(StringUtils.capitalize(dadosOf.get().getAplic()));
                        field.addStyle(dadosOf.get().getAplic().equals("NAO INICIADO") ? "danger" : dadosOf.get().getAplic().equals("PARCIAL") ? "warning" : dadosOf.get().getAplic().equals("BAIXADO") ? "success" : "info");
                    }).build(), FormFieldText.create(field -> {
                        field.title("Costura");
                        field.expanded();
                        field.editable.set(false);
                        field.alignment(Pos.CENTER);
                        field.value.set(StringUtils.capitalize(dadosOf.get().getCost()));
                        field.addStyle(dadosOf.get().getCost().equals("NAO INICIADO") ? "danger" : dadosOf.get().getCost().equals("PARCIAL") ? "warning" : dadosOf.get().getCost().equals("BAIXADO") ? "success" : "info");
                    }).build(), FormFieldText.create(field -> {
                        field.title("Acabamento");
                        field.expanded();
                        field.editable.set(false);
                        field.alignment(Pos.CENTER);
                        field.value.set(StringUtils.capitalize(dadosOf.get().getAcab()));
                        field.addStyle(dadosOf.get().getAcab().equals("NAO INICIADO") ? "danger" : dadosOf.get().getAcab().equals("PARCIAL") ? "warning" : dadosOf.get().getAcab().equals("BAIXADO") ? "success" : "info");
                    }).build()));
                content.add(toolbar -> {
                    toolbar.horizontal();
                    toolbar.add(FormButton.create(btn -> {
                                btn.title("Foto Produto");
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.PRODUTO, ImageUtils.IconSize._32));
                                btn.contentDisplay(ContentDisplay.BOTTOM);
                                btn.addStyle("primary");
                                btn.setAction(evt -> {
                                    ImageView imgProduto = ImageUtils.getImageView("K:\\TI_ERP\\Arquivos\\imagens\\produto\\" + fluxoOf.getId().getNumero().getProduto().getCodigo() + ".jpg");
                                    imgProduto.setFitHeight(500.0);
                                    imgProduto.setPreserveRatio(true);
                                    imgProduto.setSmooth(true);
                                    new Fragment().show(frag -> {
                                        frag.title("Foto Produto");
                                        frag.size(500.0, 600.0);
                                        frag.withoutCancelButton();
                                        frag.box.getChildren().add(imgProduto);
                                    });
                                });
                            }),
                            FormButton.create(btn -> {
                                btn.title("Ficha Técnica");
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ABRIR, ImageUtils.IconSize._32));
                                btn.contentDisplay(ContentDisplay.BOTTOM);
                                btn.addStyle("warning");
                                btn.setAction(evt -> fragmentFichaTecnica(fluxoOf));
                            }),
                            FormButton.create(btn -> {
                                btn.title("Ficha de Materiais");
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.MATERIAIS, ImageUtils.IconSize._32));
                                btn.contentDisplay(ContentDisplay.BOTTOM);
                                btn.addStyle("dark");
                                btn.setAction(evt -> fragmentFichaMateriais(fluxoOf));
                            }));
                });
            }));

            fragment.buttonsBox.getChildren().add(FormButton.create(btn -> {
                btn.title("Programar");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CALENDARIO, ImageUtils.IconSize._24));
                btn.addStyle("primary");
                btn.setAction(evt -> {
                    try {
                        if (reprogramarOfEmProducao(fluxoOf, fieldFornecedor.value.get(), fieldDataEnvio.value.get(), fieldDataRetorno.value.get(), fieldDataInicio.value.get(), fieldPrioridade.value.get())) {
                            atualizaManualFluxoOf(fluxoOf, fieldDataRetorno.value.get(), fieldDataInicio.value.get(), fieldDataEnvio.value.get(), fieldFornecedor.value.get(), fieldPrioridade.value.get());
                            fluxoOf.getId().setDtRetorno(fieldDataRetorno.value.get());
                            fluxoOf.getId().setDtInicio(fieldDataInicio.value.get());
                            fluxoOf.getId().setDtEnvio(fieldDataEnvio.value.get());
                            fluxoOf.getId().setFaccao(fieldFornecedor.value.get());
                            fluxoOf.setPrioridade(fieldPrioridade.value.get());
                            JPAUtils.refresh(fluxoOf);
                            tblDemandasFaccao.items.stream().filter(fac -> fac.getId().getFaccao().getCodcli().equals(fluxoOf.getId().getFaccao().getCodcli()))
                                            .findFirst().ifPresent(fac -> JPAUtils.refresh(fac));
                            MessageBox.create(message -> {
                                message.message("OF reprogramada com sucesso!");
                                message.type(MessageBox.TypeMessageBox.CONFIRM);
                                message.position(Pos.TOP_RIGHT);
                                message.notification();
                            });
                            tblOfsNaoIniciadas.refresh();
                            tblDemandasFaccao.refresh();
                            fragment.close();
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                        ExceptionBox.build(message -> {
                            message.exception(e);
                            message.showAndWait();
                        });
                    }
                });
            }));
        });
    }

    private LocalDate getDtRetornoFaccao(String faccao, SdFluxoAtualOf fluxoOf, LocalDate dtInicio) {
        // transformando a demanda seleciona em recurso
        List<SdRecursoProducao> recursoFaccaoSetor = getRecursosProducao(new Object[]{fluxoOf.getId().getSetor().getCodigo()}, new Object[]{faccao});
        recursoFaccaoSetor.forEach(JPAUtils::refresh);

        if (recursoFaccaoSetor.size() > 0) {
            // get da capacidade média (coleção e turnos) diária do recurso
            BigDecimal capacidadeRecurso = new BigDecimal(recursoFaccaoSetor.stream()
                    .mapToDouble(rec -> new BigDecimal(rec.getEquipamentos()).multiply(rec.getTempoEquipamento().compareTo(BigDecimal.ZERO) == 0 ? new BigDecimal(1440) : rec.getTempoEquipamento()).multiply(rec.getEficiencia()).multiply(rec.getMargem().add(BigDecimal.ONE)).doubleValue())
                    .sum())
                    .divide(new BigDecimal(recursoFaccaoSetor.stream().map(rec -> rec.getColecao().getCodigo()).distinct().count()), 4, RoundingMode.HALF_UP);

            AtomicReference<Integer> leadTotal = new AtomicReference<>(fluxoOf.getTempoOf().divide(capacidadeRecurso, 0, RoundingMode.CEILING).intValue());
            AtomicReference<LocalDate> dataFinal = new AtomicReference<>(dtInicio);
            for (int i = 1; i <= leadTotal.get(); i++) {
                dataFinal.set(dtInicio.plusDays(i));
                Ano dia = new FluentDao().selectFrom(Ano.class)
                        .where(eb -> eb.equal("data", dataFinal.get())).singleResult();

                if (!dia.isDiaUtil())
                    leadTotal.set(leadTotal.get() + 1);
            }

            return dataFinal.get();
        } else {
            MessageBox.create(message -> {
                message.message("Não encontrado recurso cadastrado por esse fornecedor.");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showAndWait();
            });
            return null;
        }
    }

    private boolean reprogramarOfEmProducao(SdFluxoAtualOf fluxoOf, VSdDadosEntidade faccao, LocalDate dtEnvio, LocalDate dtRetorno, LocalDate dtInicio, Boolean prioridade) throws SQLException {

        if (!((Ano) new FluentDao().selectFrom(Ano.class).where(eb -> eb.equal("data", dtEnvio)).singleResult()).isDiaUtil()) {
            MessageBox.create(message -> {
                message.message("A data de envio selecionada não é um dia útil de produção.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return false;
        }
        if (!((Ano) new FluentDao().selectFrom(Ano.class).where(eb -> eb.equal("data", dtInicio)).singleResult()).isDiaUtil()) {
            MessageBox.create(message -> {
                message.message("A data de início selecionada não é um dia útil de produção.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return false;
        }
        if (!((Ano) new FluentDao().selectFrom(Ano.class).where(eb -> eb.equal("data", dtRetorno)).singleResult()).isDiaUtil()) {
            MessageBox.create(message -> {
                message.message("A data de retorno selecionada não é um dia útil de produção");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return false;
        }

        if (dtRetorno.isBefore(dtInicio)) {
            MessageBox.create(message -> {
                message.message("A data de retorno não pode ser anterior a data de início.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return false;
        }

        SdProgFaccao progFaccao = getProgramacaoFaccao(fluxoOf.getId().getNumero().getNumero(),
                faccao.getCodcli(),
                fluxoOf.getId().getSetor().getCodigo());
        if (progFaccao == null) {
            progFaccao = new SdProgFaccao(faccao, fluxoOf.getId().getSetor(), fluxoOf.getId().getNumero().getNumero());
        }
        progFaccao.setDtInicioProd(dtInicio);
        progFaccao.setDtProgramacaoOriginal(dtRetorno);
        progFaccao.setDtRetornoProg(dtRetorno);
        progFaccao.setTipo("M");
        progFaccao.setPrioridade(prioridade);

        saveProgFaccaoOf(progFaccao);
        SysLogger.addSysDelizLog("Controle Produção", TipoAcao.EDITAR, progFaccao.getId().getNumero(), "Reprogramando OF em produção: " + progFaccao.toLog());

        updateFaccaoPcp(fluxoOf.getId().getNumero().getNumero(), fluxoOf.getId().getSetor().getCodigo(), faccao.getCodcli(), dtInicio, dtInicio);
        SysLogger.addSysDelizLog("Controle Produção", TipoAcao.EDITAR, progFaccao.getId().getNumero(), "Alterando dados PCP no setor: " +
                fluxoOf.getId().getSetor().getCodigo() + " para a faccao " + faccao.getCodcli());

        return true;
    }

    private boolean trocarDataRetorno(SdFluxoAtualOf fluxoOf, LocalDate novaData) throws SQLException {
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Deseja realmente trocar a data de retorno?");
            message.showAndWait();
        }).value.get())) {
            if (!((Ano) new FluentDao().selectFrom(Ano.class).where(eb -> eb.equal("data", novaData)).singleResult()).isDiaUtil()) {
                MessageBox.create(message -> {
                    message.message("A data selecionada não é um dia útil de produção.");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showAndWait();
                });
                return false;
            }
            if (novaData.isBefore(fluxoOf.getDtInicioProg())) {
                MessageBox.create(message -> {
                    message.message("Você selecionou uma data para retorno da OF que é menor que o início da operação.");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showAndWait();
                });
                return false;
            }

            SdProgFaccao progFaccao = getProgramacaoFaccao(fluxoOf.getId().getNumero().getNumero(),
                    fluxoOf.getId().getFaccao().getCodcli(),
                    fluxoOf.getId().getSetor().getCodigo());
            if (progFaccao == null) {
                progFaccao = new SdProgFaccao(fluxoOf.getId().getFaccao(), fluxoOf.getId().getSetor(), fluxoOf.getId().getNumero().getNumero());
                progFaccao.setDtProgramacaoOriginal(novaData);
            }
            progFaccao.setDtRetornoProg(novaData);
            saveProgFaccaoOf(progFaccao);
            SysLogger.addSysDelizLog("Controle Produção", TipoAcao.EDITAR, progFaccao.getId().getNumero(), "Definindo data de retorno para a OF: " + progFaccao.toLog());
            updateMovimentoFaccao(novaData, fluxoOf.getId().getNumero().getNumero(), fluxoOf.getId().getFaccao().getCodcli(), fluxoOf.getId().getSetor().getCodigo());
            SysLogger.addSysDelizLog("Controle Produção", TipoAcao.EDITAR, progFaccao.getId().getNumero(),
                    "Alterado data de retorno para " + progFaccao.getDtRetornoProg() + " no setor " + progFaccao.getId().getSetor().getCodigo() + " facção " + progFaccao.getId().getFaccao().getCodcli());

            diario.get().stream().filter(dia -> dia.getOfsEntrega().contains(fluxoOf)).findFirst().ifPresent(dia -> dia.getOfsEntrega().remove(fluxoOf));
            fluxoOf.setDtRetornoProg(novaData);
            diario.get().stream().filter(dia -> dia.getData().getData().isEqual(novaData)).findFirst().ifPresent(dia -> dia.getOfsEntrega().add(fluxoOf));

        }
        return true;
    }

    private boolean trocarDataInicio(SdFluxoAtualOf fluxoOf, LocalDate novaData) throws SQLException {
        if (((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Deseja realmente trocar a data de início?");
            message.showAndWait();
        }).value.get())) {

            if (!((Ano) new FluentDao().selectFrom(Ano.class).where(eb -> eb.equal("data", novaData)).singleResult()).isDiaUtil()) {
                MessageBox.create(message -> {
                    message.message("A data selecionada não é um dia útil de produção.");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showAndWait();
                });
                return false;
            }
            if (!novaData.isAfter(fluxoOf.getId().getDtEnvio())) {
                MessageBox.create(message -> {
                    message.message("Você selecionou uma data para retorno da OF que é menor que o envio da OF.");
                    message.type(MessageBox.TypeMessageBox.WARNING);
                    message.showAndWait();
                });
                return false;
            }

            // definir nova data de retorno a partir do lead do produto
            AtomicReference<LocalDate> novaDataRetorno = new AtomicReference<>(novaData.plusDays(fluxoOf.getLeadSetor()));
            while (!((Ano) new FluentDao().selectFrom(Ano.class).where(eb -> eb.equal("data", novaDataRetorno.get())).singleResult()).isDiaUtil())
                novaDataRetorno.set(novaDataRetorno.get().plusDays(1));

            SdProgFaccao progFaccao = getProgramacaoFaccao(fluxoOf.getId().getNumero().getNumero(),
                    fluxoOf.getId().getFaccao().getCodcli(),
                    fluxoOf.getId().getSetor().getCodigo());
            if (progFaccao == null) {
                progFaccao = new SdProgFaccao(fluxoOf.getId().getFaccao(), fluxoOf.getId().getSetor(), fluxoOf.getId().getNumero().getNumero());
            }
            progFaccao.setTipo("M");
            progFaccao.setDtInicioProd(novaData);
            progFaccao.setDtRetornoProg(novaDataRetorno.get());

            saveProgFaccaoOf(progFaccao);
            SysLogger.addSysDelizLog("Controle Produção", TipoAcao.EDITAR, progFaccao.getId().getNumero(), "Definindo data de início para a OF: " + progFaccao.toLog());
            // atualização do diário e entregas
            diario.get().stream().filter(dia -> !dia.getData().getData().isBefore(fluxoOf.getDtInicioProg()) && !dia.getData().getData().isAfter(fluxoOf.getDtRetornoProg())).forEach(dia -> dia.getOfsDia().remove(fluxoOf));
            diario.get().stream().filter(dia -> dia.getOfsEntrega().contains(fluxoOf)).findFirst().ifPresent(dia -> dia.getOfsEntrega().remove(fluxoOf));
            fluxoOf.setDtRetornoProg(novaDataRetorno.get());
            fluxoOf.setDtInicioProg(novaData);
            diario.get().stream().filter(dia -> dia.getData().getData().isEqual(novaDataRetorno.get())).findFirst().ifPresent(dia -> dia.getOfsEntrega().add(fluxoOf));
            diario.get().stream().filter(dia -> !dia.getData().getData().isBefore(fluxoOf.getDtInicioProg()) && !dia.getData().getData().isAfter(fluxoOf.getDtRetornoProg())).forEach(dia -> dia.getOfsDia().add(fluxoOf));

        }
        return true;
    }

    private ContextMenu menuTblEntrega(SdFluxoAtualOf fluxoOf, FormTableView table) {
        return FormContextMenu.create(menu -> {
            menu.addItem(item -> {
                item.setText("Marcar como Pronto");
                item.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._16));
                item.setOnAction(evt -> {
                    if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                        message.message("Deseja realmente marcar como pronto?");
                        message.showAndWait();
                    }).value.get())) {
                        SdProgFaccao progFaccao = getProgramacaoFaccao(fluxoOf.getId().getNumero().getNumero(), fluxoOf.getId().getFaccao().getCodcli(), fluxoOf.getId().getSetor().getCodigo());
                        if (progFaccao == null) {
                            progFaccao = new SdProgFaccao(fluxoOf.getId().getFaccao(), fluxoOf.getId().getSetor(), fluxoOf.getId().getNumero().getNumero());
                        }
                        progFaccao.setFinalizado(true);
                        saveProgFaccaoOf(progFaccao);

                        fluxoOf.setMarcadoFinalizado(true);
                        MessageBox.create(message -> {
                            message.message("Setor marcado como finalizado na OF " + fluxoOf.getId().getNumero().getNumero());
                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                            message.position(Pos.TOP_RIGHT);
                            message.notification();
                        });
                        table.refresh();
                        table.tableProperties().getSelectionModel().clearSelection();
                    }
                });
            }); // Marcar como pronto
            menu.addSeparator();
            menu.addItem(item -> {
                item.setText("Marcar Prioridade");
                item.setGraphic(ImageUtils.getIcon(ImageUtils.Icon.FAVORITO, ImageUtils.IconSize._16));
                item.setOnAction(evt -> {
                    if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                        message.message("Deseja realmente marcar como prioridade?");
                        message.showAndWait();
                    }).value.get())) {
                        SdProgFaccao progFaccao = getProgramacaoFaccao(fluxoOf.getId().getNumero().getNumero(), fluxoOf.getId().getFaccao().getCodcli(), fluxoOf.getId().getSetor().getCodigo());
                        if (progFaccao == null) {
                            progFaccao = new SdProgFaccao(fluxoOf.getId().getFaccao(), fluxoOf.getId().getSetor(), fluxoOf.getId().getNumero().getNumero());
                        }
                        progFaccao.setPrioridade(true);
                        saveProgFaccaoOf(progFaccao);

                        fluxoOf.setPrioridade(true);
                        MessageBox.create(message -> {
                            message.message("Setor marcado como prioridade na OF " + fluxoOf.getId().getNumero().getNumero());
                            message.type(MessageBox.TypeMessageBox.CONFIRM);
                            message.position(Pos.TOP_RIGHT);
                            message.notification();
                        });
                        table.refresh();
                        table.tableProperties().getSelectionModel().clearSelection();
                    }
                });
            }); // Marcar prioridade
        });
    }

    private void imprimirProgramacao() {
        VSdDemandaFaccao recursoSelecionado = tblDemandasFaccao.selectedItem();
        if (recursoSelecionado != null) {
            AtomicReference<Exception> exRwo = new AtomicReference<>();
            new RunAsyncWithOverlay(this).exec(task -> {
                try {
                    new ReportUtils().config()
                            .addReport(ReportUtils.ReportFile.PROGRAMACAO_RECURSO,
                                    new ReportUtils.ParameterReport("p_faccao", recursoSelecionado.getId().getFaccao().getCodcli()),
                                    new ReportUtils.ParameterReport("p_setor", recursoSelecionado.getId().getSetor().getCodigo()))
                            .view().toView();
                } catch (JRException | SQLException e) {
                    exRwo.set(e);
                    return ReturnAsync.EXCEPTION.value;
                }
                return ReturnAsync.OK.value;
            }).addTaskEndNotification(taskReturn -> {
                if (taskReturn.equals(ReturnAsync.EXCEPTION.value)) {
                    exRwo.get().printStackTrace();
                    ExceptionBox.build(message -> {
                        message.exception(exRwo.get());
                        message.showAndWait();
                    });
                }
            });
        } else {
            MessageBox.create(message -> {
                message.message("Selecione um recurso na lista de demandas para impressão.");
                message.type(MessageBox.TypeMessageBox.ALERT);
                message.showAndWait();
            });
        }
    }

    // ---------------- METODOS/CLASSES INTERNAS DE VIEW --------------------
    private void fragmentDiarioOfs(SdFluxoAtualOf fluxoOf, Node owner) {
        try {
            AtomicReference<String> observacoesFaccao = new AtomicReference<>(getObservacaoFaccao(fluxoOf.getId().getNumero().getNumero(), fluxoOf.getId().getFaccao().getCodcli(), fluxoOf.getId().getSetor().getCodigo()));
            AtomicReference<VSdDadosOfPendente> dadosOf = new AtomicReference<>(getDadosOfPendente(fluxoOf));

            new Fragment().show(fragment -> {
                fragment.title("Informações da OF " + fluxoOf.getId().getNumero().getNumero());
                fragment.size(400.0, 700.0);
                fragment.getCancelButton().setText("Fechar");
                fragment.withoutCancelButton();
                fragment.box.setPadding(new Insets(0));
                fragment.content(FormAccordion.create(accord -> {
                    accord.expanded();
                    accord.add(FormTitledPane.create(tpane -> {
                        tpane.title("Recurso");
                        tpane.collapse(true);
                        tpane.add(fields -> {
                            fields.vertical();
                            fields.expanded();
                            fields.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.label("Facção:");
                                field.editable.set(false);
                                field.value.set(fluxoOf.getId().getFaccao().toString());
                            }).build());
                            fields.add(subFields -> subFields.addHorizontal(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.label("Setor:");
                                field.expanded();
                                field.editable.set(false);
                                field.value.set(fluxoOf.getId().getSetor().toString());
                            }).build(), FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.label("Lead:");
                                field.width(70.0);
                                field.editable.set(false);
                                field.alignment(Pos.CENTER);
                                field.value.set(StringUtils.toIntegerFormat(fluxoOf.getLeadSetor()));
                            }).build()));
                            fields.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.label("Controlador:");
                                field.editable.set(false);
                                field.value.set(fluxoOf.getControlador() != null ? fluxoOf.getControlador().toString() : "SEM CONTROLADOR");
                            }).build());
                        });
                    }));
                    accord.add(FormTitledPane.create(tpane -> {
                        tpane.title("Dados OF");
                        tpane.collapse(false);
                        tpane.add(fields -> {
                            fields.vertical();
                            fields.expanded();
                            fields.add(subFields -> subFields.addHorizontal(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.label("Qtde:");
                                field.expanded();
                                field.editable.set(false);
                                field.value.set(StringUtils.toIntegerFormat(fluxoOf.getQtdeOf()));
                            }).build(), FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.label("T. Total:");
                                field.expanded();
                                field.editable.set(false);
                                field.alignment(Pos.CENTER);
                                field.value.set(StringUtils.toDecimalFormat(fluxoOf.getTempoOf().doubleValue(), 4));
                            }).build()));
                            fields.add(subFields -> subFields.addHorizontal(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.label("Prod:");
                                field.expanded();
                                field.editable.set(false);
                                field.value.set(fluxoOf.getId().getNumero().getProduto().toString());
                            }).build()));
                            fields.add(subFields -> subFields.addHorizontal(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.label("Família:");
                                field.expanded();
                                field.editable.set(false);
                                field.value.set(fluxoOf.getId().getNumero().getProduto().getFamilia().toString());
                            }).build(), FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.label("Entrega:");
                                field.width(150.0);
                                field.editable.set(false);
                                field.value.set(StringUtils.toDateFormat(fluxoOf.getId().getNumero().getDtFinal()));
                            }).build()));
                            fields.add(subFields -> subFields.addHorizontal(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.label("Qtde Venda:");
                                field.expanded();
                                field.editable.set(false);
                                field.value.set(StringUtils.toIntegerFormat(fluxoOf.getVenda()));
                            }).build(), FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.label("Lote:");
                                field.expanded();
                                field.editable.set(false);
                                field.alignment(Pos.CENTER);
                                field.value.set(fluxoOf.getId().getNumero().getPeriodo());
                            }).build()));
                            fields.add(subFields -> subFields.addHorizontal(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.label("Marca:");
                                field.expanded();
                                field.editable.set(false);
                                field.value.set(fluxoOf.getId().getNumero().getProduto().getMarca().toString());
                            }).build(), FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.label("Coleção:");
                                field.expanded();
                                field.editable.set(false);
                                field.alignment(Pos.CENTER);
                                field.value.set(fluxoOf.getId().getNumero().getProduto().getColecao().toString());
                            }).build()));
                            if (dadosOf.get() != null)
                                fields.add(subFields -> subFields.addHorizontal(FormFieldText.create(field -> {
                                            field.title("Corte");
                                            field.expanded();
                                            field.editable.set(false);
                                            field.alignment(Pos.CENTER);
                                            field.value.set(StringUtils.capitalize(dadosOf.get().getCort()));
                                            field.addStyle(dadosOf.get().getCort().equals("NAO INICIADO") ? "danger" : dadosOf.get().getCort().equals("PARCIAL") ? "warning" : dadosOf.get().getCort().equals("BAIXADO") ? "success" : "info");
                                        }).build(),
                                        FormFieldText.create(field -> {
                                            field.title("Aplicações");
                                            field.expanded();
                                            field.editable.set(false);
                                            field.alignment(Pos.CENTER);
                                            field.value.set(StringUtils.capitalize(dadosOf.get().getAplic()));
                                            field.addStyle(dadosOf.get().getAplic().equals("NAO INICIADO") ? "danger" : dadosOf.get().getAplic().equals("PARCIAL") ? "warning" : dadosOf.get().getAplic().equals("BAIXADO") ? "success" : "info");
                                        }).build(),
                                        FormFieldText.create(field -> {
                                            field.title("Costura");
                                            field.expanded();
                                            field.editable.set(false);
                                            field.alignment(Pos.CENTER);
                                            field.value.set(StringUtils.capitalize(dadosOf.get().getCost()));
                                            field.addStyle(dadosOf.get().getCost().equals("NAO INICIADO") ? "danger" : dadosOf.get().getCost().equals("PARCIAL") ? "warning" : dadosOf.get().getCost().equals("BAIXADO") ? "success" : "info");
                                        }).build(),
                                        FormFieldText.create(field -> {
                                            field.title("Acabamento");
                                            field.expanded();
                                            field.editable.set(false);
                                            field.alignment(Pos.CENTER);
                                            field.value.set(StringUtils.capitalize(dadosOf.get().getAcab()));
                                            field.addStyle(dadosOf.get().getAcab().equals("NAO INICIADO") ? "danger" : dadosOf.get().getAcab().equals("PARCIAL") ? "warning" : dadosOf.get().getAcab().equals("BAIXADO") ? "success" : "info");
                                        }).build()));

                            fields.add(subFields -> subFields.addHorizontal(FormButton.create(btn -> {
                                        btn.title("Priorizar OF");
                                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.FAVORITO, ImageUtils.IconSize._16));
                                        btn.addStyle("xs").addStyle("success");
                                        btn.disable.set(fluxoOf.isPrioridade());
                                        btn.setAction(evt -> {
                                            new RunAsyncWithOverlay(this).exec(task -> {

                                                return ReturnAsync.OK.value;
                                            }).addTaskEndNotification(taskReturn -> {
                                                if (taskReturn.equals(ReturnAsync.OK.value)) {
                                                    //priorizarOf(fluxoOf);
                                                    fragment.close();
                                                }
                                            });
                                        });
                                    }),
                                    FormButton.create(btn -> {
                                        btn.title("Abrir ficha do produto");
                                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ABRIR, ImageUtils.IconSize._16));
                                        btn.addStyle("xs").addStyle("info");
                                        btn.setAction(evt -> {
                                            fragmentFichaTecnica(fluxoOf);
                                        });
                                    }),
                                    FormButton.create(btn -> {
                                        btn.title("Abrir ficha de materiais");
                                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.MATERIAIS, ImageUtils.IconSize._16));
                                        btn.addStyle("xs").addStyle("dark");
                                        btn.setAction(evt -> {
                                            fragmentFichaMateriais(fluxoOf);
                                        });
                                    })));
                            ImageView imgProduto = ImageUtils.getImageView("K:\\TI_ERP\\Arquivos\\imagens\\produto\\" + fluxoOf.getId().getNumero().getProduto().getCodigo() + ".jpg");
                            imgProduto.setFitHeight(300.0);
                            imgProduto.setPreserveRatio(true);
                            imgProduto.setSmooth(true);
                            fields.add(imgProduto);
                        });
                    }));
                    FormFieldDate fieldDataInicio = FormFieldDate.create(field -> {
                        field.title("Dt. Início");
                        field.value.set(fluxoOf.getDtInicioProg());
                    });
                    accord.add(FormTitledPane.create(tpane -> {
                        tpane.title("Calendário");
                        tpane.collapse(false);
                        tpane.add(fields -> {
                            fields.vertical();
                            fields.expanded();
                            if (fluxoOf.getDtRetornoProg().isBefore(LocalDate.now())) {
                                fields.add(FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.editable.set(false);
                                    field.addStyle("danger");
                                    field.value.set("OF em atraso - (" + Duration.between(fluxoOf.getDtRetornoProg().atStartOfDay(), LocalDate.now().atStartOfDay()).toDays() + " dia(s))");
                                }).build());
                            }
                            fields.add(subFields -> subFields.addHorizontal(FormBox.create(boxLabel -> {
                                boxLabel.vertical();
                                boxLabel.title("Dt. Envio");
                                boxLabel.expanded();
                                boxLabel.add(FormLabel.create(label -> {
                                    label.value.set(StringUtils.toDateFormat(fluxoOf.getId().getDtEnvio()));
                                }).build());
                            }), fieldDataInicio.build(), FormButton.create(btn -> {
                                btn.title("Trocar Data");
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.TROCAR, ImageUtils.IconSize._16));
                                btn.addStyle("success");
                                btn.setAction(evt -> {
                                    try {
                                        if (trocarDataInicio(fluxoOf, fieldDataInicio.value.getValue())) {
                                            fragment.close();
                                            criarCalendarioRecurso(diario.get());
                                        }
                                    } catch (SQLException e) {
                                        e.printStackTrace();
                                        ExceptionBox.build(message -> {
                                            message.exception(e);
                                            message.showAndWait();
                                        });
                                    }
                                });
                            })));
                            fields.add(subFields -> subFields.addHorizontal(FormBox.create(boxLabel -> {
                                boxLabel.vertical();
                                boxLabel.title("Dt. Retorno");
                                boxLabel.expanded();
                                boxLabel.add(FormLabel.create(label -> {
                                    label.value.set(StringUtils.toDateFormat(fluxoOf.getDtRetornoProg()));
                                }).build());
                            }), FormBox.create(boxLabel -> {
                                boxLabel.vertical();
                                boxLabel.title("Prog. Original");
                                boxLabel.expanded();
                                boxLabel.add(FormLabel.create(label -> {
                                    label.value.set(StringUtils.toDateFormat(fluxoOf.getDtRetornoProgamado()));
                                }).build());
                            })));
                            if (fluxoOf.isAtrasado()) {
                                fields.add(FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.label(ImageUtils.getIcon(ImageUtils.Icon.ALERT_ERROR, ImageUtils.IconSize._24));
                                    field.editable.set(false);
                                    field.value.set("OF em atraso - (" + fluxoOf.getDiasAtraso() + " dia(s) da primeira programação)");
                                }).build());
                            }

                            FormFieldTextArea editorObservacao = FormFieldTextArea.create(field -> {
                                field.withoutTitle();
                                field.expanded();
                            });
                            fields.add(toolbar -> {
                                toolbar.horizontal();
                                toolbar.height(40.0);
                                toolbar.alignment(Pos.BOTTOM_RIGHT);
                                toolbar.add(editorObservacao.build());
                                toolbar.add(FormButton.create(btn -> {
                                    btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.SALVAR, ImageUtils.IconSize._16));
                                    btn.addStyle("dark").addStyle("xs");
                                    btn.disable.bind(editorObservacao.value.isNull());
                                    btn.setAction(evt -> {
                                        String novaObservacao = StringUtils.toDateTimeFormat(LocalDateTime.now()).concat(" - ").concat(Globals.getNomeUsuario()).concat(": ").concat(editorObservacao.value.get());
                                        observacoesFaccao.set(novaObservacao.concat("\n").concat(observacoesFaccao.get()));
                                        try {
                                            saveObservacaoFaccao(observacoesFaccao.get(), fluxoOf.getId().getNumero().getNumero(), fluxoOf.getId().getFaccao().getCodcli(), fluxoOf.getId().getSetor().getCodigo());
                                            MessageBox.create(message -> {
                                                message.message("Observação salva!");
                                                message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                message.position(Pos.TOP_RIGHT);
                                                message.notification();
                                            });
                                        } catch (SQLException e) {
                                            e.printStackTrace();
                                            ExceptionBox.build(message -> {
                                                message.exception(e);
                                                message.showAndWait();
                                            });
                                        }
                                    });
                                }));
                            });
                            FormFieldTextArea observacaoFaccao = FormFieldTextArea.create(field -> {
                                field.title("Observações Facção");
                                field.expanded();
                                field.editable.set(false);
                                field.value.set(observacoesFaccao.get());
                            });
                            fields.add(observacaoFaccao.build());
                        });
                    }));
                    accord.setExpandedPane(accord.getPanes().get(2));
                }));
            });
        } catch (SQLException e) {
            ExceptionBox.build(m -> {
                m.exception(e);
                m.showAndWait();
            });
        }
    }

    private void fragmentEntregaOfs(SdFluxoAtualOf fluxoOf, Node owner) {
        try {
            AtomicReference<String> observacoesFaccao = new AtomicReference<>(getObservacaoFaccao(fluxoOf.getId().getNumero().getNumero(), fluxoOf.getId().getFaccao().getCodcli(), fluxoOf.getId().getSetor().getCodigo()));
            AtomicReference<VSdDadosOfPendente> dadosOf = new AtomicReference<>(getDadosOfPendente(fluxoOf));

            new Fragment().show(fragment -> {
                fragment.title("Informações da OF " + fluxoOf.getId().getNumero().getNumero());
                fragment.size(400.0, 700.0);
                fragment.getCancelButton().setText("Fechar");
                fragment.withoutCancelButton();
                fragment.box.setPadding(new Insets(0));
                fragment.content(FormAccordion.create(accord -> {
                    accord.expanded();
                    accord.add(FormTitledPane.create(tpane -> {
                        tpane.title("Recurso");
                        tpane.collapse(true);
                        tpane.add(fields -> {
                            fields.vertical();
                            fields.expanded();
                            fields.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.label("Facção:");
                                field.editable.set(false);
                                field.value.set(fluxoOf.getId().getFaccao().toString());
                            }).build());
                            fields.add(subFields -> subFields.addHorizontal(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.label("Setor:");
                                field.expanded();
                                field.editable.set(false);
                                field.value.set(fluxoOf.getId().getSetor().toString());
                            }).build(), FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.label("Lead:");
                                field.width(70.0);
                                field.editable.set(false);
                                field.alignment(Pos.CENTER);
                                field.value.set(StringUtils.toIntegerFormat(fluxoOf.getLeadSetor()));
                            }).build()));
                            fields.add(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.label("Controlador:");
                                field.editable.set(false);
                                field.value.set(fluxoOf.getControlador() != null ? fluxoOf.getControlador().toString() : "SEM CONTROLADOR");
                            }).build());
                        });
                    }));
                    accord.add(FormTitledPane.create(tpane -> {
                        tpane.title("Dados OF");
                        tpane.collapse(false);
                        tpane.add(fields -> {
                            fields.vertical();
                            fields.expanded();
                            fields.add(subFields -> subFields.addHorizontal(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.label("Qtde:");
                                field.expanded();
                                field.editable.set(false);
                                field.value.set(StringUtils.toIntegerFormat(fluxoOf.getQtdeOf()));
                            }).build(), FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.label("T. Total:");
                                field.expanded();
                                field.editable.set(false);
                                field.alignment(Pos.CENTER);
                                field.value.set(StringUtils.toDecimalFormat(fluxoOf.getTempoOf().doubleValue(), 4));
                            }).build()));
                            fields.add(subFields -> subFields.addHorizontal(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.label("Prod:");
                                field.expanded();
                                field.editable.set(false);
                                field.value.set(fluxoOf.getId().getNumero().getProduto().toString());
                            }).build()));
                            fields.add(subFields -> subFields.addHorizontal(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.label("Família:");
                                field.expanded();
                                field.editable.set(false);
                                field.value.set(fluxoOf.getId().getNumero().getProduto().getFamilia().toString());
                            }).build(), FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.label("Entrega:");
                                field.width(150.0);
                                field.editable.set(false);
                                field.value.set(StringUtils.toDateFormat(fluxoOf.getId().getNumero().getDtFinal()));
                            }).build()));
                            fields.add(subFields -> subFields.addHorizontal(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.label("Qtde Venda:");
                                field.expanded();
                                field.editable.set(false);
                                field.value.set(StringUtils.toIntegerFormat(fluxoOf.getVenda()));
                            }).build(), FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.label("Lote:");
                                field.expanded();
                                field.editable.set(false);
                                field.alignment(Pos.CENTER);
                                field.value.set(fluxoOf.getId().getNumero().getPeriodo());
                            }).build()));
                            fields.add(subFields -> subFields.addHorizontal(FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.label("Marca:");
                                field.expanded();
                                field.editable.set(false);
                                field.value.set(fluxoOf.getId().getNumero().getProduto().getMarca().toString());
                            }).build(), FormFieldText.create(field -> {
                                field.withoutTitle();
                                field.label("Coleção:");
                                field.expanded();
                                field.editable.set(false);
                                field.alignment(Pos.CENTER);
                                field.value.set(fluxoOf.getId().getNumero().getProduto().getColecao().toString());
                            }).build()));
                            if (dadosOf.get() != null)
                                fields.add(subFields -> subFields.addHorizontal(FormFieldText.create(field -> {
                                            field.title("Corte");
                                            field.expanded();
                                            field.editable.set(false);
                                            field.alignment(Pos.CENTER);
                                            field.value.set(StringUtils.capitalize(dadosOf.get().getCort()));
                                            field.addStyle(dadosOf.get().getCort().equals("NAO INICIADO") ? "danger" : dadosOf.get().getCort().equals("PARCIAL") ? "warning" : dadosOf.get().getCort().equals("BAIXADO") ? "success" : "info");
                                        }).build(),
                                        FormFieldText.create(field -> {
                                            field.title("Aplicações");
                                            field.expanded();
                                            field.editable.set(false);
                                            field.alignment(Pos.CENTER);
                                            field.value.set(StringUtils.capitalize(dadosOf.get().getAplic()));
                                            field.addStyle(dadosOf.get().getAplic().equals("NAO INICIADO") ? "danger" : dadosOf.get().getAplic().equals("PARCIAL") ? "warning" : dadosOf.get().getAplic().equals("BAIXADO") ? "success" : "info");
                                        }).build(),
                                        FormFieldText.create(field -> {
                                            field.title("Costura");
                                            field.expanded();
                                            field.editable.set(false);
                                            field.alignment(Pos.CENTER);
                                            field.value.set(StringUtils.capitalize(dadosOf.get().getCost()));
                                            field.addStyle(dadosOf.get().getCost().equals("NAO INICIADO") ? "danger" : dadosOf.get().getCost().equals("PARCIAL") ? "warning" : dadosOf.get().getCost().equals("BAIXADO") ? "success" : "info");
                                        }).build(),
                                        FormFieldText.create(field -> {
                                            field.title("Acabamento");
                                            field.expanded();
                                            field.editable.set(false);
                                            field.alignment(Pos.CENTER);
                                            field.value.set(StringUtils.capitalize(dadosOf.get().getAcab()));
                                            field.addStyle(dadosOf.get().getAcab().equals("NAO INICIADO") ? "danger" : dadosOf.get().getAcab().equals("PARCIAL") ? "warning" : dadosOf.get().getAcab().equals("BAIXADO") ? "success" : "info");
                                        }).build()));

                            fields.add(subFields -> subFields.addHorizontal(FormButton.create(btn -> {
                                        btn.title("Priorizar OF");
                                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.FAVORITO, ImageUtils.IconSize._16));
                                        btn.addStyle("xs").addStyle("success");
                                        btn.disable.set(fluxoOf.isPrioridade());
                                        btn.setAction(evt -> {
                                            new RunAsyncWithOverlay(this).exec(task -> {

                                                return ReturnAsync.OK.value;
                                            }).addTaskEndNotification(taskReturn -> {
                                                if (taskReturn.equals(ReturnAsync.OK.value)) {
//                                                    priorizarOf(fluxoOf);
                                                    fragment.close();
                                                }
                                            });
                                        });
                                    }),
                                    FormButton.create(btn -> {
                                        btn.title("Abrir ficha do produto");
                                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.ABRIR, ImageUtils.IconSize._16));
                                        btn.addStyle("xs").addStyle("info");
                                        btn.setAction(evt -> {
                                            fragmentFichaTecnica(fluxoOf);
                                        });
                                    }),
                                    FormButton.create(btn -> {
                                        btn.title("Abrir ficha de materiais");
                                        btn.icon(ImageUtils.getIcon(ImageUtils.Icon.MATERIAIS, ImageUtils.IconSize._16));
                                        btn.addStyle("xs").addStyle("dark");
                                        btn.setAction(evt -> {
                                            fragmentFichaMateriais(fluxoOf);
                                        });
                                    })));
                            ImageView imgProduto = ImageUtils.getImageView("K:\\TI_ERP\\Arquivos\\imagens\\produto\\" + fluxoOf.getId().getNumero().getProduto().getCodigo() + ".jpg");
                            imgProduto.setFitHeight(300.0);
                            imgProduto.setPreserveRatio(true);
                            imgProduto.setSmooth(true);
                            fields.add(imgProduto);
                        });
                    }));
                    FormFieldDate fieldDataRetorno = FormFieldDate.create(field -> {
                        field.title("Dt. Retorno");
                        field.value.set(fluxoOf.getDtRetornoProg());
                    });
                    accord.add(FormTitledPane.create(tpane -> {
                        tpane.title("Calendário");
                        tpane.collapse(false);
                        tpane.add(fields -> {
                            fields.vertical();
                            fields.expanded();
                            if (fluxoOf.getDtRetornoProg().isBefore(LocalDate.now())) {
                                fields.add(FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.editable.set(false);
                                    field.addStyle("danger");
                                    field.value.set("OF em atraso - (" + Duration.between(fluxoOf.getDtRetornoProg().atStartOfDay(), LocalDate.now().atStartOfDay()).toDays() + " dia(s))");
                                }).build());
                            }
                            fields.add(subFields -> subFields.addHorizontal(
                                    FormBox.create(boxLabel -> {
                                        boxLabel.vertical();
                                        boxLabel.expanded();
                                        boxLabel.title("Dt. Envio");
                                        boxLabel.add(FormLabel.create(label -> {
                                            label.value.set(StringUtils.toDateFormat(fluxoOf.getId().getDtEnvio()));
                                        }).build());
                                    }),
                                    FormBox.create(boxLabel -> {
                                        boxLabel.vertical();
                                        boxLabel.expanded();
                                        boxLabel.title("Dt. Início");
                                        boxLabel.add(FormLabel.create(label -> {
                                            label.value.set(StringUtils.toDateFormat(fluxoOf.getDtInicioProg()));
                                        }).build());
                                    }),
                                    FormBox.create(boxLabel -> {
                                        boxLabel.vertical();
                                        boxLabel.expanded();
                                        boxLabel.title("Prog. Original");
                                        boxLabel.add(FormLabel.create(label -> {
                                            label.value.set(StringUtils.toDateFormat(fluxoOf.getDtRetornoProgamado()));
                                        }).build());
                                    })));
                            if (fluxoOf.isAtrasado()) {
                                fields.add(FormFieldText.create(field -> {
                                    field.withoutTitle();
                                    field.label(ImageUtils.getIcon(ImageUtils.Icon.ALERT_ERROR, ImageUtils.IconSize._24));
                                    field.editable.set(false);
                                    field.value.set("OF em atraso - (" + fluxoOf.getDiasAtraso() + " dia(s) da primeira programação)");
                                }).build());
                            }
                            fields.add(subFields -> subFields.addHorizontal(fieldDataRetorno.build(), FormButton.create(btn -> {
                                btn.title("Trocar Data");
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.TROCAR, ImageUtils.IconSize._16));
                                btn.addStyle("success");
                                btn.setAction(evt -> {
                                    try {
                                        if (trocarDataRetorno(fluxoOf, fieldDataRetorno.value.getValue())) {
                                            fragment.close();
                                            criarCalendarioRecurso(diario.get());
                                        }
                                    } catch (SQLException ex) {
                                        ExceptionBox.build(m -> {
                                            m.exception(ex);
                                            m.showAndWait();
                                        });
                                    }
                                });
                            })));


                            FormFieldTextArea editorObservacao = FormFieldTextArea.create(field -> {
                                field.withoutTitle();
                                field.expanded();
                            });
                            FormFieldTextArea observacaoFaccao = FormFieldTextArea.create(field -> {
                                field.title("Observações Facção");
                                field.expanded();
                                field.editable.set(false);
                                field.value.set(observacoesFaccao.get());
                            });
                            fields.add(toolbar -> {
                                toolbar.horizontal();
                                toolbar.height(40.0);
                                toolbar.alignment(Pos.BOTTOM_RIGHT);
                                toolbar.add(editorObservacao.build());
                                toolbar.add(FormButton.create(btn -> {
                                    btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                    btn.icon(ImageUtils.getIcon(ImageUtils.Icon.SALVAR, ImageUtils.IconSize._16));
                                    btn.addStyle("dark").addStyle("xs");
                                    btn.disable.bind(editorObservacao.value.isNull());
                                    btn.setAction(evt -> {
                                        String novaObservacao = StringUtils.toDateTimeFormat(LocalDateTime.now()).concat(" - ").concat(Globals.getNomeUsuario()).concat(": ").concat(editorObservacao.value.get());
                                        observacoesFaccao.set(novaObservacao.concat("\n").concat(observacoesFaccao.get() == null ? "" : observacoesFaccao.get()));
                                        try {
                                            saveObservacaoFaccao(observacoesFaccao.get(), fluxoOf.getId().getNumero().getNumero(), fluxoOf.getId().getFaccao().getCodcli(), fluxoOf.getId().getSetor().getCodigo());
                                            observacaoFaccao.value.set(observacoesFaccao.get());
                                            MessageBox.create(message -> {
                                                message.message("Observação salva!");
                                                message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                message.position(Pos.TOP_RIGHT);
                                                message.notification();
                                            });
                                        } catch (SQLException e) {
                                            e.printStackTrace();
                                            ExceptionBox.build(message -> {
                                                message.exception(e);
                                                message.showAndWait();
                                            });
                                        }
                                    });
                                }));
                            });
                            fields.add(observacaoFaccao.build());
                        });
                    }));
                    accord.setExpandedPane(accord.getPanes().get(2));
                }));
            });
        } catch (SQLException e) {
            ExceptionBox.build(m -> {
                m.exception(e);
                m.showAndWait();
            });
        }
    }

    private void fragmentFichaMateriais(SdFluxoAtualOf fluxoOf) {
        try {
            new ReportUtils().config()
                    .addReport(ReportUtils.ReportFile.FICHA_MATERIAIS_OF,
                            new ReportUtils.ParameterReport("P_NUMERO_OF", fluxoOf.getId().getNumero().getNumero()))
                    .view()
                    .toView();
        } catch (JRException e) {
            e.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(e);
                message.showAndWait();
            });
        } catch (SQLException e) {
            e.printStackTrace();
            ExceptionBox.build(message -> {
                message.exception(e);
                message.showAndWait();
            });
        }
    }

    private void fragmentFichaTecnica(SdFluxoAtualOf fluxoOf) {
        new Fragment().show(frag -> {
            frag.title("Fichas Produto");
            frag.size(700.0, 900.0);
            frag.withoutCancelButton();
            List<String> filesFichaTecnica = new ArrayList<>();
            List<String> filesExtras = new ArrayList<>();
            List<Node> imagsFichas = new ArrayList<>();

            String baseFiles = "K:\\FichaTecnica\\"
                    .concat(fluxoOf.getId().getNumero().getProduto().getMarca().getDescricaoPath()).concat("\\");
            File folder = new File(baseFiles);
            String[] folders = folder.list();
            if (folders != null) {
                for (String s : folders) {
                    if (s.startsWith(fluxoOf.getId().getNumero().getProduto().getColecao().getCodigo())) {
                        baseFiles += s + "\\";
                        break;
                    }
                }

                File folderColecao = new File(baseFiles);
                String[] filesColecao = folderColecao.list();
                for (String s : filesColecao) {
                    String tmp = s.toUpperCase();
                    String[] arquivo = tmp.split("_");

                    if (arquivo[0].equals(fluxoOf.getId().getNumero().getProduto().getCodigo())) {
                        if (arquivo[arquivo.length - 1].toUpperCase().equals("FT.JPG") || arquivo[arquivo.length - 1].toUpperCase().equals("FT.JPEG")) {
                            if (arquivo.length > 2) {
                                if (fluxoOf.getId().getNumero().getItens().stream().anyMatch(it -> it.getId().getCor().getCor().equals(arquivo[1]))) {
                                    filesFichaTecnica.add(baseFiles + tmp);
                                }
                            } else {
                                filesFichaTecnica.add(baseFiles + tmp);
                            }
                        } else {
                            filesExtras.add(baseFiles + tmp);
                        }
                    }
                }
            }

            filesFichaTecnica.forEach(img -> {
                ImageView ficha = ImageUtils.getImageView(img);
                ficha.setFitHeight(850.0);
                ficha.setSmooth(true);
                ficha.setPreserveRatio(true);
                imagsFichas.add(ficha);
            });
            filesExtras.forEach(img -> {
                ImageView ficha = ImageUtils.getImageView(img);
                ficha.setFitHeight(850.0);
                ficha.setSmooth(true);
                ficha.setPreserveRatio(true);
                imagsFichas.add(ficha);
            });

            frag.box.getChildren().add(FormCarousel.create(imgs -> {
                imgs.setNodesCarousel(imagsFichas);
            }));
        });
    }

    private void fragmentFornecedor(SdFluxoAtualOf fluxoOf) {
        SdFluxoAtualOf fluxoSetorAtual = new FluentDao().selectFrom(SdFluxoAtualOf.class)
                .where(eb -> eb.equal("id.numero.numero", fluxoOf.getId().getNumero().getNumero())
                        .equal("id.setor.codigo", fluxoOf.getSetorEmProducao().getCodigo()))
                .findFirst();

        if (fluxoSetorAtual != null)
            new Fragment().show(fragment -> {
                fragment.title("Informações do Fornecedor");
                fragment.size(400.0, 300.0);
                fragment.withoutCancelButton();
                fragment.content(FormBox.create(fields -> {
                    fields.vertical();
                    fields.expanded();
                    fields.add(FormFieldText.create(field -> {
                        field.withoutTitle();
                        field.label("Facção:");
                        field.editable.set(false);
                        field.value.set(fluxoSetorAtual.getId().getFaccao().toString());
                    }).build());
                    fields.add(subFields -> subFields.addHorizontal(FormFieldText.create(field -> {
                        field.withoutTitle();
                        field.label("Setor:");
                        field.expanded();
                        field.editable.set(false);
                        field.value.set(fluxoSetorAtual.getId().getSetor().toString());
                    }).build(), FormFieldText.create(field -> {
                        field.withoutTitle();
                        field.label("Lead:");
                        field.width(70.0);
                        field.editable.set(false);
                        field.alignment(Pos.CENTER);
                        field.value.set(StringUtils.toIntegerFormat(fluxoSetorAtual.getLeadSetor()));
                    }).build()));
                    fields.add(subFields -> subFields.addHorizontal(FormFieldText.create(field -> {
                        field.withoutTitle();
                        field.label("Telefone:");
                        field.expanded();
                        field.editable.set(false);
                        field.value.set(fluxoSetorAtual.getId().getFaccao().getTelefone());
                    }).build(), FormFieldText.create(field -> {
                        field.withoutTitle();
                        field.label("E-mail:");
                        field.expanded();
                        field.editable.set(false);
                        field.value.set(fluxoSetorAtual.getId().getFaccao().getEmail());
                    }).build()));
                    fields.add(subFields -> subFields.addHorizontal(FormFieldText.create(field -> {
                        field.withoutTitle();
                        field.label("Cidade:");
                        field.expanded();
                        field.editable.set(false);
                        field.value.set(fluxoSetorAtual.getId().getFaccao().getCidade());
                    }).build(), FormFieldText.create(field -> {
                        field.withoutTitle();
                        field.label("UF:");
                        field.width(70.0);
                        field.editable.set(false);
                        field.value.set(fluxoSetorAtual.getId().getFaccao().getUf());
                    }).build()));
                }));
            });
    }

    private class Diario {

        private final ObjectProperty<Ano> data = new SimpleObjectProperty<>();
        private final ObjectProperty<BigDecimal> capacidade = new SimpleObjectProperty<>();
        private final ObjectProperty<BigDecimal> ocupacao = new SimpleObjectProperty<>();
        private final ObjectProperty<VSdDadosEntidade> faccao = new SimpleObjectProperty<>();
        private final ObjectProperty<CadFluxo> setor = new SimpleObjectProperty<>();
        private final StringProperty setorAtua = new SimpleStringProperty();
        private List<SdFluxoAtualOf> ofsDia = new ArrayList<>();
        private List<SdFluxoAtualOf> ofsEntrega = new ArrayList<>();


        public Ano getData() {
            return data.get();
        }

        public ObjectProperty<Ano> dataProperty() {
            return data;
        }

        public void setData(Ano data) {
            this.data.set(data);
        }

        public VSdDadosEntidade getFaccao() {
            return faccao.get();
        }

        public ObjectProperty<VSdDadosEntidade> faccaoProperty() {
            return faccao;
        }

        public void setFaccao(VSdDadosEntidade faccao) {
            this.faccao.set(faccao);
        }

        public CadFluxo getSetor() {
            return setor.get();
        }

        public ObjectProperty<CadFluxo> setorProperty() {
            return setor;
        }

        public void setSetor(CadFluxo setor) {
            this.setor.set(setor);
        }

        public BigDecimal getCapacidade() {
            return capacidade.get();
        }

        public ObjectProperty<BigDecimal> capacidadeProperty() {
            return capacidade;
        }

        public void setCapacidade(BigDecimal capacidade) {
            this.capacidade.set(capacidade);
        }

        public BigDecimal getOcupacao() {
            return ocupacao.get();
        }

        public ObjectProperty<BigDecimal> ocupacaoProperty() {
            return ocupacao;
        }

        public void setOcupacao(BigDecimal ocupacao) {
            this.ocupacao.set(ocupacao);
        }

        public List<SdFluxoAtualOf> getOfsDia() {
            return ofsDia;
        }

        public void setOfsDia(List<SdFluxoAtualOf> ofsDia) {
            this.ofsDia = ofsDia;
        }

        public String getSetorAtua() {
            return setorAtua.get();
        }

        public StringProperty setorAtuaProperty() {
            return setorAtua;
        }

        public void setSetorAtua(String setorAtua) {
            this.setorAtua.set(setorAtua);
        }

        public List<SdFluxoAtualOf> getOfsEntrega() {
            return ofsEntrega;
        }

        public void setOfsEntrega(List<SdFluxoAtualOf> ofsEntrega) {
            this.ofsEntrega = ofsEntrega;
        }
    }

}