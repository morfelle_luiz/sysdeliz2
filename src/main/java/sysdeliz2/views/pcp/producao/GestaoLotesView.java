package sysdeliz2.views.pcp.producao;

import javafx.beans.property.*;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import sysdeliz2.controllers.views.pcp.producao.GestaoLotesController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.dao.OrderType;
import sysdeliz2.models.generics.BasicModel;
import sysdeliz2.models.sysdeliz.SdDataTabPrz;
import sysdeliz2.models.ti.CadFluxo;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Faccao;
import sysdeliz2.models.ti.TabPrz;
import sysdeliz2.models.view.VSdDadosOfPendente;
import sysdeliz2.utils.enums.ReturnAsync;
import sysdeliz2.utils.enums.TipoAcao;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.gui.window.Fragment;
import sysdeliz2.utils.hibernate.JPAUtils;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.RunAsyncWithOverlay;
import sysdeliz2.utils.sys.StringUtils;
import sysdeliz2.utils.sys.SysLogger;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

public class GestaoLotesView extends GestaoLotesController {

    //<editor-fold desc="Bean">
    private final ListProperty<TabPrz> lotesBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<CadFluxo> setoresBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final ListProperty<VSdDadosOfPendente> ofsBean = new SimpleListProperty<>(FXCollections.observableArrayList());
    private final List<SdDataTabPrz> listDatasTabPrz = new ArrayList<>();
    private final BooleanProperty novoCadastro = new SimpleBooleanProperty(false);
    //</editor-fold>

    //<editor-fold desc="Table">
    private final FormTableView<TabPrz> tblPeriodos = FormTableView.create(TabPrz.class, table -> {
        table.expanded();
        table.items.bind(lotesBean);
        table.title("Lotes");
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Ações");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(150);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<TabPrz, TabPrz>, ObservableValue<TabPrz>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<TabPrz, TabPrz>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnVizualizar = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.VISUALIZAR_PEDIDO, ImageUtils.IconSize._16));
                                btn.tooltip("Vizualizar Lote");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("info");
                            });
                            final Button btnEditar = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EDIT, ImageUtils.IconSize._16));
                                btn.tooltip("Editar Lote");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("warning");
                            });
                            final Button btnExcluir = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CANCEL, ImageUtils.IconSize._16));
                                btn.tooltip("Excluir Lote");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("danger");
                            });
                            final Button btnAlterarData = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CALENDARIO, ImageUtils.IconSize._16));
                                btn.tooltip("Alterar Data do Lote");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("dark");
                            });

                            @Override
                            protected void updateItem(TabPrz item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {

                                    btnVizualizar.setOnAction(evt -> {
                                        tblPeriodos.selectItem(item);
                                        tabs.getSelectionModel().select(1);
                                    });

                                    btnEditar.setOnAction(evt -> {
                                        editarCadastro(item);
                                        tabs.getSelectionModel().select(1);
                                    });

                                    btnExcluir.setOnAction(evt -> {
                                        excluirCadastro(item);
                                    });

                                    btnAlterarData.setOnAction(evt -> {
                                        tabs.getSelectionModel().select(1);
                                        editarCadastro(item);
                                        dataLoteField.getDatePickerBegin().requestFocus();
                                    });

                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnVizualizar, btnAlterarData, btnEditar, btnExcluir);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<TabPrz, TabPrz>, ObservableValue<TabPrz>>) param -> new ReadOnlyObjectWrapper(param.getValue().getPrazo()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(300);
                    cln.value((Callback<TableColumn.CellDataFeatures<TabPrz, TabPrz>, ObservableValue<TabPrz>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDescricao()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Ativo");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<TabPrz, TabPrz>, ObservableValue<TabPrz>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.alignment(Pos.CENTER);
                    cln.format(param -> {
                        return new TableCell<TabPrz, TabPrz>() {
                            @Override
                            protected void updateItem(TabPrz item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                setAlignment(Pos.CENTER);
                                if (item != null && !empty) {
                                    setGraphic(ImageUtils.getIcon(item.isAtivo() ? ImageUtils.Icon.STATUS_SIM : ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._24));
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Coleção");
                    cln.width(250);
                    cln.value((Callback<TableColumn.CellDataFeatures<TabPrz, TabPrz>, ObservableValue<TabPrz>>) param -> new ReadOnlyObjectWrapper(param.getValue().getColecao() == null ? "" : param.getValue().getColecao().getCodigo() + " - " + param.getValue().getColecao().getDescricao()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Tipo");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<TabPrz, TabPrz>, ObservableValue<TabPrz>>) param -> new ReadOnlyObjectWrapper(TabPrz.TipoLote.valueOf(param.getValue().getTipo()).toString()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Dt. Início");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<TabPrz, TabPrz>, ObservableValue<TabPrz>>) param -> new ReadOnlyObjectWrapper(StringUtils.toDateFormat(param.getValue().getDtInicio())));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Dt. Fim");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<TabPrz, TabPrz>, ObservableValue<TabPrz>>) param -> new ReadOnlyObjectWrapper(StringUtils.toDateFormat(param.getValue().getDtFim())));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Minutos");
                    cln.width(150);
                    cln.value((Callback<TableColumn.CellDataFeatures<TabPrz, TabPrz>, ObservableValue<TabPrz>>) param -> new ReadOnlyObjectWrapper(param.getValue().getMinuto()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Fatura Separado");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<TabPrz, TabPrz>, ObservableValue<TabPrz>>) param -> new ReadOnlyObjectWrapper(param.getValue().isFaturaSeparado() ? "Sim" : "Não"));
                }).build()
        );
    });

    private final FormTableView<VSdDadosOfPendente> tblOfsLote = FormTableView.create(VSdDadosOfPendente.class, table -> {
        table.expanded();
        table.title("OP's");
        table.items.bind(ofsBean);
        table.factoryRow(param -> {
            return new TableRow<VSdDadosOfPendente>() {
                @Override
                protected void updateItem(VSdDadosOfPendente item, boolean empty) {
                    super.updateItem(item, empty);
                    clear();

                    if (item != null && !empty) {
                        getStyleClass().add(item.getQtde() == 0 ? "table-row-success" : "table-row-warning");
                    }
                }

                private void clear() {
                    getStyleClass().removeAll("table-row-danger", "table-row-success", "table-row-warning");
                }
            };
        });
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(40);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<VSdDadosOfPendente, VSdDadosOfPendente>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnEditarData = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CALENDARIO, ImageUtils.IconSize._16));
                                btn.tooltip("Alterar data da OP");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("warning");
                                btn.disable.bind(navegation.inEdition.not());
                            });

                            @Override
                            protected void updateItem(VSdDadosOfPendente item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {

                                    btnEditarData.setOnAction(evt -> {
                                        if (janelaAlterarDataOf(item)) {
                                            MessageBox.create(message -> {
                                                message.message("Data da OP atualizada com sucesso");
                                                message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                message.position(Pos.TOP_RIGHT);
                                                message.notification();
                                            });
                                            tblOfsLote.refresh();
                                        }
                                    });

                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnEditarData);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Número");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getNumero()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(100);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo() == null ? "" : param.getValue().getCodigo().getCodigo()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Setor");
                    cln.width(200);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getSetor().getCodigo() + " - " + param.getValue().getId().getSetor().getDescricao()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Coleção");
                    cln.width(200);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo() == null ? "" : param.getValue().getCodigo().getCodCol() + " - " + param.getValue().getCodigo().getColecao()));
                }).build(),
//                FormTableColumn.create(cln -> {
//                    cln.title("Parte");
//                    cln.width(150);
//                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getParte()));
//                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Qtde Pendente");
                    cln.width(120);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Dt. Inicial");
                    cln.width(120);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getOf1() == null ? "" : StringUtils.toShortDateFormat(param.getValue().getOf1().getDtInicio())));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Dt. Final");
                    cln.width(120);
                    cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getOf1() == null ? "" : StringUtils.toShortDateFormat(param.getValue().getOf1().getDtFinal())));
                }).build()
        );
    });

    private final FormTableView<CadFluxo> tblSetores = FormTableView.create(CadFluxo.class, table -> {
        table.expanded();
        table.items.bind(setoresBean);
        table.title("Lotes");
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Código");
                    cln.width(80);
                    cln.value((Callback<TableColumn.CellDataFeatures<CadFluxo, CadFluxo>, ObservableValue<CadFluxo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Descrição");
                    cln.width(300);
                    cln.value((Callback<TableColumn.CellDataFeatures<CadFluxo, CadFluxo>, ObservableValue<CadFluxo>>) param -> new ReadOnlyObjectWrapper(param.getValue().getDescricao()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Data Ativa");
                    cln.width(60);
                    cln.alignment(Pos.CENTER);
                    cln.order(Comparator.comparing(CadFluxo::isControleData));
                    cln.value((Callback<TableColumn.CellDataFeatures<CadFluxo, CadFluxo>, ObservableValue<CadFluxo>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<CadFluxo, CadFluxo>() {
                            @Override
                            protected void updateItem(CadFluxo item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(FormButton.create(btn -> {
                                        btn.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                        btn.icon(ImageUtils.getIcon(item.isControleData() ? ImageUtils.Icon.CONFIRMAR : ImageUtils.Icon.CANCEL, ImageUtils.IconSize._24));
                                        btn.addStyle(item.isControleData() ? "success" : "danger");
                                        btn.setOnAction(evt -> {
                                            item.setControleData(!item.isControleData());
                                            new FluentDao().merge(item);
                                            desenhaFieldsDatas();
                                            MessageBox.create(message -> {
                                                message.message("Status da Data ativa alterada com sucesso.");
                                                message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                message.position(Pos.TOP_RIGHT);
                                                message.notification();
                                            });
                                            table.refresh();
                                        });
                                    }));
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Tipo");
                    cln.width(220);
                    cln.alignment(Pos.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<CadFluxo, CadFluxo>, ObservableValue<CadFluxo>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<CadFluxo, CadFluxo>() {
                            @Override
                            protected void updateItem(CadFluxo item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(FormFieldSegmentedButton.create(btn -> {
                                        btn.withoutTitle();
                                        btn.alwaysSelected();
                                        btn.options(
                                                btn.option("Jeans", "J", "success"),
                                                btn.option("Moda", "M", "warning"),
                                                btn.option("Ambos", "JM", "info")
                                        );
                                        btn.select(item.getSdLinha());
                                        btn.value.addListener((observable, oldValue, newValue) -> {
                                            item.setSdLinha(newValue.toString());
                                            desenhaFieldsDatas();
                                            new FluentDao().merge(item);
                                            MessageBox.create(message -> {
                                                message.message("Status da Data ativa alterada com sucesso.");
                                                message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                message.position(Pos.TOP_RIGHT);
                                                message.notification();
                                            });
                                        });
                                    }).build());
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Apelido");
                    cln.width(350);
                    cln.value((Callback<TableColumn.CellDataFeatures<CadFluxo, CadFluxo>, ObservableValue<CadFluxo>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<CadFluxo, CadFluxo>() {
                            @Override
                            protected void updateItem(CadFluxo item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(FormFieldText.create(text -> {
                                        text.withoutTitle();
                                        text.value.setValue(item.getSdDescricao());
                                        text.editable.set(false);
                                        text.textField.setOnMouseClicked(evt -> {
                                            text.editable.set(true);
                                        });
                                        text.focusedListener((observable, oldValue, newValue) -> {
                                            if (!newValue) {
                                                text.editable.set(false);
                                                if (!text.value.getValue().equals(item.getSdDescricao())) {
                                                    item.setSdDescricao(text.value.getValue());
                                                    new FluentDao().merge(item);
                                                    desenhaFieldsDatas();
                                                    MessageBox.create(message -> {
                                                        message.message("Apelido alterado com sucesso.");
                                                        message.type(MessageBox.TypeMessageBox.CONFIRM);
                                                        message.position(Pos.TOP_RIGHT);
                                                        message.notification();
                                                    });
                                                }
                                            }
                                        });
                                    }).build());
                                }
                            }
                        };
                    });
                }).build()
        );
    });

    //</editor-fold>

    //<editor-fold desc="Box">
    private final VBox listagemTab = (VBox) super.tabs.getTabs().get(0).getContent();
    private final VBox manutencaoTab = (VBox) super.tabs.getTabs().get(1).getContent();
    private final VBox controleTab = (VBox) super.tabs.getTabs().get(2).getContent();

    private final FormNavegation<TabPrz> navegation = FormNavegation.create(nav -> {
        nav.withNavegation(tblPeriodos);
        nav.withActions(true, true, true);
        nav.withReturn(evt -> super.tabs.getSelectionModel().select(0));
        nav.btnAddRegister(evt -> novoCadastro());
        nav.btnDeleteRegister(evt -> excluirCadastro((TabPrz) nav.selectedItem.get()));
        nav.btnEditRegister(evt -> editarCadastro((TabPrz) nav.selectedItem.get()));
        nav.btnSave(evt -> {
            salvarCadastro((TabPrz) nav.selectedItem.get());
        });
        nav.btnCancel(evt -> cancelarCadastro());
        nav.selectedItem.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                carregaDados((TabPrz) newValue);
            }
            nav.oldItem.set(oldValue);
        });
    });

    //</editor-fold>

    //<editor-fold desc="Components">
    private final FormFieldMultipleFind<TabPrz> codLoteFilter = FormFieldMultipleFind.create(TabPrz.class, field -> {
        field.title("Lote");
        field.toUpper();
        field.width(200);
    });

    private final FormFieldMultipleFind<Colecao> colecaoFilter = FormFieldMultipleFind.create(Colecao.class, field -> {
        field.title("Coleção");
        field.toUpper();
        field.width(200);
    });


    private final FormFieldDatePeriod dataLoteFilter = FormFieldDatePeriod.create(field -> {
        field.title("Data Início Lote");
        field.withLabels();
        field.valueBegin.setValue(LocalDate.now().minusMonths(1));
        field.valueEnd.setValue(LocalDate.now().plusMonths(1));
        field.width(320);
    });

    private final FormFieldDatePeriod dataLoteField = FormFieldDatePeriod.create(field -> {
        field.title("Data Lote");
        field.withLabels();
        field.editable.bind(navegation.inEdition);
    });

    private final FormFieldText codLoteField = FormFieldText.create(field -> {
        field.title("Lote");
        field.editable.bind(novoCadastro);
        field.toUpper();
        field.width(150);
        field.focusedListener((observable, oldValue, newValue) -> {
            if (!newValue && field.editable.getValue()) {
                TabPrz lote = new FluentDao().selectFrom(TabPrz.class).where(it -> it.equal("prazo", field.value.getValueSafe())).singleResult();
                if (lote != null) {
                    if (((Boolean) QuestionBox.build(Boolean.class, message -> {
                        message.message("Código de lote já existe, deseja alterar esse lote?");
                        message.showAndWait();
                    }).value.get())) {
                        navegation.selectedItem.set(lote);
                    } else {
                        field.clear();
                        field.requestFocus();
                        return;
                    }
                }
                if (field.value.getValueSafe().length() > 5) {
                    MessageBox.create(message -> {
                        message.message("Código do lote não pode ter mais de 5 digitos");
                        message.type(MessageBox.TypeMessageBox.WARNING);
                        message.position(Pos.TOP_RIGHT);
                        message.notification();
                    });
                    field.clear();
                    field.requestFocus();
                }
            }
        });
    });

    private final FormFieldText descricaoField = FormFieldText.create(field -> {
        field.title("Descrição");
        field.editable.set(false);
        field.toUpper();
        field.width(500);
    });


    private final FormFieldComboBox<TabPrz.TipoLote> tipoLoteField = FormFieldComboBox.create(TabPrz.TipoLote.class, field -> {
        field.title("Tipo");
        field.items.set(FXCollections.observableArrayList(TabPrz.TipoLote.values()));
        field.select(0);
        field.editable.bind(navegation.inEdition);
        field.width(150);
    });


    private final FormFieldToggle ativoField = FormFieldToggle.create(field -> {
        field.title("Ativo");
        field.editable.bind(navegation.inEdition);
    });


    private final FormFieldSingleFind<Colecao> colecaoField = FormFieldSingleFind.create(Colecao.class, field -> {
        field.title("Coleção");
        field.toUpper();
        field.editable.bind(navegation.inEdition);
        field.width(280);
    });

    private final FormFieldComboBox<String> linhaField = FormFieldComboBox.create(String.class, field -> {
        field.title("Linha");
        field.items(FXCollections.observableArrayList(Arrays.asList("Jeans", "Moda")));
        field.select(0);
        field.editable.bind(navegation.inEdition);
    });

    //</editor-fold>

    public GestaoLotesView() {
        super("Gestão de Lotes (Período)", ImageUtils.getImage(ImageUtils.Icon.CALENDARIO), new String[]{"Listagem", "Manutenção", "Controle"});
        initListagem();
        initManutencao();
        initControle();
    }

    private void initListagem() {
        listagemTab.getChildren().add(FormBox.create(root -> {
            root.vertical();
            root.expanded();
            root.add(header -> {
                header.horizontal();
                header.add(FormTitledPane.create(filter -> {
                    filter.filter();
                    filter.width(900);
                    filter.add(FormBox.create(lFilter -> {
                        lFilter.horizontal();
                        lFilter.add(codLoteFilter.build(), colecaoFilter.build(), dataLoteFilter.build());
                    }));
                    filter.find.setOnAction(evt -> {
                        new RunAsyncWithOverlay(this).exec(task -> {
                            buscarLotes(
                                    codLoteFilter.objectValues.stream().map(TabPrz::getPrazo).toArray(),
                                    colecaoFilter.objectValues.stream().map(Colecao::getCodigo).toArray(),
                                    dataLoteFilter.valueBegin.getValue(),
                                    dataLoteFilter.valueEnd.getValue()
                            );
                            return ReturnAsync.OK.value;
                        }).addTaskEndNotification(taskReturn -> {
                            if (taskReturn.equals(ReturnAsync.OK.value)) {
                                lotesBean.set(FXCollections.observableArrayList(lotesList));
                                tblPeriodos.refresh();
                            }
                        });
                    });
                    filter.clean.setOnAction(evt -> {
                        codLoteFilter.clear();
                        colecaoFilter.clear();
                        dataLoteFilter.valueBegin.setValue(LocalDate.now().minusMonths(1));
                        dataLoteFilter.valueEnd.setValue(LocalDate.now().plusMonths(1));
                    });
                }));
            });
            root.add(content -> {
                content.vertical();
                content.expanded();
                content.add(tblPeriodos.build());
            });
        }));
    }

    private void initManutencao() {
        manutencaoTab.getChildren().add(navegation);
        manutencaoTab.getChildren().add(FormBox.create(root -> {
            root.horizontal();
            root.expanded();
            root.add(col1 -> {
                col1.vertical();
                col1.expanded();
                col1.add(header -> {
                    header.horizontal();
                    header.alignment(Pos.BOTTOM_LEFT);
                    header.add(codLoteField.build(), descricaoField.build());
                });
                col1.add(l1 -> {
                    l1.horizontal();
                    l1.alignment(Pos.BOTTOM_LEFT);
                    l1.add(tipoLoteField.build(), linhaField.build(), colecaoField.build(), ativoField.build());
                });
                col1.add(l2 -> {
                    l2.flutuante();
                    l2.maxWidthSize(700);
                    l2.setId("boxFieldsDatasLote");
                    linhaField.value.addListener((observable, oldValue, newValue) -> {
                        desenhaFieldsDatas();
                    });

                    navegation.selectedItem.addListener((observable, oldValue, newValue) -> {
                        if (newValue != null) {
                            desenhaFieldsDatas();
                        }
                    });
                });
            });
            root.add(col2 -> {
                col2.vertical();
                col2.expanded();
                col2.add(tblOfsLote.build());
            });
        }));
    }

    private void desenhaFieldsDatas() {
        FormBox boxFieldsDatas = (FormBox) this.lookup("#boxFieldsDatasLote");
        boxFieldsDatas.clear();
        listDatasTabPrz.clear();

        if (navegation.selectedItem.getValue() == null) return;

        boxFieldsDatas.add(dataLoteField.build());

        List<CadFluxo> listSetores = (List<CadFluxo>) new FluentDao().selectFrom(CadFluxo.class).where(it -> it.equal("controleData", true).like("sdLinha", String.valueOf(linhaField.value.getValue().charAt(0)))).orderBy("codigo", OrderType.ASC).resultList();

        for (CadFluxo setor : listSetores) {

            boxFieldsDatas.add(FormFieldDatePeriod.create(field -> {
                field.withLabels();
                field.setId(setor.getCodigo());
                field.editable.bind(navegation.inEdition);
                field.title("[" + setor.getCodigo() + "] " + setor.getSdDescricao());
                SdDataTabPrz dataTabPrz = new FluentDao().selectFrom(SdDataTabPrz.class).where(it -> it
                        .equal("codigo", navegation.selectedItem.getValue().getPrazo())
                        .equal("setor", setor.getCodigo())
                ).singleResult();

                if (dataTabPrz == null) {
                    dataTabPrz = new SdDataTabPrz(navegation.selectedItem.getValue().getPrazo(), setor.getCodigo());
                }
                listDatasTabPrz.add(dataTabPrz);
                field.valueBegin.bindBidirectional(dataTabPrz.dataInicioProperty());
                field.valueEnd.bindBidirectional(dataTabPrz.dataFimProperty());
            }).build());
        }
    }

    private void initControle() {
        controleTab.getChildren().add(FormBox.create(root -> {
            root.vertical();
            root.expanded();
            root.add(tblSetores.build());
            setoresBean.addAll(((List<CadFluxo>) new FluentDao().selectFrom(CadFluxo.class).get().resultList()));
        }));
    }

    private void carregaDados(TabPrz lote) {
        navegation.selectedItem.set(lote);
        codLoteField.value.setValue(lote.getPrazo());
        colecaoField.value.setValue(lote.getColecao());
        descricaoField.value.setValue(lote.getDescricao());
        ativoField.value.setValue(lote.isAtivo());
        linhaField.select(lote.getSdLinha() == null ? "Jeans" : lote.getSdLinha().equals("J") ? "Jeans" : "Moda");
        dataLoteField.valueBegin.setValue(lote.getDtInicio() == null ? LocalDate.now() : lote.getDtInicio());
        dataLoteField.valueEnd.setValue(lote.getDtInicio() == null ? LocalDate.now() : lote.getDtFim());

        try {
            tipoLoteField.select(TabPrz.TipoLote.valueOf(lote.getTipo()));
        } catch (IllegalArgumentException | NullPointerException e) {
            tipoLoteField.clearSelection();
            e.printStackTrace();
        }

        List<VSdDadosOfPendente> ofsPendentes = new ArrayList<>();
//        if (ofsBean.size() == 0 || ofsBean.stream().anyMatch(it -> !it.getPeriodo().equals(lote.getPrazo()))) {
        List<VSdDadosOfPendente> periodo = (List<VSdDadosOfPendente>) new FluentDao().selectFrom(VSdDadosOfPendente.class).where(it -> it.equal("periodo", lote.getPrazo()).equal("id.setor.codigo", "100")).resultList();
        ofsPendentes.addAll(periodo);
        ofsBean.set(FXCollections.observableArrayList(ofsPendentes));
//        }
        tblOfsLote.refresh();
    }

    private void cancelarCadastro() {
        limparCampos();
        navegation.inEdition.set(false);
        novoCadastro.set(false);
        tblPeriodos.tableview().getSelectionModel().clearSelection();
        selecionaItemAnterior();
    }

    private void selecionaItemAnterior() {
        if (tblPeriodos.items.size() > 0) {
            if (navegation.oldItem != null && tblPeriodos.items.contains(navegation.oldItem.get()))
                tblPeriodos.selectItem(navegation.oldItem.get());
            else tblPeriodos.selectItem(0);
        }
    }

    private void limparCampos() {
        codLoteField.clear();
        colecaoField.clear();
        descricaoField.clear();
        ativoField.value.setValue(false);
        dataLoteField.clear();
        tipoLoteField.clearSelection();
        ofsBean.clear();

        desenhaFieldsDatas();
    }

    private TabPrz getValoresCampos(TabPrz lote) {
        lote.setPrazo(codLoteField.value.getValue());
        lote.setColecao(colecaoField.value.getValue());
        lote.setAtivo(ativoField.value.getValue());
        lote.setTipo(tipoLoteField.value.getValue().name());
        lote.setDtInicio(dataLoteField.valueBegin.getValue());
        lote.setDtFim(dataLoteField.valueEnd.getValue());
        lote.setDescricao("LOTE " + linhaField.value.getValue().toUpperCase() + " " + lote.getPrazo() + " - " + StringUtils.toShortDateFormat(lote.getDtInicio()) + " - " + StringUtils.toShortDateFormat(lote.getDtFim()));
        lote.setSdLinha(linhaField.value.getValue().equals("Jeans") ? "J" : "M");

        return lote;
    }

    private void salvarCadastro(TabPrz novoLote) {
        if (novoLote == null) return;

        if (!verificaCamposVazios()) return;

        if (!((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Tem certeza que deseja salvar esse lote?");
            message.showAndWait();
        }).value.get())) {
            return;
        }

        TabPrz loteSalvo = new FluentDao().selectFrom(TabPrz.class).where(it -> it.equal("prazo", codLoteField.value.getValue())).singleResult();
        novoLote = getValoresCampos(new TabPrz());

        if (loteSalvo == null) {
            try {
                novoLote = new FluentDao().persist(novoLote);
                salvarDatasSetores(novoLote);
                carregaDados(novoLote);

                SysLogger.addSysDelizLog("Gestão Lote", TipoAcao.SALVAR, novoLote.getPrazo(), "Lote " + novoLote.getPrazo() + " salvo com sucesso!");
                MessageBox.create(message -> {
                    message.message("Lote cadastrado com sucesso!");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
                TabPrz finalNovoLote = novoLote;
                tblPeriodos.items.stream().filter(it -> it.getPrazo().equals(finalNovoLote.getPrazo())).findFirst().ifPresent(item -> {
                    tblPeriodos.items.remove(item);
                    tblPeriodos.items.add(item);
                });
                carregaDados(novoLote);
                navegation.inEdition.set(false);
                novoCadastro.set(false);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return;
        }

        JPAUtils.getEntityManager().refresh(loteSalvo);
        List<VSdDadosOfPendente> ofsPendentes = ofsBean.stream().filter(it -> it.getQtde() > 0).collect(Collectors.toList());
        if (ofsPendentes.size() != 0 && (!novoLote.getDtInicio().equals(loteSalvo.getDtInicio()) || !novoLote.getDtFim().equals(loteSalvo.getDtFim()))) {
            if (!janelaAlterarDatasOf(ofsPendentes)) return;
        }
        getValoresCampos(loteSalvo);
        loteSalvo = new FluentDao().merge(loteSalvo);
        salvarDatasSetores(loteSalvo);
        carregaDados(loteSalvo);

        TabPrz finalLoteSalvo = loteSalvo;
        tblPeriodos.items.stream().filter(it -> it.getPrazo().equals(finalLoteSalvo.getPrazo())).findFirst().ifPresent(item -> {
            tblPeriodos.items.remove(item);
            tblPeriodos.items.add(item);
        });
        navegation.inEdition.set(false);
        novoCadastro.set(false);

        SysLogger.addSysDelizLog("Gestão Lote", TipoAcao.EDITAR, loteSalvo.getPrazo(), "Lote " + loteSalvo.getPrazo() + " atualizado com sucesso!");
        MessageBox.create(message -> {
            message.message("Lote atualizado com sucesso!");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }

    private void salvarDatasSetores(TabPrz loteSalvo) {
        for (SdDataTabPrz datas : listDatasTabPrz) {
            datas.setCodigo(loteSalvo.getPrazo());
        }
        new FluentDao().mergeAll(listDatasTabPrz);

        List<SdDataTabPrz> todasDatas = (List<SdDataTabPrz>) new FluentDao().selectFrom(SdDataTabPrz.class).where(it -> it.equal("codigo", loteSalvo.getPrazo())).resultList();
        List<CadFluxo> listSetores = (List<CadFluxo>) new FluentDao().selectFrom(CadFluxo.class).where(it -> it.equal("controleData", true).like("sdLinha", String.valueOf(linhaField.value.getValue().charAt(0)))).resultList();

        for (SdDataTabPrz data : todasDatas) {
            CadFluxo setor = listSetores.stream().filter(it -> it.getCodigo().equals(data.getSetor())).findFirst().orElse(null);
            if (setor == null || data.getDataInicio() == null || data.getDataFim() == null)
                new FluentDao().delete(data);
        }
    }

    private boolean verificaCamposVazios() {

        if (codLoteField.value.getValueSafe().equals("")) {
            MessageBox.create(message -> {
                message.message("Código do lote não pode ser vazio");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return false;
        }

        if (dataLoteField.valueBegin.getValue() == null || dataLoteField.valueEnd.getValue() == null || listDatasTabPrz.stream().anyMatch(it -> it.getDataInicio() == null || it.getDataFim() == null)) {
            MessageBox.create(message -> {
                message.message("Data de Início e de Fim não podem ser vazias nos campos.");
                message.type(MessageBox.TypeMessageBox.WARNING);
                message.showAndWait();
            });
            return false;
        }
        return true;
    }

    private boolean janelaAlterarDatasOf(List<VSdDadosOfPendente> ofsPendentes) {
        AtomicBoolean response = new AtomicBoolean(false);

        new Fragment().show(fg -> {
            fg.size(1000.0, 600.0);
            fg.title("Selecione as OP's que terão as datas alteradas");
            final ListProperty<VSdDadosOfPendente> ofsPendBean = new SimpleListProperty<>(FXCollections.observableArrayList(ofsPendentes));
            ofsPendBean.forEach(it -> it.setSelected(true));
            final FormTableView<VSdDadosOfPendente> tblOps = FormTableView.create(VSdDadosOfPendente.class, table -> {
                table.expanded();
                table.title("OP's");
                table.selectColumn();
                table.editable.set(true);
                table.items.bind(ofsPendBean);
                table.columns(
                        FormTableColumn.create(cln -> {
                            cln.title("Número");
                            cln.width(100);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getNumero()));
                        }).build(),
                        FormTableColumn.create(cln -> {
                            cln.title("Código");
                            cln.width(100);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getCodigo()));
                        }).build(),
                        FormTableColumn.create(cln -> {
                            cln.title("Setor");
                            cln.width(200);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getSetor().getCodigo() + " - " + param.getValue().getId().getSetor().getDescricao()));
                        }).build(),
                        FormTableColumn.create(cln -> {
                            cln.title("Coleção");
                            cln.width(200);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getCodigo().getCodCol() + " - " + param.getValue().getCodigo().getColecao()));
                        }).build(),
//                        FormTableColumn.create(cln -> {
//                            cln.title("Parte");
//                            cln.width(150);
//                            cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getParte()));
//                        }).build(),
                        FormTableColumn.create(cln -> {
                            cln.title("Qtde Pendente");
                            cln.width(120);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(param.getValue().getQtde()));
                        }).build(),
                        FormTableColumn.create(cln -> {
                            cln.title("Dt. Inicial");
                            cln.width(120);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(StringUtils.toShortDateFormat(param.getValue().getOf1().getDtInicio())));
                        }).build(),
                        FormTableColumn.create(cln -> {
                            cln.title("Dt. Final");
                            cln.width(120);
                            cln.value((Callback<TableColumn.CellDataFeatures<VSdDadosOfPendente, VSdDadosOfPendente>, ObservableValue<VSdDadosOfPendente>>) param -> new ReadOnlyObjectWrapper(StringUtils.toShortDateFormat(param.getValue().getOf1().getDtFinal())));
                        }).build()
                );
            });
            fg.box.getChildren().add(tblOps.build());
            fg.boxFullButtons.getChildren().add(FormButton.create(btn -> {
                btn.title("Confirmar");
                btn.addStyle("success");
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                btn.setOnAction(evt -> {
                    List<VSdDadosOfPendente> selecionados = tblOps.items.stream().filter(BasicModel::isSelected).collect(Collectors.toList());
                    if (selecionados.size() == 0) return;
                    selecionados.forEach(it -> atualizarDatasOf(it, dataLoteField.valueBegin.getValue(), dataLoteField.valueEnd.getValue()));
                    response.set(true);
                    fg.close();
                });
            }));
        });
        return response.get();
    }

    private boolean janelaAlterarDataOf(VSdDadosOfPendente item) {
        AtomicBoolean result = new AtomicBoolean(false);
        new Fragment().show(fg -> {
            fg.size(350.0, 150.0);
            fg.title("Defina as novas datas da OP");
            final FormFieldDatePeriod dataOpField = FormFieldDatePeriod.create(field -> {
                field.title("Data OP");
                field.withLabels();
                field.valueBegin.setValue(item.getOf1().getDtInicio());
                field.valueEnd.setValue(item.getOf1().getDtFinal());
            });
            fg.box.getChildren().add(dataOpField.build());
            fg.boxFullButtons.getChildren().add(FormButton.create(btn -> {
                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.CONFIRMAR, ImageUtils.IconSize._24));
                btn.addStyle("success");
                btn.title("Confirmar");
                btn.setOnAction(evt -> {
                    if (dataOpField.valueBegin.getValue() != item.getOf1().getDtInicio() || dataOpField.valueEnd.getValue() != item.getOf1().getDtFinal()) {
                        atualizarDatasOf(item, dataOpField.valueBegin.getValue(), dataOpField.valueEnd.getValue());
                    }
                    result.set(true);
                    fg.close();
                });
            }));
        });
        return result.get();
    }

    private void atualizarDatasOf(VSdDadosOfPendente of, LocalDate dtInicio, LocalDate dtFim) {
        if (of.getOf1() != null) {

            String dtInicioAntiga = StringUtils.toShortDateFormat(of.getOf1().getDtInicio());
            String dtFimAntiga = StringUtils.toShortDateFormat(of.getOf1().getDtFinal());

            of.getOf1().setDtInicio(dtInicio);
            of.getOf1().setDtFinal(dtFim);

            of.setOf1(new FluentDao().merge(of.getOf1()));

            List<Faccao> listFaccao = (List<Faccao>) new FluentDao().selectFrom(Faccao.class).where(it -> it.equal("id.numero", of.getId().getNumero()).equal("id.op", "100")).resultList();

            for (Faccao faccao : listFaccao) {
                CadFluxo cadFluxo = new FluentDao().selectFrom(CadFluxo.class).where(it -> it.equal("codigo", faccao.getId().getOp())).singleResult();
                faccao.setDts(dtInicio);
                faccao.setDtr(dtInicio.plusDays(cadFluxo.getDias()));
                new FluentDao().merge(faccao);
            }

            SysLogger.addSysDelizLog("Gestão Lote", TipoAcao.EDITAR, of.getOf1().getNumero(), "Of " + of.getOf1().getNumero() + " teve suas datas alteradas: " +
                    "\nData Inicial Antiga: " + dtInicioAntiga + " -> Data Inical Nova: " + StringUtils.toShortDateFormat(of.getOf1().getDtInicio()) +
                    "\nData Final Antiga: " + dtFimAntiga + " -> Data Final Nova: " + StringUtils.toShortDateFormat(of.getOf1().getDtFinal()));
        }
    }

    private void editarCadastro(TabPrz tabPrz) {
        navegation.selectedItem.set(tabPrz);
        navegation.inEdition.set(true);
        MessageBox.create(message -> {
            message.message("Modo de edição habilitado");
            message.type(MessageBox.TypeMessageBox.WARNING);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }

    private void excluirCadastro(TabPrz lote) {
        if (lote == null) return;

        if (!QuestionBox.quest("excluir o lote?")) {
            return;
        }

        new FluentDao().delete(lote);
        List<SdDataTabPrz> listDatas = (List<SdDataTabPrz>) new FluentDao().selectFrom(SdDataTabPrz.class).where(it -> it.equal("codigo", lote.getPrazo())).resultList();
        new FluentDao().deleteAll(listDatas);
        SysLogger.addSysDelizLog("Gestão Lote", TipoAcao.EXCLUIR, lote.getPrazo(), "Lote " + lote.getPrazo() + " exluído com sucesso!");

        lotesBean.remove(lote);
        tblPeriodos.refresh();

        limparCampos();

        MessageBox.create(message -> {
            message.message("Lote excluído com sucesso!");
            message.type(MessageBox.TypeMessageBox.CONFIRM);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }

    private void novoCadastro() {
        novoCadastro.set(true);
        limparCampos();
        navegation.selectedItem.set(new TabPrz());
        editarCadastro(navegation.selectedItem.getValue());
    }
}
