package sysdeliz2.views.pcp.producao;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import sysdeliz2.controllers.views.pcp.producao.IndiceProjecaoController;
import sysdeliz2.dao.FluentDao;
import sysdeliz2.models.sysdeliz.SdIndiceProjecao001;
import sysdeliz2.models.sysdeliz.SdIndiceProjecao001PK;
import sysdeliz2.models.ti.Colecao;
import sysdeliz2.models.ti.Linha;
import sysdeliz2.models.ti.Marca;
import sysdeliz2.utils.gui.components.*;
import sysdeliz2.utils.gui.messages.ExceptionBox;
import sysdeliz2.utils.gui.messages.MessageBox;
import sysdeliz2.utils.gui.messages.QuestionBox;
import sysdeliz2.utils.sys.ImageUtils;
import sysdeliz2.utils.sys.StringUtils;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;

public class IndiceProjecaoView extends IndiceProjecaoController {
    
    // <editor-fold defaultstate="collapsed" desc="Declaração: Filter">
    private final FormFieldMultipleFind<Marca> marcaFilterField = FormFieldMultipleFind.create(Marca.class, field -> {
        field.title("Marca");
    });
    private final FormFieldMultipleFind<Colecao> colecaoFilterField = FormFieldMultipleFind.create(Colecao.class, field -> {
        field.title("Coleção");
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Table">
    private final FormTableView<SdIndiceProjecao001> tableListagem = FormTableView.create(SdIndiceProjecao001.class, table -> {
        table.title("Cadastrados");
        table.expanded();
        table.items.bind(indices);
        table.columns(
                FormTableColumn.create(cln -> {
                    cln.title("Ações");
                    cln.icon(ImageUtils.getIcon(ImageUtils.Icon.EXECUTAR, ImageUtils.IconSize._16));
                    cln.width(115.0);
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdIndiceProjecao001, SdIndiceProjecao001>, ObservableValue<SdIndiceProjecao001>>) param -> new ReadOnlyObjectWrapper(param.getValue()));
                    cln.format(param -> {
                        return new TableCell<SdIndiceProjecao001, SdIndiceProjecao001>() {
                            final HBox boxButtonsRow = new HBox(3);
                            final Button btnVisualizar = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIconButton(ImageUtils.ButtonIcon.FIND, ImageUtils.IconSize._16));
                                btn.tooltip("Visualizar Cadastro");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("info");
                            });
                            final Button btnEditar = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.EDIT, ImageUtils.IconSize._16));
                                btn.tooltip("Editar Cadastro");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("warning");
                            });
                            final Button btnExcluir = FormButton.create(btn -> {
                                btn.icon(ImageUtils.getIcon(ImageUtils.Icon.TRASH, ImageUtils.IconSize._16));
                                btn.tooltip("Excluir Cadastro");
                                btn.contentDisplay(ContentDisplay.GRAPHIC_ONLY);
                                btn.addStyle("xs").addStyle("danger");
                            });
                            
                            @Override
                            protected void updateItem(SdIndiceProjecao001 item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    
                                    btnVisualizar.setOnAction(evt -> {
                                        abrirCadastro(item);
                                    });
                                    btnEditar.setOnAction(evt -> {
                                        editarCadastro(item);
                                    });
                                    btnExcluir.setOnAction(evt -> {
                                        excluirCadastro(item);
                                    });
                                    boxButtonsRow.getChildren().clear();
                                    boxButtonsRow.getChildren().addAll(btnVisualizar, btnEditar, btnExcluir);
                                    setGraphic(boxButtonsRow);
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Coleção");
                    cln.width(180.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdIndiceProjecao001, SdIndiceProjecao001>, ObservableValue<SdIndiceProjecao001>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getColecao()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Coleção");
                    cln.width(200.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdIndiceProjecao001, SdIndiceProjecao001>, ObservableValue<SdIndiceProjecao001>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getColecao()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Marca");
                    cln.width(150.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdIndiceProjecao001, SdIndiceProjecao001>, ObservableValue<SdIndiceProjecao001>>) param -> new ReadOnlyObjectWrapper(param.getValue().getId().getMarca()));
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Por Marca");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdIndiceProjecao001, SdIndiceProjecao001>, ObservableValue<SdIndiceProjecao001>>) param -> new ReadOnlyObjectWrapper(param.getValue().isByMarca()));
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.format(param -> {
                        return new TableCell<SdIndiceProjecao001, Boolean>() {
                            @Override
                            protected void updateItem(Boolean item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(item ? ImageUtils.getIcon(ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._16) : ImageUtils.getIcon(ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Por Linha");
                    cln.width(70.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdIndiceProjecao001, SdIndiceProjecao001>, ObservableValue<SdIndiceProjecao001>>) param -> new ReadOnlyObjectWrapper(param.getValue().isByLinha()));
                    cln.alignment(FormTableColumn.Alignment.CENTER);
                    cln.format(param -> {
                        return new TableCell<SdIndiceProjecao001, Boolean>() {
                            @Override
                            protected void updateItem(Boolean item, boolean empty) {
                                super.updateItem(item, empty);
                                setText(null);
                                setGraphic(null);
                                if (item != null && !empty) {
                                    setGraphic(item ? ImageUtils.getIcon(ImageUtils.Icon.STATUS_SIM, ImageUtils.IconSize._16) : ImageUtils.getIcon(ImageUtils.Icon.STATUS_NAO, ImageUtils.IconSize._16));
                                }
                            }
                        };
                    });
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Fixo");
                    cln.width(100.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdIndiceProjecao001, SdIndiceProjecao001>, ObservableValue<SdIndiceProjecao001>>) param -> new ReadOnlyObjectWrapper(param.getValue().getByFixo()));
                    cln.alignment(FormTableColumn.Alignment.RIGHT);
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Por Meta");
                    cln.width(100.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdIndiceProjecao001, SdIndiceProjecao001>, ObservableValue<SdIndiceProjecao001>>) param -> new ReadOnlyObjectWrapper(param.getValue().getByFixo()));
                    cln.alignment(FormTableColumn.Alignment.RIGHT);
                }).build(),
                FormTableColumn.create(cln -> {
                    cln.title("Linha");
                    cln.width(220.0);
                    cln.value((Callback<TableColumn.CellDataFeatures<SdIndiceProjecao001, SdIndiceProjecao001>, ObservableValue<SdIndiceProjecao001>>) param -> new ReadOnlyObjectWrapper(param.getValue().getLinhaMeta()));
                }).build()
        );
    });
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: View">
    private final VBox listagemTab = (VBox) super.tabs.getTabs().get(0).getContent();
    private final VBox manutencaoTab = (VBox) super.tabs.getTabs().get(1).getContent();
    private final FormNavegation<SdIndiceProjecao001> navegation = FormNavegation.create(nav -> {
        nav.withNavegation(tableListagem);
        nav.withActions(true, true, true);
        nav.withReturn(evt -> super.tabs.getSelectionModel().select(0));
        nav.btnAddRegister(evt -> novoCadastro());
        nav.btnDeleteRegister(evt -> excluirCadastro((SdIndiceProjecao001) nav.selectedItem.get()));
        nav.btnEditRegister(evt -> editarCadastro((SdIndiceProjecao001) nav.selectedItem.get()));
        nav.btnSave(evt -> salvarCadastro((SdIndiceProjecao001) nav.selectedItem.get()));
        nav.btnCancel(evt -> cancelarCadastro());
        nav.selectedItem.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                carregaDados((SdIndiceProjecao001) newValue);
            }
        });
    });
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Declaração: Field">
    private final FormFieldSingleFind<Colecao> colecaoField = FormFieldSingleFind.create(Colecao.class, field -> {
        field.title("Coleção");
        field.width(300.0);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldSingleFind<Marca> marcaField = FormFieldSingleFind.create(Marca.class, field -> {
        field.title("Marca");
        field.width(210.0);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldToggle porMarcaField = FormFieldToggle.create(field -> {
        field.title("Por Marca");
        field.value.set(false);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldToggle porLinhaField = FormFieldToggle.create(field -> {
        field.title("Por Linha");
        field.value.set(false);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldToggleSingle porFixoField = FormFieldToggleSingle.create(field -> {
        field.title("");
        field.value.set(false);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldToggleSingle porMetaField = FormFieldToggleSingle.create(field -> {
        field.title("");
        field.value.set(false);
        field.editable.bind(navegation.inEdition);
    });
    private final FormFieldText indiceFixoField = FormFieldText.create(field -> {
        field.title("Índice");
        field.mask(FormFieldText.Mask.DOUBLE);
        field.editable.bind(navegation.inEdition.and(porFixoField.value));
    });
    private final FormFieldText indiceMetaField = FormFieldText.create(field -> {
        field.title("Meta");
        field.mask(FormFieldText.Mask.DOUBLE);
        field.editable.bind(navegation.inEdition.and(porMetaField.value));
    });
    private final FormFieldComboBox<Linha> linhaField = FormFieldComboBox.create(Linha.class, field -> {
        field.title("Linha");
        field.editable.bind(navegation.inEdition.and(porMetaField.value));
        field.items.bind(linhas);
    });
    // </editor-fold>
    
    public IndiceProjecaoView() {
        super("Índice Projeção", ImageUtils.getImage(ImageUtils.Icon.INDICE_PROJECAO), new String[]{"Listagem", "Manutenção"});
        initListagem();
        initManutencao();
    }
    
    private void initListagem() {
        procurarIndices("", "");
        listagemTab.getChildren().add(FormBox.create(boxHeader -> {
            boxHeader.horizontal();
            boxHeader.add(FormTitledPane.create(filter -> {
                filter.filter();
                filter.add(FormBox.create(boxFieldsFilter -> {
                    boxFieldsFilter.vertical();
                    boxFieldsFilter.add(marcaFilterField.build(), colecaoFilterField.build());
                }));
                filter.find.setOnAction(evt -> {
                    String marca = marcaFilterField.textValue.getValue() != null ? marcaFilterField.textValue.getValue() : "";
                    String colecao = colecaoFilterField.textValue.getValue() != null ? colecaoFilterField.textValue.getValue() : "";
                    procurarIndices(marca, colecao);
                });
                filter.clean.setOnAction(evt -> {
                    marcaFilterField.clear();
                    colecaoFilterField.clear();
                });
            }));
        }));
        listagemTab.getChildren().add(tableListagem.build());
    }
    
    private void initManutencao() {
        porMarcaField.value.addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                porLinhaField.value.set(false);
                porFixoField.value.set(false);
                porMetaField.value.set(false);
            }
        });
        porLinhaField.value.addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                porMarcaField.value.set(false);
                porFixoField.value.set(false);
                porMetaField.value.set(false);
            }
        });
        porFixoField.value.addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                porLinhaField.value.set(false);
                porMarcaField.value.set(false);
                porMetaField.value.set(false);
            }
        });
        porMetaField.value.addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                if (marcaField.value.get().getCodigo() == null) {
                    MessageBox.create(message -> {
                        message.message("Você precisa definir a marca antes.");
                        message.type(MessageBox.TypeMessageBox.IMPORTANT);
                        message.showAndWait();
                    });
                    porMetaField.value.set(false);
                } else {
                    carregarLinhasMarcas(marcaField.value.get().getCodigo());
                    porLinhaField.value.set(false);
                    porFixoField.value.set(false);
                    porMarcaField.value.set(false);
                }
            }
        });
        
        manutencaoTab.getChildren().add(navegation);
        manutencaoTab.getChildren().add(FormSection.create(section -> {
            section.title("Dados Gerais");
            section.vertical();
            section.add(FormBox.create(box -> {
                box.horizontal();
                box.add(colecaoField.build(), marcaField.build());
            }));
        }));
        manutencaoTab.getChildren().add(FormSection.create(section -> {
            section.horizontal();
            section.add(FormBox.create(box -> {
                box.vertical();
                box.add(porMarcaField.build(), porLinhaField.build());
            }));
            section.add(FormBox.create(box -> {
                box.vertical();
                box.title("Por Fixo");
                box.add(porFixoField.build(), indiceFixoField.build());
            }));
            section.add(FormBox.create(box -> {
                box.vertical();
                box.title("Por Meta");
                box.add(porMetaField.build(),
                        FormBox.create(box1 -> {
                            box1.horizontal();
                            box1.add(indiceMetaField.build(), linhaField.build());
                        }));
            }));
        }));
    }
    
    private void novoCadastro() {
        navegation.inEdition.set(true);
        navegation.selectedItem.set(new SdIndiceProjecao001());
        
        colecaoField.clear();
        marcaField.clear();
        porMarcaField.value.set(false);
        porLinhaField.value.set(false);
        porMetaField.value.set(false);
        porFixoField.value.set(false);
        indiceFixoField.clear();
        indiceMetaField.clear();
        linhas.clear();
    }
    
    private void excluirCadastro(SdIndiceProjecao001 item) {
        if ((Boolean) QuestionBox.build(Boolean.class, message -> {
            message.message("Você deseja excluir o cadastro?");
            message.showAndWait();
        }).value.get()) {
            new FluentDao().delete(item);
            indices.remove(item);
            cancelarCadastro();
            MessageBox.create(message -> {
                message.message("Cadastro excluído com sucesso.");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
        }
    }
    
    private void abrirCadastro(SdIndiceProjecao001 item) {
        navegation.selectedItem.set(item);
        tabs.getSelectionModel().select(1);
        carregaDados(item);
    }
    
    private void carregaDados(SdIndiceProjecao001 item) {
        if (item != null)
            if (item.getId() != null) {
                colecaoField.setDefaultCode(item.getId().getColecao().getCodigo());
                marcaField.setDefaultCode(item.getId().getMarca().getCodigo());
                porMarcaField.value.set(item.isByMarca());
                porLinhaField.value.set(item.isByLinha());
                porFixoField.value.set(item.getByFixo().compareTo(BigDecimal.ZERO) > 0);
                indiceFixoField.value.set(StringUtils.toDecimalFormat(item.getByFixo().doubleValue(), 4));
                porMetaField.value.set(item.getByMeta().compareTo(BigDecimal.ZERO) > 0);
                indiceMetaField.value.set(StringUtils.toDecimalFormat(item.getByMeta().doubleValue(),2));
                if (porMetaField.value.get()) {
                    Linha linha = ((List<Linha>) new FluentDao().selectFrom(Linha.class).where(it -> it.equal("codigo", item.getLinhaMeta())).resultList()).stream().findFirst().get();
                    linhaField.select(linha);
                }
            }
    }
    
    private void editarCadastro(SdIndiceProjecao001 item) {
        navegation.inEdition.set(true);
        abrirCadastro(item);
        MessageBox.create(message -> {
            message.message("Ativado modo edição");
            message.type(MessageBox.TypeMessageBox.WARNING);
            message.position(Pos.TOP_RIGHT);
            message.notification();
        });
    }
    
    private void salvarCadastro(SdIndiceProjecao001 item) {
        item.setByLinha(porLinhaField.value.getValue());
        item.setByMarca(porMarcaField.value.getValue());
        item.setByFixo(new BigDecimal((porFixoField.value.get() ? StringUtils.unformatDecimal(indiceFixoField.value.get()) : "0")));
        item.setByMeta(new BigDecimal((porMetaField.value.get() ? StringUtils.unformatDecimal(indiceMetaField.value.get()) : "0")));
        item.setLinhaMeta(porMetaField.value.get() ? linhaField.value.get().getGrpIndice() : null);
        if (item.getId() == null) {
            item.setId(new SdIndiceProjecao001PK(colecaoField.value.get(), marcaField.value.get()));
            try {
                new FluentDao().persist(item);
                indices.add(item);
                MessageBox.create(message -> {
                    message.message("Cadastro salvo com sucesso.");
                    message.type(MessageBox.TypeMessageBox.CONFIRM);
                    message.position(Pos.TOP_RIGHT);
                    message.notification();
                });
            } catch (SQLException e) {
                e.printStackTrace();
                ExceptionBox.build(message -> {
                    message.exception(e);
                    message.showAndWait();
                });
            }
        } else {
            new FluentDao().merge(item);
            MessageBox.create(message -> {
                message.message("Cadastro alterado com sucesso.");
                message.type(MessageBox.TypeMessageBox.CONFIRM);
                message.position(Pos.TOP_RIGHT);
                message.notification();
            });
        }
        navegation.inEdition.set(false);
    }
    
    private void cancelarCadastro() {
        navegation.inEdition.set(false);
        navegation.selectedItem.set(null);
        
        colecaoField.clear();
        marcaField.clear();
        porMarcaField.value.set(false);
        porLinhaField.value.set(false);
        porMetaField.value.set(false);
        porFixoField.value.set(false);
        indiceFixoField.clear();
        indiceMetaField.clear();
        linhas.clear();
        
        tableListagem.selectItem(0);
    }
}
