package sysdeliz2.controllers.cadastros

import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.scene.control.TabPane
import javafx.scene.control.TableView
import sysdeliz2.dao.FluentDao
import sysdeliz2.dao.fluentDao.TipoExpressao
import sysdeliz2.models.ti.*
import sysdeliz2.models.ti.entidade.EntidadeBean
import sysdeliz2.models.ti.entidade.EntidadeBeanModel
import tornadofx.*

/**
 *
 * @author lima.joao
 * @since 21/01/2020 11:34
 */
class SceneCadastroEntidadeController : Controller() {

    val lstRegistros: ObservableList<EntidadeBean> = FXCollections.observableArrayList<EntidadeBean>()
    val recordCount = SimpleStringProperty(this, "record_count", "Mostrando X registros")

    val model = EntidadeBeanModel(EntidadeBean())

    val modelAtividade = AtividadeBeanModel(AtividadeBean())
    val modelSitCli = SitCliBeanModel(SitCliBean())
    val modelGrupoCli = GrupoCliBeanModel(GrupoCliBean())

    val inInsertOrEdit = SimpleBooleanProperty(this, "in_insert_or_edit", false)

    val inputFiltroCodigo = SimpleStringProperty(this, "filtro_codigo", "")
    val inputFiltroRazaoSocial = SimpleStringProperty(this, "filtro_razao_social", "")

    val inputFiltroTipoEntidadeCliente = SimpleBooleanProperty(this, "filtro_tipo_entidade_cliente", true)
    val inputFiltroTipoEntidadeFornecedor = SimpleBooleanProperty(this, "filtro_tipo_entidade_fornecedor", false)
    val inputFiltroTipoEntidadeTerceiro = SimpleBooleanProperty(this, "filtro_tipo_entidade_terceiro", false)

    val inputFiltroCidade = SimpleStringProperty(this, "filtro_cidade", "")
    val inputFiltroEstado = SimpleStringProperty(this, "filtro_estado", "")

    val inputFiltroTipoCliente = SimpleStringProperty(this, "filtro_tipo_cliente", "")
    val inputFiltroRepresentante = SimpleStringProperty(this, "filtro_representante", "")

    val inputFiltroAtivos = SimpleBooleanProperty(this, "filtro_ativos", true)
    val inputFiltroBloqueados = SimpleBooleanProperty(this, "filtro_bloqueados", false)

    lateinit var tabMainPane: TabPane
    lateinit var tableViewRegistros: TableView<EntidadeBean>

    fun addRegister() {
        inInsertOrEdit.value = true
        tabMainPane.selectionModel.select(1)
    }

    fun findWithFilter() {
        val list = FluentDao()
                .selectFrom(EntidadeBean::class.java)
                .where { expressions ->
                    var entidade = ""

                    if(inputFiltroTipoEntidadeCliente.value) {
                        entidade += "C"
                    }

                    if(inputFiltroTipoEntidadeFornecedor.value) {
                        entidade += "F"
                    }

                    if(inputFiltroTipoEntidadeTerceiro.value) {
                        entidade += "T"
                    }

                    expressions
                            .equal("codCli", inputFiltroCodigo.value) { !inputFiltroCodigo.value.isNullOrEmpty() }
                            .equal("cep.cidade.nome", inputFiltroCidade.value.toUpperCase()) { !inputFiltroCidade.value.isNullOrEmpty() }
                            .equal("cep.cidade.siglaUf", inputFiltroEstado.value.toUpperCase()) { !inputFiltroEstado.value.isNullOrEmpty() }
                            .like("nome", inputFiltroRazaoSocial.get().toUpperCase()){inputFiltroRazaoSocial.get() != null && inputFiltroRazaoSocial.get().isNotEmpty()}
                            .equal("ativo", if(inputFiltroAtivos.value) "S" else "N")
                            .equal("bloqueio", if(inputFiltroBloqueados.value) "N" else "S")
                            .equal("tipoEntidade", entidade, TipoExpressao.AND, false)
                }.resultList<EntidadeBean>()

        this.lstRegistros.setAll(list)
        runLater { this.recordCount.set("Mostrando ${list?.size ?: 0} registros") }
    }

    fun firstRegister() {
        tableViewRegistros.selectionModel.selectFirst()
    }

    fun previousRegister() {
        tableViewRegistros.selectionModel.selectPrevious()
    }

    fun nextRegister() {
        tableViewRegistros.selectionModel.selectNext()
    }

    fun lastRegister() {
        tableViewRegistros.selectionModel.selectLast()
    }

    fun editRegister() {
        inInsertOrEdit.value = true
    }

    fun deleteRegister() {

    }

    fun saveRegister() {
        inInsertOrEdit.value = false
    }

    fun cancelRegister() {
        model.rollback()
        inInsertOrEdit.value = false
    }

    fun voltar() {
        if(model.isDirty){
            information("Registro em edição", "Caso voce prossiga todas as alterações serão perdidas"){
                cancelRegister()
            }
        }
        tabMainPane.selectionModel.selectFirst()
    }

    fun buscaDadosReceitaWS() {
        // TODO não implementado
    }

    fun buscaDadosEntregaReceitaWS() {
        // TODO não implementado
    }


    fun findMotivo() {

    }

    fun findAtividade() {

    }

    fun findSituacao() {

    }

    fun findGrupo() {

    }

    fun findPais() {

    }

    fun findCep() {

    }
}