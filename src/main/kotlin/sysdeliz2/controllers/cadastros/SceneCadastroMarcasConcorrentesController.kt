package sysdeliz2.controllers.cadastros

import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.scene.control.TabPane
import javafx.scene.control.TableView
import javafx.scene.control.TextField
import sysdeliz2.daos.generics.implementation.GenericDaoImpl
import sysdeliz2.daos.generics.interfaces.GenericDao
import sysdeliz2.models.sysdeliz.SdMarcaConcorrenteBean
import sysdeliz2.models.sysdeliz.SdMarcaConcorrenteModel
import sysdeliz2.models.ti.Marca
import tornadofx.*

/**
 *
 * @author lima.joao
 * @since 13/01/2020 11:16
 */
class SceneCadastroMarcasConcorrentesController : Controller() {

    val inputFiltroAtivo = SimpleStringProperty(this, "filtro_ativo")
    val inputFiltroDefault = SimpleStringProperty(this, "filtro_default")

    val inputMarcaPropria = SimpleStringProperty(this, "marca_propria")

    private val dao: GenericDao<SdMarcaConcorrenteBean> = GenericDaoImpl(SdMarcaConcorrenteBean::class.java)

    val lstRegistros: ObservableList<SdMarcaConcorrenteBean> = FXCollections.observableArrayList<SdMarcaConcorrenteBean>()
    val recordCount = SimpleStringProperty(this, "record_count", "Mostrando X registros")

    val model = SdMarcaConcorrenteModel(SdMarcaConcorrenteBean())
    lateinit var tabMainPane: TabPane
    lateinit var nomeTextField: TextField
    lateinit var tableViewRegistros: TableView<SdMarcaConcorrenteBean>

    fun addRegister() {
        model.item = SdMarcaConcorrenteBean()
        model.item.ativo = "S"

        tabMainPane.selectionModel.select(1)

        nomeTextField.requestFocus()
        nomeTextField.isEditable = true
    }

    fun addRegister2() {
        // TODO implementar
    }

    fun editRegister() {

    }

    fun deleteRegister() {

    }

    fun saveRegister() {

    }

    fun cancelRegister() {

    }

    fun voltar() {

    }

    fun findDefault() {
        dao.initCriteria()

        if(inputFiltroAtivo.get() != "Todos") {
            dao.addPredicateEq("ativo", if (inputFiltroAtivo.get() == "Ativo") "S" else "N")
        }

        if(inputFiltroDefault.get() != null && inputFiltroDefault.get().isNotEmpty()) {
            dao.addPredicateLike("nome", inputFiltroDefault.get().toUpperCase())
        }

        val list = dao.loadListByPredicate()
        this.lstRegistros.setAll(list)
        this.recordCount.set("Mostrando ${list.size} registros")
    }

    fun firstRegister() {
        tableViewRegistros.selectionModel.selectFirst()
    }

    fun previousRegister() {
        tableViewRegistros.selectionModel.selectPrevious()
    }

    fun nextRegister() {
        tableViewRegistros.selectionModel.selectNext()
    }

    fun lastRegister() {
        tableViewRegistros.selectionModel.selectLast()
    }

    fun consultaMarcaPropria() {


        //inputMarcaPropria.set("")
    }

}