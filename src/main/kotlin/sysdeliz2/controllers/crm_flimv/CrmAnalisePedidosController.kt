package sysdeliz2.controllers.crm_flimv

import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import sysdeliz2.dao.FluentDao
import sysdeliz2.daos.generics.implementation.GenericDaoImpl
import sysdeliz2.daos.generics.interfaces.GenericDao
import sysdeliz2.models.view.VSdCrmPedidosMarcaColecao
import sysdeliz2.models.view.VSdCrmPedidosMarcaColecaoModel
import tornadofx.*
import java.time.LocalDate

class CrmAnalisePedidosController : Controller() {

    val registros: ObservableList<VSdCrmPedidosMarcaColecao> = FXCollections.observableArrayList<VSdCrmPedidosMarcaColecao>()

    val recordCount = SimpleStringProperty(this, "record_count", "Mostrando X registros")

    val inputFiltroMarca = SimpleStringProperty(this, "filtro_marca")
    val inputFiltroExibirApenas = SimpleStringProperty(this, "filtro_exibir_apenas")
    val inputFiltroColecao = SimpleStringProperty(this, "filtro_colecao")
    val inputFiltroEmissaoIni = SimpleObjectProperty<LocalDate>(this, "filtro_emissao_ini")
    val inputFiltroEmissaoFim = SimpleObjectProperty<LocalDate>(this, "filtro_emissao_fim")
    val inputFiltroCliente = SimpleStringProperty(this, "filtro_Cliente", "")

    val model = VSdCrmPedidosMarcaColecaoModel(VSdCrmPedidosMarcaColecao())

    fun loadPedidos() {
        val regs = FluentDao()
                .selectFrom(VSdCrmPedidosMarcaColecao::class.java)
                .where { expressions ->
                    expressions
                            .equal("marca", getMarca())
                            .equal("colecao", inputFiltroColecao.value) { !inputFiltroColecao.value.isNullOrBlank() }
                            .between("dtEmissao", inputFiltroEmissaoIni.value, inputFiltroEmissaoFim.value) { inputFiltroEmissaoIni.value != null }
                            .like("nome", inputFiltroCliente.value) { !inputFiltroCliente.value.isNullOrBlank() }

                    when (inputFiltroExibirApenas.value) {
                        "Com Cancelamento" -> expressions.greaterThan("qtdCancelado", 0)
                        "Com Canc. Parcial" -> {
                            expressions
                                    .greaterThan("qtdCancelado", 0)
                                    .greaterThan("qtdTotal", 0)
                        }
                        "Com Canc. Integral" -> {
                            expressions
                                    .greaterThan("qtdCancelado", 0)
                                    .equal("qtdTotal", 0)
                        }
                        else -> {
                        }
                    }
                }.resultList<VSdCrmPedidosMarcaColecao>()
        runLater {
            recordCount.value = "Mostrando ${regs!!.size} registros"
        }
        registros.setAll(regs)

    }


    private fun getMarca(): String {
        return when (inputFiltroMarca.get() == "DLZ") {
            true -> "D"
            false -> "F"
        }
    }
}