package sysdeliz2.controllers.ecomerce

import sysdeliz.apis.web.convertr.data.enums.IdentificadorLoja
import sysdeliz.apis.web.convertr.data.models.images.Image
import sysdeliz.apis.web.convertr.data.models.products.GroupPrice
import sysdeliz.apis.web.convertr.data.models.products.Product
import sysdeliz.apis.web.convertr.data.models.stocks.Stock
import sysdeliz.apis.web.convertr.data.models.warehouses.Warehouse
import sysdeliz.apis.web.convertr.services.images.ImagesWebclient
import sysdeliz.apis.web.convertr.services.products.ProductWebclient
import sysdeliz.apis.web.convertr.services.stocks.StockWebclient
import sysdeliz.apis.web.convertr.services.warehouses.WarehouseWebclient
import sysdeliz.apis.web.convertr.utils.Configurations
import sysdeliz2.dao.FluentDao
import sysdeliz2.dao.OrderType
import sysdeliz2.models.ti.TabPrzBean
import sysdeliz2.models.view.VSdProdutoMagentoB2B
import sysdeliz2.utils.CamposProdutosEnviar
import sysdeliz2.utils.enums.StatusProdutoMagento
import sysdeliz2.utils.hibernate.JPAUtils
import tornadofx.*
import java.io.File
import java.sql.SQLException
import java.util.*
import java.util.function.Predicate
import java.util.stream.Collectors
import kotlin.collections.ArrayList

/**
 *
 * @author lima.joao
 * @since 29/11/2019 08:21
 */
class ManProdMagentoB2BController : ManProdMagentoBasicController<VSdProdutoMagentoB2B>() {

    fun carregaProdutos(identificadorLoja: IdentificadorLoja) {
        JPAUtils.clearEntitys(produtos)
        val lista = FluentDao()
                .selectFrom(VSdProdutoMagentoB2B::class.java)
                .where {
                    val list = ArrayList<String>(when(inputFiltroReferencia.value) {
                        "" ->  ArrayList<String>()
                        null -> ArrayList<String>()
                            else -> inputFiltroReferencia.value.split(",")})
                    it
                            .equal("marca", inputFiltroMarca.value) { inputFiltroMarca.value != "" }
                            .equal("colecao", inputFiltroColecao.value) { inputFiltroColecao.value ?: "" != "" }
                            .isIn("codigo", list.toArray()) { list.isNotEmpty() }
                            .equal("tabelaPreco", identificadorLoja.value)
                }.orderBy("codigo", OrderType.ASC)
                .resultList<VSdProdutoMagentoB2B>()

        lista?.let {
            try {
                // remove tudo da lista primeiro
                produtos.clear()

                // Reseta o status da lista
                lista.forEach {
                    if(it.selected){
                        it.selected = false
                    }
                }

                //if (exibirOsSemEstoque.value) {
                    produtos.setAll(lista)
//                } else {
//                    lista.forEach { vSdProdutoMagento ->
//                        var exibir = false
//                        vSdProdutoMagento.estoque!!.forEach {
//                            if( it.qtd!! > 0 ){
//                                exibir = true
//                            }
//                        }
//
//                        if (exibir) {
//                            produtos.add(vSdProdutoMagento)
//                        }
//                    }
//                }
                this.recordCount.set("Mostrando ${produtos.size} produtos | Selecionado $qtdSelecionada produdtos")
            } catch (e: SQLException) {
                warning(e.message ?: "")
            }
        }
    }

    fun atualizaDadosSdProdutos001(vSdProdutoMagento: VSdProdutoMagentoB2B) {
        var comandos = ""

        vSdProdutoMagento.metaTitle?.let { comandos += " ,META_TITLE = '$it'" }
        vSdProdutoMagento.metaKeyword?.let { comandos += " ,META_KEYWORD = '$it'" }
        vSdProdutoMagento.metaDescription?.let { comandos += " ,META_DESCRIPTION = '$it'" }
        vSdProdutoMagento.name?.let { comandos += " ,DESCRIPTION = '$it'" }
        vSdProdutoMagento.description?.let { comandos += " ,LONG_DESCRIPTION = '$it'" }
        vSdProdutoMagento.shortDescription?.let { comandos += " ,SHORT_DESCRIPTION = '$it'" }
        vSdProdutoMagento.statusProcessamento?.let { comandos += " ,STATUS_B2B =  '$it'" }
        vSdProdutoMagento.tabelaMedida?.let { comandos += " ,TABELA_MEDIDA = '$it'"}

        val sql = "UPDATE SD_PRODUTO_001 SET LOJA_STATUS = '${vSdProdutoMagento.status}' $comandos WHERE CODIGO = '${vSdProdutoMagento.codigo}'"

        FluentDao()
                .runNativeQueryUpdate(sql)
    }

    fun enviarProdutosParaLoja(): Int {

        val products = ArrayList<Product>()

        val camposProdutosEnviar = CamposProdutosEnviar.Builder()
                .enviarAtributosAdicionais(checkEnviarAditionalAttributes.value)
                .enviarDescricaoCurta(checkEnviarShortDescription.value)
                .enviarDescricao(checkEnviarDescription.value)
                .enviarDescricaoSEO(checkEnviarMetaDescription.value)
                .enviarCategorias(checkEnviarCategories.value)
                .enviarPrecoEspecial(checkEnviarSpecialPrice.value)
                .enviarPeso(checkEnviarWeight.value)
                .enviarStatus(checkEnviarStatus.value)
                .enviarTituloSEO(checkEnviarMetaTitle.value)
                .enviarPalavasChaves(checkEnviarMetaKeyword.value)
                .enviarVariacoes(checkEnviarVariations.value)
                .enviarLinkrodutos(checkEnviarLinkProducts.value)
                .enviarGroupPrice(true)
                .build()

        for (vSdProdutoMagento in produtos.stream().filter(Predicate { t -> t.selected }).collect(Collectors.toList())) {
            if (vSdProdutoMagento.canSendProduto()) {
                products.add(vSdProdutoMagento.toProductMagento(camposProdutosEnviar))
                vSdProdutoMagento.statusProcessamento = StatusProdutoMagento.ENVIADO.value
            }
        }

        // Só executa o envio se existirem produtos
        if (products.isNotEmpty()) {
            ProductWebclient
                    .configure(IdentificadorLoja.B2B, emProducao.value)
                    .post(products)
            atualizaStatusProcessamento()
        }

        return products.size
    }

    fun consultaStatusEnvioProdutos(): Int {
        val response = ProductWebclient
                .configure(IdentificadorLoja.B2B, emProducao.value)
                .get()

        var processados = 0

        response?.let {
            it.products?.let { fila ->
                for (vSdProdutoMagento in produtos.filtered { t -> t.selected }) {
                    if (fila.contains(vSdProdutoMagento.codigo)) {
                        vSdProdutoMagento.statusProcessamento = StatusProdutoMagento.ENVIADO.value
                    } else {
                        if(vSdProdutoMagento.statusProcessamento == StatusProdutoMagento.ENVIADO.value) {
                            vSdProdutoMagento.statusProcessamento = StatusProdutoMagento.PROCESSADO.value
                            processados++
                        }
                    }
                }
            }
        }
        atualizaStatusProcessamento()
        return processados
    }

    fun consultaStatusEnvioEstoque(): Int {
        val response = StockWebclient
                .configure(IdentificadorLoja.B2B, emProducao.value)
                .get()

        var count = 0
        response?.let {
            it.stocks?.let { fila ->
                for (vSdProdutoMagento in produtos.filtered { t -> t.selected }) {
                    vSdProdutoMagento.statusProcessamento = if (fila.isEmpty()) {
                        count++
                        StatusProdutoMagento.PRODUTO_OK.value
                    } else {
                        if (fila.find { s -> s.startsWith(vSdProdutoMagento.codigo ?: "SKU123456") }?.isNotEmpty() == true) {
                            StatusProdutoMagento.ENVIO_ESTOQUE.value
                        } else {
                            if(vSdProdutoMagento.statusProcessamento == StatusProdutoMagento.ENVIO_ESTOQUE.value){
                                count++
                                StatusProdutoMagento.PRODUTO_OK.value
                            } else {
                                vSdProdutoMagento.statusProcessamento
                            }
                        }
                    }
                }
            }
        }
        atualizaStatusProcessamento()
        return count
    }

    fun consultaStatusEnvioFotos(): Int {
        val response = ImagesWebclient
                .configure(IdentificadorLoja.B2B, emProducao.value)
                .get()

        var count = 0
        response?.let {
            it.images?.let { fila ->
                for (vSdProdutoMagento in produtos.filtered { t -> t.selected }) {
                    vSdProdutoMagento.statusProcessamento = if (fila.isEmpty()) {
                        count++
                        StatusProdutoMagento.PRODUTO_OK.value
                    } else {
                        if (fila.find { s -> s.startsWith(vSdProdutoMagento.codigo ?: "SKU123456") }?.isNotEmpty() == true) {
                            StatusProdutoMagento.ENVIO_FOTO.value
                        } else {
                            if(vSdProdutoMagento.statusProcessamento == StatusProdutoMagento.ENVIO_FOTO.value){
                                count++
                                StatusProdutoMagento.PRODUTO_OK.value
                            } else {
                                vSdProdutoMagento.statusProcessamento
                            }
                        }
                    }
                }
            }
        }
        atualizaStatusProcessamento()
        return count
    }

    fun enviaFotos() {

        val dados = ArrayList<Image>()

        for (item in produtos.stream().filter { t -> t.selected && (t.statusProcessamento == StatusProdutoMagento.PRODUTO_OK.value || t.statusProcessamento == StatusProdutoMagento.PROCESSADO.value || t.statusProcessamento == StatusProdutoMagento.ENVIO_ESTOQUE.value) }.collect(Collectors.toList())) {
            if(checkZerarFotos.value){
                item.statusProcessamento = StatusProdutoMagento.ENVIO_FOTO.value
                item.codigo?.let { Image(it, ArrayList<String>()) }?.let { dados.add(it) }
            } else {
                if (item.marca == "D" || item.marca == "F") {

                    val base = if (item.marca == "D") {
                        Configurations.URL_PHOTOS_DLZ
                    } else {
                        Configurations.URL_PHOTOS_FLOR
                    }

                    val pathRemoto = Configurations.getPathPhotos(item.marca == "D", true)

                    val files = File(base)

                    if (files.isDirectory) {

                        files.listFiles()?.let {
                            if (item.selected) {
                                val img = Image(item.codigo ?: "SKU123456")

                                it.filter { predicate -> predicate.isFile && predicate.path.contains(item.codigo.toString()) }.forEach { f ->
                                    img.images?.add(pathRemoto + f.name)
                                }

                                if (img.images?.isNotEmpty() == true) {
                                    dados.add(img)
                                }
                            }
                        }

                        item.statusProcessamento = StatusProdutoMagento.ENVIO_FOTO.value
                    }
                }
            }
        }

        if (dados.isNotEmpty()) {
            ImagesWebclient
                    .configure(getIdentificacaoConvertr(), emProducao.value)
                    .post(dados)
            atualizaStatusProcessamento()
        }

    }

    fun enviaEstoqueParaLoja(): Int {
        val stocks = ArrayList<Stock>()
        var count = 0

        val response = WarehouseWebclient
                .configure(IdentificadorLoja.B2B, emProducao.value)
                .get()

        val wares = FluentDao()
                .selectFrom(TabPrzBean::class.java)
                .where {
                    it
                            .equal("tipo", "E")
                }
                .orderBy("prazo")
                .resultList<TabPrzBean>()

        response?.let { responseLamb ->

            wares!!.forEach { prz ->
                if(responseLamb.warehouses!!.filter { predicate -> predicate.name == prz.descricao }.isNullOrEmpty()){
                    WarehouseWebclient
                            .configure(IdentificadorLoja.B2B, emProducao.value)
                            .post(Warehouse(prz.prazo, prz.descricao))
                }
            }
        }

        for (vSdProdutoMagento in produtos.stream().filter(Predicate { t -> t.selected }).collect(Collectors.toList())) {
            if (vSdProdutoMagento.canSendEstoque()) {
                vSdProdutoMagento.estoque?.let {estoques ->
                    for (item in estoques) {
                        stocks.add(item.toStock())
                        count++
                    }
                }
                vSdProdutoMagento.statusProcessamento = StatusProdutoMagento.ENVIO_ESTOQUE.value
            }
        }

        if (stocks.isNotEmpty()) {
            StockWebclient
                    .configure(IdentificadorLoja.B2B, emProducao.value)
                    .post(stocks)

            atualizaStatusProcessamento()
        }

        return count
    }

    fun atualizaStatusProcessamento() {
        for (vSdProdutoMagento in produtos.stream().filter { t -> t.selected }.collect(Collectors.toList())) {
            FluentDao().runNativeQueryUpdate("UPDATE SD_PRODUTO_001 SET STATUS_B2B = '${vSdProdutoMagento.statusProcessamento}' WHERE CODIGO = '${vSdProdutoMagento.codigo}'")
        }
    }

}