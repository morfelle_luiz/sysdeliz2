package sysdeliz2.controllers.ecomerce

import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.scene.control.TableView
import sysdeliz.apis.web.convertr.data.enums.IdentificadorLoja
import sysdeliz.apis.web.convertr.data.models.products.Product
import sysdeliz.apis.web.convertr.services.products.ProductWebclient
import sysdeliz2.models.view.VSdEstoqueLojaVirtual
import sysdeliz2.dao.FluentDao
import sysdeliz2.dao.OrderType
import sysdeliz2.models.utils.IProdutoMagentoUtils
import sysdeliz2.models.utils.Selecionavel
import sysdeliz2.models.view.VSdProdutoMagentoB2B
import sysdeliz2.models.view.VSdProdutoMagento
import sysdeliz2.utils.CamposProdutosEnviar
import sysdeliz2.utils.enums.StatusProdutoMagento
import tornadofx.*
import java.sql.SQLException

/**
 *
 * @author lima.joao
 * @since 29/11/2019 08:21
 */
abstract class ManProdMagentoBasicController<T>: Controller() {

    val produtos: ObservableList<T> = FXCollections.observableArrayList()

    val recordCount = SimpleStringProperty(this, "record_count", "Mostrando 0 registros")

    val exibirOsSemEstoque = SimpleBooleanProperty(this, "exibir_sem_estoque", false)

    val inputFiltroMarca = SimpleStringProperty(this, "filtro_marca")
    val inputFiltroColecao = SimpleStringProperty(this, "filtro_colecao")
    val inputFiltroReferencia = SimpleStringProperty(this, "filtro_referencia")
    val selectItensTable = SimpleStringProperty(this, "selected_itens")

    // ********************************************************
    // Campos a serem enviados para loja virtual - Obrigatórios
    val checkEnviarSKU = SimpleBooleanProperty(this, "check_enviar_sku", true)
    val checkEnviarName = SimpleBooleanProperty(this, "check_enviar_name", true)
    val checkEnviarPrice = SimpleBooleanProperty(this, "check_enviar_price", true)

    // Campos a serem enviados para loja virtual - Opcionais
    val checkEnviarShortDescription = SimpleBooleanProperty(this, "check_enviar_short_description", true)
    val checkEnviarDescription = SimpleBooleanProperty(this, "check_enviar_description", true)
    val checkEnviarSpecialPrice = SimpleBooleanProperty(this, "check_enviar_special_price", false)
    val checkEnviarWeight = SimpleBooleanProperty(this, "check_enviar_weight", true)
    val checkEnviarStatus = SimpleBooleanProperty(this, "check_enviar_status", false)
    val checkEnviarMetaTitle = SimpleBooleanProperty(this, "check_enviar_meta_title", true)
    val checkEnviarMetaKeyword = SimpleBooleanProperty(this, "check_enviar_meta_keyword", true)
    val checkEnviarMetaDescription = SimpleBooleanProperty(this, "check_enviar_meta_description", true)
    val checkEnviarCategories = SimpleBooleanProperty(this, "check_enviar_categories", false)
    val checkEnviarGroupPrice = SimpleBooleanProperty(this, "check_enviar_group_price", false)
    val checkEnviarVariations = SimpleBooleanProperty(this, "check_enviar_variations", false)
    val checkEnviarAditionalAttributes = SimpleBooleanProperty(this, "check_enviar_aditional_Attributes", false)
    val checkEnviarLinkProducts = SimpleBooleanProperty(this, "check_enviar_link_products", false)
    // Zerar fotos modal
    val checkZerarFotos = SimpleBooleanProperty(this, "check_zerar_fotos", false)
    // ********************************************************

    val emProducao = SimpleBooleanProperty(this, "check_em_producao", true)

    var tabela = ""
    var qtdSelecionada = 0

    var tableProdutos: TableView<T>? = null

    fun reset() {
        produtos.clear()
        recordCount.value = "Mostrando 0 produtos"
        exibirOsSemEstoque.value = false

        inputFiltroMarca.value = ""
        inputFiltroColecao.value = ""

        // Obrigatórios
        checkEnviarSKU.value = true
        checkEnviarName.value = true
        checkEnviarPrice.value = true
        // Opcionais
        checkEnviarShortDescription.value = true
        checkEnviarDescription.value = true
        checkEnviarSpecialPrice.value = false
        checkEnviarWeight.value = true
        checkEnviarStatus.value = false
        checkEnviarMetaTitle.value = true
        checkEnviarMetaKeyword.value = true
        checkEnviarMetaDescription.value = true
        checkEnviarCategories.value = false
        checkEnviarVariations.value = false
        checkEnviarAditionalAttributes.value = false
        checkEnviarLinkProducts.value = false

        emProducao.value  = true
    }

    fun getIdentificacaoConvertr(): IdentificadorLoja {
        return IdentificadorLoja.valueOf(if (tabela == "B2B") "B2B" else "$tabela${inputFiltroMarca.value}")
    }

}