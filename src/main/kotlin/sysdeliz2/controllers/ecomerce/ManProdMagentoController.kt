package sysdeliz2.controllers.ecomerce

import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.scene.control.TableView
import sysdeliz.apis.web.convertr.data.enums.IdentificadorLoja
import sysdeliz.apis.web.convertr.data.models.images.Image
import sysdeliz.apis.web.convertr.data.models.products.Product
import sysdeliz.apis.web.convertr.data.models.stocks.Stock
import sysdeliz.apis.web.convertr.services.images.ImagesWebclient
import sysdeliz.apis.web.convertr.services.products.ProductWebclient
import sysdeliz.apis.web.convertr.services.stocks.StockWebclient
import sysdeliz.apis.web.convertr.utils.Configurations
import sysdeliz2.models.view.VSdEstoqueLojaVirtual
import sysdeliz2.models.view.VSdProdutoMagento
import sysdeliz2.dao.FluentDao
import sysdeliz2.dao.OrderType
import sysdeliz2.utils.CamposProdutosEnviar
import sysdeliz2.utils.enums.StatusProdutoMagento
import tornadofx.*
import java.io.File
import java.sql.SQLException
import java.util.stream.Collectors

/**
 *
 * @author lima.joao
 * @since 29/11/2019 08:21
 */
class ManProdMagentoController : Controller() {

    val produtosList: ObservableList<VSdProdutoMagento> = FXCollections.observableArrayList()

    val recordCount = SimpleStringProperty(this, "record_count", "Mostrando 0 registros")

    val exibirOsSemEstoque = SimpleBooleanProperty(this, "exibir_sem_estoque", false)

    val inputFiltroMarca = SimpleStringProperty(this, "filtro_marca")
    val inputFiltroColecao = SimpleStringProperty(this, "filtro_colecao")

    // ********************************************************
    // Campos a serem enviados para loja virtual - Obrigatórios
    val checkEnviarSKU = SimpleBooleanProperty(this, "check_enviar_sku", true)
    val checkEnviarName = SimpleBooleanProperty(this, "check_enviar_name", true)
    val checkEnviarPrice = SimpleBooleanProperty(this, "check_enviar_price", true)

    // Campos a serem enviados para loja virtual - Opcionais
    val checkEnviarShortDescription = SimpleBooleanProperty(this, "check_enviar_short_description", true)
    val checkEnviarDescription = SimpleBooleanProperty(this, "check_enviar_description", true)
    val checkEnviarSpecialPrice = SimpleBooleanProperty(this, "check_enviar_special_price", false)
    val checkEnviarWeight = SimpleBooleanProperty(this, "check_enviar_weight", true)
    val checkEnviarStatus = SimpleBooleanProperty(this, "check_enviar_status", false)
    val checkEnviarMetaTitle = SimpleBooleanProperty(this, "check_enviar_meta_title", true)
    val checkEnviarGroupPrice = SimpleBooleanProperty(this, "check_enviar_group_price", false)
    val checkEnviarMetaKeyword = SimpleBooleanProperty(this, "check_enviar_meta_keyword", true)
    val checkEnviarMetaDescription = SimpleBooleanProperty(this, "check_enviar_meta_description", true)
    val checkEnviarCategories = SimpleBooleanProperty(this, "check_enviar_categories", false)
    val checkEnviarVariations = SimpleBooleanProperty(this, "check_enviar_variations", false)
    val checkEnviarAditionalAttributes = SimpleBooleanProperty(this, "check_enviar_aditional_Attributes", false)
    val checkEnviarLinkProducts = SimpleBooleanProperty(this, "check_enviar_link_products", false)
    // ********************************************************

    val emProducao = SimpleBooleanProperty(this, "check_em_producao", true)

    var tabela = ""
    var qtdSelecionada = 0

    var tableProdutos: TableView<VSdProdutoMagento>? = null

    // Carrega os produtos do banco de dados
    fun carregaProdutos(tabelaEcomerce: String) {
        val lista = FluentDao()
                .selectFrom(VSdProdutoMagento::class.java)
                .where {
                    it
                            .equal("colecao", inputFiltroColecao.value) { inputFiltroColecao.value ?: "" != "" }
                            .equal("tabelaPreco", tabelaEcomerce)
                }.orderBy("sku", OrderType.ASC)
                .resultList<VSdProdutoMagento>()

        lista?.let {
            try {
                // remove tudo da lista primeiro
                produtosList.clear()

                // Reseta o status da lista
                lista.forEach {
                    if(it.isSelected){
                        it.setSelected(false)
                    }
                }

                if (exibirOsSemEstoque.value) {
                    produtosList.setAll(lista)
                } else {
                    lista.forEach { vSdProdutoMagento ->
                        val estoque = getEstoquePorProdutoCor(vSdProdutoMagento)
                        var exibir = false
                        if (!estoque.isNullOrEmpty()) {
                            estoque.forEach { vSdEstoqueLojaVirtual ->
                                if (vSdEstoqueLojaVirtual.quantidade?.toInt() ?: 0 > 0) {
                                    exibir = true
                                }
                            }
                        }
                        if (exibir) {
                            vSdProdutoMagento.posicaoEstoque = estoque
                            produtosList.add(vSdProdutoMagento)
                        }
                    }
                }
                this.recordCount.set("Mostrando ${produtosList.size} produtos | Selecionado $qtdSelecionada produdtos")
            } catch (e: SQLException) {
                warning(e.message ?: "")
            }
        }
    }

    fun atualizaDadosSdProdutos001(vSdProdutoMagento: VSdProdutoMagento) {
        var comandos = ""

        vSdProdutoMagento.metaTitle?.let { comandos += " ,META_TITLE = '$it'" }
        vSdProdutoMagento.metaKeyword?.let { comandos += " ,META_KEYWORD = '$it'" }
        vSdProdutoMagento.metaDescription?.let { comandos += " ,META_DESCRIPTION = '$it'" }
        vSdProdutoMagento.name?.let { comandos += " ,DESCRIPTION = '$it'" }
        vSdProdutoMagento.description?.let { comandos += " ,LONG_DESCRIPTION = '$it'" }
        vSdProdutoMagento.shortDescription?.let { comandos += " ,SHORT_DESCRIPTION = '$it'" }
        vSdProdutoMagento.statusProcessamento?.let { comandos += " ,STATUS_PROCESSAMENTO =  '$it'" }
        vSdProdutoMagento.tabelaMedida?.let { comandos += " ,TABELA_MEDIDA = '$it'"}

        val sql = "UPDATE SD_PRODUTO_001 SET LOJA_STATUS = '${vSdProdutoMagento.status}' $comandos WHERE CODIGO = '${vSdProdutoMagento.codigo}'"

        FluentDao()
                .runNativeQueryUpdate(sql)
    }

    fun getEstoquePorProdutoCor(vSdProdutoMagento: VSdProdutoMagento): ObservableList<VSdEstoqueLojaVirtual> {
        val retorno = FluentDao()
                .selectFrom(VSdEstoqueLojaVirtual::class.java)
                .where {
                    it
                            .equal("codigo", vSdProdutoMagento.codigo)
                            .equal("cor", vSdProdutoMagento.cor)
                }.resultList<VSdEstoqueLojaVirtual>()

        return retorno?.toObservable() ?: FXCollections.observableArrayList()
    }

    fun enviarProdutosParaLoja(): Int {

        val products = ArrayList<Product>()

        val camposProdutosEnviar = CamposProdutosEnviar.Builder()
                .enviarAtributosAdicionais(checkEnviarAditionalAttributes.value)
                .enviarDescricaoCurta(checkEnviarShortDescription.value)
                .enviarDescricao(checkEnviarDescription.value)
                .enviarDescricaoSEO(checkEnviarMetaDescription.value)
                .enviarCategorias(checkEnviarCategories.value)
                .enviarPrecoEspecial(checkEnviarSpecialPrice.value)
                .enviarPeso(checkEnviarWeight.value)
                .enviarStatus(checkEnviarStatus.value)
                .enviarGroupPrice(checkEnviarGroupPrice.value)
                .enviarTituloSEO(checkEnviarMetaTitle.value)
                .enviarPalavasChaves(checkEnviarMetaKeyword.value)
                .enviarVariacoes(checkEnviarVariations.value)
                .enviarLinkrodutos(checkEnviarLinkProducts.value)
                .build()

        for (vSdProdutoMagento in produtosList) {
            if (vSdProdutoMagento.canSendProduto()) {
                products.add(vSdProdutoMagento.toProductMagento(camposProdutosEnviar))
                vSdProdutoMagento.statusProcessamento = StatusProdutoMagento.ENVIADO.value
            }
        }

        // Só executa o envio se existirem produtos
        if (products.isNotEmpty()) {
            ProductWebclient
                    .configure(getIdentificacaoConvertr(), emProducao.value)
                    .post(products)
            atualizaStatusProcessamento()
        }

        return products.size
    }

    fun consultaStatusEnvioProdutos(): Int {

        val response = ProductWebclient
                .configure(getIdentificacaoConvertr(), emProducao.value)
                .get()

        var processados = 0

        response?.let {
            it.products?.let { fila ->
                for (vSdProdutoMagento in produtosList.filtered { t -> t.isSelected }) {
                    if (fila.contains(vSdProdutoMagento.sku)) {
                        vSdProdutoMagento.statusProcessamento = StatusProdutoMagento.ENVIADO.value
                    } else {
                        vSdProdutoMagento.statusProcessamento = StatusProdutoMagento.PROCESSADO.value
                        processados++
                    }
                }
            }
        }
        atualizaStatusProcessamento()
        return processados
    }

    fun enviaEstoqueParaLoja(): Int {

        val stocks = ArrayList<Stock>()
        var count = 0
        for (vSdProdutoMagento in produtosList) {
            if (vSdProdutoMagento.canSendEstoque()) {
                val list = getEstoquePorProdutoCor(vSdProdutoMagento)
                for (vSdEstoqueLojaVirtual in list) {
                    stocks.add(vSdProdutoMagento.toStock(vSdEstoqueLojaVirtual))
                    count++
                }
                vSdProdutoMagento.statusProcessamento = StatusProdutoMagento.ENVIO_ESTOQUE.value
            }
        }

        if (stocks.isNotEmpty()) {
            StockWebclient
                    .configure(getIdentificacaoConvertr(), emProducao.value)
                    .post(stocks)

            atualizaStatusProcessamento()
        }
        return count
    }

    fun consultaStatusEnvioEstoque(): Int {
        val response = StockWebclient
                .configure(getIdentificacaoConvertr(), emProducao.value)
                .get()

        var count = 0
        response?.let {
            it.stocks?.let { fila ->
                for (vSdProdutoMagento in produtosList.filtered { t -> t.isSelected }) {
                    vSdProdutoMagento.statusProcessamento = if (fila.isEmpty()) {
                        count++
                        StatusProdutoMagento.PRODUTO_OK.value
                    } else {
                        if (fila.find { s -> s.startsWith(vSdProdutoMagento.sku ?: "SKU123456") }?.isNotEmpty() == true) {
                            StatusProdutoMagento.ENVIO_ESTOQUE.value
                        } else {
                            count++
                            StatusProdutoMagento.PRODUTO_OK.value
                        }
                    }
                }
            }
        }
        atualizaStatusProcessamento()
        return count
    }

    fun enviaFotos() {

        val base = if (inputFiltroMarca.value == "D") {
            Configurations.URL_PHOTOS_DLZ
        } else {
            Configurations.URL_PHOTOS_FLOR
        }

        val pathRemoto = Configurations.getPathPhotos(inputFiltroMarca.value == "D", true)

        val files = File(base)

        if (files.isDirectory) {
            val dados = ArrayList<Image>()

            files.listFiles()?.let {
                for (item in produtosList) {
                    if(item.isSelected) {
                        val img = Image(item.sku ?: "SKU123456")

                        it.filter { predicate -> predicate.isFile && predicate.path.contains(item.sku.toString()) }.forEach { f ->
                            img.images?.add(pathRemoto + f.name)
                        }

                        if (img.images?.isNotEmpty() == true) {
                            dados.add(img)
                        }
                    }
                }
            }

            if (dados.isNotEmpty()) {
                ImagesWebclient
                        .configure(getIdentificacaoConvertr(), emProducao.value)
                        .post(dados)
            }
        }
    }

    fun reset() {
        produtosList.clear()
        recordCount.value = "Mostrando 0 produtos"
        exibirOsSemEstoque.value = false

        inputFiltroMarca.value = ""
        inputFiltroColecao.value = ""

        // Obrigatórios
        checkEnviarSKU.value = true
        checkEnviarName.value = true
        checkEnviarPrice.value = true
        // Opcionais
        checkEnviarShortDescription.value = true
        checkEnviarDescription.value = true
        checkEnviarSpecialPrice.value = false
        checkEnviarWeight.value = true
        checkEnviarStatus.value = false
        checkEnviarMetaTitle.value = true
        checkEnviarMetaKeyword.value = true
        checkEnviarMetaDescription.value = true
        checkEnviarCategories.value = false
        checkEnviarVariations.value = false
        checkEnviarAditionalAttributes.value = false
        checkEnviarLinkProducts.value = false

        emProducao.value  = true
    }

    private fun atualizaStatusProcessamento() {
        for (vSdProdutoMagento in produtosList.stream().filter { t -> t.isSelected }.collect(Collectors.toList())) {
            FluentDao().runNativeQueryUpdate("UPDATE SD_PRODUTO_001 SET STATUS_PROCESSAMENTO = '${vSdProdutoMagento.statusProcessamento}' WHERE CODIGO = '${vSdProdutoMagento.codigo}'")
        }
    }

    private fun getIdentificacaoConvertr(): IdentificadorLoja {
        return IdentificadorLoja.valueOf(if (tabela == "B2B") "B2B" else "$tabela${inputFiltroMarca.value}")
    }

}