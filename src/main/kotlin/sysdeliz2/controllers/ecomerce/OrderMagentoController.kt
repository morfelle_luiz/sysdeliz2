package sysdeliz2.controllers.ecomerce

import br.com.deliz.correios.api.rastreio.CorreiosRastreioApi
import br.com.deliz.correios.api.rastreio.enums.CorreiosEscopoResultado
import br.com.deliz.correios.api.rastreio.enums.CorreiosIdioma
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.concurrent.Task
import javafx.geometry.Pos
import javafx.util.Duration
import sysdeliz.apis.web.convertr.data.enums.IdentificadorLoja
import sysdeliz.apis.web.convertr.data.enums.PaymentMethod
import sysdeliz.apis.web.convertr.data.enums.StatusOrder
import sysdeliz.apis.web.convertr.data.models.orders.OrderStatusUpdate
import sysdeliz.apis.web.convertr.services.orders.OrdersWebclient
import sysdeliz2.controllers.ecomerce.helpers.OrderMagentoHelper
import sysdeliz2.dao.FluentDao
import sysdeliz2.dao.OrderType
import sysdeliz2.models.sysdeliz.*
import sysdeliz2.models.ti.*
import sysdeliz2.models.ti.entidade.EntidadeBean
import sysdeliz2.models.ti.entidade.addressMagentoToEndereco
import sysdeliz2.models.ti.entidade.addressMagentoToEnderecoCobranca
import sysdeliz2.models.ti.entidade.addressMagentoToEnderecoEntrega
import sysdeliz2.utils.Globals
import sysdeliz2.utils.extensions.getTipoEntidade
import sysdeliz2.utils.extensions.toFormatedCpfCnpj
import sysdeliz2.utils.extensions.toLocalDateMagento
import sysdeliz2.utils.extensions.toLocalDateTimeMagento
import sysdeliz2.utils.hibernate.JPAUtils
import sysdeliz2.utils.mails.SimpleMail
import tornadofx.*
import tornadofx.controlsfx.infoNotification
import tornadofx.controlsfx.warningNotification
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.NumberFormat
import java.time.LocalDate
import java.util.*
import kotlin.collections.ArrayList

/**
 *
 * @author lima.joao
 * @since 15/01/2020 08:12
 */

/*
  Pendente 	            pending
  Processando 	        processing
  Transporte 	        complete
  Pedido em Transporte 	complete_shipped
  Faturado 	            faturado
  Entregue        	    entregue
  Fechado 	            closed
  Cancelado 	        canceled
 */
class OrderMagentoController : Controller() {

    var pedidos: ObservableList<SdMagentoOrderBean> = FXCollections.observableArrayList()
    var produtos: ObservableList<SdMagentoOrderItemBean> = FXCollections.observableArrayList()

    var isB2B: Boolean = false

    val inputFiltroMarca = SimpleStringProperty(this, "filtro_marca")
    val inputTipoPedido = SimpleStringProperty(this, "filtro_tipo")
    val inputFiltroCliente = SimpleStringProperty(this, "filtro_cliente")
    val inputFiltroNota = SimpleStringProperty(this, "filtro_nota")

    val inputStatusCanceled = SimpleBooleanProperty(this, "filtro_status_canceled", false)
    val inputStatusPending = SimpleBooleanProperty(this, "filtro_status_pending", true)
    val inputStatusProcessing = SimpleBooleanProperty(this, "filtro_status_processing", true)
    val inputStatusComplete = SimpleBooleanProperty(this, "filtro_status_complete", true)
    val inputStatusEntregue = SimpleBooleanProperty(this, "filtro_status_entregue", false)
    val inputStatusFraude = SimpleBooleanProperty(this, "filtro_status_fraude", false)

    val model = SdMagentoOrderBeanModel(SdMagentoOrderBean())

    var updateStatusOrder: OrderStatusUpdate? = null

    private val statusBuscar = listOf(
            StatusOrder.CANCELADO, StatusOrder.FECHADO, StatusOrder.PENDENTE, StatusOrder.PROCESSANDO, StatusOrder.COMPLETE, StatusOrder.ENTREGUE
    )

    private var autoImport = true

    private var taskImport: Task<*>? = null

    fun disableThreadImport() {
        autoImport = false
        var forced = false
        confirm("Forçar parada da thread?", "Esta operação irá forçar o encerramento da thead, o que poderá ocasionar cadastro de dados incompletos.") {
            taskImport?.let {
                if (it.isRunning) {
                    forced = true
                    it.cancel()
                    warningNotification(
                            "Sincronização cancelada(Forçada)",
                            "Verifique as informação cadastradas pois pode haver dados inconsistentes.",
                            Pos.TOP_RIGHT,
                            Duration.seconds(5.0))
                }
            }
        }

        if (!forced) {
            warningNotification(
                    "Solicitado parada da sincronização.",
                    "Aguarde a thread concluir as .",
                    Pos.TOP_RIGHT,
                    Duration.seconds(5.0))

        }
    }

    fun enableThreadImport() {
        autoImport = true
        buscarPedidosOnline()
        infoNotification(
                "Sincronização Iniciada",
                "Iniciado a sincronização automática. Será executada a cada 5 minutos",
                Pos.TOP_RIGHT,
                Duration.seconds(5.0))

    }

    fun buscarPedidosOnline() {
        taskImport = runAsync {
            val marcas = arrayOf("D", "F")
            while (autoImport) {
                for (marca in marcas) {
                    for (statusOrder in statusBuscar) {
                        runAsync {

                            val lista = OrdersWebclient
                                    .configure(getIdentificacaoConvertr(marca))
                                    .get(statusOrder.value)?.orders ?: ArrayList()

                            // 1 - Verificar se o pedido já esta gravado na tabela SD_MAGENTO_ORDERS
                            // 2 - Se existir verificar se teve alteração de status e atualizar
                            // 3 - Se não existir deve gravar o pedido na lista
                            for (order in lista) {
                                var registro = FluentDao()
                                        .selectFrom(SdMagentoOrderBean::class.java)
                                        .where {
                                            it
                                                    .equal("orderId", order.orderId!!)
                                                    .equal("marca", marca)
                                        }.singleResult<SdMagentoOrderBean>()

                                //var registro = registros!!.get(0)

                                if (registro != null) {
                                    // Atualiza os dados do pedido existente e grava no banco antes de voltar
                                    if (registro.status != order.status) {
                                        registro.status = order.status
                                        registro = FluentDao().merge(registro)!!
                                    }
                                    // Atualiza registro Novo status
                                    atualizaStatusPedidoNoMagento(registro.status, registro.orderId, marca)
                                } else {
                                    registro = SdMagentoOrderBean.fromOrderMagento(order, marca, "B2C")

                                    registro.customer = getCustomer(registro.customer)
                                    registro.shipping.address = getAddress(registro.shipping.address!!)


                                    registro.shipping = FluentDao().merge(registro.shipping)!!
                                    registro.payment = FluentDao().merge(registro.payment)!!
                                    registro.totals = FluentDao().merge(registro.totals)!!

                                    val items = registro.items
                                    registro.items = ArrayList()

                                    registro = FluentDao().merge(registro)!!

                                    //registro.items = items
                                    items.forEach { item ->
                                        item.orderId = registro.id
                                        item.id = FluentDao().merge(item)!!.id

                                        registro.items.add(item)
                                    }

                                    // Atualiza registro Novo status
                                    atualizaStatusPedidoNoMagento(registro.status, registro.orderId, marca)
                                } // if (registro != null) {
                            } // for (order in lista) {

                            val descMarca = if (marca == "F") {
                                "Flor de Lis"
                            } else {
                                "DLZ"
                            }

                            if (lista.isEmpty()) {
                                //"Consultado status: ${StatusOrder.normalize(statusOrder)} nenhum item a atualizar para marca $descMarca"
                                ""
                            } else {
                                "Atualizado o status: ${StatusOrder.normalize(statusOrder)} de ${lista.size} pedidos para marca $descMarca"
                            }
                        } ui { text ->
                            if (!text.isEmpty()) {
                                infoNotification("Sincronização:", text, Pos.TOP_RIGHT, Duration.seconds(15.0))
                            }
                        } // runAsync {
                    } // for (statusOrder in statusBuscar) {
                } // for (marca in marcas) {

                // Só pausa se for auto import.
                if (autoImport) {
                    Thread.sleep(Duration.minutes(5.0).toMillis().toLong())
                }
            }
        }
    }

    private fun getStatusList(): ArrayList<String> {
        val list = ArrayList<String>()

        if (inputStatusCanceled.value) {
            list.add("canceled")
            list.add("closed")
        }
        if (inputStatusComplete.value) {
            list.add("complete")
            list.add("complete_shipped")
            list.add("shipping")
        }
        if (inputStatusEntregue.value) {
            list.add("entregue")
        }

        if (inputStatusPending.value) {
            list.add("pending")
        }
        if (inputStatusProcessing.value) {
            list.add("processing")
        }
        if (inputStatusFraude.value) {
            list.add("fraud")
        }

        return list
    }

    fun buscarPedidosLocal() {

        pedidos.clear()

        try {
            JPAUtils.clearEntitys(pedidos)
        } catch (e: Exception) {
        }


        val idCliente = if (inputFiltroCliente.value != null && inputFiltroCliente.value != "") {
            val numberErp = FluentDao()
                    .selectFrom(Pedido::class.java)
                    .where{
                        it
                                .equal("codcli.codcli", inputFiltroCliente.value)
                    }
                    .singleResult<Pedido>()?.numero
            FluentDao()
                    .selectFrom(SdMagentoOrderBean::class.java)
                    .where {
                        it
                                .equal("codigoErp", numberErp)
                    }
                    .singleResult<SdMagentoOrderBean>()?.customer?.id
        } else {
            ""
        }

        val filterTrackerId = if(inputFiltroNota.value != null) {inputFiltroNota.value} else {""}


        val lista = FluentDao()
                .selectFrom(SdMagentoOrderBean::class.java)
                .where {
                    val list = getStatusList()
                    it
                            .equal("marca", getMarca()) { inputFiltroMarca.value != null && inputFiltroMarca.value.isNotEmpty() }
                            .equal("customer.id", idCliente) { idCliente.toString().isNotEmpty() }
                            .equal("trackId", filterTrackerId) { filterTrackerId.toString().isNotEmpty() }
                            .isIn("status", list.toArray()) { list.isNotEmpty() }
                            .isNotNull("codigoErp") { inputTipoPedido.value == "Integrados" }
                            .isNull("codigoErp") { inputTipoPedido.value == "Não Integrados" }
                            .equal("origem", "B2C")
                }
                .orderBy("id", OrderType.DESC)
                .resultList<SdMagentoOrderBean>()

        pedidos.setAll(lista)
    }

    fun atualizaStatusPedidoLoja() {
        OrdersWebclient
                .configure(getIdentificacaoConvertr(model.marca.value))
                .updateStatus(this.updateStatusOrder!!)
    }

    fun getMarca(): String {
        return inputFiltroMarca.get() ?: ""
    }

    private fun integrarCliente(pedido: SdMagentoOrderBean): EntidadeBean {
        var entidade = FluentDao()
                .selectFrom(EntidadeBean::class.java)
                .where {
                    it.equal("cnpj", pedido.customer.taxvat?.toFormatedCpfCnpj() ?: "")
                }.singleResult<EntidadeBean>() ?: EntidadeBean()

        if (entidade.codCli == null) {
            entidade.nome = "${pedido.customer.firstname} ${pedido.customer.lastname}".toUpperCase()
            entidade.fantasia = entidade.nome
            entidade.email = (pedido.customer.email ?: "").toUpperCase()
            entidade.cnpj = (pedido.customer.taxvat ?: "").toFormatedCpfCnpj()
            entidade.numRg = pedido.customer.rg ?: ""
            // inscriçao municipal
            entidade.inscEst = ""

            if (pedido.customer.dtNasc != null) {
                entidade.dtNasc = pedido.customer.dtNasc!!.toLocalDateMagento()
            }

            // inscrição estadual
            entidade.inscricao = "ISENTO"

            entidade.tipo = entidade.cnpj?.getTipoEntidade() ?: "1"
            entidade.ativo = "S"
            entidade.telefone = pedido.shipping.address?.telephone ?: ""

            entidade.historico = "0301"
            entidade.banco = "001"

            entidade.sitDup = when (PaymentMethod.stringToEnum(pedido.payment.method)) {
                PaymentMethod.MUNDIPAGG_CREDITCARD -> "23"
                PaymentMethod.FOXSEA_PAGHIPER -> "32"
                else -> ""
            }

            entidade.atividade.codigo = "0014"
            entidade.contac = "6289"
            entidade.bloqueio = "N"
            entidade.condicao = ""
            entidade.credito = BigDecimal.ZERO
            //entidade.financeiro.contad
            entidade.com1 = BigDecimal.ZERO
            entidade.com2 = BigDecimal.ZERO
            entidade.desconto = BigDecimal.ZERO

            //entidade.simples = "A"
            entidade.dias = 0
            entidade.dataCad = LocalDate.now()
            entidade.maiorAcumulo = BigDecimal.ZERO
            entidade.ultimoFat = BigDecimal.ZERO
            entidade.consumos = BigDecimal.ZERO
            entidade.royal = BigDecimal.ZERO

            entidade.tipoEntidade = "C"

            val sitCli = FluentDao()
                    .selectFrom(SitCliBean::class.java)
                    .where {
                        val s = if (isB2B) {
                            "B2"
                        } else {
                            "BC"
                        }
                        it.equal("codigo", s)
                    }.singleResult<SitCliBean>()

            entidade.sitCli = sitCli!!

            entidade.grupo.codigo = "0001"

            entidade.classe.codigo = "0003"
            entidade.codRep = "0126"

            pedido.shipping.let { shipping ->
                shipping.address?.let { address ->

                    entidade.addressMagentoToEndereco(address)

                    // Endereco entrega
                    entidade.addressMagentoToEnderecoEntrega(address)
                    entidade.cnpjEnt = entidade.cnpj ?: ""

                    // Endereco Cobrança
                    entidade.addressMagentoToEnderecoCobranca(address)
                    entidade.cnpjCob = entidade.cnpj ?: ""
                    entidade.foneCob = entidade.telefone
                }
            }

            // Coloca a CFOP no cliente
            entidade.natureza = if (entidade.cep.cidade!!.uf == "SC") {
                "5101000"
            } else {
                when (entidade.tipo) {
                    "1" -> "6107001"
                    "2" -> "6101000"
                    else -> ""
                }
            }
            entidade.obs = "GERAR DIFAL"

            entidade = FluentDao().persist(entidade)!!
            var sdEntidade = SdEntidade()
            sdEntidade.codcli = entidade.codCli
            sdEntidade.origem = "B2C"
            sdEntidade.isAtualizaGrade = true
            sdEntidade.isAvisaBoleto = false
            sdEntidade.isLiberadoB2b = false
            sdEntidade.isSincB2b = false
            sdEntidade.perfil = "Cliente"
            FluentDao().merge(sdEntidade)!!
        }

        return entidade
    }

    fun integrarPedido(pedido: SdMagentoOrderBean) {

        if (pedido.codigoErp != null && pedido.codigoErp != "") {
            error("Operação não permitida", "O Pedido ${pedido.id} já foi integrado com o código ${pedido.codigoErp}")
            return
        }

        val entidade = integrarCliente(pedido)

        var pedidoTI = PedidoEComerceBean()

        pedidoTI.fatura = 100
        pedidoTI.com1 = BigDecimal.ZERO
        pedidoTI.com2 = BigDecimal.ZERO

        //pedidoBean.financeiro

        pedidoTI.percDesc = if (pedido.totals.desconto == BigDecimal.ZERO) {
            BigDecimal.ZERO
        } else {
            OrderMagentoHelper.getPercentualDesconto(
                    pedido.totals.grandTotal!!,
                    pedido.totals.desconto!!.multiply((-1).toBigDecimal()),
                    pedido.shipping.total ?: BigDecimal.ZERO)
        }

        pedidoTI.dtDigita = pedido.createdAt?.toLocalDateTimeMagento()?.toLocalDate() ?: LocalDate.now()
        pedidoTI.dtEmissao = pedido.createdAt?.toLocalDateTimeMagento()?.toLocalDate() ?: LocalDate.now()
        pedidoTI.bloqueio = "1"
        pedidoTI.cif = "1"

        pedidoTI.periodo = when (isB2B) {
            true -> "B2B"
            false -> "B2C"
        }

        pedidoTI.colecao = "1909"
        pedidoTI.codRep2 = null

        pedidoTI.tabPre = when (isB2B) {
            true -> "B2B"
            false -> "B2C${pedido.marca}"
        }

        //alterado de 5702 para 0018
        pedidoTI.tabTrans = "0018"
        pedidoTI.tabRedes = null
        pedidoTI.codRep = "0126"
        pedidoTI.tabPre2 = "00"

        pedidoTI.codCli = entidade.codCli.toString().padStart(5, '0')
        pedidoTI.pedCli = pedido.orderId!!

        pedidoTI.pagto = "1"

        pedidoTI.obs = null
        pedidoTI.artCli = null
        pedidoTI.ris = null

        pedidoTI.moeda = "0"
        pedidoTI.entrega = LocalDate.of(2050, 12, 31)
        pedidoTI.dtFatura = LocalDate.of(2050, 12, 31)

        // Numero nota fiscal
        pedidoTI.nota = null

        pedidoTI.tempo = 0
        pedidoTI.com3 = BigDecimal.ZERO
        pedidoTI.com4 = BigDecimal.ZERO

        pedidoTI.motivo = null
        pedidoTI.programacao = "0"


        val formatter: NumberFormat = NumberFormat.getInstance(Locale("pt", "BR"))
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        formatter.roundingMode = RoundingMode.HALF_UP
        val formatedFloat: String? = formatter.format(pedidoTI.percDesc)

        pedidoTI.desconto = if (pedidoTI.percDesc == BigDecimal.ZERO) {
            "0,00 0,00 0,00 0,00"
        } else {
            "$formatedFloat 0,00 0,00 0,00"
        }

        pedidoTI.financeiro = "1"

        pedidoTI.sitDup = when (PaymentMethod.stringToEnum(pedido.payment.method)) {
            PaymentMethod.FOXSEA_PAGHIPER -> "32"
            PaymentMethod.MUNDIPAGG_CREDITCARD -> "23"
            else -> ""
        }

        pedidoTI.redespCif = "0"
        pedidoTI.frete = pedido.shipping.total
        pedidoTI.tipo = "P"
        pedidoTI.bonif = BigDecimal.ZERO
        pedidoTI.impresso = 0
        pedidoTI.vlrDesc = BigDecimal.ZERO
        pedidoTI.taxa = BigDecimal.ZERO
        pedidoTI.locado = "0"

        pedidoTI.periodoProd = when (isB2B) {
            true -> "B2B"
            false -> "B2C"
        }

        pedidoTI.progSetor = null
        pedidoTI.contato = null
        pedidoTI.envioEspelho = "X"


        pedidoTI = FluentDao().persist(pedidoTI)!!
        FluentDao().runNativeQueryUpdate("update sd_pedido_001 set marca = '" + pedido.marca + "' where numero = '" + pedidoTI.numero + "'")
        pedido.codigoErp = pedidoTI.numero
        FluentDao().merge(pedido)

        // inicia gravação dos itens do pedido
        var itemsPedidoErp = ArrayList<PedidoItemBean>()
        var ordem = 0
        var ref = "x"
        var sortedItens = pedido.items.sortedWith(compareBy({ it.sku }))

        for (item in sortedItens) {
            val itemGravar = PedidoItemBean()

            val sku = item.sku!!.split('-') //D69326-UN-P

            if (!ref.equals(sku[0])){
                ref = sku[0]
                ordem++
            }

            itemGravar.id.ordem = ordem
            itemGravar.id.codigo = sku[0]
            itemGravar.id.cor = sku[1]
            itemGravar.id.tam = sku[2]
            itemGravar.id.numero = pedidoTI.numero

            itemGravar.qtde = item.qty?.toInt() ?: 0
            itemGravar.qtdeF = 0
            itemGravar.preco = item.price
            itemGravar.qtdeCanc = 0
            itemGravar.qtdePacks = itemGravar.qtde
            itemGravar.qualidade = "1"
            itemGravar.qtdeOrig = itemGravar.qtde
            itemGravar.precoOrig = itemGravar.preco
            itemGravar.indice2 = 0
            itemGravar.percComissao = BigDecimal.ZERO
            itemGravar.desconto = null
            itemGravar.tipo = "P"
            itemGravar.ipi = BigDecimal.ZERO
            itemGravar.margem = BigDecimal.ZERO
            itemGravar.bonif = 0
            itemGravar.custo = BigDecimal.ZERO
            itemGravar.indice = 0
            itemGravar.impostos = BigDecimal.ZERO
            itemGravar.embalagem = null
            itemGravar.observacao = null
            itemGravar.dtEntrega = LocalDate.of(2050, 12, 31)
            itemGravar.descPack = null
            itemGravar.estampa = null
            itemGravar.nrItem = ordem
            itemGravar.motivo = null
            itemGravar.valorSt = BigDecimal.ZERO

            FluentDao().merge(itemGravar)
            itemsPedidoErp.add(itemGravar)
            ordem++
        }
        pedidoTI.itens = itemsPedidoErp

        for (item in pedido.items) {
            var qtdeEstoque = FluentDao()
                    ?.selectFrom(PaItenBean::class.java)
                    .where {
                        it
                                .equal("id.codigo", item.sku!!.split('-')[0])
                                .equal("id.cor", item.sku!!.split('-')[1])
                                .equal("id.tam", item.sku!!.split('-')[2])
                                .equal("id.deposito", "0023")
                    }.resultList<PaItenBean>()!!.stream()
                    .mapToInt { paItenBean -> paItenBean.quantidade!!.toInt() }
                    .sum()
            qtdeEstoque -= FluentDao()
                    .selectFrom(PedReservaBean::class.java)
                    .where {
                        it
                                .equal("id.codigo", item.sku!!.split('-')[0])
                                .equal("id.cor", item.sku!!.split('-')[1])
                                .equal("id.tam", item.sku!!.split('-')[2])
                                .equal("id.deposito", "0023")
                    }.resultList<PedReservaBean>()!!.stream()
                    .mapToInt { pedReservaBean -> pedReservaBean.qtde!!.toInt() }
                    .sum()
            item.reservado = qtdeEstoque > 0
            if (item.reservado!!) {
                addReserva(
                        pedidoTI.numero!!,
                        item.sku!!.split('-')[0],
                        item.sku!!.split('-')[1],
                        item.sku!!.split('-')[2],
                        (item.qty!!.toInt() ?: 0).toBigDecimal()
                )
            }
        }

        if (PaymentMethod.stringToEnum(pedido.payment.method) == PaymentMethod.MUNDIPAGG_CREDITCARD) {
            // 1º - Gera o ID do lancamento
//            val lancamentoId = JPAUtils.getNextNumFromCodigo001("CONTACOR", "LANCAMENTO")
            val lancamentoId = Globals.getProximoCodigo("CONTACOR", "LANCAMENTO")

            // 2º - Insere o registro a receber
            var receber = ReceberBean()
            // Auto gerado
            //receber.numero =
            with(receber) {
                dtVencto = LocalDate.now()
                dtEmissao = LocalDate.now()
                original = LocalDate.now()

                valor = pedido.totals.grandTotal
                valor2 = pedido.totals.grandTotal
                valorPago = BigDecimal.ZERO
                valOrigin = pedido.totals.grandTotal

                frete = BigDecimal.ZERO//pedido.shipping.total
                desconto = BigDecimal.ZERO//pedido.totals.desconto

                situacao = "19" // TODO ??
                tipFat = "M"
                banco = when (pedido.payment.acquirer) {
                    "GetNet" -> "900"
                    "Cielo" -> "911"
                    else -> "900"
                }
                codRep = "0126"
                codCli = entidade.codCli.toString().padStart(5, '0')

                //"CARTÃO: 123456****5678 - BANDEIRA: VISA - DATA/HORA COMPRA: 2020-02-24 04:43:21"
                obs = "CARTÃO: ${pedido.payment.ccNumber} - DATA/HORA COMPRA: ${pedido.createdAt}"

                classe = "0172" // TODO ??
                status = "A" // TODO ??

                dtPrevisao = LocalDate.now() // TODO ??
                historico = "0330"

                lancamento = lancamentoId
            }

            receber = FluentDao().persist(receber)!!

            // Envia o email
            enviaEmailAntecipacao(pedido, entidade, receber.numero!!.toString(), lancamentoId)

            val contacor = ContaCorBean()

            with(contacor) {
                id.docto = receber.numero.toString()
                id.lancamento = lancamentoId

                id.conta = when (pedido.payment.acquirer) {
                    "GetNet" -> "900"
                    "Cielo" -> "911"
                    else -> "900"
                }
                id.operacao = "+"

                dtLan = LocalDate.now()
                valor = receber.valor
                pendente = "S"
                tipo = "R"
                banco = receber.banco
                grupo = "0172"
                historico = receber.historico
                importSap = "N"
                dtCont = LocalDate.now()
                dtVencto = LocalDate.now()
                tipoDoc = "DP"
                observacao = receber.obs
                //usuNumReg = 663561 // TODO ???
            }

            FluentDao().persist(contacor)

            val contabil = ContabilBean()

            with(contabil) {
                id.lancamento = lancamentoId
                id.ordem = 1

                data = LocalDate.now()
                valor = receber.valor
                operacao = "+"
                contaC = entidade.contac // "13839"
                contaD = entidade.contad // "1690"
                tipo = "AR"
                observacao = entidade.nome

                docto = null

                numero = receber.numero.toString()

                dataLan = LocalDate.now()
            }

            FluentDao().persist(contabil)
        }
    }

    private fun addReserva(numero: String, codigo: String, cor: String, tam: String, qtde: BigDecimal) {
        FluentDao()
                .merge(
                        PedReservaBean(
                                id = PedReservaPK(
                                        numero = numero,
                                        codigo = codigo,
                                        cor = cor,
                                        tam = tam,
                                        reserva = 1,
                                        deposito = "0023",
                                        lote = "000000"
                                ),
                                qtde = qtde,
                                qtdeB = BigDecimal.ZERO,
                                qtdeExp = BigDecimal.ZERO,
                                dtCad = null,
                                tipo = null,
                                sdReserva = null
                        )
                )
    }

    fun gravarRastreioNoPedido(rastreio: String) {
        this.model.item.trackId = rastreio
        this.model.item = FluentDao().merge(this.model.item)
    }

    fun gravarStatusDoPedido(status: String) {
        this.model.item.status = status
        this.model.item = FluentDao().merge(this.model.item)
    }

    fun consultaCodigoRastreio(rastreio: String) {

        val rastreadorApi = CorreiosRastreioApi.Builder()
                .addCodigoRastreio(rastreio)
                .idiomaRetorno(CorreiosIdioma.PORTUGUES)
                .escopoResposta(CorreiosEscopoResultado.TODOS_OS_EVENTOS)
                .build()

        val detalhes = rastreadorApi.consulta()

        detalhes?.let { retorno ->
            val msg = if (!retorno.objetosRastreio[0].descricaoErro.isNullOrEmpty()) {
                detalhes.objetosRastreio[0].descricaoErro
            } else {
                var msgTmp = "Categoria: ${retorno.objetosRastreio[0].categoria}\n"
                msgTmp += "Nome: ${retorno.objetosRastreio[0].nome}\n"
                for (evento in retorno.objetosRastreio[0].eventos) {
                    msgTmp += "Eventos:\n"
                    msgTmp += "    Descrição: ${evento.descricao}\n"
                    if (!evento.detalhe.isNullOrEmpty()) {
                        msgTmp += "    Detalhe: ${evento.detalhe}\n"
                    }
                }
                msgTmp
            }
            information("Dados de rastreio", msg)
        }
    }

    private fun enviaEmailAntecipacao(pedido: SdMagentoOrderBean, entidade: EntidadeBean, nroAntecipacao: String, codLancamento: String) {
        SimpleMail
                .addDestinatario("cartao@deliz.com.br")
                .addCCO("diego@deliz.com.br")
                .comAssunto("Antecipação de compra com cartão na loja virtual B2C")
                .comCorpoHtml {
                    val loja = if (isB2B) "B2B" else "B2C"
                    val marca = if (pedido.marca == "D") "DLZ" else "Flor de Lis"

                    var html = "<!DOCTYPE html>" +
                            "<html lang=\"pt-br\">" +
                            "    <head>" +
                            "        <title>Entrada de antecipação Financeira, Referente ao pedido  ${pedido.codigoErp} - B2C integrado  </title>" +
                            "        <meta charset=\"utf-8\">" +
                            SimpleMail.getDefaultStyle() +
                            "    </head>" +
                            "    <body>" +
                            "        <h3>Aviso de Antecipação</h3>" +
                            "        Foi integrado um pedido com antecipação de cartão de crédito:" +
                            "        <br/>" +
                            "        <table>" +
                            "            <thead>" +
                            "                <tr>" +
                            "                    <th>Loja</th>" +
                            "                    <th>Data (e-commerce)</th>" +
                            "                    <th>Pedido (e-commerce)</th>" +
                            "                    <th>Pedido (TI)</th>" +
                            "                    <th>Valor</th>" +
                            "                    <th>Parcelas</th>" +
                            "                    <th>Cartão</th>" +
                            "                    <th>NSU</th>" +
                            "                    <th>Cód Cliente</th>" +
                            "                    <th>Nome Cliente</th>" +
                            "                    <th>Nro Antecipacao</th>" +
                            "                    <th>Código Lançamento</th>" +
                            "                </tr>" +
                            "            </thead>" +
                            "            <tbody>" +
                            "                <tr>" +
                            "                    <td>$loja - $marca</td>" +
                            "                    <td>${pedido.createdAt}</td>" +
                            "                    <td>${pedido.orderId}</td>" +
                            "                    <td>${pedido.codigoErp}</td>" +
                            "                    <td>${pedido.totals.grandTotal}</td>" +
                            "                    <td>${pedido.payment.ccInstallments}</td>" +
                            "                    <td>${pedido.payment.ccNumber}</td>" +
                            "                    <td>${pedido.payment.nsu}</td>" +
                            "                    <td>${entidade.codCli}</td>" +
                            "                    <td>${entidade.nome}</td>" +
                            "                    <td>$nroAntecipacao</td>" +
                            "                    <td>$codLancamento</td>" +
                            "                </tr>" +
                            "            </tbody>" +
                            "        </table>" +
                            "    </body>" +
                            "</html>"
                    html
                }.send()
    }

    fun consultaStatusPedidosEmTransporteNosCorreios(): Int {
        var updated = 0
        for (pedido in pedidos) {
            if (!pedido.trackId.isNullOrEmpty() && pedido.status == StatusOrder.COMPLETE.value) {
                // Consulta nos correios
                val rastreadorApi = CorreiosRastreioApi.Builder()
                        .addCodigoRastreio(pedido.trackId!!)
                        .idiomaRetorno(CorreiosIdioma.PORTUGUES)
                        .escopoResposta(CorreiosEscopoResultado.ULTIMO_EVENTO)
                        .build()

                val detalhes = rastreadorApi.consulta()

                detalhes?.let {
                    if (it.objetosRastreio[0].eventos.isNotEmpty()) {
                        if (it.objetosRastreio[0].ultimoEvento.descricao == "Objeto entregue ao destinatário") {
                            updated++
                            this.updateStatusOrder = OrderStatusUpdate(
                                    orderId = pedido.orderId,
                                    state = StatusOrder.ENTREGUE.value
                            )

                            OrdersWebclient
                                    .configure(getIdentificacaoConvertr(pedido.marca!!))
                                    .updateStatus(this.updateStatusOrder!!)
                        }
                    }
                }
            }
        }
        return updated
    }

    fun marcarPedidoSelecionadoComoFraude() {
        this.model.item.status = "fraud"
        this.model.item = FluentDao().merge(this.model.item)
    }

    private fun getNextStatusPedido(status: String?): StatusOrder {
        return when (StatusOrder.stringToEnum(status ?: "?")) {
            StatusOrder.PENDENTE -> StatusOrder.PENDENTE_ERP
            StatusOrder.PROCESSANDO -> StatusOrder.PROCESSANDO_ERP
            StatusOrder.CANCELADO -> StatusOrder.CANCELADO_ERP
            StatusOrder.FECHADO -> StatusOrder.FECHADO_ERP
            StatusOrder.COMPLETE -> StatusOrder.EM_TRANSPORTE
            StatusOrder.ENTREGUE -> StatusOrder.ENTREGUE_ERP
            else -> StatusOrder.INDEFINIDO
        }
    }

    private fun atualizaStatusPedidoNoMagento(status: String?, orderId: String?, marca: String?) {
        val statusNovo = getNextStatusPedido(status)

        if (statusNovo != StatusOrder.INDEFINIDO) {
            val updateStatusOrder = OrderStatusUpdate(orderId = orderId, state = statusNovo.value)

            val response = OrdersWebclient
                    .configure(getIdentificacaoConvertr(marca!!))
                    .updateStatus(updateStatusOrder)!!

            response.let {
                if (it.status == 200) {
                    log.info("Solicitado atualização do pedido $orderId com sucesso")
                } else {
                    log.severe("Não foi processado o comando de atualização para o pedido ${orderId}. Resposta: ${it.status} - ${it.messages}")
                }
            }
        }
    }

    private fun getCustomer(customer: SdMagentoClienteBean): SdMagentoClienteBean {
        var result = FluentDao()
                .selectFrom(SdMagentoClienteBean::class.java)
                .where {
                    it
                            .equal("firstname", customer.firstname)
                            .equal("lastname", customer.lastname)
                            .equal("email", customer.email)
                            .equal("taxvat", customer.taxvat)
                            .equal("rg", customer.rg) { !customer.rg.isNullOrEmpty() }
                }.singleResult<SdMagentoClienteBean>()

        if (result == null) {
            result = FluentDao().persist(customer)
        }

        return result!!
    }

    private fun getAddress(address: SdMagentoEnderecoBean): SdMagentoEnderecoBean {
        var retorno = FluentDao()
                .selectFrom(SdMagentoEnderecoBean::class.java)
                .where {
                    it
                            .equal("street", address.street)
                            .equal("complement", address.complement)
                            .equal("neighborhood", address.neighborhood)
                            .equal("postcode", address.postcode)
                            .equal("city", address.city)
                            .equal("region", address.region)
                            .equal("telephone", address.telephone)
                }.singleResult<SdMagentoEnderecoBean>()

        if (retorno == null) {
            retorno = FluentDao().merge(address)
        }

        return retorno!!
    }

    private fun getIdentificacaoConvertr(marca: String): IdentificadorLoja {
        return IdentificadorLoja.valueOf(if (isB2B) "B2B" else "B2C${marca}")
    }

}