package sysdeliz2.controllers.ecomerce.helpers

import java.math.BigDecimal
import java.math.RoundingMode

/**
 *
 * @author lima.joao
 * @since 17/03/2020 10:02
 */
object OrderMagentoHelper {

    fun getPercentualDesconto(valorTotal: BigDecimal, valorDesconto: BigDecimal, valorFrete: BigDecimal): BigDecimal {
        /*
           Formula para pegar o percentual do desconto:
           percentual = ((valor desconto) / (valor liquido + valor desconto - valor frete)) * 100
        */

        var totalPedido: BigDecimal = valorTotal.subtract(valorFrete).add(valorDesconto);
        var desconto: BigDecimal = valorDesconto.divide(totalPedido, 5, RoundingMode.HALF_UP)
        var percDesconto: BigDecimal = desconto.multiply(100.toBigDecimal())
        return percDesconto
    }
}