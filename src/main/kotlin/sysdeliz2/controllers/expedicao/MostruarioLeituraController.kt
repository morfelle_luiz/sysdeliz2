package sysdeliz2.controllers.expedicao

import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import sysdeliz2.dao.FluentDao
import sysdeliz2.models.sysdeliz.SdMostruariosBean
import sysdeliz2.models.ti.PaItenBean
import sysdeliz2.models.ti.ProdutoBean
import sysdeliz2.models.ti.RepresenTi
import tornadofx.*
import java.io.Serializable
import java.time.LocalDate

/**
 *
 * @author lima.joao
 * @since 09/03/2020 11:49
 */
class MostruarioLeituraController : Controller() {

    val filtroRepresentante = SimpleStringProperty(this, "filtro_representante", "")
    val filtroColecao = SimpleStringProperty(this, "filtro_colecao", "")
    val filtroTipo = SimpleStringProperty(this, "filtro_tipo", "")

    val inputBarraLida = SimpleStringProperty(this, "input_barra_lida", "")
    val inputRemoverBarraLida = SimpleBooleanProperty(this, "input_remover_barra_lida", false)

    var nomeRepresentante = SimpleStringProperty(this, "nome_representante", "")

    val registros: ObservableList<SdMostruariosBean> = FXCollections.observableArrayList()

    val somatorio = SimpleIntegerProperty(this, "somatorio_lida", 0)


    fun findWithFilter() {
        val list = FluentDao()
                .selectFrom(SdMostruariosBean::class.java)
                .where {
                    val entSai = if (filtroTipo.value == "Entrada") "E" else "S"

                    it
                            .equal("codRep", filtroRepresentante.value)
                            .equal("colecao", filtroColecao.value)
                            .equal("entSai", entSai)

                }.resultList<SdMostruariosBean>()
        registros.setAll(list)
        // Contagem total
        somatorio.value = registros.size
    }

    fun addBarraLida() {
        if (inputBarraLida.value.isNotEmpty() && inputBarraLida.value.length == 16) {
            val barra = inputBarraLida.value.substring(0, 6)
            val barraCompleta = inputBarraLida.value

            val registro = FluentDao()
                    .selectFrom(SdMostruariosBean::class.java)
                    .where {
                        val entSai = if (filtroTipo.value == "Entrada") "E" else "S"
                        it
                                .equal("codRep", filtroRepresentante.value)
                                .equal("colecao", filtroColecao.value)
                                .equal("entSai", entSai)
                                .equal("barra", barraCompleta)
                    }.singleResult<SdMostruariosBean>()

            if (inputRemoverBarraLida.value) {
                if (registro != null) {
                    FluentDao().delete<SdMostruariosBean>(registro.id as Serializable)
                }

                registros.removeIf { predicate -> predicate.id == registro!!.id }
//                inputRemoverBarraLida.value = false
                somatorio.value--
            } else {
                // A barra já esta cadastrada para essa coleção, representante, tipo
                if (registro != null) {
                    error(
                            "Operação não permitida.",
                            "Barra: $barra já cadastrada para o representante ${filtroRepresentante.value} na coleção: ${filtroColecao.value} com o status: ${filtroTipo.value}")
                } else {
                    val itens = FluentDao()
                            .selectFrom(PaItenBean::class.java)
                            .where {
                                it.equal("barra28", barra)
                            }.resultList<PaItenBean>()

                    if (itens!!.size > 0) {
                        itens[0].let {paitem ->

                            // Apenas se o produto estiver ativo pode prosseguir
                            val produto = FluentDao()
                                    .selectFrom(ProdutoBean::class.java)
                                    .where {
                                        it.equal("codigo", paitem.id.codigo)
                                    }.singleResult<ProdutoBean>()

                            if (produto?.ativo?:"N" == "S") {
                                val sdMostruario = SdMostruariosBean()

                                sdMostruario.barra = barraCompleta
                                sdMostruario.codRep = filtroRepresentante.value
                                sdMostruario.entSai = if (filtroTipo.value == "Entrada") "E" else "S"
                                sdMostruario.codigo = paitem.id.codigo
                                sdMostruario.tam = paitem.id.tam
                                sdMostruario.cor = paitem.id.cor
                                sdMostruario.qtd = 1
                                sdMostruario.colecao = filtroColecao.value
                                sdMostruario.dataLeitura = LocalDate.now()

                                FluentDao().persist(sdMostruario)
                                registros.add(0, sdMostruario)
                                somatorio.value++
                            } else {
                                error(
                                        "Atenção:",
                                        "A Barra ${inputBarraLida.value} esta desativada no sistema ou não foi encontrado o produto a qual elá esta vinculada")
                            }
                       }
                    } else {
                        error(
                                "Atenção:",
                                "A Barra ${inputBarraLida.value} não foi encontrada no sistema")
                    }
                }
            }
        } else {
            error(
                    "Atenção:",
                    "A Barra ${inputBarraLida.value} não esta no formato esperado")
        }
    }

    fun loadNomeRepresentante() {
        val repres = FluentDao()
                .selectFrom(RepresenTi::class.java)
                .where {
                    it.equal("codRep", filtroRepresentante.value)
                }.singleResult<RepresenTi>()
        if (repres != null) {
            nomeRepresentante.value = repres.nome
        }
    }
}