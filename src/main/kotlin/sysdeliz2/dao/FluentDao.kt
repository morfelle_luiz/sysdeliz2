package sysdeliz2.dao

import sysdeliz2.dao.fluentDao.Where
import sysdeliz2.utils.hibernate.JPAUtils
import java.io.Serializable
import java.sql.SQLException
import javax.persistence.EntityManager
import javax.persistence.FlushModeType
import javax.persistence.RollbackException

class FluentDao {

    lateinit var delete: Any
    var entityManager: EntityManager = JPAUtils.entityManager!!

    private var autoTransaction = true

    fun selectFrom(clazz: Class<*>): Where {
        return Where(clazz)
    }

    @Throws(SQLException::class)
    fun <Any> persist(entity: Any?): Any? {
        if (!entityManager.isOpen) {
            throw SQLException("Entity manager não esta pronto")
        }

        if (entity == null) {
            throw SQLException("Entidade não pode ser nula")
        }

        if (autoTransaction) {
            if (!entityManager.transaction.isActive) {
                entityManager.transaction.begin()
            }
        }

        entityManager.persist(entity)
        val retorno: Any? = entity

        if (autoTransaction) {
            if (entityManager.transaction.isActive) {
                entityManager.flush()
                entityManager.transaction.commit()
            }
        }

        return retorno
    }

    @Throws(SQLException::class)
    fun <Any> persistAll(entities: List<Any?>) {
        if (!entityManager.isOpen) {
            throw SQLException("Entity manager não esta pronto")
        }

        if (entityManager.transaction.isActive) {
            entityManager.transaction.begin()
        }
        autoTransaction = false
        for (entity in entities) {
            persist(entity)
        }
        autoTransaction = true

        if (entityManager.transaction.isActive) {
            entityManager.flush()
            entityManager.transaction.commit()
        }
    }

    fun <Any> merge(entity: Any?): Any? {
        if (!entityManager.isOpen) {
            throw SQLException("Entity manager não esta pronto")
        }

        if (entity == null) {
            throw SQLException("Entidade não pode ser nula")
        }

        if (autoTransaction) {
            if (!entityManager.transaction.isActive) {
                entityManager.transaction.begin()
            }
        }

        val retorno: Any? = entityManager.merge(entity)

        if (autoTransaction) {
            if (entityManager.transaction.isActive) {
                entityManager.flush()
                entityManager.transaction.commit()
            }
        }
        return retorno
    }


    fun <Any> mergeAll(entities: List<Any?>) {
        if (!entityManager.isOpen) {
            throw SQLException("Entity manager não esta pronto")
        }

        if (!entityManager.transaction.isActive) {
            entityManager.transaction.begin()
        }
        autoTransaction = false
        for (entity in entities) {
            merge(entity)
        }
        autoTransaction = true

        if (entityManager.transaction.isActive) {
            entityManager.flush()
            entityManager.transaction.commit()
        }
    }

    /**
     * Verifica se a entidade já esta sendo no manager
     */
    fun contains(entity: Any?): Boolean {
        if (!entityManager.isOpen) {
            throw SQLException("Entity manager não esta pronto")
        }

        if (entity == null) {
            throw SQLException("Entidade não pode ser nula")
        }
        return entityManager.contains(entity)

    }

    inline fun <reified Any> delete(id: Serializable) {
        if (!entityManager.isOpen) {
            throw SQLException("Entity manager não esta pronto")
        }
        val entity = entityManager.find(Any::class.java, id) as Any
        delete(entity)
    }

    fun delete(entity: Any?) {
        if (!entityManager.transaction.isActive) {
            entityManager.transaction.begin()
        }

        entityManager.remove(entity)

        if (entityManager.transaction.isActive) {
            entityManager.flush()
            entityManager.transaction.commit()
        }
    }

    fun deleteAll(entities: List<Any?>) {
        if (!entityManager.transaction.isActive) {
            entityManager.transaction.begin()
        }

        autoTransaction = false
        for (entity in entities) {
            delete(entity)
        }
        autoTransaction = true

        if (entityManager.transaction.isActive) {
            entityManager.flush()
            entityManager.transaction.commit()
        }
    }

    @Suppress("unused")
    fun runQuery(sql: String): MutableList<Any?>? {
        return entityManager.createQuery(sql).resultList

    }

    @Suppress("unused")
    fun runNativeQuery(sql: String): MutableList<Any?>? {
        return entityManager.createNativeQuery(sql).resultList

    }

    @Suppress("unused")
    fun runNativeQueryUpdate(sql: String) {

        if (autoTransaction) {
            if (!entityManager.transaction.isActive) {
                entityManager.transaction.begin()
            }
        }
        entityManager.createNativeQuery(sql).executeUpdate()

        if (autoTransaction) {
            if (entityManager.transaction.isActive) {
                entityManager.flush()
                entityManager.transaction.commit()
            }
        }
    }

    fun <Any> mergeWithoutCommit(entity: Any?): Any? {
        if (!entityManager.isOpen) {
            throw SQLException("Entity manager não esta pronto")
        }

        if (entity == null) {
            throw SQLException("Entidade não pode ser nula")
        }

        if (!entityManager.transaction.isActive) {
            entityManager.transaction.begin()
//            entityManager.flushMode = FlushModeType.COMMIT
        }
        val valor = entityManager.merge(entity)
        return valor
    }

    @Throws(SQLException::class,RollbackException::class)
    fun commitTransaction() {
        if (!entityManager.isOpen) {
            throw SQLException("Entity manager não esta pronto")
        }
        if (!entityManager.transaction.isActive) {
            throw SQLException("Transaction não está ativo")
        }

        try {
//            entityManager.flushMode = FlushModeType.AUTO
            entityManager.transaction.commit()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun rollbackTransaction() {
        if (!entityManager.isOpen) {
            throw SQLException("Entity manager não esta pronto")
        }
//        if (!entityManager.transaction.isActive) {
//            throw SQLException("Transaction não está ativo")
//        }
        entityManager.transaction.rollback()
    }
}