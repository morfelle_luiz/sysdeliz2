package sysdeliz2.dao

/**
 *
 * @author lima.joao
 * @since 27/02/2020 17:34
 */
enum class OrderType {
    ASC, DESC
}