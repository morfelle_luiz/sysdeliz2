package sysdeliz2.dao.fluentDao

import javax.persistence.criteria.Join
import javax.persistence.criteria.Path
import javax.persistence.criteria.Root

/**
 *
 * @author lima.joao
 * @since 02/03/2020 13:53
 */
object FluentDaoHelper {

    @Suppress("unused", "unchecked_cast")
    fun toExpressionField(from: Root<*>, attribute: String): Path<Any> {
        var expressionField: Path<*> = from as Path<Any>

        for (s in attribute.split(".").toTypedArray()) {
            expressionField = expressionField.get<Any>(s)
        }
        return expressionField as Path<Any>
    }

    @Suppress("unused", "unchecked_cast")
    fun toExpressionField(from: Join<Any, Any>, attribute: String): Path<Any> {
        var expressionField: Path<*> = from as Path<Any>

        for (s in attribute.split(".").toTypedArray()) {
            expressionField = expressionField.get<Any>(s)
        }
        return expressionField as Path<Any>
    }

}