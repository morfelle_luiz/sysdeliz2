package sysdeliz2.dao.fluentDao

/**
 *
 * @author lima.joao
 * @since 02/03/2020 14:11
 */
enum class TipoExpressao {
    AND, OR
}