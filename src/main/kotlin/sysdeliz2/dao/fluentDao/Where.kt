package sysdeliz2.dao.fluentDao

import org.hibernate.CacheMode
import org.hibernate.JDBCException
import org.hibernate.exception.GenericJDBCException
import org.hibernate.exception.JDBCConnectionException
import org.hibernate.jpa.QueryHints
import sysdeliz2.dao.OrderType
import sysdeliz2.dao.fluentDao.TipoExpressao.AND
import sysdeliz2.dao.fluentDao.TipoExpressao.OR
import sysdeliz2.daos.generics.helpers.Ordenacao
import sysdeliz2.utils.extensions.loggerFor
import sysdeliz2.utils.hibernate.JPAUtils
import java.sql.SQLRecoverableException
import javax.persistence.EntityNotFoundException
import javax.persistence.NoResultException
import javax.persistence.PersistenceException
import javax.persistence.criteria.*

/**
 *
 * @author lima.joao
 * @since 02/03/2020 13:42
 */
class Where(clazz: Class<*>) {

    val log = loggerFor(javaClass)

    private var entityManager = JPAUtils.entityManager!!

    // Define a classe a ser consultada
    private var persistentClass: Class<*> = clazz
    private var builder: CriteriaBuilder = this.entityManager.criteriaBuilder
    private var query: CriteriaQuery<*> = this.builder.createQuery(this.persistentClass)
    private var from: Root<*> = this.query.from(this.persistentClass)
    private var joins: MutableMap<String, Join<Any, Any>> = mutableMapOf()
    private var predicate: Predicate = builder.and()

    init {
        @Suppress("unchecked_cast")
        this.query.select(from as Selection<out Nothing>)
    }

    fun join(attributte: String, joinType: JoinType): Where {
        joins[attributte] = this.from.join(attributte, joinType)
        return this
    }

    @Suppress("unchecked_cast")
    fun max(attribute: String): Where {
        this.query.select(
            builder.max(
                FluentDaoHelper.toExpressionField(
                    from,
                    attribute
                ) as Path<Number>
            ) as Selection<out Nothing>
        )
        return this
    }

    @Suppress("unchecked_cast")
    fun sum(attribute: String): Where {
        this.query.select(
            builder.sum(
                FluentDaoHelper.toExpressionField(
                    from,
                    attribute
                ) as Path<Number>
            ) as Selection<out Nothing>
        )
        return this
    }

    @Suppress("unused", "unchecked_cast")
    fun count(attribute: String): Where {
        this.query.select(
            builder.count(
                FluentDaoHelper.toExpressionField(
                    from,
                    attribute
                ) as Path<Long>
            ) as Selection<Nothing>
        )
        return this
    }

    @Suppress("unused", "unchecked_cast")
    fun countDistinct(attribute: String): Where {
        this.query.select(
            builder.countDistinct(
                FluentDaoHelper.toExpressionField(
                    from,
                    attribute
                ) as Path<Long>
            ) as Selection<Nothing>
        )
        return this
    }

    @Suppress("unchecked_cast")
    fun distinctColumn(column: String): Where {
        this.query.select((FluentDaoHelper.toExpressionField(from, column)) as Selection<out Nothing>)
        this.query.distinct(true)
        return this
    }

    @Suppress("unchecked_cast")
    fun columns(columns: List<String>): Where {
        var paths: MutableList<Path<Any>> = ArrayList<Path<Any>>()
        columns.forEach { paths.add(FluentDaoHelper.toExpressionField(from, it)) }
        var selections: MutableList<Selection<out Nothing>> = ArrayList<Selection<out Nothing>>()
        paths.forEach {
            selections.add(it as Selection<out Nothing>)
        }
        this.query.multiselect(selections as List<Selection<out Nothing>>)
        return this
    }

    @Suppress("unchecked_cast")
    fun agregate(columns: List<String>, block: (agregates: AgregateQuery) -> AgregateQuery): Where {
        var paths: MutableList<Path<Any>> = ArrayList<Path<Any>>()
        columns.forEach { paths.add(FluentDaoHelper.toExpressionField(from, it)) }
        var selections: MutableList<Selection<out Nothing>> = ArrayList<Selection<out Nothing>>()
        var columnsSelection: MutableList<Expression<out Nothing>> = ArrayList<Expression<out Nothing>>()

        paths.forEach {
            selections.add(it as Selection<out Nothing>)
            columnsSelection.add(it as Expression<out Nothing>)
        }
        block(AgregateQuery()).expressionColumns.forEach {
            selections.add(it as Selection<out Nothing>)
        }

        this.query.multiselect(selections as List<Selection<out Nothing>>)
        this.query.groupBy(columnsSelection as List<Expression<out Nothing>>)
        return this
    }

    fun distinct(): Where {
        this.query.distinct(true);
        return this
    }

    fun where(block: (expressions: ExpressionBuilder) -> Any): ResultBuilder {
        //entityManager.clear()

        entityManager.let {
            block(ExpressionBuilder())
            query.where(predicate)
        }
        return ResultBuilder()
    }

    fun get(): ResultBuilder {
        return ResultBuilder()
    }

    inner class ResultBuilder {

//        @Suppress("unused")
//        fun multiSelect(attributes: List<MultiSelecBuilder>): ResultBuilder {
//            val expression = ArrayList<Path<Any>>()
//            attributes.forEach { attribute ->  expression.add(FluentDaoHelper.toExpressionField(from, attribute))}
//            query.multiselect()
//            query.groupBy(expression as List<Expression<*>>?)
//            return this
//        }

        @Suppress("unused")
        fun groupBy(attributes: List<String>): ResultBuilder {
            val expression = ArrayList<Path<Any>>()
            attributes.forEach { attribute -> expression.add(FluentDaoHelper.toExpressionField(from, attribute)) }
            query.groupBy(expression as List<Expression<*>>?)
            return this
        }

        @Suppress("unused")
        fun orderBy(attribute: String, type: OrderType = OrderType.ASC): ResultBuilder {
            val expression = FluentDaoHelper.toExpressionField(from, attribute)
            when (type) {
                OrderType.ASC -> query.orderBy(builder.asc(expression))
                OrderType.DESC -> query.orderBy(builder.desc(expression))
            }
            return this
        }

        @Suppress("unused")
        fun orderBy(attributeList: List<Ordenacao>): ResultBuilder {
            val orderList = ArrayList<Order>()

            for (ordem in attributeList) {
                val expression = FluentDaoHelper.toExpressionField(from, ordem.fieldName)
                if (ordem.tipo.toUpperCase() == "ASC") {
                    orderList.add(
                        builder.asc(expression)
                    )
                } else {
                    orderList.add(
                        builder.desc(expression)
                    )
                }
            }
            query.orderBy(orderList)
            return this
        }

        @Suppress("unused", "unchecked_cast")
        fun <Any> findAny(): Any? {
            val results = entityManager.createQuery(query).resultList as MutableList<out Any>?
            var returnValue: Any? = null
            if (results != null) {
                if (results.size > 0)
                    results.stream().findAny().ifPresent { returnValue = it as Any? }
            }
            return returnValue;
        }

        @Suppress("unused", "unchecked_cast")
        fun <Any> findFirst(): Any? {
            val results = entityManager.createQuery(query).resultList as MutableList<out Any>?
            if (results != null) {
                if (results.size > 0)
                    return results.stream().findFirst().get()
            }
            return null;
        }

        @Suppress("unused", "unchecked_cast")
        fun <Any> resultList(): MutableList<out Any>? {
            try {
                return entityManager.createQuery(query).resultList as MutableList<out Any>?
            } catch (ex: SQLRecoverableException) {
                ex.printStackTrace()
                return resultList()
            } catch (ex: EntityNotFoundException) {
                throw EntityNotFoundException(ex.message)
            } catch (ex: PersistenceException) {
                ex.printStackTrace()
                var expName = ex.cause!!::class.java.typeName
                if (expName.equals("org.hibernate.exception.JDBCConnectionException") || expName.equals("org.hibernate.exception.GenericJDBCException")) {
                    var sqlErrorCode = (ex.cause as JDBCException).errorCode
                    if (sqlErrorCode == 17009 || sqlErrorCode == 17002 || sqlErrorCode == 17010)
                        return resultList()
                }
                throw PersistenceException(ex)
            }
        }

        @Suppress("unused", "unchecked_cast")
        fun <Any> resultListNoHint(): MutableList<out Any>? {
            return entityManager.createQuery(query)
                .setHint(QueryHints.HINT_CACHE_MODE, CacheMode.GET)
                .resultList as MutableList<out Any>?
        }

        @Suppress("unused", "unchecked_cast")
        fun <Any> resultListRefreshed(): MutableList<out Any>? {
            var results = entityManager.createQuery(query)
                .resultList as MutableList<out Any>?
            if (results != null)
                results.forEach { JPAUtils.entityManager?.refresh(it) }

            return results;
        }

        @Suppress("unused", "unchecked_cast")
        fun <Any> resultList(limit: Int): MutableList<out Any>? {
            return entityManager.createQuery(query).setMaxResults(limit).resultList as MutableList<out Any>?
        }

        @Suppress("unused", "unchecked_cast")
        fun <Any> singleResult(): Any? {
            return try {
                //if (!entityManager.transaction.isActive) {
                //    entityManager.transaction.begin()
                //}
                entityManager.createQuery(query).singleResult as Any?
            } catch (e: NoResultException) {
                log.error("Exceção: ${e.message}")
                null
            } catch (e: Exception) {
                log.error("Exceção: ${e.message}")
                null
            } catch (ex: PersistenceException) {
                ex.printStackTrace()
                var expName = ex.cause!!::class.java.typeName
                if (expName.equals("org.hibernate.exception.JDBCConnectionException") || expName.equals("org.hibernate.exception.GenericJDBCException")) {
                    var sqlErrorCode = (ex.cause as JDBCException).errorCode
                    if (sqlErrorCode == 17009 || sqlErrorCode == 17002 || sqlErrorCode == 17010)
                        return singleResult()
                }

                throw PersistenceException(ex)
            } finally {
                //if (entityManager.transaction.isActive) {
                ///    entityManager.transaction.rollback()
                //}
            }
        }

        @Suppress("unused", "unchecked_cast")
        fun <Any> singleResultRefreshed(): Any? {
            try {
                //if (!entityManager.transaction.isActive) {
                //    entityManager.transaction.begin()
                //}
                var value = entityManager.createQuery(query).singleResult as Any?
                JPAUtils.entityManager?.refresh(value)
                return value
            } catch (e: NoResultException) {
                log.error("Exceção: ${e.message}")
                return null
            } catch (e: Exception) {
                log.error("Exceção: ${e.message}")
                return null
            }
            //finally {
            //if (entityManager.transaction.isActive) {
            ///    entityManager.transaction.rollback()
            //}
            //}
            //return null
        }
    }

    inner class ExpressionBuilder {

        @Suppress("unchecked_cast")
        private fun getExpression(attribute: String): Path<Any> {
            var expression = from as Path<Any>
            if (attribute.contains(">")) {
                val joinSeparate = attribute.split(">")
                expression =
                    joins.get(joinSeparate[0])?.let { FluentDaoHelper.toExpressionField(it, joinSeparate[1]) }!!
            } else {
                expression = FluentDaoHelper.toExpressionField(from, attribute)
            }

            return expression
        }

        private var innerPredicate: Predicate = builder.and()

        private fun add(predicateNew: Predicate, tipo: TipoExpressao) {
            predicate = when (tipo) {
                AND -> builder.and(predicate, predicateNew)
                OR -> builder.or(predicate, predicateNew)
            }
        }

        private fun addInner(predicateNew: Predicate, tipo: TipoExpressao) {
            innerPredicate = when (tipo) {
                AND -> builder.and(innerPredicate, predicateNew)
                OR -> builder.or(innerPredicate, predicateNew)
            }
        }

        fun and(block: (expressions: ExpressionBuilder) -> Any): ExpressionBuilder {
            block(this)
            add(innerPredicate, AND)
            return this
        }

        fun or(block: (expressions: ExpressionBuilder) -> Any): ExpressionBuilder {
            block(this)
            add(innerPredicate, OR)
            return this
        }

        fun equal(attribute: String, valor: Any?, tipo: TipoExpressao, inner: Boolean = false): ExpressionBuilder {
            val expression = getExpression(attribute)

            if (inner.not())
                add(builder.equal(expression, valor), tipo)
            else
                addInner(builder.equal(expression, valor), tipo)
            return this
        }

        fun equal(attribute: String, valor: Any?): ExpressionBuilder {
            return equal(attribute, valor, AND, false)
        }

        fun equal(
            attribute: String,
            valor: Any?,
            tipo: TipoExpressao = AND,
            block: (condicao: Boolean) -> Any = { true }
        ): ExpressionBuilder {
            val condicao = block(true) as Boolean
            return if (condicao) {
                equal(attribute, valor, tipo, false)
            } else {
                this
            }
        }

        @Suppress("unused")
        fun equalTo(attributeA: String, attributeB: String, tipo: TipoExpressao = AND): ExpressionBuilder {
            val expressionA = FluentDaoHelper.toExpressionField(from, attributeA)
            val expressionB = FluentDaoHelper.toExpressionField(from, attributeB)

            val temp = builder.equal(expressionA, expressionB)
            add(temp, tipo)
            return this
        }

        @Suppress("unused")
        fun equalTo(attributeA: String, attributeB: String): ExpressionBuilder {
            return equalTo(attributeA, attributeB, AND)
        }

        @Suppress("unused")
        fun equalTo(
            attributeA: String,
            attributeB: String,
            tipo: TipoExpressao = AND,
            block: (condicao: Boolean?) -> Any = { true }
        ): ExpressionBuilder {
            val condicao = block(true) as Boolean
            return if (condicao) {
                equalTo(attributeA, attributeB, tipo)
            } else {
                this
            }
        }

        @Suppress("unused")
        fun notEqual(attribute: String, valor: Any?, tipo: TipoExpressao = AND): ExpressionBuilder {
            val expression = getExpression(attribute)
            val temp = builder.notEqual(expression, valor)
            add(temp, tipo)
            return this
        }

        @Suppress("unused")
        fun notEqual(attribute: String, valor: Any?): ExpressionBuilder {
            return notEqual(attribute, valor, AND)
        }

        @Suppress("unused")
        fun notEqual(
            attribute: String,
            valor: Any?,
            tipo: TipoExpressao = AND,
            block: (condicao: Boolean) -> Any = { true }
        ): ExpressionBuilder {
            val condicao = block(true) as Boolean
            return if (condicao) {
                notEqual(attribute, valor, tipo)
            } else {
                this
            }
        }

        @Suppress("unused")
        fun notEqualTo(attributeA: String, attributeB: String, tipo: TipoExpressao = AND): ExpressionBuilder {
            val expressionA = FluentDaoHelper.toExpressionField(from, attributeA)
            val expressionB = FluentDaoHelper.toExpressionField(from, attributeB)
            val temp = builder.notEqual(expressionA, expressionB)
            add(temp, tipo)
            return this
        }

        @Suppress("unused")
        fun notEqualTo(attributeA: String, attributeB: String): ExpressionBuilder {
            return notEqualTo(attributeA, attributeB, AND)
        }

        @Suppress("unused")
        fun notEqualTo(
            attributeA: String,
            attributeB: String,
            tipo: TipoExpressao = AND,
            block: (condicao: Boolean) -> Any = { true }
        ): ExpressionBuilder {
            val condicao = block(true) as Boolean
            return if (condicao) {
                notEqualTo(attributeA, attributeB, tipo)
            } else {
                this
            }
        }

        @Suppress("unused", "unchecked_cast")
        fun like(attribute: String, valor: String, tipo: TipoExpressao = AND): ExpressionBuilder {
            val v = if (valor.startsWith("%") || valor.endsWith("%")) {
                valor
            } else {
                "%$valor%"
            }
            val expression = getExpression(attribute)
            val temp = builder.like(builder.upper(expression as Expression<String>), v.toUpperCase())
            add(temp, tipo)
            return this
        }

        @Suppress("unused", "unchecked_cast")
        fun like(attribute: String, valor: String): ExpressionBuilder {
            return like(attribute, valor, AND)
        }

        @Suppress("unused", "unchecked_cast")
        fun like(
            attribute: String,
            valor: String,
            tipo: TipoExpressao = AND,
            block: (condicao: Boolean) -> Any = { true }
        ): ExpressionBuilder {
            val condicao = block(true) as Boolean
            return if (condicao) {
                like(attribute, valor, tipo)
            } else {
                this
            }
        }

        @Suppress("unused", "unchecked_cast")
        fun notLike(attribute: String, valor: String, tipo: TipoExpressao = AND): ExpressionBuilder {
            val v = if (valor.startsWith("%") || valor.endsWith("%")) {
                valor
            } else {
                "%$valor%"
            }
            val expression = getExpression(attribute)
            val temp = builder.notLike(builder.upper(expression as Expression<String>), v.toUpperCase())
            add(temp, tipo)
            return this
        }

        @Suppress("unused", "unchecked_cast")
        fun notLike(attribute: String, valor: String): ExpressionBuilder {
            return notLike(attribute, valor, AND)
        }

        @Suppress("unused", "unchecked_cast")
        fun notLike(
            attribute: String,
            valor: String,
            tipo: TipoExpressao = AND,
            block: (condicao: Boolean) -> Any = { true }
        ): ExpressionBuilder {
            val condicao = block(true) as Boolean
            return if (condicao) {
                notLike(attribute, valor, tipo)
            } else {
                this
            }
        }

        @Suppress("unused")
        fun isNotNull(attribute: String, tipo: TipoExpressao = AND): ExpressionBuilder {
            val expression = FluentDaoHelper.toExpressionField(from, attribute)
            val temp = builder.isNotNull(expression)
            add(temp, tipo)
            return this
        }

        @Suppress("unused")
        fun isNotNull(attribute: String): ExpressionBuilder {
            return isNotNull(attribute, AND)
        }

        @Suppress("unused")
        fun isNotNull(
            attribute: String,
            tipo: TipoExpressao = AND,
            block: (condicao: Boolean) -> Any = { true }
        ): ExpressionBuilder {
            val condicao = block(true) as Boolean
            return if (condicao) {
                isNotNull(attribute, tipo)
            } else {
                this
            }
        }

        @Suppress("unused")
        fun isNull(attribute: String, tipo: TipoExpressao = AND): ExpressionBuilder {
            val expression = FluentDaoHelper.toExpressionField(from, attribute)
            val temp = builder.isNull(expression)
            add(temp, tipo)
            return this
        }

        @Suppress("unused")
        fun isNull(attribute: String): ExpressionBuilder {
            return isNull(attribute, AND)
        }

        @Suppress("unused")
        fun isNull(
            attribute: String,
            tipo: TipoExpressao = AND,
            block: (condicao: Boolean) -> Any = { true }
        ): ExpressionBuilder {
            val condicao = block(true) as Boolean
            return if (condicao) {
                isNull(attribute, tipo)
            } else {
                this
            }
        }

        @Suppress("unused", "unchecked_cast")
        fun between(attribute: String, begin: Any, end: Any): ExpressionBuilder {
            return between(attribute, begin, end, AND)
        }

        @Suppress("unused", "unchecked_cast")
        fun between(
            attribute: String,
            begin: Any?,
            end: Any?,
            tipo: TipoExpressao = AND,
            block: (condicao: Boolean) -> Any = { true }
        ): ExpressionBuilder {
            val condicao = block(true) as Boolean
            return if (condicao) {
                between(attribute, begin, end, tipo)
            } else {
                this
            }
        }

        @Suppress("unused", "unchecked_cast")
        fun between(attribute: String, begin: Any?, end: Any?, tipo: TipoExpressao = AND): ExpressionBuilder {
            val expression = FluentDaoHelper.toExpressionField(from, attribute)
            val temp = builder.between(
                expression as Expression<out Comparable<Any>>,
                begin as Comparable<Any>,
                end as Comparable<Any>
            )
            add(temp, tipo)
            return this
        }

        @Suppress("unused", "unchecked_cast")
        fun between(attributeBegin: String, attributeEnd: String, value: Any): ExpressionBuilder {
            return between(attributeBegin, attributeEnd, value, AND)
        }

        @Suppress("unused", "unchecked_cast")
        fun between(
            attributeBegin: String,
            attributeEnd: String,
            value: Any,
            tipo: TipoExpressao = AND,
            block: (condicao: Boolean) -> Any = { true }
        ): ExpressionBuilder {
            val condicao = block(true) as Boolean
            return if (condicao) {
                between(attributeBegin, attributeEnd, value, tipo)
            } else {
                this
            }
        }

        @Suppress("unused", "unchecked_cast")
        fun between(
            attributeBegin: String,
            attributeEnd: String,
            value: Any,
            tipo: TipoExpressao = AND
        ): ExpressionBuilder {
            val expressionBegin = FluentDaoHelper.toExpressionField(from, attributeBegin)
            val expressionEnd = FluentDaoHelper.toExpressionField(from, attributeEnd)
            val temp = builder.between(
                value as Expression<out Comparable<Any>>,
                expressionBegin as Expression<out Comparable<Any>>,
                expressionEnd as Expression<out Comparable<Any>>
            )
            add(temp, tipo)
            return this
        }

        @Suppress("unused")
        fun isIn(
            attribute: String,
            valores: Array<Any>,
            tipo: TipoExpressao = AND,
            inner: Boolean = false
        ): ExpressionBuilder {
//            val expression = FluentDaoHelper.toExpressionField(from, attribute)
            val expression = getExpression(attribute)
            val temp = expression.`in`(*valores)
            if (inner.not())
                add(temp, tipo)
            else
                addInner(temp, tipo)
            return this
        }

        @Suppress("unused")
        fun isIn(attribute: String, valores: Array<Any>): ExpressionBuilder {
            return isIn(attribute, valores, AND, false)
        }

        @Suppress("unused")
        fun isIn(
            attribute: String,
            valores: Array<Any>,
            tipo: TipoExpressao = AND,
            block: (condicao: Boolean) -> Any = { true }
        ): ExpressionBuilder {
            val condicao = block(true) as Boolean
            return if (condicao) {
                isIn(attribute, valores, tipo, false)
            } else {
                this
            }
        }

        @Suppress("unused")
        fun notIn(attribute: String, valores: Array<Any>, tipo: TipoExpressao = AND): ExpressionBuilder {
//            val expression = FluentDaoHelper.toExpressionField(from, attribute)
            val expression = getExpression(attribute)
            val temp = expression.`in`(*valores).not()
            add(temp, tipo)
            return this
        }

        @Suppress("unused")
        fun notIn(attribute: String, valores: Array<Any>): ExpressionBuilder {
            return notIn(attribute, valores, AND)
        }

        @Suppress("unused")
        fun notIn(
            attribute: String,
            valores: Array<Any>,
            tipo: TipoExpressao = AND,
            block: (condicao: Boolean) -> Any = { true }
        ): ExpressionBuilder {
            val condicao = block(true) as Boolean
            return if (condicao) {
                notIn(attribute, valores, tipo)
            } else {
                this
            }
        }

        @Suppress("unused", "unchecked_cast")
        fun greaterThan(attribute: String, valor: Any, tipo: TipoExpressao = AND): ExpressionBuilder {
            val expression = FluentDaoHelper.toExpressionField(from, attribute)
            val temp = builder.greaterThan(
                expression as Expression<out Comparable<Any>>,
                builder.literal(valor) as Expression<out Comparable<Any>>
            )
            add(temp, tipo)
            return this
        }

        @Suppress("unused", "unchecked_cast")
        fun greaterThan(attribute: String, valor: Any): ExpressionBuilder {
            return greaterThan(attribute, valor, AND)
        }

        @Suppress("unused", "unchecked_cast")
        fun greaterThan(
            attribute: String,
            valor: Any,
            tipo: TipoExpressao = AND,
            block: (condicao: Boolean) -> Any = { true }
        ): ExpressionBuilder {
            val condicao = block(true) as Boolean
            return if (condicao) {
                greaterThan(attribute, valor, tipo)
            } else {
                this
            }
        }

        @Suppress("unused", "unchecked_cast")
        fun greaterThanOrEqualTo(attribute: String, valor: Any, tipo: TipoExpressao = AND): ExpressionBuilder {
            val expression = FluentDaoHelper.toExpressionField(from, attribute)
            val temp = builder.greaterThanOrEqualTo(
                expression as Expression<out Comparable<Any>>,
                builder.literal(valor) as Expression<out Comparable<Any>>
            )
            add(temp, tipo)
            return this
        }

        @Suppress("unused", "unchecked_cast")
        fun greaterThanOrEqualTo(attribute: String, valor: Any): ExpressionBuilder {
            return greaterThanOrEqualTo(attribute, valor, AND)
        }

        @Suppress("unused", "unchecked_cast")
        fun greaterThanOrEqualTo(
            attribute: String,
            valor: Any,
            tipo: TipoExpressao = AND,
            block: (condicao: Boolean) -> Any = { true }
        ): ExpressionBuilder {
            val condicao = block(true) as Boolean
            return if (condicao) {
                greaterThanOrEqualTo(attribute, valor, tipo)
            } else {
                this
            }
        }

        @Suppress("unused", "unchecked_cast")
        fun lessThan(attribute: String, valor: Any, tipo: TipoExpressao = AND): ExpressionBuilder {
            val expression = FluentDaoHelper.toExpressionField(from, attribute)
            val temp = builder.lessThan(
                expression as Expression<out Comparable<Any>>,
                builder.literal(valor) as Expression<out Comparable<Any>>
            )
            add(temp, tipo)
            return this
        }

        @Suppress("unused", "unchecked_cast")
        fun lessThan(attribute: String, valor: Any): ExpressionBuilder {
            return lessThan(attribute, valor, AND)
        }

        @Suppress("unused", "unchecked_cast")
        fun lessThan(
            attribute: String,
            valor: Any,
            tipo: TipoExpressao = AND,
            block: (condicao: Boolean) -> Any = { true }
        ): ExpressionBuilder {
            val condicao = block(true) as Boolean
            return if (condicao) {
                lessThan(attribute, valor, tipo)
            } else {
                this
            }
        }

        @Suppress("unused", "unchecked_cast")
        fun lessThanOrEqualTo(attribute: String, valor: Any, tipo: TipoExpressao = AND): ExpressionBuilder {
            val expression = FluentDaoHelper.toExpressionField(from, attribute)
            val temp = builder.lessThanOrEqualTo(
                expression as Expression<out Comparable<Any>>,
                builder.literal(valor) as Expression<out Comparable<Any>>
            )
            add(temp, tipo)
            return this
        }

        @Suppress("unused", "unchecked_cast")
        fun lessThanOrEqualTo(attribute: String, valor: Any): ExpressionBuilder {
            return lessThanOrEqualTo(attribute, valor, AND)
        }

        @Suppress("unused", "unchecked_cast")
        fun lessThanOrEqualTo(
            attribute: String,
            valor: Any,
            tipo: TipoExpressao = AND,
            block: (condicao: Boolean) -> Any = { true }
        ): ExpressionBuilder {
            val condicao = block(true) as Boolean
            return if (condicao) {
                lessThanOrEqualTo(attribute, valor, tipo)
            } else {
                this
            }
        }

//        fun andContains(attribute: Attribute, value: Any?): SimpleSelectBuilder<E?>? {
//            val expression: Expression = getExpression<Any>(attribute, root)
//            this.predicates.add(criteriaBuilder.isMember(value, expression))
//            return this
//        }
    }

    inner class AgregateQuery() {

        var expressionColumns: MutableList<Expression<Number>> = ArrayList<Expression<Number>>()

        @Suppress("unused", "unchecked_cast")
        fun max(attribute: String): AgregateQuery {
            expressionColumns.add(
                builder.max(
                    FluentDaoHelper.toExpressionField(
                        from,
                        attribute
                    ) as Path<Number>
                ) as Expression<Number>
            )
            return this
        }

        @Suppress("unused", "unchecked_cast")
        fun min(attribute: String): AgregateQuery {
            expressionColumns.add(
                builder.min(
                    FluentDaoHelper.toExpressionField(
                        from,
                        attribute
                    ) as Path<Number>
                ) as Expression<Number>
            )
            return this
        }

        @Suppress("unused", "unchecked_cast")
        fun count(attribute: String): AgregateQuery {
            expressionColumns.add(
                builder.count(
                    FluentDaoHelper.toExpressionField(
                        from,
                        attribute
                    ) as Path<Number>
                ) as Expression<Number>
            )
            return this
        }

        @Suppress("unused", "unchecked_cast")
        fun countDistinct(attribute: String): AgregateQuery {
            expressionColumns.add(
                builder.countDistinct(
                    FluentDaoHelper.toExpressionField(
                        from,
                        attribute
                    ) as Path<Number>
                ) as Expression<Number>
            )
            return this
        }

        @Suppress("unused", "unchecked_cast")
        fun sum(attribute: String): AgregateQuery {
            expressionColumns.add(
                builder.sum(
                    FluentDaoHelper.toExpressionField(
                        from,
                        attribute
                    ) as Path<Number>
                ) as Expression<Number>
            )
            return this
        }

        @Suppress("unused", "unchecked_cast")
        fun avg(attribute: String): AgregateQuery {
            expressionColumns.add(
                builder.avg(
                    FluentDaoHelper.toExpressionField(
                        from,
                        attribute
                    ) as Path<Number>
                ) as Expression<Number>
            )
            return this
        }
    }
}