package sysdeliz2.models.sysdeliz

import java.math.BigDecimal
import javax.persistence.*
import kotlin.jvm.Transient

/**
 * @author lima.joao
 * @since 17/07/2019 08:35
 */
@Entity
@Table(name = "SD_CIDADES_001")
@Suppress("unused")
class SdCidadeBean {

    @Id
    @Column(name = "CODIGO")
    var codigo: String? = null

    @Column(name = "UF")
    var uf: String? = null

    @Column(name = "SIGLA_UF")
    var siglaUf: String? = null

    @Column(name = "PAIS")
    var pais: String? = null

    @Column(name = "IPC")
    var ipc: BigDecimal? = BigDecimal.ZERO

    @Column(name = "POPULACAO")
    var populacao: Int? = null

    @Column(name = "POPULACAO_0_4")
    var polulacao0_4: Int? = null

    @Column(name = "POPULACAO_5_9")
    var populacao5_9: Int? = null

    @Column(name = "POPULACAO_10_14")
    var populacao10_14: Int? = null

    @Column(name = "POPULACAO_15_19")
    var populacao15_19: Int? = null

    @Column(name = "POPULACAO_20_29")
    var populacao20_29: Int? = null

    @Column(name = "POPULACAO_30_49")
    var populacao30_49: Int? = null

    @Column(name = "POPULACAO_50")
    var populacao50: Int? = null

    @Column(name = "HOMENS")
    var homens: BigDecimal? = BigDecimal.ZERO

    @Column(name = "MULHERES")
    var mulheres: BigDecimal? = BigDecimal.ZERO

    @Column(name = "URBANA")
    var urbana: BigDecimal? = BigDecimal.ZERO

    @Column(name = "RURAL")
    var rural: BigDecimal? = BigDecimal.ZERO

    @Column(name = "ALFABETIZACAO")
    var alfabetizacao: BigDecimal? = BigDecimal.ZERO

    @Column(name = "PER_CAPITA")
    var perCapita: BigDecimal? = BigDecimal.ZERO

    @Column(name = "VESTUARIO")
    var vestuario: BigDecimal? = BigDecimal.ZERO

    @Column(name = "CLASSE_A1")
    var classeA1: Int? = null

    @Column(name = "CLASSE_A2")
    var classeA2: Int? = null

    @Column(name = "CLASSE_B1")
    var classeB1: Int? = null

    @Column(name = "CLASSE_B2")
    var classeB2: Int? = null

    @Column(name = "CLASSE_C1")
    var classeC1: Int? = null

    @Column(name = "CLASSE_C2")
    var classeC2: Int? = null

    @Column(name = "CLASSE_D")
    var classeD: Int? = null

    @Column(name = "CLASSE_E")
    var classeE: Int? = null

    @Column(name = "ICC")
    var icc: BigDecimal? = BigDecimal.ZERO

    @Column(name = "SHOPPINGS")
    var shoppings: Int? = null

    @Column(name = "MICRO")
    var micro: String? = null

    @Column(name = "IRRADIACAO")
    var irradiacao: String? = null

    @Column(name = "INFORMACOES_MERCADO")
    var informacoesMercado: String? = null

    @Column(name = "DENSIDADE_DEMOGRAFICA")
    var densidadeDemografica: BigDecimal? = BigDecimal.ZERO

    @Column(name = "TAXA_CRESCIMENTO")
    var taxaCrescimento: BigDecimal? = BigDecimal.ZERO

    @Column(name = "VEICULOS")
    var veiculos: Int? = null

    @Column(name = "AGENCIA_BANCARIA")
    var agenciaBancaria: Int? = null

    @Column(name = "INDUSTRIAS")
    var industrias: Int? = null

    @Column(name = "ESTABELECIMENTO_COMERCIAL")
    var estabelecimentoComercial: Int? = null

    @Column(name = "RANKING_NACIONAL")
    var rankingNacional: Int? = null

    @Column(name = "RANKING_ESTADUAL")
    var rankingEstadual: Int? = null

    @Column(name = "CDWK")
    var cdwk: Int? = null

    @Column(name = "CONSUMO_VEST_A1")
    var consumoVestuarioA1: Int? = null

    @Column(name = "CONSUMO_VEST_A2")
    var consumoVestuarioA2: Int? = null

    @Column(name = "CONSUMO_VEST_B1")
    var consumoVestuarioB1: Int? = null

    @Column(name = "CONSUMO_VEST_B2")
    var consumoVestuarioB2: Int? = null

    @Column(name = "CONSUMO_VEST_C1")
    var consumoVestuarioC1: Int? = null

    @Column(name = "CONSUMO_VEST_C2")
    var consumoVestuarioC2: Int? = null

    @Column(name = "CONSUMO_VEST_D")
    var consumoVestuarioD: Int? = null

    @Column(name = "CONSUMO_VEST_E")
    var consumoVestuarioE: Int? = null

    @Column(name = "DISTANCIA_CAPITAL")
    var distanciaCapital:Int? = null

    @Column(name = "VENDIDO_VESTUARIO")
    var vendidoVestuario: BigDecimal? = BigDecimal.ZERO

    @Column(name = "COTA_CLIENTES")
    var cotaClientes: Int? = null

    @Column(name = "ZONA_FRANCA")
    var zonaFranca: String? = null

    @Column(name = "LATITUDE")
    var latitude: BigDecimal? = BigDecimal.ZERO

    @Column(name = "LONGITUDE")
    var longitude: BigDecimal? = BigDecimal.ZERO

    @Column(name = "NOME")
    var nome: String? = null

    @Transient
    var selected: Boolean = false

    override fun toString(): String {
        return "SdCidade001{codigo=$codigo, siglaUf=$siglaUf, nome=$nome}"
    }
}