package sysdeliz2.models.sysdeliz

import sysdeliz.apis.web.convertr.data.models.customers.Customer
import sysdeliz2.models.generics.BasicModel
import sysdeliz2.utils.sys.annotations.ColunaFilter
import sysdeliz2.utils.sys.annotations.ExibeTableView
import sysdeliz2.utils.sys.annotations.TelaSysDeliz
import javax.persistence.*

/**
 *
 * @author lima.joao
 * @since 14/01/2020 11:19
 */
@Entity
@Table(name="SD_MAGENTO_CLIENTE_001")
@TelaSysDeliz(descricao = "Cliente", icon = "cliente (4).png")
class SdMagentoClienteBean : BasicModel() {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SD_MAGENTO_CLIENTE_001")
    @SequenceGenerator(name = "SEQ_SD_MAGENTO_CLIENTE_001", sequenceName = "SEQ_SD_MAGENTO_CLIENTE_001", allocationSize = 1)
    @ExibeTableView(descricao = "Código", width = 60)
    @ColunaFilter(descricao = "Código", coluna = "id")
    @Column(name = "ID")
    var id: Int? = null

    @ExibeTableView(descricao = "Nome", width = 200)
    @ColunaFilter(descricao = "Nome", coluna = "firstname")
    @Column(name="FIRSTNAME")
    var firstname: String? = null

    @Column(name="LASTNAME")
    var lastname: String? = null

    @Column(name= "EMAIL")
    var email: String? = null

    @Column(name="TAXVAT")
    var taxvat: String? = null

    @ExibeTableView(descricao = "RG", width = 80)
    @ColunaFilter(descricao = "RG", coluna = "rg")
    @Column(name="RG")
    var rg: String? = null

    @Column(name="DT_NASC")
    var dtNasc: String? = null

    companion object {
        fun fromCustomerMagento(customer: Customer): SdMagentoClienteBean{
            val cliente = SdMagentoClienteBean()

            with(cliente){
                firstname = customer.firstname
                lastname = customer.lastname
                email = customer.email
                taxvat = customer.cpfCnpj
                rg = customer.rg
                dtNasc = customer.dtNasc
            }
            return cliente
        }
    }
}