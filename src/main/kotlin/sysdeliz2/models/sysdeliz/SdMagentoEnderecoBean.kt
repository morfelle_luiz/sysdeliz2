package sysdeliz2.models.sysdeliz

import sysdeliz.apis.web.convertr.data.models.customers.Address
import java.io.Serializable
import javax.persistence.*

/**
 *
 * @author lima.joao
 * @since 14/01/2020 10:45
 */
@Entity
@Table(name = "SD_MAGENTO_ENDERECO_001")
class SdMagentoEnderecoBean : Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SD_MAGENTO_ENDERECO_001")
    @SequenceGenerator(name = "SEQ_SD_MAGENTO_ENDERECO_001", sequenceName = "SEQ_SD_MAGENTO_ENDERECO_001", allocationSize = 1)
    @Column(name = "ID")
    var id: Int? = null
    @Column(name = "RUA")
    var street: String? = null
    @Column(name = "NUMERO")
    var number: String? = null
    @Column(name = "COMPLEMENTO")
    var complement: String? = null
    @Column(name = "BAIRRO")
    var neighborhood: String? = null
    @Column(name = "CEP")
    var postcode: String? = null
    @Column(name = "CIDADE")
    var city: String? = null
    @Column(name = "REGIAO")
    var region: String? = null
    @Column(name = "TELEFONE")
    var telephone: String? = null

    companion object {
        fun fromAddressMagento(address: Address): SdMagentoEnderecoBean {
            val endereco = SdMagentoEnderecoBean()

            with(endereco){
                street = address.street
                number = address.number
                complement = address.complement
                neighborhood = address.neighborhood
                postcode = address.postcode
                city = address.city
                region = address.region
                telephone = address.telephone
            }

            return endereco
        }
    }
}