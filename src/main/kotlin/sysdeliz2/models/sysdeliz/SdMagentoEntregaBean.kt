package sysdeliz2.models.sysdeliz

import sysdeliz.apis.web.convertr.data.models.orders.Shipping
import java.math.BigDecimal
import javax.persistence.*

/**
 *
 * @author lima.joao
 * @since 14/01/2020 11:19
 */
@Entity
@Table(name="SD_MAGENTO_ENTREGA_001")
class SdMagentoEntregaBean {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SD_MAGENTO_ENTREGA_001")
    @SequenceGenerator(name = "SEQ_SD_MAGENTO_ENTREGA_001", sequenceName = "SEQ_SD_MAGENTO_ENTREGA_001", allocationSize = 1)
    @Column(name = "ID")
    var id: Int? = null

    @Column(name="NOME")
    var firstname: String? = null

    @Column(name="SOBRENOME")
    var lastname: String? = null

    @Column(name="DESCRICAO")
    var description: String? = null

    @Column(name="TOTAL")
    var total: BigDecimal? = BigDecimal.ZERO

    @OneToOne()
    @JoinColumn(name = "ADDRESS_ID")
    var address: SdMagentoEnderecoBean? = SdMagentoEnderecoBean()

    companion object {
        fun fromShippingMagento(shipping: Shipping): SdMagentoEntregaBean{
            val entrega = SdMagentoEntregaBean()

            with(entrega){
                firstname = shipping.firstname
                lastname = shipping.lastname
                description = shipping.description
                total = shipping.total

                address = SdMagentoEnderecoBean.fromAddressMagento(shipping.address)
            }
            return entrega
        }
    }

    @Transient
    var typeFrete: String? = ""
        get(){
            return if(description?.contains("SEDEX") == true){
                "SEDEX"
            } else if(description?.contains("PAC") == true) {
                "PAC"
            } else {
                "GRÁTIS"
            }
        }

}