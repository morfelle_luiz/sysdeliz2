package sysdeliz2.models.sysdeliz

import sysdeliz.apis.web.convertr.data.models.orders.Order
import sysdeliz2.models.ti.TabPrz
import sysdeliz2.models.ti.TabPrzBean
import tornadofx.*
import java.io.Serializable
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import javax.persistence.*

/**
 *
 * @author lima.joao
 * @since 14/01/2020 13:49
 */
@Entity
@Table(name = "SD_MAGENTO_ORDER_001")
class SdMagentoOrderBean() : Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SD_MAGENTO_ORDER_001")
    @SequenceGenerator(name = "SEQ_SD_MAGENTO_ORDER_001", sequenceName = "SEQ_SD_MAGENTO_ORDER_001", allocationSize = 1)
    @Column(name = "ID")
    var id: Int? = null

    @Column(name = "ORDER_ID")
    var orderId: String? = null

    @Column(name = "STATUS")
    var status: String? = null

    @Column(name = "CREATED_AT")
    var createdAt: String? = null

    @OneToOne()
    @JoinColumn(name = "CUSTOMER_ID")
    var customer: SdMagentoClienteBean = SdMagentoClienteBean()

    @OneToOne()
    @JoinColumn(name = "SHIPPING_ID")
    var shipping: SdMagentoEntregaBean = SdMagentoEntregaBean()

    @OneToOne()
    @JoinColumn(name = "PAYMENT_ID")
    var payment: SdMagentoPagamentoBean = SdMagentoPagamentoBean()

    @OneToOne()
    @JoinColumn(name = "TOTALS_ID")
    var totals: SdMagentoTotaisBean = SdMagentoTotaisBean()

    @OneToMany
    @JoinColumn(name = "ORDER_ID")
    var items: MutableList<SdMagentoOrderItemBean> = ArrayList()

    @Column(name = "CODIGO_ERP")
    var codigoErp: String? = null

    @Column(name = "MARCA")
    var marca: String? = null

    @Column(name = "TRACK_ID")
    var trackId: String? = null

    @Column(name = "ORIGEM")
    var origem: String? = null

    @OneToOne
    @JoinColumn(name = "ENTREGA")
    var entrega: TabPrzBean? = null

    companion object {
        fun fromOrderMagento(order: Order, marca: String, origem: String, id: Int? = null): SdMagentoOrderBean {
            val retorno = SdMagentoOrderBean()

            if (id != null) {
                retorno.id = id
            }

            with(retorno) {
                this.orderId = order.orderId
                this.status = order.status
                this.createdAt = order.createdAt
                this.marca = marca
                this.origem = origem
                this.customer = SdMagentoClienteBean.fromCustomerMagento(order.customer!!)
                this.shipping = SdMagentoEntregaBean.fromShippingMagento(order.shipping!!)
                this.payment = SdMagentoPagamentoBean.fromPaymentsMagento(order.payment!!)
                this.totals = SdMagentoTotaisBean.fromTotaisMagento(order.totals!!)

                this.items = ArrayList()
                this.items.addAll(SdMagentoOrderItemBean.fromListOrderItensMagento(order.items))
            }
            return retorno
        }
    }
}

