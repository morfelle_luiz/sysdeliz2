package sysdeliz2.models.sysdeliz

import tornadofx.*

class SdMagentoOrderBeanModel(order: SdMagentoOrderBean) : ItemViewModel<SdMagentoOrderBean>(order) {
    val id = bind(SdMagentoOrderBean::id)
    val orderId = bind(SdMagentoOrderBean::orderId)
    val status = bind(SdMagentoOrderBean::status)
    val createdAt = bind(SdMagentoOrderBean::createdAt)
    val customer = bind(SdMagentoOrderBean::customer)
    val shipping = bind(SdMagentoOrderBean::shipping)
    val payment = bind(SdMagentoOrderBean::payment)
    val totals = bind(SdMagentoOrderBean::totals)
    //val items = bind(SdMagentoOrderBean::items) -- emite erro se tenta vincular
    val codigoErp = bind(SdMagentoOrderBean::codigoErp)
    val marca = bind(SdMagentoOrderBean::marca)
    val trackId = bind(SdMagentoOrderBean::trackId)
}
