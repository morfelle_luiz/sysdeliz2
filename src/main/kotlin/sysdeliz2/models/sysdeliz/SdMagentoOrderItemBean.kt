package sysdeliz2.models.sysdeliz

import sysdeliz.apis.web.convertr.data.models.orders.OrderItem
import sysdeliz2.dao.FluentDao
import sysdeliz2.models.ti.PaItenBean
import sysdeliz2.models.ti.PedReservaBean
import sysdeliz2.models.ti.Pedido3Bean
import java.math.BigDecimal
import javax.persistence.*

/**
 *
 * @author lima.joao
 * @since 14/01/2020 13:49
 */
@Entity
@Table(name="SD_MAGENTO_ORDER_ITENS_001")
class SdMagentoOrderItemBean {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SD_MAGENTO_ORDER_ITEM_001")
    @SequenceGenerator(name = "SEQ_SD_MAGENTO_ORDER_ITEM_001", sequenceName = "SEQ_SD_MAGENTO_ORDER_ITEM_001", allocationSize = 1)
    @Column(name = "ID")
    var id: Int? = null

    @Column(name="SKU")
    var sku: String? = null

    @Column(name="NAME")
    var name: String? = null

    @Column(name="QTY")
    var qty: BigDecimal? = BigDecimal.ONE

    @Column(name="PRICE")
    var price: BigDecimal? = BigDecimal.ONE

    @Column(name="DISCOUNT_AMOUNT")
    var discountAmount: BigDecimal? = BigDecimal.ONE

    @Column(name="ORDER_ID")
    var orderId: Int? = null

    @Transient
    var reservado: Boolean? = true

    companion object {
        fun fromOrderItensMagento(orderItem: OrderItem, owner: SdMagentoOrderBean? = null): SdMagentoOrderItemBean {
            val item = SdMagentoOrderItemBean()

            with (item) {
                sku = orderItem.sku
                name = orderItem.name
                qty = orderItem.qty
                price = orderItem.price
                discountAmount = orderItem.discountAmount

                if(owner != null) {
                    orderId = owner.id
                }
            }
            return item
        }

        fun fromListOrderItensMagento(itens: List<OrderItem>): List<SdMagentoOrderItemBean>{
            val list = ArrayList<SdMagentoOrderItemBean>()

            for(item in itens){
                list.add(this.fromOrderItensMagento(item))
            }

            return list
        }

        fun setReservadoItem(order: SdMagentoOrderBean) {
            order.items.forEach{
                var itemOrder = it
                var qtdeReserva = FluentDao()
                        .selectFrom(PedReservaBean::class.java)
                        .where {
                            it
                                    .equal("id.numero",  order.codigoErp)
                                    .equal("id.codigo", itemOrder.sku!!.split('-')[0])
                                    .equal("id.cor", itemOrder.sku!!.split('-')[1])
                                    .equal("id.tam", itemOrder.sku!!.split('-')[2])
                        }.resultList<PedReservaBean>()!!.stream()
                        .mapToInt { pedReservaBean -> pedReservaBean.qtde!!.toInt() }
                        .sum()
                qtdeReserva += FluentDao()
                        .selectFrom(Pedido3Bean::class.java)
                        .where {
                            it
                                    .equal("id.numero",  order.codigoErp)
                                    .equal("id.codigo", itemOrder.sku!!.split('-')[0])
                                    .equal("id.cor", itemOrder.sku!!.split('-')[1])
                                    .equal("id.tam", itemOrder.sku!!.split('-')[2])
                        }.resultList<Pedido3Bean>()!!.stream()
                        .mapToInt { pedido3Bean -> pedido3Bean.qtdeF!!.toInt() + pedido3Bean.qtde!!.toInt() }
                        .sum()
                it.reservado = qtdeReserva > 0
            }
        }
    }

}