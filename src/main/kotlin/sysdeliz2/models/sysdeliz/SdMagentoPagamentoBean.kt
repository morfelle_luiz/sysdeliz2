package sysdeliz2.models.sysdeliz

import sysdeliz.apis.web.convertr.data.models.orders.Payment
import javax.persistence.*

/**
 *
 * @author lima.joao
 * @since 14/01/2020 11:19
 */
@Entity
@Table(name="SD_MAGENTO_PAGAMENTOS_001")
class SdMagentoPagamentoBean {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SD_MAGENTO_PAGAMENTOS_001")
    @SequenceGenerator(name = "SEQ_SD_MAGENTO_PAGAMENTOS_001", sequenceName = "SEQ_SD_MAGENTO_PAGAMENTOS_001", allocationSize = 1)
    @Column(name = "ID")
    var id: Int? = null

    @Column(name="CC_TYPE")
    var ccType: String? = null

    @Column(name="CC_EXP_MONTH")
    var ccExpMonth: String? = null

    @Column(name= "CC_EXP_YEAR")
    var ccExpYear: String? = null

    @Column(name="CC_INSTALLMENTS")
    var ccInstallments: String? = null

    @Column(name="ACQUIRER")
    var acquirer: String? = null

    @Column(name="AUTHORIZATION_CODE")
    var authorizationCode: String? = null

    @Column(name="CC_NUMBER")
    var ccNumber: String? = null

    @Column(name="NSU")
    var nsu: String? = null

    @Column(name="METHOD")
    var method: String? = null

    companion object {
        fun fromPaymentsMagento(payment: Payment): SdMagentoPagamentoBean {
            val pagamento = SdMagentoPagamentoBean()

            with(pagamento){
                ccType = payment.ccType
                ccExpMonth = payment.ccExpMonth
                ccExpYear = payment.ccExpYear
                ccInstallments = payment.ccInstallments
                acquirer = payment.acquirer
                method = payment.method
                authorizationCode = payment.authorizationCode
                ccNumber = payment.ccNumber
                nsu = payment.nsu
            }
            return pagamento
        }
    }
}