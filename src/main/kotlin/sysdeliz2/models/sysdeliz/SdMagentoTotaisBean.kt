package sysdeliz2.models.sysdeliz

import sysdeliz.apis.web.convertr.data.models.orders.Totals
import java.math.BigDecimal
import javax.persistence.*

/**
 *
 * @author lima.joao
 * @since 14/01/2020 11:19
 */
@Entity
@Table(name="SD_MAGENTO_TOTAIS_001")
class SdMagentoTotaisBean {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SD_MAGENTO_TOTAIS_001")
    @SequenceGenerator(name = "SEQ_SD_MAGENTO_TOTAIS_001", sequenceName = "SEQ_SD_MAGENTO_TOTAIS_001", allocationSize = 1)
    @Column(name = "ID")
    var id: Int? = null

    @Column(name="GRANDE_TOTAL")
    var grandTotal: BigDecimal? = BigDecimal.ZERO

    @Column(name="DESCONTO")
    var desconto: BigDecimal? = BigDecimal.ZERO

    companion object{
        fun fromTotaisMagento(totals: Totals): SdMagentoTotaisBean {
            val totais = SdMagentoTotaisBean()

            with (totais){
                grandTotal = totals.grandTotal
                desconto = totals.discount
            }

            return totais
        }
    }


}