package sysdeliz2.models.sysdeliz

import sysdeliz2.models.generics.BasicModel
import sysdeliz2.models.ti.Marca
import sysdeliz2.utils.enums.ColumnType
import sysdeliz2.utils.sys.annotations.ColunaFilter
import sysdeliz2.utils.sys.annotations.ExibeTableView
import sysdeliz2.utils.sys.annotations.TelaSysDeliz
import java.io.Serializable
import javax.persistence.*

/**
 * @author lima.joao
 * @since 21/08/2019 17:18
 */
@Entity
@Table(name = "SD_MARCAS_CONCORRENTES_001")
@TelaSysDeliz(descricao = "Marcas Concorrentes", icon = "marca (4).png")
class SdMarcaConcorrenteBean : BasicModel(), Serializable {

    @Id
    @Column(name = "CODIGO")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_MARCAS_CONCORRENTES_001")
    @SequenceGenerator(sequenceName = "SD_SEQ_MARCAS_CONCORRENTES_001", name = "SEQ_MARCAS_CONCORRENTES_001", initialValue = 1, allocationSize = 1)
    @ExibeTableView(descricao = "Código", width = 80)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    var codigo: Int? = null
        set(value) {
            codigoFilter = value.toString()
            codigo = value
        }

    @Column(name = "NOME")
    @ExibeTableView(descricao = "Descrição", width = 300)
    @ColunaFilter(descricao = "Descrição", coluna = "nome")
    var nome: String? = null
        set(value) {
            nome = value
            descricaoFilter = value
        }

    @Column(name = "ATIVO")
    @ExibeTableView(descricao = "Ativo", width = 40)
    var ativo: String? = null

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODIGO_MARCA")
    @ExibeTableView(descricao = "Nossa Marca", width = 200)
    @ColunaFilter(descricao = "Marca", coluna = "marca.codigo", filterClass = "sysdeliz2.models.ti.Marca")
    var marca: Marca? = null

    init {
        ativo = "S"
    }
}