package sysdeliz2.models.sysdeliz

import tornadofx.*

/**
 *
 * @author lima.joao
 * @since 29/11/2019 08:26
 */
class SdMarcaConcorrenteModel(marcaConcorrente: SdMarcaConcorrenteBean) : ItemViewModel<SdMarcaConcorrenteBean>(marcaConcorrente) {

    val codigo = bind(SdMarcaConcorrenteBean::codigo)
    val nome = bind(SdMarcaConcorrenteBean::nome)
    val ativo = bind(SdMarcaConcorrenteBean::ativo)
    val marca = bind(SdMarcaConcorrenteBean::marca)

    init{
    }
}