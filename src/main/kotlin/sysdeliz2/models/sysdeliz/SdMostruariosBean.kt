package sysdeliz2.models.sysdeliz

import sysdeliz2.utils.converters.LocalDateAttributeConverter
import java.io.Serializable
import java.time.LocalDate
import javax.persistence.*

/**
 *
 * @author lima.joao
 * @since 09/03/2020 08:22
 */
@Entity
@Table(name="SD_MOSTRUARIOS_001")
class SdMostruariosBean: Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SD_MOSTRUARIOS_001")
    @SequenceGenerator(name = "SEQ_SD_MOSTRUARIOS_001", sequenceName = "SEQ_SD_MOSTRUARIOS_001", allocationSize = 1)
    var id: Int? = null

    var barra: String? = null

    @Column(name="CODREP")
    var codRep: String? = null
    @Column(name="ENT_SAI")
    var entSai: String? = null

    var codigo: String? = null
    var tam: String? = null
    var cor: String? = null
    var qtd: Int? = null
    var colecao: String? = null

    @Column(name="DATA_LEITURA", columnDefinition = "DATE")
    @Convert(converter = LocalDateAttributeConverter::class)
    var dataLeitura: LocalDate? = null
}