package sysdeliz2.models.sysdeliz

import java.io.Serializable
import javax.persistence.*

/**
 *
 * @author lima.joao
 * @since 06/01/2020 13:19
 */
@Entity
@Table(name="SD_PRODUTO_001")
class SdProdutoBean : Serializable {

    @Id
    @Column(name="CODIGO")
    var codigo: String? = null
    @Lob
    @Column(name="OBSERVACAO_PCP")
    var observacaoPcp: String? = null
    @Lob
    @Column(name="EXPEDICAO")
    var expedicao: String? = null
    @Column(name = "UNICO_CAIXA", columnDefinition = "Char")
    var unicoCaixa: String? = null
    @Column(name="LOJA_STATUS", columnDefinition = "Char")
    var lojaStatus: String? = null
    @Lob
    @Column(name="META_TITLE")
    var metaTitle: String? = null
    @Lob
    @Column(name="META_KEYWORD")
    var metaKeyword: String? = null
    @Lob
    @Column(name="META_DESCRIPTION")
    var metaDescription: String? = null
    @Lob
    @Column(name="DESCRIPTION")
    var description: String? = null
    @Lob
    @Column(name="SHORT_DESCRIPTION")
    var shortDescription: String? = null
    @Column(name = "STATUS_PROCESSAMENTO", columnDefinition = "Char")
    var statusProcessamento: String? = null

}