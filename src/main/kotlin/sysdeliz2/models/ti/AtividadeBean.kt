package sysdeliz2.models.ti

import tornadofx.*
import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

/**
 * @author lima.joao
 * @since 28/08/2019 15:29
 */
@Entity
@Table(name = "ATIVIDADE_001")
class AtividadeBean : Serializable {
    @Id
    @Column(name = "CODIGO")
    var codigo: String? = null

    @Column(name = "DESCRICAO")
    var descricao: String? = null
}