package sysdeliz2.models.ti

import tornadofx.*

/**
 *
 * @author lima.joao
 * @since 03/03/2020 08:50
 */
class AtividadeBeanModel(atividade: AtividadeBean) : ItemViewModel<AtividadeBean>(atividade) {
    val codigo = bind(AtividadeBean::codigo)
    val descricao = bind(AtividadeBean::descricao)
}