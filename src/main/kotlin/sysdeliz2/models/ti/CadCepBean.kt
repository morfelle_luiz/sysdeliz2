package sysdeliz2.models.ti

import sysdeliz2.models.sysdeliz.SdCidadeBean
import java.io.Serializable
import javax.persistence.*

/**
 * @author lima.joao
 * @since 27/08/2019 17:27
 */
@Entity
@Table(name = "CADCEP_001")
class CadCepBean : Serializable {

    @Id
    @Column(name = "CEP")
    var cep: String? = null

    @OneToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "COD_CID")
    var cidade: SdCidadeBean? = null

    @Column(name = "LOGR_CEP")
    var logrCep: String? = null

    @Column(name = "BAIRRO")
    var bairro: String? = null

}