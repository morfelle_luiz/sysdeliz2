package sysdeliz2.models.ti

import javafx.beans.property.*
import sysdeliz2.models.generics.BasicModel
import sysdeliz2.utils.enums.ColumnType
import sysdeliz2.utils.sys.annotations.ColunaFilter
import sysdeliz2.utils.sys.annotations.ExibeTableView
import sysdeliz2.utils.sys.annotations.TelaSysDeliz
import java.io.Serializable
import java.util.*
import javax.persistence.*

/**
 * @author lima.joao
 * @since 31/07/2019 15:23
 */
@Entity
@Table(name = "COLECAO_001")
@TelaSysDeliz(descricao = "Coleção", icon = "colecao (4).png")
class ColecaoBean : BasicModel, Serializable {
    @Transient
    @ExibeTableView(descricao = "Código", width = 80)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    private val codigo: StringProperty = SimpleStringProperty(this, "codigo")

    @Transient
    @ExibeTableView(descricao = "Descrição", width = 350)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    private val descricao: StringProperty = SimpleStringProperty(this, "descricao")

    @Transient
    private val indice: IntegerProperty = SimpleIntegerProperty(this, "indice")

    @Transient
    private val sequencia: StringProperty = SimpleStringProperty(this, "sequencia")

    @Transient
    private val inicioVig: ObjectProperty<Date> = SimpleObjectProperty(this, "inicioVig")

    @Transient
    private val fimVig: ObjectProperty<Date> = SimpleObjectProperty(this, "fimVig")

    @Transient
    @ExibeTableView(descricao = "Ativo", width = 40, columnType = ColumnType.BOOLEAN)
    private val sdAtivo: StringProperty = SimpleStringProperty(this, "sdAtivo")

    constructor() {}
    constructor(codigo: String) {
        this.codigo.set(codigo)
    }

    @Id
    @Column(name = "CODIGO")
    fun getCodigo(): String {
        return codigo.get()
    }

    @Column(name = "DESCRICAO")
    fun getDescricao(): String {
        return descricao.get()
    }

    @Column(name = "INDICE")
    fun getIndice(): Int {
        return indice.get()
    }

    @Column(name = "SEQUENCIA")
    fun getSequencia(): String {
        return sequencia.get()
    }

    @Column(name = "INICIO_VIG")
    @Temporal(TemporalType.DATE)
    fun getInicioVig(): Date {
        return inicioVig.get()
    }

    @Column(name = "FIM_VIG")
    @Temporal(TemporalType.DATE)
    fun getFimVig(): Date {
        return fimVig.get()
    }

    @Column(name = "SD_ATIVO", columnDefinition = "char")
    fun getSdAtivo(): String {
        return sdAtivo.get()
    }

    fun setCodigo(value: String) {
        codigoFilter = value
        codigo.set(value)
    }

    fun setDescricao(value: String) {
        descricaoFilter = value
        descricao.set(value)
    }

    fun setIndice(value: Int) {
        indice.set(value)
    }

    fun setSequencia(value: String) {
        sequencia.set(value)
    }

    fun setInicioVig(value: Date) {
        inicioVig.set(value)
    }

    fun setFimVig(value: Date) {
        fimVig.set(value)
    }

    fun setSdAtivo(value: String) {
        sdAtivo.set(value)
    }

    fun codigoProperty(): StringProperty {
        return codigo
    }

    fun descricaoProperty(): StringProperty {
        return descricao
    }

    fun indiceProperty(): IntegerProperty {
        return indice
    }

    fun sequenciaProperty(): StringProperty {
        return sequencia
    }

    fun inicioVigProperty(): ObjectProperty<Date> {
        return inicioVig
    }

    fun fimVigProperty(): ObjectProperty<Date> {
        return fimVig
    }

    fun sdAtivoProperty(): StringProperty {
        return sdAtivo
    }

    override fun toString(): String {
        return "[" + codigo.get() + "] " + descricao.get()
    }
}