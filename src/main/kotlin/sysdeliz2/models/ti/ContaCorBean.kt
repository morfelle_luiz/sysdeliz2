package sysdeliz2.models.ti

import sysdeliz2.utils.converters.LocalDateAttributeConverter
import java.io.Serializable
import java.math.BigDecimal
import java.time.LocalDate
import javax.persistence.*

/**
 *
 * @author lima.joao
 * @since 17/02/2020 15:55
 */
@Entity
@Table(name = "CONTACOR_001")
class ContaCorBean: Serializable {


    @EmbeddedId
    var id: ContaCorPK = ContaCorPK()

    @Column(name = "DT_LAN", columnDefinition = "DATE")
    @Convert(converter = LocalDateAttributeConverter::class)
    var dtLan: LocalDate? = null

    @Column(name = "VALOR")
    var valor: BigDecimal? = BigDecimal.ZERO

    @Column(name = "PENDENTE")
    var pendente: String? = null

    @Column(name = "TIPO")
    var tipo: String? = null

    @Column(name = "BANCO")
    var banco: String? = null

    @Column(name = "GRUPO")
    var grupo: String? = null

    @Column(name = "HISTORICO")
    var historico: String? = null

    @Column(name = "OBSERVACAO")
    var observacao: String? = null

    @Column(name = "DT_CONT", columnDefinition = "DATE")
    @Convert(converter = LocalDateAttributeConverter::class)
    var dtCont: LocalDate? = null

    @Column(name = "LANC_CON")
    var lancCon: String? = null

    @Column(name = "DT_VENCTO", columnDefinition = "DATE")
    @Convert(converter = LocalDateAttributeConverter::class)
    var dtVencto: LocalDate? = null

    @Column(name = "TIPO_DOC")
    var tipoDoc: String? = null

    @Column(name = "USU_NUMREG")
    var usuNumReg: Int? = null

    @Column(name = "IMPORT_SAP")
    var importSap: String? = "N"

}