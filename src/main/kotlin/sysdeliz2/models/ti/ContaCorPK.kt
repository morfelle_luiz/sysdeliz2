package sysdeliz2.models.ti

import java.io.Serializable
import javax.persistence.*

/**
 *
 * @author lima.joao
 * @since 17/02/2020 16:00
 */
@Embeddable
class ContaCorPK : Serializable{

    @Column(name = "LANCAMENTO")
    var lancamento: String? = "0"

    @Column(name = "DOCTO")
    var docto: String? = null

    @Column(name = "CONTA")
    var conta: String? = null

    @Column(name = "OPERACAO")
    var operacao: String? = null

    override fun equals(other: Any?): Boolean {
        return super.equals(other)
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }
}