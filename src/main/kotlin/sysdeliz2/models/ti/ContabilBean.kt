package sysdeliz2.models.ti

import sysdeliz2.utils.converters.LocalDateAttributeConverter
import java.io.Serializable
import java.math.BigDecimal
import java.time.LocalDate
import javax.persistence.*

/**
 *
 * @author lima.joao
 * @since 17/02/2020 15:54
 */
@Entity
@Table(name = "CONTABIL_001")
class ContabilBean : Serializable {

    @EmbeddedId
    var id: ContabilBeanPK = ContabilBeanPK()

    @Column(name = "DATA", columnDefinition = "DATE")
    @Convert(converter = LocalDateAttributeConverter::class)
    var data: LocalDate? = null

    @Column(name = "VALOR")
    var valor: BigDecimal? = BigDecimal.ZERO

    @Column(name = "OPERACAO")
    var operacao: String? = null

    @Column(name = "CONTA_C")
    var contaC: String? = null

    @Column(name = "CONTA_D")
    var contaD: String? = null

    @Column(name = "TIPO")
    var tipo: String? = null

    @Column(name = "OBSERVACAO")
    var observacao: String? = null

    @Column(name=  "DOCTO")
    var docto: String? = null

    @Column(name = "NUMERO")
    var numero: String? = null

    @Column(name = "DATA_LAN", columnDefinition = "DATE")
    @Convert(converter = LocalDateAttributeConverter::class)
    var dataLan: LocalDate? = null

    @Column(name = "CONTA_R")
    var contaR: String? = null

    @Column(name = "CODCLI")
    var codCli: String? = null
}