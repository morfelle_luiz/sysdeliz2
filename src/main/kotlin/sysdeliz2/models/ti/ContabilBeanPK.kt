package sysdeliz2.models.ti

import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Embeddable

/**
 *
 * @author lima.joao
 * @since 18/02/2020 08:34
 */
@Embeddable
class ContabilBeanPK: Serializable {

    @Column(name = "LANCAMENTO")
    var lancamento: String = "0"

    @Column(name = "ORDEM")
    var ordem: Int = 0

    override fun equals(other: Any?): Boolean {
        return super.equals(other)
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }
}