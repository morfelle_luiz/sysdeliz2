package sysdeliz2.models.ti

import java.io.Serializable
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

/**
 * @author lima.joao
 * @since 18/09/2019 17:10
 */
@Entity
@Table(name = "CADCOR_001")
class CorBean : Serializable {

    @Id
    var cor: String? = null

    var descricao: String? = null

    var grupo: String? = null

    var pantone: String? = null

    var corbase: String? = null

}