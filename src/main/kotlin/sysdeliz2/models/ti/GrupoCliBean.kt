package sysdeliz2.models.ti

import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.beans.property.StringProperty
import tornadofx.*
import java.math.BigDecimal
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

/**
 * @author lima.joao
 * @since 28/08/2019 16:41
 */
@Entity
@Table(name = "GRUPO_CLI_001")
class GrupoCliBean {

    @Id
    @Column(name = "CODIGO")
    var codigo: String? = null

    @Column(name = "DESCRICAO")
    var descricao: String? = null

    @Column(name = "DESCONTO")
    var desconto: BigDecimal? = null

    @Column(name = "ANALISE")
    var analise: String? = null

}

