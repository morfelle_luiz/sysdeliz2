package sysdeliz2.models.ti

import tornadofx.*

/**
 *
 * @author lima.joao
 * @since 03/03/2020 09:32
 */
class GrupoCliBeanModel(grupoCliBean: GrupoCliBean) : ItemViewModel<GrupoCliBean>(grupoCliBean) {
    val codigo = bind(GrupoCliBean::codigo)
    val descricao = bind(GrupoCliBean::descricao)
    val desconto = bind(GrupoCliBean::desconto)
    val analise = bind(GrupoCliBean::analise)
}
