package sysdeliz2.models.ti

import org.hibernate.annotations.Immutable
import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

/**
 *
 * @author lima.joao
 * @since 27/01/2020 10:31
 */
@Entity
@Table(name="MENSAGEM_001")
@Immutable
class MensagemBean: Serializable {
    @Id
    @Column(name = "CODMEN")
    var codMen: String? = null
    @Column(name = "DESCRICAO")
    var descricao: String? = null
    @Column(name = "PADRAO")
    var padrao: String? = null
}