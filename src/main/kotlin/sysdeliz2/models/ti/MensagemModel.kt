package sysdeliz2.models.ti

import tornadofx.*

/**
 *
 * @author lima.joao
 * @since 27/01/2020 10:31
 */
@Suppress("unused")
class MensagemModel(entidade: MensagemBean): ItemViewModel<MensagemBean>(entidade) {
    val codMen = bind(MensagemBean::codMen)
    val descricao = bind(MensagemBean::descricao)
    val padrao = bind(MensagemBean::padrao)
}