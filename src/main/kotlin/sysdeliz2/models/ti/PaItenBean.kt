package sysdeliz2.models.ti

import java.io.Serializable
import java.math.BigDecimal
import javax.persistence.Column
import javax.persistence.EmbeddedId
import javax.persistence.Entity
import javax.persistence.Table

/**
 *
 * @author lima.joao
 * @since 10/03/2020 11:10
 */
@Entity
@Table(name="PA_ITEN_001")
class PaItenBean: Serializable {
    @EmbeddedId
    var id: PaItenBeanPK = PaItenBeanPK()
    var quantidade: BigDecimal? = BigDecimal.ZERO
    var percentual: BigDecimal? = BigDecimal.ZERO
    var barra28: String? = null
    var barra: String? = null
    @Column(name="QTDE_BRUTO")
    var qtdeBruto: BigDecimal? = BigDecimal.ZERO
    var ativo: String? = null
    var ordem: Int? = null
    var composicao: String? = null
    var barracli: String? = null
    @Column(name="QTDE_MIN")
    var qtdeMin: BigDecimal? = BigDecimal.ZERO
    var md5: String? = null
    var local: String? = null
    @Column(name="ID_ECOM")
    var idEcom: String? = null

}