package sysdeliz2.models.ti

import java.io.Serializable
import javax.persistence.Embeddable

/**
 *
 * @author lima.joao
 * @since 10/03/2020 11:12
 */
@Embeddable
class PaItenBeanPK: Serializable {
    var tipo: String? = null
    var tam: String? = null
    var cor: String? = null
    var codigo: String? = null
    var lote: String? = null
    var deposito: String? = null

    override fun equals(other: Any?): Boolean {
        return super.equals(other)
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }
}
