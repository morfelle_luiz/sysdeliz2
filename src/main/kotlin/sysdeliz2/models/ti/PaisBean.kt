package sysdeliz2.models.ti

import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name="PAIS_001")
class PaisBean: Serializable {
    @Id
    @Column(name="COD_PAIS")
    var codigo: String? = null

    @Column(name="NOME_PAIS")
    var nome: String? = null
}