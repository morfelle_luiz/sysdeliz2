package sysdeliz2.models.ti

import sysdeliz2.utils.converters.LocalDateAttributeConverter
import java.io.Serializable
import java.math.BigDecimal
import java.time.LocalDate
import javax.persistence.*

/**
 *
 * @author lima.joao
 * @since 17/02/2020 11:11
 */
@Entity
@Table(name="PED_RESERVA_001")
class PedReservaBean(
        @EmbeddedId
        var id: PedReservaPK? = null,
        @Column(name = "QTDE")
        var qtde: BigDecimal? = BigDecimal.ZERO,
        @Column(name = "QTDE_B")
        var qtdeB: BigDecimal? = BigDecimal.ZERO,
        @Column(name = "QTDE_EXP")
        var qtdeExp: BigDecimal? = BigDecimal.ZERO,
        @Column(name= "DT_CAD")
        @Convert(converter = LocalDateAttributeConverter::class)
        var dtCad: LocalDate? = null,
        @Column(name="TIPO")
        var tipo: String? = null,
        @Column(name = "SD_RESERVA")
        var sdReserva: String? = null
) : Serializable