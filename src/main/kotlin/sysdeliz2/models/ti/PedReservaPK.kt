package sysdeliz2.models.ti

import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Embeddable

/**
 *
 * @author lima.joao
 * @since 17/02/2020 11:12
 */
@Embeddable
class PedReservaPK(
    @Column(name = "NUMERO")
    var numero: String? = null,
    @Column(name = "CODIGO")
    var codigo: String? = null,
    @Column(name = "COR")
    var cor: String? = null,
    @Column(name = "TAM")
    var tam: String? = null,
    @Column(name = "RESERVA")
    var reserva: Int? = null,
    @Column(name = "DEPOSITO")
    var deposito: String? = null,
    @Column(name = "LOTE")
    var lote: String? = null
) : Serializable {
    override fun equals(other: Any?): Boolean {
        return super.equals(other)
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }
}