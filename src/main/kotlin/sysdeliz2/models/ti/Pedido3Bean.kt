package sysdeliz2.models.ti

import sysdeliz2.utils.converters.BooleanAttributeConverter
import sysdeliz2.utils.converters.LocalDateAttributeConverter
import java.io.Serializable
import java.math.BigDecimal
import java.time.LocalDate
import javax.persistence.*

/**
 *
 * @author lima.joao
 * @since 17/02/2020 11:11
 */
@Entity
@Table(name = "PEDIDO3_001")
class Pedido3Bean(
    @EmbeddedId
    var id: Pedido3PK? = null,
    @Column(name = "QTDE_F")
    var qtdeF: BigDecimal? = BigDecimal.ZERO,
    @Column(name = "QTDE")
    var qtde: BigDecimal? = BigDecimal.ZERO,
    @Column(name = "QTDE_B")
    var qtdeB: BigDecimal? = BigDecimal.ZERO,
    @Column(name = "CODCLI")
    var codcli: String? = null,
    @Column(name = "FUNCIONARIO")
    var funcionario: String? = null,
    @Column(name = "TCAIXA")
    var tcaixa: String? = null,
    @Column(name = "PESO")
    var peso: BigDecimal? = BigDecimal.ZERO,
    @Column(name = "OBS")
    var obs: String? = null,
    @Column(name = "PESO_L")
    var pesoL: BigDecimal? = BigDecimal.ZERO,
    @Column(name = "DATA")
    @Convert(converter = LocalDateAttributeConverter::class)
    var data: LocalDate? = null,
    @Column(name = "RESERVA")
    var reserva: Int? = 0,
    @Column(name = "CUSTO")
    var custo: BigDecimal? = BigDecimal.ZERO,
    @Column(name = "LIBERADO")
    var liberado: String? = null,
    @Column(name = "NOTAFISCAL")
    var notaFiscal: String? = null,
    @Transient
    var itens: List<Pedido3Bean>? = mutableListOf(),
    @Column(name = "IMPORTADO")
    @Convert(converter = BooleanAttributeConverter::class)
    var importado : Boolean = false

) : Serializable {

    constructor(numero: String, nota: String?, data: LocalDate?) : this() {
        this.id = Pedido3PK(numero)
        this.data = data
        this.notaFiscal = nota
    }

}
