package sysdeliz2.models.ti

import java.io.Serializable
import java.time.LocalDate
import javax.persistence.Column
import javax.persistence.Embeddable
import javax.persistence.Entity
import javax.persistence.Table

/**
 * @author lima.joao
 * @since 07/02/2020
 */
@Embeddable
class Pedido3PK(
    @Column(name = "ORDEM")
    var ordem: Int? = null,

    @Column(name = "TAM")
    var tam: String? = null,

    @Column(name = "COR")
    var cor: String? = null,

    @Column(name = "NUMERO")
    var numero: String? = null,

    @Column(name = "CODIGO")
    var codigo: String? = null,

    @Column(name = "DEPOSITO")
    var deposito: String? = null,

    @Column(name = "LOTE")
    var lote: String? = null,

    @Column(name = "CAIXA")
    var caixa: String? = null
) : Serializable {

    constructor(numero: String) : this() {
        this.numero = numero
    }

    override fun equals(other: Any?): Boolean {
        return super.equals(other)
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }
}