package sysdeliz2.models.ti

import org.hibernate.annotations.GenericGenerator
import org.hibernate.annotations.Parameter
import sysdeliz2.utils.converters.LocalDateAttributeConverter
import sysdeliz2.utils.hibernate.StringPrefixedSequenceIdGenerator
import java.io.Serializable
import java.math.BigDecimal
import java.time.LocalDate
import javax.persistence.*

/**
 * @author lima.joao
 * @since 06/02/2020
 */
@Entity
@Table(name = "PEDIDO_001")
@Suppress("unused")
class PedidoEComerceBean : Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PEDIDOS_ECOMERCE")
    @GenericGenerator(
            name = "SEQ_PEDIDOS_ECOMERCE",
            strategy = "sysdeliz2.utils.hibernate.StringPrefixedSequenceIdGenerator",
            parameters = [
                Parameter(name = StringPrefixedSequenceIdGenerator.SEQUENCE_PARAM, value = "SEQ_PEDIDOS_ECOMERCE"),
                Parameter(name = StringPrefixedSequenceIdGenerator.INCREMENT_PARAM, value = "1"),
                Parameter(name = StringPrefixedSequenceIdGenerator.VALUE_PREFIX_PARAMETER, value = "W"),
                Parameter(name = StringPrefixedSequenceIdGenerator.NUMBER_FORMAT_PARAMETER, value = "%09d")
            ]
    )
    var numero: String? = null

    @Column(name = "FATURA")
    var fatura: Int? = null

    @Column(name = "COM1")
    var com1: BigDecimal? = null

    @Column(name = "COM2")
    var com2: BigDecimal? = null

    @Column(name = "PER_DESC")
    var percDesc: BigDecimal? = null

    @Column(name = "DTDIGITA", columnDefinition = "DATE")
    @Convert(converter = LocalDateAttributeConverter::class)
    var dtDigita: LocalDate? = null // Date

    @Column(name = "DT_EMISSAO", columnDefinition = "DATE")
    @Convert(converter = LocalDateAttributeConverter::class)
    var dtEmissao: LocalDate? = null

    @Column(name = "DT_FATURA", columnDefinition = "DATE")
    @Convert(converter = LocalDateAttributeConverter::class)
    var dtFatura: LocalDate? = null

    @Column(name = "BLOQUEIO")
    var bloqueio: String? = null

    @Column(name = "CIF")
    var cif: String? = null

    @Column(name = "PERIODO")
    var periodo: String? = null

    @Column(name = "COLECAO")
    var colecao: String? = null

    @Column(name = "CODREP2")
    var codRep2: String? = null

    @Column(name = "TAB_PRE")
    var tabPre: String? = null

    @Column(name = "TAB_TRANS")
    var tabTrans: String? = null

    @Column(name = "TAB_REDES")
    var tabRedes: String? = null

    @Column(name = "CODREP")
    var codRep: String? = null

    @Column(name = "TAB_PRE2")
    var tabPre2: String? = null

    @Column(name = "CODCLI")
    var codCli: String? = null

    @Column(name = "PED_CLI")
    var pedCli: String? = null

    @Column(name = "PGTO")
    var pagto: String? = null

    @Lob
    @Column(name = "OBS")
    var obs: String? = null

    @Column(name = "ART_CLI")
    var artCli: String? = null

    @Column(name = "RIS")
    var ris: String? = null

    @Column(name = "ENTREGA", columnDefinition = "DATE")
    @Convert(converter = LocalDateAttributeConverter::class)
    var entrega: LocalDate? = null

    @Column(name = "MOEDA")
    var moeda: String? = null

    @Column(name = "NOTA")
    var nota: String? = null

    @Column(name = "TEMPO")
    var tempo: Int? = null

    @Column(name = "COM3")
    var com3: BigDecimal? = null

    @Column(name = "COM4")
    var com4: BigDecimal? = null

    @Column(name = "MOTIVO")
    var motivo: String? = null

    @Column(name = "PROGRAMACAO")
    var programacao: String? = null

    @Column(name = "DESCONTO")
    var desconto: String? = null

    @Column(name = "FINANCEIRO")
    var financeiro: String? = null

    @Column(name = "SIT_DUP")
    var sitDup: String? = null

    @Column(name = "REDESP_CIF")
    var redespCif: String? = null

    @Column(name = "FRETE")
    var frete: BigDecimal? = null

    @Column(name = "TIPO")
    var tipo: String? = null

    @Column(name = "BONIF")
    var bonif: BigDecimal? = null

    @Column(name = "IMPRESSO")
    var impresso: Int? = null

    @Column(name = "VLR_DESC")
    var vlrDesc: BigDecimal? = null

    @Column(name = "TAXA")
    var taxa: BigDecimal? = null

    @Column(name = "LOCADO")
    var locado: String? = null

    @Column(name = "PERIODO_PROD")
    var periodoProd: String? = null

    @Column(name = "PROG_SETOR")
    var progSetor: String? = null

    @Column(name = "CONTATO")
    var contato: String? = null

    @Column(name = "ENVIO_ESPELHO")
    var envioEspelho: String? = null

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "NUMERO")
    var itens: List<PedidoItemBean>? = null

}