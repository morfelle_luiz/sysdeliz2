package sysdeliz2.models.ti

import sysdeliz2.utils.converters.LocalDateAttributeConverter
import java.io.Serializable
import java.math.BigDecimal
import java.time.LocalDate
import javax.persistence.*

/**
 * @author lima.joao
 * @since 07/02/2020
 */
@Entity
@Table(name = "PED_ITEN_001")
@Suppress("unused")
class PedidoItemBean : Serializable {

    @EmbeddedId
    var id: PedidoItemPK = PedidoItemPK()

    @Column(name = "QTDE")
    var qtde: Int? = null

    @Column(name = "QTDE_F")
    var qtdeF: Int? = null

    @Column(name = "PRECO")
    var preco: BigDecimal? = null

    @Column(name = "QTDE_CANC")
    var qtdeCanc: Int? = null

    @Column(name = "QTDE_PACKS")
    var qtdePacks: Int? = null

    @Column(name = "QUALIDADE")
    var qualidade: String? = null

    @Column(name = "QTDE_ORIG")
    var qtdeOrig: Int? = null

    @Column(name = "PRECO_ORIG")
    var precoOrig: BigDecimal? = null

    @Column(name = "INDICE2")
    var indice2: Int? = null

    @Column(name = "PERC_COMISSAO")
    var percComissao: BigDecimal? = null

    @Column(name = "DESCONTO")
    var desconto: String? = null

    @Column(name = "TIPO")
    var tipo: String? = null

    @Column(name = "IPI")
    var ipi: BigDecimal? = null

    @Column(name = "MARGEM")
    var margem: BigDecimal? = null

    @Column(name = "BONIF")
    var bonif: Int? = null

    @Column(name = "CUSTO")
    var custo: BigDecimal? = null

    @Column(name = "INDICE")
    var indice: Int? = null

    @Column(name = "IMPOSTOS")
    var impostos: BigDecimal? = null

    @Column(name = "EMBALAGEM")
    var embalagem: String? = null

    @Column(name = "OBSERVACAO")
    var observacao: String? = null

    @Column(name = "DT_ENTREGA", columnDefinition = "DATE")
    @Convert(converter = LocalDateAttributeConverter::class)
    var dtEntrega: LocalDate? = null

    @Column(name = "DESC_PACK")
    var descPack: String? = null

    @Column(name = "ESTAMPA")
    var estampa: String? = null

    @Column(name = "NR_ITEM")
    var nrItem: Int? = null

    @Column(name = "MOTIVO")
    var motivo: String? = null

    @Column(name = "VALOR_ST")
    var valorSt: BigDecimal? = null

}