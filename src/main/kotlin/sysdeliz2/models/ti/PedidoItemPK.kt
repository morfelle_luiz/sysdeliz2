
package sysdeliz2.models.ti

import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Embeddable
import javax.persistence.Entity
import javax.persistence.Table

/**
 * @author lima.joao
 * @since 07/02/2020
 */
@Embeddable
class PedidoItemPK: Serializable {

    @Column(name="ORDEM")
    var ordem: Int? = null

    @Column(name = "TAM")
    var tam: String? = null

    @Column(name = "COR")
    var cor: String? = null

    @Column(name = "NUMERO")
    var numero: String? = null

    @Column(name = "CODIGO")
    var codigo: String? = null

    override fun equals(other: Any?): Boolean {
        return super.equals(other)
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }
}