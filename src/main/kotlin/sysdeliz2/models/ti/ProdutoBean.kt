package sysdeliz2.models.ti

import sysdeliz2.models.Cor
import sysdeliz2.models.Tamanho
import sysdeliz2.models.generics.BasicModel
import sysdeliz2.utils.sys.annotations.ColunaFilter
import sysdeliz2.utils.sys.annotations.ExibeTableView
import sysdeliz2.utils.sys.annotations.TelaSysDeliz
import java.io.Serializable
import javax.persistence.*

/**
 * @author lima.joao
 * @since 30/08/2019 17:36
 */
@Entity
@Table(name = "PRODUTO_001")
@TelaSysDeliz(descricao = "Produto", icon = "produto (4).png")
class ProdutoBean : BasicModel(), Serializable {

    @Id
    @Column(name = "CODIGO")
    @ExibeTableView(descricao = "Código", width = 80)
    @ColunaFilter(descricao = "Código", coluna = "codigo")
    var codigo: String? = null

    @Column(name = "DESCRICAO")
    @ExibeTableView(descricao = "Descrição", width = 250)
    @ColunaFilter(descricao = "Descrição", coluna = "descricao")
    var descricao: String? = null

    @Column(name = "ATIVO")
    @ExibeTableView(descricao = "Ativo", width = 30)
    var ativo: String? = null

    /*
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COLECAO")
    @ExibeTableView(descricao = "Coleção", width = 150)
    @ColunaFilter(descricao = "Coleção", coluna = "colecao.codigo", filterClass = "sysdeliz2.models.ti.ColecaoBean")
    var colecao: ColecaoBean? = null

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LINHA")
    var linha: Linha? = null

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FAIXA")
    var faixa: Faixa? = null

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MARCA")
    @ExibeTableView(descricao = "Marca", width = 100)
    @ColunaFilter(descricao = "Marca", coluna = "marca.codigo", filterClass = "sysdeliz2.models.ti.Marca")
    var marca: Marca? = null

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FAMILIA")
    var familia: Familia? = null

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ETIQUETA", nullable = true)
    var etiqueta: EtqProd? = null

    @Transient
    var coresDemanda: ArrayList<Cor> = ArrayList()

    @Transient
    var tamanhosDemanda: ArrayList<Tamanho> = ArrayList()

    @Transient
    var gravar: Boolean = false

    @Transient
    var alterou: Boolean = false

    @Transient
    fun temCores(): Boolean {
        return coresDemanda.isNullOrEmpty() || coresDemanda.isEmpty()
    }

     */
}