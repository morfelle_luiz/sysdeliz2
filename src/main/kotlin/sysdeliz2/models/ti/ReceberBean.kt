package sysdeliz2.models.ti

import sysdeliz2.utils.converters.LocalDateAttributeConverter
import java.io.Serializable
import java.math.BigDecimal
import java.time.LocalDate
import javax.persistence.*

/**
 *
 * @author lima.joao
 * @since 17/02/2020 15:08
 */
@Entity
@Table(name = "RECEBER_001")
class ReceberBean : Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "receber001")
    @TableGenerator(
            name = "receber001",
            table = "CODIGOS_001",
            pkColumnName = "TABELA",
            valueColumnName = "PROXIMO",
            pkColumnValue = "RECEBER",
            allocationSize = 1
    )
    @Column(name = "NUMERO", columnDefinition = "VARCHAR2")
    var numero: Int? = null

    @Column(name = "VEZES")
    var vezes: Int? = 0

    @Column(name = "NUM_CX")
    var numCx: Int? = 0

    @Column(name = "BORDERO")
    var bordero: Int? = 0

    @Column(name = "COM1")
    var com1: BigDecimal? = BigDecimal.ZERO

    @Column(name = "COM2")
    var com2: BigDecimal? = BigDecimal.ZERO

    @Column(name = "TAXA")
    var taxa: BigDecimal? = BigDecimal.ZERO

    @Column(name = "ALIQUOTA")
    var aliquota: BigDecimal? = BigDecimal.ZERO

    @Column(name = "DT_VENCTO")
    @Convert(converter = LocalDateAttributeConverter::class)
    var dtVencto: LocalDate? = null

    @Column(name = "DT_EMISSAO")
    @Convert(converter = LocalDateAttributeConverter::class)
    var dtEmissao: LocalDate? = null

    @Column(name = "ORIGINAL")
    @Convert(converter = LocalDateAttributeConverter::class)
    var original: LocalDate? = null

    @Column(name = "VALOR")
    var valor: BigDecimal? = BigDecimal.ZERO

    @Column(name = "VALOR2")
    var valor2: BigDecimal? = BigDecimal.ZERO

    @Column(name = "VALOR_PAGO")
    var valorPago: BigDecimal? = BigDecimal.ZERO

    @Column(name = "QUANT")
    var quant: BigDecimal? = BigDecimal.ZERO

    @Column(name = "PESOL")
    var pesoL: BigDecimal? = BigDecimal.ZERO

    @Column(name = "PESOB")
    var pesoB: BigDecimal? = BigDecimal.ZERO

    @Column(name = "JUROS")
    var juros: BigDecimal? = BigDecimal.ZERO

    @Column(name = "FRETE")
    var frete: BigDecimal? = BigDecimal.ZERO

    @Column(name = "VAL_DEV")
    var valDev: BigDecimal? = BigDecimal.ZERO

    @Column(name = "DESCONTO")
    var desconto: BigDecimal? = BigDecimal.ZERO

    @Column(name = "VAL_ORIGIN")
    var valOrigin: BigDecimal? = BigDecimal.ZERO

    @Column(name = "DESPESAS")
    var despesas: BigDecimal? = BigDecimal.ZERO

    @Column(name = "TIPO")
    var tipo: String? = null

    @Column(name = "VAL_IPI")
    var valIpi: BigDecimal? = BigDecimal.ZERO

    @Column(name = "SITUACAO")
    var situacao: String? = "0"

    @Column(name = "TIPFAT")
    var tipFat: String? = null

    @Column(name = "BANCO")
    var banco: String? = null

    @Column(name = "TRANSPORT")
    var transport: String? = null

    @Column(name = "CODREP")
    var codRep: String? = null

    @Column(name = "CODREP2")
    var codRep2: String? = null

    @Column(name = "CODCLI")
    var codCli: String? = null

    @Column(name = "FATURA")
    var fatura: String? = null

    @Column(name = "NRBANCO")
    var nrBanco: String? = null

    @Column(name = "obs")
    var obs: String? = null

    @Column(name = "NRO_CUPOM")
    var nroCupom: Int? = 0

    @Column(name = "CLASSE")
    var classe : String? = null

    @Column(name = "DT_COMISSAO")
    @Convert(converter = LocalDateAttributeConverter::class)
    var dtComissao: LocalDate? = null

    @Column(name = "VAL_COM")
    var valCom : BigDecimal? = BigDecimal.ZERO

    @Column(name = "PEDIDO")
    var pedido: String? = null

    @Column(name = "DT_SERASA")
    @Convert(converter = LocalDateAttributeConverter::class)
    var dtSerasa: LocalDate? = null

    @Column(name = "DT_ENVIO")
    @Convert(converter = LocalDateAttributeConverter::class)
    var dtEnvio: LocalDate? = null

    @Column(name = "COM3")
    var com3 : BigDecimal? = BigDecimal.ZERO

    @Column(name = "COM4")
    var com4 : BigDecimal? = BigDecimal.ZERO

    @Column(name = "MD5")
    var md5: String? = null

    @Column(name = "STATUS")
    var status: String? = null

    @Column(name = "DT_PREVISAO")
    @Convert(converter = LocalDateAttributeConverter::class)
    var dtPrevisao: LocalDate? = null

    @Column(name = "DT_STATUS")
    @Convert(converter = LocalDateAttributeConverter::class)
    var dtStatus: LocalDate? = null

    @Column(name = "HISTORICO")
    var historico: String? = null

    @Column(name = "VAL_SUBST")
    var valSubst : BigDecimal? = BigDecimal.ZERO

    @Column(name = "NUMERO_CH")
    var numeroCh: String? = null

    @Column(name = "CONTA_CH")
    var contaCh: String? = null

    @Column(name = "AGENCIA_CH")
    var agenciaCh: String? = null

    @Column(name = "EMISSOR_CH")
    var emissorCh: String? = null

    @Column(name = "BANCO_CH")
    var bancoCh: String? = null

    @Column(name = "LANCAMENTO")
    var lancamento: String? = null

    @Column(name = "USU_NUMREG")
    var usuNumReg: Int? = null

    @Column(name = "IMPORT_SAP")
    var importSap: String? = "N"

    @Column(name = "DATA_DAG")
    @Convert(converter = LocalDateAttributeConverter::class)
    var dataDag: LocalDate? = null
}