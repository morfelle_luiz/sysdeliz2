package sysdeliz2.models.ti

import org.hibernate.annotations.Immutable
import tornadofx.*
import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

/**
 * @author lima.joao
 * @since 28/08/2019 16:15
 */
@Entity
@Table(name = "SITCLI_001")
@Immutable
class SitCliBean : Serializable {

    @Id
    @Column(name = "CODIGO")
    var codigo: String? = null

    @Column(name = "DESCRICAO")
    var descricao: String? = null

    @Column(name = "COR")
    var cor: Int? = null

}