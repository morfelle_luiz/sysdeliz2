package sysdeliz2.models.ti

import tornadofx.*

/**
 *
 * @author lima.joao
 * @since 03/03/2020 09:05
 */
class SitCliBeanModel(sitCliBean: SitCliBean) : ItemViewModel<SitCliBean>(sitCliBean) {
    val codigo = bind(SitCliBean::codigo)
    val descricao = bind(SitCliBean::descricao)
    val cor = bind(SitCliBean::cor)
}