package sysdeliz2.models.ti

import org.hibernate.annotations.Immutable
import java.io.Serializable
import javax.persistence.*

/**
 *
 * @author lima.joao
 * @since 27/01/2020 11:05
 */
@Entity
@Table(name="SUBGRUPO_001")
@Immutable
class SubgrupoModel: Serializable {

    @Id
    @Column(name = "CODIGO")
    var codigo: String? = null
    @Column(name = "CONTA")
    var conta: String? = null
    @Column(name = "DESCRICAO")
    var descricao: String? = null
    @Column(name = "OPERACAO")
    var operacao: String? = null
    @Column(name = "TIPO")
    var tipo: String? = null
    @Lob
    @Column(name = "OBSERVACAO")
    var observacao: String? = null
    @Column(name = "TP_CONTA")
    var tpConta: String? = null

}