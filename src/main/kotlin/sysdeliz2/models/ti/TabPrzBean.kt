package sysdeliz2.models.ti

import sysdeliz2.utils.converters.LocalDateAttributeConverter
import java.io.Serializable
import java.time.LocalDate
import javax.persistence.*

/**
 *
 * @author lima.joao
 * @since 07/04/2020 09:53
 */
@Entity
@Table(name="TABPRZ_001")
class TabPrzBean : Serializable {
    @Id
    var prazo: String? = null

    @Column(name="DT_FIM")
    @Convert(converter = LocalDateAttributeConverter::class)
    var dtFim: LocalDate? = null

    @Column(name="DT_INICIO")
    @Convert(converter = LocalDateAttributeConverter::class)
    var dtInicio: LocalDate? = null

    @Column(name="DESCRICAO")
    var descricao: String? = null

    var minuto: Int? = null

    var ativo: String? = null

    var tipo: String? = null

}