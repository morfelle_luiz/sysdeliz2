package sysdeliz2.models.ti.entidade

import sysdeliz2.models.ti.*
import sysdeliz2.utils.converters.CodcliAttributeConverter
import sysdeliz2.utils.converters.LocalDateAttributeConverter
import java.io.Serializable
import java.math.BigDecimal
import java.time.LocalDate
import java.util.*
import javax.persistence.*
import kotlin.jvm.Transient

/**
 *
 * @author lima.joao
 * @since 21/01/2020 11:37
 */
@Entity
@Table(name = "ENTIDADE_001")
@Suppress("unused")
class EntidadeBean : Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "entidade001")
    @TableGenerator(name = "entidade001", table = "CODIGOS_001", pkColumnName = "TABELA", valueColumnName = "PROXIMO", pkColumnValue = "ENTIDADE", allocationSize = 1)
    @Column(name = "CODCLI", columnDefinition = "VARCHAR2()")
    @Convert(converter = CodcliAttributeConverter::class)
    var codCli: Int? = null

    var classifica: String? = null

    @Column(name = "DATA_CAD", columnDefinition = "DATE")
    @Convert(converter = LocalDateAttributeConverter::class)
    var dataCad: LocalDate? = null

    var nome: String? = null
    var fantasia: String? = null
    var cnpj: String? = null
    var inscricao: String? = null
    var telefone: String? = null

    @Column(name="FAX")
    var celular: String? = null

    var email: String? = null
    var credito: BigDecimal? = null

    @OneToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "COD_ATVD", nullable = true)
    var atividade: AtividadeBean = AtividadeBean()

    @OneToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "GRUPO", nullable = true)
    var grupo: GrupoCliBean = GrupoCliBean()

    var tipo: String? = null

    @Column(name = "TIPO_ENTIDADE")
    var tipoEntidade: String? = null

    var ativo: String? = null

    // Inicio do endereco principal

    @Column(name = "ENDERECO")
    var logradouro: String? = null

    @Column(name = "NUM_END")
    var numEnd: String? = null

    var bairro: String? = null

    @OneToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "CEP")
    var cep: CadCepBean = CadCepBean()

    var transporte: String? = null
    var complemento: String? = null

    @OneToOne
    @JoinColumn(name = "COD_PAIS")
    var pais: PaisBean = PaisBean()

    // Inicio do endereco de cobranca

    @Column(name = "END_COB")
    var logradouroCob: String? = null

    @Column(name = "NUM_COB")
    var numCob: String? = null

    @Column(name = "BAIRRO_COB")
    var bairroCob: String? = null

    @Column(name = "CEP_COB")
    var cepCob: String? = null

    @Column(name = "INSC_COB")
    var inscCob: String? = null

    @Column(name = "CNPJ_COB")
    var cnpjCob: String? = null

    @Column(name = "FONE_COB")
    var foneCob: String? = null

    // INicio do endereco de entrega
    @Column(name = "END_ENT")
    var logradouroEnt: String? = null

    @Column(name = "NUM_ENT")
    var numEnt: String? = null

    @Column(name = "BAIRRO_ENT")
    var bairroEnt: String? = null

    @Column(name = "CEP_ENT")
    var cepEnt: String? = null

    @Column(name = "INSC_ENT")
    var inscEnt: String? = null

    @Column(name = "CNPJ_ENT")
    var cnpjEnt: String? = null


    // ** Aqui encerra o base model

    // Inici os dados tabAuxiliares
    var banco: String? = null

    @Column(name = "BLOQUEIO", columnDefinition = "VARCHAR2(1)")
    //@Convert(converter = BooleanSimNaoConverter.class)
    var bloqueio: String? = null
    var condicao: String? = null

    var conta: String? = null
    var agencia: String? = null
    var desconto: BigDecimal? = null

    @Column(name = "ATRASO_MEDIO")
    var atrasoMedio: Int? = null
    var historico: String? = null

    @Column(name = "SIT_DUP")
    var sitDup: String? = null
    var prazo: String? = null

    @Column(name = "TP_PAG")
    var tpPag: String? = null

    @Column(name = "DT_ANALISE")
    var dtAnalise: Date? = null

    @Column(name = "DT_PREV_ANALISE")
    var dtPrevAnalise: Date? = null

    @Lob
    @Column(name = "OBS_NOTA")
    var obsNota: String? = null

    @Lob
    @Column(name = "OBS_FINAN")
    var obsFinan: String? = null

    //@OneToOne(fetch = FetchType.LAZY)
    //@JoinColumn(name="COD_MOT", nullable = true)
    //var motivo: MensagemBean? = MensagemBean()


    @Column(name = "MAIOR_FAT")
    var maiorFat: BigDecimal? = null

    @Column(name = "DT_MAIOR_FAT")
    var dtMaiorFat: Date? = null

    var bonif: BigDecimal? = null

    @Column(name = "DATA_ACUMULO")
    var dataAcumulo: Date? = null

    @Column(name = "DT_ULTIMO_FAT")
    var dtUltimoFat: Date? = null

    var contac: String? = null
    var contad: String? = null
    var com1: BigDecimal? = null
    var com2: BigDecimal? = null
    var cif: String? = null

    @Column(name = "REDESP_CIF")
    var redespCif: String? = null

    @Column(name = "ALIQ_ST")
    var aliqSt: BigDecimal? = null

    var codRep: String? = null

    @Column(name = "DT_NASC", columnDefinition = "DATE")
    @Convert(converter = LocalDateAttributeConverter::class)
    var dtNasc: LocalDate? = null

    @Column(name = "MAIOR_ACUMULO")
    var maiorAcumulo: BigDecimal? = null

    var natureza: String? = null
    var nrloja: String? = null

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SIT_CLI", nullable = true)
    var sitCli: SitCliBean = SitCliBean()

    var tabela: String? = null

    @Column(name = "ULTIMO_FAT")
    var ultimoFat: BigDecimal? = null

    @Lob
    var obs: String? = null

    var consumos: BigDecimal? = null
    var royal: BigDecimal? = null

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CLASSE", nullable = true)
    var classe: SubgrupoBean = SubgrupoBean()

    var dias: Int? = null
    var suframa: String? = null

    @Column(name = "NOME_PAI")
    var nomePai: String? = null

    @Column(name = "NOME_MAE")
    var nomeMae: String? = null

    var sexo: String? = null

    @Column(name = "NUM_RG")
    var numRg: String? = null

    @Column(name = "EMI_RG")
    var emiRg: String? = null

    var simples: String? = null
    var redesp: String? = null
    var frete: BigDecimal? = null
    var site: String? = null

    @Column(name = "INSC_EST")
    var inscEst: String? = null

    @Column(name = "DATA_FUND")
    var dataFund: Date? = null

    var iof: String? = null


    @Transient
    var isCliente: Boolean = false
        get() {
            return this.tipoEntidade?.contains("C") ?: false
        }
        set(value) {
            if (value) {
                if (this.tipoEntidade?.contains("C") != true) {
                    this.tipoEntidade += "C"
                }
            } else {
                if (this.tipoEntidade?.contains("C") == true) {
                    this.tipoEntidade!!.removePrefix("C")
                }
            }
            field = value
        }

    @Transient
    var isFornecedor: Boolean = false
        get() {
            return this.tipoEntidade?.contains("F") ?: false
        }
        set(value) {
            if (value) {
                if (this.tipoEntidade?.contains("F") != true) {
                    this.tipoEntidade += "F"
                }
            } else {
                if (this.tipoEntidade?.contains("F") == true) {
                    this.tipoEntidade!!.removePrefix("F")
                }
            }
            field = value
        }

    @Transient
    var isTerceiro: Boolean = false
        get() {
            return this.tipoEntidade?.contains("T") ?: false
        }
        set(value) {
            if (value) {
                if (this.tipoEntidade?.contains("T") != true) {
                    this.tipoEntidade += "T"
                }
            } else {
                if (this.tipoEntidade?.contains("T") == true) {
                    this.tipoEntidade!!.removePrefix("T")
                }
            }
            field = value
        }

    @Transient
    var isAtivo: Boolean = false
        get() {
            return (this.ativo ?: "N") == "S"
        }
        set(value) {
            if (value) {
                if (this.ativo == "N") {
                    this.ativo = "S"
                }
            } else {
                if (this.ativo == "S") {
                    this.ativo = "N"
                }
            }
            field = value
        }

    @Transient
    var isBloqueado: Boolean = false
        get() {
            return this.bloqueio == "S"
        }
        set(value) {
            if (value) {
                if (this.bloqueio == "N") {
                    this.bloqueio = "S"
                }
            } else {
                if (this.bloqueio == "S") {
                    this.bloqueio = "N"
                }
            }
            field = value
        }
}