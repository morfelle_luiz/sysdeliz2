package sysdeliz2.models.ti.entidade

import sysdeliz2.models.ti.entidade.entidadeBeanModel.EntidadeDadosGeraisModel

/**
 *
 * @author lima.joao
 * @since 21/01/2020 14:55
 */
@Suppress("unused")
class EntidadeBeanModel(entidade: EntidadeBean) : EntidadeDadosGeraisModel(entidade) {
//    val motivo = bind(EntidadeBean::motivo)
}