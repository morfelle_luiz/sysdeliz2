package sysdeliz2.models.ti.entidade

import br.com.caelum.stella.format.CEPFormatter
import sysdeliz2.dao.FluentDao
import sysdeliz2.daos.generics.implementation.GenericDaoImpl
import sysdeliz2.daos.generics.interfaces.GenericDao
import sysdeliz2.models.sysdeliz.SdCidadeBean
import sysdeliz2.models.sysdeliz.SdMagentoEnderecoBean
import sysdeliz2.models.ti.CadCepBean
import sysdeliz2.utils.enums.Estados
import sysdeliz2.utils.extensions.normalize
import sysdeliz2.utils.extensions.toLength

/**
 *
 * @author lima.joao
 * @since 07/02/2020 15:01
 */
private val cepFormatter = CEPFormatter()

fun EntidadeBean.addressMagentoToEnderecoCobranca(address: SdMagentoEnderecoBean) {
    this.logradouroCob = (address.street?.toLength(50) ?: "").toUpperCase()
    this.bairroCob = (address.neighborhood?.toLength(35) ?: "").toUpperCase()
    this.numCob = (address.number?.toLength(15) ?: "").toUpperCase()

    this.cepCob = cepFormatter.unformat(address.postcode ?: "")
    this.inscCob = "ISENTO"
}

fun EntidadeBean.addressMagentoToEnderecoEntrega(address: SdMagentoEnderecoBean) {
    this.logradouroEnt = (address.street?.toLength(50) ?: "").toUpperCase()
    this.bairroEnt = (address.neighborhood?.toLength(35) ?: "").toUpperCase()
    this.numEnt = (address.number?.toLength(15) ?: "").toUpperCase()

    this.cepEnt = cepFormatter.unformat(address.postcode ?: "")
    this.inscEnt = "ISENTO"
}

fun EntidadeBean.addressMagentoToEndereco(address: SdMagentoEnderecoBean){
    this.logradouro = (address.street?.toLength(50) ?: "").toUpperCase()
    this.bairro = (address.neighborhood?.toLength(35) ?: "").toUpperCase()
    this.complemento = (address.complement?.toLength(50) ?: "").toUpperCase()
    this.numEnd = (address.number?.toLength(15) ?: "").toUpperCase()

    val cepDao: GenericDao<CadCepBean> = GenericDaoImpl(CadCepBean::class.java)
    val sdCidadeDao: GenericDao<SdCidadeBean> = GenericDaoImpl(SdCidadeBean::class.java)

    val cepFormatter = CEPFormatter()

    cepDao.initCriteria().addPredicateEq("cep", cepFormatter.unformat(address.postcode))

    var cepTmp: CadCepBean = cepDao.loadEntityByPredicate() ?: CadCepBean()

    if(cepTmp.cep == null) {
        cepTmp.cep = cepFormatter.unformat(address.postcode)
        cepTmp.bairro = (address.neighborhood ?: "").toUpperCase()
        cepTmp.logrCep = (address.street ?: "").toUpperCase()

        val cidade = FluentDao()
                .selectFrom(SdCidadeBean::class.java)
                .where {
                    it.equal("nome", address.city?.normalize()?.toUpperCase())

                    if(address.region?:"" != ""){
                        it.equal("siglaUf", Estados.getUF(address.region))
                    }
                }.singleResult<SdCidadeBean>()

        cepTmp.cidade = cidade

        cepTmp = cepDao.save(cepTmp)
    }

    this.cep = cepTmp

    this.pais.codigo = "1058"

    this.transporte = "5702"
}