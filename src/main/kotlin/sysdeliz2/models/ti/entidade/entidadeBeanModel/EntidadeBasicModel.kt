package sysdeliz2.models.ti.entidade.entidadeBeanModel

import sysdeliz2.models.ti.entidade.EntidadeBean
import tornadofx.*

/**
 *
 * @author lima.joao
 * @since 21/01/2020 14:55
 */
@Suppress("unused")
open class EntidadeBasicModel(entidade: EntidadeBean) : ItemViewModel<EntidadeBean>(entidade) {

    val codRep = bind(EntidadeBean::codRep)
    val dtNasc = bind(EntidadeBean::dtNasc)
    val maiorAcumulo = bind(EntidadeBean::maiorAcumulo)
    val natureza = bind(EntidadeBean::natureza)
    val nrloja = bind(EntidadeBean::nrloja)

    val tabela = bind(EntidadeBean::tabela)
    val ultimoFat = bind(EntidadeBean::ultimoFat)
    val obs = bind(EntidadeBean::obs)
    val consumos = bind(EntidadeBean::consumos)
    val royal = bind(EntidadeBean::royal)
    val classe = bind(EntidadeBean::classe)
    val dias = bind(EntidadeBean::dias)
    val suframa = bind(EntidadeBean::suframa)
    val nomePai = bind(EntidadeBean::nomePai)
    val nomeMae = bind(EntidadeBean::nomeMae)
    val sexo = bind(EntidadeBean::sexo)
    val numRg = bind(EntidadeBean::numRg)
    val emiRg = bind(EntidadeBean::emiRg)
    val simples = bind(EntidadeBean::simples)
    val redesp = bind(EntidadeBean::redesp)
    val frete = bind(EntidadeBean::frete)
    val site = bind(EntidadeBean::site)
    val inscEst = bind(EntidadeBean::inscEst)
    val dataFund = bind(EntidadeBean::dataFund)
    val iof = bind(EntidadeBean::iof)
    val obsNota = bind(EntidadeBean::obsNota)
    val obsFinan = bind(EntidadeBean::obsFinan)
}