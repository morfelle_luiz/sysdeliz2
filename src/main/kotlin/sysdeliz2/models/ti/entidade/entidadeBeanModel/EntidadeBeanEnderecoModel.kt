package sysdeliz2.models.ti.entidade.entidadeBeanModel

import sysdeliz2.models.ti.entidade.EntidadeBean
import tornadofx.*

/**
 *
 * @author lima.joao
 * @since 21/01/2020 14:55
 */
@Suppress("unused")
open class EntidadeBeanEnderecoModel(entidade: EntidadeBean) : EntidadeBasicModel(entidade) {

    // Endereco principal
    val logradouro = bind(EntidadeBean::logradouro)
    val numEnd = bind(EntidadeBean::numEnd)
    val bairro = bind(EntidadeBean::bairro)
    val cep = bind(EntidadeBean::cep)
    val transporte = bind(EntidadeBean::transporte)
    val complemento = bind(EntidadeBean::complemento)
    val pais = bind(EntidadeBean::pais)

    // Endereco cobranca
    val logradouroCob = bind(EntidadeBean::logradouroCob)
    val numCob = bind(EntidadeBean::numCob)
    val bairroCob = bind(EntidadeBean::bairroCob)
    val cepCob = bind(EntidadeBean::cepCob)
    val inscCob = bind(EntidadeBean::inscCob)
    val cnpjCob = bind(EntidadeBean::cnpjCob)
    val foneCob = bind(EntidadeBean::foneCob)

    // Endereco de entrega
    val logradouroEnt = bind(EntidadeBean::logradouroEnt)
    val numEnt = bind(EntidadeBean::numEnt)
    val bairroEnt = bind(EntidadeBean::bairroEnt)
    val cepEnt = bind(EntidadeBean::cepEnt)
    val inscEnt = bind(EntidadeBean::inscEnt)
    val cnpjEnt = bind(EntidadeBean::cnpjEnt)
}