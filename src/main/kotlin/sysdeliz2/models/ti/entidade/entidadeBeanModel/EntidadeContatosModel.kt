package sysdeliz2.models.ti.entidade.entidadeBeanModel

import sysdeliz2.models.ti.entidade.EntidadeBean
import tornadofx.*

/**
 *
 * @author lima.joao
 * @since 21/01/2020 14:55
 */
@Suppress("unused")
open class EntidadeContatosModel(entidade: EntidadeBean) :  EntidadeFinanceiroModel(entidade) {
    val telefone = bind(EntidadeBean::telefone)
    val celular = bind(EntidadeBean::celular)
    val email = bind(EntidadeBean::email)
}