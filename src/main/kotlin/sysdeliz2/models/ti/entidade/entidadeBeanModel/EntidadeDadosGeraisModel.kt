package sysdeliz2.models.ti.entidade.entidadeBeanModel

import sysdeliz2.models.ti.entidade.EntidadeBean

/**
 *
 * @author lima.joao
 * @since 21/01/2020 14:55
 */
@Suppress("unused")
open class EntidadeDadosGeraisModel(entidade: EntidadeBean) : EntidadeContatosModel(entidade) {
    val codCli = bind(EntidadeBean::codCli)
    val classifica = bind(EntidadeBean::classifica)
    val cnpj = bind(EntidadeBean::cnpj)
    val tipo = bind(EntidadeBean::tipo)
    val inscricao = bind(EntidadeBean::inscricao)
    val dataCad = bind(EntidadeBean::dataCad)
    val nome = bind(EntidadeBean::nome)
    val fantasia = bind(EntidadeBean::fantasia)
    val tipoEntidade = bind(EntidadeBean::tipoEntidade)
    val ativo = bind(EntidadeBean::ativo)
    val bloqueio = bind(EntidadeBean::bloqueio)
//    val motivo = bind(EntidadeBean::motivo)

    val isCliente = bind(EntidadeBean::isCliente)
    val isFornecedor = bind(EntidadeBean::isFornecedor)
    val isTerceiro = bind(EntidadeBean::isTerceiro)
    val isAtivo = bind(EntidadeBean::isAtivo)
    val isBloqueado = bind(EntidadeBean::isBloqueado)
}