package sysdeliz2.models.ti.entidade.entidadeBeanModel

import sysdeliz2.models.ti.entidade.EntidadeBean
import tornadofx.*

/**
 *
 * @author lima.joao
 * @since 21/01/2020 14:55
 */
@Suppress("unused")
open class EntidadeFinanceiroModel(entidade: EntidadeBean) : EntidadeBeanEnderecoModel(entidade) {
    val atividade = bind(EntidadeBean::atividade)
    val sitCli = bind(EntidadeBean::sitCli)
    val grupo = bind(EntidadeBean::grupo)
    val credito = bind(EntidadeBean::credito)
    val banco = bind(EntidadeBean::banco)
    val conta = bind(EntidadeBean::conta)
    val agencia = bind(EntidadeBean::agencia)
    val condicao = bind(EntidadeBean::condicao)
    val desconto = bind(EntidadeBean::desconto)
    val atrasoMedio = bind(EntidadeBean::atrasoMedio)
    val historico = bind(EntidadeBean::historico)
    val sitDup = bind(EntidadeBean::sitDup)
    val prazo = bind(EntidadeBean::prazo)
    val tpPag = bind(EntidadeBean::tpPag)
    val dtAnalise = bind(EntidadeBean::dtAnalise)
    val dtPrevAnalise = bind(EntidadeBean::dtPrevAnalise)
    val maiorFat = bind(EntidadeBean::maiorFat)
    val dtMaiorFat = bind(EntidadeBean::dtMaiorFat)
    val bonif = bind(EntidadeBean::bonif)
    val dataAcumulo = bind(EntidadeBean::dataAcumulo)
    val dtUltimoFat = bind(EntidadeBean::dtUltimoFat)
    val contac = bind(EntidadeBean::contac)
    val contad = bind(EntidadeBean::contad)
    val com1 = bind(EntidadeBean::com1)
    val com2 = bind(EntidadeBean::com2)
    val cif = bind(EntidadeBean::cif)
    val redespCif = bind(EntidadeBean::redespCif)
    val aliqSt = bind(EntidadeBean::aliqSt)
}