package sysdeliz2.models.utils

/**
 *
 * @author lima.joao
 * @since 02/04/2020 11:52
 */
interface IProdutoMagentoUtils {

    fun canSendProduto(): Boolean

    fun canSendEstoque(): Boolean
    
}