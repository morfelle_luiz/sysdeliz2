package sysdeliz2.models.utils

import javax.persistence.Transient

/**
 *
 * @author lima.joao
 * @since 02/04/2020 11:37
 */
open class Selecionavel {
    @Transient
    var selected: Boolean = false
}