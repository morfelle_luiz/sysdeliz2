package sysdeliz2.models.view

import org.hibernate.annotations.Immutable
import java.io.Serializable
import java.math.BigDecimal
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

/**
 *
 * @author lima.joao
 * @since 12/11/2019 16:07
 */
@Entity
@Table(name="V_PROD_RESERVA_COLETA")
@Immutable
class VProdReservaColeta : Serializable{

    @Id
    @Column(name = "ID_JPA")
    var idJpa: String? = null
    @Column(name="NUMERO")
    var numero: String? = null
    @Column(name="RESERVA")
    var reserva: String? = null
    @Column(name="CODIGO")
    var codigo: String? = null
    @Column(name="DESCRICAO")
    var descricao: String? = null
    @Column(name="COR")
    var cor: String? = null
    @Column(name="DESCCOR")
    var descCor: String? = null
    @Column(name="QTDE")
    var qtde: BigDecimal? = null
    @Column(name="CLIENTE")
    var cliente: String? = null
    @Column(name="OBS")
    var obs: String? = null
    @Column(name="TRANSP")
    var trans: String? = null
    @Column(name="TAMANHO")
    var tamanhos: String? = null
    @Column(name="USUARIO_LEITURA")
    var usuarioLeitura: String? = null
    @Column(name="LOCAL")
    var local: String? = null

    @Transient
    var qtdLida: BigDecimal? = null
}

