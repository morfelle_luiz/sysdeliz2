package sysdeliz2.models.view

import org.hibernate.annotations.Immutable
import java.io.Serializable
import javax.persistence.*

/**
 *
 * @author lima.joao
 * @since 06/04/2020 09:06
 */
@Entity
@Table(name = "v_sd_cores_produto_magento")
@Immutable
class VSdCoresMagentoProduto: Serializable {

    @EmbeddedId
    var id: Pk? = null

    var descricao: String? = null



    @Embeddable
    class Pk: Serializable{
        var codigo: String? = null
        var cor: String? = null
    }

    override fun toString(): String {
        return "[${id?.cor}] $descricao"
    }

}