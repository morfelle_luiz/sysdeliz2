package sysdeliz2.models.view

import jdk.nashorn.internal.ir.annotations.Immutable
import sysdeliz2.utils.converters.LocalDateAttributeConverter
import java.io.Serializable
import java.math.BigDecimal
import java.time.LocalDate
import javax.persistence.*

@Entity
@Table(name="V_SD_CRM_PEDIDOS_MARCA_COLECAO")
@Immutable
class VSdCrmPedidosMarcaColecao: Serializable {

    @Id
    @Column(name="NUMERO")
    var numero: String? = null
    @Column(name="CODCLI")
    var codcli: String? = null
    @Column(name="NOME")
    var nome: String? = null
    @Column(name="DT_EMISSAO", columnDefinition = "DATE")
    @Convert(converter = LocalDateAttributeConverter::class)
    var dtEmissao: LocalDate? = null
    @Column(name="DT_FATURA", columnDefinition = "DATE")
    @Convert(converter = LocalDateAttributeConverter::class)
    var dtFatura: LocalDate? = null
    @Column(name = "QTD")
    var qtd: BigDecimal? = BigDecimal.ZERO
    @Column(name="VALOR_PEND")
    var valorPendente: BigDecimal? = BigDecimal.ZERO
    @Column(name = "QTD_F")
    var qtdFaturada: BigDecimal? = BigDecimal.ZERO
    @Column(name="VALOR_FAT")
    var valorFaturado: BigDecimal? = BigDecimal.ZERO
    @Column(name = "QTD_TOT")
    var qtdTotal: BigDecimal? = BigDecimal.ZERO
    @Column(name="VALOR_TOT")
    var valorTotal: BigDecimal? = BigDecimal.ZERO
    @Column(name = "QTD_CANC")
    var qtdCancelado: BigDecimal? = BigDecimal.ZERO
    @Column(name="VALOR_CANC")
    var valorCancelado: BigDecimal? = BigDecimal.ZERO
    @Column(name="MARCA")
    var marca: String? = null
    @Column(name="COLECAO")
    var colecao: String? = null
    @Column(name="ACAO_TOMADA_CRM")
    var acaoTomadaCrm: String? = null

}

