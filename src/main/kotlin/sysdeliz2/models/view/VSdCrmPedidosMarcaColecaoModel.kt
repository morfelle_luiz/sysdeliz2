package sysdeliz2.models.view

import tornadofx.*

class VSdCrmPedidosMarcaColecaoModel(registro: VSdCrmPedidosMarcaColecao) : ItemViewModel<VSdCrmPedidosMarcaColecao>(registro) {
    val numero = bind(VSdCrmPedidosMarcaColecao::numero)
    val codcli = bind(VSdCrmPedidosMarcaColecao::codcli)
    val nome = bind(VSdCrmPedidosMarcaColecao::nome)
    val dtEmissao = bind(VSdCrmPedidosMarcaColecao::dtEmissao)
    val dtFatura = bind(VSdCrmPedidosMarcaColecao::dtFatura)
    val qtd = bind(VSdCrmPedidosMarcaColecao::qtd)
    val valorPendente = bind(VSdCrmPedidosMarcaColecao::valorPendente)
    val qtdFaturada = bind(VSdCrmPedidosMarcaColecao::qtdFaturada)
    val valorFaturado = bind(VSdCrmPedidosMarcaColecao::valorFaturado)
    val qtdTotal = bind(VSdCrmPedidosMarcaColecao::qtdTotal)
    val valorTotal = bind(VSdCrmPedidosMarcaColecao::valorTotal)
    val qtdCancelado = bind(VSdCrmPedidosMarcaColecao::qtdCancelado)
    val valorCancelado = bind(VSdCrmPedidosMarcaColecao::valorCancelado)
    val marca = bind(VSdCrmPedidosMarcaColecao::marca)
    val colecao = bind(VSdCrmPedidosMarcaColecao::colecao)
}
