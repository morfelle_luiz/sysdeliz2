package sysdeliz2.models.view

import sysdeliz.apis.web.convertr.data.models.stocks.Stock
import sysdeliz2.utils.converters.LocalDateAttributeConverter
import java.io.Serializable
import java.time.LocalDate
import javax.persistence.*

/**
 *
 * @author lima.joao
 * @since 03/04/2020 10:10
 */
@Entity
@Table(name="SD_ESTOQUE_B2B_001")
class VSdEstoqueB2B: Serializable {

    @EmbeddedId
    var id: VSdEstoqueB2BPK = VSdEstoqueB2BPK()

    var descricao: String? = null

    @Column(name="DT_INICIO")
    @Convert(converter = LocalDateAttributeConverter::class)
    var dtInicio: LocalDate? = null

    @Column(name="DT_FIM")
    @Convert(converter = LocalDateAttributeConverter::class)
    var dtFim: LocalDate? = null

    @Column(name="QTDE_EST")
    var qtdEst: Int? = null

    @Column(name="VEND_RES")
    var vendRes: Int? = null

    @Column(name="QTDE_ORIG")
    var qtdeOrig: Int? = null

    @Column(name="QTDE")
    var qtd: Int? = null

    fun toStock(): Stock {
        return Stock("${id.codigo}-${id.cor}-${id.tam}", qtd ?: 0, id.entrega)
    }

}