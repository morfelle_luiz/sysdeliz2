package sysdeliz2.models.view

import org.hibernate.annotations.Immutable
import java.io.Serializable
import java.math.BigDecimal
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

/**
 *
 * @author lima.joao
 * @since 06/01/2020 16:18
 */
@Entity
@Immutable
@Table(name="V_SD_ESTOQUE_LOJA_VIRTUAL")
class VSdEstoqueLojaVirtual : Serializable {

    @Id
    @Column(name="SKU")
    var sku: String? = null
    @Column(name="CODIGO")
    var codigo: String? = null
    @Column(name="TAM")
    var tamanho: String? = null
    @Column(name="COR")
    var cor: String? = null
    @Column(name="QUANTIDADE")
    var quantidade: BigDecimal? = null

}