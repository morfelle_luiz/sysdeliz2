package sysdeliz2.models.view

import org.hibernate.annotations.Immutable
import sysdeliz2.models.utils.IProdutoMagentoUtils
import sysdeliz2.models.utils.Selecionavel
import java.io.Serializable
import java.math.BigDecimal
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "V_SD_GRUPO_PRECOS_B2B")
@Immutable
class VSdGrupoPrecosB2b : Serializable, Selecionavel()  {

    @Id
    var idjpa: String? = null

    var codigo: String? = null

    var grupo: String? = null

    var descricao: String? = null

    @Column(name = "PRECO_00")
    var preco: BigDecimal? = null
}
