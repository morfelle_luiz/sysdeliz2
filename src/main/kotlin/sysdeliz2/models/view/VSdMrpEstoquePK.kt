package sysdeliz2.models.view

import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Embeddable

/**
 *
 * @author lima.joao
 * @since 07/04/2020 08:09
 */
@Embeddable
class VSdMrpEstoquePK: Serializable {

    @Column(name="CODIGO")
    var codigo: String? = null
    @Column(name="COR")
    var cor: String? = null
    @Column(name="TAM")
    var tam: String? = null
    @Column(name="ENTREGA")
    var entrega: String? = null
}