package sysdeliz2.models.view

import javafx.collections.ObservableList
import org.apache.commons.lang.WordUtils
import org.hibernate.annotations.Immutable
import sysdeliz.apis.web.convertr.data.models.products.*
import sysdeliz.apis.web.convertr.data.models.stocks.Stock
import sysdeliz2.models.ti.FaixaItem
import sysdeliz2.dao.FluentDao
import sysdeliz2.dao.OrderType
import sysdeliz2.models.generics.BasicModel
import sysdeliz2.utils.CamposProdutosEnviar
import sysdeliz2.models.utils.IProdutoMagentoUtils
import java.io.Serializable
import java.math.BigDecimal
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table
import javax.persistence.Transient


/**
 *
 * @author lima.joao
 * @since 29/11/2019 08:26
 */
@Entity
@Table(name = "V_SD_PRODUTO_MAGENTO")
@Immutable
class VSdProdutoMagento : Serializable, IProdutoMagentoUtils, BasicModel() {

    @Id
    @Column(name = "SKU")
    var sku: String? = null

    @Column(name = "CODIGO")
    var codigo: String? = null

    @Column(name = "REGIAO")
    var tabelaPreco: String? = null

    @Column(name = "COR")
    var cor: String? = null

    @Column(name = "COR_NAME")
    var corName: String? = null

    @Column(name = "NAME")
    var name: String? = null

    @Column(name = "DESCRIPTION")
    var description: String? = null

    @Column(name = "SHORT_DESCRIPTION")
    var shortDescription: String? = null

    @Column(name = "CATEGORIA")
    var categoria: String? = null

    @Column(name = "PRICE")
    var price: BigDecimal? = null

    @Column(name = "SPECIAL_PRICE")
    var specialPrice: BigDecimal? = null
        get() {
            return if (field ?: BigDecimal.ZERO == BigDecimal.ZERO) {
                null
            } else {
                field
            }
        }

    @Column(name = "WEIGHT")
    var weight: BigDecimal? = null

    @Column(name = "STATUS", columnDefinition = "Char")
    var status: String? = null

    @Column(name = "META_TITLE")
    var metaTitle: String? = null

    @Column(name = "META_KEYWORD")
    var metaKeyword: String? = null

    @Column(name = "META_DESCRIPTION")
    var metaDescription: String? = null

    @Column(name = "FAIXA")
    var faixa: String? = null

    @Column(name = "COLECAO")
    var colecao: String? = null

    @Column(name = "COLECAO_DESC")
    var colecaoDesc: String? = null

    @Column(name = "STATUS_PROCESSAMENTO", columnDefinition = "Char")
    var statusProcessamento: String? = null

    @Column(name = "CROSS_SELL")
    var crossSell: String? = null

    @Column(name = "LINHA")
    var linha: String? = null

    @Column(name = "LINHA_DESC")
    var linhaDesc: String? = null

    @Column(name = "TABELA_MEDIDA")
    var tabelaMedida: String? = null

    @Transient
    var posicaoEstoque: ObservableList<VSdEstoqueLojaVirtual>? = null

    override fun canSendProduto(): Boolean {
        return (this.statusProcessamento == "0" ||
                this.statusProcessamento == "2" ||
                this.statusProcessamento == "4") && selected.get()
    }

    override fun canSendEstoque(): Boolean {
        return (this.statusProcessamento == "2" || this.statusProcessamento == "4") && selected.get()
    }

    fun toStock(obj: VSdEstoqueLojaVirtual): Stock {
        return Stock("$sku-${obj.tamanho}", obj.quantidade?.toInt() ?: 0)
    }

    fun toProductMagento(filtro: CamposProdutosEnviar): Product {
        val s = ArrayList<SingleData>()
        s.add(SingleData("cor", corName!!))
        s.add(SingleData("medidas_peca", tabelaMedida ?: ""))

        return Product.Builder()
                .comSKU(sku, filtro.enviarSKU)
                .comName(name, filtro.enviarNome)
                .comPrice(price, filtro.enviarPreco)
                .comShortDescription(shortDescription, filtro.enviarDescricaoCurta)
                .comDescription(description, filtro.enviarDescricao)
                .comSpecialPrice(specialPrice, filtro.enviarPrecoEspecial)
                .comWeight(weight, filtro.enviarPeso)
                .comStatus(status!!.toInt(), filtro.enviarStatus)
                .comMetaTitle(metaTitle, filtro.enviarTituloSEO)
                .comMetaKeyword(metaKeyword, filtro.enviarPalavasChaves)
                .comMetaDescription(metaDescription, filtro.enviarDescricaoSEO)
                .comCategories(getCategories(), filtro.enviarCategorias)
                .comVariations(getVariations(), filtro.enviarVariacoes)
                .comAditionalAttributes(AdditionalAttribute(s), filtro.enviarAtributosAdicionais)
                .comProductLinks(getProductLinks(), filtro.enviarLinkProdutos)
                .build()
    }

    private fun getCategories(): ArrayList<ArrayList<Category>> {
        val categories = ArrayList<ArrayList<Category>>()
        var lista = ArrayList<Category>()

        // Adiciona a arvore de coleção
        lista.add(Category(WordUtils.capitalize(colecao?.toLowerCase() ?: "")))

        if (categoria ?: "" != "") {
            lista.add(Category(WordUtils.capitalize(categoria?.toLowerCase() ?: "")))
        }
        categories.add(lista)

        // Adiciona a arvore da linha
        lista = ArrayList()
        lista.add(Category(WordUtils.capitalize(linhaDesc?.toLowerCase() ?: "")))

        if (categoria ?: "" != "") {
            lista.add(Category(WordUtils.capitalize(categoria?.toLowerCase() ?: "")))
        }
        categories.add(lista)

        lista = ArrayList()
        lista.add(Category(WordUtils.capitalize(categoria?.toLowerCase() ?: "")))
        categories.add(lista)


        lista = ArrayList()
        lista.add(Category("Todos os Produtos"))
        categories.add(lista)

        return categories
    }

    private fun getVariations(): Variation {
        val itens = FluentDao()
                .selectFrom(FaixaItem::class.java)
                .where {
                    it.equal("faixaItemId.faixa", faixa)
                }
                .orderBy("posicao", OrderType.ASC)
                .resultList<FaixaItem>()

        val variation = Variation(null)

        itens?.let {
            for (iten in itens) {
                variation.tamanho?.add(iten.faixaItemId.tamanho!!)
            }
        }

        return variation
    }

    private fun getProductLinks(): ProductLinks {
        val productLinks = ProductLinks()
        crossSell?.let {
            val cross = crossSell!!.split(',')

            for (c in cross) {
                productLinks.crossSell!!.add(c)
            }
        }
        return productLinks
    }

}