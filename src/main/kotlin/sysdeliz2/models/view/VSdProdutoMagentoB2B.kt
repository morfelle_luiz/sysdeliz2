package sysdeliz2.models.view

import org.apache.commons.lang.WordUtils
import org.hibernate.annotations.Immutable
import sysdeliz.apis.web.convertr.data.models.products.*
import sysdeliz2.dao.FluentDao
import sysdeliz2.dao.OrderType
import sysdeliz2.models.ti.FaixaItem
import sysdeliz2.models.utils.IProdutoMagentoUtils
import sysdeliz2.models.utils.Selecionavel
import sysdeliz2.utils.CamposProdutosEnviar
import java.io.Serializable
import java.math.BigDecimal
import javax.persistence.*

/**
 *
 * @author lima.joao
 * @since 29/11/2019 08:26
 */
@Entity
@Table(name = "V_SD_PRODUTO_MAGENTO_B2B")
@Immutable
class VSdProdutoMagentoB2B : Serializable, IProdutoMagentoUtils, Selecionavel() {

    @Id
    var codigo: String? = null

    @Column(name = "REGIAO")
    var tabelaPreco: String? = null

    var name: String? = null

    var description: String? = null

    @Column(name = "SHORT_DESCRIPTION")
    var shortDescription: String? = null

    var categoria: String? = null

    var price: BigDecimal? = null

    @Column(name = "SPECIAL_PRICE")
    var specialPrice: BigDecimal? = null
        get() {
            return if (field ?: BigDecimal.ZERO == BigDecimal.ZERO) {
                null
            } else {
                field
            }
        }

    var weight: BigDecimal? = null

    @Column(name = "STATUS", columnDefinition = "Char")
    var status: String? = null

    @Column(name = "META_TITLE")
    var metaTitle: String? = null

    @Column(name = "META_KEYWORD")
    var metaKeyword: String? = null

    @Column(name = "META_DESCRIPTION")
    var metaDescription: String? = null

    var faixa: String? = null

    var colecao: String? = null

    @Column(name = "COLECAO_DESC")
    var colecaoDesc: String? = null

    @Column(name = "STATUS_PROCESSAMENTO", columnDefinition = "Char")
    var statusProcessamento: String? = null

    var linha: String? = null

    @Column(name = "LINHA_DESC")
    var linhaDesc: String? = null

    @Column(name = "TABELA_MEDIDA")
    var tabelaMedida: String? = null

    @Column(name = "MARCA")
    var marca: String? = null

    @OneToMany
    @JoinColumn(name = "CODIGO")
    var cores: List<VSdCoresMagentoProduto>? = null

    @OneToMany
    @JoinColumn(name = "CODIGO")
    var estoque: List<VSdEstoqueB2B>? = null

    @OneToMany
    @JoinColumn(name = "CODIGO", columnDefinition = "CODIGO")
    var precosB2b: List<VSdGrupoPrecosB2b>? = null

    override fun canSendProduto(): Boolean {
        return (this.statusProcessamento == "0" || this.statusProcessamento == "2" || this.statusProcessamento == "4") && this.selected
    }

    override fun canSendEstoque(): Boolean {
        return (this.statusProcessamento == "2" || this.statusProcessamento == "4") && this.selected
    }

    fun toProductMagento(filtro: CamposProdutosEnviar): Product {
        val s = ArrayList<SingleData>()
        if (filtro.enviarAtributosAdicionais)
            s.add(SingleData("medidas_peca", tabelaMedida ?: ""))

        val descMarca = when (marca?.toLowerCase()) {
            "d" -> "DLZ"
            "f" -> "Flor de Lis"
            else -> {
                ""
            }
        }
        s.add(SingleData("marca", descMarca))

        return Product.Builder()
                .comSKU(codigo, filtro.enviarSKU)
                .comName(name, filtro.enviarNome)
                .comPrice(price, filtro.enviarPreco)
                .comGroupPrice(getGroupPrice(), filtro.enviarGroupPrice)
                .comShortDescription(shortDescription, filtro.enviarDescricaoCurta)
                .comDescription(description, filtro.enviarDescricao)
                .comSpecialPrice(specialPrice, filtro.enviarPrecoEspecial)
                .comWeight(weight, filtro.enviarPeso)
                .comStatus(status!!.toInt(), filtro.enviarStatus)
                .comMetaTitle(metaTitle, filtro.enviarTituloSEO)
                .comMetaKeyword(metaKeyword, filtro.enviarPalavasChaves)
                .comMetaDescription(metaDescription, filtro.enviarDescricaoSEO)
                .comCategories(getCategories(), filtro.enviarCategorias)
                .comVariations(getVariations(), filtro.enviarVariacoes)
                .comAditionalAttributes(AdditionalAttribute(s), true)
//                .comProductLinks(getProductLinks(), filtro.enviarLinkProdutos)
                .build()
    }

    private fun getGroupPrice(): ArrayList<GroupPrice> {
        val gP = ArrayList<GroupPrice>()
        precosB2b?.forEach {
            gP.add(GroupPrice(0, it.grupo?.toInt(), it.preco))
        }

        return gP
    }

    private fun getCategories(): ArrayList<ArrayList<Category>> {
        val categories = ArrayList<ArrayList<Category>>()
        var lista = ArrayList<Category>()

        // Adiciona a arvore de Marca > Família
        lista = ArrayList()
        lista.add(Category(when (marca?.toLowerCase()) {
            "d" -> "DLZ"
            "f" -> "Flor de Lis"
            else -> {
                ""
            }
        }))
        lista.add(Category(WordUtils.capitalize(categoria?.toLowerCase() ?: "")))
        categories.add(lista)

        // Adiciona a arvore de Marca > Entrega > Família
        estoque?.filter { vSdMrpEstoque -> vSdMrpEstoque.qtd!! > 0 }?.map { vSdMrpEstoque -> vSdMrpEstoque.descricao }
                ?.distinct()?.forEach {
                    lista = ArrayList()
                    lista.add(Category(when (marca?.toLowerCase()) {
                        "d" -> "DLZ"
                        "f" -> "Flor de Lis"
                        else -> {
                            ""
                        }
                    }))
                    if (it != null) {
                        lista.add(Category(WordUtils.capitalize(it.toLowerCase() ?: "")))
                    }
                    if (categoria ?: "" != "") {
                        lista.add(Category(WordUtils.capitalize(categoria?.toLowerCase() ?: "")))
                    }
                    categories.add(lista)
                }

        return categories
    }

    private fun getVariations(): Variation {
        val variation = Variation(null)
        // Cores
        cores?.let {
            variation.cor = ArrayList()
            for (cor in it) {
                val list = ArrayList<String>()
                list.add(cor.id!!.cor!!)
                list.add(cor.descricao!!)
                variation.cor!!.add(list)
            }
        }

        // Tamanhos
        val itens = FluentDao()
                .selectFrom(FaixaItem::class.java)
                .where {
                    it.equal("faixaItemId.faixa", faixa)
                }
                .orderBy("posicao", OrderType.ASC)
                .resultList<FaixaItem>()

        itens?.let {
            for (iten in itens) {
                variation.tamanho?.add(iten.faixaItemId.tamanho!!)
            }
        }

        return variation
    }

    //private fun getProductLinks(): ProductLinks {
    //    val productLinks = ProductLinks()
    //    crossSell?.let {
    //        val cross = crossSell!!.split(',')
    //
    //        for (c in cross) {
    //              productLinks.crossSell!!.add(c)
    //        }
    //    }
    //    return productLinks
    //}

}



