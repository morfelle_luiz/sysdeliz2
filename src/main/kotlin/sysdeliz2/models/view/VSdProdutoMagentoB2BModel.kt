package sysdeliz2.models.view

import tornadofx.*

/**
 *
 * @author lima.joao
 * @since 02/04/2020 11:04
 */
class VSdProdutoMagentoB2BModel(produto: VSdProdutoMagentoB2B) : ItemViewModel<VSdProdutoMagentoB2B>(produto) {
    val codigo = bind(VSdProdutoMagentoB2B::codigo)
    val tabelaPreco = bind(VSdProdutoMagentoB2B::tabelaPreco)
    val name = bind(VSdProdutoMagentoB2B::name)
    val description = bind(VSdProdutoMagentoB2B::description)
    val shortDescription = bind(VSdProdutoMagentoB2B::shortDescription)
    val categoria = bind(VSdProdutoMagentoB2B::categoria)
    val price = bind(VSdProdutoMagentoB2B::price)
    val specialPrice = bind(VSdProdutoMagentoB2B::specialPrice)
    val weight = bind(VSdProdutoMagentoB2B::weight)
    val status = bind(VSdProdutoMagentoB2B::status)
    val metaTitle = bind(VSdProdutoMagentoB2B::metaTitle)
    val metaKeyword = bind(VSdProdutoMagentoB2B::metaKeyword)
    val metaDescription = bind(VSdProdutoMagentoB2B::metaDescription)
    val faixa = bind(VSdProdutoMagentoB2B::faixa)
    val colecao = bind(VSdProdutoMagentoB2B::colecao)
    val colecaoDesc = bind(VSdProdutoMagentoB2B::colecaoDesc)
    val statusProcessamento = bind(VSdProdutoMagentoB2B::statusProcessamento)
    val linha = bind(VSdProdutoMagentoB2B::linha)
    val linhaDesc = bind(VSdProdutoMagentoB2B::linhaDesc)
    val tabelaMedida = bind(VSdProdutoMagentoB2B::tabelaMedida)
    val selected = bind(VSdProdutoMagentoB2B::selected)
}
