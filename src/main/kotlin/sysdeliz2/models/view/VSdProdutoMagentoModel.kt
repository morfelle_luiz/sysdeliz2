package sysdeliz2.models.view

import tornadofx.*

/**
 *
 * @author lima.joao
 * @since 29/11/2019 08:26
 */
class VSdProdutoMagentoModel(produto: VSdProdutoMagento) : ItemViewModel<VSdProdutoMagento>(produto) {

    val sku = bind(VSdProdutoMagento::sku)
    val tabelaPreco = bind(VSdProdutoMagento::tabelaPreco)
    val cor = bind(VSdProdutoMagento::cor)
    val corName  = bind(VSdProdutoMagento::corName)
    val name  = bind(VSdProdutoMagento::name)
    val description = bind(VSdProdutoMagento::description)
    val shortDescription = bind(VSdProdutoMagento::shortDescription)
    val categoria = bind(VSdProdutoMagento::categoria)
    val price = bind(VSdProdutoMagento::price)
    val specialPrice = bind(VSdProdutoMagento::specialPrice)
    val weight = bind(VSdProdutoMagento::weight)
    val status = bind(VSdProdutoMagento::status)
    val metaTitle = bind(VSdProdutoMagento::metaTitle)
    val metaKeyword = bind(VSdProdutoMagento::metaKeyword)
    val metaDescription = bind(VSdProdutoMagento::metaDescription)
    val faixa = bind(VSdProdutoMagento::faixa)
    val colecao = bind(VSdProdutoMagento::colecao)
    val statusProcessamento = bind(VSdProdutoMagento::statusProcessamento)
    val tabelaMedida = bind(VSdProdutoMagento::tabelaMedida)

}