package sysdeliz2.utils

/**
 *
 * @author lima.joao
 * @since 13/03/2020 15:56
 */
class CamposProdutosEnviar private constructor(
        val enviarSKU: Boolean = true,
        val enviarNome: Boolean = true,
        val enviarPreco: Boolean = true,

        val enviarDescricaoCurta: Boolean = true,
        val enviarDescricao: Boolean = false,
        val enviarPrecoEspecial: Boolean = false,
        val enviarPeso: Boolean = true,
        val enviarStatus: Boolean = false,
        val enviarTituloSEO: Boolean = true,
        val enviarPalavasChaves: Boolean = true,
        val enviarDescricaoSEO: Boolean = true,
        val enviarCategorias: Boolean = false,
        val enviarVariacoes: Boolean = false,
        val enviarAtributosAdicionais: Boolean = false,
        val enviarLinkProdutos: Boolean = false,
        val enviarGroupPrice: Boolean = true
) {
    class Builder() {
        private var enviarSKU: Boolean = true
        private var enviarNome: Boolean = true
        private var enviarPreco: Boolean = true

        private var enviarDescricaoCurta: Boolean = true
        private var enviarDescricao: Boolean = true
        private var enviarPrecoEspecial: Boolean = false
        private var enviarPeso: Boolean = true
        private var enviarStatus: Boolean = false
        private var enviarTituloSEO: Boolean = true
        private var enviarPalavasChaves: Boolean = true
        private var enviarDescricaoSEO: Boolean = true
        private var enviarCategorias: Boolean = false
        private var enviarVariacoes: Boolean = false
        private var enviarAtributosAdicionais: Boolean = false
        private var enviarLinkProdutos: Boolean = false
        private var enviarGroupPrice: Boolean = false

        private fun enviarSKU(enviarSKU: Boolean): Builder {
            this.enviarSKU = enviarSKU
            return this
        }

        private fun enviarNome(enviarNome: Boolean): Builder {
            this.enviarNome = enviarNome
            return this
        }

        private fun enviarPreco(enviarPreco: Boolean): Builder {
            this.enviarPreco = enviarPreco
            return this
        }

        fun enviarGroupPrice(enviarGroupPrice: Boolean): Builder {
            this.enviarGroupPrice = enviarGroupPrice
            return this
        }

        fun enviarDescricaoCurta(enviarDescricaoCurta: Boolean): Builder {
            this.enviarDescricaoCurta = enviarDescricaoCurta
            return this
        }

        fun enviarDescricao(enviarDescricao: Boolean): Builder {
            this.enviarDescricao = enviarDescricao
            return this
        }

        fun enviarPrecoEspecial(enviarPrecoEspecial: Boolean): Builder {
            this.enviarPrecoEspecial = enviarPrecoEspecial
            return this
        }

        fun enviarPeso(enviarPeso: Boolean): Builder {
            this.enviarPeso = enviarPeso
            return this
        }

        fun enviarStatus(enviarStatus: Boolean): Builder {
            this.enviarStatus = enviarStatus
            return this
        }

        fun enviarTituloSEO(enviarTituloSEO: Boolean): Builder {
            this.enviarTituloSEO = enviarTituloSEO
            return this
        }

        fun enviarPalavasChaves(enviarPalavasChaves: Boolean): Builder {
            this.enviarPalavasChaves = enviarPalavasChaves
            return this
        }

        fun enviarDescricaoSEO(enviarDescricaoSEO: Boolean): Builder {
            this.enviarDescricaoSEO = enviarDescricaoSEO
            return this
        }

        fun enviarCategorias(enviarCategorias: Boolean): Builder {
            this.enviarCategorias = enviarCategorias
            return this
        }

        fun enviarVariacoes(enviarVariacoes: Boolean): Builder {
            this.enviarVariacoes = enviarVariacoes
            return this
        }

        fun enviarAtributosAdicionais(enviarAtributosAdicionais: Boolean): Builder {
            this.enviarAtributosAdicionais = enviarAtributosAdicionais
            return this
        }

        fun enviarLinkrodutos(enviarLinkProdutos: Boolean): Builder {
            this.enviarLinkProdutos = enviarLinkProdutos
            return this
        }

        fun build(): CamposProdutosEnviar {
            return CamposProdutosEnviar(
                    enviarSKU,
                    enviarNome,
                    enviarPreco,
                    enviarDescricaoCurta,
                    enviarDescricao,
                    enviarPrecoEspecial,
                    enviarPeso,
                    enviarStatus,
                    enviarTituloSEO,
                    enviarPalavasChaves,
                    enviarDescricaoSEO,
                    enviarCategorias,
                    enviarVariacoes,
                    enviarAtributosAdicionais,
                    enviarLinkProdutos,
                    enviarGroupPrice
            )
        }
    }
}