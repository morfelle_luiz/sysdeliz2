package sysdeliz2.utils

import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.WriterException
import com.google.zxing.client.j2se.MatrixToImageWriter
import com.google.zxing.qrcode.QRCodeWriter
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel
import sysdeliz2.models.ti.Marca
import java.awt.image.BufferedImage
import java.awt.image.IndexColorModel
import java.io.File
import java.io.IOException
import java.nio.file.FileSystems
import java.util.*
import kotlin.collections.ArrayList

/**
 * @author lima.joao
 * @since 07/10/2019 08:51
 */
object ImageUtils {
    /*
     * Converte a imagem passara para preto e branco
     */
    fun image2BlackWhite(image1: BufferedImage): BufferedImage {
        val w = image1.width
        val h = image1.height
        val comp = byteArrayOf(0, -1)
        val cm = IndexColorModel(2, 2, comp, comp, comp)
        val image2 = BufferedImage(w, h, BufferedImage.TYPE_BYTE_INDEXED, cm)
        val g = image2.createGraphics()
        g.drawRenderedImage(image1, null)
        g.dispose()
        return image2
    }

    /*
     * Converte a imagem passara para escala de cinza
     */
    @JvmStatic
    fun image2GrayScale(image: BufferedImage): BufferedImage {
        val imageGray = BufferedImage(image.width, image.height, BufferedImage.TYPE_BYTE_GRAY)
        val g = imageGray.graphics
        g.drawImage(image, 0, 0, null)
        g.dispose()
        return imageGray
    }

    /*
     * Cria um QRCode
     */
    @JvmStatic
    fun buildQRCode(text: String, width: Int, height: Int, fullFileNamePath: String) {
        val qrCodeWriter = QRCodeWriter()
        try {
            val map: MutableMap<EncodeHintType, Enum<*>?> = EnumMap(com.google.zxing.EncodeHintType::class.java)
            map[EncodeHintType.ERROR_CORRECTION] = ErrorCorrectionLevel.H
            val bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height, map)
            val path = FileSystems.getDefault().getPath(fullFileNamePath)
            MatrixToImageWriter.writeToPath(bitMatrix, "PNG", path)
        } catch (e: WriterException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    /*
     * @author cristiano.diego
     * Deleta um QRCode
     */
    @JvmStatic
    fun deleteImage(fullFileNamePath: String) {
        try {
            var imageFile = File(fullFileNamePath)
            imageFile.deleteRecursively()
        } catch (e: WriterException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    @JvmStatic
    fun getFolderMarca(marca: Marca): String? {
        return if (marca.sdGrupo == "D") {
            "DLZ/"
        } else if (marca.sdGrupo == "F") {
            "Flor De Lis/"
        } else {
            marca.descricao + "/"
        }
    }

    fun loadImagemListByFolder(pathBase:String, fileName: String?): List<String> {

        val retorno = ArrayList<String>()

        var files = File(pathBase)

        if( !files.exists() ){
            return retorno
        }

        files.list()?.let{imgs ->
            imgs.forEach {
                if(it.toUpperCase().endsWith(".JPG")){
                    if(fileName != null) {
                        if(it.startsWith(fileName) ) {
                            retorno.add(it)
                        }
                    } else {
                        retorno.add(it)
                    }
                }
            }
        }

        return retorno
        /*
        private void buildPath(boolean isFichaTecnica) throws SQLException {
        String pathBase = "K:/";
        String pathBaseFichaTecnica = "FichaTecnica/";
        String pathBaseTabelaMedida = "Tabela de Medidas/";

        if (isFichaTecnica) {
            pathFichaTecnica = "";
            String base = pathBase + pathBaseFichaTecnica;
            base += ImageUtils.getFolderMarca(selectedOF1.getProduto().getMarca().getSdGrupo());

            File folder = new File(base);
            String[] folders = folder.list();

            if (folders != null) {
                for (String s : folders) {
                    if (s.startsWith(selectedOF1.getProduto().getColecao().getCodigo())) {
                        base += s + "/";
                        break;
                    }
                }

                File folderColecao = new File(base);
                String[] filesColecao = folderColecao.list();
                if(filesExtras == null) {
                    filesExtras = new ArrayList<>();
                } else {
                    filesExtras.clear();
                }
                assert filesColecao != null;
                for (String s : filesColecao) {
                    String tmp = s.toUpperCase();
                    if (
                            ( tmp.startsWith(selectedOF1.getProduto().getCodigo() + ".") || tmp.startsWith(selectedOF1.getProduto().getCodigo() + "_") ) &&
                                    (!tmp.equals(selectedOF1.getProduto().getCodigo() + "_FT.JPG")) ) {
                        filesExtras.add(base + tmp);
                    }
                }

                base += selectedOF1.getProduto().getCodigo() + "_FT.JPG";
                if (new File(base).exists()) {
                    pathFichaTecnica = base;
                }
            }
        } else {
            if (filesTabelaMedida == null) {
                filesTabelaMedida = new ArrayList<>();
            } else {
                filesTabelaMedida.clear();
            }

            String base = pathBase + pathBaseTabelaMedida;
            base += ImageUtils.getFolderMarca(selectedOF1.getProduto().getMarca().getSdGrupo());

            File folder = new File(base);
            String[] folders = folder.list();

            if (folders != null) {
                for (String s : folders) {
                    if (s.startsWith(selectedOF1.getProduto().getColecao().getCodigo() ) || s.startsWith(selectedOF1.getProduto().getColecao().getCodigo() )) {
                        base += s + "/";
                        break;
                    }
                }

                File folderColecao = new File(base);
                String[] filesColecao = folderColecao.list();
                String sufixo = "";
                String sufixo_jeans = "";

                if (filesColecao != null) {
                    GenericDao<PcpFtOf> pcpFtOfGenericDao = new GenericDaoImpl<>(JPAUtils.getEntityManager(), PcpFtOf.class);

                    if(selectedOF1.getPeriodo().equals("M")){
                        sufixo = "M";
                        sufixo_jeans = "2";
                    }

                    for (String s : filesColecao) {
                        if (s.contains(selectedOF1.getProduto().getCodigo() + ".") ||
                                s.contains(selectedOF1.getProduto().getCodigo() + "_" + sufixo) ||
                                s.contains(selectedOF1.getProduto().getCodigo() + "_" + sufixo_jeans)
                        ) {
                            String[] frag = s.split("_");
                            if(frag.length <= 2) {
                                if(sufixo.equals("") && (!s.contains("_M") && !s.contains("_2"))) {
                                    filesTabelaMedida.add(base + s);
                                } else {
                                    if(!sufixo.equals("") && (s.contains("_M") && s.contains("_2"))){
                                        filesTabelaMedida.add(base + s);
                                    }
                                }
                            } else {
                                ObservableList pcpFtOfList = pcpFtOfGenericDao
                                        .initCriteria()
                                        .addPredicateEqPkEmbedded("pcpFtOfID", "numero", selectedOF1.getNumero(), false)
                                        .loadListByPredicate();

                                for (Object pcpFtOf : pcpFtOfList) {
                                    if(s.contains(((PcpFtOf) pcpFtOf).getPcpFtOfID().getInsumo())){
                                        boolean existe = false;
                                        for (String s1 : filesTabelaMedida) {
                                            existe = s1.equals(base + s);
                                        }

                                        if(!existe) {
                                            filesTabelaMedida.add(base + s);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

    }
         */
    }


}