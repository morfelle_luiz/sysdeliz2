package sysdeliz2.utils

import org.osgi.service.component.annotations.Component
import sysdeliz2.utils.stylesheets.SysdelizCSS
import tornadofx.osgi.StylesheetProvider


/**
 *
 * @author lima.joao
 * @since 28/01/2020 13:29
 */
@Component
class StyleRegistration : StylesheetProvider {
    override val stylesheet = SysdelizCSS::class
}