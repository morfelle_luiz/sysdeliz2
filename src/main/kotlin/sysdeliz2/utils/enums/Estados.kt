package sysdeliz2.utils.enums

/**
 *
 * @author lima.joao
 * @since 30/03/2020 09:56
 */
enum class Estados(val nome: String) {
    AC("Acre"){ override fun valueOf(): String = "AC" },
    AL("Alagoas"){ override fun valueOf(): String = "AL" },
    AP("Amapá"){ override fun valueOf(): String = "AP" },
    AM("Amazonas"){ override fun valueOf(): String = "AM" },
    BA("Bahia"){ override fun valueOf(): String = "BA" },
    CE("Ceará"){ override fun valueOf(): String = "CE" },
    DF("Distrito Federal"){ override fun valueOf(): String = "DF" },
    ES("Espírito Santo"){ override fun valueOf(): String = "ES" },
    GO("Goiás"){ override fun valueOf(): String = "GO" },
    MA("Maranhão"){ override fun valueOf(): String = "MA" },
    MT("Mato Grosso"){ override fun valueOf(): String = "MT" },
    MS("Mato Grosso do Sul"){ override fun valueOf(): String = "MS" },
    MG("Minas Gerais"){ override fun valueOf(): String = "MG" },
    PA("Pará"){ override fun valueOf(): String = "PA" },
    PB("Paraíba"){ override fun valueOf(): String = "PB" },
    PR("Paraná"){ override fun valueOf(): String = "PR" },
    PE("Pernambuco"){ override fun valueOf(): String = "PE" },
    PI("Piauí"){ override fun valueOf(): String = "PI" },
    RJ("Rio de Janeiro"){ override fun valueOf(): String = "RJ" },
    RN("Rio Grande do Norte"){ override fun valueOf(): String = "RN" },
    RS("Rio Grande do Sul"){ override fun valueOf(): String = "RS" },
    RO("Rondônia"){ override fun valueOf(): String = "RO" },
    RR("Roraima"){ override fun valueOf(): String = "RR" },
    SC("Santa Catarina"){ override fun valueOf(): String = "SC" },
    SP("São Paulo"){ override fun valueOf(): String = "SP" },
    SE("Sergipe"){ override fun valueOf(): String = "SE" },
    TO("Tocantins"){ override fun valueOf(): String = "TO" };

    abstract fun valueOf(): String

    companion object {
        fun getUF(uf: String?): String {
            return when (uf) {
                AC.nome -> AC.valueOf()
                AL.nome -> AL.valueOf()
                AP.nome -> AP.valueOf()
                AM.nome -> AM.valueOf()
                BA.nome -> BA.valueOf()
                CE.nome -> CE.valueOf()
                DF.nome -> DF.valueOf()
                ES.nome -> ES.valueOf()
                GO.nome -> GO.valueOf()
                MA.nome -> MA.valueOf()
                MT.nome -> MT.valueOf()
                MS.nome -> MS.valueOf()
                MG.nome -> MG.valueOf()
                PA.nome -> PA.valueOf()
                PB.nome -> PB.valueOf()
                PR.nome -> PR.valueOf()
                PE.nome -> PE.valueOf()
                PI.nome -> PI.valueOf()
                RJ.nome -> RJ.valueOf()
                RN.nome -> RN.valueOf()
                RS.nome -> RS.valueOf()
                RO.nome -> RO.valueOf()
                RR.nome -> RR.valueOf()
                SC.nome -> SC.valueOf()
                SP.nome -> SP.valueOf()
                SE.nome -> SE.valueOf()
                TO.nome -> TO.valueOf()
                else -> ""
            }
        }
    }

}