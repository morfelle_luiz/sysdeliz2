package sysdeliz2.utils.enums

/**
 *
 * @author lima.joao
 * @since 10/12/2019 15:50
 */
enum class MarcaEnum(val value: String) {
    DLZ("D"),
    FLOR("F")
}