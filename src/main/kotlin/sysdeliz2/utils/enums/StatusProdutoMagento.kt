package sysdeliz2.utils.enums

/**
 *
 * @author lima.joao
 * @since 07/01/2020 14:52
 */
enum class StatusProdutoMagento(val value: String) {
    UNDEFINED("-1") {
        override fun valueOf(): String = "Não Informado"
    },
    PENDENTE("0") {
        override fun valueOf(): String = "Pendente"
    },
    ENVIADO("1") {
        override fun valueOf(): String = "Enviado"
    },
    PROCESSADO("2") {
        override fun valueOf(): String = "Processado"
    },
    ENVIO_ESTOQUE("3") {
        override fun valueOf(): String = "Env. Estoque"
    },
    ENVIO_FOTO("5") {
        override fun valueOf(): String = "Env. Foto"
    },
    PRODUTO_OK("4") {
        override fun valueOf(): String = "Prod. OK"
    };

    abstract fun valueOf(): String

}