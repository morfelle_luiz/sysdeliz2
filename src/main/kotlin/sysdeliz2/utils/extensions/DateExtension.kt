package sysdeliz2.utils.extensions

import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.util.*

/**
 *
 * @author lima.joao
 * @since 27/01/2020 08:35
 */
object DateExtension {
    val locale = Locale("pt", "BR")
    val simpleDateTimeFormat = SimpleDateFormat("dd/MM/yyyy HH:mm", locale)
    val simpleDateFormat = SimpleDateFormat("dd/MM/yyyy", locale)
    val simpleTimeFormat = SimpleDateFormat("HH:mm", locale)

    val simpleLocalDateTimeFormat: DateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm", locale)
    val simpleLocalDateFormat: DateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy", locale)

    val simpleLocalTimeFormat: DateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm", locale)

    val simpleLocalDateMagentoFormat: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd", locale)
    val simpleLocalDateTimeMagentoFormat: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss", locale)

}


fun Date.toFormatedDateTime(): String = DateExtension.simpleDateTimeFormat.format(this)

fun Date.toFormatedDate(): String = DateExtension.simpleDateFormat.format(this)

fun Date.toFormatedTime(): String = DateExtension.simpleTimeFormat.format(this)

fun LocalDateTime.toFormatedDateTime(): String = this.format(DateExtension.simpleLocalDateTimeFormat)

fun LocalDateTime.toFormatedDate(): String = this.format(DateExtension.simpleLocalDateFormat)

fun LocalDateTime.toFormatedTime(): String = this.format(DateExtension.simpleLocalTimeFormat)

fun LocalDate.toFormatedDateTime(): String = this.format(DateExtension.simpleLocalDateTimeFormat)

fun LocalDate.toFormatedDate(): String = this.format(DateExtension.simpleLocalDateFormat)

fun LocalDate.toFormatedTime(): String = this.format(DateExtension.simpleLocalTimeFormat)

fun LocalTime.toFormatedDateTime(): String = this.format(DateExtension.simpleLocalDateTimeFormat)

fun LocalTime.toFormatedDate(): String = this.format(DateExtension.simpleLocalDateFormat)

fun LocalTime.toFormatedTime(): String = this.format(DateExtension.simpleLocalTimeFormat)
