package sysdeliz2.utils.extensions

import javax.persistence.EntityManager

/**
 *
 * @author lima.joao
 * @since 18/02/2020 14:00
 */
fun EntityManager.run(autoCommit: Boolean = false, block: (it: EntityManager) -> Any): Any {
    val logger = loggerFor(javaClass)

    if (autoCommit) {
        logger.debug("Iniciando transação")
        this.transaction.begin()
    }

    // Executa o bloco
    logger.debug("run() -> Iniciando execução do bloco")
    val retorno: Any = block(this)
    logger.debug("run() -> Concluido execução do bloco")

    if (autoCommit && this.transaction.isActive) {
        logger.debug("Encerrando transação")
        this.transaction.commit()
    }
    return retorno
}