package sysdeliz2.utils.extensions

import org.slf4j.LoggerFactory

/**
 *
 * @author lima.joao
 * @since 18/02/2020 11:15
 */
inline fun <reified T:Any> loggerFor(clazz: Class<T>) = LoggerFactory.getLogger(clazz)

