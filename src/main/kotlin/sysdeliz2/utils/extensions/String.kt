package sysdeliz2.utils.extensions

import br.com.caelum.stella.format.CNPJFormatter
import br.com.caelum.stella.format.CPFFormatter
import javafx.scene.input.Clipboard
import javafx.scene.input.ClipboardContent
import java.text.Normalizer
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.util.*

/**
 *
 * @author lima.joao
 * @since 06/02/2020 15:14
 */

private val REGEX_UNACCENT = "\\p{InCombiningDiacriticalMarks}+".toRegex()

fun String.toFormatedCpfCnpj(): String {
    return if(length <= 11) {
        val cpfFormatter = CPFFormatter()
        cpfFormatter.format(cpfFormatter.unformat(this))
    } else {
        val cnpjFormatter = CNPJFormatter()
        cnpjFormatter.format(cnpjFormatter.unformat(this))
    }
}

fun String.getTipoEntidade(): String {
    return if(this.length <= 14) {
        "1"
    } else {
        "2"
    }
}


fun String.toLocalDateTime(): LocalDateTime = LocalDateTime.parse(this, DateExtension.simpleLocalDateTimeFormat)

fun String.toLocalDateTimeMagento(): LocalDateTime = LocalDateTime.parse(this, DateExtension.simpleLocalDateTimeMagentoFormat)

fun String.toLocalDate(): LocalDate = LocalDate.parse(this, DateExtension.simpleLocalDateFormat)

fun String.toLocalDateMagento(): LocalDate = LocalDate.parse(this, DateExtension.simpleLocalDateMagentoFormat)

fun String.toLocalTime(): LocalTime = LocalTime.parse(this, DateExtension.simpleLocalTimeFormat)

fun String.normalize(): String {
    val temp = Normalizer.normalize(this, Normalizer.Form.NFD)
    return REGEX_UNACCENT.replace(temp, "")
}

fun String.toLength(finalLength: Int): String {
    return if((this).length > finalLength){ this.substring(0, finalLength) } else { this }
}

fun String.toClipboardFX(): Boolean {
    val clipboard = Clipboard.getSystemClipboard()
    val content = ClipboardContent()
    content.putString(this)
    return clipboard.setContent(content)
}