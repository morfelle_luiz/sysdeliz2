package sysdeliz2.utils.hibernate

import org.apache.commons.lang.StringUtils
import sysdeliz2.utils.extensions.loggerFor
import sysdeliz2.utils.extensions.run
import java.math.BigDecimal
import javax.persistence.EntityManager
import javax.persistence.EntityManagerFactory
import javax.persistence.Persistence

/**
 * @author lima.joao
 * @since 15/07/2019 08:50
 */
object JPAUtils{

    private val factory: EntityManagerFactory? = when(sysdeliz2.utils.Globals.getEmpresaLogada().codigo) {
        "2000" -> Persistence.createEntityManagerFactory("SysDeliz2UPWPU")
        else -> Persistence.createEntityManagerFactory("SysDeliz2PU")
    }

    @JvmStatic
    //@Deprecated(message = "O EntityManager como singleton mantém cache e bloqueia a limpeza por poder estar sendo usado em mais \n de uma dao simultaneamente. " +
    //        "\nUsando a nova abordagem garente que cada DAO receberá uma instancia limpa do entityManager.")
    val entityManager: EntityManager? = factory!!.createEntityManager()

    private val logger = loggerFor(javaClass)

    @JvmStatic
    fun destroy() {
        factory?.let{ fac ->
            entityManager?.let{
                if (it.isOpen) {
                    // Se a houver uma transação aberta encerra
                    if (it.transaction.isActive) {
                        it.transaction.rollback()
                    }
                    // Fecha o entity manager
                    it.close()
                }
            }
            // fecha a fabrica
            if (fac.isOpen) {
                fac.close()
            }
        }
    }

    @Throws(Exception::class)
    @JvmStatic
    fun run(autoCommit: Boolean = false, block: (it: EntityManager) -> Any): Any? {
        if (entityManager == null) {
            logger.error("(Método run($autoCommit){}) - EntityManager não foi inicializado corretamente.  Verifique os logs e tente novamente")
            throw Exception("(Método run($autoCommit){}) - EntityManager não foi iniciazado corretamente. Verifique os logs e tente novamente")
        }

        return entityManager.run(autoCommit) {
            block(entityManager)
        }
    }

    @Throws(Exception::class)
    @JvmStatic
    fun getNextNumFromCodigo001(tabela: String, campo: String): String {

        // Select para pegar o valor do codigo
        val selectSQL = "select PROXIMO from CODIGOS_001 where TABELA = '$tabela' and CAMPO = '$campo'"

        // Select para pegar o tamanho final do campo
        val tamanhoSQL = "select TAMANHO from CODIGOS_001 where TABELA = '$tabela' and CAMPO = '$campo'"

        // Pega o tamanho da string de retorno
        val tamanho: Int = run {
            (it.createNativeQuery(tamanhoSQL).singleResult as BigDecimal).toInt()
        } as Int

        // Pega o valor do incremento atual
        var atual: Int = run {
            (it.createNativeQuery(selectSQL).singleResult as BigDecimal).toInt()
        } as Int

        // Valor da chave a ser retornado
        val retorno = StringUtils.leftPad(atual.toString(), tamanho, "0")

        // Incrementa o valor com o proximo valor
        atual++

        // Grava o valor no banco
        val updateSQL = "update CODIGOS_001 " +
                " set PROXIMO = " + atual +
                " where TABELA = '" + tabela + "' and CAMPO = '" + campo + "'"

        // Executa o update
        val result = run(true) {
            it.createNativeQuery(updateSQL).executeUpdate()
        } as Int

        if (result > 1) {
            throw Exception("Ocorreu um problema, método deveria ter atualizado apenas uma linha porem atualizou $result")
        }

        return retorno
    }

    @JvmStatic
    fun clearEntitys(values: List<Any>?) {
        values?.forEach { entityManager?.detach(it) }
    }

    @JvmStatic
    fun clearEntity(value: Any?) {
        if (value != null)
            entityManager?.detach(value)
    }

    @JvmStatic
    fun refresh(value: Any?) {
        if (value != null)
            entityManager?.refresh(value)
    }
}

