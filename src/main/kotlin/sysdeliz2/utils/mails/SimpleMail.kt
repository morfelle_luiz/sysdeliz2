package sysdeliz2.utils.mails

import sysdeliz2.utils.ContentIdGenerator
import sysdeliz2.utils.GUIUtils
import sysdeliz2.utils.extensions.loggerFor
import sysdeliz2.utils.sys.MailUtils
import java.io.File
import java.io.IOException
import java.util.*
import java.util.logging.Level
import java.util.logging.Logger
import javax.mail.*
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeBodyPart
import javax.mail.internet.MimeMessage
import javax.mail.internet.MimeMultipart
import kotlin.collections.ArrayList

/**
 *
 * @author lima.joao
 * @since 26/02/2020 08:54
 */
object SimpleMail {

    private val logger = loggerFor(javaClass)

    private const val host_mail = "serverexchange.nowar.corp"
    private const val smtp_port = "587"
    private const val from_mail_s = "relatorios@deliz.com.br"
    private const val pass_mail_s = "d25m04"
    //private const val from_mail_s = "servicos@deliz.com.br"
    //private const val pass_mail_s = "d25m04"

    private val destinatarios = ArrayList<Address>()
    private val cc = ArrayList<Address>()
    private val cco = ArrayList<Address>()
    private val reply = ArrayList<Address>()
    private val attachments = ArrayList<String>()
    private val files = ArrayList<File>()

    private var assunto = ""
    private var body = ""
    private var senderName = ""

    private val properties = Properties()
    private val session: Session
    private val message: MimeMessage

    private var multipart = MimeMultipart()
    private lateinit var mimeMultipart: MimeMultipart

    init {
        logger.debug("----------------------- Inicializando Envio de email -----------------------")

        properties["mail.smtp.host"] = host_mail
        properties["mail.smtp.port"] = smtp_port
        properties["mail.smtp.auth"] = "true"
        properties["mail.smtp.starttls.enable"] = "false"
        properties["mail.debug"] = "true"

        logger.debug("  ------------------------------------------------ ")
        logger.debug("  > mail.smtp.host = $host_mail")
        logger.debug("  > mail.smtp.port = $smtp_port")
        logger.debug("  > mail.smtp.auth = true")
        logger.debug("  > mail.smtp.starttls.enable = false")
        logger.debug("  > mail.debug = true")

        logger.debug("  ------------------------------------------------ ")
        logger.debug("  Instânciando Sessão")
        session = Session.getInstance(
                properties,
                object : Authenticator() {
                    override fun getPasswordAuthentication(): PasswordAuthentication {
                        return PasswordAuthentication(from_mail_s, pass_mail_s)
                    }
                }
        )
        logger.debug("  Sessão instanciada")
        logger.debug("  Criando a Mensagem")
        message = MimeMessage(session)
        logger.debug("  Mensagem criado")
        logger.debug("  ------------------------------------------------ ")
    }

    private fun clear() {
        destinatarios.clear()
        cc.clear()
        reply.clear()
        attachments.clear()
        assunto = ""
        body = ""
        senderName = ""
    }

    fun addReplyTo(mail: String): SimpleMail {
        logger.debug(" Add reply -> $mail")
        reply.add(InternetAddress(mail))
        return this
    }

    fun addDestinatario(email: String): SimpleMail {
        logger.debug("  Add destinatário -> $email")
        destinatarios.add(InternetAddress(email))
        return this
    }

    fun addCC(email: String): SimpleMail {
        logger.debug("  Add CC -> $email")
        cc.add(InternetAddress(email))
        return this
    }

    fun addCC(emails: List<String>): SimpleMail {
        logger.debug("  Add lista de CC -> $emails")
        emails.forEach {
            logger.debug("  Add CC -> $it")
            cc.add(InternetAddress(it))
        }
        return this
    }

    fun addCCO(email: String): SimpleMail {
        logger.debug("  Add CCO -> $email")
        cco.add(InternetAddress(email))
        return this
    }

    fun setSender(descricao: String): SimpleMail {
        senderName = descricao
        return this
    }

    fun comAssunto(assunto: String): SimpleMail {
        logger.debug("  Add Assunto -> $assunto")
        this.assunto = assunto
        return this
    }

    fun comCorpo(corpo: String): SimpleMail {
//        this.body = corpo
//        return this
        body = corpo

        // Corpo
        val messagePart = MimeBodyPart()
        logger.debug(" Configurando Body: Charset=UTF-8, Subtype=html, Body: $body")
        messagePart.setText(body, "UTF-8", "html")

        // Inicia o Multipart
        mimeMultipart = MimeMultipart()
        mimeMultipart.addBodyPart(messagePart)

        return this
    }

    fun comCorpoHtml(block: () -> Any): SimpleMail {
//        logger.debug("  Add Corpo -> Atual: $body")
//        block(this)
//        logger.debug("            -> Final: $body")
//        return this
        logger.debug("  Add Corpo -> Atual: $body")
        body = block() as String

        // Corpo
        val messagePart = MimeBodyPart()
        logger.debug(" Configurando Body: Charset=UTF-8, Subtype=html, Body: $body")
        messagePart.setText(body, "UTF-8", "html")

        // Inicia o Multipart
        mimeMultipart = MimeMultipart()
        mimeMultipart.addBodyPart(messagePart)

        logger.debug("            -> Final: $body")
        return this
    }

    fun comMultipartBody(block: (multipart: MimeMultipart) -> Any): SimpleMail {
        logger.debug("  Add Corpo -> Atual: $body")
        mimeMultipart = MimeMultipart("related")
        block(mimeMultipart)

        logger.debug("            -> Final: $body")
        return this
    }

    fun addAttachment(attachment: String): SimpleMail {
        logger.debug("  Add anexo -> $attachment")
        this.attachments.add(attachment)
        return this
    }

    fun addAttachment(attachment: File): SimpleMail {
        logger.debug("  Add anexo -> $attachment")
        this.files.add(attachment)
        return this
    }

    fun send() {
        logger.debug(" --------- INICIO DO ENVIO DO EMAIL --------- ")
        try {
            // Remetente
            logger.debug(" Configurando Rementente: $from_mail_s ")
            senderName = when (senderName.length) {
                0 -> "Serviços SysDeliz"
                else -> senderName
            }
            message.setFrom(InternetAddress(from_mail_s, senderName))

            if (reply.isNotEmpty()) {
                message.setReplyTo(reply.toTypedArray())
            }

            // Destinatario
            logger.debug(" Configurando Destinatários: ${destinatarios.toTypedArray()}")
            message.setRecipients(Message.RecipientType.TO, destinatarios.toTypedArray())

            // CC
            if (cc.isNotEmpty()) {
                logger.debug(" Configurando Destinatários em cópia: ${cc.toTypedArray()}")
                message.setRecipients(Message.RecipientType.CC, cc.toTypedArray())
            }

            // CCO
            if (cco.isNotEmpty()) {
                logger.debug(" Configurando Destinatários em cópia oculta: ${cco.toTypedArray()}")
                message.setRecipients(Message.RecipientType.BCC, cco.toTypedArray())
            }

            // Assunto
            logger.debug(" Configurando assunto: $assunto")
            message.subject = assunto

            // Data de Envio
            val dtEnvio = Date()
            logger.debug(" Configurando Data de Envio: ${dtEnvio.toString()}")
            message.sentDate = dtEnvio

            // Corpo
            val messagePart = MimeBodyPart()
            logger.debug(" Configurando Body: Charset=UTF-8, Subtype=html, Body: $body")
            messagePart.setText(body, "UTF-8", "html")

            val cid = ContentIdGenerator.getContentId()
            logger.debug(" Gerado CID=$cid")

            var attachmentList: ArrayList<MimeBodyPart>? = null

            // Build attachment list
            if (!attachments.isNullOrEmpty()) {
                logger.debug(" Inserindo os Anexos informados")
                attachmentList = ArrayList()

                for (attachment in attachments) {
                    val part = MimeBodyPart()
                    part.attachFile(attachment)
                    part.contentID = cid
                    part.disposition = MimeBodyPart.INLINE
                    attachmentList.add(part)
                }
            }

            if (files.isNotEmpty()) {
                logger.debug(" Inserindo os Anexos informados")
                if (attachmentList == null) attachmentList = ArrayList()

                for (file in files) {
                    val part = MimeBodyPart()
                    part.attachFile(file)
                    part.contentID = cid
                    part.disposition = MimeBodyPart.INLINE
                    attachmentList.add(part)
                }
            }

            logger.debug(" Juntando partes dentro de um multipart")
            //multipart.addBodyPart(messagePart)
            attachmentList?.let {
                for (mimeBodyPart in it) {
                    logger.debug(" Adicinando anexo")
                    mimeMultipart.addBodyPart(mimeBodyPart)
                }
            }

            // Build attachment out list
//            var attachmentOutList: ArrayList<MimeBodyPart>? = null
//            if (!attachments.isNullOrEmpty()) {
//                logger.debug(" Inserindo os Anexos informados")
//                attachmentOutList = ArrayList()
//                for (attachment in attachments) {
//                    val part = MimeBodyPart()
//                    part.attachFile(attachment)
//                    part.contentID = cid + "OUT"
//                    part.disposition = MimeBodyPart.ATTACHMENT
//                    attachmentOutList.add(part)
//                }
//            }
//            attachmentOutList?.let {
//                for (mimeBodyPart in it) {
//                    logger.debug(" Adicinando anexo")
//                    mimeMultipart.addBodyPart(mimeBodyPart)
//                }
//            }

            logger.debug(" Inserindo o conteudo na mensagem")
            message.setContent(mimeMultipart)
            logger.debug(" Enviando a mensagem")
            Transport.send(message)
            clear()
        } catch (e: MessagingException){
            e.printStackTrace()
            if (e.message?.contains("Authentication unsuccessful")!!)
                send()
            else
                GUIUtils.showException(e)

        } catch (ex: IOException){
            Logger.getLogger(SimpleMail.javaClass.name).log(Level.SEVERE, null, ex)
            GUIUtils.showException(ex)
        }
    }

    @JvmStatic
    fun getDefaultStyle(): String {
        return "    <style type=\"text/css\">" +
                "        body {" +
                "            color: #444;" +
                "            font: 100%/25px 'Helvetica Neue', helvetica, arial, sans-serif;" +
                "        }" +
                "        table {" +
                "            background: #f5f5f5;" +
                "            border-collapse: separate;" +
                "            box-shadow: inset 0 1px 0 #fff;" +
                "            font-size: 12px;" +
                "            line-height: 14px;" +
                "            text-align: left;" +
                "        }" +
                "        th {" +
                "            background-color: #777;" +
                "            border-left: 1px solid #555;" +
                "            border-right: 1px solid #777;" +
                "            border-top: 1px solid #555;" +
                "            border-bottom: 1px solid #333;" +
                "            box-shadow: inset 0 1px 0 #999;" +
                "            color: #fff;" +
                "            font-weight: bold;" +
                "            padding: 5px 10px;" +
                "            position: relative;" +
                "            text-shadow: 0 1px 0 #000;" +
                "        }" +
                "        td {" +
                "            border-right: 1px solid #fff;" +
                "            border-left: 1px solid #e8e8e8;" +
                "            border-top: 1px solid #fff;" +
                "            border-bottom: 1px solid #e8e8e8;" +
                "            padding: 5px 10px;" +
                "            position: relative;" +
                "            transition: all 300ms;" +
                "            text-transform: uppercase;" +
                "        }" +
                "        span {" +
                "            font: 90%/10px 'Helvetica Neue', helvetica, arial, sans-serif;" +
                "        }" +
                "    </style>"
    }

}