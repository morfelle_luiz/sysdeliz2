package sysdeliz2.utils.stylesheets

import tornadofx.*

/**
 *
 * @author lima.joao
 * @since 14/11/2019 09:30
 */
class SysdelizCSS: Stylesheet() {

    companion object {
        const val colorFieldSet = "#F5F5F5"
        const val colorFieldlabel = "#cecece"
    }

    init {

        labelContainer {
            padding = box(0.px)
            backgroundColor = multi(c(colorFieldlabel))
            borderColor = multi(box(c(colorFieldlabel)))
            borderRadius = multi(box(2.px, 2.px, 0.px, 0.px))
            backgroundRadius = multi(box(2.px, 2.px, 0.px, 0.px))

            label {
                backgroundColor = multi(c(colorFieldlabel))
                borderColor = multi(box(c(colorFieldlabel)))
                padding = box(0.px, 5.px, 0.px, 3.px)
            }
        }


//        mFieldSet {
//            fontSize = 25.px
//            backgroundColor += c(SysdelizCSS.colorFieldSet)
//            padding = box(5.px)
//            borderRadius = multi(box(5.px))
//        }
    }

}