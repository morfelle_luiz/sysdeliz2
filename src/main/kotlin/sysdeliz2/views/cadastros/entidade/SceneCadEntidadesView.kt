package sysdeliz2.views.cadastros.entidade

import javafx.geometry.Pos
import javafx.geometry.Side
import javafx.scene.control.TabPane
import javafx.scene.layout.BorderPane
import sysdeliz2.controllers.cadastros.SceneCadastroEntidadeController
import sysdeliz2.views.helpers.ViewHelpers
import tornadofx.*

/**
 *
 * @author lima.joao
 * @since 21/01/2020 11:29
 */
class SceneCadEntidadesView: View("") {

    val controller: SceneCadastroEntidadeController by inject()

    init {
        controller.tabMainPane = tabpane {
            id = "tabMainPane"
            prefHeight = 200.0
            prefWidth = 200.0
            side = Side.BOTTOM
            tabClosingPolicy = TabPane.TabClosingPolicy.UNAVAILABLE

            BorderPane.setAlignment(this, Pos.CENTER)

            tab<TabEntidadeRegistros>{
                title = "Registros"
            }
            tab<TabEntidadeFormulario>{
                title = "Formulário"
            }
        }

    }

    val borderPane : BorderPane = borderpane {
        prefHeight = 1024.0
        prefWidth = 1920.0
        top = ViewHelpers.buildHBoxTopo(
                "/images/icons/cliente (4).png",
                "Entidades",
                ">Cadastros>Entidades"
        )
        center = controller.tabMainPane
        bottom = hbox {
            button("debug"){
                action{
                    LayoutDebugger.debug(scene.root.scene)
                }
            }
        }
    }
    override val root = borderPane
}
