package sysdeliz2.views.cadastros.entidade

import javafx.scene.layout.HBox
import sysdeliz2.controllers.cadastros.SceneCadastroEntidadeController
import tornadofx.*

/**
 *
 * @author lima.joao
 * @since 13/01/2020 11:47
 */
class TabEntidadeEnderecoEntrega : View("Entrega") {

    private val controller: SceneCadastroEntidadeController by inject()

    override val root = vbox(5) {
        prefWidth = 100.0
        prefHeight = 200.0
        padding = insets(5)

        form {

            field("CNPJ/CPF") {
                styleClass += "form-field"
                textfield(controller.model.cnpjEnt) {
                    styleClass += "input-group-apend"
                    prefWidth = 150.0
                    enableWhen(controller.inInsertOrEdit)
                }
                button("", imageview("/images/icons/buttons/find (1).png")) {
                    HBox.setMargin(this, insets(0, 0, 0, -11))
                    styleClass += "last"
                    styleClass += "warning"

                    enableWhen(controller.inInsertOrEdit)

                    action {
                        controller.buscaDadosEntregaReceitaWS()
                    }
                }
            }

            field("Inscrição") {
                styleClass += "form-field"
                textfield(controller.model.inscEnt) {
                    styleClass += "form-field"
                    prefWidth = 150.0
                    enableWhen(controller.inInsertOrEdit)
                }
            }

            field("CEP") {
                styleClass += "form-field"
                textfield(controller.model.cepEnt) {
                    styleClass += "input-group-apend"
                    prefWidth = 150.0
                    enableWhen(controller.inInsertOrEdit)
                }
                button("", imageview("/images/icons/buttons/find (1).png")) {
                    HBox.setMargin(this, insets(0, 0, 0, -11))
                    styleClass += "last"
                    styleClass += "warning"

                    enableWhen(controller.inInsertOrEdit)

                    action {
                        controller.buscaDadosEntregaReceitaWS()
                    }
                }
            }

            field("UF") {
                styleClass += "form-field"
//                textfield(controller.model.entrega.value.cidade!!..toProperty()) {
//                    styleClass += "form-field"
//                    prefWidth = 150.0
//                    isEditable = false
//                }
            }

            field("Cidade") {
                styleClass += "form-field"
//                textfield(controller.model.endereco.value.cep!!.cidade!!.nome.toProperty()) {
//                    styleClass += "form-field"
//                    prefWidth = 150.0
//                    isEditable = false
//                }
            }

            field("Bairro") {
                styleClass += "form-field"
                textfield(controller.model.cep.value.bairro.toProperty()) {
                    styleClass += "form-field"
                    prefWidth = 150.0
                    isEditable = false
                }
            }

            field("Endereço") {
                styleClass += "form-field"
                textfield(controller.model.logradouro) {
                    styleClass += "form-field"
                    prefWidth = 180.0
                }
            }

            field("Numero") {
                styleClass += "form-field"
                textfield(controller.model.numEnd) {
                    styleClass += "form-field"
                    prefWidth = 180.0
                }
            }

            field("Complemento") {
                styleClass += "form-field"
                textfield(controller.model.numEnd) {
                    styleClass += "form-field"
                    prefWidth = 180.0
                }
            }
        }
/*
                                    <TabPane>
        <Tab text="Entrega">

            <VBox prefHeight="200.0" prefWidth="100.0">
                <HBox layoutX="15.0" layoutY="15.0" prefHeight="56.0" prefWidth="1125.0">
                    <VBox layoutX="155.0" layoutY="10.0" prefHeight="80.0" prefWidth="61.0">
                        <Label styleClass="form-field" text="UF" />
                        <TextField fx:id="tboxUfEntrega" prefWidth="180.0" styleClass="form-field" />
                    </VBox>
                    <VBox prefHeight="80.0" prefWidth="151.0">
                        <Label styleClass="form-field" text="Cidade" />
                        <TextField fx:id="tboxCidadeEntrega" prefWidth="180.0" styleClass="form-field" />
                    </VBox>
                    <VBox layoutX="481.0" layoutY="10.0" prefHeight="80.0" prefWidth="132.0">
                        <Label styleClass="form-field" text="Bairro" />
                        <TextField fx:id="tboxBairroEntrega" prefWidth="180.0" styleClass="form-field" />
                    </VBox>
                    <VBox layoutX="311.0" layoutY="10.0" prefHeight="80.0" prefWidth="287.0">
                        <Label styleClass="form-field" text="Endereço" />
                        <TextField fx:id="tboxEnderecoEntrega" prefWidth="180.0" styleClass="form-field" />
                    </VBox>
                    <VBox layoutX="481.0" layoutY="10.0" prefHeight="80.0" prefWidth="67.0">
                        <Label styleClass="form-field" text="Numero" />
                        <TextField fx:id="tboxNumeroEntrega" prefWidth="180.0" styleClass="form-field" />
                    </VBox>
                    <VBox layoutX="773.0" layoutY="10.0" prefHeight="80.0" prefWidth="158.0">
                        <Label styleClass="form-field" text="Complemento" />
                        <TextField fx:id="tboxComplementoEntrega" prefWidth="180.0" styleClass="form-field" />
                    </VBox>
                </HBox>
            </VBox>
        </Tab>



                                        <Tab text="Cobrança">
                                            <VBox prefHeight="135.0" prefWidth="1130.0">
                                                <HBox prefHeight="56.0" prefWidth="1125.0">
                                                    <VBox.margin>
                                                        <Insets left="5.0" top="5.0" />
                                                    </VBox.margin>
                                                    <VBox prefHeight="56.0" prefWidth="218.0">
                                                        <Label styleClass="form-field" text="CNPJ/CPF" />
                                                        <HBox prefHeight="31.0" prefWidth="319.0">
                                                            <TextField fx:id="tboxCnpjCobranca" editable="false" prefWidth="252.0" styleClass="input-group-apend">
                                                                <padding>
                                                                    <Insets left="3.0" />
                                                                </padding>
                                                            </TextField>
                                                            <Button fx:id="btnFindCnpjCobranca" contentDisplay="GRAPHIC_ONLY" mnemonicParsing="false" onAction="#btnFindCnpjCobrancaOnAction" prefWidth="36.0" text="Pesquisar Marca Própria">
                                                                <graphic>
                                                                    <ImageView fitHeight="16.0" fitWidth="16.0" pickOnBounds="true" preserveRatio="true">
                                                                        <Image url="@../../../../images/icons/buttons/find%20(1).png" />
                                                                    </ImageView>
                                                                </graphic>
                                                                <styleClass>
                                                                    <String fx:value="last" />
                                                                    <String fx:value="warning" />
                                                                </styleClass>
                                                            </Button>
                                                        </HBox>
                                                    </VBox>
                                                    <VBox layoutX="155.0" layoutY="10.0" prefHeight="56.0" prefWidth="194.0">
                                                        <Label styleClass="form-field" text="Inscrição" />
                                                        <TextField fx:id="tboxInscricaoCobranca" prefWidth="180.0" styleClass="form-field" />
                                                        <HBox.margin>
                                                            <Insets left="5.0" />
                                                        </HBox.margin>
                                                    </VBox>
                                                </HBox>
                                                <HBox layoutX="15.0" layoutY="15.0" prefHeight="56.0" prefWidth="1125.0">
                                                    <VBox.margin>
                                                        <Insets left="5.0" />
                                                    </VBox.margin>
                                                    <VBox prefHeight="80.0" prefWidth="140.0">
                                                        <HBox.margin>
                                                            <Insets />
                                                        </HBox.margin>
                                                        <Label styleClass="form-field" text="CEP" />
                                                        <HBox prefHeight="31.0" prefWidth="319.0">
                                                            <TextField fx:id="tboxCepCobranca" editable="false" prefWidth="252.0" styleClass="input-group-apend">
                                                                <padding>
                                                                    <Insets left="3.0" />
                                                                </padding>
                                                            </TextField>
                                                            <Button fx:id="btnFindCepCobranca" contentDisplay="GRAPHIC_ONLY" mnemonicParsing="false" onAction="#btnFindCepCobrancaOnAction" prefWidth="36.0" text="Pesquisar">
                                                                <graphic>
                                                                    <ImageView fitHeight="16.0" fitWidth="16.0" pickOnBounds="true" preserveRatio="true">
                                                                        <Image url="@../../../../images/icons/buttons/find%20(1).png" />
                                                                    </ImageView>
                                                                </graphic>
                                                                <styleClass>
                                                                    <String fx:value="last" />
                                                                    <String fx:value="warning" />
                                                                </styleClass>
                                                            </Button>
                                                        </HBox>
                                                    </VBox>
                                                    <VBox layoutX="155.0" layoutY="10.0" prefHeight="80.0" prefWidth="61.0">
                                                        <HBox.margin>
                                                            <Insets left="5.0" />
                                                        </HBox.margin>
                                                        <Label styleClass="form-field" text="UF" />
                                                        <TextField fx:id="tboxUfCobranca" prefWidth="180.0" styleClass="form-field" />
                                                    </VBox>
                                                    <VBox prefHeight="80.0" prefWidth="151.0">
                                                        <HBox.margin>
                                                            <Insets left="5.0" />
                                                        </HBox.margin>
                                                        <Label styleClass="form-field" text="Cidade" />
                                                        <TextField fx:id="tboxCidadeCobranca" prefWidth="180.0" styleClass="form-field" />
                                                    </VBox>
                                                    <VBox layoutX="481.0" layoutY="10.0" prefHeight="80.0" prefWidth="132.0">
                                                        <HBox.margin>
                                                            <Insets left="5.0" />
                                                        </HBox.margin>
                                                        <Label styleClass="form-field" text="Bairro" />
                                                        <TextField fx:id="tboxBairroCobranca" prefWidth="180.0" styleClass="form-field" />
                                                    </VBox>
                                                    <VBox layoutX="311.0" layoutY="10.0" prefHeight="80.0" prefWidth="287.0">
                                                        <HBox.margin>
                                                            <Insets left="5.0" />
                                                        </HBox.margin>
                                                        <Label styleClass="form-field" text="Endereço" />
                                                        <TextField fx:id="tboxEnderecoCobranca" prefWidth="180.0" styleClass="form-field" />
                                                    </VBox>
                                                    <VBox layoutX="481.0" layoutY="10.0" prefHeight="80.0" prefWidth="67.0">
                                                        <HBox.margin>
                                                            <Insets left="5.0" />
                                                        </HBox.margin>
                                                        <Label styleClass="form-field" text="Numero" />
                                                        <TextField fx:id="tboxNumeroCobranca" prefWidth="180.0" styleClass="form-field" />
                                                    </VBox>
                                                    <VBox layoutX="773.0" layoutY="10.0" prefHeight="80.0" prefWidth="158.0">
                                                        <HBox.margin>
                                                            <Insets left="5.0" />
                                                        </HBox.margin>
                                                        <Label styleClass="form-field" text="Complemento" />
                                                        <TextField fx:id="tboxComplementoCobranca" prefWidth="180.0" styleClass="form-field" />
                                                    </VBox>
                                                </HBox>
                                            </VBox>
                                        </Tab>
                                    </TabPane>
                                </VBox>
                            </VBox>
                        </HBox>
                    </VBox>
                </VBox>
 */



    }
}