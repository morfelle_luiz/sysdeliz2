package sysdeliz2.views.cadastros.entidade

import javafx.geometry.Orientation
import javafx.scene.layout.HBox

import sysdeliz2.controllers.cadastros.SceneCadastroEntidadeController
import tornadofx.*

/**
 *
 * @author lima.joao
 * @since 13/01/2020 11:47
 */
class TabEntidadeEnderecoPrincipal : Fragment("Principal") {

    private val controller: SceneCadastroEntidadeController by inject()

    override val root = hbox(5) {
        prefWidth = 100.0
        prefHeight = 200.0

        form {
            fieldset("", null, Orientation.HORIZONTAL) {
                field("Pais") {
                    styleClass += "form-field"
                    textfield(controller.model.pais.value.nome.toProperty()) {
                        styleClass += "input-group-apend"
                        prefWidth = 150.0
                        isEditable = false
                    }
                    button("", imageview("/images/icons/buttons/find (1).png")) {
                        HBox.setMargin(this, insets(0, 0, 0, -11))
                        styleClass += "last"
                        styleClass += "warning"

                        enableWhen(controller.inInsertOrEdit)

                        action {
                            controller.findPais()
                        }
                    }
                }
            }
            fieldset("", null, Orientation.HORIZONTAL) {
                field("CEP") {
                    styleClass += "form-field"
                    textfield(controller.model.cep.value.cep.toProperty()) {
                        styleClass += "input-group-apend"
                        prefWidth = 150.0
                        isEditable = false
                    }
                    button("", imageview("/images/icons/buttons/find (1).png")) {
                        HBox.setMargin(this, insets(0, 0, 0, -11))
                        styleClass += "last"
                        styleClass += "warning"

                        enableWhen(controller.inInsertOrEdit)

                        action {
                            controller.findCep()
                        }
                    }
                }
            }
            field("UF") {
                styleClass += "form-field"
//                textfield(controller.model.endereco.value.cep!!.cidade!!.uf.toProperty()) {
//                    styleClass += "form-field"
//                    prefWidth = 150.0
//                    isEditable = false
//                }
            }

            field("Cidade") {
                styleClass += "form-field"
//                textfield(controller.model.endereco.value.cep!!.cidade!!.nome.toProperty()) {
//                    styleClass += "form-field"
//                    prefWidth = 150.0
//                    isEditable = false
//                }
            }

            field("Bairro") {
                styleClass += "form-field"
                textfield(controller.model.cep.value.bairro.toProperty()) {
                    styleClass += "form-field"
                    prefWidth = 150.0
                    isEditable = false
                }
            }

            field("Endereço") {
                styleClass += "form-field"
                textfield(controller.model.logradouro) {
                    styleClass += "form-field"
                    prefWidth = 180.0
                }
            }

            field("Numero") {
                styleClass += "form-field"
                textfield(controller.model.numEnd) {
                    styleClass += "form-field"
                    prefWidth = 180.0
                }
            }

            field("Complemento") {
                styleClass += "form-field"
                textfield(controller.model.numEnd) {
                    styleClass += "form-field"
                    prefWidth = 180.0
                }
            }
        }

    }
}