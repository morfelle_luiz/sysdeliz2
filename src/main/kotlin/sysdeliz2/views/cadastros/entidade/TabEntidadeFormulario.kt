package sysdeliz2.views.cadastros.entidade

import javafx.beans.property.Property
import javafx.geometry.Orientation
import javafx.geometry.Pos
import javafx.scene.control.ContentDisplay
import javafx.scene.control.TabPane
import javafx.scene.layout.HBox
import javafx.scene.layout.Priority
import sysdeliz2.controllers.cadastros.SceneCadastroEntidadeController
import tornadofx.*
import tornadofx.controlsfx.toggleswitch
import java.math.BigDecimal
import java.time.LocalDate

/**
 *
 * @author lima.joao
 * @since 13/01/2020 11:47
 */
class TabEntidadeFormulario : Fragment("Formulário") {

    private val controller: SceneCadastroEntidadeController by inject()

    override val root = vbox(5) {
        prefWidth = 100.0
        prefHeight = 200.0
        padding = insets(5)
        hbox {
            minHeight = 15.0
            prefHeight = 15.0
            prefWidth = 489.0
            styleClass += "sm"
            hbox(2, Pos.CENTER_LEFT) {
                prefHeight = 20.0
                prefWidth = 489.0
                hgrow = Priority.ALWAYS
                button("Primeiro") {
                    contentDisplay = ContentDisplay.GRAPHIC_ONLY
                    graphic = imageview("/images/icons/buttons/first (2).png") {
                        fitHeight = 24.0
                        fitWidth = 24.0
                        isPickOnBounds = true
                        isPreserveRatio = true
                    }
                    styleClass += "secundary"
                    styleClass += "sm"
                    padding = insets(0, 1.0, 0, 1.0)
                    action {
                        controller.firstRegister()
                    }
                    disableWhen(controller.inInsertOrEdit)
                }
                button("Anterior") {
                    contentDisplay = ContentDisplay.GRAPHIC_ONLY
                    graphic = imageview("/images/icons/buttons/previous (2).png") {
                        fitHeight = 24.0
                        fitWidth = 24.0
                        isPickOnBounds = true
                        isPreserveRatio = true
                    }
                    styleClass += "secundary"
                    styleClass += "sm"
                    padding = insets(0, 1.0, 0, 1.0)
                    action {
                        controller.previousRegister()
                    }
                    disableWhen(controller.inInsertOrEdit)
                }
                button("Próximo") {
                    contentDisplay = ContentDisplay.GRAPHIC_ONLY
                    graphic = imageview("/images/icons/buttons/next (2).png") {
                        fitHeight = 24.0
                        fitWidth = 24.0
                        isPickOnBounds = true
                        isPreserveRatio = true
                    }
                    styleClass += "secundary"
                    styleClass += "sm"
                    padding = insets(0, 1.0, 0, 1.0)
                    action {
                        controller.nextRegister()
                    }
                    disableWhen(controller.inInsertOrEdit)
                }
                button("Último") {
                    contentDisplay = ContentDisplay.GRAPHIC_ONLY
                    graphic = imageview("/images/icons/buttons/last (2).png") {
                        fitHeight = 24.0
                        fitWidth = 24.0
                        isPickOnBounds = true
                        isPreserveRatio = true
                    }
                    styleClass += "secundary"
                    styleClass += "sm"
                    padding = insets(0, 1.0, 0, 1.0)
                    action {
                        controller.lastRegister()
                    }

                    disableWhen(controller.inInsertOrEdit)
                }

                separator(Orientation.VERTICAL) {
                    prefHeight = 200.0
                }

                button("Adicionar Novo") {
                    layoutX = 146.0
                    layoutY = 10.0
                    styleClass += "success"

                    graphic = imageview("images/icons/buttons/new register (1).png") {
                        fitHeight = 16.0
                        fitWidth = 16.0
                        isPickOnBounds = true
                        isPreserveRatio = true
                    }

                    action {
                        //controller.nomeTextField.isEditable = true
                        controller.addRegister()
                    }
                }
                button("Editar") {
                    styleClass += "warning"

                    graphic = imageview("images/icons/buttons/edit register (1).png") {
                        fitHeight = 16.0
                        fitWidth = 16.0
                        isPickOnBounds = true
                        isPreserveRatio = true
                    }

                    action {
                        //controller.nomeTextField.isEditable = true
                        controller.editRegister()
                    }
                }
                button("Excluir") {
                    styleClass += "danger"

                    graphic = imageview("images/icons/buttons/delete register (1).png") {
                        fitHeight = 16.0
                        fitWidth = 16.0
                        isPickOnBounds = true
                        isPreserveRatio = true
                    }

                    action {
                        controller.deleteRegister()
                    }
                }

                separator(Orientation.VERTICAL) {
                    prefHeight = 200.0
                }

                button("Salvar") {
                    styleClass += "success"

                    graphic = imageview("images/icons/buttons/select (1).png") {
                        fitHeight = 16.0
                        fitWidth = 16.0
                        isPickOnBounds = true
                        isPreserveRatio = true
                    }

                    enableWhen(controller.model.dirty)

                    action {
                        controller.saveRegister()
                        //controller.nomeTextField.isEditable = false
                    }
                }
                button("Cancelar") {
                    styleClass += "danger"

                    graphic = imageview("images/icons/buttons/cancel (1).png") {
                        fitHeight = 16.0
                        fitWidth = 16.0
                        isPickOnBounds = true
                        isPreserveRatio = true
                    }

                    enableWhen(controller.inInsertOrEdit)

                    action {
                        controller.cancelRegister()
                    }

                }
            }
            hbox(2, Pos.CENTER_RIGHT) {
                prefHeight = 30.0
                prefWidth = 82.0
                button("Voltar") {
                    styleClass += "dark"
                    graphic = imageview("/images/icons/buttons/return (1).png") {
                        fitHeight = 16.0
                        fitWidth = 16.0
                        isPickOnBounds = true
                        isPreserveRatio = true
                    }
                    action {
                        controller.voltar()
                    }
                }
            }
        }

        separator { prefWidth = 200.0 }

        form {
            fieldset("Dados Gerais", null, Orientation.VERTICAL) {
                separator { prefWidth = 200.0 }
                hbox {
                    vbox {

                        hbox(5) {
                            field("Código") {
                                prefWidth = 64.0
                                textfield(controller.model.codCli) {
                                    styleClass += "form-field"
                                    styleClass += "no-editable"
                                    isEditable = false
                                }
                            }

                            field("CNPJ / CPF") {
                                textfield(controller.model.cnpj) {
                                    styleClass += "input-group-apend"
                                    prefWidth = 150.0
                                    enableWhen(controller.inInsertOrEdit)
                                }
                                button("", imageview("/images/icons/buttons/find (1).png")) {
                                    HBox.setMargin(this, insets(0, 0, 0, -11))
                                    styleClass += "last"
                                    styleClass += "warning"

                                    enableWhen(controller.inInsertOrEdit)

                                    action {
                                        controller.buscaDadosReceitaWS()
                                    }
                                }
                            }

                            field("Tipo") {
                                //styleClass += "form-field"
                                prefWidth = 64.0
                                textfield(controller.model.tipo) {
                                    styleClass += "form-field"
                                    enableWhen(controller.inInsertOrEdit)
                                }
                            }

                            field("Inscrição") {
                                //styleClass += "form-field"
                                prefWidth = 180.0
                                textfield(controller.model.inscricao) {
                                    styleClass += "form-field"
                                    enableWhen(controller.inInsertOrEdit)
                                }
                            }

                            field("Dt Cadastro") {
                                //styleClass += "form-field"
                                prefWidth = 180.0
                                datepicker(controller.model.dataCad as Property<LocalDate>) {
                                    styleClass += "form-field"
                                    enableWhen(controller.inInsertOrEdit)
                                }
                            }
                        }

                        hbox {
                            hgrow = Priority.ALWAYS
                            field("Razão Social") {
                                hgrow = Priority.ALWAYS
                                textfield(controller.model.nome) {
                                    styleClass += "form-field"
                                    hgrow = Priority.ALWAYS
                                    enableWhen(controller.inInsertOrEdit)
                                }
                            }
                        }

                        hbox {
                            hgrow = Priority.ALWAYS
                            field("Nome Fantasia") {
                                hgrow = Priority.ALWAYS
                                textfield(controller.model.fantasia) {
                                    styleClass += "form-field"
                                    hgrow = Priority.ALWAYS
                                    enableWhen(controller.inInsertOrEdit)
                                }
                            }
                        }

                    }

                    vbox {
                        padding = insets(left = 10.0)

                        field("Entidade") {
                            vbox {
                                style {
                                    borderColor += box(c("#CECECE"))
                                    borderRadius = multi(box(10.px))
                                }
                                padding = insets(5)
                                hbox {
                                    padding = insets(top = 5, left = -15)
                                    toggleswitch("", controller.model.isCliente) {
                                        enableWhen(controller.inInsertOrEdit)
                                    }
                                    label("Cliente") {
                                        padding = insets(left = 10.0)
                                        style {
                                            fontSize = 15.px
                                        }
                                    }
                                }
                                hbox {
                                    padding = insets(top = 5, left = -15)
                                    toggleswitch("", controller.model.isFornecedor) {
                                        enableWhen(controller.inInsertOrEdit)
                                    }
                                    label("Fornecedor") {
                                        padding = insets(left = 10.0)
                                        style {
                                            fontSize = 15.px
                                        }
                                    }
                                }
                                hbox {
                                    padding = insets(top = 5, left = -15)
                                    toggleswitch("", controller.model.isTerceiro) {
                                        enableWhen(controller.inInsertOrEdit)
                                    }
                                    label("Terceiro") {
                                        padding = insets(left = 10.0)
                                        style {
                                            fontSize = 15.px
                                        }
                                    }
                                }
                            }
                        }
                    }

                    vbox {
                        padding = insets(left = 10.0)

                        field("Status") {
                            styleClass += "form-field"

                            vbox {
                                style {
                                    borderColor += box(c("#CECECE"))
                                    borderRadius = multi(box(10.px))
                                }
                                padding = insets(5)

                                hbox {
                                    padding = insets(top = 5, left = -15)
                                    toggleswitch("", controller.model.isCliente) {

                                        enableWhen(controller.inInsertOrEdit)
                                    }
                                    label("Ativo") {
                                        padding = insets(left = 10.0)
                                        style {
                                            fontSize = 15.px
                                        }
                                    }
                                }

                                hbox {
                                    padding = insets(top = 5, left = -15)
                                    toggleswitch("", controller.model.isBloqueado) {
                                        enableWhen(controller.inInsertOrEdit)
                                    }
                                    label("Bloqueado") {
                                        padding = insets(left = 10.0)
                                        style {
                                            fontSize = 15.px
                                        }
                                    }
                                }

                                field("Motivo") {
                                    styleClass += "form-field"
//                                    textfield(controller.model.financeiro.value.motivo!!.descricao) {
//                                        styleClass += "input-group-apend"
//                                        prefWidth = 150.0
//
//                                        enableWhen(controller.inInsertOrEdit)
//                                    }
//                                    button("", imageview("/images/icons/buttons/find (1).png")) {
//                                        HBox.setMargin(this, insets(0, 0, 0, -11))
//                                        styleClass += "last"
//                                        styleClass += "warning"
//
//                                        enableWhen(controller.inInsertOrEdit)
//
//                                        action {
//                                            controller.findMotivo()
//                                        }
//                                    }
                                }

                            }
                        }
                    }

                }
            }

            fieldset("Contato", null, Orientation.VERTICAL) {
                separator { prefWidth = 200.0 }

                hbox(5) {
                    field("Telefone") {
                        prefWidth = 150.0
                        textfield(controller.model.telefone) {
                            styleClass += "form-field"
                            enableWhen(controller.inInsertOrEdit)
                        }
                    }

                    field("Celular") {
                        prefWidth = 150.0
                        textfield(controller.model.celular) {
                            styleClass += "form-field"
                            enableWhen(controller.inInsertOrEdit)
                        }
                    }

                    field("Email") {
                        textfield(controller.model.email) {
                            styleClass += "form-field"
                            enableWhen(controller.inInsertOrEdit)
                            prefWidth = 300.0
                        }
                    }
                }
            }

            fieldset("Financeiro", null, Orientation.VERTICAL) {
                separator { prefWidth = 200.0 }

                hbox(5) {
                    field("Ramo de Atividade") {
                        styleClass += "form-field"
                        textfield(controller.modelAtividade.descricao) {
                            styleClass += "input-group-apend"
                            prefWidth = 150.0
                            isDisable = true
                        }
                        button("", imageview("/images/icons/buttons/find (1).png")) {
                            HBox.setMargin(this, insets(0, 0, 0, -11))
                            styleClass += "last"
                            styleClass += "warning"

                            enableWhen(controller.inInsertOrEdit)

                            action {
                                controller.findAtividade()
                            }
                        }

                    }

                    field("Situação do Cliente") {
                        styleClass += "form-field"
                        textfield(controller.modelSitCli.descricao) {
                            styleClass += "input-group-apend"
                            prefWidth = 150.0
                            isDisable = true
                        }

                        button("", imageview("/images/icons/buttons/find (1).png")) {
                            HBox.setMargin(this, insets(0, 0, 0, -11))
                            styleClass += "last"
                            styleClass += "warning"

                            enableWhen(controller.inInsertOrEdit)

                            action {
                                controller.findSituacao()
                            }
                        }
                    }

                    field("Grupo") {
                        styleClass += "form-field"
                        textfield(controller.modelGrupoCli.descricao) {
                            styleClass += "input-group-apend"
                            prefWidth = 150.0
                            isDisable = true
                        }

                        button("", imageview("/images/icons/buttons/find (1).png")) {
                            HBox.setMargin(this, insets(0, 0, 0, -11))
                            styleClass += "last"
                            styleClass += "warning"

                            enableWhen(controller.inInsertOrEdit)

                            action {
                                controller.findGrupo()
                            }
                        }
                    }

                    field("Limite de Crédito") {
                        styleClass += "form-field"

                        textfield(controller.model.credito as Property<Number>) {
                            styleClass += "form-field"
                            prefWidth = 150.0

                            enableWhen(controller.inInsertOrEdit)
                        }
                    }
                }
            }

            fieldset("Endereços", null, Orientation.VERTICAL) {
                separator { prefWidth = 200.0 }
                tabpane {
                    id="tab_endereco"
                    prefHeight = 155.0
                    prefWidth = 1130.0
                    tabClosingPolicy = TabPane.TabClosingPolicy.UNAVAILABLE

                    tab<TabEntidadeEnderecoPrincipal>()
                    tab<TabEntidadeEnderecoEntrega>()
                    tab<TabEntidadeEnderecoCobranca>()
                }
            }
        }

    }
}