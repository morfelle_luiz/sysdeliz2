package sysdeliz2.views.cadastros.entidade

import javafx.geometry.Orientation
import javafx.scene.control.ContentDisplay
import javafx.scene.control.TextFormatter
import javafx.scene.layout.Priority
import javafx.scene.layout.VBox
import sysdeliz2.controllers.cadastros.SceneCadastroEntidadeController
import sysdeliz2.models.ti.AtividadeBean
import sysdeliz2.models.ti.GrupoCliBean
import sysdeliz2.models.ti.SitCliBean
import sysdeliz2.models.ti.entidade.EntidadeBean
import sysdeliz2.utils.extensions.toFormatedDate
import tornadofx.*
import tornadofx.controlsfx.toggleswitch


/**
 *
 * @author lima.joao
 * @since 13/01/2020 11:47
 */
class TabEntidadeRegistros() : View("Registros") {

    private val controller: SceneCadastroEntidadeController by inject()
    private val filtro = titledpane {
        prefWidth = 1920.0
        isAnimated = true
        isCollapsible = true
        text = "Filtros"

        paddingAll = 5.0

        form {
            padding = insets(5.0)
            hbox(10) {
                prefHeight = 60.0
                minHeight = 60.0
                maxHeight = 60.0
                prefWidth = 100.0

                fieldset("", null, Orientation.VERTICAL) {
                    field("Código") {
                        textfield(controller.inputFiltroCodigo) {
                            prefWidth = 50.0
                            promptText = "Código"
                            styleClass += "sm"
                            styleClass += "first"
                        }
                    }
                }

                fieldset("", null, Orientation.VERTICAL) {
                    field("Razão Social") {
                        textfield(controller.inputFiltroRazaoSocial) {
                            prefWidth = 327.0
                            promptText = "Buscar por Razão Social"
                            styleClass += "sm"
                            styleClass += "first"
                        }
                    }
                }

                fieldset("", null, Orientation.VERTICAL) {
                    field("Cidade") {
                        textfield(controller.inputFiltroCidade) {
                            prefWidth = 150.0
                            promptText = "Cidade"
                            styleClass += "sm"
                            styleClass += "first"

                            textFormatter = TextFormatter<Any> {
                                it.text = it.text.toUpperCase()
                                it
                            }
                        }
                    }
                }

                fieldset("", null, Orientation.VERTICAL) {
                    field("UF") {
                        textfield(controller.inputFiltroEstado) {
                            prefWidth = 50.0
                            promptText = "UF"
                            styleClass += "sm"
                            styleClass += "first"

                            textFormatter = TextFormatter<Any> {
                                it.text = it.text.toUpperCase()
                                it
                            }
                        }
                    }
                }

//                    fieldset("", null, Orientation.VERTICAL) {
//                        field("Cidade") {
//                            //combobox(controller.inputTipoPedido, listOf("Todos", "Integrados", "Não Integrados")).selectionModel.selectFirst()
//                        }
//                    }

                fieldset("", null, Orientation.VERTICAL) {
                    hbox(10) {
                        vbox {
                            //checkbox("Cancelados", controller.inputStatusCanceled)
                            //checkbox("Entregues", controller.inputStatusEntregue)
                        }
                        vbox {
                            //checkbox("Aguardando Pagto", controller.inputStatusPending)
                            //checkbox("Pagto Processado", controller.inputStatusProcessing)
                        }
                        vbox {
                            //checkbox("Em Transporte", controller.inputStatusComplete)
                        }
                    }
                }

                fieldset("", null, Orientation.VERTICAL) {
                    field("Entidade") {
                        hbox {
                            style {
                                borderColor += box(c("#CECECE"))
                                borderRadius = multi(box(10.px))
                            }
                            padding = insets(5)
                            hbox {
                                padding = insets(top = 5, left = -15)
                                toggleswitch("", controller.inputFiltroTipoEntidadeCliente)
                                label("Cliente") {
                                    padding = insets(left = 10.0)
                                    style {
                                        fontSize = 15.px
                                    }
                                }
                            }
                            hbox {
                                padding = insets(top = 5, left = -15)
                                toggleswitch("", controller.inputFiltroTipoEntidadeFornecedor)
                                label("Fornecedor") {
                                    padding = insets(left = 10.0)
                                    style {
                                        fontSize = 15.px
                                    }
                                }
                            }
                            hbox {
                                padding = insets(top = 5, left = -15)
                                toggleswitch("", controller.inputFiltroTipoEntidadeTerceiro)
                                label("Terceiro") {
                                    padding = insets(left = 10.0)
                                    style {
                                        fontSize = 15.px
                                    }
                                }
                            }
                        }
                    }
                }

                fieldset("", null, Orientation.VERTICAL) {
                    field("Status") {
                        hbox {
                            style {
                                borderColor += box(c("#CECECE"))
                                borderRadius = multi(box(10.px))
                            }
                            padding = insets(5)
                            hbox {
                                padding = insets(top = 5, left = -15)
                                toggleswitch("", controller.inputFiltroAtivos)
                                label("Ativos") {
                                    padding = insets(left = 10.0)
                                    style {
                                        fontSize = 15.px
                                    }
                                }
                            }
                            hbox {
                                padding = insets(top = 5, left = -15)
                                toggleswitch("", controller.inputFiltroBloqueados)
                                label("Bloqueados") {
                                    padding = insets(left = 10.0)
                                    style {
                                        fontSize = 15.px
                                    }
                                }
                            }
                        }
                    }
                }

                hbox {
                    paddingAll = 15.0
                    button("Filtrar", imageview("/images/icons/buttons/find (1).png")) {
                        style {
                            fontSize = 18.px
                            //styleClass += "sm"
                            //styleClass += "last"
                            styleClass += "success"
                        }
                        action {
                            root.runAsyncWithOverlay {
                                controller.findWithFilter()
                            }
                        }
                    }
                }
            }
        }
    }

    private val panel = vbox(5) {
        prefHeight = 200.0
        prefWidth = 100.0
        paddingAll = 5

        add(filtro)

        vbox {
            prefHeight = 200.0
            prefWidth = 100.0
            vgrow = Priority.ALWAYS
            hbox {
                prefHeight = 975.0
                prefWidth = 1920.0
                vbox {
                    prefHeight = 975.0
                    prefWidth = 1920.0
                    controller.tableViewRegistros = tableview(controller.lstRegistros) {
                        isEditable = true
                        prefHeight = 975.0
                        prefWidth = 1920.0
                        column("Ações", EntidadeBean::codCli) {
                            isEditable = false
                            prefWidth = 108.0
                            isResizable = false
                            isSortable = false

                            cellFormat { cell ->
                                graphic = hbox(3) {
                                    cell?.let {
                                        button("Informações do Registro") {
                                            graphic = imageview("/images/icons/buttons/find (1).png")
                                            tooltip = tooltip("Visualizar informações")
                                            styleClass += "info"
                                            styleClass += "xs"
                                            contentDisplay = ContentDisplay.GRAPHIC_ONLY
                                        }
                                        button("Editar registro selecionado") {
                                            graphic = imageview("/images/icons/buttons/edit register (1).png")
                                            tooltip = tooltip("Alteração do Registro")
                                            styleClass += "warning"
                                            styleClass += "xs"
                                            contentDisplay = ContentDisplay.GRAPHIC_ONLY
                                        }
                                        button("Exclusão do Registro") {
                                            graphic = imageview("/images/icons/buttons/delete register (1).png")
                                            tooltip = tooltip("Excluir registro selecionado")
                                            styleClass += "danger"
                                            styleClass += "xs"
                                            contentDisplay = ContentDisplay.GRAPHIC_ONLY
                                        }
                                    }
                                }
                            }
                        }
                        column("Código", EntidadeBean::codCli) {
                            isEditable = false
                            prefWidth = 50.0
                        }
                        column("Razão Social", EntidadeBean::nome) {
                            isEditable = false
                            prefWidth = 317.0
                        }
                        column("Telefone", EntidadeBean::telefone) { prefWidth = 111.0 }
                        column("Celular", EntidadeBean::celular) { prefWidth = 111.0 }
                        column("Ult. Pedido", EntidadeBean::dtUltimoFat) {
                            prefWidth = 80.0
                            cellFormat {
                                text = it?.toFormatedDate() ?: " "
                            }
                        }
                        column("Cadastro", EntidadeBean::dataCad) {
                            prefWidth = 70.0
                            cellFormat {
                                text = " "
                                it?.let { data ->
                                    text = data.toFormatedDate()
                                }
                            }
                        }
                        nestedColumn("Endereço") {
                            column("Cód Cid", EntidadeBean::cep) {
                                prefWidth = 64.0
                                cellFormat {
                                    text = it.cidade?.codigo ?: " "
                                }
                            }
                            column("Nome Cidade", EntidadeBean::cep) {
                                prefWidth = 166.0
                                cellFormat {
                                    text = it.cidade?.nome ?: " "
                                }
                            }
                            column("UF", EntidadeBean::cep) {
                                prefWidth = 26.0
                                cellFormat {
                                    text = it.cidade?.siglaUf ?: " "
                                }
                            }
                        }
                        column("Tipo Entidade", EntidadeBean::tipoEntidade) { prefWidth = 68.0 }
                        column("Tipo Cliente", EntidadeBean::tipo) { prefWidth = 77.0 }
//                        nestedColumn("Motivo") {
//                            column("Cód.", EntidadeBean::motivo) {
//                                prefWidth = 25.0
//                                cellFormat {
//                                    //text = it?.codMen ?: ""
//                                }
//                            }
//                            column("Descrição", EntidadeBean::motivo) {
//                                prefWidth = 210.0
//                                cellFormat {
//                                    //text = it?.descricao ?: ""
//                                }
//                            }
//                        }
                        nestedColumn("Classe") {
                            column("Cód", EntidadeBean::classe) {
                                prefWidth = 38.0
                                cellFormat {
                                    text = it.codigo ?: ""
                                }
                            }
                            column("Descrição", EntidadeBean::classe) {
                                prefWidth = 210.0
                                cellFormat {
                                    text = it.descricao ?: ""
                                }
                            }
                        }
                        column("Ativo", EntidadeBean::ativo) {
                            prefWidth = 36.0
                            cellFormat {
                                graphic = if (it == "S") {
                                    imageview("/images/icons/buttons/active (2).png", false)
                                } else {
                                    imageview("/images/icons/buttons/inactive (2).png", false)
                                }
                            }
                        }
                        column("Bloq.", EntidadeBean::bloqueio) {
                            prefWidth = 44.0
                            cellFormat {
                                graphic = if ((it ?: "N") == "S") {
                                    imageview("/images/icons/buttons/active (2).png", false)
                                } else {
                                    imageview("/images/icons/buttons/inactive (2).png", false)
                                }
                            }
                        }

                        controller.model.rebindOnChange(this) { selected ->
                            item = selected ?: EntidadeBean()
                            controller.modelAtividade.item = selected?.atividade ?: AtividadeBean()
                            controller.modelSitCli.item = selected?.sitCli ?: SitCliBean()
                            controller.modelGrupoCli.item = selected?.grupo ?: GrupoCliBean()
                        }
                    }

                    label(controller.recordCount) {
                        //text = "Mostrando X registros"
                        maxWidth = 1.7976931348623157E308
                        textFill = c("#655959")
                    }
                }
            }
        }
    }


    override val root: VBox = panel
}