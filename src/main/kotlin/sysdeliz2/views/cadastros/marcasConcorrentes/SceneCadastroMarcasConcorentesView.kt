package sysdeliz2.views.cadastros.marcasConcorrentes

import javafx.geometry.Pos
import javafx.geometry.Side
import javafx.scene.control.TabPane
import javafx.scene.layout.BorderPane
import sysdeliz2.controllers.cadastros.SceneCadastroMarcasConcorrentesController
import sysdeliz2.views.helpers.ViewHelpers
import tornadofx.*

/**
 *
 * @author lima.joao
 * @since 13/01/2020 11:15
 */
class SceneCadastroMarcasConcorentesView: View(""){


    val controller: SceneCadastroMarcasConcorrentesController by inject()

    init {
        controller.tabMainPane = tabpane{
            id = "tabMainPane"
            prefHeight = 200.0
            prefWidth = 200.0
            side = Side.BOTTOM
            tabClosingPolicy = TabPane.TabClosingPolicy.UNAVAILABLE

            BorderPane.setAlignment(this, Pos.CENTER)

            tab<TabCadastroMarcasConcorrentesRegistros>{
                title = "Registros"
            }
            tab<TabCadastroMarcasConcorrentesFormulario>{
                title = "Formulário"
            }
        }
    }

    val borderPane : BorderPane = borderpane {
        prefHeight = 1024.0
        prefWidth = 1920.0
        top = ViewHelpers.buildHBoxTopo(
                "/images/icons/representante (4).png",
                "Marcas Concorrentes",
                ">Cadastros>Representantes"
        )
        center = controller.tabMainPane
        bottom = hbox {
            button("debug"){
                action{
                    LayoutDebugger.debug(scene.root.scene)
                }
            }
        }
    }

    override val root = borderPane
}