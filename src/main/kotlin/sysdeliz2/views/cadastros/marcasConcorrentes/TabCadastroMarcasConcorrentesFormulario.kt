package sysdeliz2.views.cadastros.marcasConcorrentes

import javafx.geometry.Orientation
import javafx.geometry.Pos
import javafx.scene.control.ContentDisplay
import javafx.scene.layout.HBox
import javafx.scene.layout.Priority
import sysdeliz2.controllers.cadastros.SceneCadastroMarcasConcorrentesController
import tornadofx.*

/**
 *
 * @author lima.joao
 * @since 13/01/2020 11:47
 */
class TabCadastroMarcasConcorrentesFormulario : View("Formulário"){

    private val controller: SceneCadastroMarcasConcorrentesController by inject()

    override val root = vbox(5) {
        prefWidth = 100.0
        prefHeight = 200.0
        padding = insets(5)
        hbox {
            minHeight = 15.0
            prefHeight = 15.0
            prefWidth = 489.0
            styleClass += "sm"
            hbox(2, Pos.CENTER_LEFT){
                prefHeight = 20.0
                prefWidth = 489.0
                hgrow = Priority.ALWAYS
                button("Primeiro") {
                    contentDisplay = ContentDisplay.GRAPHIC_ONLY
                    graphic = imageview("/images/icons/buttons/first (2).png") {
                        fitHeight = 24.0
                        fitWidth = 24.0
                        isPickOnBounds = true
                        isPreserveRatio = true
                    }
                    styleClass += "secundary"
                    styleClass += "sm"
                    padding = insets(0,1.0,0,1.0)
                    action {
                       controller.firstRegister()
                    }
                    disableWhen(controller.model.dirty)
                }
                button("Anterior") {
                    contentDisplay = ContentDisplay.GRAPHIC_ONLY
                    graphic = imageview("/images/icons/buttons/previous (2).png") {
                        fitHeight = 24.0
                        fitWidth = 24.0
                        isPickOnBounds = true
                        isPreserveRatio = true
                    }
                    styleClass += "secundary"
                    styleClass += "sm"
                    padding = insets(0,1.0,0,1.0)
                    action {
                        controller.previousRegister()
                    }
                    disableWhen(controller.model.dirty)
                }
                button("Próximo") {
                    contentDisplay = ContentDisplay.GRAPHIC_ONLY
                    graphic = imageview("/images/icons/buttons/next (2).png") {
                        fitHeight = 24.0
                        fitWidth = 24.0
                        isPickOnBounds = true
                        isPreserveRatio = true
                    }
                    styleClass += "secundary"
                    styleClass += "sm"
                    padding = insets(0,1.0,0,1.0)
                    action {
                        controller.nextRegister()
                    }
                    disableWhen(controller.model.dirty)
                }
                button("Último") {
                    contentDisplay = ContentDisplay.GRAPHIC_ONLY
                    graphic = imageview("/images/icons/buttons/last (2).png") {
                        fitHeight = 24.0
                        fitWidth = 24.0
                        isPickOnBounds = true
                        isPreserveRatio = true
                    }
                    styleClass += "secundary"
                    styleClass += "sm"
                    padding = insets(0,1.0,0,1.0)
                    action {
                        controller.lastRegister()
                    }

                    disableWhen(controller.model.dirty)
                }

                separator(Orientation.VERTICAL) {
                    prefHeight = 200.0
                }

                button("Adicionar Novo"){
                    layoutX = 146.0
                    layoutY = 10.0
                    styleClass += "success"

                    graphic = imageview("images/icons/buttons/new register (1).png") {
                        fitHeight = 16.0
                        fitWidth = 16.0
                        isPickOnBounds = true
                        isPreserveRatio = true
                    }

                    action {
                        controller.nomeTextField.isEditable = true
                        controller.addRegister2()
                    }
                }
                button("Editar"){
                    styleClass += "warning"

                    graphic = imageview("images/icons/buttons/edit register (1).png") {
                        fitHeight = 16.0
                        fitWidth = 16.0
                        isPickOnBounds = true
                        isPreserveRatio = true
                    }

                    action {
                        controller.nomeTextField.isEditable = true
                        controller.editRegister()
                    }
                }
                button("Excluir"){
                    styleClass += "danger"

                    graphic = imageview("images/icons/buttons/delete register (1).png") {
                        fitHeight = 16.0
                        fitWidth = 16.0
                        isPickOnBounds = true
                        isPreserveRatio = true
                    }

                    action {
                        controller.deleteRegister()
                    }
                }

                separator(Orientation.VERTICAL) {
                    prefHeight = 200.0
                }

                button("Salvar"){
                    styleClass += "success"

                    graphic = imageview("images/icons/buttons/select (1).png") {
                        fitHeight = 16.0
                        fitWidth = 16.0
                        isPickOnBounds = true
                        isPreserveRatio = true
                    }

                    action {
                        controller.saveRegister()
                        controller.nomeTextField.isEditable = false
                    }
                }
                button("Cancelar"){
                    styleClass += "danger"

                    graphic = imageview("images/icons/buttons/cancel (1).png") {
                        fitHeight = 16.0
                        fitWidth = 16.0
                        isPickOnBounds = true
                        isPreserveRatio = true
                    }

                    action {
                        controller.cancelRegister()
                        controller.nomeTextField.isEditable = false
                    }
                }
            }
            hbox(2, Pos.CENTER_RIGHT) {
                prefHeight = 30.0
                prefWidth = 82.0
                button("Voltar") {
                    styleClass += "dark"
                    graphic = imageview("/images/icons/buttons/return (1).png"){
                        fitHeight = 16.0
                        fitWidth = 16.0
                        isPickOnBounds = true
                        isPreserveRatio = true
                    }
                    action {
                        controller.voltar()
                    }
                }
            }
        }

        separator { prefWidth = 200.0 }

        form {
            fieldset("Dados Gerais", null, Orientation.VERTICAL){
                separator {  }
                hbox(5) {
                    field("Código"){
                        styleClass += "form-field"
                        textfield(controller.model.codigo){
                            styleClass += "form-field"
                            styleClass += "no-editable"
                            isEditable = false
                            prefWidth = 64.0
                        }
                    }
                    field("Nome") {
                        styleClass += "form-field"
                        controller.nomeTextField = textfield(controller.model.nome) {
                            styleClass += "form-field"
                            prefWidth = 333.0
                            isEditable = false
                        }
                    }
                    field("Marca Própria") {
                        //styleClass += "form-field"
                        textfield(controller.inputMarcaPropria){
                            styleClass += "input-group-apend"
                            prefWidth = 191.0
                            isEditable = false
                        }
                        button("", imageview("/images/icons/buttons/find (1).png")) {
                            HBox.setMargin(this, insets(0, 0, 0, -11))
                            styleClass += "last"
                            styleClass += "warning"
                        }
                    }
                }

            }
        }

//        vbox {
//            hbox {
//                vbox{
//                    vbox {
//                        hbox {
//                            vbox {
//                                prefHeight = 288.0
//                                prefWidth = 548.0
//
//                                label("Nome"){
//                                    styleClass += "form-field"
//                                }
//                                textfield(controller.model.nome){
//                                    prefWidth = 333.0
//                                    styleClass += "form-field"
//                                }
//                            }
//                            vbox {
//                                label("Marca Própria"){
//                                    styleClass += "form-field"
//                                }
//                                hbox {
//                                    prefHeight = 100.0
//                                    prefWidth = 172.0
//                                    textfield(controller.inputMarcaPropria) {
//                                        isEditable = false
//                                        prefWidth = 143.0
//                                        styleClass += "input-group-apend"
//                                        padding = insets(0,0,0,3)
//                                    }
//                                    button("Pesquisar Marca Própria"){
//                                        contentDisplay = ContentDisplay.GRAPHIC_ONLY
//                                        prefWidth = 36.0
//                                        graphic = imageview("/images/icons/buttons/find (1).png"){
//                                            fitHeight = 16.0
//                                            fitWidth = 16.0
//                                            isPickOnBounds = true
//                                            isPreserveRatio = true
//                                        }
//                                        styleClass += "last"
//                                        styleClass += "warning"
//                                        action {
//                                            controller.consultaMarcaPropria()
//                                        }
//                                    }
//                                }
//                            }
//                            vbox(0, Pos.TOP_RIGHT) {
//                                prefHeight = 55.0
//                                prefWidth = 43.0
//                            }
//                            vbox {
//                                prefHeight = 200.0
//                                prefWidth = 100.0
//                                hbox(0, Pos.TOP_RIGHT) {
//                                    padding = insets(20,0,0,0)
//
//                                    val toggle = ToggleSwitch()
//                                    toggle.prefHeight = 35.0
//                                    toggle.prefWidth = 33.0
//
//                                    add(toggle)
//
//                                    label("Ativo"){
//                                        prefHeight = 17.0
//                                        prefWidth = 79.0
//                                        padding = insets(0,0,0,10)
//                                    }
//
//
//                                }
//                                hbox {
//                                    prefHeight = 55.0
//                                    prefWidth = 548.0
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
    }
}