package sysdeliz2.views.cadastros.marcasConcorrentes

import javafx.geometry.Pos
import javafx.scene.control.ContentDisplay
import javafx.scene.layout.Priority
import sysdeliz2.controllers.cadastros.SceneCadastroMarcasConcorrentesController
import sysdeliz2.models.sysdeliz.SdMarcaConcorrenteBean
import tornadofx.*


/**
 *
 * @author lima.joao
 * @since 13/01/2020 11:47
 */
class TabCadastroMarcasConcorrentesRegistros() : View("Registros") {

    private val controller: SceneCadastroMarcasConcorrentesController by inject()


    override val root = vbox(5) {
        prefHeight = 200.0
        prefWidth = 100.0
        paddingAll = 5
        hbox {
            prefHeight = 0.0
            prefWidth = 689.0
            hbox(0, Pos.CENTER_LEFT) {
                maxHeight = Double.NEGATIVE_INFINITY
                maxWidth = Double.NEGATIVE_INFINITY
                prefHeight = 30.0
                prefWidth = 315.0
                hgrow = Priority.ALWAYS
                button("Adicionar Novo") {
                    prefWidth = 131.0
                    styleClass += "sm"
                    styleClass += "success"

                    graphic = imageview("/images/icons/buttons/new register (1).png")

                    action {
                        controller.addRegister()
                    }
                }
            }

            hbox(0, Pos.CENTER_LEFT) {
                prefHeight = 30.0
                prefWidth = 566.0
                hgrow = Priority.ALWAYS
                combobox(controller.inputFiltroAtivo, listOf("Todos", "Ativos", "Inativos")) {
                    prefWidth = 150.0
                }.selectionModel.selectFirst()

                textfield(controller.inputFiltroDefault) {
                    prefWidth = 327.0
                    promptText = "Buscar por Nome ou Marca própria"
                    styleClass += "sm"
                    styleClass += "first"
                }

                button("Buscar Função") {
                    contentDisplay = ContentDisplay.GRAPHIC_ONLY
                    isMnemonicParsing = false
                    graphic = imageview("/images/icons/buttons/find (1).png") {
                        fitHeight = 16.0
                        fitWidth = 16.0
                        isPickOnBounds = true
                        isPreserveRatio = true

                        styleClass += "sm"
                        styleClass += "last"
                        styleClass += "dark"
                    }
                    action {
                        controller.findDefault()
                        controller.tableViewRegistros.refresh()
                    }
                }
            }
        }
        vbox {
            prefHeight = 200.0
            prefWidth = 100.0
            vgrow = Priority.ALWAYS
            hbox {
                prefHeight = 975.0
                prefWidth = 1910.0
                vbox {
                    prefHeight = 657.0
                    prefWidth = 817.0

                    controller.tableViewRegistros = tableview(controller.lstRegistros) {
                        isEditable = true
                        prefHeight = 641.0
                        prefWidth = 988.0
                        column("Ações", SdMarcaConcorrenteBean::codigo) {
                            isEditable = false
                            prefWidth = 108.0
                            isResizable = false
                            isSortable = false

                            cellFormat {cell ->
                                graphic = hbox(3) {
                                    cell?.let{
                                        button("Informações do Registro") {
                                            graphic = imageview("/images/icons/buttons/find (1).png")
                                            tooltip = tooltip("Visualizar informações")
                                            styleClass += "info"
                                            styleClass += "xs"
                                            contentDisplay = ContentDisplay.GRAPHIC_ONLY
                                        }
                                        button("Editar registro selecionado") {
                                            graphic = imageview("/images/icons/buttons/edit register (1).png")
                                            tooltip = tooltip("Alteração do Registro")
                                            styleClass += "warning"
                                            styleClass += "xs"
                                            contentDisplay = ContentDisplay.GRAPHIC_ONLY
                                        }
                                        button("Exclusão do Registro") {
                                            graphic = imageview("/images/icons/buttons/delete register (1).png")
                                            tooltip = tooltip("Excluir registro selecionado")
                                            styleClass += "danger"
                                            styleClass += "xs"
                                            contentDisplay = ContentDisplay.GRAPHIC_ONLY
                                        }
                                    }
                                }
                            }
                        }
                        column("Código", SdMarcaConcorrenteBean::codigo) {
                            isEditable = false
                            prefWidth = 50.0
                        }
                        column("Nome", SdMarcaConcorrenteBean::nome) {
                            isEditable = false
                            prefWidth = 300.0
                        }
                        column("Nossa Marca", SdMarcaConcorrenteBean::marca){
                            isEditable = false
                            prefWidth = 342.0

//                            cellFormat {
//                                text = it?.descricao ?: ""
//                            }
                        }

                        controller.model.rebindOnChange(this) { selected ->
                            item = selected ?: SdMarcaConcorrenteBean()
                            controller.inputMarcaPropria.set(item.marca?.descricao ?: "")
                        }
                    }

                    label(controller.recordCount){
                        //text = "Mostrando X registros"
                        maxWidth = 1.7976931348623157E308
                        textFill = c("#655959")
                    }
                }
            }
        }
    }
}