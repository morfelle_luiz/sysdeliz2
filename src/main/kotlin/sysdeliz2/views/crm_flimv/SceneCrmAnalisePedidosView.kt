package sysdeliz2.views.crm_flimv

import br.com.validators.simple.enums.Severity
import br.com.validators.simple.message.SimpleMessage
import br.com.validators.simple.validator.CustomValidator
import javafx.geometry.Orientation
import javafx.scene.layout.BorderPane
import javafx.scene.layout.HBox
import javafx.scene.layout.Priority
import javafx.scene.paint.Color
import javafx.stage.FileChooser
import sysdeliz2.controllers.crm_flimv.CrmAnalisePedidosController
import sysdeliz2.models.ti.Colecao
import sysdeliz2.models.view.VSdCrmPedidosMarcaColecao
import sysdeliz2.utils.GUIUtils
import sysdeliz2.utils.enums.ResultTypeFilter
import sysdeliz2.utils.gui.window.GenericFilter
import sysdeliz2.utils.ExportExcel
import sysdeliz2.utils.sys.LocalLogger
import sysdeliz2.views.helpers.ViewHelpers
import tornadofx.*
import java.sql.SQLException
import java.time.LocalDate


/**
 *
 * @author lima.joao
 * @since 28/11/2019 15:38
 */
class SceneCrmAnalisePedidosView : View("") {

    private val controller: CrmAnalisePedidosController by inject()

    private val tableRegistros = tableview(controller.registros) {
        vgrow = Priority.ALWAYS
        hgrow = Priority.ALWAYS
        id = "table_analise_pedidos"

        column("Pedido", VSdCrmPedidosMarcaColecao::numero)
        column("Emissão", VSdCrmPedidosMarcaColecao::dtEmissao)
        nestedColumn("Cliente") {
            column("Código", VSdCrmPedidosMarcaColecao::codcli)
            column("Nome", VSdCrmPedidosMarcaColecao::nome).remainingWidth()
        }
        column("Marca", VSdCrmPedidosMarcaColecao::marca)
        column("Coleção", VSdCrmPedidosMarcaColecao::colecao)
        nestedColumn("Pendentes") {
            column("Qtd", VSdCrmPedidosMarcaColecao::qtd) {
                prefWidth = 70.0
                cellFormat {
                    text = it.toString()
                    style {
                        if (it.toString().toInt() > 0) {
                            backgroundColor += c("#fab552")
                            textFill = Color.BLACK
                        }
                    }
                }
            }
            column("Valor", VSdCrmPedidosMarcaColecao::valorPendente) {
                cellFormat {
                    text = it.toString()
                    style {
                        if (it.toString().toFloat() > 0) {
                            backgroundColor += c("#fab552")
                            textFill = Color.BLACK
                        }
                    }
                }
            }
        }
        nestedColumn("Faturados") {
            column("Qtd", VSdCrmPedidosMarcaColecao::qtdFaturada) {
                prefWidth = 70.0
                cellFormat {
                    text = it.toString()
                    style {
                        if (it.toString().toInt() > 0) {
                            backgroundColor += c("#81cf81")
                            textFill = Color.BLACK
                        }
                    }
                }
            }
            column("Valor", VSdCrmPedidosMarcaColecao::valorFaturado) {
                cellFormat {
                    text = it.toString()
                    style {
                        if (it.toString().toFloat() > 0) {
                            backgroundColor += c("#81cf81")
                            textFill = Color.BLACK
                        }
                    }
                }
            }
            //c("#69bad2")
        }
        nestedColumn("Pendente + Faturado") {
            column("Qtd", VSdCrmPedidosMarcaColecao::qtdTotal) {
                cellFormat {
                    text = it.toString()
                    style {
                        if (it.toString().toInt() > 0) {
                            backgroundColor += c("#69bad2")
                            textFill = Color.BLACK
                        }
                    }
                }
            }
            column("Valor", VSdCrmPedidosMarcaColecao::valorTotal) {
                cellFormat {
                    text = it.toString()
                    style {
                        if (it.toString().toFloat() > 0) {
                            backgroundColor += c("#69bad2")
                            textFill = Color.BLACK
                        }
                    }
                }
            }
        }
        nestedColumn("Cancelados") {
            column("Qtd", VSdCrmPedidosMarcaColecao::qtdCancelado) {
                prefWidth = 70.0
                cellFormat {
                    text = it.toString()
                    style {
                        if (it.toString().toInt() > 0) {
                            backgroundColor += c("#dc6d69")
                            textFill = Color.BLACK
                        }
                    }
                }
            }
            column("Valor", VSdCrmPedidosMarcaColecao::valorCancelado) {
                cellFormat {
                    text = it.toString()
                    style {
                        if (it.toString().toFloat() > 0) {
                            backgroundColor += c("#dc6d69")
                            textFill = Color.BLACK
                        }
                    }
                }
            }
        }
        smartResize()
        disableWhen(controller.model.dirty)

        controller.model.rebindOnChange(this) { selectedProduct ->
            item = selectedProduct ?: VSdCrmPedidosMarcaColecao()
        }
    }

    private val filtro = titledpane {
        prefWidth = 1920.0
        isAnimated = true
        isCollapsible = true
        text = "Filtros"

        hgrow = Priority.ALWAYS

        paddingAll = 5.0

        form {
            padding = insets(10.0)
            hbox(10) {
                hgrow = Priority.ALWAYS
                prefHeight = 60.0
                minHeight = 60.0
                maxHeight = 60.0
                prefWidth = 100.0

                fieldset("", null, Orientation.VERTICAL) {
                    field("Marca") {
                        combobox(controller.inputFiltroMarca, listOf("Selecione", "DLZ", "FLOR DE LIS")).selectionModel.selectFirst()
                    }
                }

                fieldset("", null, Orientation.VERTICAL) {
                    field("Coleção") {
                        styleClass += "form-field"
                        textfield(controller.inputFiltroColecao) {
                            styleClass += "input-group-apend"
                            prefWidth = 191.0
                        }
                        button("", imageview("/images/icons/buttons/find (1).png")) {
                            HBox.setMargin(this, insets(0, 0, 0, -11))
                            styleClass += "last"
                            styleClass += "warning"
                            action {
                                val filterColecao: GenericFilter<Colecao?>?
                                try {
                                    filterColecao = object : GenericFilter<Colecao?>() {}
                                    filterColecao.show(ResultTypeFilter.SINGLE_RESULT)
                                    controller.inputFiltroColecao.value = filterColecao.selectedReturn?.codigo ?: ""
                                } catch (e: SQLException) {
                                    e.printStackTrace()
                                    LocalLogger.addLog(e.message + "::" + e.localizedMessage, this.javaClass.name + ":" + this.javaClass.enclosingMethod.name)
                                    GUIUtils.showException(e)
                                }
                            }
                        }
                    }
                }

                fieldset("", null, Orientation.VERTICAL) {
                    field("Cliente") {
                        styleClass += "form-field"
                        textfield(controller.inputFiltroCliente) {
                            styleClass += "form-field"
                            prefWidth = 200.0
                        }
                    }
                }

                fieldset("", null, Orientation.VERTICAL) {
                    hbox(10) {
                        field("De:") {
                            styleClass += "form-field"
                            datepicker(controller.inputFiltroEmissaoIni) {}
                        }

                        field("Até:") {
                            styleClass += "form-field"
                            datepicker(controller.inputFiltroEmissaoFim) {}
                        }
                    }
                }

                fieldset("", null, Orientation.VERTICAL) {
                    field("Exibir Apenas") {
                        combobox(
                                controller.inputFiltroExibirApenas,
                                listOf(
                                        "Todos os Pedidos",
                                        "Com Cancelamento",
                                        "Com Canc. Parcial",
                                        "Com Canc. Integral"
                                )).selectionModel.selectFirst()
                    }
                }

                hbox {
                    paddingAll = 20.0
                    button("Filtrar", imageview("/images/icons/buttons/find (1).png")) {
                        style {
                            fontSize = 18.px
                        }
                        action {
                            val validator = CustomValidator()
                                    .addIf(
                                            controller.inputFiltroMarca.value == "Selecione",
                                            SimpleMessage.error("Informe uma marca antes de proceguir", "")
                                    )
                                    .addIf(
                                            controller.inputFiltroEmissaoIni.value == null,
                                            SimpleMessage("A Data de emissão inicial não pode ser em branco", "")
                                    )
                                    .addIf(
                                            controller.inputFiltroEmissaoFim.value == null,
                                            SimpleMessage("A Data de emissão final não pode ser em branco", "")
                                    )
                                    .addIf(
                                            controller.inputFiltroEmissaoIni.value ?: LocalDate.now() > LocalDate.now(),
                                            SimpleMessage("A Data de emissão inicial não deve ser maior que a data de hoje", "")
                                    )
                                    .addIf(
                                            controller.inputFiltroEmissaoIni.value ?: LocalDate.now() > controller.inputFiltroEmissaoFim.value ?: LocalDate.now(),
                                            SimpleMessage("A Data de emissão inicial não deve ser maior que a data de emissão final", "")
                                    )
//                                    .addIf(
//                                            Period.between(
//                                                    (controller.inputFiltroEmissaoIni.value ?: LocalDate.now()),
//                                                    (controller.inputFiltroEmissaoFim.value ?: LocalDate.now())
//                                            ).days > 90,
//                                            SimpleMessage("O Periodo a ser pesquisado não pode exceder 90 dias, devido a quantidade de registros", "")
//                                    )
                                    .execWhen(severity = Severity.ERROR) {
                                        if (it.isNotEmpty()) {
                                            error("Erro", it)
                                        }
                                    }

                            if (!validator.hasError()) {
                                root.runAsyncWithOverlay {
                                    controller.loadPedidos()
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    init {
    }

    val borderpane: BorderPane = borderpane {
        prefHeight = 1080.0
        prefWidth = 1920.0

        // Insere o topo do border pane
        top = ViewHelpers.buildHBoxTopo(
                "/images/icons/gestao pedidos (4).png",
                "Analise de Pedidos (CRM)",
                ">CRM/FLIMV>Analise>Analise de Pedidos (CRM)"
        )

        center = vbox(5) {
            vgrow = Priority.ALWAYS
            hgrow = Priority.ALWAYS
            hbox(5) {
                prefWidth = 1920.0
                vgrow = Priority.ALWAYS
                hgrow = Priority.ALWAYS
                paddingAll = 5.0

                vbox {
                    //prefWidth = 1520.0
                    vgrow = Priority.ALWAYS
                    hgrow = Priority.ALWAYS

                    // Insere o filtro
                    add(filtro)

                    label("Pedidos")

                    // Insere a tabela de produtos
                    add(tableRegistros)

                    label(controller.recordCount) {
                        //text = "Mostrando X registros"
                        maxWidth = 1.7976931348623157E308
                        textFill = c("#655959")
                    }
                }
            }
        }

        bottom = hbox(10) {
            vgrow = Priority.ALWAYS
            paddingAll = 5
            button("Exportar para Excel") {
                styleClass += "info"
                style {
                    fontSize = 15.0.px
                }
                action {
                    val exporter = ExportExcel(controller.registros, VSdCrmPedidosMarcaColecao::class.java)
                    val fileChooser = FileChooser()
                    fileChooser.extensionFilters.addAll(
                            FileChooser.ExtensionFilter("XLSX files (*.xlsx)", "*.xlsx"),
                            FileChooser.ExtensionFilter("XLS files (*.xls)", "*.xls")
                    )
                    val saveFile = fileChooser.showSaveDialog(tableRegistros.scene.window)
                    exporter.exportArrayToExcel(saveFile.absolutePath)
                }
            }
        }
    }

    override val root = borderpane
}