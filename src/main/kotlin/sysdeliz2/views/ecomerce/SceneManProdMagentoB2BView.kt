package sysdeliz2.views.ecomerce

import javafx.beans.property.SimpleStringProperty
import javafx.beans.value.ObservableValue
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.geometry.Orientation
import javafx.geometry.Pos
import javafx.scene.control.ComboBox
import javafx.scene.control.ListCell
import javafx.scene.control.TableColumn
import javafx.scene.control.cell.CheckBoxTableCell
import javafx.scene.image.Image
import javafx.scene.layout.BorderPane
import javafx.scene.layout.HBox
import javafx.scene.layout.Priority
import javafx.stage.StageStyle
import sysdeliz.apis.web.convertr.data.enums.IdentificadorLoja
import sysdeliz.apis.web.convertr.utils.Configurations
import sysdeliz2.controllers.ecomerce.ManProdMagentoB2BController
import sysdeliz2.dao.FluentDao
import sysdeliz2.dao.OrderType
import sysdeliz2.models.ti.Colecao
import sysdeliz2.models.ti.FaixaItem
import sysdeliz2.models.ti.Marca
import sysdeliz2.models.view.*
import sysdeliz2.utils.GUIUtils
import sysdeliz2.utils.enums.ResultTypeFilter
import sysdeliz2.utils.enums.StatusProdutoMagento
import sysdeliz2.utils.gui.window.GenericFilter
import sysdeliz2.utils.sys.LocalLogger
import sysdeliz2.utils.sys.StringUtils
import sysdeliz2.views.ecomerce.fragments.DadosDoProdutoB2BFragment
import sysdeliz2.views.ecomerce.fragments.DadosDoProdutoFragment
import sysdeliz2.views.ecomerce.fragments.FotosDoProdutoB2BFragment
import sysdeliz2.views.helpers.ViewHelpers
import tornadofx.*
import tornadofx.controlsfx.toggleswitch
import java.io.File
import java.sql.SQLException

/**
 *
 * @author lima.joao
 * @since 28/11/2019 15:38
 */
class SceneManProdMagentoB2BView : View("") {
    private val tabelaEcomerce = "B2B"
    private val defHeight = 122.0
    private val defWidth = 122.0

    private val controller: ManProdMagentoB2BController by inject()

    //private val estoqueList = FXCollections.observableArrayList<VSdEstoqueLojaVirtual>()
    //private val fixedList = FXCollections.observableArrayList<VSdEstoqueLojaVirtual>()

    private val model = VSdProdutoMagentoB2BModel(VSdProdutoMagentoB2B())

    private val img1 = imageview("/images/sem_foto.png", false) { fitHeight = defHeight; fitWidth = defWidth }
    private val img2 = imageview("/images/sem_foto.png", false) { fitHeight = defHeight; fitWidth = defWidth }
    private val img3 = imageview("/images/sem_foto.png", false) { fitHeight = defHeight; fitWidth = defWidth }
    private val img4 = imageview("/images/sem_foto.png", false) { fitHeight = defHeight; fitWidth = defWidth }
    private val img5 = imageview("/images/sem_foto.png", false) { fitHeight = defHeight; fitWidth = defWidth }

    private var listTabelaMedida: List<String> = ArrayList()
    private var comboTabelaMedida: ComboBox<String>? = null

    private var tableEstoque = tableview<VSdEstoqueLojaVirtual> {
        maxHeight = 100.0
        prefHeight = 100.0
    }

    private var tablePrecos = tableview<VSdGrupoPrecosB2b> {
        prefWidth = 330.0
        maxWidth = 330.0
        maxHeight = 300.0
        prefHeight = 300.0

        column("Grupo", VSdGrupoPrecosB2b::grupo) {
            minWidth = 40.0
            prefWidth = 40.0
        }
        column("Descrição", VSdGrupoPrecosB2b::descricao) { minWidth = 200.0 }
        column("Preço", VSdGrupoPrecosB2b::preco) {
            minWidth = 85.0
            prefWidth = 85.0
            cellFormat {
                text = StringUtils.toMonetaryFormat(it, 2)
            }
        }
    }

    private val filtro = titledpane {
        prefWidth = 1920.0
        isAnimated = true
        isCollapsible = true
        text = "Filtros"

        paddingAll = 5.0

        form {
            padding = insets(10.0)
            hbox(10) {
                prefHeight = 60.0
                minHeight = 60.0
                maxHeight = 60.0
                prefWidth = 100.0

                fieldset("", null, Orientation.VERTICAL) {
                    field("Código") {
                        styleClass += "form-field"
                        textfield(controller.inputFiltroReferencia) {
                            prefWidth = 80.0
                        }
                    }
                }

                fieldset("", null, Orientation.VERTICAL) {
                    field("Marca") {
                        styleClass += "form-field"
                        textfield(controller.inputFiltroMarca){
                            styleClass += "input-group-apend"
                            prefWidth = 120.0
                        }
                        button("", imageview("/images/icons/buttons/find (1).png")){
                            HBox.setMargin(this, insets(0, 0, 0, -11))
                            styleClass += "last"
                            styleClass += "warning"
                            action {
                                val filterMarca: GenericFilter<Marca?>?
                                try {
                                    filterMarca = object : GenericFilter<Marca?>() {}
                                    filterMarca.show(ResultTypeFilter.SINGLE_RESULT)
                                    controller.inputFiltroMarca.value = filterMarca.selectedReturn?.codigo ?: ""
                                } catch (e: SQLException) {
                                    e.printStackTrace()
                                    LocalLogger.addLog(e.message + "::" + e.localizedMessage, this.javaClass.name + ":" + this.javaClass.enclosingMethod.name)
                                    GUIUtils.showException(e)
                                }
                            }
                        }
                    }
                }

                fieldset("", null, Orientation.VERTICAL) {
                    field("Coleção") {
                        styleClass += "form-field"
                        textfield(controller.inputFiltroColecao) {
                            styleClass += "input-group-apend"
                            prefWidth = 191.0
                        }
                        button("", imageview("/images/icons/buttons/find (1).png")) {
                            HBox.setMargin(this, insets(0, 0, 0, -11))
                            styleClass += "last"
                            styleClass += "warning"
                            action {
                                val filterColecao: GenericFilter<Colecao?>?
                                try {
                                    filterColecao = object : GenericFilter<Colecao?>() {}
                                    filterColecao.show(ResultTypeFilter.SINGLE_RESULT)
                                    controller.inputFiltroColecao.value = filterColecao.selectedReturn?.codigo ?: ""
                                } catch (e: SQLException) {
                                    e.printStackTrace()
                                    LocalLogger.addLog(e.message + "::" + e.localizedMessage, this.javaClass.name + ":" + this.javaClass.enclosingMethod.name)
                                    GUIUtils.showException(e)
                                }
                            }
                        }
                    }
                }

                hbox {
                    prefHeight = 60.0
                    paddingAll = 17.0

                    checkbox("Exibir referencias sem estoque?", controller.exibirOsSemEstoque)
                }

                hbox {
                    paddingAll = 20.0
                    button("Filtrar", imageview("/images/icons/buttons/find (1).png")) {
                        style {
                            fontSize = 18.px
                        }
                        action {
                            //if (controller.inputFiltroMarca.get() == "Selecione") {
                            //    warning("Selecione ao menos um registro para prosseguir")
                            //} else {
                                controller.carregaProdutos(IdentificadorLoja.B2B)
                            //}

                            listTabelaMedida = listOf(
                                        "Selecione",
                                        "CAMISAS_CAMISETAS_JAQUETAS_D",
                                        "REGULAR_SLIM_RELAXED_CONFORT_D",
                                        "SKINNY_D",
                                        "DENIM_F",
                                        "OUTROS_F")

                            if(comboTabelaMedida != null) {
                                comboTabelaMedida!!.items.clear()
                                comboTabelaMedida!!.items.setAll(listTabelaMedida)
                            }
                        }
                    }
                }
            }
        }
    }

    private val fieldSetDadosMagento = fieldset("Dados Magento", null, Orientation.VERTICAL) {
        vbox(5) {
            field("Título para o SEO") {
                textfield(model.metaTitle) { prefWidth = 800.0 }
            }
            field("Palavras Chaves (Meta Keyword)") {
                textfield(model.metaKeyword) { prefWidth = 800.0 }
            }
            field("Descrição para o SEO") {
                textfield(model.metaDescription) { prefWidth = 800.0 }
            }
        }

        vbox {
            field("Descrição") {
                textfield(model.name) { prefWidth = 800.0 }
            }

            hbox(5) {
                field("Descrição Longa") {
                    textarea(model.description) {
                        minWidth = 200.0
                        minHeight = 80.0
                        prefHeight = 80.0
                        maxHeight = 80.0
                        isWrapText = true
                    }
                }
                field("Descrição Curta") {
                    textarea(model.shortDescription) {
                        minWidth = 200.0
                        minHeight = 80.0
                        prefHeight = 80.0
                        maxHeight = 80.0
                        isWrapText = true
                    }
                }
            }
        }

        hbox(5) {
            prefWidth = 800.0
            minWidth = 800.0
            maxWidth = 800.0
            field("Tabela de Medida") {
                prefWidth = 800.0
                minWidth = 800.0
                maxWidth = 800.0
                comboTabelaMedida = combobox(model.tabelaMedida, listTabelaMedida) {
                    prefWidth = 800.0
                    minWidth = 800.0
                    maxWidth = 800.0


                    setCellFactory {
                        object : ListCell<String?>() {
                            override fun updateItem(item: String?, empty: Boolean) {
                                super.updateItem(item, empty)
                                if (item == null || empty) {
                                    graphic = null
                                } else {
                                    text = when(item){
                                        // FLOR DE LIS
                                        "DENIM_F" -> "DENIM"
                                        "OUTROS_F" -> "OUTROS"

                                        //DLZ
                                        "CAMISAS_CAMISETAS_JAQUETAS_D" -> "CAMISAS, CAMISETAS, JAQUETAS"
                                        "REGULAR_SLIM_RELAXED_CONFORT_D" -> "REGULAR, SLIM, RELAXED, CONFORT"
                                        "SKINNY_D" -> "SKINNY"
                                        // NAO DEFINIDO
                                        else -> "Selecione"
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }

        hbox(5) {
            button("Salvar") {
                enableWhen(model.dirty)
                action {
                    if (controller.produtos.isEmpty()) {
                        warning("Selecione ao menos um registro para prosseguir")
                    }
                    // Persiste os dados para o objeto
                    model.commit()
                    controller.atualizaDadosSdProdutos001(model.item)
                    // Atualiza os dados no grid
                    controller.tableProdutos!!.refresh()
                }
            }

            hbox {
                minWidth = 25.0
            }

            button("Cancelar") {
                enableWhen(model.dirty)
                action {
                    model.rollback()
                }
            }
        }
    }

    private val fieldSetTabelaEstoque = fieldset("Estoque por Tamanho", null) {
        maxHeight = 100.0
        prefHeight = 100.0
        add(tableEstoque)
    }

    private val fieldSetTabelaPrecos = fieldset("Grupos de Preços", null) {
        maxHeight = 200.0
        prefHeight = 200.0
        add(tablePrecos)
    }

    private val fieldSetImagemProdutos = fieldset("Imagens dos produtos") {
        flowpane {
            paddingAll = 5.0

            add(img1)
            hbox(5) { prefWidth = 5.0 }
            add(img2)
            hbox(5) { prefWidth = 5.0 }
            add(img3)
            hbox(5) { prefWidth = 5.0 }
            add(img4)
            hbox(5) { prefWidth = 5.0 }
            add(img5)
        }

    }

    private fun reloadTableEstoque() {
        // remove as colunas
        tableEstoque.columns.clear()
        if (tableEstoque.items.size != 1) {
            //tableEstoque.items.setAll(fixedList)
        }
/*
        for (vSdEstoqueLojaVirtual in estoqueList) {
            val col = TableColumn<VSdEstoqueLojaVirtual, String>(vSdEstoqueLojaVirtual.tamanho ?: "")
            col.setCellValueFactory {
                col.text = vSdEstoqueLojaVirtual.tamanho
                SimpleStringProperty(vSdEstoqueLojaVirtual.quantidade.toString())
            }
            tableEstoque.columns.add(col)
        }

 */
    }

    private fun getImage(url: String): Image {
        return if (File(url).exists()) {
            Image("file:/$url")
        } else {
            Image("/images/sem_foto.png")
        }
    }

    init {

        //fixedList.add(VSdEstoqueLojaVirtual())

        controller.tabela = tabelaEcomerce

        // executa um reset ao abrir a tela novamente.
        controller.reset()
    }

    val borderpane: BorderPane = borderpane {
        prefHeight = 1080.0
        prefWidth = 1920.0

        // Insere o topo do border pane
        top = ViewHelpers.buildHBoxTopo(
                "/images/logo-magento-48.png",
                "Manutenção de Produtos",
                ">Loja Virtual>Produtos B2B"
        )

        center = vbox(5) {
            hbox(5) {
                prefWidth = 1920.0
                vgrow = Priority.ALWAYS

                paddingAll = 5.0

                // Bloco da esquerda que contem a listagem de produtos
                vbox {
                    prefWidth = 1520.0
                    vgrow = Priority.ALWAYS

                    hbox(5) {

                        minHeight = 50.0
                        padding = insets(5, 5, 5, 5)

                        button("Enviar Produtos") {
                            styleClass += "success"
                            prefWidth = 150.0
                            style { fontSize = 15.0.px }
                            action {
                                if (controller.produtos.isNotEmpty()) {
                                    confirm(
                                            "Confirmar Envio dos produtos",
                                            "Apenas os produtos selecionados e com o status Pendente, Processado ou Prod. OK serão enviados."
                                    ) {
                                        DadosDoProdutoB2BFragment().openModal(stageStyle = StageStyle.UTILITY)
                                        controller.tableProdutos!!.refresh()
                                    }
                                } else {
                                    warning("Não existem referências para envio", "Clique em filtrar antes de tentar enviar os produtos")
                                }
                            }
                        }

                        button("Cons. Sit. Prod.") {
                            styleClass += "info"
                            prefWidth = 150.0
                            style { fontSize = 15.0.px }
                            action {
                                if (controller.produtos.isNotEmpty()) {
                                    root.runAsyncWithOverlay {
                                        controller.consultaStatusEnvioProdutos()
                                    } ui { count ->
                                        controller.tableProdutos!!.refresh()
                                        information("Atualizado o status de $count produtos.")
                                    }
                                } else {
                                    warning("Não existem referências para atualizar", "Clique em filtrar antes de tentar verificar o status dos produtos")
                                }
                            }
                        }

                        button("Enviar Estoque") {
                            styleClass += "warning"
                            prefWidth = 150.0
                            style { fontSize = 15.0.px }
                            action {
                                if (controller.produtos.isNotEmpty()) {
                                    confirm("Confirmar Envio do estoque dos produtos", "Apenas os estoques dos produtos com os status Processado e Prod. OK serão enviados") {
                                        root.runAsyncWithOverlay {
                                           controller.enviaEstoqueParaLoja()
                                        } ui { count ->
                                            controller.tableProdutos!!.refresh()
                                            information("Enviado posição do estoque de $count produtos.")
                                        }
                                    }
                                } else {
                                    warning("Não existem referências para envio", "Clique em filtrar antes de tentar enviar a posição do estoque dos produtos")
                                }
                            }
                        }

                        button("Cons. Sit. Estoque") {
                            styleClass += "info"
                            prefWidth = 150.0
                            style { fontSize = 15.0.px }
                            action {
                                if (controller.produtos.isNotEmpty()) {
                                    root.runAsyncWithOverlay {
                                        controller.consultaStatusEnvioEstoque()
                                    } ui { count ->
                                        controller.tableProdutos!!.refresh()
                                        information("Atualizado status do envio do estoque de $count produtos.")
                                    }
                                } else {
                                    warning("Não existem referências para atualizar", "Clique em filtrar antes de tentar verificar o status dos produtos")
                                }

                            }
                        }

                        button("Enviar Fotos") {
                            styleClass += "warning"
                            prefWidth = 150.0
                            style { fontSize = 15.0.px }
                            action {
                                if (controller.produtos.isNotEmpty()) {
                                            FotosDoProdutoB2BFragment().openModal(stageStyle = StageStyle.UTILITY)
                                            controller.tableProdutos!!.refresh()
                                } else {
                                    warning("Não existem referências para envio", "Clique em filtrar antes de tentar enviar a posição do estoque dos produtos")
                                }
                            }
                        }

                        button("Cons. Sit. Fotos") {
                            styleClass += "info"
                            prefWidth = 150.0
                            style { fontSize = 15.0.px }
                            action {
                                if (controller.produtos.isNotEmpty()) {
                                    root.runAsyncWithOverlay {
                                        controller.consultaStatusEnvioFotos()
                                    } ui { count ->
                                        controller.tableProdutos!!.refresh()
                                        information("Atualizado status do envio de imagem de $count produtos.")
                                    }
                                } else {
                                    warning("Não existem referências para atualizar", "Clique em filtrar antes de tentar verificar o status dos produtos")
                                }

                            }
                        }

                        hbox { prefWidth = 50.0; minWidth = 50.0 }

                        vbox {

                            hbox(5) {
                                padding = insets(top = 5, left = -15)
                                toggleswitch("", controller.emProducao)
                                label("Enviar dados para produção") {
                                    padding = insets(left = 10.0)
                                    style {
                                        fontSize = 15.px
                                    }
                                }
                            }

                        }
                    }

                    // Insere o filtro
                    add(filtro)

                    label("Produtos")

                    // Insere a tabela de produtos
                    controller.tableProdutos = tableview(controller.produtos) {
                        prefHeight = 800.0
                        isEditable = true
                        column("#", VSdProdutoMagentoB2B::selected) {
                            style {
                                alignment = Pos.CENTER_RIGHT
                            }
                            prefWidth = 30.0
                            setCellFactory { CheckBoxTableCell() }
                            setCellValueFactory { cellData: TableColumn.CellDataFeatures<VSdProdutoMagentoB2B, Boolean?> ->
                                val cellValue = cellData.value
                                val property = cellValue.selected.toProperty()
                                property.addListener { _: ObservableValue<out Boolean?>?, _: Boolean?, newValue: Boolean? ->
                                    cellValue.selected = newValue!!

                                    if(cellValue.selected) {
                                        controller.qtdSelecionada++
                                    } else {
                                        controller.qtdSelecionada--
                                    }

                                    if(controller.qtdSelecionada<0){
                                        controller.qtdSelecionada = 0
                                    }
                                    controller.recordCount.value = "Mostrando ${controller.produtos.size} produtos | Selecionado ${controller.qtdSelecionada} produdtos"
                                }
                                property
                            }
                        }
                        column("SKU", VSdProdutoMagentoB2B::codigo) { minWidth = 55.0 }
                        column("Descrição", VSdProdutoMagentoB2B::name) { minWidth = 220.0 }
                        column("Descrição Longa", VSdProdutoMagentoB2B::description) { minWidth = 290.0 }
                        column("Coleção", VSdProdutoMagentoB2B::colecaoDesc) { minWidth = 100.0 }
                        column("Preço", VSdProdutoMagentoB2B::price)
                        column("Preco Especial", VSdProdutoMagentoB2B::specialPrice)
                        column("Status", VSdProdutoMagentoB2B::status) {
                            maxWidth = 50.0
                            cellFormat {
                                try {
                                    graphic = if (it == "1") {
                                        imageview("/images/icons/buttons/active (2).png", false)
                                    } else {
                                        imageview("/images/icons/buttons/inactive (2).png", false)
                                    }
                                } catch (e: Exception) {
                                    text = it
                                }
                            }
                        }
                        column("Processamento", VSdProdutoMagentoB2B::statusProcessamento) {
                            // maxWidth = 150.0
                            cellFormat {
                                text = when (it) {
                                    "0" -> StatusProdutoMagento.PENDENTE.valueOf()
                                    "1" -> StatusProdutoMagento.ENVIADO.valueOf()
                                    "2" -> StatusProdutoMagento.PROCESSADO.valueOf()
                                    "3" -> StatusProdutoMagento.ENVIO_ESTOQUE.valueOf()
                                    "4" -> StatusProdutoMagento.PRODUTO_OK.valueOf()
                                    "5" -> StatusProdutoMagento.ENVIO_FOTO.valueOf()
                                    else -> StatusProdutoMagento.UNDEFINED.valueOf()
                                }
                            }
                        }

                        smartResize()

                        disableWhen(model.dirty)

                        // Update the person inside the view model on selection change
                        model.rebindOnChange(this) { selectedProduct ->
                            item = selectedProduct ?: VSdProdutoMagentoB2B()

                            reloadTableEstoque()
                            tablePrecos.items.clear()
                            if (selectedProduct != null) {
                                tablePrecos.items.setAll(selectedProduct.precosB2b)
                            }

                            if(item.marca == "D" || item.marca == "F") {
                                val file = if( item.marca == "D") {
                                    File(Configurations.URL_PHOTOS_DLZ)
                                } else {
                                    File(Configurations.URL_PHOTOS_FLOR)
                                }

                                file.listFiles()?.let {
                                    runAsync {
                                        try {
                                            img1.image = Image("/images/loading.gif")
                                            img2.image = Image("/images/loading.gif")
                                            img3.image = Image("/images/loading.gif")
                                            img4.image = Image("/images/loading.gif")
                                            img5.image = Image("/images/loading.gif")

                                            var temImg1 = false
                                            var temImg2 = false
                                            var temImg3 = false
                                            var temImg4 = false
                                            var temImg5 = false

                                            it.filter { predicate -> predicate.isFile && predicate.path.contains("${item.codigo}") }.forEach { f ->
                                                if (f.path.toLowerCase().contains("_1.jpg")) {
                                                    img1.image = getImage(f.path)
                                                    temImg1 = true
                                                }

                                                if (f.path.toLowerCase().contains("_2.jpg")) {
                                                    img2.image = getImage(f.path)
                                                    temImg2 = true
                                                }

                                                if (f.path.toLowerCase().contains("_3.jpg")) {
                                                    img3.image = getImage(f.path)
                                                    temImg3 = true
                                                }

                                                if (f.path.toLowerCase().contains("_4.jpg")) {
                                                    img4.image = getImage(f.path)
                                                    temImg4 = true
                                                }

                                                if (f.path.toLowerCase().contains("_5.jpg")) {
                                                    img5.image = getImage(f.path)
                                                    temImg5 = true
                                                }
                                            }

                                            if (!temImg1) img1.image = Image("/images/sem_foto_obrigatoria.png")
                                            if (!temImg2) img2.image = getImage("")
                                            if (!temImg3) img3.image = getImage("")
                                            if (!temImg4) img4.image = getImage("")
                                            if (!temImg5) img5.image = getImage("")

                                            true
                                        } catch (e: Exception) {
                                            false
                                        }
                                    }
                                }
                            }

                        }
                    }

                    label(controller.recordCount) {
                        //text = "Mostrando X registros"
                        maxWidth = 1.7976931348623157E308
                        textFill = c("#655959")
                    }

                    hbox(5) {

                        combobox(controller.selectItensTable, listOf("Selecionar...", "Todos", "Com Descrição", "Sem Descrição", "Não Enviados", "Enviados", "Processados", "Com Estoque", "Envio Completo")){
                            selectionModel.selectedItemProperty().addListener(ChangeListener { observable, oldValue, newValue ->
                                if (newValue != null && newValue.equals("Selecionar...").not()){
                                    controller.produtos.forEach {
                                        it.selected = false
                                    }
                                    if (controller.qtdSelecionada > 0) {
                                        controller.qtdSelecionada = 0
                                    }

                                    if (newValue.equals("Todos")){
                                        controller.produtos.forEach {
                                            if(!it.selected){
                                                controller.qtdSelecionada++
                                            }
                                            it.selected = true
                                        }
                                    }else if (newValue.equals("Todos")){
                                        controller.produtos.forEach {
                                            if(!it.selected){
                                                controller.qtdSelecionada++
                                            }
                                            it.selected = true
                                        }
                                    }else if (newValue.equals("Com Descrição")){
                                        controller.produtos.filtered { t -> t.description != null && t.description?.length?.compareTo(0) != 0 }.forEach {
                                            if(!it.selected){
                                                controller.qtdSelecionada++
                                            }
                                            it.selected = true
                                        }
                                    }else if (newValue.equals("Sem Descrição")){
                                        controller.produtos.filtered { t -> t.description == null || t.description?.length?.compareTo(0) == 0 }.forEach {
                                            if(!it.selected){
                                                controller.qtdSelecionada++
                                            }
                                            it.selected = true
                                        }
                                    }else if (newValue.equals("Não Enviados")){
                                        controller.produtos.filtered { t -> t.statusProcessamento == "0" }.forEach {
                                            if(!it.selected){
                                                controller.qtdSelecionada++
                                            }
                                            it.selected = true
                                        }
                                    }else if (newValue.equals("Enviados")){
                                        controller.produtos.filtered { t -> t.statusProcessamento == "1" }.forEach {
                                            if(!it.selected){
                                                controller.qtdSelecionada++
                                            }
                                            it.selected = true
                                        }
                                    }else if (newValue.equals("Processados")){
                                        controller.produtos.filtered { t -> t.statusProcessamento == "2" }.forEach {
                                            if(!it.selected){
                                                controller.qtdSelecionada++
                                            }
                                            it.selected = true
                                        }
                                    }else if (newValue.equals("Com Estoque")){
                                        controller.produtos.filtered { t -> t.statusProcessamento == "3" }.forEach {
                                            if(!it.selected){
                                                controller.qtdSelecionada++
                                            }
                                            it.selected = true
                                        }
                                    }else if (newValue.equals("Envio Completo")){
                                        controller.produtos.filtered { t -> t.statusProcessamento == "4" }.forEach {
                                            if(!it.selected){
                                                controller.qtdSelecionada++
                                            }
                                            it.selected = true
                                        }
                                    }

                                    controller.recordCount.value = "Mostrando ${controller.produtos.size} produtos | Selecionado ${controller.qtdSelecionada} produdtos"
                                    controller.tableProdutos!!.refresh()
                                }
                            })
                        }.selectionModel.selectFirst()


//                        button("Selecionar todos") {
//                            styleClass += "success"
//                            style { fontSize = 12.0.px }
//                            action {
//                                controller.produtos.forEach {
//                                    if(!it.selected){
//                                        controller.qtdSelecionada++
//                                    }
//                                    it.selected = true
//                                }
//                                controller.recordCount.value = "Mostrando ${controller.produtos.size} produtos | Selecionado ${controller.qtdSelecionada} produdtos"
//                                controller.tableProdutos!!.refresh()
//                            }
//                        }

                        button("Desmarcar todos") {
                            styleClass += "success"
                            style { fontSize = 12.0.px }
                            action {
                                controller.produtos.forEach {
                                    it.selected = false
                                    if (controller.qtdSelecionada > 0) {
                                        controller.qtdSelecionada--
                                    }
                                }
                                controller.recordCount.value = "Mostrando ${controller.produtos.size} produtos | Selecionado ${controller.qtdSelecionada} produdtos"
                                controller.tableProdutos!!.refresh()
                            }
                        }
                    }
                }
                form {
                    add(fieldSetDadosMagento)
                    //add(fieldSetTabelaEstoque)
                    add(fieldSetImagemProdutos)
                    add(fieldSetTabelaPrecos)
                }
            }
        }
    }

    override val root = borderpane
}