package sysdeliz2.views.ecomerce

import javafx.geometry.Orientation
import javafx.geometry.Pos
import javafx.scene.control.ContentDisplay
import javafx.scene.control.TableRow
import javafx.scene.control.TableView
import javafx.scene.layout.BorderPane
import javafx.scene.layout.HBox
import javafx.scene.layout.Priority
import javafx.scene.paint.Color
import javafx.stage.StageStyle
import javafx.util.Callback
import javafx.util.Duration
import sysdeliz.apis.web.convertr.data.enums.PaymentMethod
import sysdeliz.apis.web.convertr.data.enums.StatusOrder
import sysdeliz2.controllers.ecomerce.OrderMagentoController
import sysdeliz2.models.sysdeliz.SdMagentoOrderBean
import sysdeliz2.models.sysdeliz.SdMagentoOrderItemBean
import sysdeliz2.models.ti.Entidade
import sysdeliz2.models.ti.Marca
import sysdeliz2.utils.GUIUtils
import sysdeliz2.utils.enums.ResultTypeFilter
import sysdeliz2.utils.extensions.toClipboardFX
import sysdeliz2.utils.gui.messages.MessageBox
import sysdeliz2.utils.gui.window.GenericFilter
import sysdeliz2.utils.sys.LocalLogger
import sysdeliz2.utils.sys.StringUtils
import sysdeliz2.views.ecomerce.fragments.UpdateStatusPedidoFragment
import sysdeliz2.views.helpers.ViewHelpers
import tornadofx.*
import tornadofx.controlsfx.errorNotification
import tornadofx.controlsfx.warningNotification
import java.sql.SQLException


/**
 *
 * @author lima.joao
 * @since 14/01/2020 16:06
 */
class SceneOrderMagentoView() : View("") {

    private val controller: OrderMagentoController by inject()

    private lateinit var tablePedidos: TableView<SdMagentoOrderBean>
    private lateinit var tableProdutos: TableView<SdMagentoOrderItemBean>

    init {
        this.controller.isB2B = false
        //this.controller.enableThreadImport()
    }

    private val filtro = titledpane {
        prefWidth = 1920.0
        isAnimated = true
        isCollapsible = true
        text = "Filtros"

        paddingAll = 5.0

        form {
            padding = insets(5.0)
            hbox(10) {
                prefHeight = 60.0
                minHeight = 60.0
                maxHeight = 60.0
                prefWidth = 100.0

                fieldset("", null, Orientation.VERTICAL) {
                    field("Marca") {
                        styleClass += "form-field"
                        textfield(controller.inputFiltroMarca) {
                            styleClass += "input-group-apend"
                            prefWidth = 120.0
                        }
                        button("", imageview("/images/icons/buttons/find (1).png")) {
                            HBox.setMargin(this, insets(0, 0, 0, -11))
                            styleClass += "last"
                            styleClass += "warning"
                            action {
                                val filterMarca: GenericFilter<Marca?>?
                                try {
                                    filterMarca = object : GenericFilter<Marca?>() {}
                                    filterMarca.show(ResultTypeFilter.SINGLE_RESULT)
                                    controller.inputFiltroMarca.value = filterMarca.selectedReturn?.codigo ?: ""
                                } catch (e: SQLException) {
                                    e.printStackTrace()
                                    LocalLogger.addLog(e.message + "::" + e.localizedMessage, this.javaClass.name + ":" + this.javaClass.enclosingMethod.name)
                                    GUIUtils.showException(e)
                                }
                            }
                        }
                    }
                }

                fieldset("", null, Orientation.VERTICAL) {
                    field("Tipo") {
                        combobox(controller.inputTipoPedido, listOf("Todos", "Integrados", "Não Integrados")).selectionModel.selectFirst()
                    }
                }

                fieldset("", null, Orientation.VERTICAL) {
                    field("Cliente") {
                        styleClass += "form-field"
                        textfield(controller.inputFiltroCliente) {
                            styleClass += "input-group-apend"
                            prefWidth = 191.0
                        }
                        button("", imageview("/images/icons/buttons/find (1).png")) {
                            HBox.setMargin(this, insets(0, 0, 0, -11))
                            styleClass += "last"
                            styleClass += "warning"
                            action {
                                val filterCliente: GenericFilter<Entidade?>?
                                try {
                                    filterCliente = object : GenericFilter<Entidade?>() {}
                                    filterCliente.show(ResultTypeFilter.SINGLE_RESULT)
                                    controller.inputFiltroCliente.value = if (filterCliente?.selectedReturn?.codcli != null) {
                                        filterCliente?.selectedReturn?.codcli.toString()
                                    } else {
                                        ""
                                    }
                                } catch (e: SQLException) {
                                    e.printStackTrace()
                                    LocalLogger.addLog(e.message + "::" + e.localizedMessage, this.javaClass.name + ":" + this.javaClass.enclosingMethod.name)
                                    GUIUtils.showException(e)
                                }
                            }
                        }
                    }
                }

                fieldset("", null, Orientation.VERTICAL) {
                    field("Código de Rastreio") {
                        styleClass += "form-field"
                        textfield(controller.inputFiltroNota) {
                            styleClass += "input-group-apend"
                            prefWidth = 191.0
//                            filterInput { change ->
//
//                                change.text = change.text.filter { it.isDigit() }
//                                true
//                            }
                        }
                    }
                }
                fieldset("", null, Orientation.VERTICAL) {
                    hbox(10) {
                        vbox {
                            checkbox("Cancelados", controller.inputStatusCanceled)
                            checkbox("Entregues", controller.inputStatusEntregue)
                        }
                        vbox {
                            checkbox("Aguardando Pagto", controller.inputStatusPending)
                            checkbox("Pagto Processado", controller.inputStatusProcessing)
                        }
                        vbox {
                            checkbox("Em Transporte", controller.inputStatusComplete)
                            checkbox("Fraudes", controller.inputStatusFraude)
                        }
                    }
                }

                hbox {
                    paddingAll = 15.0
                    button("Filtrar", imageview("/images/icons/buttons/find (1).png")) {
                        style {
                            fontSize = 18.px
                        }
                        action {
                            tablePedidos.refresh()
                            controller.buscarPedidosLocal()
                            tablePedidos.refresh()
                        }
                    }
                }
            }

        }

    }

    val borderpane: BorderPane = borderpane {
        top = ViewHelpers.buildHBoxTopo(
                "/images/logo-magento-48.png",
                "Manutenção de Pedidos",
                ">Loja Virtual>Manutenção Pedidos"
        )

        center = vbox(5) {
            paddingAll = 10.0

            vgrow = Priority.ALWAYS

            hbox(5) {

                prefHeight = 60.0
                prefWidth = Double.NEGATIVE_INFINITY

                button("Pausar Consulta Automática") {
                    styleClass += "warning"
                    style {
                        fontSize = 15.0.px
                    }
                    action {
                        //controller.disableThreadImport()
                    }
                }

                button("Iniciar Consulta Automática") {
                    styleClass += "info"
                    style {
                        fontSize = 15.0.px
                    }
                    action {
                        //controller.enableThreadImport()
                    }
                }

                button("Verificar pedidos em Transporte") {
                    styleClass += "success"
                    style {
                        fontSize = 15.0.px
                    }
                    action {
                        val updated = controller.consultaStatusPedidosEmTransporteNosCorreios()
                        if (updated > 0) {
                            information("Atualizado $updated na loja", "Aguarde a proxima sincronização para que o status seja atualizado.")
                        }
                    }
                }
            }

            add(filtro)

            hbox {
                vgrow = Priority.ALWAYS
                hgrow = Priority.ALWAYS
                vbox {
                    label("Pedidos")
                    hgrow = Priority.ALWAYS
                    vgrow = Priority.ALWAYS

                    // Aqui informa a tabela de pedidos
                    tablePedidos = tableview(controller.pedidos) {
                        vgrow = Priority.ALWAYS
                        column("Ações", SdMagentoOrderBean::id) {
                            isEditable = false
                            prefWidth = 108.0
                            isResizable = false
                            isSortable = false

                            cellFormat { cell ->
                                graphic = hbox(3) {
                                    cell.let {
                                        button("Integrar Pedido") {
                                            graphic = imageview("/images/icons/buttons/select (1).png")
                                            tooltip = tooltip("Importar pedido para o ERP")
                                            styleClass += "info"
                                            styleClass += "xs"
                                            contentDisplay = ContentDisplay.GRAPHIC_ONLY
                                            action {
                                                selectWhere { sdMagentoOrderBean -> sdMagentoOrderBean.id == it }
                                                if (!selectedItem?.codigoErp.isNullOrEmpty()) {
                                                    errorNotification(
                                                            "Operação nao permitida",
                                                            "O Pedido selecionado já esta integrado no ERP com o codigo: ${selectedItem?.codigoErp}",
                                                            Pos.TOP_RIGHT,
                                                            Duration.seconds(5.0))
                                                } else {
                                                    if (StatusOrder.stringToEnum(selectedItem?.status
                                                                    ?: "") != StatusOrder.PROCESSANDO) {
                                                        warningNotification(
                                                                "Não permitido",
                                                                "Somente pedidos com o status \"Pagto Processado\" podem ser integrados",
                                                                Pos.TOP_RIGHT,
                                                                Duration.seconds(5.0))
                                                    } else {
                                                        confirm("Deseja integrar o pedido?", " O Pedido será gravado na tabela de pedidos.") {
                                                            selectedItem?.let {
                                                                controller.integrarPedido(it)
                                                                tablePedidos.refresh()
                                                                MessageBox.create {
                                                                    it.message("Pedido integrado com sucesso")
                                                                    it.type(MessageBox.TypeMessageBox.CONFIRM)
                                                                    it.position(Pos.TOP_RIGHT)
                                                                    it.notification()
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        button("Atualizar Status") {
                                            graphic = imageview("/images/icons/buttons/send (1).png")
                                            tooltip = tooltip("Atualizar o status na loja")
                                            styleClass += "warning"
                                            styleClass += "xs"
                                            contentDisplay = ContentDisplay.GRAPHIC_ONLY

                                            action {
                                                selectWhere { sdMagentoOrderBean -> sdMagentoOrderBean.id == it }

                                                val canExecute = StatusOrder.stringToEnum(selectedItem?.status
                                                        ?: "") in arrayOf(
                                                        StatusOrder.CANCELADO,
                                                        StatusOrder.FECHADO
                                                )

                                                if (canExecute) {
                                                    warningNotification(
                                                            "Não permitido",
                                                            "O Status do pedido não pode ser alterado",
                                                            Pos.TOP_RIGHT,
                                                            Duration.seconds(5.0))
                                                } else {
                                                    confirm("Deseja enviar uma alteração de status?", "O Status do pedido será atualizado na loja virtual") {
                                                        selectedItem?.let {
                                                            UpdateStatusPedidoFragment(StatusOrder.COMPLETE).openModal(stageStyle = StageStyle.UTILITY)
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        button("Cancelar o pedido") {
                                            graphic = imageview("/images/icons/buttons/cancel (1).png")
                                            tooltip = tooltip("Cancelar o pedido selecionado")
                                            styleClass += "danger"
                                            styleClass += "xs"
                                            contentDisplay = ContentDisplay.GRAPHIC_ONLY

                                            action {
                                                selectWhere { sdMagentoOrderBean -> sdMagentoOrderBean.id == it }

                                                val canExecute = StatusOrder.stringToEnum(selectedItem?.status
                                                        ?: "") in arrayOf(
                                                        StatusOrder.PENDENTE,
                                                        StatusOrder.PROCESSANDO,
                                                        StatusOrder.FATURADO,
                                                        StatusOrder.COMPLETE,
                                                        StatusOrder.EM_TRANSPORTE
                                                )
                                                if (!canExecute) {
                                                    warningNotification(
                                                            "Não permitido",
                                                            "O Status do pedido não pode ser alterado",
                                                            Pos.TOP_RIGHT,
                                                            Duration.seconds(5.0))

                                                } else {
                                                    confirm("Deseja cancelar?", "O Status do pedido seja alterado para cancelado na loja") {
                                                        selectedItem?.let {
                                                            UpdateStatusPedidoFragment(StatusOrder.CANCELADO).openModal(stageStyle = StageStyle.UTILITY)
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        column("ID", SdMagentoOrderBean::orderId) { prefWidth = 70.0 }
                        column("Código ERP", SdMagentoOrderBean::codigoErp) {
                            prefWidth = 80.0
                            cellFormat {
                                text = it.toString()
                                style {
                                    if ((it ?: "").isNotEmpty()) {
                                        backgroundColor += c("#81cf81")
                                    }
                                }
                            }
                        }
                        column("Status", SdMagentoOrderBean::status) {
                            prefWidth = 100.0
                            cellFormat {
                                text = StatusOrder.normalize(StatusOrder.stringToEnum(it.toString()))

                                style {
                                    backgroundColor += when (it ?: "pendente") {
                                        "canceled" -> c("#dc6d69")
                                        "closed" -> c("#dc6d69")
                                        "pending" -> c("#fab552") //
                                        "processing" -> c("#81cf81") //62c462
                                        "complete" -> c("#69bad2") //d8ecf6
                                        "complete_shipped" -> c("#69bad2") //d8ecf6
                                        "entregue" -> c("#62C462")
                                        "fraud" -> c("#dc6d69")
                                        else -> Color.WHITE
                                    }
                                }
                            }
                        }
                        column("Marca", SdMagentoOrderBean::marca) {
                            prefWidth = 40.0
                            minWidth = 40.0
                            maxWidth = 40.0
                            value { param ->
                                when (param.value.marca) {
                                    "F" -> "FLOR"
                                    "D" -> "DLZ"
                                    else -> ""
                                }
                            }

                            cellFormat {
                                text = it.toString()
                                style {
                                    backgroundColor += if ((it ?: "") == "FLOR") {
                                        c("#f2dede")
                                    } else {
                                        c("#d8ecf6")
                                    }
                                    textFill = Color.BLACK
                                }
                            }

                        }
                        nestedColumn("Cliente") {
                            column("Nome", String::class) {
                                prefWidth = 150.0
                                maxWidth = 150.0
                                minWidth = 100.0
                                value { param -> param.value.customer.firstname ?: "" }
                            }.remainingWidth()
                            column("Sobrenome", String::class) {
                                prefWidth = 250.0
                                maxWidth = 250.0
                                value { param -> param.value.customer.lastname ?: "" }
                            }
                            column("Email", String::class) {
                                prefWidth = 250.0
                                maxWidth = 250.0
                                value { param -> param.value.customer.email ?: "" }
                            }
                        }
                        nestedColumn("Pagamento") {
                            column("Método", String::class) {
                                prefWidth = 150.0
                                value { param ->
                                    when (PaymentMethod.stringToEnum(param.value.payment.method)) {
                                        PaymentMethod.MUNDIPAGG_CREDITCARD -> "Cartão Débito/Crédito"
                                        PaymentMethod.FOXSEA_PAGHIPER -> "Boleto bancário"
                                        else -> "Indefinido"
                                    }
                                }
                            }
                            column("Frete", String::class) {
                                prefWidth = 60.0
                                value { param -> param.value.shipping.total }
                            }
                            column("Total", String::class) {
                                prefWidth = 80.0
                                value { param -> param.value.totals.grandTotal }
                            }
                            column("Desconto", String::class) {
                                prefWidth = 80.0
                                value { param -> param.value.totals.desconto }
                            }
                        }

                        column("Ratreio", SdMagentoOrderBean::trackId)
                        column("Entrega", String::class) {
                            prefWidth = 150.0
                            value { param -> param.value.shipping.typeFrete }
                        }

                        column("Dt Cad", SdMagentoOrderBean::createdAt) {
                            prefWidth = 120.0
                            minWidth = 120.0
                            maxWidth = 120.0
                        }

                        onSelectionChange {
                            controller.produtos.clear()
                            it?.let { order ->
                                controller.produtos.setAll(order.items)
                                SdMagentoOrderItemBean.setReservadoItem(order)
                            }
                        }

                        controller.model.rebindOnChange(this) { selectedOrder ->
                            item = selectedOrder ?: SdMagentoOrderBean()
                        }

                        smartResize()

                        contextmenu {
                            item("Copiar Código Rastreio").action {
                                selectedItem?.let {
                                    if (it.trackId != null) {
                                        it.trackId!!.toClipboardFX()
                                    }
                                }
                            }
                            item("Consultar Rastreio").action {
                                selectedItem?.let {
                                    if (it.trackId != null) {
                                        controller.consultaCodigoRastreio(it.trackId!!)
                                    }
                                }
                            }
                            item("Definir Pedido como Fraude").action {
                                selectedItem?.let {
                                    information("Você deseja marcar este pedido como Fraude?") {
                                        controller.marcarPedidoSelecionadoComoFraude()
                                        tablePedidos.refresh()
                                    }
                                }
                            }
                        }
                    }
                }

                vbox(5) {
                    prefWidth = 20.0
                    minWidth = 20.0
                    vgrow = Priority.ALWAYS
                }

                vbox {
                    vgrow = Priority.ALWAYS

                    label("Produtos")
                    tableProdutos = tableview(controller.produtos) {
                        minWidth = 450.0
                        vgrow = Priority.ALWAYS

                        rowFactory = Callback {
                            object : TableRow<SdMagentoOrderItemBean>() {
                                override fun updateItem(item: SdMagentoOrderItemBean?, empty: Boolean) {
                                    super.updateItem(item, empty)
                                    styleClass.remove("table-row-danger")
                                    if (item != null && empty.not())
                                        if (item.reservado!!.not())
                                            styleClass.add("table-row-danger")
                                }
                            }
                        }
                        column("SKU", SdMagentoOrderItemBean::sku) {
                            minWidth = 100.0
                            prefWidth = 100.0
                            maxWidth = 100.0
                        }
                        column("Nome", SdMagentoOrderItemBean::name) {
                            minWidth = 180.0
                            maxWidth = 180.0
                            prefWidth = 180.0
                        }
                        column("Quantidade", SdMagentoOrderItemBean::qty) {
                            minWidth = 70.0
                            prefWidth = 70.0
                            maxWidth = 70.0
                        }
                        column("Preço", SdMagentoOrderItemBean::price) {
                            minWidth = 65.0
                            prefWidth = 65.0
                            maxWidth = 65.0

                            cellFormat {
                                text = StringUtils.toMonetaryFormat(it!!.toDouble(), 2)
                            }
                        }
                    }
                }
            }

        }
    }


    override val root = borderpane

}