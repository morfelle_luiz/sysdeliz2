package sysdeliz2.views.ecomerce.fragments


import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Orientation
import javafx.scene.layout.BorderPane
import javafx.scene.layout.Priority
import sysdeliz.apis.web.convertr.data.enums.StatusOrder
import sysdeliz.apis.web.convertr.data.models.orders.OrderStatusUpdate
import sysdeliz2.controllers.ecomerce.ManProdMagentoB2BController
import sysdeliz2.controllers.ecomerce.ManProdMagentoController
import sysdeliz2.controllers.ecomerce.OrderMagentoController
import tornadofx.*
import tornadofx.controlsfx.toggleswitch

class DadosDoProdutoB2BFragment() : Fragment("Informe os campos dos produtos a ser enviado") {

    private val controller: ManProdMagentoB2BController by inject()

    private val borderpane: BorderPane = borderpane {
        prefHeight = 400.0
        prefWidth = 500.0

        vgrow = Priority.ALWAYS
        hgrow = Priority.ALWAYS

        center = hbox {
            form {
                vgrow = Priority.ALWAYS
                hgrow = Priority.ALWAYS

                fieldset("Obrigatórios", null, Orientation.VERTICAL) {
                    vbox {
                        vgrow = Priority.ALWAYS
                        hgrow = Priority.ALWAYS

                        hbox(5) {
                            padding = insets(top = 5, left = -15)
                            toggleswitch("", controller.checkEnviarSKU) {
                                enableWhen(SimpleBooleanProperty(false))
                            }
                            label("Enviar o SKU") {
                                padding = insets(left = 10.0)
                                style {
                                    fontSize = 15.px
                                }
                            }
                        }

                        hbox(5) {
                            padding = insets(top = 5, left = -15)
                            toggleswitch("", controller.checkEnviarName) {
                                enableWhen(SimpleBooleanProperty(false))
                            }
                            label("Enviar o Nome") {
                                padding = insets(left = 10.0)
                                style {
                                    fontSize = 15.px
                                }
                            }
                        }

                        hbox(5) {
                            padding = insets(top = 5, left = -15)
                            toggleswitch("", controller.checkEnviarPrice) {
                                enableWhen(SimpleBooleanProperty(false))
                            }
                            label("Enviar o Preço") {
                                padding = insets(left = 10.0)
                                style {
                                    fontSize = 15.px
                                }
                            }
                        }
                    }
                }

                fieldset("Opcionais", null, Orientation.VERTICAL) {
                    hbox(5) {
                        vbox(5) {
                            vgrow = Priority.ALWAYS
                            hgrow = Priority.ALWAYS

                            hbox(5) {
                                padding = insets(top = 5, left = -15)
                                toggleswitch("", controller.checkEnviarShortDescription)
                                label("Enviar a descrição curta") {
                                    padding = insets(left = 10.0)
                                    style {
                                        fontSize = 15.px
                                    }
                                }
                            }

                            hbox(5) {
                                padding = insets(top = 5, left = -15)
                                toggleswitch("", controller.checkEnviarDescription)
                                label("Enviar a descrição") {
                                    padding = insets(left = 10.0)
                                    style {
                                        fontSize = 15.px
                                    }
                                }
                            }


                            hbox(5) {
                                padding = insets(top = 5, left = -15)
                                toggleswitch("", controller.checkEnviarSpecialPrice)
                                label("Enviar o preço especial") {
                                    padding = insets(left = 10.0)
                                    style {
                                        fontSize = 15.px
                                    }
                                }
                            }

                            hbox(5) {
                                padding = insets(top = 5, left = -15)
                                toggleswitch("", controller.checkEnviarWeight)
                                label("Enviar o Peso") {
                                    padding = insets(left = 10.0)
                                    style {
                                        fontSize = 15.px
                                    }
                                }
                            }

                            hbox(5) {
                                padding = insets(top = 5, left = -15)
                                toggleswitch("", controller.checkEnviarStatus)
                                label("Enviar o status") {
                                    padding = insets(left = 10.0)
                                    style {
                                        fontSize = 15.px
                                    }
                                }
                            }

                            hbox(5) {
                                padding = insets(top = 5, left = -15)
                                toggleswitch("", controller.checkEnviarMetaTitle)
                                label("Enviar o título para SEO") {
                                    padding = insets(left = 10.0)
                                    style {
                                        fontSize = 15.px
                                    }
                                }
                            }

                        }

                        vbox(5) {
                            vgrow = Priority.ALWAYS
                            hgrow = Priority.ALWAYS

                            hbox(5) {
                                padding = insets(top = 5, left = -15)
                                toggleswitch("", controller.checkEnviarMetaKeyword)
                                label("Enviar as palavras chaves") {
                                    padding = insets(left = 10.0)
                                    style {
                                        fontSize = 15.px
                                    }
                                }
                            }

                            hbox(5) {
                                padding = insets(top = 5, left = -15)
                                toggleswitch("", controller.checkEnviarMetaDescription)
                                label("Enviar a descrição para SEO") {
                                    padding = insets(left = 10.0)
                                    style {
                                        fontSize = 15.px
                                    }
                                }
                            }

                            hbox(5) {
                                padding = insets(top = 5, left = -15)
                                toggleswitch("", controller.checkEnviarCategories)
                                label("Enviar as categorias") {
                                    padding = insets(left = 10.0)
                                    style {
                                        fontSize = 15.px
                                    }
                                }
                            }

                            hbox(5) {
                                padding = insets(top = 5, left = -15)
                                toggleswitch("", controller.checkEnviarVariations)
                                label("Enviar as variações") {
                                    padding = insets(left = 10.0)
                                    style {
                                        fontSize = 15.px
                                    }
                                }
                            }

                            hbox(5) {
                                padding = insets(top = 5, left = -15)
                                toggleswitch("", controller.checkEnviarAditionalAttributes)
                                label("Enviar os dados adicionais") {
                                    padding = insets(left = 10.0)
                                    style {
                                        fontSize = 15.px
                                    }
                                }
                            }

                            hbox(5) {
                                padding = insets(top = 5, left = -15)
                                toggleswitch("", controller.checkEnviarLinkProducts)
                                label("Enviar os Links entre produtos") {
                                    padding = insets(left = 10.0)
                                    style {
                                        fontSize = 15.px
                                    }
                                }
                            }
                        }
                    }
                }

                separator {  }

                fieldset("", null, Orientation.VERTICAL) {
                    hbox(10) {
                        button("Enviar Produtos") {
                            styleClass += "success"
                            prefWidth = 150.0
                            style { fontSize = 15.0.px }
                            action {
                                root.runAsyncWithOverlay {
                                    controller.enviarProdutosParaLoja()
                                } ui { qtd ->
                                    controller.tableProdutos!!.refresh()
                                    information("Enviado $qtd produtos para fila.")
                                    this@DadosDoProdutoB2BFragment.close()
                                }
                            }
                        }

                        button("Cancelar") {
                            styleClass += "danger"
                            prefWidth = 150.0
                            style { fontSize = 15.0.px }
                            action {
                                this@DadosDoProdutoB2BFragment.close()
                            }
                        }
                    }
                }
            }
        }
    }

    override val root = borderpane

}