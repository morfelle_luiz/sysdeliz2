package sysdeliz2.views.ecomerce.fragments


import javafx.geometry.Orientation
import javafx.scene.layout.BorderPane
import javafx.scene.layout.Priority
import sysdeliz2.controllers.ecomerce.ManProdMagentoB2BController
import tornadofx.*
import tornadofx.controlsfx.toggleswitch

class FotosDoProdutoB2BFragment() : Fragment("Configuração de envio de fotos") {

    private val controller: ManProdMagentoB2BController by inject()

    private val borderpane: BorderPane = borderpane {
        prefHeight = 180.0
        prefWidth = 300.0

        vgrow = Priority.ALWAYS
        hgrow = Priority.ALWAYS

        center = hbox {
            form {
                vgrow = Priority.ALWAYS
                hgrow = Priority.ALWAYS

                fieldset("Opcionais", null, Orientation.VERTICAL) {
                    hbox(5) {
                        vbox(5) {
                            vgrow = Priority.ALWAYS
                            hgrow = Priority.ALWAYS

                            hbox(5) {
                                padding = insets(top = 5, left = -15)
                                toggleswitch("", controller.checkZerarFotos)
                                label("Zerar Fotos") {
                                    padding = insets(left = 10.0)
                                    style {
                                        fontSize = 15.px
                                    }
                                }
                            }
                        }
                    }
                }

                separator { }

                fieldset("", null, Orientation.VERTICAL) {
                    hbox(10) {
                        button("Enviar Fotos") {
                            styleClass += "success"
                            prefWidth = 150.0
                            style { fontSize = 15.0.px }
                            action {
                                confirm("Confirmar envio das fotos dos produtos",
                                        "Atenção: É necessário que se tenha a primeira foto SEMPRE, ou terá problema na exibição das imagens na loja virtual") {
                                    root.runAsyncWithOverlay {
                                        controller.enviaFotos()
                                    } ui {
                                        controller.tableProdutos!!.refresh()
                                        information("Fotos " + when (controller.checkZerarFotos.value) {
                                            true -> "Zeradas"
                                            false -> "Enviadas"
                                        })
                                        this@FotosDoProdutoB2BFragment.close()
                                    }
                                }
                            }
                        }

                        button("Cancelar") {
                            styleClass += "danger"
                            prefWidth = 150.0
                            style { fontSize = 15.0.px }
                            action {
                                this@FotosDoProdutoB2BFragment.close()
                            }
                        }
                    }
                }
            }
        }
    }

    override val root = borderpane

}