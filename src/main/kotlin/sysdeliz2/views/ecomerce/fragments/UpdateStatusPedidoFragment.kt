package sysdeliz2.views.ecomerce.fragments


import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Orientation
import javafx.scene.layout.Priority
import sysdeliz.apis.web.convertr.data.enums.StatusOrder
import sysdeliz.apis.web.convertr.data.models.orders.OrderStatusUpdate
import sysdeliz2.controllers.ecomerce.OrderMagentoController
import tornadofx.*
import tornadofx.controlsfx.toggleswitch

class UpdateStatusPedidoFragment(val situacao: StatusOrder) : Fragment("Atualizar o status do pedido") {

    private val controller: OrderMagentoController by inject()

    private val inputStatusOrder = SimpleStringProperty(this, "order_status", "")
    private val inputMessageOrder = SimpleStringProperty(this, "order_messages", "")
    private val inputCarrierOrder = SimpleStringProperty(this, "order_carrier", "")
    private val inputTrackId = SimpleStringProperty(this, "order_track_id", "")
    private val inputNotify = SimpleBooleanProperty(this, "order_notify", false)

    private val shppingEnabled = SimpleBooleanProperty(this, "dados_shpping_enabled", false)

    init {
        controller.updateStatusOrder = OrderStatusUpdate()
        disableClose()

        shppingEnabled.value = situacao == StatusOrder.COMPLETE
    }

    private val borderpane = borderpane {
        prefHeight = 400.0
        prefWidth = 500.0

        center = hbox {
            form {
                fieldset("", null, Orientation.VERTICAL) {
                    hbox(5) {
                        hgrow = Priority.ALWAYS
                        field("Cliente") {
                            textfield("${controller.model.customer.value.firstname} ${controller.model.customer.value.lastname}".toProperty()) {
                                isEditable = false
                            }
                        }
                    }
                }

                fieldset("", null, Orientation.VERTICAL) {
                    hbox(5) {
                        field("Pedido Loja") {
                            textfield(controller.model.orderId) {
                                isEditable = false
                            }
                        }

                        field("Pedido ERP") {
                            textfield(controller.model.codigoErp) {
                                isEditable = false
                            }
                        }
                    }
                }

                fieldset("", null, Orientation.VERTICAL) {
                    hbox(5) {
                        field("Status") {
                            combobox(inputStatusOrder, StatusOrder.getListStatusUpdate()) {
                                selectionModel.select(if (situacao == StatusOrder.CANCELADO) 1 else 0)
                                isDisable = situacao == StatusOrder.CANCELADO
                            }
                        }

                        field("Marca") {
                            textfield((if (controller.model.marca.value == "D") "DLZ" else "FLOR").toProperty()) {
                                isEditable = false
                            }
                        }
                    }

                    hbox {
                        field("Mensagem") {
                            textarea(inputMessageOrder) {
                                vgrow = Priority.ALWAYS
                                hgrow = Priority.ALWAYS
                                requestFocus()
                            }
                        }
                    }

                    hbox(5) {
                        field("Trasportador") {
                            combobox(inputCarrierOrder, listOf("Selecione", "Correios", "Braspress", "Próprio")) {
                                //selectionModel.select(0)
                                enableWhen(shppingEnabled)
                                selectionModel.selectFirst()
                                selectionModel.selectedItemProperty().addListener { observable, oldValue, newValue ->
                                    if (inputStatusOrder.get().equals("Em Transporte") && newValue.equals("Braspress")) {
                                        inputMessageOrder.set("" +
                                                "Como acompanhar a entrega do meu pedido?<br/>" +
                                                "<br/>" +
                                                "Para acompanhar o status da sua entrega, com a transportadora, acesse o endereço https://www.braspress.com/rastreie-sua-encomenda/ e preencha os campos:<br/>" +
                                                "CNPJ: Neste campo você deve utilizar o seu CPF, cadastrado no momento da compra.<br/>" +
                                                "Documento: Selecione a opção Nota Fiscal.<br/>" +
                                                "Número: [NUMERO_NF]<br/>" +
                                                "ou clique neste link: " +
                                                "<a href=\"https://blue.braspress.com/site/w/tracking/v3/search?cpfCnpj=" + controller.model.customer.value.taxvat + "&pedidoNf=[NUMERO_NF]\" >RASTREAR PEDIDO</a>");
                                    }
                                }
                            }
                        }

                        field("Código Rastreio") {
                            textfield(inputTrackId) {
                                enableWhen(shppingEnabled)
                            }
                        }
                    }

                    hbox(5) {
                        padding = insets(top = 5, left = -15)
                        toggleswitch("", inputNotify) {
                            //enableWhen(controller.inInsertOrEdit)
                        }
                        label("Notificar o cliente") {
                            padding = insets(left = 10.0)
                            style {
                                fontSize = 15.px
                            }
                        }
                    }
                }

                fieldset("", null, Orientation.VERTICAL) {
                    hbox(10) {
                        button("Enviar") {
                            styleClass += "success"
                            styleClass += "sm"
                            action {
                                val novaSituacao = when (situacao) {
                                    StatusOrder.CANCELADO -> situacao
                                    else -> {
                                        when (inputStatusOrder.value) {
                                            "Cancelado" -> StatusOrder.CANCELADO
                                            "Em Transporte" -> StatusOrder.TRANSPORTADORA
                                            "Entregue" -> StatusOrder.ENTREGUE
                                            else -> StatusOrder.INDEFINIDO
                                        }
                                    }
                                }

                                if (novaSituacao != StatusOrder.INDEFINIDO) {
                                    // Faz alguma coisa

                                    controller.updateStatusOrder = when (novaSituacao) {
                                        StatusOrder.CANCELADO -> {
                                            OrderStatusUpdate(
                                                    orderId = controller.model.orderId.value,
                                                    state = novaSituacao.value,
                                                    notify = if (!inputNotify.value) 0 else 1
                                            )
                                        }
                                        else -> {
                                            inputMessageOrder.set(inputMessageOrder.value.replace("[NUMERO_NF]", inputTrackId.value))
                                            OrderStatusUpdate(
                                                    orderId = controller.model.orderId.value,
                                                    state = novaSituacao.value,
                                                    message = inputMessageOrder.value,
                                                    carrier = inputCarrierOrder.value,
                                                    tackId = inputTrackId.value,
                                                    notify = if (!inputNotify.value) 0 else 1
                                            )
                                        }
                                    }
                                    controller.atualizaStatusPedidoLoja()
                                    controller.gravarStatusDoPedido(novaSituacao.value)
                                    if (!inputTrackId.value.isNullOrEmpty()) {
                                        controller.gravarRastreioNoPedido(inputTrackId.value)
                                    }
                                }
                                this@UpdateStatusPedidoFragment.close()
                            }
                        }

                        button("Cancelar") {
                            styleClass += "danger"
                            styleClass += "sm"
                            action {
                                // Faz alguma coisa
                                this@UpdateStatusPedidoFragment.close()
                            }
                        }
                    }
                }

            }
        }
    }

    override val root = borderpane

}