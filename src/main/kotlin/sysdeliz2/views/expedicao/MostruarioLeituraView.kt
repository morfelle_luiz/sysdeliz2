package sysdeliz2.views.expedicao

import br.com.validators.simple.enums.Severity
import br.com.validators.simple.message.SimpleMessage
import br.com.validators.simple.validator.CustomValidator
import javafx.beans.property.SimpleBooleanProperty
import javafx.geometry.Orientation
import javafx.scene.Parent
import javafx.scene.control.TableRow
import javafx.scene.control.TextField
import javafx.scene.input.KeyCode
import javafx.scene.layout.HBox
import javafx.scene.layout.Priority
import javafx.scene.paint.Color
import javafx.stage.FileChooser
import javafx.util.Callback
import org.controlsfx.control.ToggleSwitch
import sysdeliz2.controllers.expedicao.MostruarioLeituraController
import sysdeliz2.models.sysdeliz.SdMostruariosBean
import sysdeliz2.models.ti.Colecao
import sysdeliz2.models.ti.RepresenTi
import sysdeliz2.utils.GUIUtils
import sysdeliz2.utils.enums.ResultTypeFilter
import sysdeliz2.utils.extensions.loggerFor
import sysdeliz2.utils.extensions.toFormatedDate
import sysdeliz2.utils.gui.window.GenericFilter
import sysdeliz2.utils.stylesheets.SysdelizCSS
import sysdeliz2.utils.ExportExcel
import sysdeliz2.utils.sys.LocalLogger
import sysdeliz2.views.helpers.ViewHelpers
import tornadofx.*
import tornadofx.controlsfx.toggleswitch
import java.sql.SQLException

/**
 *
 * @author lima.joao
 * @since 09/03/2020 08:21
 */
class MostruarioLeituraView : View("") {

    private val controller: MostruarioLeituraController by inject()

    private val logger = loggerFor(javaClass)


    init {
        logger.debug("-------------------------------------------------------------")
        logger.debug("- Iniciando ${this.javaClass.name}")
        logger.debug("-------------------------------------------------------------")

        logger.debug("Importando a tabela de estilos")
        importStylesheet(SysdelizCSS::class)
        logger.debug("Importando tabela de estilo com sucesso")

        logger.debug("Forçando a recarga da tabela de estilos")
        reloadStylesheetsOnFocus()
        logger.debug("Concluido recarga dos estilos")


    }

    private val filtro = titledpane {
        isAnimated = true
        isCollapsible = false
        text = "Filtros"
        hgrow = Priority.ALWAYS
        paddingAll = 5.0

        form {
            padding = insets(5)
            hbox(10) {
                fieldset("", null, Orientation.VERTICAL) {
                    field("Representante") {
                        styleClass += "form-field"
                        textfield(controller.filtroRepresentante) {
                            styleClass += "input-group-apend"
                            prefWidth = 100.0
                            setOnKeyPressed { keyEvent ->
                                if (keyEvent.code == KeyCode.ENTER || keyEvent.code == KeyCode.TAB) {
                                    controller.loadNomeRepresentante()
                                    if(controller.filtroColecao.value != null){
                                        findFilter()
                                    }
                                }
                            }

                            /*
                            setOnKeyPressed { keyEvent ->
                                    if (keyEvent.code == KeyCode.ENTER) {
                                        CustomValidator()
                                                .addIf(
                                                        controller.filtroRepresentante.value.isNullOrEmpty(),
                                                        SimpleMessage.error("Informe um representante", "")
                                                )
                                                .addIf(
                                                        controller.filtroColecao.value.isNullOrEmpty(),
                                                        SimpleMessage.error("Informe uma coleção", "")
                                                )
                                                .execWhen(severity = Severity.ERROR) {
                                                    if (it.isNotEmpty()) {
                                                        error("Erro", it)
                                                    }
                                                }
                                                .execWhenNoMsgs {
                                                    controller.addBarraLida()
                                                    tableRegistros.refresh()
                                                }
                                        requestFocus()
                                        selectAll()
                                    }
                                    if (keyEvent.code == KeyCode.F4) {
                                        controller.inputRemoverBarraLida.value = !controller.inputRemoverBarraLida.value
                                    }
                                }
                             */
                        }

                        textfield(controller.nomeRepresentante) {
                            styleClass += "input-group-apend"
                            prefWidth = 300.0

                            disableWhen(SimpleBooleanProperty(true))
                        }

                        button("", imageview("/images/icons/buttons/find (1).png")) {
                            HBox.setMargin(this, insets(0, 0, 0, -11))
                            styleClass += "last"
                            styleClass += "warning"
                            action {
                                try {
                                    logger.debug("  > Abrindo filtro Representantes")
                                    val filter = object : GenericFilter<RepresenTi?>() {}
                                    filter.show(ResultTypeFilter.SINGLE_RESULT)
                                    controller.filtroRepresentante.value = filter.selectedReturn?.codRep ?: ""
                                    controller.nomeRepresentante.value = filter.selectedReturn?.nome ?: ""
                                } catch (e: SQLException) {
                                    e.printStackTrace()
                                    LocalLogger.addLog(e.message + "::" + e.localizedMessage, this.javaClass.name + ":" + this.javaClass.enclosingMethod.name)
                                    GUIUtils.showException(e)
                                }
                            }
                        }

                    }

                }
                fieldset("", null, Orientation.VERTICAL) {
                    field("Coleção") {
                        styleClass += "form-field"
                        textfield(controller.filtroColecao) {
                            styleClass += "input-group-apend"
                            prefWidth = 100.0
                            setOnKeyPressed { keyEvent ->
                                if (keyEvent.code == KeyCode.ENTER || keyEvent.code == KeyCode.TAB) {
                                    if(controller.filtroColecao.value != null){
                                        findFilter()
                                    }
                                }
                            }
                        }
                        button("", imageview("/images/icons/buttons/find (1).png")) {
                            HBox.setMargin(this, insets(0, 0, 0, -11))
                            styleClass += "last"
                            styleClass += "warning"
                            action {
                                logger.debug("  > Abrindo filtro coleção")
                                try {
                                    val filter = object : GenericFilter<Colecao?>() {}
                                    filter.show(ResultTypeFilter.SINGLE_RESULT)
                                    controller.filtroColecao.value = filter.selectedReturn?.codigo ?: ""

                                } catch (e: SQLException) {
                                    e.printStackTrace()
                                    LocalLogger.addLog(e.message + "::" + e.localizedMessage, this.javaClass.name + ":" + this.javaClass.enclosingMethod.name)
                                    GUIUtils.showException(e)
                                }
                            }
                        }
                    }
                }

                fieldset("", null, Orientation.VERTICAL) {
                    field("Tipo") {
                        styleClass += "form-field"
                        combobox(controller.filtroTipo, listOf("Saida", "Entrada")).selectionModel.selectFirst()
                    }
                }

                hbox {
                    paddingAll = 15.0
                    button("Consultar", imageview("/images/icons/buttons/find (1).png")) {
                        style {
                            fontSize = 18.px
                            styleClass += "success"
                        }
                        action {
                            findFilter()
                        }
                    }
                }

            }
        }
    }

    private fun findFilter(){
        root.runAsyncWithOverlay {
            controller.findWithFilter()
            runLater {
                controller.inputRemoverBarraLida.value = false
            }
        }
    }

    private val tableRegistros = tableview(controller.registros) {
        hgrow = Priority.ALWAYS
        vgrow = Priority.ALWAYS
        nestedColumn("SKU") {
            column("Codigo", SdMostruariosBean::codigo)
            column("Tamanho", SdMostruariosBean::tam)
            column("Cor", SdMostruariosBean::cor)
        }
        column("Coleção", SdMostruariosBean::colecao)
        column("Tipo", SdMostruariosBean::entSai) {
            cellFormat {
                text = if (it ?: "E" == "E") {
                    "Entrada"
                } else {
                    "Saida"
                }
            }
        }
        column("Data Leitura", SdMostruariosBean::dataLeitura) {
            cellFormat {
                text = it?.toFormatedDate() ?: ""
            }
        }

        rowFactory = Callback {
            object : TableRow<SdMostruariosBean>() {
                override fun updateItem(item: SdMostruariosBean?, empty: Boolean) {
                    super.updateItem(item, empty)

                    background = Color.TRANSPARENT.asBackground()
                    if (item != null) {
                        if (controller.registros.stream().anyMatch { it.codigo.equals(item.codigo) && it != item })
                            background = Color.TOMATO.asBackground()
                    }
                }
            }
        }
    }

    private var textInput: TextField? = null
    private var textSomatorio: TextField? = null

    private var checkRemover: ToggleSwitch? = null

    val pane = borderpane {
        prefHeight = 1080.0
        prefWidth = 1920.0
        // Insere o topo do border pane
        top = ViewHelpers.buildHBoxTopo(
                "/images/icons/cadastro entradas (2).png",
                "Leitura do Mostruário",
                ">Expedição>Leitura do Mostruário"
        )

        center = vbox(5) {
            hgrow = Priority.ALWAYS
            vgrow = Priority.ALWAYS
            paddingAll = 5.0
            add(filtro)

            // Leitura
            form {
                hbox(10) {
                    fieldset("", null, Orientation.VERTICAL) {
                        field("Barra") {

                            style {
                                fontSize = 15.px
                            }

                            textInput = textfield(controller.inputBarraLida) {

                                style {
                                    fontSize = 20.px
                                }

                                prefWidth = 300.0
                                minWidth = 300.0
                                maxWidth = 300.0

                                // executa ação ao pressionar enter
                                setOnKeyPressed { keyEvent ->
                                    if (keyEvent.code == KeyCode.ENTER) {
                                        CustomValidator()
                                                .addIf(
                                                        controller.filtroRepresentante.value.isNullOrEmpty(),
                                                        SimpleMessage.error("Informe um representante", "")
                                                )
                                                .addIf(
                                                        controller.filtroColecao.value.isNullOrEmpty(),
                                                        SimpleMessage.error("Informe uma coleção", "")
                                                )
                                                .execWhen(severity = Severity.ERROR) {
                                                    if (it.isNotEmpty()) {
                                                        error("Erro", it)
                                                    }
                                                }
                                                .execWhenNoMsgs {
                                                    controller.addBarraLida()
                                                    controller.inputBarraLida.set("")
                                                    tableRegistros.refresh()
                                                }
                                        requestFocus()
                                        selectAll()
                                    }
                                    if (keyEvent.code == KeyCode.F4) {
                                        controller.inputRemoverBarraLida.value = !controller.inputRemoverBarraLida.value
                                    }
                                }
                            }
                        }
                    }

                    fieldset("", null, Orientation.VERTICAL) {
                        field("") {
                            padding = insets(top = 25, left = -15)
                            checkRemover = toggleswitch("", controller.inputRemoverBarraLida) {
                                style {
                                    fontSize = 20.px
                                }
                                enableWhen(SimpleBooleanProperty(this, "algo", false))
                            }
                            label("Remover Barra (F4)") {
                                padding = insets(left = 10.0)
                                style {
                                    fontSize = 20.px
                                }
                            }
                        }
                    }

                    fieldset("", null, Orientation.VERTICAL) {
                        field("Quantidade Lida") {
                            textSomatorio = textfield(controller.somatorio) {
                                style {
                                    fontSize = 20.px
                                }
                                prefWidth = 150.0
                                minWidth = 150.0
                                maxWidth = 150.0
                                isEditable = false
                            }
                        }
                    }
                }
            }

            add(tableRegistros)
        }

        bottom = hbox(10) {
            vgrow = Priority.ALWAYS
            paddingAll = 5
            button("Exportar para Excel") {
                styleClass += "info"
                style {
                    fontSize = 15.0.px
                }
                action {
                    val exporter = ExportExcel(controller.registros, SdMostruariosBean::class.java)
                    val fileChooser = FileChooser()
                    fileChooser.extensionFilters.addAll(
                            FileChooser.ExtensionFilter("XLSX files (*.xlsx)", "*.xlsx"),
                            FileChooser.ExtensionFilter("XLS files (*.xls)", "*.xls")
                    )
                    val saveFile = fileChooser.showSaveDialog(tableRegistros.scene.window)

                    if(saveFile != null){
                        exporter.exportArrayToExcel(saveFile.absolutePath)
                    }
                }
            }
        }
    }

    override val root: Parent = pane
}