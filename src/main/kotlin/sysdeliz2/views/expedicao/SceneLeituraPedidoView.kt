package sysdeliz2.views.expedicao

import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.Parent
import javafx.scene.layout.*
import javafx.scene.paint.Color
import javafx.scene.text.Font
import sysdeliz2.utils.stylesheets.SysdelizCSS
import sysdeliz2.daos.generics.implementation.GenericDaoImpl
import sysdeliz2.models.view.VProdReservaColeta
import tornadofx.*
import java.math.BigDecimal

/**
 * @author lima.joao
 * @since 09/10/2019 11:54
 */
object SceneLeituraPedidoView : View("") {

    private var itensLidos = FXCollections.observableArrayList<VProdReservaColeta>()
    private var itensPendentes = FXCollections.observableArrayList<VProdReservaColeta>()
    private var itemSelecionado: VProdReservaColeta?

    private var outputReferenciaLer = SimpleStringProperty(this, "referencia")
    private var outputDescricaoLer = SimpleStringProperty(this, "descricao")
    private var outputTamanhoLer = SimpleStringProperty(this, "tamanho")
    private var outputCorLer = SimpleStringProperty(this, "cor")
    private var outputLocalLer = SimpleStringProperty(this, "local")

    private var daoVProdReservaColeta = GenericDaoImpl(VProdReservaColeta::class.java)

    init {

        importStylesheet(SysdelizCSS::class)

        // Recarrega os estilos ao carregar a tela
        reloadStylesheetsOnFocus()

        val lista = daoVProdReservaColeta
                .initCriteria()
                .addPredicateEq("usuarioLeitura", "JOAO FERNANDES")
                .loadListByPredicate() as ObservableList<VProdReservaColeta>

        itensPendentes.setAll(lista)

        itemSelecionado = if (itensPendentes.size > 0) {
            itensPendentes[0]
        } else {
            VProdReservaColeta()
        }

        outputReferenciaLer.set(itemSelecionado!!.codigo)
        outputDescricaoLer.set(itemSelecionado!!.descricao)
        outputTamanhoLer.set(itemSelecionado!!.tamanhos)
        outputCorLer.set(itemSelecionado!!.descCor)
        outputLocalLer.set(itemSelecionado!!.local)
    }

    private fun setMarginsXBox(node: Node, insets: Insets) {
        VBox.setMargin(node, insets)
        HBox.setMargin(node, insets)
    }

    var pane: BorderPane = borderpane {
        prefHeight = 749.0
        prefWidth = 1075.0

        top = hbox(5, Pos.CENTER_LEFT) {
            prefHeight = 55.0
            prefWidth = 200.0
            style = "-fx-background-color: #F5F5F5;"

            imageview("/images/48-barcode.png") {
                fitHeight = 65.0
                fitWidth = 55.0
                isPickOnBounds = true
                isPreserveRatio = true
            }

            label("Expedição > Leitura de Reservas") {
                font = Font.font("System Bold", 18.0)
            }

            paddingAll = 0.0
        }

        center = vbox() {
            hgrow = Priority.ALWAYS
            vgrow = Priority.ALWAYS

            form {
                hgrow = Priority.ALWAYS

                hbox(spacing = 5.0) {
                    fieldset("Reserva") {
                        style {
                            fontSize = 25.px
                            backgroundColor += c(SysdelizCSS.colorFieldSet)
                            padding = box(5.px)
                            backgroundRadius += box(15.px)
                        }
                        field(itemSelecionado!!.reserva ?: "") {
                            prefWidth = 150.0
                        }
                    }

                    fieldset("Pedido") {
                        style {
                            fontSize = 25.px
                            backgroundColor += c(SysdelizCSS.colorFieldSet)
                            padding = box(5.px)
                            backgroundRadius += box(15.px)
                        }
                        field(itemSelecionado!!.numero ?: "") {
                            prefWidth = 150.0
                        }
                    }

                    vbox(spacing = 5.0) {
                        fieldset("Cliente") {
                            style {
                                fontSize = 10.px
                                backgroundColor += c(SysdelizCSS.colorFieldSet)
                                padding = box(5.px)
                                backgroundRadius += box(15.px)
                            }
                            field(itemSelecionado!!.cliente ?: "")
                        }

                        fieldset("Transportadora") {
                            style {
                                fontSize = 10.px
                                backgroundColor += c(SysdelizCSS.colorFieldSet)
                                padding = box(5.px)
                                backgroundRadius += box(15.px)
                            }
                            field(itemSelecionado!!.trans ?: "")
                        }
                    }

                    vbox(spacing = 5.0) {
                        fieldset("Obs.:") {
                            style {
                                fontSize = 10.px
                                backgroundColor += c(SysdelizCSS.colorFieldSet)
                                padding = box(5.px)
                                backgroundRadius += box(15.px)
                            }

                            field(itemSelecionado!!.obs ?: "")

                        }
                    }
                }

                hbox(spacing = 5.0) {
                    prefHeight = 5.0
                }

                hbox(spacing = 5.0) {

                    fieldset("Referência") {
                        style {
                            fontSize = 25.px
                            backgroundColor += c(SysdelizCSS.colorFieldSet)
                            padding = box(5.px)
                            backgroundRadius += box(15.px)
                        }
                        textfield(outputReferenciaLer) {
                            prefWidth = 150.0
                        }.disableProperty().set(true)
                    }

                    fieldset("Descrição") {
                        style {
                            fontSize = 25.px
                            backgroundColor += c(SysdelizCSS.colorFieldSet)
                            padding = box(5.px)
                            backgroundRadius += box(15.px)
                        }
                        textfield(outputDescricaoLer) {
                            prefWidth = 450.0
                        }.disableProperty().set(true)
                    }

                    fieldset("Tam") {
                        style {
                            fontSize = 25.px
                            backgroundColor += c(SysdelizCSS.colorFieldSet)
                            padding = box(5.px)
                            backgroundRadius += box(15.px)
                        }
                        textfield(outputTamanhoLer) {
                            prefWidth = 75.0
                        }.disableProperty().set(true)
                    }

                    fieldset("Cor") {
                        style {
                            fontSize = 25.px
                            backgroundColor += c(SysdelizCSS.colorFieldSet)
                            padding = box(5.px)
                            backgroundRadius += box(15.px)
                        }
                        textfield(outputCorLer) {
                            prefWidth = 200.0
                        }.disableProperty().set(true)
                    }

                    fieldset("Local") {
                        style {
                            fontSize = 25.px
                            backgroundColor += c(SysdelizCSS.colorFieldSet)
                            padding = box(5.px)
                            backgroundRadius += box(15.px)
                        }
                        textfield(outputLocalLer) {
                            prefWidth = 150.0
                        }.disableProperty().set(true)
                    }

                    fieldset("Ações") {
                        style {
                            backgroundColor += c(SysdelizCSS.colorFieldSet)
                            padding = box(5.px)
                            backgroundRadius += box(15.px)
                            fontSize = 25.px
                        }
                        hbox(5.0) {
                            button("Pular Referência") {
                                style {
                                    backgroundColor += Color.DARKRED
                                    fontSize = 30.px
                                    textFill = Color.WHITE
                                    backgroundRadius += box(15.px)
                                    borderColor += box(c(SysdelizCSS.colorFieldSet))
                                }
                            }.action {
                                if (itemSelecionado!!.codigo != null && itemSelecionado!!.codigo != "") {
                                    itensLidos.add(itemSelecionado)
                                    itensPendentes.remove(itemSelecionado)
                                    if (itensPendentes.size > 0) {
                                        itemSelecionado = itensPendentes[0]
                                        outputReferenciaLer.set(itemSelecionado!!.codigo)
                                        outputDescricaoLer.set(itemSelecionado!!.descricao)
                                        outputTamanhoLer.set(itemSelecionado!!.tamanhos)
                                        outputCorLer.set(itemSelecionado!!.descCor)
                                        outputLocalLer.set(itemSelecionado!!.local)
                                    } else {
                                        itemSelecionado = VProdReservaColeta()
                                        outputDescricaoLer.set("")
                                        outputReferenciaLer.set("")
                                        outputTamanhoLer.set("")
                                        outputCorLer.set("")
                                        outputLocalLer.set("")
                                    }
                                }
                            }

                            button("Informar Falta") {
                                style {
                                    backgroundColor += Color.ORANGE
                                    fontSize = 30.px
                                    backgroundRadius += box(15.px)
                                    borderColor += box(c(SysdelizCSS.colorFieldSet))
                                }
                            }
                        }
                    }
                }
            }

            hbox(spacing = 5.0) {
                setMarginsXBox(this, Insets(5.0, 0.0, 5.0, 10.0))
                vbox {
                    label("Referências Lidas")
                    tableview<VProdReservaColeta>(itensLidos) {
                        hgrow = Priority.ALWAYS
                        column("Código", VProdReservaColeta::codigo)
                        column("Cor", VProdReservaColeta::cor)
                        column("Tamanho", VProdReservaColeta::tamanhos)
                        column("Qtd", VProdReservaColeta::qtde)
                        column<VProdReservaColeta, BigDecimal>("Saldo") { cellDataFeatures ->
                            if (cellDataFeatures.value.qtdLida == null) {
                                cellDataFeatures.value.qtdLida = BigDecimal.ZERO
                            }
                            cellDataFeatures.value.observable<BigDecimal>("qtdLida")
                        }
                        rowFactory
                    }
                }

                hbox {
                    hgrow = Priority.ALWAYS
                    vbox {
                        label("Referencia Atual")
                        hbox(spacing = 5.0) {
                            textfield(outputReferenciaLer) { prefWidth = 70.0 }
                            textfield(outputDescricaoLer) { prefWidth = 300.0 }
                        }
                    }
                }

                hbox {
                    label("caixas abertas")
                }
            }
        }
    }

    override val root: Parent = pane


}
