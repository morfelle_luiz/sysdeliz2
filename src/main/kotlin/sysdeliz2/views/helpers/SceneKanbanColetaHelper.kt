package sysdeliz2.views.helpers

/**
 * @author lima.joao
 * @since 30/10/2019 13:35
 */
object SceneKanbanColetaHelper {
    @JvmField
    var HEIGHT_VBOX_LEITOR = 364.0
    @JvmField
    var WIDTH_VBOX_LEITOR = 290.0
    @JvmField
    var MARGIN_DEFAULT = 10.0
    @JvmField
    var HEIGHT_HBOX_BOTOES = 50.0
    @JvmField
    var PADING_DEFAULT = 5.0
    var WIDTH_COLUMN_ORDEM = 20.0
    @JvmField
    var WIDTH_COLUMN_PEDIDO = 68.0
    @JvmField
    var WIDTH_COLUMN_RESERVA = 68.0
    @JvmField
    var WIDTH_COLUMN_STATUS = 65.0
    @JvmField
    var WIDTH_COLUMN_ACTIONS = 67.0
    @JvmField
    var WIDTH_BUTONS_ACTIONS = 20.0
}