package sysdeliz2.views.helpers

import javafx.geometry.Pos
import javafx.scene.Parent
import javafx.scene.layout.HBox
import javafx.scene.layout.Priority
import javafx.scene.paint.Color
import sysdeliz2.utils.Globals
import tornadofx.*

/**
 *
 * @author lima.joao
 * @since 14/01/2020 15:14
 */
object ViewHelpers: View("") {

    fun buildHBoxTopo(urlIcon: String, titulo: String, caminho: String): HBox {
        return hbox(5, Pos.BOTTOM_RIGHT) {
            prefHeight = 0.0
            prefWidth = 711.0
            style {
                backgroundColor = multi(c(if(Globals.getEmpresaLogada().codigo == "1000") "#802046" else "rgba(33, 37, 43, 0.95)"))
            }
            paddingAll = 5
            hbox(5.0, Pos.CENTER_LEFT) {
                prefHeight = 24.0
                prefWidth = 699.0
                hgrow = Priority.ALWAYS
                imageview(urlIcon) {
                    fitHeight = 60.0
                    fitWidth = 60.0
                    isPickOnBounds = true
                    isPreserveRatio = true
                }
                label(titulo){
                    prefHeight = 22.0
                    prefWidth = 437.0
                    textFill = Color.WHITE
                    style {
                        fontSize = 20.0.px
                    }
                }
            }
            label(caminho){
                alignment = Pos.BOTTOM_RIGHT
                prefHeight = 40.0
                prefWidth = 287.0
                textFill = Color.WHITE
                graphic = imageview("/images/icons/buttons/home (1).png")
            }
        }
    }

    // Não usado. Declarado apenas para poder ter acesso aos métodos helpers de tela
    override val root = hbox{}
}