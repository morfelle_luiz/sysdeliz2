package sysdeliz2.controllers.ecomerce

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import sysdeliz2.dao.FluentDao
import sysdeliz2.models.sysdeliz.SdMagentoOrderBean

/**
 * @author lima.joao
 * @since 16/03/2020 10:29
 */
internal class OrderMagentoControllerTest {

    @Test
    fun teste(){
        val teste = FluentDao()
                .selectFrom(SdMagentoOrderBean::class.java)
                .where {
                    it.equal("orderId", "100000015")
                            .equal("marca", "F")
                }.singleResult<SdMagentoOrderBean>()


    }

}