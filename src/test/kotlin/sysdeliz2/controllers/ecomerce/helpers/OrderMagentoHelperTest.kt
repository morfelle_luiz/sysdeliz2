package sysdeliz2.controllers.ecomerce.helpers

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

/**
 * @author lima.joao
 * @since 17/03/2020 10:50
 */
internal class OrderMagentoHelperTest {

    @Test
    fun `o percentual deve ser 30% quando os 3 parametros forem passado corretamente`() {
        val percentual = OrderMagentoHelper.getPercentualDesconto(269.62.toBigDecimal(), 105.57.toBigDecimal(), 23.29.toBigDecimal())
        assertEquals(0.30.toBigDecimal().multiply(100.toBigDecimal()).toBigInteger(), percentual.toBigInteger())
    }

    @Test
    fun `o percentual deve ser diferente 30% quando não for passado o frete e a venda possuir frete`() {
        val percentual = OrderMagentoHelper.getPercentualDesconto(269.62.toBigDecimal(), 105.57.toBigDecimal(), 0.toBigDecimal())
        assertNotEquals(0.30.toBigDecimal().multiply(100.toBigDecimal()).toBigInteger(), percentual.toBigInteger())
    }

}