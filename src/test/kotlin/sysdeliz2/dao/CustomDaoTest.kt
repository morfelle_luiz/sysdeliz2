package sysdeliz2.dao

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import sysdeliz2.models.sysdeliz.SdMagentoOrderBean
import sysdeliz2.models.ti.Pedido
import sysdeliz2.models.ti.PedidoEComerceBean
import sysdeliz2.models.view.VSdCoresMagentoProduto

/**
 * @author lima.joao
 * @since 26/02/2020 16:47
 */
internal class CustomDaoTest {


    @Test
    fun simpleTeste() {
        val teste = FluentDao()
                .selectFrom(VSdCoresMagentoProduto::class.java)
                .where {
                    it.equal("id.codigo", "D9000")
                }.resultList<VSdCoresMagentoProduto>()

        if(teste!!.isEmpty()) {
            // algo
        }


//        val genericDao = GenericDaoImpl<SdMagentoOrderBean>(SdMagentoOrderBean::class.java)
//        genericDao.initCriteria()
//        genericDao.addPredicateGt("customer.id", 0)
//        val teste = genericDao.loadListByPredicate()


//        val result = FluentDao()
//                .selectFrom(SdMagentoOrderBean::class.java)
//                .where { predicateBuilder ->
//                    predicateBuilder
//                            .and {
//                                it.greaterThan("orderId", "100000002")
//                            }
//                }.resultList<SdMagentoOrderBean>()

        //assertNotNull(result)
    }

}