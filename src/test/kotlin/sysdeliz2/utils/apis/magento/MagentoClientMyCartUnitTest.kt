package sysdeliz2.utils.apis.magento


/**
 * Created by xschen on 10/7/2017.
 */
class MagentoClientMyCartUnitTest {

//    @Test
//    fun test_newCart() {
//        val client = MagentoClient(Mediator.url, BasicHttpComponent())
//        val token = client.loginAsClient(Mediator.customerUsername, Mediator.customerPassword)
//        val cartId = client.myCart.newQuote()
//        val cart = client.myCart.getCart()
//        val cartTotal = client.myCart.getCartTotal()
//
//        logger.info("token: {}", token)
//        logger.info("cart: \r\n{}", JSON.toJSONString(cart, SerializerFeature.PrettyFormat))
//        logger.info("cartTotal: \r\n{}", JSON.toJSONString(cartTotal, SerializerFeature.PrettyFormat))
//    }
//
//    @Test
//    fun test_addItemToCart() {
//        val client = MagentoClient(Mediator.url, BasicHttpComponent())
//        client.loginAsClient(Mediator.customerUsername, Mediator.customerPassword)
//        val quoteId = client.myCart.newQuote()
//
//        var item = CartItem()
//        item.qty = 1
//        item.sku = "product_dynamic_758"
//
//        println(quoteId)
//
//        item = client.myCart.addItemToCart(quoteId!!, item)!!
//
//        val cart = client.myCart.getCart()
//        val cartTotal = client.myCart.getCartTotal()
//
//        logger.info("cartItem: \r\n{}", JSON.toJSONString(item, SerializerFeature.PrettyFormat))
//        logger.info("cart: \r\n{}", JSON.toJSONString(cart, SerializerFeature.PrettyFormat))
//        logger.info("cartTotal: \r\n{}", JSON.toJSONString(cartTotal, SerializerFeature.PrettyFormat))
//    }
//
//    @Test
//    fun test_updateItemInCart() {
//        val client = MagentoClient(Mediator.url, BasicHttpComponent())
//        client.loginAsClient(Mediator.customerUsername, Mediator.customerPassword)
//        val quoteId = client.myCart.newQuote()
//
//        var item = CartItem()
//        item.qty = 1
//        item.sku = "product_dynamic_758"
//
//        item = client.myCart.addItemToCart(quoteId!!, item)!!
//        item.qty = 3
//        item = client.myCart.updateItemInCart(quoteId, item)!!
//
//        val cart = client.myCart.getCart()
//        val cartTotal = client.myCart.getCartTotal()
//
//        logger.info("cartItem: \r\n{}", JSON.toJSONString(item, SerializerFeature.PrettyFormat))
//        logger.info("cart: \r\n{}", JSON.toJSONString(cart, SerializerFeature.PrettyFormat))
//        logger.info("cartTotal: \r\n{}", JSON.toJSONString(cartTotal, SerializerFeature.PrettyFormat))
//    }
//
//    @Test
//    fun test_deleteItemInCart() {
//        val client = MagentoClient(Mediator.url, BasicHttpComponent())
//        client.loginAsClient(Mediator.customerUsername, Mediator.customerPassword)
//        val quoteId = client.myCart.newQuote()
//
//        var item = CartItem()
//        item.qty = 1
//        item.sku = "product_dynamic_758"
//
//        item = client.myCart.addItemToCart(quoteId!!, item)!!
//        val result = client.myCart.deleteItemInCart(item.item_id!!)
//
//        val cart = client.myCart.getCart()
//        val cartTotal = client.myCart.getCartTotal()
//
//        logger.info("result: {}", result)
//        logger.info("cart: \r\n{}", JSON.toJSONString(cart, SerializerFeature.PrettyFormat))
//        logger.info("cartTotal: \r\n{}", JSON.toJSONString(cartTotal, SerializerFeature.PrettyFormat))
//    }
//
//    @Test
//    fun test_transferGuestCartToMyCart() {
//        val client = MagentoClient(Mediator.url, BasicHttpComponent())
//
//        val cartId = client.guestCart.newCart()
//
//        var item = CartItem()
//        item.qty = 1
//        item.sku = "product_dynamic_758"
//
//        item = client.guestCart.addItemToCart(cartId!!, item)!!
//
//        client.loginAsClient(Mediator.customerUsername, Mediator.customerPassword)
//        val result = client.myCart.transferGuestCartToMyCart(cartId)
//
//        val cart = client.myCart.getCart()
//        val cartTotal = client.myCart.getCartTotal()
//
//        logger.info("result: {}", result)
//        logger.info("cart: \r\n{}", JSON.toJSONString(cart, SerializerFeature.PrettyFormat))
//        logger.info("cartTotal: \r\n{}", JSON.toJSONString(cartTotal, SerializerFeature.PrettyFormat))
//    }
//
//    companion object {
//        private val logger = LoggerFactory.getLogger(MagentoClientMyCartUnitTest::class.java)
//    }
}
