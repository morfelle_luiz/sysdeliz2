package sysdeliz2.utils.apis.magento

/**
 * Created by xschen on 15/6/2017.
 */
class MagentoClientProductMediaUnitTest {

//    @Test
//    fun test_get_product_media_list() {
//        val productSku = "B202-SKU"
//        val client = MagentoClient(Mediator.url, BasicHttpComponent())
//        client.loginAsAdmin(Mediator.adminUsername, Mediator.adminPassword)
//        logger.info("media list: \r\n{}", JSON.toJSONString(client.media.getProductMediaList(productSku), SerializerFeature.PrettyFormat))
//    }
//
//    @Test
//    fun test_get_product_media_urls() {
//        val productSku = "B202-SKU"
//        val client = MagentoClient(Mediator.url, BasicHttpComponent())
//        client.loginAsAdmin(Mediator.adminUsername, Mediator.adminPassword)
//        logger.info("media absolute urls: \r\n{}", JSON.toJSONString(client.media.getProductMediaAbsoluteUrls(productSku), SerializerFeature.PrettyFormat))
//        logger.info("media relative urls: \r\n{}", JSON.toJSONString(client.media.getProductMediaRelativeUrls(productSku), SerializerFeature.PrettyFormat))
//    }
//
//    @Test
//    fun test_get_product_media_url() {
//        val productSku = "B202-SKU"
//        val client = MagentoClient(Mediator.url, BasicHttpComponent())
//        client.loginAsAdmin(Mediator.adminUsername, Mediator.adminPassword)
//        val entryId = 1L
//        logger.info("media absoluate url: \r\n{}", JSON.toJSONString(client.media.getProductMediaAbsoluteUrl(productSku, entryId), SerializerFeature.PrettyFormat))
//        logger.info("media relative url: \r\n{}", JSON.toJSONString(client.media.getProductMediaRelativeUrl(productSku, entryId), SerializerFeature.PrettyFormat))
//    }
//
//    @Test
//    fun test_get_product_media() {
//        val productSku = "B202-SKU"
//        val client = MagentoClient(Mediator.url, BasicHttpComponent())
//        client.loginAsAdmin(Mediator.adminUsername, Mediator.adminPassword)
//        val entryId = 1L
//        logger.info("media: \r\n{}", JSON.toJSONString(client.media.getProductMedia(productSku, entryId), SerializerFeature.PrettyFormat))
//    }
//
//    @Test
//    fun test_delete_product_media() {
//        val productSku = "B202-SKU"
//        val client = MagentoClient(Mediator.url, BasicHttpComponent())
//        client.loginAsAdmin(Mediator.adminUsername, Mediator.adminPassword)
//        val entryId = 2L
//        logger.info("media deleted: \r\n{}", JSON.toJSONString(client.media.deleteProductMedia(productSku, entryId), SerializerFeature.PrettyFormat))
//    }
//
//    @Test
//    @Throws(IOException::class)
//    fun test_upload_image() {
//        val productSku = "B202-SKU"
//
//        val client = MagentoClient(Mediator.url, BasicHttpComponent())
//        client.loginAsAdmin(Mediator.adminUsername, Mediator.adminPassword)
//
//        val filename = "/m/b/mb01-blue-0.png"
//        val position = 1
//        val type = "image/png"
//        val imageFileName = "new_image.png"
//
//        val inputStream = MagentoClientProductUnitTest::class.java.classLoader.getResourceAsStream("sample.png")
//
//        val baos = ByteArrayOutputStream()
//
//        var bytes = ByteArray(1024)
//
//        while (true){
//            val length = inputStream!!.read(bytes, 0, 1024)
//            if(length <= 0) break
//            baos.write(bytes, 0, length)
//        }
//
//        bytes = baos.toByteArray()
//        logger.info("uploaded image id: {}", client.media.uploadProductImage(productSku, position, filename, bytes, type, imageFileName))
//    }
//
//    @Test
//    @Throws(IOException::class)
//    fun test_update_image() {
//        val productSku = "B202-SKU"
//
//        val client = MagentoClient(Mediator.url, BasicHttpComponent())
//        client.loginAsAdmin(Mediator.adminUsername, Mediator.adminPassword)
//
//        val filename = "/m/b/mb01-blue-0.png"
//        val position = 1
//        val type = "image/png"
//        val imageFileName = "new_image.png"
//
//        val inputStream = MagentoClientProductUnitTest::class.java.classLoader.getResourceAsStream("sample.png")
//
//        val baos = ByteArrayOutputStream()
//        var bytes = ByteArray(1024)
//
//        while (true){
//            val length = inputStream!!.read(bytes, 0, 1024)
//            if(length <= 0) break
//            baos.write(bytes, 0, length)
//        }
//
//        bytes = baos.toByteArray()
//        val entryId = 3L
//        logger.info("image updated: {}", client.media.updateProductImage(productSku, entryId, position, filename, bytes, type, imageFileName))
//    }
//
//    companion object {
//
//        private val logger = LoggerFactory.getLogger(MagentoClientProductMediaUnitTest::class.java)
//    }

}
